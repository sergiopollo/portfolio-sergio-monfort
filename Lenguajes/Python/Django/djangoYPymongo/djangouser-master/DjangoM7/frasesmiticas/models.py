from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class User(AbstractUser):
    nFrases = models.IntegerField(default=0)


class Tag(models.Model):
    nombre_tag = models.CharField(max_length=50, unique=True)
    nsfw = models.BooleanField(default=False)

class Frase(models.Model):
    texto_frase = models.CharField(max_length=500)
    data = models.DateTimeField('fecha frase')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='frasesu')
    tags = models.ManyToManyField(Tag, related_name='frases')

