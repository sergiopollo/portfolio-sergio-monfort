from django.urls import path
from . import views



app_name = "frasesmiticas"
urlpatterns = [
    path('', views.index, name="index"),
    path('gordo', views.kilos, name="kilos"),
    path('addFrase', views.addFrase, name="addFrase"),
    path('postAddFrase', views.postAddFrase, name="postAddFrase"),
    path('fraseRandom', views.fraseRandom, name="fraseRandom"), #fraseRandomNSFW
    path('fraseRandomSFW', views.fraseRandomSFW, name="fraseRandomSFW"),
    path('frasesDe/<str:nombre>/', views.frasesDe, name="frasesDe"),
    path('fraseDe/<str:nombre>/', views.fraseDe, name="fraseDe"),
    path('addTag/<int:id>/', views.addTag, name="addTag"),
    path('addTagPost/<int:idF>/', views.addTagPost, name="addTagPost"),
    path('removeTag/<int:idF>/<str:nomTag>', views.removeTag, name="removeTag")

]

