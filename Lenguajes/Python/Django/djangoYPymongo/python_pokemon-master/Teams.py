from random import choices, randrange
from typing import Type
from mongoengine import *
import pymongo
from pymongo import MongoClient
from datetime import date, datetime
from pprint import pprint

# nos conectamos a la base de datos example
db = connect('project')

client = MongoClient()
db = client["project"]
pokemons = db["pokemon"]
# borra la bd
#db.drop_database('example')
#db.drop_collection("team")
#db.drop_collection("move")

print(0)

# Choice, algo tiene que estar en estos valores. Funciona como un enum
#SINO = ('Si', 'No')
Type = ('Bug','Dark','Dragon','Electric','Fairy','Fighting','Fire','Flying','Ghost','Grass','Ground','Ice','Normal','Poison','Psychic','Rock','Steel','Water')

#AQUI LOS MOVIMIENTOS
class Move(Document):
    meta = {'allow_inheritance': True}
    name = StringField(required=True)
    pwr = DecimalField(required=True, min_value=0, max_value=180)
    tipus = StringField(choices=Type, required=True)

class FastMove(Move):
    energyGain = IntField(required=True, min_value=0, max_value=20)

class ChargedMove(Move):
    energyCost = IntField(required=True, min_value=33, max_value=100)

"""
steelwing = FastMove(name='Steel Wing', pwr = 11, tipus = 'Steel' )
steelwing.energyGain = 7
steelwing.save()

dragontail = FastMove(name='Dragon Tail', pwr = 15, tipus = 'Dragon' )
dragontail.energyGain = 8
dragontail.save()

doomdesire = ChargedMove(name = 'Doom Desire', pwr = 80, tipus = 'Steel')
doomdesire.energyCost = 50
doomdesire.save()

futuresight = ChargedMove(name = 'Future Sight', pwr = 120, tipus = 'Psychic')
futuresight.energyCost = 100
futuresight.save()
"""
# Document: Documento base. Creara su propia collection.
# pasar parametro a una declaracion de clase: HERENCIA. User es hijo de Document
class Team(Document):
    # usamos campos xField que son clases propias de MongoEngine
    num = IntField()
    name = StringField(max_length=50)
    type = ListField(choices=Type)
    catch_date = DateTimeField()
    CP = FloatField()
    HPmax = FloatField()
    HP = FloatField()
    atk = IntField()
    defense = IntField()
    energy = IntField(min_value=0, max_value=100)
    moves = ListField(ReferenceField(Move, reverse_delete_rule=CASCADE))
    candy = StringField(max_length=50)
    candy_count = FloatField()
    current_candy = FloatField()
    weaknesses = ListField(choices=Type)

    #premium = StringField(choices=SINO)

print(1)
"""
pok = pokemons.find_one({"name":"Bulbasaur"})
Bulbasaur = Team(num = pok["num"], name = pok["name"], type = pok["type"], catch_date = datetime.now, HPmax = randrange(200, 1000), atk = randrange(10, 50), defense = randrange(10, 50), energy = 0, candy = pok["candy"], candy_count = pok["candy_count"], current_candy = 0, weaknesses = pok["weaknesses"])
Bulbasaur.CP = (Bulbasaur.HPmax + Bulbasaur.atk + Bulbasaur.defense)
Bulbasaur.HP = Bulbasaur.HPmax
movimientos = [dragontail, steelwing]
Bulbasaur.moves = movimientos
Bulbasaur.save()

pprint("////////////////////////////////")
pprint(Bulbasaur.name)
pprint("////////////////////////////////")
# Embedded Document, documento que va dentro de otro. Objeto JSON que va dentro de un doc
# NO es una coleccio
"""

"""
class Comment(EmbeddedDocument):
    content = StringField()
    name = StringField(max_length=120)


class Post(Document):
    title = StringField(max_length=120, required=True)
    # referencia. Permite referenciar el objectId de otra collection. En Python se accedera como si fuese un objeto de la clase
    author = ReferenceField(User, reverse_delete_rule=CASCADE)
    tags = ListField(StringField(max_length=30))
    comments = ListField(EmbeddedDocumentField(Comment))

    # permite clases heredadas
    meta = {'allow_inheritance': True}


# Clases heredadas: Van dentro de la misma collection que el padre, con un identificador especial
class TextPost(Post):
    content = StringField()


class ImagePost(Post):
    image_path = StringField()


class LinkPost(Post):
    link_url = StringField()


print(1)

# crear INSERT
# recordad que en python los constructores funcionan como en C#. Es decir que puedes definir los campos a mano)
ross = User(email='ross@example.com', first_name='Ross', last_name='Lawley', premium='Si')
ross.save()
marc = User(email='marc@example.com', first_name='Marc', last_name='Albareda', premium='No')
marc.save()

# Fixeu-vos que a author estic posant LA VARIABLE ross de classe User, no una string
post1 = TextPost(title='Fun with MongoEngine', author=ross)
post1.content = 'Took a look at MongoEngine today, looks pretty cool.'
post1.tags = ['mongodb', 'mongoengine']
post1.save()

# update
post1.content = 'otro content'
post1.save()

# Fijaos que noe stoy creando los posts como Post, sino como sus clases heredadas
post2 = LinkPost(title='MongoEngine Documentation', author=ross)
post2.link_url = 'http://docs.mongoengine.com/'
post2.tags = ['mongoengine']
post2.save()

post3 = ImagePost(title='fotos de gatos', author=marc)
post3.image_path = 'http://docs.mongoengine.com/gatete.png'
post3.tags = ['gatetes']
post3.save()

# borrar
post2.delete()

print(2)

print("ejemplo para carlos")
pprint(post3.author.first_name)
# buscar
print("anem a buscar")
# NombreDelaClase.objects es un campo estatico que te deveuvlve una lista con todos los documentos de esa colleciton
for post in Post.objects:
    print(post.title)

# buscar en clase heredada. Automaticamente te cogera los que son TextPost, ignorando los otros
for post in TextPost.objects:
    print(post.content)

print("\n\n\n Busqueda")

# coges todos los posts
for post in Post.objects:
    # imprime el titulo y una barra separadora
    print(post.title)
    print('=' * len(post.title))

    # si (isinstance es como el instanceof) el post es un textpost
    if isinstance(post, TextPost):
        print("texto: ", post.content)

    # si el post es un linkpost
    if isinstance(post, LinkPost):
        print('Link: ', post.link_url)

print("\n\n\n Busqueda con parametros ")

# busqueda con parametros de busqueda
for post in Post.objects(tags='mongodb'):
    print(post.title)

for post in Post.objects(title='fotos de gatos'):
    print(post.author.first_name)

# contar
num_posts = Post.objects(tags='mongodb').count()
print('Found ' + str(num_posts) + ' posts with tag "mongodb"')

# buscar y modificar
usuario = User.objects(first_name='Ross')[0]
usuario.first_name = 'Iker'
usuario.save()

for user in User.objects:
    pprint(user.first_name)


print("---------------------")

#cuantos posts contienen el tag gatete
gatetes = 0
for post in Post.objects():
    print(post.tags)
    if "gatetes" in post.tags:
        print("he encontrado un gatete")
        gatetes+=1
print("cantidad de gatetes "+str(gatetes))
"""

