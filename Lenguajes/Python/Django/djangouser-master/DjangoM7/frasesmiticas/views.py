from django.shortcuts import render

# Create your views here.
from .models import Frase, Tag, User
import datetime
import random

from django.http import HttpResponse

def index(request):
    return HttpResponse("/addFrase <br> /fraseRandom <br>")

def kilos(request):
    return HttpResponse("iker ha hecho mucho ejercicio, well done")

def addFrase(request):
    return render(request, 'frasesmiticas/addFrase.html')


def postAddFrase(request):
    #request.POST["frase"]
    #request.POST["user"]
    m_user = User()
    m_user = User.objects.get(username=request.POST["user"])
    m_user.nFrases = m_user.nFrases + 1
    m_user.save()

    frase = Frase(texto_frase=request.POST["frase"], data=datetime.date.today(),user=m_user)
    frase.save()

    return HttpResponse("Introducida la frase: "+request.POST["frase"]+" del usuario: "+request.POST["user"])

def fraseRandom(request):
    all_entries = Frase.objects.all()
    r_frase = random.choice(all_entries)
    return HttpResponse(r_frase.texto_frase);

def fraseRandomSFW(request):
    all_entries = Frase.objects.filter()

    iker = (list(all_entries))
    random.shuffle(iker)

    chek = True
    for r_frase in iker:
        chek = False
        for tag in r_frase.tags.all():
            if tag.nsfw == True:
                chek = True


        if chek == False:

            return HttpResponse(r_frase.texto_frase);

    return HttpResponse("lo sentimos no hay frases family friendly");





def frasesDe(request, nombre):
    sus = User()
    try:
        sus = User.objects.get(username = nombre)
        try:
            frase = Frase.objects.filter(user=sus)
            return render(request, 'frasesmiticas/frasesDe.html', {'frases': frase, 'usuario': sus})
        except frase.DoesNotExist:
            return HttpResponse("No existen frases.")
    except sus.DoesNotExist:
        return HttpResponse("No existe ese usuario.")

def fraseDe(request, nombre):
    sus = User()
    try:
        sus = User.objects.get(username=nombre)
        try:
            frase = random.choice(Frase.objects.filter(user=sus))
            return HttpResponse("Frase aleatoria de "+ sus.username + ": <br>" + frase.texto_frase+"<br> -"+frase.user.username+ ", "+str(frase.data.year))
        except frase.DoesNotExist:
            return HttpResponse("No existen frases.")
    except sus.DoesNotExist:
        return HttpResponse("No existe ese usuario.")

def addTag(request, id):
    tags = Tag.objects.filter();

    frase = Frase()
    try:
        frase = Frase.objects.get(id=id)  # get porque solo necesito 1
    except frase.DoesNotExist:
        return HttpResponse("No existe esa frase.")

    return render(request, 'frasesmiticas/addTag.html', {'tags': tags, 'frase':frase})


def addTagPost(request, idF):

    tag = Tag()

    try:
        tag = Tag.objects.get(id=request.POST["choice"])
        frase = Frase()
        try:
            frase = Frase.objects.get(id=idF)

            frase.tags.add(tag)


            frase.save()
            return HttpResponse("Tag añadido")
        except frase.DoesNotExist:
            return HttpResponse("error.")
    except tag.DoesNotExist:
        return HttpResponse("error.")



def removeTag(request, idF, nomTag):

    tag = Tag()

    try:
        tag = Tag.objects.get(nombre_tag=nomTag)

        frase = Frase()

        try:
            frase = Frase.objects.get(id=idF)

            frase.tags.remove(tag)
            return HttpResponse("Tag borrado")
        except frase.DoesNotExist:
            return HttpResponse("error.")
    except tag.DoesNotExist:
        return HttpResponse("error.")



