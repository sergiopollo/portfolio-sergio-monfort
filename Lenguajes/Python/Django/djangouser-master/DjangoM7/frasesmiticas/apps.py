from django.apps import AppConfig


class FrasesmiticasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'frasesmiticas'
