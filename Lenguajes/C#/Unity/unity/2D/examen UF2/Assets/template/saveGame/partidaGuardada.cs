using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/partidaGuardada", order = 1)]
public class partidaGuardada : ScriptableObject
{
    public Vector2 posicionKris;
}
