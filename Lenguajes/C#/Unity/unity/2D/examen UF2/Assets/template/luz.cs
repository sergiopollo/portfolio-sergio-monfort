using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class luz : MonoBehaviour
{
    public GameObject luzGlobal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void encenderLuz()
    {
        if (luzGlobal.active == true)
        {
            luzGlobal.SetActive(false);
        }
        else
        {
            luzGlobal.SetActive(true);
        }
    }
}
