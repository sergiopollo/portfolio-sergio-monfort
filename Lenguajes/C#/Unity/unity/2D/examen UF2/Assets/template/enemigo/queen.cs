using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class queen : MonoBehaviour
{
    private Animator m_animator;
    public AudioSource risa;
    public string state = "idle";
    // Start is called before the first frame update
    void Start()
    {
        m_animator = gameObject.GetComponent<Animator>();
        this.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        StartCoroutine(MaquinaEstados());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator MaquinaEstados()
    {
        while (true)
        {
            yield return new WaitForSeconds(10f);

            switch (state)
            {
                case "walk":
                    StartCoroutine(walk());
                    break;
                case "laugh":
                    StartCoroutine(laugh());
                    break;
                case "idle":
                    StartCoroutine(idle());
                    break;
            }
        }
    }

    IEnumerator idle()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        m_animator.Play("idle", -1, 0f);
        yield return new WaitForSeconds(5f);
        state = "walk";
    }

    IEnumerator walk()
    {
        int i = 0;
        while (i < 1)
        {
            Debug.Log(i);
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-5, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            
            m_animator.Play("walking", -1, 0f);

            yield return new WaitForSeconds(3f);

            this.GetComponent<Rigidbody2D>().velocity = new Vector3(5, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, -180, 0);

            yield return new WaitForSeconds(3f);
            i++;
        }

        
        i = 0;
        state = "laugh";
    }

    IEnumerator laugh()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        m_animator.Play("laughing", -1, 0f);
        this.risa.mute = false;
        this.risa.Play();
        yield return new WaitForSeconds(5f);
        this.risa.mute = true;
        state = "idle";
    }

    

}
