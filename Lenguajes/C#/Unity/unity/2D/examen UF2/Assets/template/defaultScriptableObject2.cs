using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/defaultScriptableObject2", order = 1)]
public class defaultScriptableObject2 : ScriptableObject
{
    public int num1;
    public int num2;
    public int num3;

}
