using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Combate_script : MonoBehaviour
{
    public Party_SC party;
    public EPacks_SC[] encuentros;
    private EPacks_SC enemigos;
    public GameObject hudCombate1;
    public GameObject hudCombate2;

    public GameObject textoCombate;
    public GameObject flecha;
    public int lvlmin, lvlMax;

    public List<GameObject> miembrosA;
    public List<UnidadDeCombate_Script> miembrosV;
    public List<GameObject> enemigosA;

    public GameObject empty;

    public List<GameObject> listaTurnos;
    //public GameObject[] turnos;

    private bool seleccionado;
    private int ataqueSeleccionado;
    public int turno=0;
    public bool partyViva = true;
    private int index;
    private bool turnEnd;
    private bool valid;
    private bool Spaced;
    private bool ataqueTerminado;
    private string m_Objeto;
    private string m_pot;

    public GameEvent abrirInventario;
    public GameObject inventariomanager;
    public pocion_SO pkk;
    public pocion_SO pm;
    public pocion_SO pt;
    public escenaprevia sm;
    public GameObject fondo;
    public GameObject luzCampo;
    public GameObject luzCueva;
    public Sprite cueva;

    // Start is called before the first frame update
    void Start()
    {
       
        Inicializar();

        StartCoroutine(espera());
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Prepara las unidades de combate (monoBehaviour) y crea gameobjects para mostrar
    public void Inicializar()
    {
        if (sm.scene == "Cueva")
        {
            fondo.GetComponent<SpriteRenderer>().sprite = cueva;
            luzCampo.SetActive(false);
            luzCueva.SetActive(true);
            lvlmin = 5;
            lvlMax = 10;

        }
        else
        {
            lvlmin = 1;
            lvlMax = 5;
        }



        //1 3 5 10
        int arriba = -2;
        int lado = 0;
        float a = Random.Range(0, 99);
        for (int k = 0; k < encuentros.Length; k++)
        {

            a -= encuentros[k].porcentaje;

            if (a < 0)
            {
                enemigos = encuentros[k];
                break;
            }
        }



        //enemigosA = new GameObject[enemigos.enemigos.Length];
        enemigosA = new List<GameObject>();
        for (int i = 0; i < enemigos.enemigos.Length; i++)
        {

            
            GameObject newg = Instantiate(empty);
            newg.GetComponent<UnidadDeCombate_Script>().u = enemigos.enemigos[i];          
            newg.transform.name = "Enemigo" + i;
            newg.gameObject.transform.position = new Vector3((lado + 1) * 5, arriba, 0);
            newg.SetActive(true);
            newg.GetComponent<SpriteRenderer>().sprite = enemigos.enemigos[i].sprite;
            enemigosA.Add(newg);
            lado++;
            if (i == 2)
            {
                arriba = 2;
                lado = 0;
            }
        }

        arriba = -2;
        lado = 0;
        //miembrosA = new GameObject[party.Members.Length];
        miembrosA = new List<GameObject>();
        miembrosV = new List<UnidadDeCombate_Script>();
        for (int j = 0; j < party.Members.Length; j++)
        {
            if (party.Members[j].hp > 0)
            {
            GameObject newg = Instantiate(empty);
            newg.GetComponent<UnidadDeCombate_Script>().u = party.Members[j];
            newg.transform.name = "Miembro" + j;
            newg.gameObject.transform.position = new Vector3(((lado) * -7)-2, arriba, 0);
            newg.SetActive(true);
            newg.GetComponent<SpriteRenderer>().sprite = party.Members[j].sprite;
            miembrosA.Add(newg);
            miembrosV.Add(newg.GetComponent<UnidadDeCombate_Script>());
            lado++;
            if (j == 2)
            {
                arriba = 2;
                lado = 0;
            }

            }
        }
    }


    IEnumerator espera()
    {
        yield return new WaitForSeconds(0.1f);
        foreach (GameObject g in enemigosA)
        {
            g.GetComponent<UnidadDeCombate_Script>().lvl = Random.Range(lvlmin, lvlMax);
           // Debug.Log(g.GetComponent<UnidadDeCombate_Script>().lvl);
            g.GetComponent<UnidadDeCombate_Script>().StatsXLevel();
        }
        inventariomanager.SetActive(false);
        StartCoroutine(Pelear());
    }

    IEnumerator Pelear()
    {

        foreach (GameObject m in miembrosA)
        {
            m.GetComponentInChildren<TMPro.TextMeshPro>().text = "hp:" + m.GetComponent<UnidadDeCombate_Script>().hp + " M:" + m.GetComponent<UnidadDeCombate_Script>().mana;
        }

        foreach (GameObject e in enemigosA)
        {
            e.GetComponentInChildren<TMPro.TextMeshPro>().text = "hp:" + e.GetComponent<UnidadDeCombate_Script>().hp + " M:" + e.GetComponent<UnidadDeCombate_Script>().mana;
        }

        //poner un evento que cuando todos los miembros de la party mueran ponga partyviva a false
        while (partyViva)
        {
            //primero ordena el orden de los turnos segun la velocidad de cada uno de ellos;
            calcularTurno();

            for (int i = 0; i < listaTurnos.Count; i++)
            {
                if(miembrosA.Count <= 0 ||enemigosA.Count <= 0)
                {
                    partyViva = false;
                }


                if (!partyViva)
                {
                    //POS NO HACE NADA
                }
                else
                {

                

                turnEnd = false;
                StartCoroutine(Turno(listaTurnos[i]));
                yield return new WaitUntil(() => turnEnd);


                for (int j = 0; j < listaTurnos.Count; j++)
                {

                        listaTurnos[j].GetComponentInChildren<TMPro.TextMeshPro>().text = "hp:" + listaTurnos[j].GetComponent<UnidadDeCombate_Script>().hp + " M:" + listaTurnos[j].GetComponent<UnidadDeCombate_Script>().mana;

                    if (listaTurnos[j].GetComponent<UnidadDeCombate_Script>().hp <= 0)
                    {
                        Debug.Log("Muerte");
                        textoCombate.GetComponentInChildren<Text>().text = "Ha muerto: " + listaTurnos[j].GetComponent<UnidadDeCombate_Script>().nombre;
                        listaTurnos[j].gameObject.SetActive(false);
                        if (listaTurnos[j].GetComponent<UnidadDeCombate_Script>().isEnemigo)
                        {
                            enemigosA.Remove(listaTurnos[j]);
                            Debug.Log(enemigosA.Count);
                        }
                        else
                        {
                                for (int k = 0; k < miembrosV.Count; k++)
                                {
                                    if(listaTurnos[j].GetComponent<UnidadDeCombate_Script>().nombre == miembrosV[k].nombre)
                                    {
                                        miembrosV[k].vivo = false;
                                    }
                                }

                            miembrosA.Remove(listaTurnos[j]);
                        }

                        listaTurnos.Remove(listaTurnos[j]);
                        StartCoroutine(PressSpace());
                        Spaced = false;
                        yield return new WaitUntil(() => Spaced);

                        j--;
                        if (j < i)
                        {
                            i--;
                        }

                    }
                }

            }



            }
        }

        Debug.Log("fin");
        StartCoroutine(finCombate());


    }

    public IEnumerator finCombate()
    {
        yield return new WaitForSeconds(1f);

        int r;

        if(miembrosA.Count == 0)
        {
            Debug.Log("Game Over");
            SceneManager.LoadScene("menu");
        }
        else
        {
            float exp = 0;
            for (int i = 0; i < enemigos.enemigos.Length; i++)
            {
                r = Random.Range(0, 99);
                if((r - enemigos.enemigos[i].lootTable[0].ItemDropChance) < 0)
                {
                    //TOCA EL LOOT
                    if(enemigos.enemigos[i].lootTable[0].armas.Length >0)
                    {
                        for(int k = 0; k < enemigos.enemigos[i].lootTable[0].armas.Length; k++)
                        {
                            inventariomanager.GetComponent<InventoryManager>().CollectItem(enemigos.enemigos[i].lootTable[0].armas[k]);
                            textoCombate.GetComponentInChildren<Text>().text = "Has conseguido: " + enemigos.enemigos[i].lootTable[0].armas[k].name;
                            StartCoroutine(PressSpace());
                            Spaced = false;
                            yield return new WaitUntil(() => Spaced);
                        }
                       
                    }

                    if(enemigos.enemigos[i].lootTable[0].armaduras.Length > 0)
                    {
                        for (int l = 0; l < enemigos.enemigos[i].lootTable[0].armaduras.Length; l++)
                        {
                            inventariomanager.GetComponent<InventoryManager>().CollectItem(enemigos.enemigos[i].lootTable[0].armaduras[l]);
                            textoCombate.GetComponentInChildren<Text>().text = "Has conseguido: " + enemigos.enemigos[i].lootTable[0].armaduras[l].name;
                            StartCoroutine(PressSpace());
                            Spaced = false;
                            yield return new WaitUntil(() => Spaced);
                        }
                    }
                }
                party.gold += enemigos.enemigos[i].lootTable[0].oro;
                textoCombate.GetComponentInChildren<Text>().text = "Has conseguido: " + enemigos.enemigos[i].lootTable[0].oro + " oro";
                StartCoroutine(PressSpace());
                Spaced = false;
                yield return new WaitUntil(() => Spaced);
                exp += enemigos.enemigos[i].lootTable[0].experiencia;
                textoCombate.GetComponentInChildren<Text>().text = "Has conseguido: " + enemigos.enemigos[i].lootTable[0].experiencia + " xp";
                StartCoroutine(PressSpace());
                Spaced = false;
                yield return new WaitUntil(() => Spaced);
            }

            exp /= miembrosA.Count;

            for (int i = 0; i < miembrosV.Count; i++)
            {
                
                stats_SC st = (stats_SC)miembrosV[i].GetComponent<UnidadDeCombate_Script>().u;
                if (!miembrosV[i].vivo)
                {
                    st.hp = 0;
                    st.mana = 0;
                }
                else
                {
                    st.getStats(miembrosV[i].GetComponent<UnidadDeCombate_Script>());
                    st.getExp(exp);
                }

            }
        }

        SceneManager.LoadScene(sm.scene);

    }

    public IEnumerator Turno(GameObject unidadEnTurno)
    {
        //ACTUAR
        if (!unidadEnTurno.GetComponent<UnidadDeCombate_Script>().stun)
        {
            ataqueTerminado = false;
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().isEnemigo)
            {
                TurnoEnemigo(unidadEnTurno);

            }
            else
            {

                StartCoroutine(TurnoAliado(unidadEnTurno));
            }
           
            yield return new WaitUntil(() => ataqueTerminado);
            Debug.Log("ha atacado");
            StartCoroutine(PressSpace());
            Spaced = false;
            yield return new WaitUntil(() => Spaced);
            Debug.Log("Todo bien");
            
        }
        else
        {
            textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " esta estuneado y no puede actuar!";
            unidadEnTurno.GetComponent<UnidadDeCombate_Script>().stun = false;
        }

        for (int i = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs.Count - 1; i >= 0; i--)
        {
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs[i].numTurnos < 0)
            {
                //NO SE PROCESA PORQUE VIENE DADO POR LA ARMADURA
            }
            else
            {
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs[i].aplicar(unidadEnTurno.GetComponent<UnidadDeCombate_Script>());
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsTurns[i]--;
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + ", ha recibido " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs[i].estado + ", le quedan " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsTurns[i] + " turnos;";
                StartCoroutine(PressSpace());
                Spaced = false;
                yield return new WaitUntil(() => Spaced);
                if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsTurns[i] == 0)
                {
                    textoCombate.GetComponentInChildren<Text>().text = "Se ha eliminado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs[i].estado;
                    StartCoroutine(PressSpace());
                    Spaced = false;
                    yield return new WaitUntil(() => Spaced);
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsYDebuffs.RemoveAt(i);
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().buffsTurns.RemoveAt(i);
                }
            }
        }

        //BUFF/DEBUFF
        StartCoroutine(finalTurno()); 
    }

    public IEnumerator PressSpace()
    {
        bool end = false;
        while (!end)
        {
            yield return new WaitForSeconds(0.1f);
            //SE PULSA CADA VEZ
            if (Input.GetKey("space"))
            {
                end = true;
            }
        }
        Spaced = true;
    }
    public IEnumerator TurnoAliado(GameObject unidadEnTurno)
    {
        textoCombate.GetComponentInChildren<Text>().text = "Turno de " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre;
        seleccionado = false;
        hudCombate1.gameObject.SetActive(true);
        for (int i = 0; i < 4; i++)
        {
            hudCombate2.transform.GetChild(i).GetComponent<BottonControler>().SetTexto(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[i].name + "/" + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[i].CosteMana); //spell
        }
        valid = true;
        while (valid)
        {
            yield return new WaitUntil(() => seleccionado);
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].CosteMana <= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana)
            {
                valid = false;
            }
            else
            {
                seleccionado = false;
                hudCombate2.SetActive(true);
                textoCombate.GetComponentInChildren<Text>().text = "No tienes suficiente mana";
            }
        }
        seleccionado = false;
        if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].objetivoEnemigo)
        {

            if(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].numObjetivos == 0)
            {

            
            //ATACAR A ENEMIGOS
            //1:Seleccionar Enemigo
            StartCoroutine(Seleccionar(enemigosA));
            yield return new WaitUntil(() => seleccionado);
            unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].atacar(enemigosA[index].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].CosteMana;
                Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " contra: " + enemigosA[index].GetComponent<UnidadDeCombate_Script>().nombre
            );
            textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " contra: " + enemigosA[index].GetComponent<UnidadDeCombate_Script>().nombre;
                //2: Da�ar Enemigo
            }
            else
            {
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].CosteMana;
                for (int i = 0; i < enemigosA.Count; i++)
                {
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].atacar(enemigosA[i].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                }
                Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " contra: Todos" ) ;
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " contra: Todos";

            }
        }
        else
        {
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].numObjetivos == 0)
            {
                //ATACAR ALIADO

                StartCoroutine(Seleccionar(miembrosA));
                yield return new WaitUntil(() => seleccionado);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].CosteMana;
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].atacar(miembrosA[index].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " en: " + miembrosA[index].GetComponent<UnidadDeCombate_Script>().nombre);
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " en: " + miembrosA[index].GetComponent<UnidadDeCombate_Script>().nombre;

            }
            else
            {
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].CosteMana;
                for (int i = 0; i< miembrosA.Count; i++)
                {
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].atacar(miembrosA[i].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                }
                Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " en: Todos");
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataqueSeleccionado].name + " en: Todos";

            }
        }
        ataqueTerminado = true;
    }

    public IEnumerator Seleccionar(List<GameObject> posibles)
    {
        bool aux = true;
        index = 0;
        flecha.SetActive(true);

        while (aux)
        {
            yield return new WaitForSeconds(0.25f);
            if (Input.GetKey("d"))
            {
                index++;
                if(index > posibles.Count - 1)
                {
                    index = 0;
                }
               // Debug.Log(index);
            }else if (Input.GetKey("a"))
            {
                index--;
                if (index < 0)
                {
                    index = posibles.Count - 1;
                }
            }else if (Input.GetKey("e"))
            {
                aux = false;
                seleccionado = true;
                flecha.SetActive(false);
            }
            flecha.gameObject.transform.position = new Vector3(posibles[index].gameObject.transform.position.x, posibles[index].gameObject.transform.position.y + 4, 0);
        }


    }
    public void Huir()
    {
        for (int i = 0; i < miembrosV.Count; i++)
        {

            stats_SC st = (stats_SC)miembrosV[i].GetComponent<UnidadDeCombate_Script>().u;
            if (!miembrosV[i].vivo)
            {
                st.hp = 0;
                st.mana = 0;
            }
            else
            {
                st.getStats(miembrosV[i].GetComponent<UnidadDeCombate_Script>());
            }

        }
        SceneManager.LoadScene(sm.scene);
    }
    public void Hud1(int seleccion)
    {
        switch (seleccion)
        {
            case 1:
                hudCombate2.gameObject.SetActive(true);
                hudCombate1.SetActive(false);
                break;
            case 2:
                Inventario();             
                break;
            case 3:
                Huir();
                break;

        }
        
    }

    public void Inventario()
    {
      
        
        if (!inventariomanager.activeSelf)
        {
            inventariomanager.SetActive(true);
            abrirInventario.Raise();
        }
        else
        {
            abrirInventario.Raise();
            inventariomanager.SetActive(false);
        }
        
        if (hudCombate1.activeSelf)
        {
            hudCombate1.SetActive(false);
        }
        else
        {
            hudCombate1.SetActive(true);
        }
       
    }

    public void objetoUsado()
    {

        StopCoroutine("TurnoAliado");
        abrirInventario.Raise();
        inventariomanager.SetActive(false);
        textoCombate.GetComponentInChildren<Text>().text = "Has usado un objeto";



        ataqueTerminado = true;
        m_Objeto = null;
    }

    public void pocionkk()
    {
        //Debug.Log("B");
        foreach (GameObject go in listaTurnos)
        {
            if(go.GetComponent<UnidadDeCombate_Script>().nombre == m_Objeto)
            {

                if((go.GetComponent<UnidadDeCombate_Script>().hp + pkk.cantidadAModificar) > go.GetComponent<UnidadDeCombate_Script>().hpmax)
                {
                    go.GetComponent<UnidadDeCombate_Script>().hp = go.GetComponent<UnidadDeCombate_Script>().hpmax;
                }
                else
                {
                    go.GetComponent<UnidadDeCombate_Script>().hp += pkk.cantidadAModificar;
                }
                
            }
        }

    }

    public void pociontocha()
    {
       // Debug.Log("B");
        foreach (GameObject go in listaTurnos)
        {
            if (go.GetComponent<UnidadDeCombate_Script>().nombre == m_Objeto)
            {

                if ((go.GetComponent<UnidadDeCombate_Script>().hp + pt.cantidadAModificar) > go.GetComponent<UnidadDeCombate_Script>().hpmax)
                {
                    go.GetComponent<UnidadDeCombate_Script>().hp = go.GetComponent<UnidadDeCombate_Script>().hpmax;
                }
                else
                {
                    go.GetComponent<UnidadDeCombate_Script>().hp += pt.cantidadAModificar;
                }

            }
        }
    }

    public void pocionmana()
    {
        //Debug.Log("B");
        foreach (GameObject go in listaTurnos)
        {
            if (go.GetComponent<UnidadDeCombate_Script>().nombre == m_Objeto)
            {

                if ((go.GetComponent<UnidadDeCombate_Script>().mana + pm.cantidadAModificar) > go.GetComponent<UnidadDeCombate_Script>().manamax)
                {
                    go.GetComponent<UnidadDeCombate_Script>().mana = go.GetComponent<UnidadDeCombate_Script>().manamax;
                }
                else
                {
                    go.GetComponent<UnidadDeCombate_Script>().mana += pm.cantidadAModificar;
                }

            }
        }
    }

    public void arquero()
    {
        m_Objeto = "arquero";
        //Debug.Log("A");
    }
    public void knight()
    {
        m_Objeto = "knight";
       // Debug.Log("A");
    }
    public void witch()
    {
        m_Objeto = "witch";
       // Debug.Log("A");
    }
    public void AtaqueSeleccionar(int ataque)
    {

        

        switch (ataque)
        {
            case 1:
                ataqueSeleccionado = 0;
                break;
            case 2:
                ataqueSeleccionado = 1;
                break;
            case 3:
                ataqueSeleccionado = 2;
                break;
            case 4:
                ataqueSeleccionado = 3;
                break;
            case 5:
                hudCombate2.gameObject.SetActive(false);
                hudCombate1.gameObject.SetActive(true);
                break;
        }
        if(ataque != 5)
        {
            seleccionado = true;
            hudCombate2.SetActive(false);
        }


    }
    public void TurnoEnemigo(GameObject unidadEnTurno)
    {
        valid = true;
        int ataque = 0;
        //ACTUAR PARA ENEMIGO
        //1: Seleccionar Ataque (Asegurarse de que puede seleccionar todos los ataques)
        while (valid)
        {
           ataque = Random.Range(0, unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades.Length);
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].CosteMana <= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana)
            {
                valid = false;
            }
        }
        

        if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].objetivoEnemigo)
        {
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].numObjetivos == 0)
            {
                //2: Seleccionar Objetivo(Asegurarse de que puede seleccionar todos los objetivos)
                int objetivo = Random.Range(0, miembrosA.Count);
                //3: Ejecutar Ataque

                //Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque]);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].atacar(miembrosA[objetivo].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].CosteMana;
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].name + " contra: " + miembrosA[objetivo].GetComponent<UnidadDeCombate_Script>().nombre;
            }
            else
            {
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].CosteMana;
                for (int i = 0; i < miembrosA.Count; i++)
                {
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].atacar(miembrosA[i].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                }
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].name + " contra: Todos";
            }


            
        }
        else
        {
            if (unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].numObjetivos == 0)
            {
                //2: Seleccionar Objetivo(Asegurarse de que puede seleccionar todos los objetivos)
                int objetivo = Random.Range(0, enemigosA.Count);
                //3: Ejecutar Ataque

                //Debug.Log(unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque]);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].atacar(enemigosA[objetivo].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].CosteMana;
                textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].name + " en: " + enemigosA[objetivo].GetComponent<UnidadDeCombate_Script>().nombre;
            }
            else
            {
                unidadEnTurno.GetComponent<UnidadDeCombate_Script>().mana -= unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].CosteMana;
                for (int i = 0; i < enemigosA.Count; i++)
                {
                    unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].atacar(enemigosA[i].GetComponent<UnidadDeCombate_Script>(), unidadEnTurno.GetComponent<UnidadDeCombate_Script>().atkBase);
                    textoCombate.GetComponentInChildren<Text>().text = unidadEnTurno.GetComponent<UnidadDeCombate_Script>().nombre + " ha usado: " + unidadEnTurno.GetComponent<UnidadDeCombate_Script>().habilidades[ataque].name + " en: Todos sus aliados";
                }
            }
            
        }
        ataqueTerminado = true;
        //Debug.Log(ataqueTerminado);
    }
    public IEnumerator finalTurno()
    {
        bool end = false;
        while (!end)
        {
            yield return new WaitForSeconds(0.25f);
            if (Input.GetKey("space"))
            {
                end = true;
            }
        }
        turnEnd = true;
    }
    public void calcularTurno()
    {

        this.listaTurnos = new List<GameObject>();

        for (int i = 0; i < miembrosA.Count; i++)
        {
            if (miembrosA[i].GetComponent<UnidadDeCombate_Script>().hp > 0)
            {
                listaTurnos.Add(miembrosA[i]);
            }
        }

        for (int i = 0; i<enemigosA.Count; i++)
        {
            if (enemigosA[i].GetComponent<UnidadDeCombate_Script>().hp > 0)
            {
                listaTurnos.Add(enemigosA[i]);
            }
        }

        listaTurnos.Sort(
            delegate (GameObject a, GameObject b) {
            return (a.GetComponent<UnidadDeCombate_Script>().velBase).CompareTo(b.GetComponent<UnidadDeCombate_Script>().velBase);                                               }
            );
        listaTurnos.Reverse();

        //for (int i = 0; i < listaTurnos.Count; i++)
        //{
        //    Debug.Log("Unidad: " + listaTurnos[i].name + " Velocidad: " + listaTurnos[i].GetComponent<UnidadDeCombate_Script>().velBase);
        //}

    }
    
}
