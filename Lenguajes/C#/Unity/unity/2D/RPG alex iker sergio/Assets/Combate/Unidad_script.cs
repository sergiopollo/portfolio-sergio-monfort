using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad_script : ScriptableObject
{
    public string nombre;
    public Sprite sprite;
    public habilidad_SC[] habilidades;
    public float lvl;
    public float hpmax;
    public float hp;
    public float mana;
    public float manamax;
    public float atkBase;
    public float defBase;
    public float velBase;
    public List<BuffODebuff_SC> buffsYDebuffs;
    public float dodgeBase;


    public void getStats(UnidadDeCombate_Script uc)
    {
        this.hp = uc.hp;
        this.mana = uc.mana;
    }
}
