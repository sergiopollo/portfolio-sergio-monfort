using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnidadDeCombate_Script : MonoBehaviour 
{

    public Unidad_script u;

    public bool isEnemigo;

    public string nombre;
    public Sprite sprite;
    public habilidad_SC[] habilidades;
    public float lvl;
    public float hpmax;
    public float hp;
    public float mana;
    public float manamax;
    public float atkBase;
    public float defBase;
    public float velBase;
    public bool stun;
    public float xpboost;
    public bool vivo;

    public List<BuffODebuff_SC> buffsYDebuffs;
    public List<int> buffsTurns;
    public float dodgeBase;

    //exclusivas de jugador
    public ArmaScriptableObject weapon;
    public ArmaduraScriptableObject armor;
    public float exp;
    public float expMax;

    //exclusivas de enemigo
    public ObjetosLooteables[] lootTable;


    // Start is called before the first frame update
    void Start()
    {
        vivo = true;
        stun = false;
        nombre = u.nombre;
        sprite = u.sprite;
        habilidades = u.habilidades;
        lvl = u.lvl;
        hpmax = u.hpmax;
        hp = u.hp;
        mana = u.mana;
        manamax = u.manamax;
        atkBase = u.atkBase;
        defBase = u.defBase;
        velBase = u.velBase;
        buffsYDebuffs = new List<BuffODebuff_SC>();
        buffsYDebuffs.AddRange(u.buffsYDebuffs);
        buffsTurns = new List<int>();
        for(int i= 0; i< buffsYDebuffs.Count-1; i++)
        {
            buffsTurns.Add(buffsYDebuffs[i].numTurnos);
        }
        dodgeBase = u.dodgeBase;


        if (u is stats_SC)
        {
            this.xpboost = 0;
            this.isEnemigo = false;
            stats_SC uJugador = (stats_SC)u;
            this.weapon = uJugador.weapon;
            this.armor = uJugador.armor;
            this.exp = uJugador.exp;
            this.expMax = uJugador.expMax;
        }
        else if (u is SO_Enemigo)
        {
            this.isEnemigo = true;
            SO_Enemigo uEnemigo = (SO_Enemigo)u;
            this.lootTable = uEnemigo.lootTable;
        }

    }

    public void StatsXLevel()
    {
        //Debug.Log(this.hpmax);
        this.hpmax += 10 * lvl;
       // Debug.Log(this.hpmax);
        this.hp += 10 * lvl;
        this.manamax += 20 * lvl;
        this.mana += 20 * lvl;
        this.atkBase += 5 * lvl;
        this.defBase += 2 * lvl;
        this.velBase += 5 * lvl;
        this.dodgeBase += 0.01f * lvl;
        //Debug.Log("Cambiado estadisticas");
            
    }
    // Update is called once per frame
    void Update()
    {
        
    }


}
