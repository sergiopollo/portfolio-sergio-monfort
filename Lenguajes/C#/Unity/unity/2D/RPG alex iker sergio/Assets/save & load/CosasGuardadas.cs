using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PartidaGuardada", order = 1)]
public class CosasGuardadas : ScriptableObject
{
    public Party_SC party;
    public string nombreEscenaActual;
    public Item_ScriptableObject[] itemsPartida;
}
