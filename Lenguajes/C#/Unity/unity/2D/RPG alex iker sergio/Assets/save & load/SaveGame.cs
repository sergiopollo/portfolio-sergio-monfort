using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using OdinSerializer;

public class SaveGame : MonoBehaviour
{
    public Party_SC party;
    public CosasGuardadas partidaGuardada;
    public CosasGuardadas partidaNueva;
    public Item_ScriptableObject[] itemsPartida;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown("n"))
        {
            guardarPartida();
        }
        */
        /*
        if (Input.GetKeyDown("m"))
        {
            //cargar partida guardada
            cargarPartida(false);
        }
        if (Input.GetKeyDown(","))
        {
            //cargar partida nueva
            cargarPartida(true);
        }
        */
    }

    public void guardarPartida()
    {
        Debug.Log("AAAAA");
        partidaGuardada.nombreEscenaActual = SceneManager.GetActiveScene().name;
        partidaGuardada.party = party;
        partidaGuardada.itemsPartida = itemsPartida;
        //json viejo
        /*
        string jsonStr = JsonUtility.ToJson(partidaGuardada);
        string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonStr));
        File.WriteAllText("Guardado.json", jsonStr);
        */
        //odin

        byte[] serializedData = SerializationUtility.SerializeValue<CosasGuardadas>(partidaGuardada, DataFormat.JSON);
        File.WriteAllBytes("Guardado.json", serializedData);

        print("partida guardada con exito");
    }

    public void cargarPartida(bool nueva)
    {
        if (nueva)
        {
            Debug.Log("Estoy haciendo una nueva partida");
            party.gold = partidaNueva.party.gold;
            party.listaItems = partidaNueva.party.listaItems;
            //igualar cada uno de los stats
            for (int i = 0; i < partidaNueva.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaNueva.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
            }
            itemsPartida = partidaNueva.itemsPartida;
            for (int i = 0; i < partidaNueva.itemsPartida.Length; i++)
            {
                //Debug.Log("Estoy modificando items: " + i);
                //empiecas con 3 pociones kk 
                if (itemsPartida[i].name.Equals("lesser Healing potion"))
                {
                    itemsPartida[i].quantity = 3;
                }
                else
                {
                    itemsPartida[i].quantity = 0;
                }
            }
            SceneManager.LoadScene(partidaNueva.nombreEscenaActual);
        }
        else
        {
            Debug.Log("Estoy cargando partida");
            //json viejo
            /*
            string jsonStr = File.ReadAllText("Guardado.json");
            JsonUtility.FromJsonOverwrite(jsonStr, partidaGuardada);
            
            for (int i = 0; i < partidaGuardada.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaGuardada.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
            }
            //party = partidaGuardada.party;
            itemsPartida = partidaGuardada.itemsPartida;
            SceneManager.LoadScene(partidaGuardada.nombreEscenaActual);
            */
            //odin
            byte[] postFile = File.ReadAllBytes("Guardado.json");

            partidaGuardada = SerializationUtility.DeserializeValue<CosasGuardadas>(postFile, DataFormat.JSON);


            party.gold = partidaGuardada.party.gold;
            //no funciona: la lista de items se queda vacia aunque en el guardado se ven los objetos 
            //party.listaItems = partidaGuardada.party.listaItems;
            /*
            for (int i = 0; i < partidaGuardada.party.listaItems.Count; i++)
            {
                party.listaItems[i] = partidaGuardada.party.listaItems[i];
            }
            */

            for (int i = 0; i < partidaGuardada.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaGuardada.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
                //por alguna razon al cargar a los members todo lo que no es un int o un string lo vacia asi que le paso los sprites y habilidades del personaje base (arma y armadura equipada se pierde :c)
                party.Members[i].sprite = partidaNueva.party.Members[i].sprite;
                party.Members[i].habilidades = partidaNueva.party.Members[i].habilidades;
                
            }

            //party = partidaGuardada.party;
            itemsPartida = partidaGuardada.itemsPartida;

            //le a�ado los items a la lista de la party desde aqui por que por alguna razon no se meten haciendo party.listaItems = partidaGuardada.party.listaItems;
            /*
            int contador = 0;
            for (int i = 0; i < itemsPartida.Length; i++)
            {
                if (itemsPartida[i].quantity > 0 && contador < 18)
                {
                    party.listaItems[contador] = itemsPartida[i];
                    contador++;
                }
            }
            */

            SceneManager.LoadScene(partidaGuardada.nombreEscenaActual);
        }

        print("He cargado de puta madre");
    }

    public void goBackToMenu()
    {
        SceneManager.LoadScene("menu");
    }

}

