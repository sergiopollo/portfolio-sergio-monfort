using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ObjetosLooteables", order = 1)]
public class ObjetosLooteables : ScriptableObject
{
    // Start is called before the first frame update

    public int oro;
    public int experiencia;
    public float ItemDropChance;
    public ArmaduraScriptableObject[] armaduras;
    public ArmaScriptableObject[] armas;
}
