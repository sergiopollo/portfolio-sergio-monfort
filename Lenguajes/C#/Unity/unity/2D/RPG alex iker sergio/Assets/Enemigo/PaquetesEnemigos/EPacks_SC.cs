using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GrupoEnemigos", order = 1)]
public class EPacks_SC : ScriptableObject
{

    public SO_Enemigo[] enemigos;
    public float porcentaje;
}
