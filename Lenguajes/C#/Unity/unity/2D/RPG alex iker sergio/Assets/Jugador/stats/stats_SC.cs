using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/stats Jugador", order = 1)]
public class stats_SC : Unidad_script
{
 
    public ArmaScriptableObject weapon;
    public ArmaduraScriptableObject armor;  
    public float exp;
    public float expMax;


    public void getHealed(float vida)
    {
        float vidaActual = this.hp;

        if (vidaActual + vida > this.hpmax)
        {
            this.hp = this.hpmax;
        }
        else
        {
            this.hp += vida;
        }
    }

    public void getMana(float mana)
    {
        float manaActual = this.mana;

        if (manaActual + mana > this.manamax)
        {
            this.mana = this.manamax;
        }
        else
        {
            this.mana += mana;
        }
    }

    public void equipWeapon(ArmaScriptableObject arma, Party_SC party)
    {
        //si ya tiene un arma la desequipa y la envia al inventario
        if (this.weapon == null)
        {
            this.weapon = arma;

            //aplico los buffs o debuffs de este arma
            for (int i = 0; i < arma.buffs2.Length-1; i++)
            {
                switch (arma.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax += arma.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax += arma.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase += arma.buffs2[i].modificacion;
                        break;
                }
            }
        }
        else
        {
            //desequipo el arma actual, le quito los buffs o debuffs al personaje y devuelvo el arma al inventario

            ArmaScriptableObject armaVieja = this.weapon;

            //quito los buffs del arma a quitar
            for (int i = 0; i < armaVieja.buffs2.Length - 1; i++)
            {
                switch (armaVieja.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase -= armaVieja.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase -= armaVieja.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase -= armaVieja.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax -= armaVieja.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax -= armaVieja.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase -= armaVieja.buffs2[i].modificacion;
                        break;
                }
            }

            //party.listaItems.Add(armaVieja);
            //hay que hacer un collectItem en inventory manager

            this.weapon = arma;

            //aplico los buffs o debuffs de este arma
            for (int i = 0; i < arma.buffs2.Length - 1; i++)
            {
                switch (arma.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase += arma.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax += arma.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax += arma.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase += arma.buffs2[i].modificacion;
                        break;
                }
            }
        }
    }

    public void equipArmor(ArmaduraScriptableObject armor, Party_SC party)
    {
        //si ya tiene un arma la desequipa y la envia al inventario
        if (this.armor == null)
        {
            this.armor = armor;

            //aplico los buffs o debuffs de este arma
            for (int i = 0; i < armor.buffs2.Length - 1; i++)
            {
                switch (armor.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax += armor.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax += armor.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase += armor.buffs2[i].modificacion;
                        break;
                }
            }
        }
        else
        {
            //desequipo el arma actual, le quito los buffs o debuffs al personaje y devuelvo el arma al inventario

            ArmaduraScriptableObject armorVieja = this.armor;

            //quito los buffs del arma a quitar
            for (int i = 0; i < armorVieja.buffs2.Length - 1; i++)
            {
                switch (armorVieja.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase -= armorVieja.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase -= armorVieja.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase -= armorVieja.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax -= armorVieja.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax -= armorVieja.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase -= armorVieja.buffs2[i].modificacion;
                        break;
                }
            }

            //party.listaItems.Add(armorVieja);

            this.armor = armor;

            //aplico los buffs o debuffs de este arma
            for (int i = 0; i < armor.buffs2.Length - 1; i++)
            {
                switch (armor.buffs2[i].estado)
                {
                    case Estado.AtkMod:
                        this.atkBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.DefMod:
                        this.defBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.DodgeMod:
                        this.dodgeBase += armor.buffs2[i].modificacion;
                        break;
                    case Estado.HpMod:
                        this.hpmax += armor.buffs2[i].modificacion;
                        break;
                    case Estado.ManaMod:
                        this.manamax += armor.buffs2[i].modificacion;
                        break;
                    case Estado.VelMod:
                        this.velBase += armor.buffs2[i].modificacion;
                        break;
                }
            }
        }
    }

    public void getExp(float exp)
    {
        float expActual = this.exp;

        if (expActual + exp >= this.expMax)
        {
            float leftoverExp = this.expMax - (expActual + exp);
            if (leftoverExp < 0)
            {
                leftoverExp = 0;
            }
            lvlUp(leftoverExp);
        }
        else
        {
            this.exp += exp;
        }
    }

    private void lvlUp(float extraExp)
    {
        this.lvl += 1;
        this.expMax += 100;
        this.exp = extraExp;
        this.hpmax += 10;
        this.manamax += 10;
        this.atkBase += 1;
        this.defBase += 1;
        Debug.Log(this.name+" Level Up! new level: "+this.lvl);


        float leftoverExp = this.exp - this.expMax;
        if (leftoverExp > 0)
        {
            lvlUp(leftoverExp);
        }
    }

}
