using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorOverworld_script : MonoBehaviour
{
    //Overworld
    public float velOverworld = 2;
    public bool inventarioAbierto = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!inventarioAbierto)
        {

            //esto ya hace el movimiento en el wasd y las flechas
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Input.GetAxisRaw("Horizontal") * velOverworld, Input.GetAxisRaw("Vertical") * velOverworld);
        }
    }

    public void stopMoving()
    {
        if (inventarioAbierto)
        {
            this.inventarioAbierto = false;
        }
        else
        {
            this.inventarioAbierto = true;

        }
    }
}
