using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Shop_Manager : MonoBehaviour
{
    public GameObject inventario;
    public GameObject inventarioTienda;
    private enum state {NADA, VENDER, COMPRAR};

    private state estado = state.NADA;

    public Party_SC party;
    public TextMeshProUGUI gold;

    public Image cursor;
    private Vector2 cursorPosInicial;
    public Image cursorVender;
    private Vector2 cursorVenderPosInicial;
    public Image cursorComprar;
    private Vector2 cursorComprarPosInicial;
    
    public TextMeshProUGUI[] text = new TextMeshProUGUI[18];
    public List<Item_ScriptableObject> llista;

    public TextMeshProUGUI[] textTienda = new TextMeshProUGUI[18];
    public List<Item_ScriptableObject> llistaTienda;
    //public HealingItemUsedEvent healingItemEvent;
    int indexVender = 0;
    int indexComprar = 0;
    int indexNada = 0;



    // Start is called before the first frame update
    void Start()
    {
        cursorPosInicial = cursor.transform.position;
        cursorVenderPosInicial = cursorVender.transform.position;
        cursorComprarPosInicial = cursorComprar.transform.position;

        //actualizar lista items
        llista = party.listaItems;

        actualizarUI();

    }

    public void actualizarUI()
    {
        //actualizar items party (no es de la ui xd)
        party.listaItems = llista;

        //actualizar oro
        gold.text = "Gold: " + party.gold;

        //actualizar los items del inventario
        for (int i = 0; i < llista.Count; i++)
        {
            if (llista[i] == null)
            {
                text[i].text = "Empty";
            }
            else if (llista[i].quantity <= 0)
            {
                llista[i] = null;
                text[i].text = "Empty";
            }
            else
            {
                int dineroRecibido = llista[i].goldCost - ((llista[i].goldCost) * 20 / 100);
                text[i].text = llista[i].name + ": " + llista[i].quantity+" Value $: " + dineroRecibido;
            }
        }

        //actualizar los items del inventario de la tienda
        for (int i = 0; i < llistaTienda.Count; i++)
        {
            if (llistaTienda[i] == null)
            {
                textTienda[i].text = "Empty";
            }
            else
            {
                textTienda[i].text = llistaTienda[i].name + " Cost $: "+ llistaTienda[i].goldCost;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

        //pruebas
        if (Input.GetKeyDown("2"))
        {
            Debug.Log(estado + " " + indexNada + " " + indexComprar + " " + indexVender);
        }
        if (Input.GetKeyDown("s"))
        {
            if (indexVender != 8 && indexVender != 17 && estado == state.VENDER)
            {
                indexVender++;
                cursorVender.rectTransform.anchoredPosition = new Vector2(cursorVender.rectTransform.anchoredPosition.x, cursorVender.rectTransform.anchoredPosition.y - 50);
            }else if (estado == state.COMPRAR && indexComprar != 5)
            {
                indexComprar++;
                cursorComprar.rectTransform.anchoredPosition = new Vector2(cursorComprar.rectTransform.anchoredPosition.x, cursorComprar.rectTransform.anchoredPosition.y - 50);

            }
        }
        else if (Input.GetKeyDown("w"))
        {
            if (indexVender != 0 && indexVender != 9 && estado == state.VENDER)
            {
                indexVender--;
                cursorVender.rectTransform.anchoredPosition = new Vector3(cursorVender.rectTransform.anchoredPosition.x, cursorVender.rectTransform.anchoredPosition.y + 50);
            }
            else if (estado == state.COMPRAR && indexComprar != 0)
            {
                indexComprar--;
                cursorComprar.rectTransform.anchoredPosition = new Vector2(cursorComprar.rectTransform.anchoredPosition.x, cursorComprar.rectTransform.anchoredPosition.y + 50);

            }
        }
        else if (Input.GetKeyDown("d"))
        {
            if (indexVender < 9 && estado == state.VENDER)
            {
                indexVender += 9;
                cursorVender.rectTransform.anchoredPosition = new Vector3(cursorVender.rectTransform.anchoredPosition.x + 450, cursorVender.rectTransform.anchoredPosition.y);
            }
            else if (estado == state.NADA && indexNada != 2)
            {
                indexNada ++;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x + 700, cursor.rectTransform.anchoredPosition.y);
            }

        }
        else if (Input.GetKeyDown("a"))
        {
            if (indexVender > 8 && estado == state.VENDER)
            {
                indexVender -= 9;
                cursorVender.rectTransform.anchoredPosition = new Vector3(cursorVender.rectTransform.anchoredPosition.x - 450, cursorVender.rectTransform.anchoredPosition.y);
            }
            else if (estado == state.NADA && indexNada != 0)
            {
                indexNada--;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x - 700, cursor.rectTransform.anchoredPosition.y);
            }

        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            if (estado == state.NADA)
            {
                if (indexNada == 0)
                {
                    estado = state.COMPRAR;
                    desaparecerTienda();
                }
                else if (indexNada == 1)
                {
                    estado = state.VENDER;
                    desaparecerInventario();

                }
                else if (indexNada == 2)
                {
                    actualizarUI();
                    SceneManager.LoadScene("Overworld");
                }
            }else if (estado == state.COMPRAR)
            {
                if (llistaTienda[indexComprar] == null)
                {
                    Debug.Log("item out of stock");
                }
                else if (llistaTienda[indexComprar].goldCost <= party.gold)
                {
                    party.gold -= llistaTienda[indexComprar].goldCost;
                    CollectItem(llistaTienda[indexComprar]);
                }
                else
                {
                    Debug.Log("sorry link, i cant give credit come back when you're a little mmmmm richer!");
                }


            }else if (estado == state.VENDER)
            {
                if (llista[indexVender] != null && (llista[indexVender]).quantity > 0)
                {
                    //para que vender te de menos dinero que comprar
                    int dineroRecibido = llista[indexVender].goldCost - ((llista[indexVender].goldCost) * 20 / 100);
                    party.gold += dineroRecibido;
                    (llista[indexVender]).quantity--;
                    actualizarUI();

                }
            }
        }
        else if(Input.GetKeyDown("q"))
        {
            if (estado == state.COMPRAR)
            {
                desaparecerTienda();
                estado = state.NADA;
            }
            else if (estado == state.VENDER)
            {
                desaparecerInventario();
                estado = state.NADA;
            }

        }
    }

    public void CollectItem(Item_ScriptableObject item)
    {
        //primero comprueba si ya tienes de este item
        bool haveItem = false;
        for (int i = 0; i < llista.Count; i++)
        {
            if (!haveItem && llista[i] == item)
            {
                haveItem = true;
                item.quantity++;
            }
        }

        //si no tienes de este item que compruebe si hay un slot vacio para poder recogerlo
        if (!haveItem)
        {
            bool collected = false;
            for (int i = 0; i < llista.Count; i++)
            {
                if (!collected && llista[i] == null)
                {
                    collected = true;
                    item.quantity++;
                    llista[i] = item;
                }
            }

            if (!collected)
            {
                Debug.Log("inventario lleno! no se ha podido recoger el objeto");
            }
        }


        actualizarUI();
    }

   

    public void desaparecerInventario()
    {
        if (inventario.activeSelf)
        {
            cursor.transform.position = cursorPosInicial;
            cursorVender.transform.position = cursorVenderPosInicial;
            cursorComprar.transform.position = cursorComprarPosInicial;
            indexVender = 0;
            indexComprar = 0;
            indexNada = 0;
            inventario.SetActive(false);
        }
        else
        {
            inventario.SetActive(true);
            actualizarUI();
        }
    }

    public void desaparecerTienda()
    {
        if (inventarioTienda.activeSelf)
        {
            cursor.transform.position = cursorPosInicial;
            cursorVender.transform.position = cursorVenderPosInicial;
            cursorComprar.transform.position = cursorComprarPosInicial;
            indexVender = 0;
            indexComprar = 0;
            indexNada = 0;
            inventarioTienda.SetActive(false);
        }
        else
        {
            inventarioTienda.SetActive(true);
            actualizarUI();
        }
    }

}
