using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ArmaduraScriptableObject", order = 1)]

public class ArmaduraScriptableObject : Item_ScriptableObject
{
    // Start is called before the first frame update
    public float vida;
    public float defFinal;
    public float velFinal;
    public float dodgeFinal;

    public string[] buffs;
    public BuffODebuff_SC[] buffs2;

    public override void useItem(stats_SC partyMember, Party_SC party)
    {
       Debug.Log("he equipado la armadura " + name + " en " + partyMember.name);
        partyMember.equipArmor(this, party);
    }
}
