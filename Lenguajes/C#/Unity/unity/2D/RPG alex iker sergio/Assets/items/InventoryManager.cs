using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public GameObject inventario;
    public GameObject invSeleccionPlayers;
    public bool selectingPlayer = false;
    public Party_SC party;
    public TextMeshProUGUI gold;

    public Image[] playerImgs;
    public TextMeshProUGUI[] playerTexts;

    public Image cursor;
    private Vector2 cursorPosInicial;
    public Image cursor2;
    private Vector2 cursor2PosInicial;
    public TextMeshProUGUI[] text = new TextMeshProUGUI[18];
    public List<Item_ScriptableObject> llista;
    //public HealingItemUsedEvent healingItemEvent;
    int index = 0;
    int indexPlayers = 0;

    //pruebas
    public Item_ScriptableObject itemCheat;
    public Item_ScriptableObject itemCheat2;

    //evento para el combate
    public GameEvent objetoUsadoCombate;
    public GameEvent pocionkk;
    public GameEvent pociontocha;
    public GameEvent pocionmana;
    public GameEvent arquero;
    public GameEvent knight;
    public GameEvent witch;

    // Start is called before the first frame update
    void Start()
    {
        cursorPosInicial = cursor.transform.position;
        cursor2PosInicial = cursor2.transform.position;

        //actualizar lista items
        llista = party.listaItems;

        actualizarUI();


        //poner seleccion de players segun los que haya en tu party
        //esta hecho pa que el maximo sea 3, si pones mas de 3 petara aqui
        for (int i = 0; i<party.Members.Length; i++)
        {
            playerImgs[i].sprite = party.Members[i].sprite;
            playerTexts[i].text = party.Members[i].name;
            
        }
    }

    public void actualizarUI()
    {
        //actualizar items party
        party.listaItems = llista;

        //actualizar oro
        gold.text = "Gold: "+party.gold;

        //actualizar los items del inventario
        for (int i = 0; i < llista.Count; i++)
        {
            if (llista[i] == null)
            {
                text[i].text = "Empty";
            }else if (llista[i].quantity <= 0)
            {
                llista[i] = null;
                text[i].text = "Empty";
            }
            else
            {
                text[i].text = llista[i].name + " : " + llista[i].quantity;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

        //prueba
       // if (Input.GetKeyDown("1"))
       // {
        //    CollectItem(itemCheat2);
       // }


        if (Input.GetKeyDown("q"))
        {
            if (selectingPlayer)
            {
                invSeleccionPlayers.SetActive(false);
                selectingPlayer = false;
            }
            else {
                //prueba
              //  CollectItem(itemCheat);
            }
        }

        if (Input.GetKeyDown("s"))
        {
            if (index != 8 && index != 17 && !selectingPlayer)
            {
                index++;
                cursor.rectTransform.anchoredPosition = new Vector2(cursor.rectTransform.anchoredPosition.x, cursor.rectTransform.anchoredPosition.y - 50);
            }
        }
        else if (Input.GetKeyDown("w"))
        {
            if (index != 0 && index != 9 && !selectingPlayer)
            {
                index--;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x, cursor.rectTransform.anchoredPosition.y + 50);
            }
        }
        else if (Input.GetKeyDown("d"))
        {
            if (index < 9 && !selectingPlayer)
            {
                index += 9;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x + 450, cursor.rectTransform.anchoredPosition.y);
            }
            else if (indexPlayers != party.Members.Length-1)
            {
                indexPlayers++;
                cursor2.rectTransform.anchoredPosition = new Vector3(cursor2.rectTransform.anchoredPosition.x + 225, cursor2.rectTransform.anchoredPosition.y);
            }
            
        }
        else if (Input.GetKeyDown("a"))
        {
            if (index > 8 && !selectingPlayer)
            {
                index -= 9;
                cursor.rectTransform.anchoredPosition = new Vector3(cursor.rectTransform.anchoredPosition.x - 450, cursor.rectTransform.anchoredPosition.y);
            }
            else if (indexPlayers != 0)
            {
                indexPlayers--;
                cursor2.rectTransform.anchoredPosition = new Vector3(cursor2.rectTransform.anchoredPosition.x - 225, cursor2.rectTransform.anchoredPosition.y);
            }
            
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {   
            if (llista[index] != null && (llista[index]).quantity > 0)
            {
                if (selectingPlayer)
                {
                    (llista[index]).quantity--;
                    //aqui un evento useItem comun que tendran todos los items y dependiendo de cual sea ps que herede de la funcion de respuesta del evento y que haga lo que sea
                    if (SceneManager.GetActiveScene().name == "CombateCampo")
                    {
                     
                        if (llista[index] is pocion_SO)
                        {
                            

                            stats_SC per = (stats_SC)party.Members[indexPlayers];
                           
                            switch (per.nombre)
                            {

                                case "arquero":
                                    arquero.Raise();
                                    break;
                                case "knight":
                                    knight.Raise();
                                    break;
                                case "witch":
                                    witch.Raise();
                                    break;
                            }

                            pocion_SO pot = (pocion_SO)llista[index];
                            
                            switch (pot.name)
                            {

                                case "lesser Healing potion":
                                    pocionkk.Raise();
                                    break;
                                case "mana potion":
                                    pociontocha.Raise();
                                    break;
                                case "big healing potion":
                                    pocionmana.Raise();
                                    break;
                            }
                        }

                        
                        //para combate
                        objetoUsadoCombate.Raise();
                    }
                    //si es equipamiento necesito que se procese normal, pero una pocion como afecta sobre el so me da igual porque al final del combate lo cambio igual
                    llista[index].useItem(party.Members[indexPlayers], party);
                  
                    actualizarUI();
                    invSeleccionPlayers.SetActive(false);
                    selectingPlayer = false;
                }
                else
                {
                    //seleccion de personaje sobre el que quieres usar el item
                    selectingPlayer = true;
                    invSeleccionPlayers.SetActive(true);
                }
            }

        }

    }

    public void CollectItem(Item_ScriptableObject item)
    {
        //primero comprueba si ya tienes de este item
        bool haveItem = false;
        for (int i = 0; i < llista.Count; i++)
        {
            if (!haveItem && llista[i] == item)
            {
                haveItem = true;
                item.quantity++;
            }
        }

        //si no tienes de este item que compruebe si hay un slot vacio para poder recogerlo
        if (!haveItem)
        {
            bool collected = false;
            for (int i = 0; i < llista.Count; i++)
            {
                if (!collected && llista[i] == null)
                {
                    collected = true;
                    item.quantity++;
                    llista[i] = item;
                }
            }

            if (!collected)
            {
                Debug.Log("inventario lleno! no se ha podido recoger el objeto");
            }
        }


        actualizarUI();
    }

    public void desaparecer()
    {
        if (inventario.activeSelf)
        {
            cursor.transform.position = cursorPosInicial;
            cursor2.transform.position = cursor2PosInicial;
            selectingPlayer = false;
            index = 0;
            indexPlayers = 0;
            inventario.SetActive(false);
        }
        else
        {
            inventario.SetActive(true);
            invSeleccionPlayers.SetActive(false);
        }
    }

    public void openPlayerSelect()
    {
        cursor2.transform.position = cursor2PosInicial;
        indexPlayers = 0;
        invSeleccionPlayers.SetActive(true);
    }





}
