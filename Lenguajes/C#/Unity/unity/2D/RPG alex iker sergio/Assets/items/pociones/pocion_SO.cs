using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/pocion", order = 1)]

public class pocion_SO : Item_ScriptableObject
{
    // Start is called before the first frame update
    public enum modificador { VIDA, MANA, EXP };

    public modificador modifier;
    public float cantidadAModificar;

    public override void useItem(stats_SC partyMember, Party_SC party)
    {
        //aqui la party no la necesito a menos que haga pociones de area o algo
        Debug.Log("he usado la pocion " + name + " en " + partyMember.name);

        switch (modifier)
        {
            case modificador.VIDA:
                partyMember.getHealed(cantidadAModificar);
                break;
            case modificador.MANA:
                partyMember.getMana(cantidadAModificar);
                break;
            case modificador.EXP:
                partyMember.getExp(cantidadAModificar);
                break;
            default:
                Debug.Log("NO DEBERIA HABER ENTRADO AQUI! asigna un modificador a la pocion");
                break;
        }



    }
}
