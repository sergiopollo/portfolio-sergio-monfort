using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ArmaScriptableObject", order = 1)]

public class ArmaScriptableObject : Item_ScriptableObject
{
    // Start is called before the first frame update
    public float atkFinal;
    public float velFinal;
    public string[] buffs;
    public BuffODebuff_SC[] buffs2;

    public override void useItem(stats_SC partyMember, Party_SC party)
    {
        Debug.Log("he equipado el arma " + name + " en " + partyMember.name);
        partyMember.equipWeapon(this, party);
    }
}
