using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//NO SE PUEDEN CREAR ITEMS; SON ABSTACTOS; TIENES QUE HACER HERENCIA
public abstract class Item_ScriptableObject : ScriptableObject
{

    public Sprite sprite;
    public string name;
    public int quantity;
    public int goldCost;

    public abstract void useItem(stats_SC partyMember, Party_SC party);
    

}


