using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BuffODebuff", order = 1)]
public class BuffODebuff_SC : ScriptableObject
{
   public Estado estado;
   public float modificacion;
   public int numTurnos;


    public void aplicar(UnidadDeCombate_Script objetivo)
    {
        switch (this.estado)
        {
            case Estado.AtkMod:
                objetivo.atkBase += (objetivo.atkBase*this.modificacion);
                break;
            case Estado.DefMod:
                objetivo.defBase += (objetivo.defBase * this.modificacion);
                break;
            case Estado.VelMod:
                objetivo.velBase += (objetivo.velBase * this.modificacion);
                break;
            case Estado.ManaMod:
                objetivo.manamax += (objetivo.manamax * this.modificacion);
                break;
            case Estado.HpMod:
                objetivo.hpmax += (objetivo.hpmax * this.modificacion);
                break;
            case Estado.XpBoost:
                objetivo.xpboost +=  this.modificacion;
                break;
            case Estado.DodgeMod:
                objetivo.dodgeBase += (objetivo.dodgeBase * this.modificacion);
                break;
            case Estado.Stun:
                objetivo.stun = true;
                break;
            case Estado.Fire:
                objetivo.hp += (objetivo.hpmax * this.modificacion);             
                break;
            case Estado.Bleed:
                objetivo.hp += (objetivo.hpmax * this.modificacion);
                break;
        }

    }
}
