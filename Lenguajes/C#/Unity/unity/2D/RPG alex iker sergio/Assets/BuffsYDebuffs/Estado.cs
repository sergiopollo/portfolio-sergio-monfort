using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Estado
{
    //STATS
    AtkMod,
    DefMod,
    VelMod,
    ManaMod,
    HpMod,
    XpBoost,
    DodgeMod,

    //ALTERACIONES-
    Stun,
    Fire,
    Bleed,

}
