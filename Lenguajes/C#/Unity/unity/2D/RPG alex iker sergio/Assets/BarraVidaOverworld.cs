using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraVidaOverworld : MonoBehaviour
{
    // Start is called before the first frame update
    public stats_SC statBruja;
    public stats_SC statCaballero;
    public stats_SC statArquero;

    public Slider ManaBruja;
    public Slider VidaBruja;
    public Slider ManaCaballero;
    public Slider VidaCaballero;
    public Slider ManaArquero;
    public Slider VidaArquero;


    void Start()
    {
       ManaBruja.maxValue = statBruja.manamax;
        VidaBruja.maxValue = statBruja.hpmax;
        ManaArquero.maxValue = statArquero.manamax;
        ManaCaballero.maxValue = statCaballero.manamax;
        VidaArquero.maxValue = statArquero.hpmax;
        VidaCaballero.maxValue = statCaballero.hpmax;


        Recalcular();
       


    }

     void Update()
    {
        Recalcular();
    }

    // Update is called once per frame
    public void Recalcular()
    {
        ManaBruja.value = statBruja.mana;
        VidaBruja.value = statBruja.hp;
        ManaArquero.value = statArquero.mana;
        ManaCaballero.value = statCaballero.mana;
        VidaArquero.value = statArquero.hp;
        VidaCaballero.value = statCaballero.hp;
    }
}
