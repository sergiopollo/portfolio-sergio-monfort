using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/habilidad", order = 1)]
public class habilidad_SC : ScriptableObject
{
    public string nombre;
    public float vidaAModificar;
    public BuffODebuff_SC buffODebuff;
    public bool objetivoEnemigo;
    public int numObjetivos;
    public float CosteMana;


    public void atacar(UnidadDeCombate_Script objetivo, float atkAtacante)
    {

        float fallar = Random.Range(0, 99);
        //IF CURA
        if (vidaAModificar > 0)
        {
            if ((objetivo.hp + this.vidaAModificar) > objetivo.hpmax)
            {
                objetivo.hp = objetivo.hpmax;
            }
            else
            {
                objetivo.hp += this.vidaAModificar;
            }         
        }
        else if ((fallar - objetivo.dodgeBase) > 0)
        {
            if ((this.vidaAModificar + objetivo.defBase) >= 0)
            {
                objetivo.hp--;
            }
            else
            {
                objetivo.hp += (this.vidaAModificar + (objetivo.defBase - atkAtacante));
            }
            if (this.buffODebuff != null && !objetivo.buffsYDebuffs.Contains(buffODebuff))
            {
                objetivo.buffsYDebuffs.Add(buffODebuff);
                objetivo.buffsTurns.Add(buffODebuff.numTurnos);
            }
            
        }
        else
        {
            Debug.Log("Ha fallado el ataque");
        }
    }


}
