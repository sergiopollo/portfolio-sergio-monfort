using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class encuentros : MonoBehaviour
{

    private int CombateInminente;
    public int pasosOverworld;
    public int pasosCueva;
    public GameObject Player;
    public escenaprevia sm;
    // Start is called before the first frame update
    void Start()
    {
        CombateInminente = 0;
        StartCoroutine("ControlarJugador");
    }
    void Update()
    {
        
    }

    IEnumerator ControlarJugador()
    {
        //Debug.Log("A");
        Player = GameObject.Find("jugador");

        while (true)
        {

            yield return new WaitForSeconds(1f);
            if (Player.GetComponent<Rigidbody2D>().velocity.x != 0 || Player.GetComponent<Rigidbody2D>().velocity.y != 0)
            {
                CombateInminente++;
            }

            if (SceneManager.GetActiveScene().name == "Overworld" && CombateInminente >= pasosOverworld)
            {
                sm.scene = "Overworld";
                SceneManager.LoadScene("CombateCampo");


            }
            else if (SceneManager.GetActiveScene().name == "Cueva" && CombateInminente >= pasosCueva)
            {
                sm.scene = "Cueva";
                SceneManager.LoadScene("CombateCampo");

            }
        }
    }
}
