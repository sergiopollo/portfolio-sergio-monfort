using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class entrarMazmorra : MonoBehaviour
{

    public int zonaALaQueIr;

    private void OnCollisionEnter2D(Collision2D collision)
    {

        switch (zonaALaQueIr)
        {
            case 0:

                SceneManager.LoadScene("Overworld");
                break;
            case 1:

                SceneManager.LoadScene("Cueva");
                break;
            case 2:

                SceneManager.LoadScene("Tienda");
                break;
            default:

                SceneManager.LoadScene("Overworld");
                break;
        }
    }
}
