using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/sceneManager", order = 1)]

public class escenaprevia : ScriptableObject
{
   public string scene;
}
