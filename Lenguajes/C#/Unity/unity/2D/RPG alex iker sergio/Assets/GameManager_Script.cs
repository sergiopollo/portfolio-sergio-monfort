using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager_Script : MonoBehaviour
{
    public bool overworld;

    public GameEvent abrirEsc;
    public GameEvent abrirInventario;
    public GameObject Player;
    public GameObject PartyMember;
    public Party_SC party;
    public GameObject Escape;

    public bool InventarioAbierto = false;

    public float distancia = 0.1f;
    public GameObject freeze;

    void Awake()
    {

    }

    void Start()
    {
        if (overworld)
        {
            Player = GameObject.Find("jugador");

            Player.GetComponent<SpriteRenderer>().sprite = party.Members[0].sprite;
            Player.transform.localScale = new Vector3(0.25f, 0.25f, 1);

            GameObject partyMemberAnterior = Player;

            

            //por cada miembro de la party crea un partymember
            for (int i = 1; i < party.Members.Length; i++)
            {
                Debug.Log("creado partymemeber numero " + i);
                GameObject newPartyMember = Instantiate(PartyMember);
                newPartyMember.GetComponent<SpriteRenderer>().sprite = party.Members[i].sprite;
                newPartyMember.transform.position = new Vector2 (partyMemberAnterior.transform.position.x + distancia/2, partyMemberAnterior.transform.position.y + distancia/2);
                newPartyMember.transform.localScale = new Vector3(0.25f, 0.25f, 1);
                StartCoroutine(followParty(newPartyMember,partyMemberAnterior));
                
                partyMemberAnterior = newPartyMember;
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            abrirInventario.Raise();
        }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            menuEscape();
        }

    }

    public void menuEscape()
    {
        if (Escape.active)
        {
            Escape.SetActive(false);
            freeze.SetActive(false);
        }
        else
        {
            Escape.SetActive(true);
            freeze.SetActive(true);
        }
    }

    IEnumerator followParty(GameObject newPartyMember,GameObject partyMemberAnterior)
    {

        Vector3 posicionHace1segundo = partyMemberAnterior.transform.position;
        Vector3 posicionHace2segundo = partyMemberAnterior.transform.position;

        while (true)
        {

            yield return new WaitForSeconds(0.5f);

            if (posicionHace1segundo != partyMemberAnterior.transform.position)
            {

                posicionHace2segundo = posicionHace1segundo;
                posicionHace1segundo = partyMemberAnterior.transform.position;
            }


            Vector2 posicionAnterior = new Vector2(partyMemberAnterior.transform.position.x+distancia, partyMemberAnterior.transform.position.y+distancia);

            //lerp

            StartCoroutine(LerpPosition(newPartyMember, partyMemberAnterior, posicionHace2segundo, 0.5f));
            //StartCoroutine(LerpPositionCoordinate(newPartyMember, posicionHace1segundo, 1));
        }
    }

    IEnumerator LerpPosition(GameObject newPartyMember, GameObject partyMemberAnterior, Vector2 ph1s, float duration)
    {
        //print("a");
        float time = 0;
        Vector2 startPosition = newPartyMember.transform.position;
        //print(target);
        while (time < duration)
        {
            newPartyMember.transform.position = Vector2.Lerp(startPosition, ph1s, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        //newPartyMember.transform.position = new Vector2(partyMemberAnterior.transform.position.x, partyMemberAnterior.transform.position.y);
    }

   

}

