using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveGame : MonoBehaviour
{
    public Party_SC party;
    public CosasGuardadas partidaGuardada;
    public CosasGuardadas partidaNueva;
    public Item_ScriptableObject[] itemsPartida;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("n"))
        {
            guardarPartida();
        }
        /*
        if (Input.GetKeyDown("m"))
        {
            //cargar partida guardada
            cargarPartida(false);
        }
        if (Input.GetKeyDown(","))
        {
            //cargar partida nueva
            cargarPartida(true);
        }
        */
    }

    public void guardarPartida()
    {
        partidaGuardada.nombreEscenaActual = SceneManager.GetActiveScene().name;
        partidaGuardada.party = party;
        partidaGuardada.itemsPartida = itemsPartida;

        string jsonStr = JsonUtility.ToJson(partidaGuardada);
        //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
        string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonStr));
        File.WriteAllText("Guardado.json", base64);
        print("partida guardada con exito");
    }

    public void cargarPartida(bool nueva)
    {
        if (nueva)
        {
            party.gold = partidaNueva.party.gold;
            party.listaItems = partidaNueva.party.listaItems;
            //igualar cada uno de los stats
            for (int i = 0; i < partidaNueva.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaNueva.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
            }
            itemsPartida = partidaNueva.itemsPartida;
            for (int i = 0; i < partidaGuardada.itemsPartida.Length; i++)
            {
                //empiecas con 3 pociones kk 
                if (itemsPartida[i].name.Equals("lesser Healing potion"))
                {
                    itemsPartida[i].quantity = 3;
                }
                else
                {
                    itemsPartida[i].quantity = 0;
                }
            }
            SceneManager.LoadScene(partidaNueva.nombreEscenaActual);
        }
        else
        {
            string base64 = File.ReadAllText("Guardado.json");
            //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
            string jsonStr = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
            JsonUtility.FromJsonOverwrite(jsonStr, partidaGuardada);
            party = partidaGuardada.party;
            itemsPartida = partidaGuardada.itemsPartida;
            SceneManager.LoadScene(partidaGuardada.nombreEscenaActual);
        }

        print("He cargado de puta madre");
    }

}
