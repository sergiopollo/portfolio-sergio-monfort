using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PartyMembers", order = 1)]
public class Party_SC : ScriptableObject
{
    // Start is called before the first frame update

    public stats_SC[] Members;
    public int gold;
    public List<Item_ScriptableObject> listaItems;

}
