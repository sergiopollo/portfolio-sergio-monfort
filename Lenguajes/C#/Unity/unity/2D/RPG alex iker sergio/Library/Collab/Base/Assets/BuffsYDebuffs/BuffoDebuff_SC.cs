using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BuffODebuff", order = 1)]
public class BuffODebuff_SC : ScriptableObject
{
    public float dmgAModificar;
    public float defAModificar;
}
