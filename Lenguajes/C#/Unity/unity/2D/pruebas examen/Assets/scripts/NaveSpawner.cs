using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaveSpawner : MonoBehaviour
{
    public GameObject nave;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Spawn");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            Instantiate(nave);
            int r = Random.Range(1,4);
            if (r==1)
            {
                nave.transform.position = new Vector3(0, 10, 0);
                nave.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (r==2)
            {
                nave.transform.position = new Vector3(0, -10, 0);
                nave.transform.eulerAngles=new Vector3(0,0,180);
            }
            else if (r == 3)
            {
                nave.transform.position = new Vector3(10, 0, 0);
                nave.transform.eulerAngles = new Vector3(0, 0, -90);
            }
            else if (r == 4)
            {
                nave.transform.position = new Vector3(-10, 0, 0);
                nave.transform.eulerAngles = new Vector3(0, 0, 90);
            }
            yield return new WaitForSeconds(1f);
        }
    }
}
