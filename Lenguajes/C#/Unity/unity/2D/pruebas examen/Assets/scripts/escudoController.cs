using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escudoController : MonoBehaviour
{
    public delegate void pierdeVida();
    public event pierdeVida OnPierdeVida;
    public int vida;

    // Start is called before the first frame update
    void Start()
    {
        vida = 7;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void mePegan()
    {
        OnPierdeVida?.Invoke();
    }
}
