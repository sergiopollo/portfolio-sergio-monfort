using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vidaController : MonoBehaviour
{
    public escudoController escudo;

    // Start is called before the first frame update
    void Start()
    {
        escudo.OnPierdeVida += ActualizarVida;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActualizarVida()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vida Escudo:" + escudo.vida;
    }
}
