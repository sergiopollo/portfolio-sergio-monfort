using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pj : MonoBehaviour
{
    public int spd;

    public delegate void ActivarSwitch();
    public event ActivarSwitch OnActivarSwitch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d")){
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(spd, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-spd, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }

        if (Input.GetKeyDown("w"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
        }


        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "switch")
        {
            OnActivarSwitch?.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "malo") {
            Destroy(this.gameObject);
        }
    }
}
