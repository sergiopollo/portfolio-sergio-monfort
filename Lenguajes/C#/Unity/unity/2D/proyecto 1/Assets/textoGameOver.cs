using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class textoGameOver : MonoBehaviour
{
    /*
    la UI estaba en una z muy alejada de la camara por alguna razon, y como el canvas tiene el movimiento bloqueado no podia moverlo hacia adelante,
    entonces para arreglarlo he cambiado el canvas de camera view a overlay, lo que hace que en el editor se vea gigante y mal colocado pero en el juego esta bien puesto.
    */

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void aparecer()
    {
        gameObject.SetActive(true);
    }

    public void buttonRestart()
    {
        //cosa para resetear y poner en el boton
        SceneManager.LoadScene("prueba juego cubos");
    }
}
