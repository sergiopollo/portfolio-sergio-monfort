using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigocontroller : MonoBehaviour
{
    // Start is called before the first frame update
    private playercontroller pc;
    private void OnEnable()
    {
        //esta funcion COME MUCHO. SOLO DEBE USARSE EN UN START, NUNCA EN UN UPDATE
        pc = GameObject.Find("player").GetComponent<playercontroller>();

        if (pc.temps <= 20)
        {
            this.GetComponent<Rigidbody2D>().velocity = (new Vector3(-8, 0, 0));

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = (new Vector3(-12, 0, 0));
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (pc.temps <= 20)
        {
            this.GetComponent<Rigidbody2D>().velocity = (new Vector3(-8, 0, 0));

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = (new Vector3(-12, 0, 0));
        }

        if (this.transform.position.x <= -10)
        {   
            //si no tiene padre (entonces no es compuesto) que se desactive solo el, si tiene padre que desactive al padre y por tanto se desactiva el tambien
            if (transform.parent == null)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.transform.parent.gameObject.SetActive(false);
            }
    
        }

    }
}
