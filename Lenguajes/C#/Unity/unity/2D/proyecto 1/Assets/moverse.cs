using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverse : MonoBehaviour
{
    public float speed;
    public float posInicial;
    public float limite;
    public playercontroller pc;

    // Start is called before the first frame update
    void Start()
    {
        //suscripcion al delegado GameOver de playercontroller+
        pc.OnGameOver += stopVel;
        this.GetComponent<Rigidbody2D>().velocity = (new Vector3(speed, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x <= limite)
        {
            //la z es 1 para que se queden detras, si la dejo en 0 cuando la mueva se vera delante de las paredes
            this.transform.position = new Vector3(posInicial, this.transform.position.y, 1);
        }
    }

    public void stopVel()
    {
        this.GetComponent<Rigidbody2D>().velocity = (new Vector3(0, 0, 0));
    }
    private void OnDisable()
    {
        //ens dessuscribim
        pc.OnGameOver -= stopVel;
    }
}
