using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroller : MonoBehaviour
{
    public bool tocaSuelo = false;
    public int temps = 0;
    public bool stopTiempo = false;
    public textoGameOver textoGameOver;

    //delegado
    public delegate void GameOver();
    public event GameOver OnGameOver;
    
    // Start is called before the first frame update
    void Start()
    {
        //llama a la funcion sumaTiempo
        sumaTiempo();

    }

    // Update is called once per frame
    void Update()
    {
       

        if (Input.GetKey("d"))
        {
            
            if (temps <= 20)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(6, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(9, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            }



        }else if(Input.GetKey("a"))
        {
            if (temps <= 20)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-6, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-9, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            }

        }

        if (Input.GetKeyUp("space"))
        {
            this.tocaSuelo = false;
        }


        if (Input.GetKey("space"))
        {
            if (tocaSuelo)
            {
                    
                if (this.GetComponent<Rigidbody2D>().velocity.y >= 17.5)
                {
                    this.tocaSuelo = false;
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 30));
                }

            }

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag=="suelo")
        {
            this.tocaSuelo = true;
        }


        if (collision.transform.tag=="enemigo")
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.transform.tag=="enemigo")
        {
            gameObject.SetActive(false);
        }
    }

    public void sumaTiempo()
    {
        if (!stopTiempo)
        {
            this.temps++;
            Invoke("sumaTiempo", 1f);
        }
    }

    private void OnDisable()
    {
        this.stopTiempo = true;
        OnGameOver?.Invoke();
        if (textoGameOver != null) {
        textoGameOver.aparecer();
        }

    }

}
