using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public GameObject enemigo;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("spawneaEnemigo", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void spawneaEnemigo()
    {
        Instantiate(enemigo);
        enemigo.transform.position = new Vector3(0,-10,0);
    }
}
