using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnterrenoViejo : MonoBehaviour
{

    public GameObject[] terrenos;
    //comprovar que las variables en el editor no esten diferentes
    public float spawnRate = 3f;
    public bool stop = false;

    public playercontroller pc;
    public int temps;

    // Start is called before the first frame update
    void Start()
    {

        //suscripcion al delegado GameOver de playercontroller+
        pc.OnGameOver += StopSpawn;

        this.temps = pc.temps;
        //invokerepeating pero hecho recursivamente con 2 invokes
        Invoke("spawnTerreno", spawnRate);

    }

    // Update is called once per frame
    void Update()
    {
        this.temps = pc.temps;

        if (temps == 20)
        {
            this.spawnRate = 1f;
        }

    }

    public void spawnTerreno()
    {
        if (!stop)
        {
            int index = Random.Range(0, terrenos.Length);
            Instantiate(terrenos[index]);
            Invoke("spawnTerreno", spawnRate);
        }

    }

    public void StopSpawn()
    {
        stop = true;
    }

    private void OnDisable()
    {
        //ens dessuscribim
        pc.OnGameOver -= StopSpawn;
    }

}
