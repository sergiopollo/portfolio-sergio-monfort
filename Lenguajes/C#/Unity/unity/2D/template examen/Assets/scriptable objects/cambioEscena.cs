using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/cambioEscena", order = 1)]
public class cambioEscena : ScriptableObject
{
    public int opcion;
}
