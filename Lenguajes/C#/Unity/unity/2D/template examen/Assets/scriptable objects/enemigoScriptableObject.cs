using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/EnemigoScriptable", order = 1)]
public class enemigoScriptableObject : ScriptableObject
{
    public float velFrontal;
    public int HpMax;
    public Color color;
    public float tempsCooldown;
    public int QuantProjectilsPerDisparo;
    public float AngleRotacio;
    public float velProj;
    public float sizeProj;

}