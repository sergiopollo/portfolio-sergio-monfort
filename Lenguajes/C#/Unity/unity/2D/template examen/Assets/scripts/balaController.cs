using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaController : MonoBehaviour
{
    public playerController playerController;
    public EnemicController enemicController;
    public float speed;
    public Vector3 direccion;
    public bool Blanco;
    public Sprite spriteBlanco;
    public Sprite spriteNegro;
    public Sprite supersprite;
    public bool balaEnemiga;
    public bool superBala;
    public float damage;

    // Start is called before the first frame update
    void Start()
    {
        this.speed = 7;
    }

    // Update is called once per frame
    void Update()
    {

        this.GetComponent<Rigidbody2D>().velocity = direccion * speed;
        
        if (this.transform.position.x > 10 || this.transform.position.x < -10 || this.transform.position.y > 10 || this.transform.position.y < -10)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        

        if (!balaEnemiga)
        {
            this.transform.position = playerController.transform.position + playerController.transform.up * 1f;
            this.transform.eulerAngles = playerController.gameObject.transform.eulerAngles;
            this.direccion = playerController.transform.up;
            if (this.superBala)
            {
                this.GetComponent<SpriteRenderer>().sprite = supersprite;
            }
            else
            {


                if (this.Blanco)
                {
                    this.GetComponent<SpriteRenderer>().sprite = spriteBlanco;
                }
                else
                {
                    this.GetComponent<SpriteRenderer>().sprite = spriteNegro;
                }
            }
        }
        else
        {
            this.transform.position = enemicController.transform.position - enemicController.transform.up * 0.5f;
  
            if (this.Blanco)
            {
                this.GetComponent<SpriteRenderer>().sprite = spriteBlanco;
            }
            else
            {
                this.GetComponent<SpriteRenderer>().sprite = spriteNegro;
            }
        }
        
    }
    private void Awake()
    {
        playerController = GameObject.Find("player").GetComponent<playerController>();
    }
}
