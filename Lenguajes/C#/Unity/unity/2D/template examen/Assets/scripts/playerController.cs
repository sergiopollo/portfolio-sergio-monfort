using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public int vida;
    public float Speed;
    public Sprite spriteBlanco;
    public Sprite spriteNegro;
    public bool Blanco;
    public int balasAbsorbidas;
    public cambioEscena cambio;
    //si cammbio es 1 = control raton, 0 control teclas
    public targetController targetController;
    public delegate void GameOver();
    public event GameOver OnGameOver;


    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().freezeRotation=true;
        this.Speed = 3f;
        this.Blanco = true;
        this.vida = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.vida <= 0)
        {
            OnGameOver?.Invoke();
            Destroy(this.gameObject);
        }

        if (cambio.opcion==1)
        {
            this.transform.position = new Vector3 (targetController.posicionRaton.x, targetController.posicionRaton.y, 0);
   
            //acceso a pool
            if (Input.GetMouseButtonDown(0))
            {
                GameObject bala = balaPool.SharedInstance.GetPooledObject();
                if (bala != null)
                {
                    bala.GetComponent<balaController>().Blanco = this.Blanco;
                    bala.GetComponent<balaController>().balaEnemiga = false;
                    bala.GetComponent<balaController>().damage = 1;

                    bala.SetActive(true);
                }

            }

            //cambio sprites
            if (Input.GetMouseButtonDown(1))
            {

                if (this.GetComponent<SpriteRenderer>().sprite == spriteBlanco)
                {
                    this.Blanco = false;
                    this.GetComponent<SpriteRenderer>().sprite = spriteNegro;
                }
                else
                {
                    this.Blanco = true;
                    this.GetComponent<SpriteRenderer>().sprite = spriteBlanco;
                }
                if (this.balasAbsorbidas >= 4)
                {
                    GameObject bala = balaPool.SharedInstance.GetPooledObject();
                    if (bala != null)
                    {
                        bala.GetComponent<balaController>().balaEnemiga = false;
                        bala.GetComponent<balaController>().superBala = true;
                        bala.GetComponent<balaController>().damage = balasAbsorbidas / 4;

                        bala.SetActive(true);
                    }
                    this.balasAbsorbidas = 0;
                }
            }
        }
        else
        {
            if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(Speed, this.GetComponent<Rigidbody2D>().velocity.y, 0);

            }
            else if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(-Speed, this.GetComponent<Rigidbody2D>().velocity.y, 0);

            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            }

            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, Speed, 0);

            }
            else if (Input.GetKey("s"))
            {

                this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, -Speed, 0);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, 0, 0);
            }

            //acceso a pool
            if (Input.GetKeyDown("space"))
            {
                GameObject bala = balaPool.SharedInstance.GetPooledObject();
                if (bala != null)
                {
                    bala.GetComponent<balaController>().Blanco = this.Blanco;
                    bala.GetComponent<balaController>().balaEnemiga = false;
                    bala.GetComponent<balaController>().damage = 1;

                    bala.SetActive(true);
                }

            }

            //cambio sprites
            if (Input.GetKeyDown("c"))
            {

                if (this.GetComponent<SpriteRenderer>().sprite == spriteBlanco)
                {
                    this.Blanco = false;
                    this.GetComponent<SpriteRenderer>().sprite = spriteNegro;
                }
                else
                {
                    this.Blanco = true;
                    this.GetComponent<SpriteRenderer>().sprite = spriteBlanco;
                }
                if (this.balasAbsorbidas >= 4)
                {
                    GameObject bala = balaPool.SharedInstance.GetPooledObject();
                    if (bala != null)
                    {
                        bala.GetComponent<balaController>().balaEnemiga = false;
                        bala.GetComponent<balaController>().superBala = true;
                        bala.GetComponent<balaController>().damage = balasAbsorbidas / 4;

                        bala.SetActive(true);
                    }
                    this.balasAbsorbidas = 0;
                }
            }
        }
        


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<balaController>() != null)
        {
            if (collision.GetComponent<balaController>().balaEnemiga)
            {
                if (collision.GetComponent<balaController>().Blanco)
                {
                    if (this.Blanco)
                    {
                        //absorbo
                        this.balasAbsorbidas++;
                    }
                    else
                    {
                        this.vida--;

                    }
                    collision.gameObject.SetActive(false);
                }
                else
                {
                    if (this.Blanco)
                    {
                        this.vida--;
                    }
                    else
                    {
                        //absorbo
                        this.balasAbsorbidas++;

                    }
                    collision.gameObject.SetActive(false);
                }
            }
        }
    }
}
