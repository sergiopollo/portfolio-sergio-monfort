using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemicController : MonoBehaviour
{
    public int vida;
    public bool Blanco;
    public Sprite spriteBlanco;
    public Sprite spriteNegro;

    // Start is called before the first frame update
    void Start()
    {
        this.vida = 10;
        if (this.Blanco)
        {
            this.GetComponent<SpriteRenderer>().sprite = spriteBlanco;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = spriteNegro;
        }
        StartCoroutine("Disparar");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.vida <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator Disparar()
    {
        while (true)
        {
            GameObject bala = balaPool.SharedInstance.GetPooledObject();
            if (bala != null)
            {
                bala.GetComponent<balaController>().Blanco = this.Blanco;
                bala.GetComponent<balaController>().balaEnemiga = true;
                bala.GetComponent<balaController>().enemicController = this;

                bala.GetComponent<balaController>().direccion = this.transform.up;

                bala.SetActive(true);
            }
            GameObject bala2 = balaPool.SharedInstance.GetPooledObject();
            if (bala2 != null)
            {
                bala2.GetComponent<balaController>().Blanco = this.Blanco;
                bala2.GetComponent<balaController>().balaEnemiga = true;
                bala2.GetComponent<balaController>().enemicController = this;

                bala2.transform.Rotate(0, 0, 30);
                bala2.GetComponent<balaController>().direccion = -bala2.transform.up;

                bala2.SetActive(true);
            }
            GameObject bala3 = balaPool.SharedInstance.GetPooledObject();
            if (bala3 != null)
            {
                bala3.GetComponent<balaController>().Blanco = this.Blanco;
                bala3.GetComponent<balaController>().balaEnemiga = true;
                bala3.GetComponent<balaController>().enemicController = this;

                bala3.transform.Rotate(0, 0, -30);
                bala3.GetComponent<balaController>().direccion = -bala2.transform.up;

                bala3.SetActive(true);
            }

            yield return new WaitForSeconds(3f);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<balaController>() != null)
        {
            if (collision.GetComponent<balaController>().balaEnemiga == false)
            {
                if (collision.GetComponent<balaController>().superBala)
                {
                    this.vida = this.vida - (int) collision.GetComponent<balaController>().damage;
                }
                else if (collision.GetComponent<balaController>().Blanco)
                {
                    if (!this.Blanco)
                    {
                        this.vida --;
                    }
                    collision.gameObject.SetActive(false);
                }
                else if(collision.GetComponent<balaController>().Blanco == false)
                {
                    if (this.Blanco)
                    {
                        this.vida--;
                    }
                    collision.gameObject.SetActive(false);
                }
            }
        }
    }
}
