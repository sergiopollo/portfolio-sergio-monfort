using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private playerController playerController;
    public int balasDisparadas;
    public Canvas canvas;
    public cambioEscena cambio;
    public int temps;
    public GameObject tempsUI;

    // Start is called before the first frame update
    void Start()
    {
        /*
        balasDisparadas = 0;
        //playerController.OnActualizarBalasUI += actualizacionBalasUI;
        */
        temps = 0;
        StartCoroutine("SumaTemps");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SliderSlided(float valor)
    {
        print(valor);
        //df.num1 = (int) valor;
    }

    public void OnDrodownDropdowned(int opcio)
    {
        print(opcio);

        cambio.opcion = opcio;
    }
    /*
    private void actualizacionBalasUI()
    {
        balasDisparadas++;
        canvas.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Balas disparadas: "+balasDisparadas;
    }
    */
    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "Examen1")
        {
            playerController = GameObject.Find("player").GetComponent<playerController>();
        }
        
    }

    public void BotonCambioEscena()
    {
        SceneManager.LoadScene("Examen1");
    }
    
    IEnumerator SumaTemps()
    {
        while (true)
        {
            this.temps++;

            tempsUI.GetComponent<TMPro.TextMeshProUGUI>().text = "Punts: " + temps;
            yield return new WaitForSeconds(1f);
        }
    }

    //sceneManager
    //SceneManager.GetActiveScene().name;
    //SceneManager.LoadScene("template2");
    //playerprefs
    //PlayerPrefs.GetInt("coso", 0); -> (devuelve 0 si es nulo)
    //PlayerPrefs.SetInt("coso", a) -> (pone la variable a en el campo coso del diccionario)
}
