using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemics : MonoBehaviour
{
    public playerController playerController;
    public GameObject enemic;
    public bool spawn;
    // Start is called before the first frame update
    void Start()
    {
        spawn = true;
        StartCoroutine("spawnEnemics");
        playerController.OnGameOver += ELJUgadorHaMuerto;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnEnemics()
    {
        while (spawn)
        {
            int random = Random.Range(0, 2);
            print(random);
            float randomPos = Random.Range(-7, 7);
            
            if (random == 0)
            {
                enemic.GetComponent<EnemicController>().Blanco = true;
            }
            else
            {
                enemic.GetComponent<EnemicController>().Blanco = false;
            }
            Instantiate(enemic);
            enemic.transform.position = new Vector3(randomPos, 4, 0);
            yield return new WaitForSeconds(3f);
        }
        
    }

    public void ELJUgadorHaMuerto()
    {

        print("s'ha matao");
        spawn = false;

    }
}
