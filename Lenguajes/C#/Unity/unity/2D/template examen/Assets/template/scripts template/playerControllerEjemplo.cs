using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerControllerEjemplo : MonoBehaviour
{
    public targetControllerEjemplo targetControllerEjemplo;
    public bool muevete = false;
    public float Speed;
    public Vector3 positionTarget;
    public delegate void ActualizarBalasUI();
    public event ActualizarBalasUI OnActualizarBalasUI;
    //movimiento por tiles
    //public Vector3Int TileActual;


    // Start is called before the first frame update
    void Start()
    {
        targetControllerEjemplo.OnUserClicked += OnClicked;
        this.GetComponent<Rigidbody2D>().freezeRotation=true;
        this.Speed = 7f;
    }

    // Update is called once per frame
    void Update()
    {
        //movimiento por tiles
        //TileActual = targetController.tilemap.WorldToCell(this.transform.position);
        if (muevete)
        {
            //movimiento por tiles
            //Vector3 direccion = positionTarget - TileActual;
            //movimiento coordenadas
            Vector3 direccion = positionTarget - this.transform.position;
            direccion.Normalize();
            direccion *= Speed;
            this.GetComponent<Rigidbody2D>().velocity = direccion;
            
            /*
            this.GetComponent<Rigidbody2D>().velocity = positionTarget-this.transform.position;
            this.GetComponent<Rigidbody2D>().velocity.Normalize();
            this.GetComponent<Rigidbody2D>().velocity *= Speed;
            */
        }
        //movimiento por tiles
        /*
        if (TileActual == positionTarget)
        {
            this.muevete = false;
        }
        */
        //movimiento coordenadas
        if (this.transform.position == positionTarget)
        {
            this.muevete = false;
        }

        //chuleta addforce
        //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(1,0));

        //control por teclado
        //rotacion 
        if (Input.GetKey("a"))
        {

            this.gameObject.transform.Rotate(0, 0, 0.3f);

        }
        else if (Input.GetKey("d"))
        {

            this.gameObject.transform.Rotate(0, 0, -0.3f);

        }
        //escala
        if (Input.GetKey("w"))
        {

            this.gameObject.transform.localScale = this.gameObject.transform.localScale + new Vector3(0.01f,0.01f,0);

        }
        else if (Input.GetKey("s"))
        {

            this.gameObject.transform.localScale = this.gameObject.transform.localScale - new Vector3(0.01f, 0.01f, 0);

        }
        //acceso a pool
        if (Input.GetKeyDown("space"))
        {
            GameObject bala = balaPoolEjemplo.SharedInstance.GetPooledObject();
            if (bala != null)
            {
                bala.SetActive(true);
                OnActualizarBalasUI?.Invoke();
            }

        }
    }

    private void OnClicked(Vector3 position, Vector3 positionTile)
    {
        muevete = true;
        //movimiento por coordenadas
        positionTarget = position;
        //movimiento por tiles
        //positionTarget = positionTile;
    }
    private void OnDisable()
    {
        targetControllerEjemplo.OnUserClicked -= OnClicked;
    }

}
