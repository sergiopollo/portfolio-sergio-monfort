using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class balaControllerEjemplo : MonoBehaviour
{
    public playerControllerEjemplo playerController;
    public float speed;
    public Vector3 direccion;

    // Start is called before the first frame update
    void Start()
    {
        this.speed = 1;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = -direccion*speed;

        if (this.transform.position.x > 10 || this.transform.position.x < -10 || this.transform.position.y > 10 || this.transform.position.y < -10)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        this.transform.position = playerController.transform.position - playerController.transform.up*0.1f;
        this.transform.eulerAngles = playerController.gameObject.transform.eulerAngles;
        this.direccion = playerController.transform.up;
    }
    private void Awake()
    {
        playerController = GameObject.Find("nave").GetComponent<playerControllerEjemplo>();
    }
}
