using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerEjemplo : MonoBehaviour
{
    private playerControllerEjemplo playerController;
    public int balasDisparadas;
    public Canvas canvas;
    public defaultScriptableObject df;
    public int temps;
    public GameObject tempsUI;

    // Start is called before the first frame update
    void Start()
    {
        balasDisparadas = 0;
        playerController.OnActualizarBalasUI += actualizacionBalasUI;
        temps = 0;
        StartCoroutine("SumaTemps");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SliderSlided(float valor)
    {
        print(valor);
        df.num1 = (int) valor;
    }

    public void OnDrodownDropdowned(int opcio)
    {
        print(opcio);
        df.num2 = opcio;
    }

    private void actualizacionBalasUI()
    {
        balasDisparadas++;
        canvas.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Balas disparadas: "+balasDisparadas;
    }
    void Awake()
    {
        playerController = GameObject.Find("nave").GetComponent<playerControllerEjemplo>();
    }

    public void BotonCambioEscena()
    {
        SceneManager.LoadScene("template2");
    }

    IEnumerator SumaTemps()
    {
        while (true)
        {
            this.temps++;
            //esto no va bien
            tempsUI.GetComponent<TMPro.TextMeshProUGUI>().text = "temps: " + temps;
            //canvas.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "temps: " + temps;
            yield return new WaitForSeconds(1f);
        }
    }
}
