using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Seleccionador : MonoBehaviour
    {
        private Vector2 initialMousePosition, currentMousePosition;
        private BoxCollider boxColl;

        void Start()
        {

        }


        void Update()
        {

            if (Input.GetMouseButtonDown(0))
            {

                initialMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);


                boxColl = gameObject.AddComponent<BoxCollider>();
                boxColl.isTrigger = true;
            // boxColl. = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                boxColl.transform.position = new Vector3 (boxColl.transform.position.x, boxColl.transform.position.y, 0);
            }


            if (Input.GetMouseButton(0))
            {
                currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                transform.position = (currentMousePosition + initialMousePosition) / 2;

                boxColl.size = new Vector3(
                    Mathf.Abs(initialMousePosition.x - currentMousePosition.x),
                    Mathf.Abs(initialMousePosition.y - currentMousePosition.y));
            }


            if (Input.GetMouseButtonUp(0))
            {
                Destroy(boxColl);
                transform.position = Vector3.zero;
            }
        }

    }

