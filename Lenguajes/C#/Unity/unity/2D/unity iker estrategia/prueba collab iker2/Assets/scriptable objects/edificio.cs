using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Data", menuName="ScriptableObjects/edificio", order=1)]
public class edificio : ScriptableObject
{ 
    public int vida;
    public int def;
    //la primera posicion es madera, la segunda piedra y la tercera oro
    public Vector3Int recursosCrafteo;

}
