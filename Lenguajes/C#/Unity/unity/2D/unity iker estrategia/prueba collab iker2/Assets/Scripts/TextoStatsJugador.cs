using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoStatsJugador : MonoBehaviour
{
    public StatsJugador stats;
    
    // Start is called before the first frame update
    void Start()
    {
        stats.numMadera = 99999;
        stats.numPiedra = 99999;
        stats.numOro = 99999;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Madera: "+stats.numMadera+"\nPiedra: "+stats.numPiedra+"\nOro: "+stats.numOro;
    }
}
