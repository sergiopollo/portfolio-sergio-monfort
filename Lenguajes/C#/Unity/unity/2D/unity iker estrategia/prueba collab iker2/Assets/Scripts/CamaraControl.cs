using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraControl : MonoBehaviour
{

    public Vector3 posini;
    public Vector3 distancia;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        Camera.main.orthographicSize -= Input.mouseScrollDelta.y;


        if (Input.GetMouseButtonDown(2))
            posini = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButton(2))
        {
            distancia = posini - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            distancia.z = 0;
            this.transform.position = this.transform.position + distancia;
            posini = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        }



    }
}
