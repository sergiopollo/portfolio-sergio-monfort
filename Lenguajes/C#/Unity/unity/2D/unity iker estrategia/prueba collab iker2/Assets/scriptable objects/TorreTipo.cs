using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TorreTipo", order = 1)]
public class TorreTipo : ScriptableObject
{
    public GameObject bala;
    public balaTipo balaTipo;
    public int fireRate;
    public int rango;
}
