using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{
    public int vel;
    public int atk;
    public GameObject enemigo;
    public balaTipo tipo;
    public Vector3 ultimaPosEnemigo;

    // Start is called before the first frame update
    void Start()
    {
        this.vel = tipo.speed;
        this.atk = tipo.atk;
        Color c = tipo.color;
        this.GetComponent<SpriteRenderer>().color = c;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemigo.activeSelf)
        {
            this.ultimaPosEnemigo = enemigo.transform.position;
        }

        float step = vel * Time.deltaTime;
        transform.position = Vector3.MoveTowards(this.transform.position, ultimaPosEnemigo, step);

        if (!enemigo.activeSelf )
        {
            this.gameObject.SetActive(false);
        }
        
    }
    
    private void OnEnable()
    {
        if (this.tipo != null)
        {
            this.vel = tipo.speed;
            this.atk = tipo.atk;
            Color c = tipo.color;
            this.GetComponent<SpriteRenderer>().color = c;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //cuando choque contra un enemigo, le reste vida y se elimine la bala, pool opcional
        if (other.tag == "Enemigo")
        {
            if (other != null && enemigo.GetComponent<EnemigoScript>() != null)
            {
                int dmgfinal = (this.atk - other.GetComponent<EnemigoScript>().def);
                if (dmgfinal <= 0)
                    dmgfinal = 1;

                other.GetComponent<EnemigoScript>().vida -= dmgfinal;
                if (other.GetComponent<EnemigoScript>().vida <= 0)
                {
                    other.GetComponent<EnemigoScript>().Morirse();
                    
                }
                this.gameObject.SetActive(false);
            }

        }
    }
}
