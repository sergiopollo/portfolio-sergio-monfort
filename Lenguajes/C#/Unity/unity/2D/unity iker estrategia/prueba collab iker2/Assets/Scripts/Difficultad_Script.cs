using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficultad_Script : MonoBehaviour
{

    public int nivelDiff;
    public int tiempoAumento;
    public delegate void incrementoDiff(int niveldif);
    public event incrementoDiff OnincrementoDiff;

    // Start is called before the first frame update
    void Start()
    {
        nivelDiff = 0;
        StartCoroutine("DiffInc");
    }

  
    IEnumerator DiffInc()
    {

        while (true)
        {
            yield return new WaitForSeconds(tiempoAumento);
            nivelDiff++;
            OnincrementoDiff?.Invoke(nivelDiff);
        
        
        }
    }

}
