using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nexo_Script : CosaColocable
{

    public edificio nexo;
    public delegate void gameOver();
    public event gameOver OnGameOver;

    // Start is called before the first frame update
    void Start()
    {
        nexo.vida = 5000;
        vida = nexo.vida;
        this.def = nexo.def;
    }

    // Update is called once per frame
    void Update()
    {

        nexo.vida = vida;

        if(nexo.vida <= 0)
        {
            // Debug.Log("Game over aqui");
            if (OnGameOver != null)
            {
               // print("delegado llamado");
            }
            OnGameOver?.Invoke();
            
            if (OnGameOver == null)
            {
              //  print("delegado nulo");
            }
            SceneManager.LoadScene("GameOverScene");
        }

    }
   
    private void OnMouseDown()
    {
        nexo.vida--;
    }
}
