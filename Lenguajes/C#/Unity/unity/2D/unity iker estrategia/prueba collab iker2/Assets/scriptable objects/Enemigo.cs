using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/enemigo", order = 1)]
public class Enemigo : ScriptableObject
{
    public int vida;
    public int def;
    public int dmg;
    public int spd;
    public float atkspd;
    public float size;
    public Color color;

}

