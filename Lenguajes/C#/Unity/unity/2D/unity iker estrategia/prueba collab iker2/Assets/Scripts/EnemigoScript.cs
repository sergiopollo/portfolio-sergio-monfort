using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoScript : MonoBehaviour
{
    public Enemigo tipo;
    public GameObject edificio;

    public int vida;
    public int def;
    public int dmg;
    public int spd;
    public float atkspd;
    public float size;
    private Color color;
    public bool recibir;

    // Start is called before the first frame update
    void Start()
    {
        
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        Morirse();
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.tag == "Da�able")
        {
            recibir = false;
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Da�able" && collision.isTrigger == false)
        {

            edificio = collision.gameObject;
            recibir = true;
            StartCoroutine("Da�ar");
        }
    }

    private void OnEnable()
    {
        
        PonerStats();
    }
    IEnumerator Da�ar()
    {
        while (recibir)
        {
            yield return new WaitForSeconds(this.atkspd);
            if (edificio != null && edificio.GetComponent<CosaColocable>() != null)
            {
                int dmgfinal = (this.dmg - edificio.GetComponent<CosaColocable>().def);
                if (dmgfinal <= 0)
                    dmgfinal = 1;
                edificio.GetComponent<CosaColocable>().vida -= dmg;
                if(edificio.GetComponent<CosaColocable>().vida <= 0)
                {
                    recibir = false;
                    edificio.GetComponent<CosaColocable>().Moricionar();
                }
            }

        }

    }

    public void Morirse()
    {
       // Debug.Log("Muerte");
        this.gameObject.SetActive(false);
    }

    public void PonerStats()
    {
        this.vida = tipo.vida;
        this.def = tipo.def;
        this.dmg = tipo.dmg;
        this.spd = tipo.spd;
        this.atkspd = tipo.atkspd;
        this.GetComponent<Transform>().localScale = new Vector3(tipo.size, tipo.size, 1);
        this.GetComponent<SpriteRenderer>().color = tipo.color;
        this.GetComponent<Pathfinding.AILerp>().speed = this.spd;
    }

}
