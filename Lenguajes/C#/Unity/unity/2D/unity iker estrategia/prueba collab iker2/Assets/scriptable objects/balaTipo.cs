using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/BalaTipo", order = 1)]
public class balaTipo : ScriptableObject
{
    public int atk;
    public int speed;
    public Color color;
}
