using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class UIbotones : MonoBehaviour
{
    public Tilemap tilemap;
    public bool colocar = false;
    public bool edificio1Tile = false;

    //lista con cada edificio
    public List<GameObject> listaEdificiosGameObject = new List<GameObject>();
    public int index;

    public bool preview;
    public GameObject edificioPreview;
    public Color colorEdificioPreview;
    public TileBase tilePintar;
    public bool puedesConstruir = true;

    public StatsJugador stats;
    public edificio edificioAConstruir;

    // Start is called before the first frame update
    void Start()
    {
        GameObject nexo = GameObject.Find("Nexo");
        nexo.GetComponent<CosaColocable>().tilemap = this.tilemap;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3Int posicionDeLaTileWorldToCell = (tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
        Tile tile = tilemap.GetTile<Tile>(posicionDeLaTileWorldToCell);

        Vector3Int pDeLaTileWTCIzquierda = new Vector3Int(posicionDeLaTileWorldToCell.x -1, posicionDeLaTileWorldToCell.y, posicionDeLaTileWorldToCell.z);
        Tile tileIzquierda = tilemap.GetTile<Tile>(pDeLaTileWTCIzquierda);

        Vector3Int pDeLaTileWTCIzquierdaAbajo = new Vector3Int(posicionDeLaTileWorldToCell.x -1, posicionDeLaTileWorldToCell.y -1, posicionDeLaTileWorldToCell.z);
        Tile tileIzquierdaAbajo = tilemap.GetTile<Tile>(pDeLaTileWTCIzquierdaAbajo);

        Vector3Int pDeLaTileWTCAbajo = new Vector3Int(posicionDeLaTileWorldToCell.x, posicionDeLaTileWorldToCell.y -1, posicionDeLaTileWorldToCell.z);
        Tile tileAbajo = tilemap.GetTile<Tile>(pDeLaTileWTCAbajo);

        Vector3 posicionDeLaTile = tilemap.CellToWorld(posicionDeLaTileWorldToCell);
        Vector3 posicionDelBordeDeLaTile = new Vector3(posicionDeLaTile.x + 0.5f, posicionDeLaTile.y + 0.5f, posicionDeLaTile.z);
        //print(tile);
        //print(posicionDeLaTile);
        //print(posicionDelBordeDeLaTile);
        

        //para colocar una casa
        if (this.colocar)
        {
            //print(posicionDeLaTile);

            //la preview
            if (preview)
            {
                GameObject newCasa = null;
                //si edificio1Tile es false colocara casa de 2*2, si es true colocara casa de 1*1
                newCasa = Instantiate(listaEdificiosGameObject[index]);
                newCasa.GetComponent<CosaColocable>().tilemap = tilemap;
                //newCasa.GetComponent<Collider>().isTrigger = true;
                newCasa.GetComponent<Collider>().enabled = false;
                Color c = newCasa.GetComponent<SpriteRenderer>().color;
                colorEdificioPreview = c;
                c.a = 0.5f;
                newCasa.GetComponent<SpriteRenderer>().color = c;

                this.edificioPreview = newCasa;
                this.preview = false;
            }

            //la preview siga la posicion del raton
            if (!this.edificio1Tile)
            {
                edificioPreview.transform.position = posicionDeLaTile;
                
                //si las tiles estan vacias se pone color verde y te deja construirlo, si no se pone color rojo y te lo destruye cuando intentas construir
                if (tile == null && tileIzquierda == null && tileIzquierdaAbajo == null && tileAbajo == null)
                {
                    Color c1 = edificioPreview.GetComponent<SpriteRenderer>().color;
                    c1.g = 1f;
                    c1.b = 0f;
                    c1.r = 0f;
                    edificioPreview.GetComponent<SpriteRenderer>().color = c1;
                    puedesConstruir = true;
                }
                else
                {
                    print("no puedes");
                    Color c1 = edificioPreview.GetComponent<SpriteRenderer>().color;
                    c1.g = 0f;
                    c1.b = 0f;
                    c1.r = 1f;
                    edificioPreview.GetComponent<SpriteRenderer>().color = c1;
                    puedesConstruir = false;
                }

            }
            else
            {
                //si edificio1Tile es true colocara casa de 1*1
                edificioPreview.transform.position = posicionDelBordeDeLaTile;
                //si es una torre, es deicir no es el muro que la coloque un pelin para arriba para que no quede raro
                if (!(index == 2))
                {
                    edificioPreview.transform.position = new Vector3(edificioPreview.transform.position.x, edificioPreview.transform.position.y + (float)0.5, edificioPreview.transform.position.z);
                }
                //si las tiles estan vacias se pone color verde y te deja construirlo, si no se pone color rojo y te lo destruye cuando intentas construir
                if (tile == null)
                {
                    Color c1 = edificioPreview.GetComponent<SpriteRenderer>().color;
                    c1.g = 1f;
                    c1.b = 0f;
                    c1.r = 0f;
                    edificioPreview.GetComponent<SpriteRenderer>().color = c1;
                    puedesConstruir = true;
                }
                else
                {
                    //print("no puedes");
                    Color c1 = edificioPreview.GetComponent<SpriteRenderer>().color;
                    c1.g = 0f;
                    c1.b = 0f;
                    c1.r = 1f;
                    edificioPreview.GetComponent<SpriteRenderer>().color = c1;
                    puedesConstruir = false;
                }
            }

            if (Input.GetMouseButtonDown(1))
            {

                if (puedesConstruir)
                {
                    //colocar tiles con colisiones
                    
                    if (edificio1Tile)
                    {
                        tilemap.SetTile(posicionDeLaTileWorldToCell, tilePintar);
                        edificioPreview.GetComponent<CosaColocable>().t1 = posicionDeLaTileWorldToCell;
                    }
                    else
                    {
                        tilemap.SetTile(posicionDeLaTileWorldToCell, tilePintar);
                        tilemap.SetTile(pDeLaTileWTCIzquierda, tilePintar);
                        tilemap.SetTile(pDeLaTileWTCIzquierdaAbajo, tilePintar);
                        tilemap.SetTile(pDeLaTileWTCAbajo, tilePintar);
                        edificioPreview.GetComponent<CosaColocable>().t1 = posicionDeLaTileWorldToCell;
                        edificioPreview.GetComponent<CosaColocable>().t2 = pDeLaTileWTCIzquierda;
                        edificioPreview.GetComponent<CosaColocable>().t3 = pDeLaTileWTCIzquierdaAbajo;
                        edificioPreview.GetComponent<CosaColocable>().t4 = pDeLaTileWTCAbajo;
                    }

                    //colocar la casa 
                    //edificioPreview.GetComponent<Collider>().isTrigger = false;
                    edificioPreview.GetComponent<CosaColocable>().edificio1Tile = this.edificio1Tile;
                    edificioPreview.GetComponent<Collider>().enabled = true;
                    edificioPreview.GetComponent<SpriteRenderer>().color = colorEdificioPreview;
                    
                    this.colocar = false;
                    edificioPreview.GetComponent<CosaColocable>().construido = true;
                    edificioPreview = null;
                    Invoke("Rescan", 1f);

                    //resta los rescursos de statsJugador
                    stats.numMadera -= edificioAConstruir.recursosCrafteo.x;
                    stats.numPiedra -= edificioAConstruir.recursosCrafteo.y;
                    stats.numOro -= edificioAConstruir.recursosCrafteo.z;
                }
                else
                {
                    Destroy(edificioPreview);
                    colocar = false;
                }

            }

        }
        /*
        if (Input.GetMouseButtonDown(0))
        {
            print(posicionDeLaTile);
        }
        */
    }

    public void colocarEdificio()
    {
        //print("selecciona un tile en el que colocar la casa");
        //si pulsas el boton de nuevo teniendo la preview activa que la quite
        if (colocar==true)
        {
            colocar = false;
            Destroy(edificioPreview);
        }
        else
        {
            //index cambia en ondropdowndropdowned
            //0==casa, 1==extractorRecursos,2==muro,3==torre,a partir de aqui tipos de torres
            edificioAConstruir = listaEdificiosGameObject[index].GetComponent<CosaColocable>().basic;

            //si no tienes los recursos necesarios que no deje construir el edificio
            if (stats.numMadera >= edificioAConstruir.recursosCrafteo.x && stats.numPiedra >= edificioAConstruir.recursosCrafteo.y && stats.numOro >= edificioAConstruir.recursosCrafteo.z)
            {
                this.colocar = true;
                this.preview = true;

                if (index == 0 || index == 1)
                {
                    //para edificios 2*2
                    this.edificio1Tile = false;
                }
                else
                {
                    //para edificios 1*1
                    this.edificio1Tile = true;
                }

                

            }
            else
            {
                print("te faltan recursos");
            }
        }
       
    }

    public void Rescan()
    {
        AstarPath.active.Scan();
    }

    //para el dropdown con las opciones de edificios
    public void OnDrodownDropdowned(int opcio)
    {
        index = opcio;
        //0==casa, 1==extractorRecursos,2==torre,3==muro
       // print(index);
    }

}
