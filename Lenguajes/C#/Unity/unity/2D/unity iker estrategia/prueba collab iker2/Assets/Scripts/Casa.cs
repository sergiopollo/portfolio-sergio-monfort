using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Casa : CosaColocable
{
  
    public bool creandoUnidades = false;


    // Start is called before the first frame update
    void Start()
    {

        //funcion de su padre cosacolocable
        inicioPartida();
    }


    // Update is called once per frame
    void Update()
    {

        if (creandoUnidades)
        {
            Color c = this.GetComponent<SpriteRenderer>().color;
            c.g = c.g+0.0005f;
            this.GetComponent<SpriteRenderer>().color = c;
        }
    }

    void OnMouseDown()
    {
        if (construido)
        {
            if (!creandoUnidades)
            {
                creandoUnidades = true;
                StartCoroutine("crearUnidades");
            }
            else
            {
                Color c = this.GetComponent<SpriteRenderer>().color;
                c.g = 1;
                this.GetComponent<SpriteRenderer>().color = c;
                creandoUnidades = false;
            }  
        }
    }

    IEnumerator crearUnidades()
    {
        while (creandoUnidades)
        {
            Color c = this.GetComponent<SpriteRenderer>().color;
            c.g = 0;
            this.GetComponent<SpriteRenderer>().color = c;
            yield return new WaitForSeconds(5f);
            GameObject newUnit = UnidadPool.SharedInstance.GetPooledObject();
            if (newUnit != null)
            {
                //que reste 5 de madera para crear una unidad
                stats.numMadera -= 5;
                newUnit.transform.position = new Vector3(this.transform.position.x + 1, this.transform.position.y - 1, this.transform.position.z);
                newUnit.SetActive(true);
            }
            else
            {
                print("maximo de unidades creadas (no quedan unidades que poolear)");
                c = this.GetComponent<SpriteRenderer>().color;
                c.g = 1;
                this.GetComponent<SpriteRenderer>().color = c;
                creandoUnidades = false;
            }
        }
    }





}
