using UnityEngine;
using System.Collections;

namespace Pathfinding
{



	/// <summary>
	/// Sets the destination of an AI to the position of a specified object.
	/// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
	/// This component will then make the AI move towards the <see cref="target"/> set on this component.
	///
	/// See: <see cref="Pathfinding.IAstarAI.destination"/>
	///
	/// [Open online documentation to see images]
	/// </summary>
	[UniqueComponent(tag = "ai.destination")]
	[HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
	public class AIDestinationSetter : VersionedMonoBehaviour
	{
        /// <summary>The object that the AI should move to</summary>
        /// 


        //MIRAR SI TENGO QUE INSTANCIAR UN NUEVO TARGET TAMBIEN, O SOLO TENGO QUE COGER EL DEL MAIN
        public Transform target;


		IAstarAI ai;
		private bool seleccionado;
		
		private Color c;
		void OnEnable()
		{
			ai = GetComponent<IAstarAI>();
			// Update the destination right before searching for a path as well.
			// This is enough in theory, but this script will also update the destination every
			// frame as the destination is used for debugging and may be used for other things by other
			// scripts as well. So it makes sense that it is up to date every frame.
			if (ai != null) ai.onSearchPath += Update;
		}

		void OnDisable()
		{
			if (ai != null) ai.onSearchPath -= Update;
		}

		/// <summary>Updates the AI's destination every frame</summary>
		void Update()
		{
            if(this.tag == "Enemigo")
            {
                //Debug.Log("Si soy");
                ai.destination = new Vector3(0,0,0);
            }
		   

			if (seleccionado)
			{
                if (Input.GetMouseButtonDown(1))
                {
                    deseleccionar();
                    if (target != null && ai != null) ai.destination = target.position;
                }
				

			}


		}
        private void Start()
        {
            target = GameObject.Find("Target").transform;

            c = this.GetComponent<SpriteRenderer>().color;
		}
        void OnMouseDown()
		{
			if(this.tag != "Enemigo")
            {
				if (this.seleccionado)
					{
						//si lo seleccionas estando ya seleccionado se deselecciona
						deseleccionar();
					}else
					{
						Seleccionar();
					}
            }
			
		}
        private void deseleccionar()
        {
            this.seleccionado = false;
            this.GetComponent<SpriteRenderer>().color = c;
        }

        private void Seleccionar()
        {
                this.ai.destination = this.transform.position;
				//print("selecciona una casilla a la que enviarme");
				this.seleccionado = true;				
				this.GetComponent<SpriteRenderer>().color = Color.red;
        }

        private void OnTriggerEnter(Collider collision)
        {
			//Debug.Log("Colisiono");
            if (collision.gameObject.GetComponent<Seleccionador>() && this.tag != "Enemigo")
            {
                Seleccionar();
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.GetComponent<Seleccionador>() && Input.GetMouseButton(0) && this.tag != "Enemigo")
            {
                deseleccionar();
            }
        }

    }
}
