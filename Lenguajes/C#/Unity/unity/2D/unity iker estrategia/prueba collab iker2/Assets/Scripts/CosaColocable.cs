using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class CosaColocable : MonoBehaviour
{

    public Nexo_Script nexo_script;
    public edificio basic;
    public StatsJugador stats;
    public Tilemap tilemap;
    public bool edificio1Tile;
    public Vector3Int t1, t2, t3, t4;
    public Boton_retry br;


    public int vida;
    public int def;
    //para evitar que los edificios hagan cosas en modo preview, hay que poner en todas las funciones de los edificios que solo se ejecuten si esta construido
    public bool construido;

    void Start()
    {
       
    }

    private void OnEnable()
    {
        if(this.gameObject.GetComponent<Nexo_Script>() == null)
        nexo_script.OnGameOver += GameOver;
        //print("me he suscrito correctamente");
    }

    public void CambioEscena(Scene escena, LoadSceneMode lsm)
    {

        if (SceneManager.GetActiveScene().name == "EscenaFinal")
        {
            Debug.Log("Estoy en la escena correcta");
            if (this != null && this.gameObject != null)
                Destroy(this.gameObject);
        }
    }

    public void GameOver()
    {
       if (this != null && this.gameObject != null)
            DontDestroyOnLoad(gameObject);
        //print("has llamao correctamente al delegado");
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void inicioPartida()
    {
        SceneManager.sceneLoaded += CambioEscena;
        vida = basic.vida;
        def = basic.def;
        this.construido = false;
        
    }

    public void Moricionar()
    {

        StartCoroutine("Reescan");

        if (!this.edificio1Tile)
        {
            tilemap.SetTile(t1, null);
            tilemap.SetTile(t2, null);
            tilemap.SetTile(t3, null);
            tilemap.SetTile(t4, null);
        }
        else
        {
            tilemap.SetTile(t1, null);
        }
       
    }

    IEnumerator Reescan()
    {
        yield return new WaitForSeconds(1f);
        AstarPath.active.Scan();
        nexo_script.OnGameOver -= GameOver;
        Destroy(this.gameObject);
    }

    void Awake()
    {
        nexo_script = GameObject.Find("Nexo").GetComponent<Nexo_Script>();
    }
}
