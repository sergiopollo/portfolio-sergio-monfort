using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PaqueteEnemigos", order = 1)]
public class PaquetesEnemigos : ScriptableObject
{

    public Enemigo[] tipos;

}

