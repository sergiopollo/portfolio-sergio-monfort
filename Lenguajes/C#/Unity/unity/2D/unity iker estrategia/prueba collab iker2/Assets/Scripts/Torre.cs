using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : CosaColocable
{
    //tipo de torre(ScriptableObject)
    public TorreTipo tipo;
    //public GameObject[] listaEnemigos;

    //Enemigo al que esta traqueando actualmente
    public GameObject enemigo;

    //Si esta disparando o no
    public bool disparar;

    //tipo de proyectil que dispara la torre
    public balaTipo balaTipo;

    //variables varias
    public int fireRate;
    public int rango;
    public bool on;
    public BoxCollider colsuelo;
    public BoxCollider colrango;

    // Start is called before the first frame update
    void Start()
    {
        this.on = false;
        this.fireRate = tipo.fireRate;
        this.rango = tipo.rango;
        this.balaTipo = tipo.balaTipo;
        this.enemigo = null;
        this.inicioPartida();
        //x escala 1+1 y y escala 0.5+0.5
        this.colrango.size = new Vector3(rango, 0.5f+(rango*0.5f), 0);

        
        //Debug.Log("All ready Boss!");
        //reScanEnemigos();
    }

    // Update is called once per frame
    void Update()
    {

        if (this.enemigo != null &&  !this.enemigo.activeSelf)
        {
            //Debug.Log("I'ts dead Boss!");
            this.enemigo = null;
        }


    }


    IEnumerator Disparar()
    {
        //Debug.Log("On it Boss!");
        while (true)
        {
            //Debug.Log("Shoting!!!!");
           
            yield return new WaitForSeconds(fireRate);

            if (disparar)
            {
                GameObject newBala = balaPool.SharedInstance.GetPooledObject();
           

            if (newBala != null && this.enemigo != null && this.enemigo.activeSelf && this.disparar)
            {
                //LA CORRUTINA O ALGUIEN GUARDA ENEMIGOS ANTERIORES, ENTONCES NO PARA Y DA LA SENSACION DE QUE DISPARA MUCHAS SEGUIDAS CUANDO NO DEBERIA



                newBala.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);

                newBala.GetComponent<bala>().enemigo = this.enemigo;
                newBala.GetComponent<bala>().tipo = this.balaTipo;
                newBala.SetActive(true);

                //if (this.enemigo.GetComponent<EnemigoScript>().vida < (newBala.GetComponent<bala>().atk - this.enemigo.GetComponent<EnemigoScript>().def))
                 //   enemigo = null;

                

                //SOY INCAPAZ DE MOVER LA BALA HACIA EL ENEMIGO, VOY A HACERLO EN EL SRIPT DE BALA DE FORMA ULTRA TERRORISTA :D
                //newBala.transform.Translate(enemigo.transform.position);
                //newBala.GetComponent<Rigidbody>().velocity = enemigo.GetComponent<Rigidbody>().velocity * velDisparo;
                //newBala.GetComponent<Rigidbody>().velocity = enemigo.transform.position * velDisparo;
            }
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (construido)
        {
            if (!on)
            {
                StartCoroutine("Enchegar");
                on = true;
            }
            
            if (other.tag == "Enemigo")
            {
                if (this.enemigo == null)
                {
                    //Debug.Log("Targeting: " + other.gameObject.transform.position);
                    this.enemigo = other.gameObject;
                    this.disparar = true;
                   // Debug.Log(disparar);
                }
            }
        }
    }

    IEnumerator Enchegar()
    {
        yield return new WaitForSeconds(1f);
        this.StartCoroutine("Disparar");
                
    }


    private void OnTriggerStay(Collider other)
    {
        if (construido)
        {
            if (other.tag == "Enemigo")
            {
                if (this.enemigo == null)
                {
                   // Debug.Log("Changing target in range! " + other.gameObject.transform.position);
                    this.enemigo = other.gameObject;
                    this.disparar = true;
                }
            }
        }
    }

   private void OnTriggerExit(Collider other)
    {
        if (construido)
        {
            if (other.tag == "Enemigo")
            {
                if (this.enemigo == other.gameObject)
                {
                   // Debug.Log("Target out of reach Boss!");
                    this.enemigo = null;
                    this.disparar = false;
                }
            }
        }
    }
}
