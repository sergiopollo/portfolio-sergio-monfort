using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoVidaNexo : MonoBehaviour
{
    public edificio nexo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Vida Nexo: " + nexo.vida;
    }
}
