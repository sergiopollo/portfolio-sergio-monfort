using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/statsJugador", order = 1)]
public class StatsJugador : ScriptableObject
{
    //los stats se resetean en el start de textostatsjugador
    public int numMadera;
    public int numPiedra;
    public int numOro;

}
