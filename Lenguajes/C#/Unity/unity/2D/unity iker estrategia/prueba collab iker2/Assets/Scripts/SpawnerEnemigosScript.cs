using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerEnemigosScript : MonoBehaviour
{

    public PaquetesEnemigos[] tipos;
    private PaquetesEnemigos paqueteEnemigos;
    private int numeroEnemigos;
    public int SpawnRate;
    private int random, randomX, randomY;
    public Difficultad_Script dif;

    private int d_hp, d_dmg;


    // Start is called before the first frame update
    void Start()
    {
        dif.OnincrementoDiff += GenerationUp;
        StartCoroutine("GenerarEnemigo");
    }

   public void GenerationUp(int niveldif)
    {
        if (SpawnRate > 1)
        {
            SpawnRate--;
        }

        d_hp = niveldif*2;
        d_dmg = niveldif; 
    }
  
    IEnumerator GenerarEnemigo()
    {

        while (true)
        {
            yield return new WaitForSeconds(SpawnRate);
            random = (int)Random.Range(0f, tipos.Length);
            paqueteEnemigos = tipos[random];
            //Debug.Log(paqueteEnemigos);
            numeroEnemigos = paqueteEnemigos.tipos.Length;

            
            randomY = (int)Random.Range(-1, 1);
            if (randomY == 0)
                randomY = 1;
            randomX = (int)Random.Range(-1, 2);
            
            for (int i = 0; i < paqueteEnemigos.tipos.Length; i++)
            {
                GameObject newEnemigo = EnemigoPool.SharedInstance.GetPooledObject();
                if (newEnemigo != null)
                 {
                 newEnemigo.GetComponent<EnemigoScript>().tipo = paqueteEnemigos.tipos[i];
                    newEnemigo.GetComponent<EnemigoScript>().vida += d_hp;
                    newEnemigo.GetComponent<EnemigoScript>().dmg += d_hp;
                    //Debug.Log(newEnemigo.GetComponent<EnemigoScript>().tipo);

                    newEnemigo.transform.position = new Vector3((45*randomX) + i, (45 * randomY) +i, 0);
                 //Debug.Log(newEnemigo.transform.position);
                 newEnemigo.SetActive(true);
                }
                else
                {
                   // print("maximo de enemigos creados (no quedan enemigos por poolear)");
                    break;
                }
            }
            
        }

    }
}
