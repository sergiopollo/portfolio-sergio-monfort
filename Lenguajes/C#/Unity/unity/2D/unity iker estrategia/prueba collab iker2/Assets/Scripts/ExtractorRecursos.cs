using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ExtractorRecursos : CosaColocable
{
 
    public bool extrayendo = false;
    public bool stop = false;
    public GameObject[] listaDepositos;
    public GameObject deposito;

    // Start is called before the first frame update
    void Start()
    {
        //funcion de su padre cosacolocable
        inicioPartida();
        listaDepositos = GameObject.FindGameObjectsWithTag("deposito");
    }

    // Update is called once per frame
    void Update()
    {
        if (construido)
        {
            if (!stop)
            {
                stop = true;
                comprobarSiHayDeposito();
                if (this.deposito != null)
                {
                    Color c = this.GetComponent<SpriteRenderer>().color;
                    c.r = 0.5f;
                    this.GetComponent<SpriteRenderer>().color = c;
                    print("A EXTRAEEER");
                    this.extrayendo = true;
                    StartCoroutine("Extraer");
                }
                else
                {
                    print("fuuuuuuuuuuck");
                }
                
            }
        }
    }

    IEnumerator Extraer()
    {
        //si tiene tiles con depositos de recursos alrededor suyo, que extraiga, si no, no hace nada y es completamente inutil (comprobado antes)
        while (extrayendo)
        {
            yield return new WaitForSeconds(1f);
            if (deposito.GetComponent<depositoRecursos>() != null)
            {
                //si no le quedan recursos al deposito, que pare el extractor
                if (deposito.GetComponent<depositoRecursos>().numRecurso > 0)
                {
                    deposito.GetComponent<depositoRecursos>().aņadirRecurso();
                }
                else
                {
                    print("no quedan recursos que extraer");
                    Color c = this.GetComponent<SpriteRenderer>().color;
                    c.r = 1;
                    this.GetComponent<SpriteRenderer>().color = c;
                    extrayendo = false;
                }
            }
        }
        
    }

    void comprobarSiHayDeposito()
    {
        for (int i = 0; i < listaDepositos.Length; i++)
        {
            GameObject depositoDeLaLista = listaDepositos[i];
            print("mi x: "+this.transform.position.x);
            print("x deposito: "+depositoDeLaLista.transform.position.x);
            print("mi y: " + this.transform.position.y);
            print("y deposito: " + depositoDeLaLista.transform.position.y);
            //si hay un deposito de recursos justo en la tile de al lado ya sea arriba debajo derecha o izquierda, se le asigna y empieza a extraer los recursos
            if ((this.transform.position.y == depositoDeLaLista.transform.position.y && (this.transform.position.x == depositoDeLaLista.transform.position.x + 2 || this.transform.position.x == depositoDeLaLista.transform.position.x - 2)) || (this.transform.position.x == depositoDeLaLista.transform.position.x && (this.transform.position.y == depositoDeLaLista.transform.position.y + 2 || this.transform.position.y == depositoDeLaLista.transform.position.y - 2)))
            {
                this.deposito = depositoDeLaLista;
                i = listaDepositos.Length + 1;
            }
        }

    }

}
