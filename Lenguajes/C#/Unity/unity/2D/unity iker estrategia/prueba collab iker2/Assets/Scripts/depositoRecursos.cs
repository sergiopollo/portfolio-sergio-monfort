using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class depositoRecursos : MonoBehaviour
{
    public Nexo_Script nexo_script;
    public StatsJugador stats;
    public int numRecurso;
    public string tipoRecurso;
    public int min;
    public int max;

    // Start is called before the first frame update
    void Start()
    {
        int rand = Random.Range(min, max);
        /*
        int randTipo = Random.Range(1, 3);
        if (randTipo == 1)
        {
            this.tipoRecurso = "madera";

        }else if (randTipo == 2)
        {
            this.tipoRecurso = "piedra";
        }
        else if (randTipo == 3)
        {
            this.tipoRecurso = "oro";
        }
        */

        this.numRecurso = rand;


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDown()
    {
        //print("tipo:"+tipoRecurso+" cantidad:"+numRecurso);
        if (this.tipoRecurso == "madera")
        {
            AņadirMadera();
        }else if (this.tipoRecurso == "piedra")
        {
            AņadirPiedra();
        }else if (this.tipoRecurso == "oro")
        {
            AņadirOro();
        }
        
    }

    public void aņadirRecurso()
    {
        //print("tipo:" + tipoRecurso + " cantidad:" + numRecurso);
        if (this.tipoRecurso == "madera")
        {
            AņadirMadera();
        }
        else if (this.tipoRecurso == "piedra")
        {
            AņadirPiedra();
        }
        else if (this.tipoRecurso == "oro")
        {
            AņadirOro();
        }
    }

    void AņadirMadera()
    {
        if (numRecurso > 0)
        {
            numRecurso--;
            stats.numMadera += 1;
        }
    }

    void AņadirPiedra()
    {
        if (numRecurso > 0)
        {
            numRecurso--;
            stats.numPiedra += 1;
        }
    }

    void AņadirOro()
    {
        if (numRecurso > 0)
        {
            numRecurso--;
            stats.numOro += 1;
        }
    }

    private void OnEnable()
    {
        nexo_script.OnGameOver += gameOver;
    }

    private void OnDisable()
    {
        nexo_script.OnGameOver -= gameOver;
    }

    public void gameOver()
    {
        DontDestroyOnLoad(gameObject);
    }

}
