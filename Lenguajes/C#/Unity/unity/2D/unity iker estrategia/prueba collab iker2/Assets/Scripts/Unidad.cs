using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unidad : MonoBehaviour
{
    public int vida;
    public int atk;
    public int def;
    public int velocidad;
    public bool atacar;
    public GameObject enemigo;
    //public bool seleccionado = false;
    //public Vector3 pos;
    //public bool hasTarget = false;


    // Start is called before the first frame update
    void Start()
    {
        this.enemigo = null;
        StartCoroutine("Da�ar");
    }

    void Update()
    {
        if (this.enemigo != null && !this.enemigo.activeSelf)
        {
            Debug.Log("I'ts dead Boss!");
            this.enemigo = null;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.tag == "Enemigo")
        {
            if(this.enemigo == collision.gameObject)
            Debug.Log("Target out of reach Boss!");
            atacar = false;
            this.enemigo = null;
        }
    }

    private void OnTriggerStay(Collider other)
    {

            if (other.tag == "Enemigo")
            {
                if (this.enemigo == null)
                {
                    Debug.Log("Changing target in range! " + other.gameObject.transform.position);
                    this.enemigo = other.gameObject;
                    this.atacar = true;
                }
            }
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Enemigo")
        {
            if(this.enemigo == null)
            {
            Debug.Log("Targeting: " + collision.gameObject.transform.position);
            this.enemigo = collision.gameObject;
            this.atacar = true;
            }
            
            
        }
    }


    IEnumerator Da�ar()
    {
        while (true)
        {   yield return new WaitForSeconds(1f);
            if (atacar)
            {
            
            if (enemigo != null && this.enemigo.activeSelf)
            {

                int dmgfinal = (this.atk - enemigo.GetComponent<EnemigoScript>().def);
                if (dmgfinal <= 0)
                    dmgfinal = 1;

                enemigo.GetComponent<EnemigoScript>().vida -= dmgfinal;
                if (enemigo.GetComponent<EnemigoScript>().vida <= 0)
                {
                    enemigo.GetComponent<EnemigoScript>().Morirse();
                }
            }
            }
           
        }

    }
   
}
