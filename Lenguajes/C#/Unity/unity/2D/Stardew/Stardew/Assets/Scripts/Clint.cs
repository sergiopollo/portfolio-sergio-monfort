using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clint : MonoBehaviour
{
    public manager gameManager;
    [SerializeField]
    private int spd=5;
    public string state;
    public Vector2 posicioInicial;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        this.animator = this.GetComponent<Animator>();
        //aixada, regadora, tisores, llavorA, llavorM, llavorG
        state = "aixada";
        posicioInicial = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = new Vector2();
        if (Input.GetKey(KeyCode.W))
        {
            move += Vector2.up;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            move -= Vector2.up;
        }
        if (Input.GetKey(KeyCode.A))
        {
            move -= Vector2.right;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            move += Vector2.right;
        }


        this.GetComponent<Rigidbody2D>().velocity = move.normalized * spd;

        //Debug.Log(state);

        if (Input.GetKeyDown("1"))
        {
            state = "aixada";
        }
        else if (Input.GetKeyDown("2"))
        {
            
            state = "regadora";
        }
        else if (Input.GetKeyDown("3"))
        {

            state = "tisores";
        }
        else if (Input.GetKeyDown("4"))
        {
            if (gameManager.semillasA > 0)
            {
                state = "llavorA";
                StartCoroutine(wait());
            }
            else
            {
                Debug.Log("no tienes semillas");
            }
        }
        else if (Input.GetKeyDown("5"))
        {
            if (gameManager.semillasM > 0)
            {
                state = "llavorM";
                StartCoroutine(wait());
            }
            else
            {
                Debug.Log("no tienes semillas");
            }
        }
        else if (Input.GetKeyDown("6"))
        {
            if (gameManager.semillasG > 0)
            {
                state = "llavorG";
                StartCoroutine(wait());
            }
            else
            {
                Debug.Log("no tienes semillas");
            }
        }

        

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (state)
            {
                case "aixada":
                    //activar animacion
                    animator.Play("aixada", -1, 0f);
                    StartCoroutine(idle());
                    break;
                case "regadora":
                    animator.Play("regadora", -1, 0f);
                    StartCoroutine(idle());
                    break;
                case "tisores":
                    animator.Play("tisores", -1, 0f);
                    StartCoroutine(idle());
                    break;
                default:
                    Debug.Log("selecciona una herramienta");
                    break;
            }
        }

    }

    IEnumerator idle()
    {
        yield return new WaitForSeconds(1f);
        animator.Play("idle", -1, 0f);
    }


    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.5f);


            state = "aixada";
    }

    public void cambioDia()
    {
        print("a");
        this.transform.position = posicioInicial;
    }

}
