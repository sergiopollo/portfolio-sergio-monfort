using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manager : MonoBehaviour
{
    public GameObject menuCompra;
    public int dia;
    public int dinero;
    public int semillasA;
    public int semillasM;
    public int semillasG;
    public GameEvent cambioDia;
    public GameEvent cambioDinero;

    public GameObject bondia;

    // Start is called before the first frame update
    void Start()
    {
        dinero = 50;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            bondia.GetComponent<AudioSource>().Play();
            cambioDia.Raise();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            abrirMenu();
        }
    }

    public void abrirMenu()
    {
        if (menuCompra.activeSelf)
        {
            menuCompra.SetActive(false);
        }
        else
        {
            menuCompra.SetActive(true);
        }
    }

    public void comprarA()
    {
        if (dinero >= 10)
        {
            semillasA++;
            dinero -= 10;
            cambioDinero.Raise();
        }
    }

    public void comprarM()
    {
        if (dinero >= 20)
        {
            semillasM++;
            dinero -= 20;

            cambioDinero.Raise();
        }
    }

    public void comprarG()
    {
        if (dinero >= 5)
        {
            semillasG++;
            dinero -= 5;

            cambioDinero.Raise();
        }
    }


}
