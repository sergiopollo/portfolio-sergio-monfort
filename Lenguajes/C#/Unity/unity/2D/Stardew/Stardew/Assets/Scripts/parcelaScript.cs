using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parcelaScript : MonoBehaviour
{
    public Sprite seco;
    public Sprite arado;
    public Sprite regado;
    public Clint clint;
    public string state;
    public planta miPlanta;
    public planta alberginia;
    public planta maduixa;
    public planta girasol;
    public manager gameManager;
    public GameEvent moneyChanged;
    // Start is called before the first frame update
    void Start()
    {
        //seca, remoguda, regada
        state = "seca";
        seco = this.gameObject.GetComponent<SpriteRenderer>().sprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void cambioEstado(string estadoClint)
    {
        switch (state)
        {
            case "seca":
                if (estadoClint == "aixada")
                {
                    state = "remoguda";
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = arado;
                }
                break;
            case "remoguda":
                if(miPlanta != null && this.gameObject.GetComponent<SpriteRenderer>().sprite == miPlanta.sprite3)
                {
                    if (estadoClint == "tisores")
                    {
                        //se recoge la planta
                        gameManager.dinero += miPlanta.sellPrice;
                        moneyChanged.Raise();
                        state = "seca";
                        this.miPlanta = null;
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = seco;
                    }
                }else if (estadoClint == "regadora")
                {
                    state = "regada";
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = regado;
                }
                break;
            case "regada":
                //si cambia de dia y se llama a esta funcion con un evento
                if (miPlanta == null)
                {
                    if (estadoClint == "regadora")
                    {
                        state = "regada";
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = regado;
                    }
                    else if(estadoClint == "cambioDia")
                    {
                        state = "seca";
                        this.gameObject.GetComponent<SpriteRenderer>().sprite = seco;
                    }
                }
                else
                {
                    //hay planta
                    state = "remoguda";
                    if (estadoClint == "tisores")
                    {
                        //se recoge la planta
                        gameManager.dinero += miPlanta.sellPrice;

                        moneyChanged.Raise();
                    }
                }
                break;
            default:
                Debug.Log("error?");
                break;
        }
    }

    public void cambioDia()
    {
        if (this.state == "regada" && this.miPlanta != null)
        {
            if(this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite1)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite2;
            }
            else if (this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite2)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite3;
            }
        }
        cambioEstado("cambioDia");
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Clint" && (state == "remoguda" || state=="regada"))
        {
            if (this.miPlanta == null)
            {
                Debug.Log(clint.state);
                if (clint.state == "llavorA")
                {
                    this.miPlanta = alberginia;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite1;
                }
                else if (clint.state == "llavorM")
                {
                    this.miPlanta = maduixa;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite1;
                }
                else if (clint.state == "llavorG")
                {
                    this.miPlanta = girasol;
                    this.gameObject.GetComponent<SpriteRenderer>().sprite = miPlanta.sprite1;
                }
            }
        }
        else if (collision.gameObject.name == "aixada")
        {
            cambioEstado("aixada");
        }
        else if (collision.gameObject.name == "regadora")
        {
            cambioEstado("regadora");
        }
        else if (collision.gameObject.name == "tisores")
        {
            cambioEstado("tisores");
        }
    }

}
