using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/partidaGuardada", order = 1)]
public class partidaGuardada : ScriptableObject
{
    public int dinero;
    public int semillasA;
    public int semillasM;
    public int semillasG;
}
