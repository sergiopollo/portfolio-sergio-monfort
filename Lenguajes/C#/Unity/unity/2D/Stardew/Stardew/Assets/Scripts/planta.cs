using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/planta", order = 1)]
public class planta : ScriptableObject
{
    public float growTime;
    public int sellPrice;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
}
