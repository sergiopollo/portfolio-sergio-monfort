using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;

public class saveGame : MonoBehaviour
{
    public partidaGuardada partidaGuardada;
    public manager gameManager;
    public GameEvent dineroCambiado;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            guardarPartida();
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            cargarPartida();
        }
    }
    
    public void guardarPartida()
    {
        partidaGuardada.dinero = gameManager.dinero;
        partidaGuardada.semillasA = gameManager.semillasA;
        partidaGuardada.semillasM = gameManager.semillasM;
        partidaGuardada.semillasG = gameManager.semillasG;

        string jsonStr = JsonUtility.ToJson(partidaGuardada);
        //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
        string base64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(jsonStr));
        //File.WriteAllText("Guardado.json", base64);
        File.WriteAllText("Guardado.json", jsonStr);
        print("partida guardada con exito");
        
    }

    public void cargarPartida()
    {
            Debug.Log("Estoy cargando partida");
            string jsonStr = File.ReadAllText("Guardado.json");
            //string base64 = File.ReadAllText("Guardado.json");
            //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
            //string jsonStr = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
            JsonUtility.FromJsonOverwrite(jsonStr, partidaGuardada);

            gameManager.dinero = partidaGuardada.dinero;
        dineroCambiado.Raise();
            gameManager.semillasA = partidaGuardada.semillasA;
             gameManager.semillasM = partidaGuardada.semillasM;
            gameManager.semillasG= partidaGuardada.semillasG;

            print("He cargado de puta madre");

    }

    /*
    public void cargarPartida(bool nueva)
    {
        if (nueva)
        {
            Debug.Log("Estoy haciendo una nueva partida");
            
            party.gold = partidaNueva.party.gold;
            party.listaItems = partidaNueva.party.listaItems;
            //igualar cada uno de los stats
            for (int i = 0; i < partidaNueva.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaNueva.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
            }
            itemsPartida = partidaNueva.itemsPartida;
            for (int i = 0; i < partidaNueva.itemsPartida.Length; i++)
            {
                //Debug.Log("Estoy modificando items: " + i);
                //empiecas con 3 pociones kk 
                if (itemsPartida[i].name.Equals("lesser Healing potion"))
                {
                    itemsPartida[i].quantity = 3;
                }
                else
                {
                    itemsPartida[i].quantity = 0;
                }
            }
            SceneManager.LoadScene(partidaNueva.nombreEscenaActual);
        }
        else
        {
            Debug.Log("Estoy cargando partida");
            string jsonStr = File.ReadAllText("Guardado.json");
            //string base64 = File.ReadAllText("Guardado.json");
            //si vols guardarho en base64. base64 es un format de compressió per a que els jugadors no puguin canviar el savegame a ma, i per a que ocupi menys el fitxer de save
            //string jsonStr = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
            JsonUtility.FromJsonOverwrite(jsonStr, partidaGuardada);
            for (int i = 0; i < partidaGuardada.party.Members.Length; i++)
            {
                string miembroJson = JsonUtility.ToJson(partidaGuardada.party.Members[i]);
                JsonUtility.FromJsonOverwrite(miembroJson, party.Members[i]);
            }
            //party = partidaGuardada.party;
            itemsPartida = partidaGuardada.itemsPartida;
            SceneManager.LoadScene(partidaGuardada.nombreEscenaActual);
        }
        
            print("He cargado de puta madre");
        
    }
        */
}
