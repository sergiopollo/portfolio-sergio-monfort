using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/vidaSO", order = 1)]
public class vidaSO : ScriptableObject
{
    public float hpPlayer;
    public float hpBoss;
}
