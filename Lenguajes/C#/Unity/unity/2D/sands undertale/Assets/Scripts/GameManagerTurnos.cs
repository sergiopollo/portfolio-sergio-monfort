using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerTurnos : MonoBehaviour
{
    public vidaSO vida;
    public float hpBoss;
    public pj player;
    public bool turnoBoss=false;
    int numCuras = 5;
    public GameEvent cambioHpBoss;
    public GameEvent cambioHpPlayer;

    public GameObject proyectil;
    // Start is called before the first frame update
    void Start()
    {
        this.hpBoss = vida.hpBoss;
        this.hpBoss = 100;
        vida.hpBoss = 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator TurnoBoss()
    {
        StartCoroutine(atacar());

        yield return new WaitForSeconds(7f);

        StopCoroutine(atacar());

        player.move = false;
        this.turnoBoss = false;
    }

    IEnumerator atacar()
    {
        while (turnoBoss) { 
        GameObject newProyectil = Instantiate(proyectil);
            newProyectil.transform.position = new Vector2(-3, -2);
        yield return new WaitForSeconds(0.35f);
        }
    }

    public void fight()
    {
        if (!turnoBoss)
        {
            float diezporciento = this.hpBoss * 10 / 100;
            this.hpBoss -= diezporciento;
            vida.hpBoss = this.hpBoss;
            cambioHpBoss.Raise();
            turnoBoss = true;
            player.move = true;
            StartCoroutine(TurnoBoss());
        }
    }

    public void item()
    {
        if (!turnoBoss)
        {
            if (numCuras > 0)
            {
                float cuarentaporciento = player.maxHp * 40 / 100;
                if (player.hp + cuarentaporciento > player.maxHp)
                {
                    player.hp = player.maxHp;
                    vida.hpPlayer = player.hp;
                    cambioHpPlayer.Raise();
                }
                else
                {
                    player.hp += cuarentaporciento;
                    vida.hpPlayer = player.hp;
                    cambioHpPlayer.Raise();
                }
                numCuras--;
            }
            else
            {
                Debug.Log("no te quedan items");
            }
            
            turnoBoss = true;
            player.move = true;

            StartCoroutine(TurnoBoss());
        }
    }

    

}
