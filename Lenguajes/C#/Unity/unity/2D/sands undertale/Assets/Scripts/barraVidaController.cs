using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barraVidaController : MonoBehaviour
{
    public vidaSO vida;
    public GameManagerTurnos gameManager;
    public GameObject barraVida;
    public bool isBoss;
    public float hp;
    // Start is called before the first frame update
    void Start()
    {
        if (isBoss)
        {
            hp = vida.hpBoss;
        }
        else
        {
            hp = vida.hpPlayer;
        }

        barraVida.transform.localScale = new Vector3(1, barraVida.transform.localScale.y, barraVida.transform.localScale.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void cambioHp()
    {
        if (isBoss)
        {
            hp = vida.hpBoss;
        }
        else
        {
            hp = vida.hpPlayer;
        }
        barraVida.transform.localScale = new Vector3(hp/100, barraVida.transform.localScale.y, barraVida.transform.localScale.z);
        barraVida.transform.position = new Vector2(barraVida.transform.position.x-0.05f, barraVida.transform.position.y);
    }


}
