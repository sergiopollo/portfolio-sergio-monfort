﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pj : MonoBehaviour
{
    public vidaSO vida;
    public float maxHp = 100;
    public float hp;
    public bool move = false;
    public float spd;
    // Start is called before the first frame update
    void Start()
    {
        this.hp = vida.hpPlayer;
        hp = maxHp;
        vida.hpPlayer = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-spd, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(spd, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            }

            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, spd);
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -spd);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            this.hp--;
            this.vida.hpPlayer--;
        }
    }


}
