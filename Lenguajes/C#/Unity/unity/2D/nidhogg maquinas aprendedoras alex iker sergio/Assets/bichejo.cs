using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using TMPro;
using Unity.MLAgents.Sensors;

public class bichejo : Agent
{

    public GameObject espada;
    public GameObject escudo;
    public delegate void ResetDelegate();
    public event ResetDelegate OnReset;
    public bool canDoAction = true;
    public int score=0;
    public int max = 0;
    public GameObject boss;
    /*public TextMeshProUGUI maxScoreText;
    public TextMeshProUGUI scoreText;
    */
    public GameObject maxScoreText;
    public GameObject scoreText;
    private Vector2 posac;

    public override void Initialize()
    {
        posac = espada.transform.localPosition;
        base.Initialize();
    }

    private void FixedUpdate()
    {
        if (canDoAction)
        {
            RequestDecision();
        }
    }


    public override void OnActionReceived(float[] vectorAction)
    {
        int accioVector = (int)Mathf.Floor(vectorAction[0]);
        //Debug.Log(accioVector);
        if (accioVector == 1)
        {
            attack();
        }else if (accioVector == 2)
        {
            deffend();
        }
        else
        {
            
        }
    }

    public override void OnEpisodeBegin()
    {
        
        canDoAction = true;
        score = 0;
        scoreText.GetComponent<TextMeshPro>().text = score + "";
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        OnReset?.Invoke();
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(boss.GetComponent<SpriteRenderer>().color.r);
        sensor.AddObservation(boss.GetComponent<SpriteRenderer>().color.g);
        sensor.AddObservation(boss.GetComponent<SpriteRenderer>().color.b);
    }

    /*public override void Heuristic(float[] actionsOut)
    {

        if (Input.GetKey("space"))
        {
            actionsOut[0] = 1;
        }
        else
        {
            actionsOut[0] = 0;
        }

    }*/

    private void attack()
    {
        if (canDoAction)
        {
            canDoAction = false;
            espada.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
            StartCoroutine(cooldownConObjeto(0.1f, espada));
            StartCoroutine(cooldown(1));

        }
    }

    private void deffend()
    {
        if (canDoAction)
        {
            escudo.SetActive(true);
            canDoAction = false;
            StartCoroutine(cooldownConObjeto(0.1f, escudo));
            StartCoroutine(cooldown(1));
        }
    }

    IEnumerator cooldownConObjeto(float seconds, GameObject objeto)
    {
        yield return new WaitForSeconds(seconds);
        if (objeto.name == "espada")
        {
            objeto.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            objeto.transform.localPosition = posac;
        }
        else
        {
            objeto.SetActive(false);
        }
    }

    IEnumerator cooldown(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        canDoAction = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.transform.tag == "enemy")
        {
            AddReward(-1f);
            //Debug.Log(GetCumulativeReward());
            if (score > max)
            {
                max = score;
                maxScoreText.GetComponent<TextMeshPro>().text = score + "";
            }
            EndEpisode();

        }
    }

    public void heChocadoConBossEnMiEspada()
    {
        //Debug.Log("choco con mi pana el boss");
        if(boss.GetComponent<SpriteRenderer>().color != Color.green)
        {      
        AddReward(0.1f);
        score++;
        scoreText.GetComponent<TextMeshPro>().text = score + "";
        }
        else
        {
            AddReward(-1f);
            //Debug.Log(GetCumulativeReward());
            if (score > max)
            {
                max = score;
                maxScoreText.GetComponent<TextMeshPro>().text = score + "";
            }
            EndEpisode();
        }
        //Debug.Log(GetCumulativeReward());
    }


}







