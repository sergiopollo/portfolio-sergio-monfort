using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossScript : MonoBehaviour
{

    public GameObject bala;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().color = Color.red;
        StartCoroutine(atacar());
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator atacar()
    {
        while (true)
        {

            //ESPERA UN TIEMPO ALEATORIO
            yield return new WaitForSeconds(Random.Range(3, 7));

            //Selecciona un ataque
            int ataque = Random.Range(1, 3);

            switch (ataque)
            {
                case 1:
                    //DISPARAR UNA BALA
                    yield return new WaitForSeconds(1f);
                    this.GetComponent<SpriteRenderer>().color = Color.blue;
                    Instantiate(bala);
                    bala.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y - 1);
                    yield return new WaitForSeconds(0.5f);
                    break;
                case 2:
                    //THORNS (Si le pegan durante este tiempo se tendrian que morir)
                    yield return new WaitForSeconds(1f);
                    this.GetComponent<SpriteRenderer>().color = Color.green;
                    yield return new WaitForSeconds(2f);
                    break;
            }
            //VUELVE A SU COLOR NORMAL
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
    }



}
