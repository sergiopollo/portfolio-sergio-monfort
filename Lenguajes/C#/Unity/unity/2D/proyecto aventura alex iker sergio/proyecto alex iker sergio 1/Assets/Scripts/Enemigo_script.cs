using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Animations;

public class Enemigo_script : MonoBehaviour
{

    public EnemigosSc tipo;
    public int dmg;
    public int hp;
    public int velocidad;
    private Animator m_animator;
    public BoxCollider2D Hurtbox;

    private Transform target;
    private int sentido = 1;

    public string state;

    public void setTarget(Transform t)
    {
        target = t;
    }
    public void setState(string st)
    {
        state = st;
    }

    // PONGO TODAS LAS VARIABLES RELACIONADAS CON EL SCRIPTABLEOBJECT
    void Start()
    {
        m_animator = gameObject.GetComponent<Animator>();
        m_animator.runtimeAnimatorController = tipo.a;
        this.transform.localScale = new Vector3(tipo.size, tipo.size, 0);
        this.dmg = tipo.dmg;
        this.hp = tipo.hp;
        this.GetComponent<SpriteRenderer>().sprite = tipo.sprite;
        Hurtbox.size = new Vector3(tipo.size, tipo.size, 0);


        if (tipo.volador)
        {
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        }
        else
        {
        m_animator.Play("Caminar");
        }
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(tipo.velocidad, 0);       

        state = "Patrullar";
        StartCoroutine("Comportamiento");
        this.transform.GetChild(0).GetComponent<CircleCollider2D>().radius = tipo.radio;
    }


    

    IEnumerator Comportamiento()
    {
        while (true)
        {
            //Debug.Log(this.GetComponentInChildren<CircleCollider2D>().radius);
            yield return new WaitForSeconds(1);
            //Debug.Log(this.GetComponentInChildren<CircleCollider2D>().radius);
            //Debug.Log(state);
            switch (state)
            {
                case "Pegar":
                    StartCoroutine("Pegar");
                    break;

                case "Patrullar":
                    if(!tipo.volador)
                       m_animator.Play("Caminar");
                    StartCoroutine("Moverse");
                    break;

                case "Seguir":
                    if (!tipo.volador)
                        m_animator.Play("Caminar");
                    StopCoroutine("Moverse");
                    Seguir();
                    break;
            }


        }
    }

    IEnumerator Pegar()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        if (tipo.volador)
        {
            GameObject roca = rocaPool.SharedInstance.GetPooledObject();
            if (roca != null)
            {
                roca.transform.position = new Vector3(this.gameObject.transform.position.x,this.gameObject.transform.position.y-1,0);
                roca.SetActive(true);
            }
        }
        else
        {
              m_animator.Play("Pegar", - 1, 0f);
        }
        
        yield return new WaitForSeconds(1);
        state = "Seguir";
    }

    private void Seguir()
    {
        if(target.position.x < this.transform.position.x)
        {
            this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(tipo.velocidad * -1, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(tipo.velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
        }
       
        if(target.position.x > this.transform.position.x-7 && target.position.x < this.transform.position.x + 7)
        {
            state = "Pegar";
        }
        //Debug.Log("Siguiendo");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
            //this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;     
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
      
    }

    public void getHurt()
    {
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
        this.hp--;
        if (this.hp <= 0)
        {
            this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            this.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }

    IEnumerator Moverse()
    {

            sentido *= -1;
            if(sentido ==1)
            {
                this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else
            {
                this.gameObject.transform.eulerAngles = new Vector3(0, 180, 0);
            }
            
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(tipo.velocidad * sentido, this.GetComponent<Rigidbody2D>().velocity.y);
            yield return new WaitForSeconds(2);
        
        
    }

}
