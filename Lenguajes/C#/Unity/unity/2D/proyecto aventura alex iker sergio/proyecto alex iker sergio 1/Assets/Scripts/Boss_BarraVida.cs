using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss_BarraVida : MonoBehaviour
{
    public Boss_script boss;
    public Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        boss.OnPerderVida += SetHealth;
        slider = this.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHealth(float vida)
    {

        slider.value = vida;
    }

}
