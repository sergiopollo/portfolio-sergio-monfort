using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roca_script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<BoxCollider2D>().size = new Vector2(1.5f, 1.5f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<ControlesEsqueleto>() != null)
            {
                collision.gameObject.GetComponent<ControlesEsqueleto>().getHurt();
            }

            if (collision.gameObject.GetComponent<ControlesBruja>() != null)
            {
                collision.gameObject.GetComponent<ControlesBruja>().getHurt();
            }

        }

        if(collision.gameObject.tag == "suelo")
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }
    }
}
