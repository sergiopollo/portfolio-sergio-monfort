using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saco_Script : MonoBehaviour
{

    public int vida_Saco;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.vida_Saco--;
        if(this.vida_Saco <= 0)
        {
            Destroy(this.gameObject);
        }
    }

}
