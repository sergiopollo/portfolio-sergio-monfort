using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraControl : MonoBehaviour
{
    public ControlesEsqueleto esqueleto;
    public ControlesBruja bruja;
    public Transform posicionesqueleto;
    public Transform posicionbruja;

    private bool esqueletomuerto = false, brujamuerta = false;
    // Start is called before the first frame update
    void Start()
    {
        esqueleto.OnPerderVida += Ifmorir;
        bruja.OnPerderVida += Ifmorir;
    }

    // Update is called once per frame
    void Update()
    {
        //print(Vector2.Distance(posicionbruja.position, posicionesqueleto.position));

        if (esqueletomuerto && !brujamuerta)
        {
            this.transform.position = new Vector3(posicionbruja.position.x, posicionbruja.position.y +10, -10);
        }
        if(brujamuerta && !esqueletomuerto)
        {
            this.transform.position = new Vector3(posicionesqueleto.position.x, posicionesqueleto.position.y +10, -10);
        }
        if (!esqueletomuerto && !brujamuerta)
        {
            this.GetComponent<Camera>().orthographicSize = Mathf.Clamp(Vector2.Distance(posicionbruja.position, posicionesqueleto.position) * 0.5f, 15f, 100f);

            this.transform.position = new Vector3((posicionesqueleto.position.x + posicionbruja.position.x) / 2, (posicionesqueleto.position.y + posicionbruja.position.y) / 2 + 10, -10);

        }
    }

    private void Ifmorir(float vida)
    {
        if(esqueleto.vida <= 0)
        {
            esqueletomuerto = true;
        }
        if(bruja.vida <= 0)
        {
            brujamuerta = true;
        }
    }

}
