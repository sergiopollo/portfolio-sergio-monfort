using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{


    [SerializeField]
    private Enemigo_script enemigo1;
    [SerializeField]
    private Enemigo_script enemigo2;
    [SerializeField]
    private ControlesEsqueleto jugadorEsqueleto;

    [SerializeField]
    private ControlesBruja jugadorBruja;

    [SerializeField]
    private Enemigo_script[] arrayEnemigos;

    [SerializeField]
    private Interruptor InterruptorBruja;
    [SerializeField]
    private Interruptor InterruptorEsqueleto;

    [SerializeField]
    private Interruptor interruptor;


    private bool interruptor1 = false;
    public bool interruptor2 = false;
    public bool interruptoresGenerados = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!interruptor2 && !interruptoresGenerados)
        {
            foreach (Enemigo_script es in arrayEnemigos)
            {
                if (es.hp <= 0)
                {
                    interruptor2 = true;
                }
                else
                {
                    interruptor2 = false;
                    break;
                }
            }
        }
        

        if (interruptor2)
        {
            Instantiate(InterruptorBruja);
            InterruptorBruja.transform.position = new Vector3(InterruptorBruja.transform.position.x, InterruptorBruja.transform.position.y, 0);
            Instantiate(InterruptorEsqueleto);
            InterruptorEsqueleto.transform.position = new Vector3(InterruptorEsqueleto.transform.position.x, InterruptorEsqueleto.transform.position.y, 0);
            interruptor2 = false;
            interruptoresGenerados = true;
        }

        if(enemigo1.hp <= 0 && enemigo2.hp <= 0 && !interruptor1){
            
           
                Instantiate(interruptor);
                interruptor1 = true;
        }

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
       

    }
}
