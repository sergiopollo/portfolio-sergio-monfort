using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Players_BarraVida : MonoBehaviour
{
    public ControlesEsqueleto esqueleto;
    public ControlesBruja bruja;
    public Slider slider;
    public bool barraEsqueleto;
    // Start is called before the first frame update
    void Start()
    {
        if (barraEsqueleto)
        {
            esqueleto = GameObject.Find("PSB_Character_Skeleton_No_Variation 1").GetComponent<ControlesEsqueleto>();
            esqueleto.OnPerderVida += SetHealth;
        }
        else
        {
            bruja = GameObject.Find("Bruja").GetComponent<ControlesBruja>();
            bruja.OnPerderVida += SetHealth;
        }

        slider = this.GetComponent<Slider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetHealth(float vida)
    {

        slider.value = vida;
    }
}
