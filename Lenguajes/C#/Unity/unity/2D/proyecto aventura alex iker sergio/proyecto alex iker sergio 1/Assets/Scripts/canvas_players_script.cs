using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class canvas_players_script : MonoBehaviour
{
    public ControlesEsqueleto esqueleto;
    public ControlesBruja bruja;
    public Scene escenaActual;
    public GameObject boss;
    // Start is called before the first frame update
    void Start()
    {
        escenaActual = SceneManager.GetActiveScene();
        esqueleto = GameObject.Find("PSB_Character_Skeleton_No_Variation 1").GetComponent<ControlesEsqueleto>();
       bruja = GameObject.Find("Bruja").GetComponent<ControlesBruja>();
        boss = GameObject.Find("Boss");
    }

    // Update is called once per frame
    void Update()
    {
        //esto habria que hacerlo con delegados, pero ahora mismo me da palo 
        if (esqueleto.vida <= 0 && bruja.vida <= 0)
        {
            gameObject.transform.GetChild(2).gameObject.SetActive(true);
        }

        //esto habria que hacerlo con delegados, pero ahora mismo me da palo 
        if (boss != null && boss.GetComponent<Boss_script>() != null && boss.GetComponent<Boss_script>().vida <= 0)
        {
            gameObject.transform.GetChild(3).gameObject.SetActive(true);
        }
        
    }

    public void onClick()
    {
        SceneManager.LoadScene(escenaActual.name);
    }

    public void onClickMenu()
    {
        SceneManager.LoadScene("SampleScene");
    }

}
