using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jugador : MonoBehaviour
{
    public bool tocaSuelo = false;


    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey("d"))
        {
           this.GetComponent<Rigidbody2D>().velocity = new Vector3(6, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-6, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        }

        if (Input.GetKeyUp("space"))
        {
            this.tocaSuelo = false;
        }


        if (Input.GetKey("space"))
        {
            if (tocaSuelo)
            {
                //para poder mantener el espacio y llegar mas alto
                //if (this.GetComponent<Rigidbody2D>().velocity.y >= 17.5)
                if (this.GetComponent<Rigidbody2D>().velocity.y >= 10)
                {
                    this.tocaSuelo = false;
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 20));
                }

            }

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag == "suelo")
        {
            this.tocaSuelo = true;
        }


        if (collision.transform.tag == "enemigo")
        {
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.transform.tag == "enemigo")
        {
            gameObject.SetActive(false);
        }
    }

}
