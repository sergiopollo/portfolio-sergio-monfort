using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class esqueleto_puño_script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigo")
        {
            if (collision.gameObject.GetComponent<Enemigo_script>() != null)
            {
                collision.gameObject.GetComponent<Enemigo_script>().getHurt();
            }
            else if (collision.gameObject.GetComponent<Boss_script>() != null)
            {
                collision.gameObject.GetComponent<Boss_script>().getHurt(1);
            }
            else if (collision.gameObject.GetComponent<Boss_hand_script>() != null)
            {
                collision.gameObject.GetComponent<Boss_hand_script>().boss.getHurt(1);
            }
        }
    }
}
