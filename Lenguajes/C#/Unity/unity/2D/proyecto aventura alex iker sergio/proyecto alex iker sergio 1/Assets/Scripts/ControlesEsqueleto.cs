using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesEsqueleto : MonoBehaviour
{
    public float vida;
    private Animator m_animator;
    public bool tocaSuelo = false;
    public CircleCollider2D hitbox;
    public BoxCollider2D hurtbox;
    public int jumpForce;
    private bool tooSoon;
    private string state = ".";
    public float mintime, maxtime;
    private bool puedesDisparar = true;
    public GameObject proyectil;
    public ProyectilScriptableObject tipoProyectil;
    private bool incombo;
    private bool andando;
    public delegate void PerderVida(float vida);
    public event PerderVida OnPerderVida;
    public bool invulnerable=false;

    // Start is called before the first frame update
    void Start()
    {
        m_animator = gameObject.GetComponent<Animator>();
        this.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
    }

    // Update is called once per frame
    void Update()
    {
       
        

        //movimiento
        if (Input.GetKey("d") )
        {        
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(10, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);

            if (!andando)
            {
                m_animator.Play("caminar", -1, 0f);
                andando = true;
            }
           
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-10, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, -180, 0);

            if (!andando)
            {
                m_animator.Play("caminar", -1, 0f);
                andando = true;
            }
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            if (andando && !incombo)
            {
                m_animator.Rebind();
                andando = false;
            }
            
        }
        ///////////////////////////////////////////////////////////////////////////
        if (Input.GetKeyDown("space"))
        {
            if (tocaSuelo)
            {
                StartCoroutine ("noSuelo");
                if(!incombo)
                m_animator.Play("miniSalto", -1, 0f);
                StartCoroutine("Saltar");
            }

        }
        ////////////////////////////////////////////////////////////////////////////

        //ataque basico
        if (Input.GetKeyDown("j"))
        {

            //print("te meto un ostion");
            //m_animator.SetBool("Atacando", true);
            StartCoroutine("Combo");
        }
        //else
        //{
        //    m_animator.SetBool("Atacando", false);
        //}


        ////////////////////////////////////////////////////////////////////////////
        if (Input.GetKey(KeyCode.K) && puedesDisparar)
        {
            GameObject newProyectil = Instantiate(proyectil);
            newProyectil.GetComponent<proyectilBase>().tipo = this.tipoProyectil;
            newProyectil.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 3, this.transform.position.z);
            newProyectil.transform.rotation = this.transform.rotation;
            newProyectil.GetComponent<proyectilBase>().sentido = this.transform.right;
            //torque: para ponerle movimiento angular
            int sentidoTorque = -1;
            if (this.transform.right.x < 0)
            {
                sentidoTorque = 1;
            }
            newProyectil.GetComponent<Rigidbody2D>().AddTorque(sentidoTorque*1000f);
            puedesDisparar = false;
            StartCoroutine("disparar");
        }
    }

    IEnumerator Combo()
    {
        //print(tooSoon);
        if (!tooSoon)
        {

            switch (state)
            {
                case ".":
                    m_animator.Play("Golpe1", -1, 0f);

                    state = "A";
                   // Debug.Log("A");
                    break;
                case "A":

                    m_animator.Play("Golpe2", -1, 0f);
                    state = "AA";
                    //Debug.Log("AA");
                    break;
                case "AA":
                    m_animator.Play("Golpe3", -1, 0f);
                    state = ".";
                    //Debug.Log("AAA");
                    break;
            }



            string copystate = state;
            tooSoon = true;
            incombo = true;
            yield return new WaitForSeconds(mintime);
            // Debug.Log("Tiempo esperado");
            incombo = false;
            tooSoon = false;

            yield return new WaitForSeconds(maxtime - mintime);
            if (state == copystate && state != ".")
            {
                //print("Combo stopped at " + state + " Boss!");
                state = ".";
            }
        }
        else
        {
          //  Debug.Log("Don't push that fast Boss...");
        }

    }

    IEnumerator noSuelo()
    {
        yield return new WaitForSeconds(0.05f);
        this.tocaSuelo = false;
    }
    IEnumerator Saltar()
    {
        
        //Debug.Log("Starting jump Boss!!!");

        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce*4));
        int salt = 0;
        while (Input.GetKey("space"))
        {
            if(salt < 5)
            {
               // Debug.Log("Still addig force Boss!!!");
                salt++;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            }
            yield return new WaitForSeconds(0.05f);

        }
    }

    IEnumerator disparar()
    {

        yield return new WaitForSeconds(1f);
        puedesDisparar = true;

    }
    public void getHurt()
    {
        if (!invulnerable)
        {
            StartCoroutine(invulnerabilidad());
            this.vida--;
            OnPerderVida?.Invoke(vida);
            if (this.vida <= 0)
            {
                this.gameObject.SetActive(false);
            }
        }
        
    }

    IEnumerator invulnerabilidad()
    {
        invulnerable = true;
        yield return new WaitForSeconds(2f);
        invulnerable = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag == "suelo")
        {
            this.tocaSuelo = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        
        if (collision.transform.tag == "suelo")
        {
            this.tocaSuelo = true;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
    }

}
