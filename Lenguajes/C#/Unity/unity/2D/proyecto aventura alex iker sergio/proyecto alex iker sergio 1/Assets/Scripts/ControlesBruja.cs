using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlesBruja : MonoBehaviour
{
    public float vida;
    private Animator m_animator;
    public bool tocaSuelo = false;
    public BoxCollider2D hurtbox;
    public int jumpForce;
    private bool puedesDisparar = true;
    private bool puedesDispararViento = true;
    public GameObject vientoEffector;
    public GameObject proyectil;
    public ProyectilScriptableObject tipoProyectil;
    public float maxCharge;
    public bool cargandoAtaque;
    public bool andando = false;
    public delegate void PerderVida(float vida);
    public event PerderVida OnPerderVida;
    bool invulnerable = false;

    // Start is called before the first frame update
    void Start()
    {
        m_animator = gameObject.GetComponent<Animator>();
        this.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        this.maxCharge = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        //movimiento
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (!andando)
            {
                m_animator.Play("Caminar");
                andando = true;
            }
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(10, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (!andando)
            {
                m_animator.Play("Caminar");
                andando = true;
            }
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(-10, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            this.gameObject.transform.eulerAngles = new Vector3(0, -180, 0);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y, 0);
            if (andando)
            {
                m_animator.Rebind();
                andando = false;
            }
        }
        ///////////////////////////////////////////////////////////////////////////
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (tocaSuelo)
            {
                StartCoroutine("noSuelo");
                m_animator.Play("Saltar");
                StartCoroutine("Saltar");
            }

        }
        ////////////////////////////////////////////////////////////////////////////

        //ataque viento
        if (Input.GetKey(KeyCode.Keypad1) && puedesDispararViento)
        {
            //print("te lanzo viento");
            GameObject newViento = Instantiate(vientoEffector);
            newViento.transform.rotation = this.transform.rotation;
            if (this.transform.right.x > 0)
            {
                newViento.transform.position = new Vector3(this.transform.position.x+10, this.transform.position.y + 3, this.transform.position.z);
                
            }
            else
            {
                newViento.transform.position = new Vector3(this.transform.position.x-10, this.transform.position.y + 3, this.transform.position.z);
                newViento.gameObject.GetComponent<AreaEffector2D>().forceAngle *= -1;
                newViento.gameObject.GetComponent<AreaEffector2D>().forceMagnitude *= -1;
            }
            newViento.transform.localScale = new Vector2(5,5);
            this.puedesDispararViento = false;
            StartCoroutine("dispararViento");
            StartCoroutine("destruyeViento", newViento);
        }


        /*
        if (Input.GetKey(KeyCode.Keypad2) && puedesDisparar)
        {
            cargandoAtaque = false;
            GameObject newProyectil = Instantiate(proyectil);
            newProyectil.GetComponent<proyectilBase>().tipo = this.tipoProyectil;
            newProyectil.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 3, this.transform.position.z);
            newProyectil.transform.rotation = this.transform.rotation;
            newProyectil.GetComponent<proyectilBase>().sentido = this.transform.right;
            puedesDisparar = false;
            StartCoroutine("disparar");
        }
        */


        //bola de fuego (se puede mantener)
        if (Input.GetKey(KeyCode.Keypad2) && puedesDisparar && !cargandoAtaque)
        {
            this.cargandoAtaque = true;
           // print("estoy manteniendo");
            StartCoroutine("cargaAtaque", KeyCode.Keypad2);
        }
    }

    IEnumerator noSuelo()
    {
        yield return new WaitForSeconds(0.05f);
        this.tocaSuelo = false;
    }
    IEnumerator Saltar()
    {

        //Debug.Log("Starting jump Boss!!!");

        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce * 4));
        int salt = 0;
        while (Input.GetKey(KeyCode.UpArrow))
        {
            if (salt < 5)
            {
               // Debug.Log("Still addig force Boss!!!");
                salt++;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            }
            yield return new WaitForSeconds(0.05f);

        }
    }
    IEnumerator disparar()
    {
        StopCoroutine("cargaAtaque");
        yield return new WaitForSeconds(1f);
        puedesDisparar = true;

    }

    IEnumerator dispararViento()
    {
        yield return new WaitForSeconds(1f);
        puedesDispararViento = true;

    }

    IEnumerator destruyeViento(GameObject newViento)
    {
        yield return new WaitForSeconds(3f);
        Destroy(newViento);
    }

    IEnumerator cargaAtaque(KeyCode kc)
    {
       // state = chargingState + "->";
       // print("cargant atac " + "asd" + "...");
        float cargaAtac = -0.01f;
        while (Input.GetKey(kc))
        {
            if (cargaAtac < maxCharge) cargaAtac+=0.05f;
            yield return new WaitForSeconds(0.1f);
        }

        //fer coses
        GameObject newProyectil = Instantiate(proyectil);
        newProyectil.GetComponent<proyectilBase>().tipo = this.tipoProyectil;
        newProyectil.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 3, this.transform.position.z);
        newProyectil.transform.rotation = this.transform.rotation;
        newProyectil.GetComponent<proyectilBase>().sentido = this.transform.right;
        newProyectil.GetComponent<proyectilBase>().cargado = true;
        newProyectil.GetComponent<proyectilBase>().carga = cargaAtac;
        cargandoAtaque = false;
        puedesDisparar = false;
        StartCoroutine("disparar");
        //print("he disparat un atac " + "asd" + " amb potencia " + cargaAtac);
        //state = "-";

    }

    public void getHurt()
    {
        if (!invulnerable)
        {
            StartCoroutine(invulnerabilidad());
            this.vida--;
            OnPerderVida?.Invoke(vida);
            if (this.vida <= 0)
            {
                this.gameObject.SetActive(false);
            }
        }
    }
    IEnumerator invulnerabilidad()
    {
        invulnerable = true;
        yield return new WaitForSeconds(2f);
        invulnerable = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag == "suelo")
        {
            this.tocaSuelo = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

        if (collision.transform.tag == "suelo")
        {
            this.tocaSuelo = true;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    }

}
