using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampolin_Right : MonoBehaviour
{
    public float launchforce;

    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.up * launchforce + Vector2.right * launchforce;
        }
    }
}
