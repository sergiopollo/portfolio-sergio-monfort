using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton : MonoBehaviour
{
    private bool flag = false;
    [SerializeField]
    private GameObject AirCannon;

    [SerializeField]
    private GameObject ColumnaAire1;
    [SerializeField]
    private GameObject ColumnaAire2;

    [SerializeField]
    private GameObject NewSuelo;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (!flag)
            {
                Instantiate(AirCannon);
                Instantiate(ColumnaAire1);
                Instantiate(ColumnaAire2);
                Instantiate(NewSuelo);
                Destroy(GameObject.Find("ParedQueMata"));
                flag = true;
            }
            this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
        }

    }
}
