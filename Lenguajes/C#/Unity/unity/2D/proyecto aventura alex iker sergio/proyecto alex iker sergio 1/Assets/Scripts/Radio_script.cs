using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio_script : MonoBehaviour
{

    private GameObject target = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && target == null)
        {
            //Debug.Log("Detectado");
            GetComponentInParent<Enemigo_script>().setState("Seguir");
            target = collision.gameObject;
            GetComponentInParent<Enemigo_script>().setTarget(collision.transform);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == target)
        {
            GetComponentInParent<Enemigo_script>().setState("Patrullar");
            target = null;
        }
            
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == target)
        {
            GetComponentInParent<Enemigo_script>().setTarget(collision.transform);
        }
        if( target == null && collision.gameObject.tag == "Player")
        {
            GetComponentInParent<Enemigo_script>().setState("Seguir");
            target = collision.gameObject;
            GetComponentInParent<Enemigo_script>().setTarget(collision.transform);
        }
            
    }

}
