using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/proyectilScriptableObject", order = 1)]
public class ProyectilScriptableObject : ScriptableObject
{
    public float size;
    public float dmg;
    public Sprite sprite;
    public GameObject ps;
    public PhysicsMaterial2D material;
    public float TiempoDeVida;
    public float FuerzaVertical;
    public Vector2 hitboxSize;
}
