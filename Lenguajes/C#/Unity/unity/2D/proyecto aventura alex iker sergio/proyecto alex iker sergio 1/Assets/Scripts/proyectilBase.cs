using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilBase : MonoBehaviour
{
    public Vector3 sentido;
    public float dmg;
    public ProyectilScriptableObject tipo;
    public bool cargado;
    public float carga;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = tipo.sprite;
        this.dmg = tipo.dmg;
        if (!cargado)
        {
            this.transform.localScale = new Vector3(tipo.size, tipo.size, 0);
            this.GetComponent<BoxCollider2D>().size = tipo.hitboxSize;
            this.GetComponent<CircleCollider2D>().radius = 4;
        }
        else
        {
            this.transform.localScale = new Vector3(tipo.size+carga, tipo.size+carga, 0);
            //CAMBIAR ATAMA�O HITBOX SI NO HAY PROBLEMAS
            this.GetComponent<BoxCollider2D>().size = tipo.hitboxSize;
            this.GetComponent<CircleCollider2D>().radius = 4+carga;
        }
        //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 999));
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, tipo.FuerzaVertical));
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(sentido.x*20, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        this.GetComponent<Rigidbody2D>().sharedMaterial = tipo.material;
        if (tipo.ps != null)
        {
            GameObject newps = Instantiate(tipo.ps);
            newps.transform.position = this.transform.position;
            newps.transform.rotation = this.transform.rotation;
            newps.transform.localScale = this.transform.localScale;
            newps.transform.SetParent(this.transform);
        }
        StartCoroutine("desaparecer", tipo.TiempoDeVida);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator desaparecer(float tiempoDeVida)
    {
        yield return new WaitForSeconds(tiempoDeVida);
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            if (collision.gameObject.GetComponent<Enemigo_script>() != null)
            {
                Destroy(this.gameObject);
                collision.gameObject.GetComponent<Enemigo_script>().getHurt();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            if (collision.gameObject.GetComponent<Enemigo_script>() != null)
            {
                Destroy(this.gameObject);
                collision.gameObject.GetComponent<Enemigo_script>().getHurt();
            }
        }
    }

}
