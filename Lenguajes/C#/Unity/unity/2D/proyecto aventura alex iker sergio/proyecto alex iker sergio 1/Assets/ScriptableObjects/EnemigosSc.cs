using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Enemigo", order = 1)]

public class EnemigosSc : ScriptableObject
{
    public int hp;
    public bool volador;
    public float size;
    public int dmg;
    public Sprite sprite;
    public int velocidad;
    public AnimatorController a;
    public int radio;


}
