using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_hand_script : MonoBehaviour
{
    //private Boss_script boss = GetComponentInParent<Boss_script>();
    public Boss_script boss;
    public int state;
    public bool atacando=false;
    public bool manoDerecha;
    public bool CR_Ataque2_Running = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ataques");
        boss.OnAtacar += inicioAtaques;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.GetComponent<ControlesBruja>() != null)
            {
                print("me choco con bruha");
                collision.gameObject.GetComponent<ControlesBruja>().getHurt();
            }else if (collision.gameObject.GetComponent<ControlesEsqueleto>() != null)
            {
                print("me choco con esqueleto");
                collision.gameObject.GetComponent<ControlesEsqueleto>().getHurt();
            }
        }

        if (collision.gameObject.tag == "Proyectil")
        {
            //print("asdasdasdad");
            boss.vida -= collision.gameObject.GetComponent<proyectilBase>().dmg;
            //print(this.boss.vida);
            //mas adelante usar una pool
            Destroy(collision.gameObject);
        }
    }

    public void inicioAtaques(int numAtaque)
    {
        //print("inicioataques " + this.name + " "+numAtaque);
        state = numAtaque;
    }

    IEnumerator ataques()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            //print(state+"//"+boss.ataqueAcabado+"//"+this.atacando);
            if (!boss.ataqueAcabado && !this.atacando)
            {
                //print("ataques " + this.name + " " + state);
                switch (state)
                {
                    case 1:
                        this.atacando = true;
                        StartCoroutine(ataque1());
                        //print("empiezo ataque 1");
                        break;
                    case 2:
                        this.atacando = true;
                        StartCoroutine(ataque2());
                        //print("empiezo ataque 2");
                        break;
                    case 3:
                        this.atacando = true;
                        StartCoroutine(ataque3());
                        //print("empiezo ataque 3");
                        break;
                    default:
                        print("nada");
                        this.atacando = false;
                        boss.ataqueAcabado = true;
                        break;
                }
                
            }
        }
    }

    IEnumerator ataque1()
    {
        float distancia = 4;
        if (!manoDerecha) { distancia *= -1; }
        

        Vector2 posicionInicial = this.transform.position;
        float alturaMax = 8;
        float alturaMin = -3;

        //sube
        float duration = 0.3f;
        Vector2 positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);
        ///////////////////////////////////////////////////////

        //se acerca al medio del boss
        duration = 2;
        positionToMoveTo = new Vector2(boss.transform.position.x+distancia, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration+0.01f);

            //ALERTA SHAKE hace la alerta de que va a bajar (shake)
            duration = 1;
            StartCoroutine(Shake(this.transform.position, duration));
            yield return new WaitForSeconds(duration + 0.01f*5);

            //cae al suelo
            duration = 0.1f;
            positionToMoveTo = new Vector2(boss.transform.position.x + distancia, alturaMin);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            //vuelve a subir
            duration = 0.1f;
            positionToMoveTo = new Vector2(boss.transform.position.x + distancia, alturaMax);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

        //se mueve a la segunda posicion
        distancia = distancia *3;
        duration = 1;
        positionToMoveTo = new Vector2(boss.transform.position.x + distancia, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);

            //ALERTA SHAKE hace la alerta de que va a bajar (shake)
            duration = 1;
            StartCoroutine(Shake(this.transform.position, duration));
            yield return new WaitForSeconds(duration + 0.01f * 5);

            //cae al suelo
            duration = 0.1f;
            positionToMoveTo = new Vector2(boss.transform.position.x + distancia, alturaMin);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            //vuelve a subir
            duration = 0.1f;
            positionToMoveTo = new Vector2(boss.transform.position.x + distancia, alturaMax);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

        //se mueve a la tercera posicion (posicionInicial)
        distancia = distancia * 2;
        duration = 1;
        positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);

            //ALERTA SHAKE hace la alerta de que va a bajar (shake)
            duration = 1;
            StartCoroutine(Shake(this.transform.position, duration));
            yield return new WaitForSeconds(duration + 0.01f * 5);

            //cae al suelo
            duration = 0.1f;
            positionToMoveTo = new Vector2(posicionInicial.x, alturaMin);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);



        //////////////////////////////////////////////////
        //no es necesario que vuelva ala pos inicial porque ya esta ahi
        /*
        //vuelve a la posicion inicial
        duration = 2;
        positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);

        //baja
        duration = 0.3f;
        positionToMoveTo = posicionInicial;
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);
        */
        this.atacando = false;
        boss.ataqueAcabado = true;

    }

    IEnumerator ataque2()
    {
        CR_Ataque2_Running = true;
        float distancia = 4;
        int numVeces=50;
        if (!manoDerecha) {
            distancia *= -1;
            yield return new WaitForSeconds(0.2f);
        }


        Vector2 posicionInicial = this.transform.position;
        float alturaMax = 3;
        float alturaMin = -3;

        for (int i = 0; i < numVeces; i++)
        {
            //sube
            float duration = 0.1f;
            Vector2 positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            yield return new WaitForSeconds(0.1f);

            //cae al suelo
            duration = 0.1f;
            positionToMoveTo = new Vector2(posicionInicial.x, alturaMin);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            //para que no spawnee demasiadas rocas
            int randomSpawn = Random.Range(-25, 50);
            if (randomSpawn < 0)
            {
                GameObject roca = rocaPool.SharedInstance.GetPooledObject();
                if (roca != null)
                {
                    int randomPosition = Random.Range(-25, 25);
                    roca.SetActive(true);
                    roca.transform.position = new Vector2(randomPosition, 15);
                }
            }
        }
        //////////////////////////////////////////////////
        //no es necesario que vuelva ala pos inicial porque ya esta ahi
        /*
        //vuelve a la posicion inicial
        duration = 2;
        positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);

        //baja
        duration = 0.3f;
        positionToMoveTo = posicionInicial;
        StartCoroutine(LerpPosition(positionToMoveTo, duration));
        yield return new WaitForSeconds(duration + 0.01f);
        */
        this.atacando = false;
        boss.ataqueAcabado = true;
        CR_Ataque2_Running = false;
    }

    IEnumerator ataque3()
    {
        GameObject target;
        int numVeces = 3;

        Vector2 posicionInicial = this.transform.position;
        float alturaMax = 8;
        float alturaMin = -3;
        float duration;
        Vector2 positionToMoveTo;

        if (!manoDerecha) 
        { 
            target = GameObject.Find("PSB_Character_Skeleton_No_Variation 1");
        }
        else
        {
            target = GameObject.Find("Bruja");
        }


        for (int i = 0; i < numVeces; i++)
        {
            //si ha muerto su target que pare este ataque y haga el ataque 2 (el de lanzar piedras)
            if ((target == null || target.activeSelf == false))
            {
                //vuelve a la posicion inicial
                duration = 2;
                positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                StartCoroutine(LerpPosition(positionToMoveTo, duration));
                yield return new WaitForSeconds(duration + 0.01f);

                //baja
                duration = 0.3f;
                positionToMoveTo = posicionInicial;
                StartCoroutine(LerpPosition(positionToMoveTo, duration));
                yield return new WaitForSeconds(duration + 0.01f);
                
                if (!CR_Ataque2_Running)
                {
                    StartCoroutine(ataque2());
                }
                
                StopCoroutine(ataque3());
            }
            else
            {
                if ((target == null || target.activeSelf == false))
                {
                    //vuelve a la posicion inicial
                    duration = 2;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    //baja
                    duration = 0.3f;
                    positionToMoveTo = posicionInicial;
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    if (!CR_Ataque2_Running)
                    {
                        StartCoroutine(ataque2());
                    }
                    StopCoroutine(ataque3());
                }
                else
                {


                    //sube
                    duration = 0.3f;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);
                    ///////////////////////////////////////////////////////
                }

                if ((target == null || target.activeSelf == false))
                {
                    //vuelve a la posicion inicial
                    duration = 2;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    //baja
                    duration = 0.3f;
                    positionToMoveTo = posicionInicial;
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    if (!CR_Ataque2_Running)
                    {
                        StartCoroutine(ataque2());
                    }
                    StopCoroutine(ataque3());
                }
                else
                {


                    //se pone sobre el jugador
                    duration = 2;
                    positionToMoveTo = new Vector2(target.transform.position.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);
                }

                if ((target == null || target.activeSelf == false))
                {
                    //vuelve a la posicion inicial
                    duration = 2;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    //baja
                    duration = 0.3f;
                    positionToMoveTo = posicionInicial;
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    if (!CR_Ataque2_Running)
                    {
                        StartCoroutine(ataque2());
                    }
                    StopCoroutine(ataque3());
                }
                else
                {


                    //ALERTA SHAKE hace la alerta de que va a bajar (shake)
                    duration = 1;
                    StartCoroutine(Shake(this.transform.position, duration));
                    yield return new WaitForSeconds(duration + 0.01f * 5);
                }

                if ((target == null || target.activeSelf == false))
                {
                    //vuelve a la posicion inicial
                    duration = 2;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    //baja
                    duration = 0.3f;
                    positionToMoveTo = posicionInicial;
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    if (!CR_Ataque2_Running)
                    {
                        StartCoroutine(ataque2());
                    }
                    StopCoroutine(ataque3());
                }
                else
                {


                    //cae al suelo
                    duration = 0.1f;
                    positionToMoveTo = new Vector2(this.transform.position.x, alturaMin);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);
                }

                if ((target == null || target.activeSelf == false))
                {
                    //vuelve a la posicion inicial
                    duration = 2;
                    positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    //baja
                    duration = 0.3f;
                    positionToMoveTo = posicionInicial;
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);

                    if (!CR_Ataque2_Running)
                    {
                        StartCoroutine(ataque2());
                    }
                    StopCoroutine(ataque3());
                }
                else
                {


                    //vuelve a subir
                    duration = 0.1f;
                    positionToMoveTo = new Vector2(this.transform.position.x, alturaMax);
                    StartCoroutine(LerpPosition(positionToMoveTo, duration));
                    yield return new WaitForSeconds(duration + 0.01f);
                }
            }
        }

        
        //////////////////////////////////////////////////
        //si no estaba haciendo ataque2 que pare 
        if (target != null && target.activeSelf != false)
        {
            //vuelve a la posicion inicial
            duration = 2;
            positionToMoveTo = new Vector2(posicionInicial.x, alturaMax);
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            //baja
            duration = 0.3f;
            positionToMoveTo = posicionInicial;
            StartCoroutine(LerpPosition(positionToMoveTo, duration));
            yield return new WaitForSeconds(duration + 0.01f);

            this.atacando = false;
            boss.ataqueAcabado = true;
        }
    }

    IEnumerator Shake(Vector2 position, float duration)
    {
        int numVeces=5;
        for (int i=0; i<numVeces; i++)
        {
            float durationLerp = (duration/ numVeces)/2;
            //print(durationLerp);
            Vector2 positionToMoveTo = new Vector2(position.x-0.5f, position.y);
            StartCoroutine(LerpPosition(positionToMoveTo, durationLerp));
            yield return new WaitForSeconds(durationLerp + 0.01f);

            durationLerp = (duration / numVeces)/2;
            positionToMoveTo = new Vector2(position.x + 0.5f, position.y);
            StartCoroutine(LerpPosition(positionToMoveTo, durationLerp));
            yield return new WaitForSeconds(durationLerp + 0.01f);
        }
    }

    IEnumerator LerpPosition(Vector2 target, float duration)
    {
        //print("a");
        float time = 0;
        Vector2 startPosition = transform.position;

        while (time < duration)
        {
            transform.position = Vector2.Lerp(startPosition, target, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target;
    }

    IEnumerator LerpPositionTarget(Transform target, float duration)
    {
        //print("a");
        float time = 0;
        Vector2 startPosition = transform.position;

        while (time < duration)
        {
            transform.position = Vector2.Lerp(startPosition, target.position, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        transform.position = target.position;
    }


}
