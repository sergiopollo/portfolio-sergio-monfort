using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muerte : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (collision.gameObject.name == "Bruja")
            {
                collision.gameObject.GetComponent<ControlesBruja>().getHurt();
            }
            else if (collision.gameObject.name == "PSB_Character_Skeleton_No_Variation 1")
            {
                collision.gameObject.GetComponent<ControlesEsqueleto>().getHurt();
            }
        }
    }
}
