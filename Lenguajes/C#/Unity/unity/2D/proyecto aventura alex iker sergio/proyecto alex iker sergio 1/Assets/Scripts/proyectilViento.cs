using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilViento : MonoBehaviour
{
    public Vector3 sentido;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(sentido.x * 20, this.GetComponent<Rigidbody2D>().velocity.y, 0);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
