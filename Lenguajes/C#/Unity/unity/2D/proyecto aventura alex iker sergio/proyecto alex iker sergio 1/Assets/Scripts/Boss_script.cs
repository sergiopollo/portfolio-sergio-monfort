using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_script : MonoBehaviour
{
    public float vida;
    public bool ataqueAcabado=true;
    public delegate void Atacar(int numAtaque);
    public event Atacar OnAtacar;
    public delegate void PerderVida(float vida);
    public event PerderVida OnPerderVida;
    public GameObject video;
    public GameObject musica1, musica2;

    // Start is called before the first frame update
    void Start()
    {
        vida = 100;
        StartCoroutine("ataques");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Proyectil")
        {
            getHurt(collision.gameObject.GetComponent<proyectilBase>().dmg);
            StartCoroutine("desapareceProyectil", collision.gameObject);
        }


    }

    public void getHurt(float dmg)
    {
        this.vida -= dmg;
        OnPerderVida?.Invoke(vida);
        //print(this.vida);
        if (this.vida <= 0)
        {
            //lo que sea que tenga que hacer cuando muere
            Boss_hand_script[] hijos = this.GetComponentsInChildren<Boss_hand_script>();
            foreach (Boss_hand_script hijo in hijos)
            {
                hijo.StopAllCoroutines();
                hijo.gameObject.SetActive(false);
            }

            video.gameObject.SetActive(true);
            musica2.gameObject.SetActive(true);
            musica1.gameObject.SetActive(false);
            this.gameObject.SetActive(false);
        }
    }

    IEnumerator desapareceProyectil(GameObject proyectil)
    {
        yield return new WaitForSeconds(0.05f);
        
        //mas adelante usar una pool
        Destroy(proyectil);
    }

    IEnumerator ataques()
    {
        int random = 0;
        int randomAnterior = -1;
        while (true)
        {
            yield return new WaitForSeconds(0f);
            /*
            if (ataqueAcabado && vida > 0)
            {
                Boss_hand_script[] hijos = this.GetComponentsInChildren<Boss_hand_script>();
                int contador = 0;
                foreach (Boss_hand_script hijo in hijos)
                {
                    if (hijo.atacando)
                    {
                        contador++;
                    }
                }

                if (contador == 0)
                {
                    random = Random.Range(1, 4);
                    print("random: " + random);
                    print("randomAnterior: " + randomAnterior);
                    //para que no repita ataques (no funciona bien del todo? es raro)
                    if (random != randomAnterior)
                    {
                        randomAnterior = random;
                        print("random: " + random);
                        print("randomAnterior: " + randomAnterior);
                        ataqueAcabado = false;
                        //si alguna mano esta haciendo el ataque 2 que justo ha empezado por morir un jugador, si entra aqui la otra mano, que la que este en el at 2 pare.
                        foreach (Boss_hand_script hijo in hijos)
                        {
                            if (hijo.CR_Ataque2_Running)
                            {
                                hijo.StopCoroutine("ataque2");
                                //hijo.StopAllCoroutines();
                                print("corrutina ataque 2 de " + hijo + " parada");
                            }
                            //hijo.StopAllCoroutines();
                        }
                        OnAtacar?.Invoke(random);
                        print("iniciando ataque " + random);
                    }

                    /*
                    //pruebas
                    random = 3;
                    ataqueAcabado = false;
                    OnAtacar?.Invoke(random);
                    print("iniciando ataque " + random);
                    
                }
                else
                {
                    this.ataqueAcabado = false;
                }
                

            }
            */
            if (ataqueAcabado && vida > 0)
            {
               

                
                    random = Random.Range(1, 4);
                    print("random: " + random);
                    print("randomAnterior: " + randomAnterior);
                    //para que no repita ataques (no funciona bien del todo? es raro)
                    if (random != randomAnterior)
                    {
                        randomAnterior = random;
                        print("random: " + random);
                        print("randomAnterior: " + randomAnterior);
                        ataqueAcabado = false;
                    //si alguna mano esta haciendo el ataque 2 que justo ha empezado por morir un jugador, si entra aqui la otra mano, que la que este en el at 2 pare.
                    Boss_hand_script[] hijos = this.GetComponentsInChildren<Boss_hand_script>();
                    foreach (Boss_hand_script hijo in hijos)
                        {
                            if (hijo.CR_Ataque2_Running)
                            {
                                hijo.StopCoroutine("ataque2");
                                //hijo.StopAllCoroutines();
                                print("corrutina ataque 2 de " + hijo + " parada");
                            }
                            //hijo.StopAllCoroutines();
                        }
                        OnAtacar?.Invoke(random);
                        print("iniciando ataque " + random);
                    }

                    /*
                    //pruebas
                    random = 3;
                    ataqueAcabado = false;
                    OnAtacar?.Invoke(random);
                    print("iniciando ataque " + random);
                    */
                
                

            }



        }
    }
}
