using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pruebaEventoLuzListener : MonoBehaviour
{

    private bool luzApagada = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void cambioLuz()
    {
        Debug.Log("evento");
        if (luzApagada)
        {
            luzApagada = false;
            this.transform.eulerAngles = new Vector3(45, 0,0);
        }
        else
        {
            luzApagada = true;
            this.transform.eulerAngles = new Vector3(200, 0, 0);
        }
    }
}
