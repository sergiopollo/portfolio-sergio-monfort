using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class armaRaycastCutre : MonoBehaviour
{
    public Camera cameraBrain;
    public float rango;
    public float dispersion;
    public LayerMask mask;

    public LineRenderer lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            DisparaRaycast();
        }
    }

    public void DisparaRaycast()
    {
        RaycastHit hit;

        float dispersionX = Random.Range(-dispersion, dispersion);
        float dispersionY = Random.Range(-dispersion, dispersion);
        float dispersionZ = Random.Range(-dispersion, dispersion);
        //para linerenderer
        Vector3 finalLinea = new Vector3(this.transform.position.x + (cameraBrain.transform.forward.x + dispersionX) * rango, this.transform.position.y + (cameraBrain.transform.forward.y + dispersionY) * rango, this.transform.position.z + (cameraBrain.transform.forward.z + dispersionZ) * rango);

        Debug.DrawRay(this.transform.position, new Vector3(cameraBrain.transform.forward.x + dispersionX, cameraBrain.transform.forward.y + dispersionY, cameraBrain.transform.forward.z + dispersionZ) * rango, Color.blue, 2f);

        if (Physics.Raycast(this.transform.position, new Vector3(cameraBrain.transform.forward.x + dispersionX, cameraBrain.transform.forward.y + dispersionY, cameraBrain.transform.forward.z + dispersionZ) * rango, out hit, rango, mask))
        {
            GameObject GOHit = hit.transform.gameObject;

           

            Debug.Log("estoy chocando con " + GOHit + " con tag " + GOHit.tag);


        }
        //para linerenderer
        Vector3[] positions = new Vector3[]
        {
            this.transform.position,
            finalLinea
        };

        lineRenderer.SetPositions(positions);
    }


}
