using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Estados_Enemigo
{
   
    Shoot,
    TakeCover,
    Move,
    Search,
    Recharge,
}
