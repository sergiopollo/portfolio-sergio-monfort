using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAbasica : MonoBehaviour
{

    public Transform Objective;
    // Start is called before the first frame update
    void Start()
    {
        Objective = GameObject.Find("player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<NavMeshAgent>().SetDestination(Objective.position);
    }
}
