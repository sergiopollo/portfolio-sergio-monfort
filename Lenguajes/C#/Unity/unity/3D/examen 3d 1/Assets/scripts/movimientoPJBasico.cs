using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class movimientoPJBasico : MonoBehaviour
{
    //vida y respawnPosition

    public float maxVida = 20;
    public float vida;
    private Vector3 posicionInicial;

    public float speed = 5.0f;
    public float sprintSpeed = 10.0f;
    private CharacterController characterController;
    private float sensitivityX = 2F;

    /*
    private float gravity = -9.81f;
    [Tooltip("Move speed of the character in m/s")]
    private float MoveSpeed = 4.0f;
    [Tooltip("Sprint speed of the character in m/s")]
    private float SprintSpeed = 6.0f;
    */
    [Tooltip("Rotation speed of the character")]
    private float RotationSpeed = 1.0f;
    [Tooltip("Acceleration and deceleration")]
    private float SpeedChangeRate = 10.0f;

    [Space(10)]
    [Tooltip("The height the player can jump")]
    public float JumpHeight = 3f;
    [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
    private float Gravity = -15.0f;

    [Space(10)]
    [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
    private float JumpTimeout = 0.1f;
    [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
    private float FallTimeout = 0.15f;

    [Header("Player Grounded")]
    [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
    private bool Grounded = true;
    [Tooltip("Useful for rough ground")]
    private float GroundedOffset = -0.14f;
    [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
    private float GroundedRadius = 0.5f;
    [Tooltip("What layers the character uses as ground")]
    private LayerMask GroundLayers;

    [Header("Cinemachine")]
    [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
    private GameObject CinemachineCameraTarget;
    [Tooltip("How far in degrees can you move the camera up")]
    private float TopClamp = 90.0f;
    [Tooltip("How far in degrees can you move the camera down")]
    private float BottomClamp = -90.0f;


    // player
    private float _speed;
    private float _rotationVelocity;
    private float _verticalVelocity;
    private float _terminalVelocity = 53.0f;

    // timeout deltatime
    private float _jumpTimeoutDelta;
    private float _fallTimeoutDelta;

    private GameObject _mainCamera;




    // Start is called before the first frame update
    void Start()
    {
        this.posicionInicial = this.transform.position;
        this.vida = maxVida;

       

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        characterController = GetComponent<CharacterController>();

    }
    
    private void Update()
    {
        rotacionCamara();
        JumpAndGravity();
        Move();
   

    }

   

    private void rotacionCamara()
    {
        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            float movimientoDelRatonXQueAplicaRotacionEnY = this.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;


            this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, movimientoDelRatonXQueAplicaRotacionEnY, 0);
        }

    }

    private void Move()
    {
        float finalSpeed;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            finalSpeed = sprintSpeed;
        }
        else
        {
            finalSpeed = speed;
        }

        Vector3 movement = this.transform.forward * Input.GetAxis("Vertical") * -1*-1 + this.transform.right * Input.GetAxis("Horizontal") * -1*-1;

        //Vector3 movement = Vector3.forward * Input.GetAxis("Vertical") * -1 + Vector3.right * Input.GetAxis("Horizontal") * -1;

        // move the player
        characterController.Move(movement * finalSpeed * Time.deltaTime + new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);
    }

    private void JumpAndGravity()
    {
        if (characterController.isGrounded)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = FallTimeout;

            // stop our velocity dropping infinitely when grounded
            if (_verticalVelocity < 0.0f)
            {
                _verticalVelocity = -2f;
            }

            // Jump
            if (Input.GetKeyDown(KeyCode.Space) && _jumpTimeoutDelta <= 0.0f)
            {
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
            {
                _jumpTimeoutDelta -= Time.deltaTime;
            }
        }
        else
        {
            // reset the jump timeout timer
            _jumpTimeoutDelta = JumpTimeout;

            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }

        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (_verticalVelocity < _terminalVelocity)
        {
            _verticalVelocity += Gravity * Time.deltaTime;
        }
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }


    public void getHurt(float damage)
    {
        if (this.vida - damage < 0)
        {
            this.vida = 0;
        }
        else
        {
            this.vida -= damage;
        }

        //this.eventoCambioVida.Raise();

        if (this.vida == 0)
        {
            this.morir();
        }

    }

    public void morir()
    {
        this.vida = maxVida;
        this.transform.position = posicionInicial;
    }


}
