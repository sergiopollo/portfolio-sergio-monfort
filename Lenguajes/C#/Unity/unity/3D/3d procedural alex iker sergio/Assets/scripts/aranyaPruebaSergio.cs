using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonGen;

public class aranyaPruebaSergio : MonoBehaviour
{
    public float generationTime;
    public float rangoMatrizX;
    public float rangoMatrizZ;
    public GameObject salaActual;
    public Sala spawn;
    public Sala[] tiposDeSalas;
    public int numSalasAGenerar;
    public List<GameObject> salasColocadas;
    public GameObject pasillo;

    //pruebas chungas
    private HashSet<Point> points;

    // Start is called before the first frame update
    void Start()
    {
        points = new HashSet<Point>();
        this.transform.position = new Vector3(0, 0, 0);
        StartCoroutine(Generar());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerarPasillos()
    {
        //la mayoria de cosas de esta funcion son algoritmos de internet
        for (int i = 0; i<salasColocadas.Count; i++)
        {
            this.points.Add(new Point(Mathf.RoundToInt(salasColocadas[i].transform.position.x), Mathf.RoundToInt(salasColocadas[i].transform.position.z), salasColocadas[i].GetComponent<SalaSc>().tipo));
        }

        
        var triangles = BowyerWatson.Triangulate(points);

        var graph = new HashSet<Edge>();
        foreach (var triangle in triangles)
            graph.UnionWith(triangle.edges);

        var tree = Kruskal.MinimumSpanningTree(graph);


        
        
        //construye los pasillos del minimum spanning tree
        foreach (var edge in tree)
        {

            List<Vector3> puntos = edge.calcularPuntosBordesSalas();
            //Vector3 p1 = new Vector3(edge.a.x, 0, edge.a.z);
            //Vector3 p2 = new Vector3(edge.b.x, 0, edge.b.z);

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //hay que llamar a la funcion de edge



            //que genere los pasillos
            this.transform.position = puntos[0];
            construirPasillosRectos(puntos[1], true, false);
            construirPasillosRectos(puntos[1], false, false);
        }
        
        //que coja algunas de aqui para que no sean solo los pasillos minimos, que haya alguno extra
        foreach (var edge in graph)
        {
            float random = Random.Range(0, 100);
            if (random < 10)
            {

                List<Vector3> puntos = edge.calcularPuntosBordesSalas();
                // Vector3 p1 = new Vector3(edge.a.x, 0, edge.a.z);

                //Vector3 p2 = new Vector3(edge.b.x, 0, edge.b.z);

                //que genere los pasillos
                this.transform.position = puntos[0];
                construirPasillosRectos(puntos[1], true, false);
                construirPasillosRectos(puntos[1], false, false);
            }
        }

    }


    IEnumerator Generar()
    {
        //primero genera el spawn
        Debug.Log("primero genera el spawn");
        salaActual = Instantiate(spawn.tipoSala);
        this.salasColocadas.Add(salaActual);

        for (int i = 0; i < numSalasAGenerar; i++)
        {

            float randomX = Random.Range(-rangoMatrizX, rangoMatrizX);
            

            float randomZ = Random.Range(-rangoMatrizZ, rangoMatrizZ);
            

            this.transform.position = new Vector3(randomX, 0, randomZ);

            SalaSc PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();

            SalaSc salaAnterior = PropiedadesSalaActual;

            bool CabeSala = true;


            //intenta generar una sala
            int numSala = Random.Range(0, tiposDeSalas.Length);
            GameObject SalaAGenerar = tiposDeSalas[numSala].tipoSala;
            
            //comprueba si la sala cabe
            
            for (int j = 0; j < salasColocadas.Count; j++)
            {
                Debug.Log("comparando sala " + j + " con posible sala " + salasColocadas.Count);
                Debug.Log("distancia de los centros: " + Vector3.Distance(this.transform.position, salasColocadas[j].transform.position));
                float cosaRAdios = salasColocadas[j].GetComponent<SalaSc>().tipo.radio + tiposDeSalas[numSala].radio;
                Debug.Log("suma radios: " + cosaRAdios);
                Debug.Log("radio 1: " + salasColocadas[j].GetComponent<SalaSc>().tipo.radio);
                Debug.Log("radio 2: " + tiposDeSalas[numSala].radio);
                //mira si la distancia entre la sala que va a generar y las que ya hay es suficiente (if distancia entre salas es mas peque�a que la suma de sus radios)
                if (Vector3.Distance(this.transform.position, salasColocadas[j].transform.position) < (cosaRAdios+10))
                {
                    CabeSala = false;
                }
            }


            if (CabeSala)
            {
                //si cabe la genera
                salaActual = Instantiate(SalaAGenerar);
                //salaActual = Instantiate(tiposDeSalas[1].tipoSala);
                salaActual.transform.position = this.transform.position;
                int girar = Random.Range(-1, 2);

                salaActual.transform.eulerAngles = new Vector3(salaActual.transform.eulerAngles.x, salaActual.transform.eulerAngles.y + 90 * girar, salaActual.transform.eulerAngles.z);

                //salaActual.transform.forward = this.transform.forward;

                PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();
                

                //le decimos quien es su vecino anterior
                PropiedadesSalaActual.Vecinos.Add(salaAnterior.gameObject);

                //a�ado la sala a la lista
                this.salasColocadas.Add(salaActual);
                Debug.Log("tamanyo lista " + salasColocadas.Count);
            }
            else
            {
                i--;
            }

            yield return new WaitForSeconds(0.001f);
        }

        //genera los pasillos
        this.GenerarPasillos();
       
    }


    //booleano de si es recto en x o en z, si es true hara los pasillos rectos en x, si es false en z
    public void construirPasillosRectos(Vector3 salaDestino, bool X, bool yaTeHeLLamado)
    {

        GameObject pasilloPrimero = Instantiate(pasillo);
        pasilloPrimero.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);


        if (X)
        {
            if (this.transform.position.x < salaDestino.x)
            {
                GameObject ultimoPasillo = null;

                while (this.transform.position.x < salaDestino.x/*- Lado de la salaDestino*/)
                {
                    this.transform.position = new Vector3(this.transform.position.x + 5f, this.transform.position.y, this.transform.position.z);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    ultimoPasillo = pasilloPuesto;
                }

                if (ultimoPasillo != null)
                {
                    GameObject.Destroy(ultimoPasillo);
                }

                if (!yaTeHeLLamado)
                {
                    //para que quede el camino recto hacia la posicion borra el ultimo pasillo y pone uno en esta posicion
                    this.transform.position = new Vector3(salaDestino.x, this.transform.position.y, this.transform.position.z);
                    
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);

                    construirPasillosRectos(salaDestino, false, true);
                }
            }
            else if (this.transform.position.x > salaDestino.x)
            {
                GameObject ultimoPasillo = null;

                while (this.transform.position.x > salaDestino.x/*+ Lado de la salaDestino*/)
                {
                    this.transform.position = new Vector3(this.transform.position.x - 5f, this.transform.position.y, this.transform.position.z);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    ultimoPasillo = pasilloPuesto;
                }

                if (ultimoPasillo != null)
                {
                    GameObject.Destroy(ultimoPasillo);
                }

                if (!yaTeHeLLamado)
                {
                    //para que quede el camino recto hacia la posicion borra el ultimo pasillo y pone uno en esta posicion
                    this.transform.position = new Vector3(salaDestino.x, this.transform.position.y, this.transform.position.z);
                    
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    construirPasillosRectos(salaDestino, false, true);
                }
            }
        }
        else
        {
            if (this.transform.position.z < salaDestino.z)
            {
                GameObject ultimoPasillo = null;
                while (this.transform.position.z < salaDestino.z/*- Lado de la salaDestino*/)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 5f);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    ultimoPasillo = pasilloPuesto;
                }

                if (ultimoPasillo != null)
                {
                    GameObject.Destroy(ultimoPasillo);
                }

                if (!yaTeHeLLamado)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, salaDestino.z);
                    
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    construirPasillosRectos(salaDestino, true, true);
                }
            }
            else if (this.transform.position.z > salaDestino.z)
            {
                GameObject ultimoPasillo = null;
                while (this.transform.position.z > salaDestino.z/*+ Lado de la salaDestino*/)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5f);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    ultimoPasillo = pasilloPuesto;
                }

                if (ultimoPasillo != null)
                {
                    GameObject.Destroy(ultimoPasillo);
                }

                if (!yaTeHeLLamado)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, salaDestino.z);
                   
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    construirPasillosRectos(salaDestino, true, true);
                }
            }
        }


    }


    /*
    IEnumerator generarPasillos2()
    {
        for (int i = 0; i < salasColocadas.Count; i++)
        {
            GameObject miSala = salasColocadas[i];

            SalaSc propiedadesSala = salasColocadas[i].GetComponent<SalaSc>();

            GameObject salaMasCercana1 = null;
            GameObject salaMasCercana2 = null;

            float distancia = 9999999999999999;

            //busca la sala mas cercana1
            for (int k = 0; k < salasColocadas.Count; k++)
            {
                if (salasColocadas[i] != salasColocadas[k])
                {

                    if (k == 0)
                    {
                        distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                        salaMasCercana1 = salasColocadas[k];
                    }
                    else
                    {
                        if (Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position) < distancia)
                        {
                            distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                            salaMasCercana1 = salasColocadas[k];
                        }
                    }

                }
            }

            //busca la sala mas cercana2
            for (int k = 0; k < salasColocadas.Count; k++)
            {
                if (salasColocadas[i] != salasColocadas[k])
                {

                    if (k == 0 && salasColocadas[k] != salaMasCercana1)
                    {

                        distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                        salaMasCercana2 = salasColocadas[k];
                        
                    }
                    else
                    {
                        if (Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position) < distancia && salasColocadas[k] != salaMasCercana1)
                        {
                            distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                            salaMasCercana2 = salasColocadas[k];
                        }
                    }

                }
            }

            Debug.Log("sala cercana 1: "+salaMasCercana1);

            SalaSc propiedadesSalaCercana1 = salaMasCercana1.GetComponent<SalaSc>();
            //SalaSc propiedadesSalaCercana2 = salaMasCercana2.GetComponent<SalaSc>();

            //creo pasillo a la salacercana1


            bool estoyCercaEnX = false;
            bool estoyCercaEnZ = false;

            //punto inicial 1 tiene que ser mas peque�o que punto final 2 && punto final 1 tiene que ser mas grande que punto final 2
            //OR
            //punto inicial 1 tiene que ser mas peque�o que punto inicial 2 && punto final 1 tiene que ser mas grande que punto inicial 2

            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //comprobacion mal hecha
            if (((miSala.transform.position.x - propiedadesSala.lado) < (salaMasCercana1.transform.position.x + propiedadesSalaCercana1.lado) && (miSala.transform.position.x - propiedadesSala.lado) > (salaMasCercana1.transform.position.x + propiedadesSalaCercana1.lado)) || ((miSala.transform.position.x - propiedadesSala.lado) < (salaMasCercana1.transform.position.x - propiedadesSalaCercana1.lado) && (miSala.transform.position.x + propiedadesSala.lado) > (salaMasCercana1.transform.position.x - propiedadesSalaCercana1.lado)))
            {
                estoyCercaEnX = true;
            }

            if ((miSala.transform.position.z - propiedadesSala.lado) < (salaMasCercana1.transform.position.z + propiedadesSalaCercana1.lado) && (miSala.transform.position.z - propiedadesSala.lado) > (salaMasCercana1.transform.position.z + propiedadesSalaCercana1.lado) || ((miSala.transform.position.z - propiedadesSala.lado) < (salaMasCercana1.transform.position.z - propiedadesSalaCercana1.lado) && (miSala.transform.position.z + propiedadesSala.lado) > (salaMasCercana1.transform.position.z - propiedadesSalaCercana1.lado)))
            {
                estoyCercaEnZ = true;
            }
            Debug.Log(estoyCercaEnX +" "+estoyCercaEnZ);

            //if (estoyCercaEnX || estoyCercaEnZ)
            //{
                //genera pasillos  en linea recta de una sala a la otra
                Debug.Log("genero pasillos entre "+miSala+" y "+salaMasCercana1);
                this.transform.position = miSala.transform.position;
                //this.transform.LookAt(salaMasCercana1.transform);

                //if (estoyCercaEnX)
                //{

                    //this.construirPasillosRectos(salaMasCercana1, true, false);




                //}
                //else{
                    //this.construirPasillosRectos(salaMasCercana1, false, false);
                //}
               
            //}
            //else
            //{
                //Debug.Log("genero pasillos entre " + miSala + " y " + salaMasCercana1);
                //this.transform.position = miSala.transform.position;
                //this.transform.LookAt(salaMasCercana1.transform);

                //this.construirPasillos(salaMasCercana1);
            //}






        }

        yield return new WaitForSeconds(0.0001f);
    }



    
    public void construirPasillos(GameObject salaMasCercana1)
    {
        if (this.transform.position.x < salaMasCercana1.transform.position.x)
        {
            while (this.transform.position.x < salaMasCercana1.transform.position.x)
            {
                this.transform.position += this.transform.forward * 2.5f;
                GameObject pasilloPuesto = Instantiate(pasillo);
                pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }
        else if (this.transform.position.x > salaMasCercana1.transform.position.x)
        {
            while (this.transform.position.x > salaMasCercana1.transform.position.x)
            {
                this.transform.position += this.transform.forward * 2.5f;
                GameObject pasilloPuesto = Instantiate(pasillo);
                pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }
        else if (this.transform.position.z < salaMasCercana1.transform.position.z)
        {
            while (this.transform.position.z < salaMasCercana1.transform.position.z)
            {
                this.transform.position += this.transform.forward * 2.5f;
                GameObject pasilloPuesto = Instantiate(pasillo);
                pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }
        else if (this.transform.position.z > salaMasCercana1.transform.position.z)
        {
            while (this.transform.position.z > salaMasCercana1.transform.position.z)
            {
                this.transform.position += this.transform.forward * 2.5f;
                GameObject pasilloPuesto = Instantiate(pasillo);
                pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            }
        }
    }




    IEnumerator generarPasillos()
    {
        for (int i = 0; i<salasColocadas.Count; i++) 
        {

            GameObject puertaDestino = null;

            SalaSc propiedadesSala = salasColocadas[i].GetComponent<SalaSc>();

            for (int j = 0; j < propiedadesSala.puertas.Length; j++)
            {

                puertaSc puerta = propiedadesSala.puertas[j].GetComponent<puertaSc>();

                //coge una puerta sin pasillo
                if (!puerta.tengoPasillo)
                {
                    GameObject salaMasCercana = null;

                    float distancia = 0;

                    //busca la sala mas cercana
                    for (int k = 0; k < salasColocadas.Count; k++)
                    {
                        if (salasColocadas[i] != salasColocadas[k])
                        {

                            if (k == 0)
                            {
                                distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                                salaMasCercana = salasColocadas[k];
                            }
                            else
                            {
                                if (Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position) < distancia)
                                {
                                    distancia = Vector3.Distance(salasColocadas[i].transform.position, salasColocadas[k].transform.position);
                                    salaMasCercana = salasColocadas[k];
                                }
                            }

                        }
                    }


                    SalaSc propiedadesSalaCercana = salaMasCercana.GetComponent<SalaSc>();


                    //una vez tiene la sala mas cercana busca una puerta disponible
                    for (int k = 0; k < propiedadesSalaCercana.puertas.Length; k++)
                    {
                        puertaSc puertaCercana = propiedadesSalaCercana.puertas[k].GetComponent<puertaSc>();

                        if (!puertaCercana.tengoPasillo)
                        {
                            puertaDestino = propiedadesSalaCercana.puertas[k];
                            k = propiedadesSalaCercana.puertas.Length+1;
                        }
                    }

                }


            }

            //si tiene una puerta de destino crea el camino hacia ella
            if (puertaDestino != null)
            {


                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //cosas



            }

        }

        yield return new WaitForSeconds(0.0001f);
    }

    */
}
