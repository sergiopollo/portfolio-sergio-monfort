using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AranyaPrincipal : MonoBehaviour
{
    public float generationTime;
    public GameObject salaActual;
    public Sala spawn;
    public Sala[] tiposDeSalas;
    public GameObject pasillo;
    public int numSalasAGenerar;
    public List<GameObject> salasColocadas;
    public List<GameObject> pasillosColocados;

    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(0,0,0);
        StartCoroutine(Generar());
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator Generar()
    {
        //primero genera el spawn
        Debug.Log("primero genera el spawn");
        salaActual=Instantiate(spawn.tipoSala);
        this.salasColocadas.Add(salaActual);

        for (int i = 0; i<numSalasAGenerar; i++)
        {
            //una vez ha generado la sala, va a buscar una puerta de su sala actual
            SalaSc PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();

            int numPuerta = 0;
            bool heSeleccionadoUnaPuerta = false;

            for (int j = 0; j < PropiedadesSalaActual.puertas.Length; j++)
            {
                //PropiedadesSalaActual.puertas[j];
                //tiene que mirar si esta puerta tiene ya un pasillo o no, necesitamos otro script o scriptable para puertas?

                if (!heSeleccionadoUnaPuerta && PropiedadesSalaActual.puertas[j].GetComponent<puertaSc>().tengoPasillo)
                {
                    heSeleccionadoUnaPuerta = true;
                    numPuerta = j;
                    //le pone que ya tiene pasillo
                    PropiedadesSalaActual.puertas[j].GetComponent<puertaSc>().tengoPasillo = true;
                }                
            }

            GameObject puerta = PropiedadesSalaActual.puertas[numPuerta];
            //la ara�a va a la puerta
            this.transform.position = puerta.transform.position;
            this.transform.forward = puerta.transform.forward;

            //una vez tiene una puerta seleccionada se mueve hacia adelante una cantidad random 
            int movimientosPaAlante = Random.Range(1, 10); ;


            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+0.35f, this.transform.position.z);



            //coloca un primer pasillo
            
            //primer pasillo
            this.transform.position += this.transform.forward * 2.5f;
            GameObject pasilloPuesto = Instantiate(pasillo);
            pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            pasillosColocados.Add(pasilloPuesto);


            //comprueba si el nuevo pasillo colisionaria con otro pasillo o con otra sala
            int numChecksPasillo = 5;
            

            //hace un numero limitado de checks, en caso de que despues de todos los checks no haya podido colocar el pasillo, no hace nada (intentara colocar los pasillos y las salas que pida numSalasAGenerar)
            //(pero no colocara ninguna si no puede)


            for (int j = 0; j < movimientosPaAlante; j++)
            {

                bool miniPuedoColocarPasillo1 = false;
                bool miniPuedoColocarPasillo2 = false;

                for (int l = 0; l< numChecksPasillo; l++)
                {
                    if (!miniPuedoColocarPasillo1 || !miniPuedoColocarPasillo2)
                    {
                           
                            //si al avanzar choca con algo, que gire hacia algun lado
                            for (int k = 0; k < salasColocadas.Count; k++)
                            {

                                //comprueba con cada sala si el pasillo que va a colocar colisiona con una sala que ya haya colocada
                                if (Vector3.Distance((this.transform.forward * 2.5f), salasColocadas[k].transform.position) > (salasColocadas[k].GetComponent<SalaSc>().radio + 2.5f))
                                {
                                    miniPuedoColocarPasillo1 = true;
                                }
                            }

                            for (int k = 0; k < pasillosColocados.Count; k++)
                            {

                                //comprueba con cada pasillo ya colocado si el pasillo que va a colocar colisiona con un pasillo que ya haya colocado
                                if (Vector3.Distance((this.transform.forward * 2.5f), pasillosColocados[k].transform.position) > (2.5f + 2.5f))
                                {
                                    miniPuedoColocarPasillo2 = true;
                                }
                            }

                            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            //si lo puede colocar, lo coloca de forma normal, si no, gira de forma random 90 o -90 grados y vuelve a hacer el check, si despues de varios checks sigue diciendo que no, deja de construir pasillos
                            //yield return new WaitForSeconds(0.5f);
                            Debug.Log("check1 "+ miniPuedoColocarPasillo1 + miniPuedoColocarPasillo2);
                            if (miniPuedoColocarPasillo1 && miniPuedoColocarPasillo2)
                            {
                                this.transform.position += this.transform.forward * 5;//(lo que mida el pasillo);
                                                                                      //que la ara�a tenga una probabilidad de girar
                                int VoyAGirar = Random.Range(0, 10);
                                int girar = Random.Range(-1, 1);
                                if (j % 3 == 0 && VoyAGirar < 5)
                                {
                                    this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y + (90 * girar), this.transform.eulerAngles.z);

                                }


                                pasilloPuesto = Instantiate(pasillo);
                                pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                                pasillo.transform.forward = this.transform.forward;
                                //lo a�ade a la lista
                                this.pasillosColocados.Add(pasilloPuesto);
                            }
                            else
                            {
                                //gira
                                int girar = Random.Range(-1, 1);
                                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y + 90 * girar, this.transform.eulerAngles.z);

                                //vuelve a comprobar si puede colocar pasillo

                            }
                            Debug.Log("check2 " + miniPuedoColocarPasillo1+ miniPuedoColocarPasillo2);

                    }
                }
            }

            

            SalaSc salaAnterior = PropiedadesSalaActual;

            bool CabeSala = true;

            
            //intenta generar una sala
            int numSala = Random.Range(0, tiposDeSalas.Length);
            GameObject SalaAGenerar = tiposDeSalas[numSala].tipoSala;

            //SalaSc PropiedadesFuturaSala = SalaAGenerar.GetComponent<SalaSc>();

            //comprueba si la sala cabe
            Vector3 posicionDeLaFuturaSala = this.transform.position + this.transform.forward * (tiposDeSalas[numSala].radio + 2.5f);


            

            for (int j=0; j<salasColocadas.Count; j++)
            {
                Debug.Log("comparando sala "+j+" con posible sala "+salasColocadas.Count);
                Debug.Log("distancia de los centros: " + Vector3.Distance(this.transform.position, salasColocadas[j].transform.position));
                float cosaRAdios = salasColocadas[j].GetComponent<SalaSc>().tipo.radio + tiposDeSalas[numSala].radio;
                Debug.Log("suma radios: " + cosaRAdios);
                Debug.Log("radio 1: " + salasColocadas[j].GetComponent<SalaSc>().tipo.radio);
                Debug.Log("radio 2: " + tiposDeSalas[numSala].radio);
                //mira si la distancia entre la sala que va a generar y las que ya hay es suficiente (if distancia entre salas es mas peque�a que la suma de sus radios)
                if (Vector3.Distance(posicionDeLaFuturaSala, salasColocadas[j].transform.position) < (cosaRAdios))
                {
                    CabeSala = false;
                }
            }


            if (CabeSala)
            {
                //si cabe la genera
                salaActual = Instantiate(SalaAGenerar);
                //salaActual = Instantiate(tiposDeSalas[1].tipoSala);
                salaActual.transform.position = this.transform.position;
                salaActual.transform.forward = this.transform.forward;

                yield return new WaitForSeconds(0.001f);
                PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();
                //CAMBIOS RADIO IKER
                //salaActual.transform.position += salaActual.transform.forward * (PropiedadesSalaActual.radio + 2.5f);
                salaActual.transform.position += salaActual.transform.forward * (PropiedadesSalaActual.lado + 2.5f+5f);
                Debug.Log("aa" + PropiedadesSalaActual.radio);

                salaActual.transform.position = new Vector3(salaActual.transform.position.x, salaActual.transform.position.y + PropiedadesSalaActual.altura / 2 - 2.5f, salaActual.transform.position.z);

                Debug.Log("aa" + PropiedadesSalaActual.altura);

                //le decimos quien es su vecino anterior
                PropiedadesSalaActual.Vecinos.Add(salaAnterior.gameObject);

                //a�ado la sala a la lista
                this.salasColocadas.Add(salaActual);
                Debug.Log("tamanyo lista "+salasColocadas.Count);
                //si no cabe la ara�a sigue
            }

            yield return new WaitForSeconds(generationTime);
        }

        //despues de este bucle tiene que activarse la "ara�a pasillos"






    }



    


}
