﻿using System.Collections.Generic;
using UnityEngine;

namespace DungeonGen
{
    public class Edge
    {
        
        public readonly Point a;
        public readonly Point b;

        private float length = -1;
        public float Length
        {
            get
            {
                if (length < 0)
                {
                    float dx = a.x - b.x;
                    float dz = a.z - b.z;
                    length = Mathf.Sqrt(dx * dx + dz * dz);
                }
                return length;
            }
        }

        public Edge(Point a, Point b)
        {
            if (a == b)
            {
                throw new DuplicatePointException(a, b);
            }
            else if (a < b)
            {
                this.a = a;
                this.b = b;
            }
            else
            {
                this.a = b;
                this.b = a;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Edge other && a == other.a && b == other.b;
        }

        public override int GetHashCode()
        {
            var hashCode = 2118541809;
            hashCode = hashCode * -1521134295 + a.GetHashCode();
            hashCode = hashCode * -1521134295 + b.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"Edge({a}, {b})";
        }

        public static int LengthComparison(Edge x, Edge z)
        {
            float lx = x.Length;
            float lz = z.Length;
            if (Mathf.Approximately(lx, lz))
                return 0;
            else if (lx > lz)
                return 1;
            else
                return -1;
        }

        //cosas anyadidas

        //Devuelve los dos puntos mas cercanos entre si para generar los pasillos
        public List<Vector3> calcularPuntosBordesSalas()
        {

            Vector3 ladoA = this.calcularLadoCercano(a, b);
            Vector3 ladoB = this.calcularLadoCercano(b, a);

            List<Vector3> lista = new List<Vector3>();

                lista.Add(ladoA);
            lista.Add(ladoB);

            return lista;

        }

        //Pasandole dos puntos te comprueba que lado del primer punto esta mas cercano al segundo
        public Vector3 calcularLadoCercano(Point inicio, Point destino)
        {

            float puntoX = 0;
            float puntoZ = 0;

            //Comprovamos cual de los dos puntos en x esta mas cerca
            if (inicio.x < destino.x)
            {
                if ((destino.x - (inicio.x + inicio.miSala.lado)) < (destino.x - (inicio.x - inicio.miSala.lado)))
                {
                    Debug.Log("IZQUIERDA/DERECHA");
                    puntoX = inicio.x + inicio.miSala.lado;
                }
                else
                {
                    Debug.Log("IZQUIERDA/IZQUIERDA");
                    puntoX = inicio.x - inicio.miSala.lado;
                }
            }
            else
            {
                if ((destino.x - (inicio.x + inicio.miSala.lado)) > (destino.x - (inicio.x - inicio.miSala.lado)))
                {
                    Debug.Log("DERECHA/DERECHA");
                    puntoX = inicio.x + inicio.miSala.lado;
                }
                else
                {
                    Debug.Log("DERECHA/IZQUIERDA");
                    puntoX = inicio.x - inicio.miSala.lado;
                }
            }

            //Comprovamos cual de los dos puntos en z esta mas cerca
            if (inicio.z < destino.z)
            {
                if ((destino.z - (inicio.z + inicio.miSala.lado)) < (destino.z - (inicio.z - inicio.miSala.lado)))
                {
                    Debug.Log("ABAJO/ARRIBA");
                    puntoZ = inicio.z + inicio.miSala.lado;
                }
                else
                {
                    Debug.Log("ABAJO/ABAJO");
                    puntoZ = inicio.z - inicio.miSala.lado;
                }
            }
            else
            {
                if ((destino.z - (inicio.z + inicio.miSala.lado)) > (destino.z - (inicio.z - inicio.miSala.lado)))
                {
                    Debug.Log("ARRIBA/ARRIBA");
                    puntoZ = inicio.z + inicio.miSala.lado;
                }
                else
                {
                    Debug.Log("ARRIBA/ABAJO");
                    puntoZ = inicio.z - inicio.miSala.lado;
                }
            }

            //Comprovamos si esta mas cerca de X o de Z
            if (Vector3.Distance(new Vector3(puntoX, 0, inicio.z), new Vector3(destino.x, 0, destino.z)) < Vector3.Distance(new Vector3(inicio.x , 0, puntoZ), new Vector3(destino.x, 0, destino.z)))
            {
                Debug.Log("MEJOR LA X");
                Debug.Log("//////////////////////////////////////////");
                return new Vector3(puntoX+2.5f, 0, inicio.z);
            }
            else
            {
                Debug.Log("MEJOR LA Z");
                Debug.Log("//////////////////////////////////////////");
                return new Vector3(inicio.x, 0, puntoZ+2.5f);
            }
        }
    }
}
