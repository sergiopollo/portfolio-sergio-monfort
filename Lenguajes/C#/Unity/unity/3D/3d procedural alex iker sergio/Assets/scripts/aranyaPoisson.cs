using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonGen;

public class aranyaPoisson : MonoBehaviour
{
    public float generationTime;
    public float rangoMatrizX;
    public float rangoMatrizZ;
    public GameObject salaActual;
    public Sala spawn;
    public Sala[] tiposDeSalas;
    public int numSalasAGenerar;
    public List<GameObject> salasColocadas;
    public GameObject pasillo;

    //pruebas chungas
    private HashSet<Point> points;

    // Start is called before the first frame update
    void Start()
    {
        points = new HashSet<Point>();
        this.transform.position = new Vector3(0, 0, 0);
        StartCoroutine(Generar());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerarPasillos()
    {
        //la mayoria de cosas de esta funcion son algoritmos de internet
        for (int i = 0; i < salasColocadas.Count; i++)
        {
            this.points.Add(new Point(Mathf.RoundToInt(salasColocadas[i].transform.position.x), Mathf.RoundToInt(salasColocadas[i].transform.position.z)));
        }


        var triangles = BowyerWatson.Triangulate(points);

        var graph = new HashSet<Edge>();
        foreach (var triangle in triangles)
            graph.UnionWith(triangle.edges);

        var tree = Kruskal.MinimumSpanningTree(graph);

        foreach (var edge in tree)
        {
            Vector3 p1 = new Vector3(edge.a.x, 0, edge.a.z);
            Vector3 p2 = new Vector3(edge.b.x, 0, edge.b.z);

            //que genere los pasillos
            this.transform.position = p1;
            construirPasillosRectos(p2, true, false);
        }
    }


    IEnumerator Generar()
    {
        //primero genera el spawn
        Debug.Log("primero genera el spawn");
        salaActual = Instantiate(spawn.tipoSala);
        this.salasColocadas.Add(salaActual);

        for (int i = 0; i < numSalasAGenerar; i++)
        {

            float randomX = Random.Range(-rangoMatrizX, rangoMatrizX);

            float randomZ = Random.Range(-rangoMatrizZ, rangoMatrizZ);

            this.transform.position = new Vector3(randomX, 0, randomZ);

            SalaSc PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();

            SalaSc salaAnterior = PropiedadesSalaActual;

            bool CabeSala = true;


            //intenta generar una sala
            int numSala = Random.Range(0, tiposDeSalas.Length);
            GameObject SalaAGenerar = tiposDeSalas[numSala].tipoSala;

            //comprueba si la sala cabe

            for (int j = 0; j < salasColocadas.Count; j++)
            {
                Debug.Log("comparando sala " + j + " con posible sala " + salasColocadas.Count);
                Debug.Log("distancia de los centros: " + Vector3.Distance(this.transform.position, salasColocadas[j].transform.position));
                float cosaRAdios = salasColocadas[j].GetComponent<SalaSc>().tipo.radio + tiposDeSalas[numSala].radio;
                Debug.Log("suma radios: " + cosaRAdios);
                Debug.Log("radio 1: " + salasColocadas[j].GetComponent<SalaSc>().tipo.radio);
                Debug.Log("radio 2: " + tiposDeSalas[numSala].radio);
                //mira si la distancia entre la sala que va a generar y las que ya hay es suficiente (if distancia entre salas es mas peque�a que la suma de sus radios)
                if (Vector3.Distance(this.transform.position, salasColocadas[j].transform.position) < (cosaRAdios + 10))
                {
                    CabeSala = false;
                }
            }


            if (CabeSala)
            {
                //si cabe la genera
                salaActual = Instantiate(SalaAGenerar);
                //salaActual = Instantiate(tiposDeSalas[1].tipoSala);
                salaActual.transform.position = this.transform.position;
                int girar = Random.Range(-1, 2);

                salaActual.transform.eulerAngles = new Vector3(salaActual.transform.eulerAngles.x, salaActual.transform.eulerAngles.y + 90 * girar, salaActual.transform.eulerAngles.z);

                //salaActual.transform.forward = this.transform.forward;

                PropiedadesSalaActual = salaActual.GetComponent<SalaSc>();


                //le decimos quien es su vecino anterior
                PropiedadesSalaActual.Vecinos.Add(salaAnterior.gameObject);

                //a�ado la sala a la lista
                this.salasColocadas.Add(salaActual);
                Debug.Log("tamanyo lista " + salasColocadas.Count);
            }
            else
            {
                i--;
            }

            yield return new WaitForSeconds(0.001f);
        }

        //genera los pasillos
        this.GenerarPasillos();

    }


    //booleano de si es recto en x o en z, si es true hara los pasillos rectos en x, si es false en z
    public void construirPasillosRectos(Vector3 salaDestino, bool X, bool yaTeHeLLamado)
    {

        if (X)
        {
            if (this.transform.position.x < salaDestino.x)
            {
                while (this.transform.position.x < salaDestino.x)
                {
                    this.transform.position = new Vector3(this.transform.position.x + 5f, this.transform.position.y, this.transform.position.z);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                }
                if (!yaTeHeLLamado)
                {
                    construirPasillosRectos(salaDestino, false, true);
                }
            }
            else if (this.transform.position.x > salaDestino.x)
            {
                while (this.transform.position.x > salaDestino.x)
                {
                    this.transform.position = new Vector3(this.transform.position.x - 5f, this.transform.position.y, this.transform.position.z);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                }
                if (!yaTeHeLLamado)
                {
                    construirPasillosRectos(salaDestino, false, true);
                }
            }
        }
        else
        {
            if (this.transform.position.z < salaDestino.z)
            {
                while (this.transform.position.z < salaDestino.z)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 5f);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                }
                if (!yaTeHeLLamado)
                {
                    construirPasillosRectos(salaDestino, true, true);
                }
            }
            else if (this.transform.position.z > salaDestino.z)
            {
                while (this.transform.position.z > salaDestino.z)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5f);
                    GameObject pasilloPuesto = Instantiate(pasillo);
                    pasilloPuesto.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                }
                if (!yaTeHeLLamado)
                {
                    construirPasillosRectos(salaDestino, true, true);
                }
            }
        }


    }


}
