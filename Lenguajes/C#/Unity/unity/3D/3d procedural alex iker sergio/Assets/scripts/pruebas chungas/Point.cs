﻿using UnityEngine;

namespace DungeonGen
{
    public class Point
    {
        public readonly int x;
        public readonly int z;
        //cosas anyadidas
        public Sala miSala;

        public Point(int x, int z, Sala sala)
        {
            this.x = x;
            this.z = z;
            this.miSala = sala;
        }

        public Point(int x, int z)
        {
            this.x = x;
            this.z = z;
        }

        public override bool Equals(object obj)
        {
            return obj is Point other && x == other.x && z == other.z;
        }

        public override int GetHashCode()
        {
            var hashCode = 1502939027;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + z.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"Point({this.x}, {this.z})";
        }

        public static bool operator <(Point lhs, Point rhs)
        {
            return (lhs.x < rhs.x) || ((lhs.x == rhs.x) && (lhs.z < rhs.z));
        }

        public static bool operator >(Point lhs, Point rhs)
        {
            return (lhs.x > rhs.x) || ((lhs.x == rhs.x) && (lhs.z > rhs.z));
        }
    }
}
