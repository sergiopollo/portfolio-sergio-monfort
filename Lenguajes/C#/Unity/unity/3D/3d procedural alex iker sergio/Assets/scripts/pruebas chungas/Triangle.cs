﻿using System.Collections.Generic;

namespace DungeonGen
{
    public class Triangle
    {
        private readonly Point a;
        private readonly Point b;
        private readonly Point c;

        public readonly HashSet<Edge> edges;

        private readonly float circumCenterX;
        private readonly float circumCenterZ;
        private readonly float circumRadius2;

        public Triangle(Point a, Point b, Point c)
        {
            // hashing depends on the orderliness of a, b, c
            if (a < b)
            {
                if (b < c)
                {
                    // a, b, c
                    this.a = a;
                    this.b = b;
                    this.c = c;
                }
                else if (a < c)
                {
                    // a, c, b
                    this.a = a;
                    this.b = c;
                    this.c = b;
                }
                else
                {
                    // c, a, b
                    this.a = c;
                    this.b = a;
                    this.c = b;
                }
            }
            else if (a < c)
            {
                // b, a, c
                this.a = b;
                this.b = a;
                this.c = c;
            }
            else if (b < c)
            {
                // b, c, a
                this.a = b;
                this.b = c;
                this.c = a;
            }
            else
            {
                // c, b, a
                this.a = c;
                this.b = a;
                this.c = b;
            }

            edges = new HashSet<Edge>
            {
                new Edge(this.a, this.b),
                new Edge(this.b, this.c),
                new Edge(this.a, this.c)
            };

            float D = (a.x * (b.z - c.z) +
                     b.x * (c.z - a.z) +
                     c.x * (a.z - b.z)) * 2;
            float x = (a.x * a.x + a.z * a.z) * (b.z - c.z) +
                    (b.x * b.x + b.z * b.z) * (c.z - a.z) +
                    (c.x * c.x + c.z * c.z) * (a.z - b.z);
            float z = (a.x * a.x + a.z * a.z) * (c.x - b.x) +
                    (b.x * b.x + b.z * b.z) * (a.x - c.x) +
                    (c.x * c.x + c.z * c.z) * (b.x - a.x);

            circumCenterX = x / D;
            circumCenterZ = z / D;
            float dx = a.x - circumCenterX;
            float dz = a.z - circumCenterZ;
            circumRadius2 = dx * dx + dz * dz;
        }

        public override bool Equals(object obj)
        {
            return obj is Triangle other && a == other.a && b == other.b && c == other.c;
        }

        public override int GetHashCode()
        {
            var hashCode = 1474027755;
            hashCode = hashCode * -1521134295 + a.GetHashCode();
            hashCode = hashCode * -1521134295 + b.GetHashCode();
            hashCode = hashCode * -1521134295 + c.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"Triangle({a}, {b}, {c})";
        }

        public bool HasEdge(Edge edge)
        {
            return edges.Contains(edge);
        }

        private bool HasVertex(Point point)
        {
            return a == point || b == point || c == point;
        }

        public bool HasVertexFrom(Triangle triangle)
        {
            return HasVertex(triangle.a) || HasVertex(triangle.b) || HasVertex(triangle.c);
        }

        public bool CircumCircleContains(Point point)
        {
            float dx = point.x - circumCenterX;
            float dz = point.z - circumCenterZ;
            float distance2 = dx * dx + dz * dz;
            return distance2 < circumRadius2;
        }
    }
}
