using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class basiquito : MonoBehaviour
{


    public Vector3 pruebaParticula;
    public bool invi;

    // Start is called before the first frame update
    void Start()
    {
        pruebaParticula = new Vector3(this.transform.forward.x, this.transform.forward.y, this.transform.forward.z)*100;
        invi = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            GameObject newVEGO = ParticulaPool.SharedInstance.GetPooledObject();
            if (newVEGO != null)
            {
                newVEGO.SetActive(true);
                //GameObject newVEGO = Instantiate(miVE);

                newVEGO.transform.position = this.transform.position;

                //newVEGO.transform.rotation = this.transform.rotation;

                VisualEffect newVE = newVEGO.GetComponent<VisualEffect>();
                Debug.Log("PARTICULA");

                newVE.SetVector3("direccion", pruebaParticula);

                newVE.Play();

                StartCoroutine(desactivarParticula(newVEGO));
            }
            else
            {
                Debug.Log("no hay suficientes visual effects en la pool");
            }
        }

        if (Input.GetKeyDown("e"))
        {
            if (!invi)
            {
                invi = true;
                this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                invi = false;
                this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }

        IEnumerator desactivarParticula(GameObject newVEGO)
        {
            yield return new WaitForSeconds(5);

            newVEGO.SetActive(false);

        }

    }
}
