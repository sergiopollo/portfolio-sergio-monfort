using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class PlayerControler : NetworkBehaviour
{
    public float speed;
    public float InternalSpeed;
    public float rotation;
    public Camera miCamara;
    //public GameObject virtualCamera;
    public GameObject arma;
    public GameObject bullet;
    private bool visible;
    private bool InviCD;

    //cabeza prueba
    public GameObject cabeza;

    //cosa del arma
    public LayerMask mask;
    public LineRenderer lineRenderer;
    public float cadencia;
    public bool puedoDisparar;

    public override void OnStartLocalPlayer()
    {
        InternalSpeed = speed;
        visible = true;
        InviCD = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //cm = GameObject.Find("cameraManager").GetComponent<cameraManager>();
        //cm.numPlayers++;
        /*miCamara = GameObject.Find("Main Camera").GetComponent<Camera>();
        miCamara.transform.parent = cabeza.transform;*/
        print("es la camara nula?:" + miCamara);
        this.puedoDisparar = true;

        Debug.Log("disparo booelano: puedodisparar " + this.puedoDisparar);
        //virtualCamera.SetActive(true);
        miCamara.gameObject.SetActive(true);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            //print("es la camara nula?:" + miCamara);
            this.GetComponent<Rigidbody>().velocity = (this.transform.forward * Input.GetAxis("Vertical")* InternalSpeed + new Vector3(0, this.gameObject.GetComponent<Rigidbody>().velocity.y ,0)+ this.transform.right * Input.GetAxis("Horizontal")* InternalSpeed);

            this.transform.forward = new Vector3(miCamara.transform.forward.x, 0, miCamara.transform.forward.z);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                this.InternalSpeed = speed * 1.7f;
            }
            else
            {
                this.InternalSpeed = speed;
            }

            rotacionCamara();
            rotacionCamaraVertical();


            //ARMA
            if (Input.GetMouseButton(0))
            {
                Debug.Log("pulso disparo");
                if (this.puedoDisparar)
                {
                    Debug.Log("disparo con booleano");
                    this.disparar();
                    StartCoroutine(this.cooldown(this.cadencia));
                }

            }
            /*
            //INVISIBILIDAD
            if (Input.GetMouseButton(1) && !InviCD)
            {
                Invisible();
            }*/
           

        }

    }

    /*
    [Command]
    public void disparar()
    {
        Debug.Log("disparo command");
        GameObject objetivo = arma.disparar();
        print("PostDisparo");
        //print(objetivo.name);
        if (objetivo != null)
        {
            print("He golpeado al enemigo");
            NetworkServer.Destroy(objetivo);
        }
        else
        {
            print("objetivo es null");
        }
    }*/

    [ClientCallback]
    public void disparar()
    {

        Debug.Log("micamara: "+miCamara);
        dispararServer(cabeza.transform);

    }

    [Command]
    public void dispararServer(Transform target)
    {
        /*
        Debug.Log("disparo des de arma");

        if (target != null)
        {
            lineRenderer.enabled = true;
            RaycastHit hit;
            Vector3 pruebaParticula = new Vector3(target.transform.forward.x, target.transform.forward.y, target.transform.forward.z) * 100;
            Vector3 finalLinea = new Vector3(arma.transform.position.x + target.transform.forward.x * 100, arma.transform.position.y + target.transform.forward.y * 100, arma.transform.position.z + target.transform.forward.z * 100);
            Debug.DrawRay(this.transform.position, new Vector3(target.transform.forward.x, target.transform.forward.y, target.transform.forward.z) * 100, Color.blue, 2f);
            GameObject newVEGO = ParticulaPool.SharedInstance.GetPooledObject();
            if (newVEGO != null)
            {
                newVEGO.SetActive(true);
                //GameObject newVEGO = Instantiate(miVE);

                newVEGO.transform.position = arma.transform.position;


                VisualEffect newVE = newVEGO.GetComponent<VisualEffect>();
                Debug.Log("PARTICULA");

                newVE.SetVector3("direccion", pruebaParticula);

                newVE.Play();

                StartCoroutine(desactivarParticula(newVEGO));
            }
            else
            {
                Debug.Log("no hay suficientes visual effects en la pool");
            }
            print("iker");
            if (Physics.Raycast(arma.transform.position, new Vector3(target.transform.forward.x, target.transform.forward.y, target.transform.forward.z), out hit, 100, mask))
            {
                GameObject GOHit = hit.transform.gameObject;
                finalLinea = hit.point;
                Debug.Log("Choco con " + GOHit);

                if (GOHit.tag == "Player")
                {
                    Debug.Log("TE HE DADO");
                    //Destroy(GOHit);
                    moricion(GOHit);
                }
                else if (GOHit.tag == "Destroyable")
                {
                    GOHit.GetComponent<DestroyableBox>().danyar();
                }

                Vector3[] positions = new Vector3[]
                {
                    arma.transform.position,
                    finalLinea
                };

                lineRenderer.SetPositions(positions);
            }
            else
            {
                print("Hago el raycast, pero devuelve false");
            }

            StartCoroutine(borraLinea());
        }
        else
        {
            print("la camara es nula");
        }
        */

        GameObject newBala = Instantiate(bullet);
        newBala.transform.position = cabeza.transform.position + (cabeza.transform.forward * 2);//new Vector3(this.transform.forward.x, this.transform.position.y, this.transform.forward.z);
        newBala.transform.rotation = cabeza.transform.rotation;
        newBala.GetComponent<Rigidbody>().velocity = cabeza.transform.forward * 1000 * Time.fixedDeltaTime;
        //metode de network que et torna una id unica per cada jugador connectat
        newBala.GetComponent<Bala>().playerId = this.GetInstanceID();
        NetworkServer.Spawn(newBala);
        


    }
    
    [ClientCallback]
    private void OnTriggerEnter(Collider collision)
    {
        
        if (collision.transform.tag == "Bala" && this.GetInstanceID() != collision.gameObject.GetComponent<Bala>().playerId)
        {
            moricion2();
        }
    }
    


    /*
    [ServerCallback]
    void Invisible()
    {
        if (visible)
        {
            InviCD = true;
            Debug.Log("Me hago invisible");
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            StartCoroutine("InvisibleCD");
            visible = false;
        }
        else
        {
            Debug.Log("Me hago visible");
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            visible = true;
        }
    }

    
    
    IEnumerator InvisibleCD()
    {
        yield return new WaitForSeconds(10);
        Invisible();
        InviCD = false;
    }
    IEnumerator desactivarParticula(GameObject newVEGO)
    {
        yield return new WaitForSeconds(5);

        newVEGO.SetActive(false);

    }
    IEnumerator borraLinea()
    {
        yield return new WaitForSeconds(0.1f);
        lineRenderer.enabled = false;
    }
    */
    public IEnumerator cooldown(float tiempoAEsperar)
    {
        puedoDisparar = false;

        yield return new WaitForSeconds(tiempoAEsperar);

        puedoDisparar = true;
    }

    /*
    [ServerCallback]
    public void moricion(GameObject GOHit)
    {
        Debug.Log("llego a moricion");
        GOHit.GetComponent<PlayerControler>().moricion2();
    }*/

    
    public void moricion2()
    {
        Debug.Log("muere ");
        this.transform.position = new Vector3(0, 5, 0);
    }

    private void rotacionCamara()
    {
        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            float movimientoDelRatonXQueAplicaRotacionEnY = this.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * 2f;


            this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, movimientoDelRatonXQueAplicaRotacionEnY, 0);
        }

    }

    private void rotacionCamaraVertical()
    {
        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            float movimientoDelRatonYQueAplicaRotacionEnX = cabeza.transform.localEulerAngles.x + Input.GetAxis("Mouse Y") * -2f;


            cabeza.transform.localEulerAngles = new Vector3(movimientoDelRatonYQueAplicaRotacionEnX, cabeza.transform.localEulerAngles.y,  0);
        }

    }

}
