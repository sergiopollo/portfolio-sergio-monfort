using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Arma : MonoBehaviour
{
    public LayerMask mask;
    public LineRenderer lineRenderer;
    //EL CAMERA BRAIN SE LO PASA EL JUGADOR SI EL ARMA ESTA PASADA POR PARAMETRO A EL;
    public Camera cameraBrain;
    public float cadencia;
    public bool puedoDisparar;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
       
    }
    
    public GameObject disparar()
    {
        Debug.Log("disparo des de arma");
        if (this.cameraBrain != null)
        {
            lineRenderer.enabled = true;
            RaycastHit hit;
            Vector3 pruebaParticula = new Vector3(cameraBrain.transform.forward.x , cameraBrain.transform.forward.y , cameraBrain.transform.forward.z ) * 100;
            Vector3 finalLinea = new Vector3(this.transform.position.x + cameraBrain.transform.forward.x*100, this.transform.position.y + cameraBrain.transform.forward.y*100 , this.transform.position.z + cameraBrain.transform.forward.z*100);
            Debug.DrawRay(this.transform.position, new Vector3(cameraBrain.transform.forward.x , cameraBrain.transform.forward.y , cameraBrain.transform.forward.z )*100 , Color.blue, 2f);
            GameObject newVEGO = ParticulaPool.SharedInstance.GetPooledObject();
            if (newVEGO != null)
            {
                newVEGO.SetActive(true);
                //GameObject newVEGO = Instantiate(miVE);

                newVEGO.transform.position = this.transform.position;

                newVEGO.transform.rotation = this.transform.rotation;

                VisualEffect newVE = newVEGO.GetComponent<VisualEffect>();
                Debug.Log("PARTICULA");

                newVE.SetVector3("direccion", pruebaParticula);

                newVE.Play();

                StartCoroutine(desactivarParticula(newVEGO));
            }
            else
            {
                Debug.Log("no hay suficientes visual effects en la pool");
            }

            if (Physics.Raycast(this.transform.position, new Vector3(cameraBrain.transform.forward.x, cameraBrain.transform.forward.y, cameraBrain.transform.forward.z), out hit, 100, mask))
            {
                GameObject GOHit = hit.transform.gameObject;
                finalLinea = hit.point;
                Debug.Log("Choco con " + GOHit);

                if(GOHit.tag == "Player")
                {
                    Debug.Log("TE HE DADO");
                    //Destroy(GOHit);
                    return GOHit;
                }

                Vector3[] positions = new Vector3[]
                {
                    this.transform.position,
                    finalLinea
                };

                lineRenderer.SetPositions(positions);
            }
            else
            {
                return null;
            }
           
            StartCoroutine(borraLinea());
        }
        return null;
       
    }
    IEnumerator desactivarParticula(GameObject newVEGO)
    {
        yield return new WaitForSeconds(5);

        newVEGO.SetActive(false);

    }
    IEnumerator borraLinea()
    {
        yield return new WaitForSeconds(0.1f);
        lineRenderer.enabled = false;
    }
    public IEnumerator cooldown(float tiempoAEsperar)
    {
        puedoDisparar = false;
        
        yield return new WaitForSeconds(tiempoAEsperar);

        puedoDisparar = true;
    }
}
