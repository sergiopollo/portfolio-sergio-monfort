using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DestroyableBox : NetworkBehaviour
{
    public Material dmg1;
    public Material dmg2;

    [SyncVar(hook = nameof(Dmg))]
    public int health;

    private void Start()
    {
        health = 3;
    }

   void Dmg(int Old, int New)
    {
        if(health == 2)
            this.GetComponent<MeshRenderer>().material = dmg1;

        if(health == 1)
            this.GetComponent<MeshRenderer>().material = dmg2;
    }

    [ServerCallback]
    internal void danyar()
    {
        health--;
        if (health <= 0)
            Destroy(this.gameObject);   
    }


}
