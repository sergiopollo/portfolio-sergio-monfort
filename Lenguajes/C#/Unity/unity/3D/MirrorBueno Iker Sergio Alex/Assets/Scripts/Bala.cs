using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : NetworkBehaviour
{
    public int playerId;


    private void Start()
    {
        StartCoroutine(desaparecer());
    }

    [ServerCallback]
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("bala choca");
        
         if (collision.transform.tag == "Destroyable")
        {
            collision.gameObject.GetComponent<DestroyableBox>().danyar();
            Destroy(this.gameObject);
        }
        
    }


    IEnumerator desaparecer()
    {
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }
}
