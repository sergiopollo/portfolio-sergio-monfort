using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

[AddComponentMenu("")]
public class NetworkManagerDuelo : NetworkManager
{

    public Transform P1;
    public Transform P2;
    private int numJ = 0;
    NetworkConnectionToClient connP1 = null;
    //public GameObject obst;


    public override void OnServerAddPlayer(NetworkConnectionToClient conn)
    {

        // add player at correct spawn position   
        numJ++;
        Transform start = P1;
        if (numJ == 1)
        {
            connP1 = conn;
            Debug.Log("" + connP1);
            Debug.Log("TENEMOS 1 JUGADOR");
            //start = P1;
        }


        if (numJ == 2)
        {
            Debug.Log("TENEMOS 2 JUGADORES");
            Debug.Log("" + connP1);
            Debug.Log("" + conn);
            GameObject player = Instantiate(playerPrefab, P1.position, P1.rotation);
            NetworkServer.AddPlayerForConnection(connP1, player);

            GameObject player2 = Instantiate(playerPrefab, P2.position, P2.rotation);
            NetworkServer.AddPlayerForConnection(conn, player2);
            //start = P2;
            //start.Rotate(new Vector3(0, 0, 180));
        }

        /*
        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);
       
        
        if(numPlayers > 1)
        {
            Debug.Log("Start");
        }*/


    }
    
    public override void OnServerDisconnect(NetworkConnectionToClient conn)
    {

        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }

}



