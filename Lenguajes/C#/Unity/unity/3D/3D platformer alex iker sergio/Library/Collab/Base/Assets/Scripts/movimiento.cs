using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public float velocidad;
    private float velocidad_real;
    private bool controles;

    public PhysicMaterial mifriccion;

    public Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        controles = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (controles)
        {
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.forward.x * velocidad_real * -1, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.forward.z * velocidad_real * -1);
            }

            if (Input.GetKey("a"))
            {
                //this.transform.Rotate(new Vector3(0, -1, 0) * 2);
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.right.x * velocidad_real*-1, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.right.z * velocidad_real * -1);
            }
            else if (Input.GetKey("d"))
            {
                //this.transform.Rotate(new Vector3(0, 1, 0) * 2);
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.right.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.right.z * velocidad_real);

            }

            if (Input.GetKeyDown("space"))
            {
                this.GetComponent<Rigidbody>().AddForce(this.transform.up * 5, ForceMode.Impulse);
            }

            if (Input.GetKeyDown("c"))
            {
                StartCoroutine("Dash");
            }


            if (Input.GetKey((KeyCode.LeftShift)))
            {
                velocidad_real = velocidad * 2;
            }
            else
            {
                velocidad_real = velocidad;
            }
        }
       
    }

    IEnumerator Dash()
    {
        mifriccion.dynamicFriction = 0;
        mifriccion.bounciness = 1;
        controles = false;
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 20, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        mifriccion.dynamicFriction = 5f;
        //this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x *5, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z *5f);
        yield return new WaitForSeconds(0.5f);
        controles = true;
        mifriccion.dynamicFriction = 0.68f;
        mifriccion.bounciness = 0;
    }

}
