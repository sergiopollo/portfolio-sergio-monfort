using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoSergio : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject contenedor;
    public float velocidad;
    private float velocidad_real;

    public PhysicMaterial mifriccion;

    private States state;

    // Start is called before the first frame update
    void Start()
    {
        state = States.Caminar;
    }

    // Update is called once per frame
    void Update()
    {
        contenedor.transform.position = this.transform.position;


        if (state == States.Caminar || state == States.Saltar)
        {

            if (Input.GetKey("w") && Input.GetKey("d"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 45, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("w") && Input.GetKey("a"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 315, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("s") && Input.GetKey("d"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 90 + 45, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("s") && Input.GetKey("a"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 270 - 45, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("w"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("s"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 180, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("d"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 90, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("a"))
            {
                this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x, mainCamera.transform.eulerAngles.y + 90 * 3, this.transform.eulerAngles.z);
                this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z * velocidad_real);
            }


            if (Input.GetKeyDown("c"))
            {
                StartCoroutine("Dash");
            }


            if (Input.GetKey((KeyCode.LeftShift)))
            {
                velocidad_real = velocidad * 2;
            }
            else
            {
                velocidad_real = velocidad;
            }
        }

        if (Input.GetKeyDown("space") && state != States.Dash && state != States.Saltar && state != States.WallRun)
        {
            state = States.Saltar;
            this.GetComponent<Rigidbody>().AddForce(this.transform.up * 10, ForceMode.Impulse);
        }


    }

    IEnumerator Dash()
    {
        mifriccion.dynamicFriction = 0;
        mifriccion.bounciness = 1;
        state = States.Dash;
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * 20, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        mifriccion.dynamicFriction = 5f;
        //this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x *5, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z *5f);
        yield return new WaitForSeconds(0.5f);
        state = States.Caminar;
        mifriccion.dynamicFriction = 0.68f;
        mifriccion.bounciness = 0;
    }

    IEnumerator WallRun(Vector3 normal)
    {
        state = States.WallRun;
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;

        //Debug.Log(Vector3.Cross(this.transform.forward, normal).y);
        //Debug.Log(Vector3.Angle(this.transform.forward, normal));
        if (Vector3.Cross(this.transform.forward, normal).y > 0)
        {
            //print("CW");
            this.transform.Rotate(new Vector3(0, Vector3.Angle(this.transform.forward, normal) - 90, 0));
        }
        else
        {
            //print("CC");
            this.transform.Rotate(new Vector3(0, -Vector3.Angle(this.transform.forward, normal) + 90, 0));
        }

        // this.transform.Rotate(new Vector3());
        this.GetComponent<Rigidbody>().velocity = this.transform.forward * velocidad_real;

        //this.transform.Rotate(new Vector3(0, Vector3.Angle(this.gameObject.GetComponent<Rigidbody>().velocity, normal) + 90, 0));
        StartCoroutine(EndWallRun(normal));
        yield return new WaitForSeconds(2f);
        state = States.Caminar;
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        StopCoroutine("EndWallRun");

    }

    IEnumerator EndWallRun(Vector3 normalWall)
    {
        //Debug.Log("WOOOOOOOOOOOOOOO");
        bool end = false;
        while (!end)
        {
            yield return new WaitForSeconds(0.3f);
            if (Input.GetKey("space"))
            {
                //Debug.Log("WEEEEEEEEEEE");
                end = true;
                StopCoroutine("WallRun");
                state = States.Saltar;
                this.gameObject.GetComponent<Rigidbody>().useGravity = true;
                this.GetComponent<Rigidbody>().AddForce(this.transform.up * 10, ForceMode.Impulse);
                this.GetComponent<Rigidbody>().AddForce(normalWall * 10, ForceMode.Impulse);
            }
        }


    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Wall" && state == States.Saltar)
        {
            if (Input.GetKey((KeyCode.LeftShift)))
            {
                StartCoroutine(WallRun(collision.contacts[0].normal));
                //Debug.DrawRay(this.transform.position, collision.contacts[0].normal*10,Color.green,10f);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            mifriccion.dynamicFriction = 0;
            mifriccion.staticFriction = 0;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            state = States.Caminar;
            mifriccion.dynamicFriction = 0.68f;
            mifriccion.staticFriction = 0.68f;
        }

    }

}
