
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject targetObject;
    public Camera mainCamera;
    public float lerpTime;
    private States state;
    private float modx, mody, modz;
    public int minzoom, maxzoom;
    private int zoom;
    public float rotationSpeed = 7f;
    private float radius = 5.5f;
    private bool zonaCreepy = false;
    public GameObject luzGlobal;

    // The target that the camera looks at
    Vector3 target = new Vector3(0, 0, 0);

    // The spherical coordinates
    Vector3 sc = new Vector3();

    Vector3 cameraPosition = new Vector3();

    void Start()
    {
        zoom = 2;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        target = targetObject.transform.position;
        mainCamera.transform.position = new Vector3(radius, 0.0f, 0.0f);
        mainCamera.transform.LookAt(target);
        sc = getSphericalCoordinates(mainCamera.transform.position);
    }


    Vector3 getSphericalCoordinates(Vector3 cartesian)
    {
        float r = Mathf.Sqrt(
            Mathf.Pow(cartesian.x, 2) +
            Mathf.Pow(cartesian.y, 2) +
            Mathf.Pow(cartesian.z, 2)
        );

        float phi = Mathf.Atan2(cartesian.z / cartesian.x, cartesian.x);
        float theta = Mathf.Acos(cartesian.y / r);

        if (cartesian.x < 0)
            phi += Mathf.PI;

        return new Vector3(r, phi, theta);
    }

    Vector3 getCartesianCoordinates(Vector3 spherical)
    {
        Vector3 ret = new Vector3();

        ret.x = spherical.x * Mathf.Cos(spherical.z) * Mathf.Cos(spherical.y);
        ret.y = spherical.x * Mathf.Sin(spherical.z);
        ret.z = spherical.x * Mathf.Cos(spherical.z) * Mathf.Sin(spherical.y);

        return ret;
    }

    private void Update()
    {
        MouseWheeling();
    }
    void FixedUpdate()
    {
       
        target = new Vector3(targetObject.transform.position.x, targetObject.transform.position.y + 2, targetObject.transform.position.z);

        // Get the deltas that describe how much the mouse cursor got moved between frames
        // lo multiplico por rotacion en negativo para que se mueva con sentido
        float dx = Input.GetAxis("Mouse X") * -rotationSpeed;
        float dy = Input.GetAxis("Mouse Y") * -rotationSpeed;

        // Rotate the camera left and right
        sc.y += dx * Time.deltaTime;

        // Rotate the camera up and down
        // Prevent the camera from turning upside down (1.5f = approx. Pi / 2)
        sc.z = Mathf.Clamp(sc.z + dy * Time.deltaTime, -1.5f, 1.5f);

        // Calculate the cartesian coordinates for unity
        cameraPosition = getCartesianCoordinates(sc) + target;
        
        mainCamera.transform.position = new Vector3(

           Mathf.Lerp(mainCamera.transform.position.x, cameraPosition.x + (mainCamera.transform.forward.x + modx), Time.deltaTime * lerpTime),
           Mathf.Lerp(mainCamera.transform.position.y, cameraPosition.y + (mainCamera.transform.up.y * mody), Time.deltaTime * lerpTime),
           Mathf.Lerp(mainCamera.transform.position.z, cameraPosition.z + (mainCamera.transform.forward.z * modz), Time.deltaTime * lerpTime)

            );



        // Make the camera look at the target
        mainCamera.transform.LookAt(target);
        mainCamera.transform.rotation = Quaternion.RotateTowards(mainCamera.transform.rotation, targetObject.transform.rotation, Time.deltaTime * lerpTime);


        targetObject.transform.forward = new Vector3(mainCamera.transform.forward.x, targetObject.transform.forward.y, mainCamera.transform.forward.z);

    }

    public void cambioEstado(States estado)
    {
        state = estado;

        switch (state)
        {
            case States.Caminar:
                //Camara normal
                modx = 0;
                mody = 0;
                modz = 0;
                break;
            case States.Correr:
                //Camara atras
                modx = -1;
                mody = 0;
                modz = 0;
                break;
            case States.Saltar:
                //Camara un poco mas arriba
                modx = 0;
                mody = 2;
                modz = 0;
                break;
            case States.Dash:
                //Camara mas atras
                modx = -2;
                mody = 0;
                modz = 0;
                break;
            case States.WallRun:

                break;

        }

        //mainCamera.transform.position = new Vector3(posCam.x + xcam, posCam.y + ycam, posCam.z + zcam);

        //print(state);
    }

    void MouseWheeling()
    {
        Vector3 pos = mainCamera.transform.position;
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (zoom < maxzoom)
            {
                zoom++;

                sc += new Vector3(1, 0, 0);
                //print("A: " + zoom);
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (zoom > minzoom)
            {
                zoom--;
                sc -= new Vector3(1, 0, 0);
                //print("B: " + zoom);
            }

        }

    }


    public void activarZonaCreepy()
    {
        if (this.zonaCreepy)
        {
        Debug.Log("no vuelvas");
        this.zonaCreepy = false;
        //this.luzGlobal.SetActive(true);
        this.luzGlobal.transform.eulerAngles = new Vector3(50, -30, 0);

        }
        else
        {
        Debug.Log("cuidado con la oscuridad...");
        this.zonaCreepy = true;
        //this.luzGlobal.SetActive(false);
        this.luzGlobal.transform.eulerAngles = new Vector3(-90, 0, 0);
         //de esto se encargara el jugador
        //creepyCoroutine = StartCoroutine(segundosDeVidaEnOscuridad());
        }
    }

}
