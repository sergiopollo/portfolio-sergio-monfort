using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeRay : MonoBehaviour
{
    public float tiempoCongelacion=3;
    public float cooldownTime;
    public float rango;
    public LayerMask mask;
    public Material Congelado;
    private bool puedesDisparar = true;
    public LineRenderer lineRenderer;
    public Camera mainCamera;
    

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (puedesDisparar)
            {
                Debug.Log("pew");
                //FesmeUnRaycast();
                shoot();
                StartCoroutine(cooldown());
            }
        }
    }

    private void shoot()
    {
        //activa el linerenderer
        lineRenderer.enabled = true;

        //guarda la informaci sobre la collisio amb la que ha xocat el raycast
        RaycastHit hit;

        Vector3 finalLinea = new Vector3(this.transform.position.x + mainCamera.transform.forward.x * rango, this.transform.position.y + mainCamera.transform.forward.y * rango, this.transform.position.z + mainCamera.transform.forward.z * rango);

        //Physics.RaycastAll
        Debug.DrawRay(this.transform.position, mainCamera.transform.forward * rango, Color.blue, 2f);

        if (Physics.Raycast(this.transform.position, mainCamera.transform.forward * rango, out hit, rango, mask))
        { 
            GameObject GOHit = hit.transform.gameObject;

            finalLinea = hit.point;

            StartCoroutine(congelado(GOHit));
        }

        Vector3[] positions = new Vector3[]
        {

            this.transform.position,
            finalLinea
        };

        lineRenderer.SetPositions(positions);
        StartCoroutine(borraLinea());
    }

    IEnumerator borraLinea()
    {
        yield return new WaitForSeconds(0.1f);
        lineRenderer.enabled = false;
    }


    IEnumerator congelado(GameObject hit)
    {
        if (hit.GetComponent<MeshRenderer>().material != null)
        {
            Material materialGO = hit.GetComponent<MeshRenderer>().material;
            hit.GetComponent<MeshRenderer>().material = Congelado;
            bool miKinematic=true;
            bool miGravity=true;

            if (hit.GetComponent<Rigidbody>() != null)
            {

                miKinematic = hit.GetComponent<Rigidbody>().isKinematic;
                miGravity = hit.GetComponent<Rigidbody>().useGravity;

                hit.GetComponent<Rigidbody>().isKinematic = true;
                hit.GetComponent<Rigidbody>().useGravity = false;
            }

            yield return new WaitForSeconds(tiempoCongelacion);
         

            if (hit.GetComponent<Rigidbody>() != null)
            { 
                hit.GetComponent<Rigidbody>().isKinematic = miKinematic;
                hit.GetComponent<Rigidbody>().useGravity = miGravity;
            }

            hit.GetComponent<MeshRenderer>().material = materialGO;
        }
    }

    IEnumerator cooldown()
    {
        puedesDisparar = false;
        yield return new WaitForSeconds(cooldownTime);
        puedesDisparar = true;
    }

    

    

}
