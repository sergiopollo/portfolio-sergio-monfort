using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IkerCamara : MonoBehaviour
{
    public Camera mainCamera;
    public GameObject joan;
    private States state;
    private float sensitivityX = 2F;
    private float sensitivityY = 2F;
    private float minimumX = -360F;
    private float maximumX = 360F;
    private float minimumY = -90F;
    private float maximumY = 90F;
    private float movimientoDelRatonYQueAplicaRotacionEnX = -60F;
    private float rangoMin;
    private float rangoMax;


    void Start()
    {
        state = States.Caminar;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

    }


    void Update()
    {
        //LookAt con esta linea hacemos que la camara mire hacia un lugar en concreto, pero no la movemos.
        mainCamera.transform.LookAt(new Vector3(joan.transform.position.x, joan.transform.position.y + 2, joan.transform.position.z));

        if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
        {
            MouseMove();
        }
       // MouseWheeling(); 

    }


    public void cambioEstado(States estado)
    {
        state = estado;

        switch (state)
        {
            case States.Caminar:
                //Necesito que vuelva a colocarse en la posicion correcta del personaje
                // mainCamera.transform.position = new Vector3(contenedor.transform.parent.forward.x, contenedor.transform.position.y+2, contenedor.transform.forward.z-5);
                break;
            case States.Correr:
                //mainCamera.transform.position -= mainCamera.transform.forward;
                break;
            case States.Saltar:
                //mainCamera.transform.position += mainCamera.transform.up;
                break;
            case States.Dash:
                //mainCamera.transform.position -= mainCamera.transform.forward * 1.5f;
                break;
            case States.WallRun:

                break;

        }

        //mainCamera.transform.position = new Vector3(posCam.x + xcam, posCam.y + ycam, posCam.z + zcam);

        //print(state);
    }


    void MouseMove()
    {
        //agafo la rotacio actual i li sumo el moviment X del ratoli
        float movimientoDelRatonXQueAplicaRotacionEnY = joan.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

        //agafo la rotacio actual i li sumo el moviment X del ratoli
        //movimientoDelRatonYQueAplicaRotacionEnX += Input.GetAxis("Mouse Y") * sensitivityY;
        //movimientoDelRatonYQueAplicaRotacionEnX = Mathf.Clamp(movimientoDelRatonYQueAplicaRotacionEnX, minimumY, maximumY);
        //Clamp(valor, rangomin, rangomax)
        //si el valor es mas peque�o que rangomin pone rangomin
        //si el valor es mas grande que rangomax pone rangomax
        //si el valor esta entre medio lo deja igual

        joan.transform.localEulerAngles = new Vector3(joan.transform.localEulerAngles.x, movimientoDelRatonXQueAplicaRotacionEnY, 0);
        //mainCamera.transform.localEulerAngles = new Vector3(-movimientoDelRatonYQueAplicaRotacionEnX, 0, 0);

        //print(contenedor.transform.localEulerAngles + " iker");
    }

    /*void MouseWheeling()
    {
        Vector3 pos = mainCamera.transform.position;
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            pos = pos - mainCamera.transform.forward;
            mainCamera.transform.position = pos;
            //print("A");
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            pos = pos + mainCamera.transform.forward;
            mainCamera.transform.position = pos;
            //print("B");
        }

    }*/

}
