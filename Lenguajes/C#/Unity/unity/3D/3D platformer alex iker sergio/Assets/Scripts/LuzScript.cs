using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzScript : MonoBehaviour
{

    public GameObject conoLuz;
    public float TiempoActivo;
    private Vector3 posicionInicialConoluz;

    // Start is called before the first frame update
    void Start()
    {
        posicionInicialConoluz = conoLuz.transform.position;
        StartCoroutine(ApagaYEnciende());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ApagaYEnciende()
    {
        while (true)
        {
            yield return new WaitForSeconds(TiempoActivo);
            //se apaga
            conoLuz.transform.position = new Vector3(99999,99999,99999);
            yield return new WaitForSeconds(0.01f);
            conoLuz.SetActive(false);
            this.GetComponent<Light>().intensity = 0;
            yield return new WaitForSeconds(TiempoActivo);
            //se enciende
            conoLuz.transform.position = posicionInicialConoluz;
            conoLuz.SetActive(true);
            this.GetComponent<Light>().intensity = 5;
        }
    }
}
