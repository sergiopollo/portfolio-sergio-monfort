using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerState : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventState Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<States> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(States newstates)
    {
        Response.Invoke(newstates);
    }

}
