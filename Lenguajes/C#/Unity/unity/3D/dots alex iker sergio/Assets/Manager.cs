using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public static Manager patata;
    public Transform player;
    public Transform suelo;
    //private AudioSource audio;�
    
    public  AudioClip[] audioClips;

   
    public static void playSound(int sound, Vector3 soundPosition)
    {
        patata.PlaySound(sound, soundPosition);
    }


    public static Vector3 GetPlayerPosition()
    {


        return patata.player.transform.position;
    }

    public static Vector3 GetSueloPosition()
    {
        if (Manager.patata == null)
        {
            return Vector3.zero;
        }

        return (patata.suelo != null) ? Manager.patata.suelo.position : Vector3.zero;
    }

    void Awake()
    {
        if (patata != null && patata != this)
        {
            Destroy(gameObject);
        }
        else
        {
            patata = this;
            //this.audio =  patata.GetComponent<AudioSource>();
        }


      
    }

    public void PlaySound(int sound, Vector3 soundPosition)
    {
        if(sound == 1)
        {
            AudioSource.PlayClipAtPoint(audioClips[sound], soundPosition, 0.2f);
        }
        else
        {
            AudioSource.PlayClipAtPoint(audioClips[sound], soundPosition, 1.5f);
        }
       
    }


    // Start is called before the first frame update

}
