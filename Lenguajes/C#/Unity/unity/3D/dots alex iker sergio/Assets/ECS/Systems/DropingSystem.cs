using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class DropingSystem : SystemBase
{

    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer();

        float dt = Time.DeltaTime;

        Entities.WithAll<DropTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, in DropSpeed speed)=>
        {

            trans.Value += speed.dropSpeed * dt * (math.up()*-1);

        }).ScheduleParallel();

    }
}
