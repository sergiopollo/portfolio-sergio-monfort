using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

public class CollisionSystem : ComponentSystem
{

    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

    protected override void OnUpdate()
    {

        float dt = Time.DeltaTime;

        Entities.WithAll<DropTag>().ForEach((Entity entity, ref Translation trans) =>
        {
            Vector3 player = Manager.GetPlayerPosition();

            if (math.distance(trans.Value.y, -2) < 2f)
            {
                //PostUpdateCommands.AddComponent<AudioSource>(entity);
                 Manager.playSound(0, trans.Value);
                PostUpdateCommands.DestroyEntity(entity);
                //Debug.Log("A");
                
               
                
                
            }

            if (math.distance(trans.Value.y, player.y-2) < 3.2f && math.distance(trans.Value.x, player.x) < 1.2f && math.distance(trans.Value.z, player.z) < 1.2f)
            {
                PostUpdateCommands.DestroyEntity(entity);
                Manager.playSound(1, trans.Value);
            }
        });

    }
}
