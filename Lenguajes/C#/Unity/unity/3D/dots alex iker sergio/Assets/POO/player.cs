using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        //this.GetComponent<Rigidbody>().velocity = this.transform.forward * Input.GetAxis("Vertical")*speed + this.transform.right * Input.GetAxis("Horizontal")*speed;
        
        if (Input.GetKey(KeyCode.W))
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(this.transform.forward.x * speed, this.GetComponent<Rigidbody>().velocity.y, this.transform.forward.z * speed);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(this.transform.forward.x * -speed, this.GetComponent<Rigidbody>().velocity.y, this.transform.forward.z * -speed);
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(new Vector3(0,-5,0),Space.Self);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.transform.Rotate(new Vector3(0, 5, 0), Space.Self);
        }

    }
}
