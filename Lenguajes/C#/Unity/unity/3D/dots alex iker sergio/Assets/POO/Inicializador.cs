using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

public class Inicializador : MonoBehaviour, IConvertGameObjectToEntity
{
    public float minSpeed;
    public float maxSpeed;
    public Vector3 minSpawn;
    public Vector3 maxSpawn;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        Vector3 player = Manager.GetPlayerPosition();
        float x = Random.Range(minSpawn.x, maxSpawn.x);
        float z = Random.Range(minSpawn.z, maxSpawn.z);
        float velocidadRandom = Random.Range(minSpeed, maxSpeed);

        dstManager.SetComponentData(entity, new Translation { Value = new Vector3(player.x + x, maxSpawn.y, player.z + z) });
        dstManager.SetComponentData(entity, new DropSpeed { dropSpeed = velocidadRandom});
    }
}
