using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class luz : MonoBehaviour
{
    public bool alerta=false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void alertar()
    {
        if (this.alerta == false)
        {
            this.alerta = true;
            StartCoroutine(alertaCorrutina());
        }
    }

    IEnumerator alertaCorrutina()
    {
        this.GetComponent<Light>().color = Color.red;
        this.GetComponent<Light>().intensity = 15;
        yield return new WaitForSeconds(10f);
        this.GetComponent<Light>().color = Color.white;
        this.GetComponent<Light>().intensity = 1;
        this.alerta = false;
    }
}
