using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puerta : MonoBehaviour
{
    public bool isPuertaSalida;
    public bool alerta = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void alertar()
    {
        if (this.alerta == false)
        {
            this.alerta = true;
            StartCoroutine(alertaCorrutina());
        }
    }

    IEnumerator alertaCorrutina()
    {
        this.GetComponent<BoxCollider>().isTrigger = false;
        yield return new WaitForSeconds(10f);
        this.GetComponent<BoxCollider>().isTrigger = true;
        this.alerta = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.isPuertaSalida && other.tag == "espia")
        {
            Debug.Log("VICTORIA");
        }
    }


}
