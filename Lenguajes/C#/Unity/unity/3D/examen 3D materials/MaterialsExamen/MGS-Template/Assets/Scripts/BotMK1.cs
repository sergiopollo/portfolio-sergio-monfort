﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotMK1 : MonoBehaviour
{
    public Vector3 posicionSalaGuardias;
    private bool alerta = false;
    public Vector3 destination;
    public LayerMask mask;
    public GameObject player;
    bool HeVistoJugador = false;
    public GameEvent activaAlarmaOtraVEz;
    public GameObject linterna;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("FPSController");
        this.posicionSalaGuardias = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(HeVistoJugador);
        //Debug.Log(destination);
        if (alerta)
        {
            if (!HeVistoJugador)
            {
                RaycastHit hit;
                Vector3 raycastDir = player.transform.position - this.transform.position;

                Debug.DrawRay(this.transform.position, raycastDir, Color.blue, 2f);

                if (Physics.Raycast(this.transform.position, raycastDir, out hit, 20, mask))
                {
                    GameObject GOHit = hit.transform.gameObject;



                    Debug.Log("estoy chocando con " + GOHit + " con tag " + GOHit.tag);
                    if (GOHit.tag == "espia")
                    {
                        HeVistoJugador = true;
                    }

                }
            }
            else
            {
                //reinicia la alarma
                StopCoroutine(alertaCorrutina());
                //StartCoroutine(alertaCorrutina());
                this.activaAlarmaOtraVEz.Raise();

                this.GetComponent<NavMeshAgent>().SetDestination(player.transform.position);
            }
        }
        else
        {
            this.GetComponent<NavMeshAgent>().SetDestination(posicionSalaGuardias);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "espia")
        {
            collision.gameObject.GetComponent<Espia>().gameOver();
        }
    }


    public void alertar()
    {
        if (this.alerta==false)
        {
            this.alerta = true;
            this.linterna.SetActive(true);
            StartCoroutine(alertaCorrutina());
        }
    }
    
    IEnumerator alertaCorrutina()
    {
        if (!HeVistoJugador)
        {
            yield return new WaitForSeconds(0.2f);
            this.GetComponent<NavMeshAgent>().SetDestination(destination);
        }
        yield return new WaitForSeconds(10f);
        this.linterna.SetActive(false);
        this.alerta = false;
    }
    

}
