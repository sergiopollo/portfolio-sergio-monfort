﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espia : MonoBehaviour
{
    
    private bool documents = false;
    public Camera MiCamara;
    public Camera camaraSeguridad;
    public bool usandoCamSeguridad = false;
    public bool enSalaDeCamaras = false;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enSalaDeCamaras)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                print("Q a la sala de cameres");
                if (this.usandoCamSeguridad)
                {
                    print("bbbbbbbbbbbb");
                    this.usandoCamSeguridad = false;
                    this.camaraSeguridad.enabled = false;
                    this.MiCamara.enabled = true;
                }
                else
                {
                    print("aaaaaaa");
                    this.usandoCamSeguridad = true;
                    this.camaraSeguridad.enabled = true;
                    this.MiCamara.enabled = false;
                }
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Sala Cameres")
        {
            this.enSalaDeCamaras = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Sala Cameres")
        {
            this.enSalaDeCamaras = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("Espia: " + collision.transform.tag);
        if (collision.transform.tag == "guardia")
        {
            this.gameOver();
        }
        if (collision.transform.tag == "powerup")
        {
            print("He agafat els documents!");
            Destroy(collision.transform.gameObject);
            documents = true;
        }
        
    }

    /*
    private void OnTriggerStay(Collider other)
    {
        if(other.name== "Sala Cameres")
        {
            this.enSalaDeCamaras = true;
            
            if (Input.GetKeyDown(KeyCode.Q))
            {
                print("Q a la sala de cameres");
                if (this.usandoCamSeguridad)
                {
                    print("bbbbbbbbbbbb");
                    this.usandoCamSeguridad = false;
                    this.camaraSeguridad.enabled = false;
                    this.MiCamara.enabled = true;
                }
                else
                {
                    print("aaaaaaa");
                    this.usandoCamSeguridad = true;
                    this.camaraSeguridad.enabled = true;
                    this.MiCamara.enabled = false;
                }
            }
            
        }
    }
    */

    public void gameOver()
    {
        print("Game over");
        Destroy(this.gameObject);
    }
}
