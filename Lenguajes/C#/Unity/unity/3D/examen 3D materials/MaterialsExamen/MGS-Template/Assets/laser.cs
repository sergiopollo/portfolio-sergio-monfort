using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser : MonoBehaviour
{
    public GameEvent ge;
    public LayerMask mask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        Debug.DrawRay(this.transform.position, this.transform.forward*10, Color.red, 2f);

        if (Physics.Raycast(this.transform.position, this.transform.forward*10, out hit, 10, mask))
        {
            GameObject GOHit = hit.transform.gameObject;



            //Debug.Log("estoy chocando con " + GOHit + " con tag " + GOHit.tag);
            if (GOHit.tag == "espia")
            {
                ge.RaiseConDestination(this.transform.position);
            }

        }
    }
}
