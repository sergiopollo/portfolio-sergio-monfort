using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camSeguretat : MonoBehaviour
{
    public GameEvent ge;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(muevete());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        print("camSeguretat: " + other.name);
        if (other.tag == "espia")
        {
            print("espia detectat");

            ge.RaiseConDestination(this.transform.position);
            //ge.Raise();
        }
    }

    IEnumerator muevete()
    {
        while (true)
        {
            this.transform.Rotate(new Vector3(0,0,1));
            
            yield return new WaitForSeconds(10f);

            this.transform.Rotate(new Vector3(0,0,-1));
            yield return new WaitForSeconds(10f);
            
        }
    }
}

