﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotMK2 : MonoBehaviour
{
    public Vector3 posicionSalaGuardias;
    private bool alerta = false;
    public GameObject player;
    public GameObject linterna;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("FPSController");
        this.posicionSalaGuardias = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (alerta)
        {
            this.GetComponent<NavMeshAgent>().SetDestination(player.transform.position);
        }
        else
        {
            this.GetComponent<NavMeshAgent>().SetDestination(posicionSalaGuardias);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "espia")
        {
            collision.gameObject.GetComponent<Espia>().gameOver();
        }
    }

    public void alertar()
    {
        if (this.alerta == false)
        {
            this.alerta = true;
            this.linterna.SetActive(true);
            StartCoroutine(alertaCorrutina());
        }
    }

    IEnumerator alertaCorrutina()
    {
        yield return new WaitForSeconds(10f);
        this.linterna.SetActive(false);
        this.alerta = false;
    }

}