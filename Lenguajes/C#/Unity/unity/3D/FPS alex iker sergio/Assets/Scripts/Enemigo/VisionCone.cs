using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCone : MonoBehaviour
{

    private bool onArea;
    private GameObject objective;
    // Start is called before the first frame update
    void Start()
    {
        onArea = false;
        StartCoroutine("CheckItOut");
        //Debug.Log("Operativo");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator CheckItOut()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            if (onArea)
            {
             
                RaycastHit hit;
                Debug.DrawRay(this.transform.parent.position, (objective.transform.position - this.transform.parent.position), Color.black, 3f);
                if (Physics.Raycast(this.transform.parent.position, (objective.transform.position - this.transform.parent.position), out hit, 100))
                {
                   if(hit.transform.gameObject.tag == "Jugador")
                    {
                        //print("te veo");
                        GetComponentInParent<IA_Enemigo>().setVer(true);
                    }
                    else
                    {    
                        print(hit.transform.gameObject.tag);
                        GetComponentInParent<IA_Enemigo>().setVer(false);
                    }
                }
                onArea = false;   
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jugador")
        {
            //Debug.Log("Seguir al jugador");          
            objective = other.gameObject;
            onArea = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Jugador")
        {         
            onArea = false;
            objective = null;
            GetComponentInParent<IA_Enemigo>().setVer(false);
        }
    }
}
