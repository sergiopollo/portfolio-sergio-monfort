using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class canvas : MonoBehaviour
{

    private MovimientoPj jugador;
    private armaBasica armaActual;
    public RawImage mirilla;
    public TMPro.TextMeshProUGUI municion;
    public Slider sliderVida;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("jugador").GetComponent<MovimientoPj>();
        //tiene que esperar un poco a que el jugador se inicialize primero
        StartCoroutine(espera());
        sliderVida = this.GetComponentInChildren<Slider>();
        sliderVida.maxValue = jugador.maxVida;
        sliderVida.value = jugador.maxVida;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void cambioMirilla()
    {
        armaActual = jugador.armaActual;
        mirilla.texture = armaActual.statsArma.mirilla;
        updateMunicion();
    }

    public void updateMunicion()
    {
        //Debug.Log(armaActual);
        municion.text = "" + armaActual.balasActuales + "/" + armaActual.balasMaximas;
    }

    IEnumerator espera()
    {
        yield return new WaitForSeconds(0.1f);
        this.armaActual = jugador.armaActual;
        updateMunicion();
    }

    public void SetHealth()
    {
        sliderVida.value = jugador.vida;
    }
}
