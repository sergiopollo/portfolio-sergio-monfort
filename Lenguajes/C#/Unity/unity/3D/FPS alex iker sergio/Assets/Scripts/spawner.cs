using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public bool spawnerActivo = true;
    public float TiempoSpawneo;
    public float rangoSpawn;
    public GameObject enemigo;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnear());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnear()
    {
        while (spawnerActivo) {
            yield return new WaitForSeconds(TiempoSpawneo);
            GameObject newEnemigo = Instantiate(enemigo);
            newEnemigo.transform.position = new Vector3(this.transform.position.x + Random.Range(-rangoSpawn, rangoSpawn), this.transform.position.y, this.transform.position.z + Random.Range(-rangoSpawn, rangoSpawn));
        }
    }
}
