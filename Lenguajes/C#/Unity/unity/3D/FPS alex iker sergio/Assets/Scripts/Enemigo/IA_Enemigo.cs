using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IA_Enemigo : MonoBehaviour
{
    //PUBLIC
    public GameObject CoverDetection;
    //public GameObject cone;
    public GameObject Ragdoll;
    public GameObject NoRagdoll;
    public GameObject myGun;
    //PRIVATE
    private Transform Objective;
    private Estados_Enemigo state;
    private bool alive;
    private float speed = 0.5F;
    private Quaternion rot;
    private float vida = 100;
    private Dictionary<GameObject, float> covers;
    private bool teVeo;
    private bool meDisparan;
    private int trickery;
    private bool rotate;
    //public GameObject aaaa;
   

    // Start is called before the first frame update
    void Start()
    {
        Objective = GameObject.Find("jugador").transform;
        covers = new Dictionary<GameObject, float>();
        alive = true;
        state = Estados_Enemigo.Search;
        //rot = Quaternion.Euler(0, 0, 0);
        teVeo = false;
        meDisparan = false;
        trickery = 0;
        myGun.GetComponent<armaBasica>().miPadreEnemigo = this.gameObject;
        rotate = false;
        StartCoroutine("IA");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rotate)
        {
            this.transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 5);
        }
    }

    IEnumerator IA()
    {
        while (alive)
        {
            yield return new WaitForSeconds(0.5f);
            //print(state);
            switch (state)
            {
                case Estados_Enemigo.Search:
                    //print("---SEARCH---");
                    //ROTAR
                    rot = Quaternion.Euler(0, Random.Range(-180, 180), 0);
                    rotate = true;
                    yield return new WaitForSeconds(2f);
                          
                    rotate = false;
                    //VEO
                    if (meDisparan)
                    {
                        //print("Me estan disparando");
                        setState(Estados_Enemigo.Shoot);
                        break;
                    }
                    else if (teVeo)
                    {
                        //print("Te estoy viendo");
                        setState(Estados_Enemigo.Move);
                        break;
                    }
                    else
                    {
                        //RAYCAST
                        RaycastHit hit;
                        Debug.DrawRay(this.transform.position, this.transform.forward * 2, Color.cyan, 5f);
                        if (Physics.Raycast(this.transform.position, this.transform.forward * 2, out hit, 2))
                        {
                            //OBS
                            if (hit.transform.gameObject.tag == "Obstaculo")
                            {
                                //POS NADA JAJA SALU2
                                //print("Pos esta wapa la pared tu");
                            }
                            else
                            {
                                //MOVERME
                                //print("ME VOY A MOVER AHORA"); 
                                this.GetComponent<NavMeshAgent>().SetDestination(this.transform.position + (this.transform.forward * 5));
                                yield return new WaitForSeconds(2f);
                                //VEO
                                if (meDisparan)
                                {
                                    //print("Me estan disparando");
                                    setState(Estados_Enemigo.Shoot);
                                    break;
                                }
                                else if (teVeo)
                                {
                                    setState(Estados_Enemigo.Move);
                                    break;
                                }
                                else
                                {
                                    //print("Vuelta a patrullar");
                                }
                            }
                        }
                        else
                        {
                            //MOVERME
                            //print("ME VOY A MOVER AHORA");
                            //this.GetComponent<NavMeshAgent>().SetDestination(new Vector3(10,0,10));
                            this.GetComponent<NavMeshAgent>().SetDestination(this.transform.position + (this.transform.forward * 3));
                            yield return new WaitForSeconds(2f);
                            //VEO
                            if (meDisparan)
                            {
                                //print("Me estan disparando");
                                setState(Estados_Enemigo.Shoot);
                                break;
                            }
                            else if (teVeo)
                            {
                                setState(Estados_Enemigo.Move);
                                break;
                            }
                            else
                            {
                                //print("Vuelta a patrullar");
                            }
                        }
                    }

                    //print(rot.y);                  
                    break;
                case Estados_Enemigo.Move:
                    //print("---MOVE---");
                    Vector3 des = Objective.position - (this.transform.forward*15);
                    this.GetComponent<NavMeshAgent>().SetDestination(des);
                        setState(Estados_Enemigo.Shoot);
                    

                    break;
                case Estados_Enemigo.TakeCover:
                    //print("---TAKE COVER---");
                    CoverDetection.SetActive(true);
                    yield return new WaitForSeconds(0.5f);
                    CoverDetection.SetActive(false);
                    float min = 1000;
                    foreach (KeyValuePair<GameObject, float> cov in covers)
                    {
                        if (cov.Value < min)
                        {
                            min = cov.Value;
                        }
                    }

                    foreach (KeyValuePair<GameObject, float> cov in covers)
                    {
                        if (cov.Value == min)
                        {
                            //print(cov.Key);
                            Vector3 sus = calcularDistMin(cov.Key);
                            //print(sus);

                                this.GetComponent<NavMeshAgent>().SetDestination(sus);  

                            //print("por que se mueva");
                            //GameObject target = Instantiate(aaaa);
                            //target.transform.position = calcularDistMin(cov.Key).position;
                            //target.GetComponent<MeshRenderer>().material.color = Color.cyan;
                        }
                    }
                    covers.Clear();
                    yield return new WaitForSeconds(3f);
                    setState(Estados_Enemigo.Recharge);
                    break;
                case Estados_Enemigo.Shoot:
                    //print("---SHOOT---");

                    while (myGun.GetComponent<armaBasica>().balasActuales > 0)
                    {
                        this.transform.LookAt(Objective);
                        myGun.transform.LookAt(Objective);
                       myGun.GetComponent<armaBasica>().shootEnemigo();
                       yield return new WaitForSeconds(myGun.GetComponent<armaBasica>().cadenciaTime);
                        //print("DISPARO");
                    }
                    setState(Estados_Enemigo.TakeCover);
                    break;
                case Estados_Enemigo.Recharge:
                    //print("---RECHARGE---");
                    for (int i = 0; i < myGun.GetComponent<armaBasica>().ReloadTime; i++)
                    {
                        print(i);
                        yield return new WaitForSeconds(1f);
                    }
                    myGun.GetComponent<armaBasica>().balasActuales = myGun.GetComponent<armaBasica>().balasMaximas;

                    if (meDisparan)
                    {
                        setState(Estados_Enemigo.Shoot);
                    }
                    else
                    {
                        setState(Estados_Enemigo.Search);
                    }
                   
                    break;
            }
        }
    }

    public void setState(Estados_Enemigo state)
    {
        this.state = state;
    }

    public Estados_Enemigo getState()
    {
        return state;
    }

    public void setVer(bool v)
    {
        teVeo = v;
    }
    public void getHurt(float dmg)
    {
        //print("Au, me han dado / " + this.vida);
        this.vida -= dmg;
        if (vida <= 0)
        {
            StopAllCoroutines();
            alive = false;
            this.GetComponent<NavMeshAgent>().isStopped = true;
            myGun.SetActive(false);
            Ragdoll.SetActive(true);
            NoRagdoll.SetActive(false);
            StartCoroutine("Desaparecer");
        }
        else
        {
            StartCoroutine("Disparado");
        }
    }

    public IEnumerator Desaparecer()
    {
        yield return new WaitForSeconds(5f);
        this.gameObject.SetActive(false);
    }
    public IEnumerator Disparado() {

        if(meDisparan == false)
        {         
            meDisparan = true;
            while(trickery < 5)
            {
                //print("HEEEEEEEEEEEEEEEEEEEEEEEEY");
                yield return new WaitForSeconds(1f);
                trickery++;
            }           
            meDisparan = false;
        }
        else
        {
            trickery = 0;
        }
        
    }


    public Vector3 calcularDistMin(GameObject go){

        

        Vector3 pos = this.transform.position;

        if (go.transform.localScale.x > go.transform.localScale.z)
        {
            //calculamos las dos puntas en x
            Vector3 ladoPositivo = go.transform.position + (go.transform.right * (go.transform.localScale.x / 2));
            Vector3 ladoNegativo = go.transform.position + (-go.transform.right * (go.transform.localScale.x / 2));

            //calculamos los dos lados en z
            Vector3 z1 = go.transform.position + (go.transform.forward * (go.transform.localScale.z / 2));
            Vector3 z2 = go.transform.position + (-go.transform.forward * (go.transform.localScale.z / 2));

            //Calculamos mi distancia hasta cada punta en x
            float dist1 = Vector3.Distance(ladoPositivo, this.transform.position);
            float dist2 = Vector3.Distance(ladoNegativo, this.transform.position);

            //Calculamos la distancia del Jugador de cada lado de z
            float distOb1 = Vector3.Distance(z1, Objective.position);
            float distOb2 = Vector3.Distance(z2, Objective.position);

            /*//Bolitas x
            GameObject ca = Instantiate(aaaa);
            ca.transform.position = go.transform.position+(go.transform.right * (go.transform.localScale.x/2));
            GameObject ca2 = Instantiate(aaaa);
            ca2.transform.position = go.transform.position + (-go.transform.right * (go.transform.localScale.x / 2)); ;
       
            //Bolitas z
            GameObject ca3 = Instantiate(aaaa);
            ca3.transform.position = go.transform.position+(go.transform.forward * (go.transform.localScale.z/2));
            ca3.GetComponent<MeshRenderer>().material.color = Color.blue;
            GameObject ca4 = Instantiate(aaaa);
            ca4.transform.position = go.transform.position + (-go.transform.forward * (go.transform.localScale.z / 2));
            ca4.GetComponent<MeshRenderer>().material.color = Color.blue;
            */
            //si estoy mas cerca de dist1
            if (dist1 < dist2)
            {
                //print("estoy mas cerca de dist1");
                //si el lado distOb1 esta mas lejos del jugador
                if (distOb1 > distOb2)
                {
                    //print("estoy mas cerca de distob2");

                    pos = ladoPositivo + go.transform.forward * ((go.transform.localScale.z / 2) + 1);
                    pos -= go.transform.right;

                    //GameObject ca5 = Instantiate(aaaa);
                    //ca5.transform.position = trans.position;
                    //ca5.GetComponent<MeshRenderer>().material.color = Color.black;
                }
                else
                {
                    pos = ladoPositivo - go.transform.forward * ((go.transform.localScale.z / 2)+1);
                    pos -= go.transform.right;

                    //GameObject ca5 = Instantiate(aaaa);
                    //ca5.transform.position = trans.position;
                    //ca5.GetComponent<MeshRenderer>().material.color = Color.black;
                }
              
            }
            else
            {
                //print("estoy mas cerca de dist2");
                if (distOb1 > distOb2)
                {
                    //print("estoy mas cerca de distob2");

                    pos = ladoNegativo + go.transform.forward * ((go.transform.localScale.z / 2) + 1);
                    pos += go.transform.right;
                    //GameObject ca5 = Instantiate(aaaa);
                    //ca5.transform.position = trans.position;
                    //ca5.GetComponent<MeshRenderer>().material.color = Color.black;

                }
                else
                {
                    //print("estoy mas cerca de distob1");
                    pos = ladoNegativo - go.transform.forward * ((go.transform.localScale.z / 2) + 1);
                    pos += go.transform.right;
                }
            }

        }
        else
        {
            //calculamos las dos puntas en x
            Vector3 ladoPositivo = go.transform.position + (go.transform.forward * (go.transform.localScale.z / 2));
            Vector3 ladoNegativo = go.transform.position + (-go.transform.forward * (go.transform.localScale.z / 2));

            //calculamos los dos lados en z
            Vector3 x1 = go.transform.position + (go.transform.right * (go.transform.localScale.x / 2));
            Vector3 x2 = go.transform.position + (-go.transform.right * (go.transform.localScale.x / 2));

            //Calculamos mi distancia hasta cada punta en x
            float dist1 = Vector3.Distance(ladoPositivo, this.transform.position);
            float dist2 = Vector3.Distance(ladoNegativo, this.transform.position);

            //Calculamos la distancia del Jugador de cada lado de z
            float distOb1 = Vector3.Distance(x1, Objective.position);
            float distOb2 = Vector3.Distance(x2, Objective.position);


            if (dist1 < dist2)
            {
                if (distOb1 > distOb2)
                {
                    pos = ladoPositivo + go.transform.right * ((go.transform.localScale.x / 2) + 1);
                    pos -= go.transform.forward;
                }
                else
                {
                    pos = ladoPositivo - go.transform.right * ((go.transform.localScale.x / 2) + 1);
                    pos -= go.transform.forward;
                }

            }
            else
            {
               
                if (distOb1 > distOb2)
                {
                    //print("A");
                    pos = ladoNegativo + go.transform.right * ((go.transform.localScale.x / 2) + 1);
                    pos += go.transform.forward;
                }
                else
                {
                    pos = ladoNegativo - go.transform.right * ((go.transform.localScale.x / 2) + 1);
                    pos += go.transform.forward;
                }
            }

        }


        return pos;
    }

    public void addCovers(GameObject cover)
    {

        Vector3 a, b;


        if (cover.transform.localScale.x > cover.transform.localScale.z)
        {
            a = cover.transform.position + (cover.transform.right / 2);
            b = cover.transform.position - (cover.transform.right / 2);      
        }
        else
        {
            a = cover.transform.position + (cover.transform.forward / 2);
            b = cover.transform.position - (cover.transform.forward / 2);
        }

        float dist1 = Vector3.Distance(a, this.transform.position);
        float dist2 = Vector3.Distance(b, this.transform.position);

        if (dist1 < dist2)
        {
            covers.Add(cover, dist1);
        }
        else
        {
            covers.Add(cover, dist2);
        }


    }
}
