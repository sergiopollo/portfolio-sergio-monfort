using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoverDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Obstaculo")
        {
            //print(other.transform.localScale.x);
            //print(other.transform.localScale.z);

            GetComponentInParent<IA_Enemigo>().addCovers(other.gameObject);
        }
    }

}
