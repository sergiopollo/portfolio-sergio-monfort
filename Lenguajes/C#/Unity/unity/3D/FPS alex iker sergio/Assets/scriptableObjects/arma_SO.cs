using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/arma", order = 1)]
public class arma_SO : ScriptableObject
{
    public float cadenciaTime;
    public float ReloadTime;
    public float rango;
    public float balasMaximas;
    public float dispersion;
    public float numBalasDisparo;
    public float damage;
    public Texture2D mirilla;

}
