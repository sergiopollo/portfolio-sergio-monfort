using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxDestruible : MonoBehaviour
{
  
    private int duplicateChance;
    public Material hp2;
    public Material hp1;
    public int hp;
    public habilidadSO pocion;
  

    // Start is called before the first frame update
    void Start()
    {
     
        duplicateChance = Random.RandomRange(0, 100);
        hp = 1;
       duplicate();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (hp <= 0)
        {
            
            if(duplicateChance >20 && duplicateChance< 70)
            {
                GameObject newCoin = coinPool.SharedInstance.GetPooledObject();
                newCoin.transform.position = this.transform.position;
                newCoin.SetActive(true);
            }
           
            if(duplicateChance >20 && duplicateChance < 30)
           {
                GameObject objeto = armasPool.SharedInstance.GetPooledObject();
                objeto.transform.position = this.transform.position;
                objeto.GetComponent<Objeto>().cambiarHabilidad(pocion);
                objeto.SetActive(true);
            }
            AudioSource.PlayClipAtPoint(this.GetComponent<AudioSource>().clip, this.transform.position, 1);
            Destroy(this.gameObject);            
        }
    }

   

    public void duplicate()
    {


        if (duplicateChance>40 && duplicateChance<60)
        {
       
            Instantiate(this.gameObject, new Vector3(this.transform.position.x, this.transform.position.y + 1.2f, this.transform.position.z), Quaternion.identity);
          //  Instantiate(this.gameObject, new Vector3(this.transform.position.x+ 1.2f, this.transform.position.y , this.transform.position.z), Quaternion.identity);

        }
    }

   

}
