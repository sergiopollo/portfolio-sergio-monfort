using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DungeonGen;

public class aranyaPruebaSergio : MonoBehaviour
{
    //Tama�o de la matriz
    [Header("RangoMatriz")]
    public float rangoMatrizX;
    public float rangoMatrizZ;

    //Tipos de salas
    [Header("Tipos de salas")]
    public Sala spawn;
    public Sala[] tiposDeSalas;
    public GameObject piezaSpawn;
    public GameObject[] PiezasMapa;
    public GameObject pasillo;
    public GameObject[] TPS;
    public Sala Puzle;
    public GameObject piezaPuzle;

    //Porcentajes de salas
    [Header("Procentajes salas (Asegurar que suma 100)")]
    public int salaElite_p;
    public int salaNormal_p;
    public int salaRectangular_p;
    public int salaEscaleras_p;

    //Variables de entrada
    [Header("Parametros generacion")]
    public float RadioSalas;
    public int NumIntentosGenerar;
    public int maxEscalerasPiso;
    public int AlturaPisoArriba;
    public int AlturaPisoAbajo;
    public int AlturaEscalerasArriba;
    public int AlturaEscalerasAbajo;
    public GameObject ElComePuertas;

    //Variables privadas
    private float side;
    private int[,] matriz;
    private List<Vector2> salas;
    private List<Vector2> superior;
    private List<Vector2> inferior;
    private List<GameObject> salasGeneradas;
    private List<GameObject> salasGeneradasSuperior;
    private List<GameObject> salasGeneradasInferior;
    private int numEscalerasArriba;
    private int numEscalerasAbajo;
    private int pisoBoss;


    //pruebas chungas
    public GameObject Pilar;
    private HashSet<Point> points;
   

    // Start is called before the first frame update
    void Start()
    {
        //CrearPasillo(new Vector3(0, 0, 0), new Vector3(10,0,10));
        numEscalerasAbajo = 0;
        numEscalerasArriba = 0;
        pisoBoss = Random.RandomRange(0, 2);
        superior = new List<Vector2>();
        inferior = new List<Vector2>();
        salasGeneradas = new List<GameObject>();
        salasGeneradasSuperior = new List<GameObject>();
        salasGeneradasInferior = new List<GameObject>();

        points = new HashSet<Point>();
        this.transform.position = new Vector3(0, 0, 0);


        //El tama�o del lado de una de las casillas de la matriz
        side = RadioSalas / Mathf.Sqrt(2);
        Debug.Log("Side:" + side);
        //la matriz de puntos que generaremos 
        matriz = new int[(int)(rangoMatrizX / side), (int)(rangoMatrizZ / side)];
        Debug.Log("Tama�o de la matriz: " + (matriz.GetLength(0)-1) + ", " + (matriz.GetLength(1)-1));

   
        GameObject Pilare1 = Instantiate(Pilar);
        Pilare1.transform.position = new Vector3(0, 0, 0);

        GameObject Pilare2 = Instantiate(Pilar);
        Pilare2.transform.position = new Vector3(rangoMatrizX, 0, 0);

        GameObject Pilare3 = Instantiate(Pilar);
        Pilare3.transform.position = new Vector3(0, 0, rangoMatrizZ);

        GameObject Pilare4 = Instantiate(Pilar);
        Pilare4.transform.position = new Vector3(rangoMatrizX, 0, rangoMatrizZ);



        //Lista donde guardaremos las coordenadas de las salas a generar
        salas = new List<Vector2>();

        //StartCoroutine(Generar());

        StartCoroutine("PoissonPrincipal");
    }

    //Genera cuantas salas pueda dentro de la matriz con una distribucion de poisson
    IEnumerator PoissonPrincipal()
    {

        //Aqui estan las salas que aun no han gastado su posibilidad de generar una sala alrededor
        List<Vector2> salasActivas = new List<Vector2>();

        salasActivas.Add(new Vector2(rangoMatrizX/2, rangoMatrizZ / 2));
        salas.Add(new Vector2(rangoMatrizX / 2, rangoMatrizZ / 2));
        matriz[(int)Mathf.Clamp(((rangoMatrizX / 2) / side), 0, matriz.GetLength(0) - 1), (int)Mathf.Clamp(((rangoMatrizZ / 2) / side), 0, matriz.GetLength(1) - 1)] = salas.Count;

        GameObject salaSpawnMapa = Instantiate(piezaSpawn);
        salaSpawnMapa.transform.position = new Vector3(rangoMatrizX / 2 + 3000, 0, rangoMatrizZ / 2);
        GameObject SalaSpawn = spawn.tipoSala;
        GameObject Spawn = Instantiate(SalaSpawn);
        Spawn.transform.position = new Vector3(rangoMatrizX / 2, 0, rangoMatrizZ / 2);
        salasGeneradas.Add(Spawn);

        //Mientras queden salas activas para generar mas salas
        while (salasActivas.Count > 0)
        {
            //Elegimos una sala de las salas activas para mirar a su alrededor
            int Index = Random.RandomRange(0, salasActivas.Count);
            Vector2 sala = (salasActivas[Index]);

            bool salaGenerada = false;
            //Intentamos generar salas alrededor de la sala generada durante un maximo numero de intentos NumIntentosGenerar
            for(int i = 0; i < NumIntentosGenerar; i++)
            {
                yield return new WaitForSeconds(0.0005f);
                //Angulo aleatorio para colocar la sala (Las salas se generan en un circulo alrededor de la sala activa)
                float angulo = Random.value * Mathf.PI * 2;
                Vector2 direcion = new Vector2(Mathf.Sin(angulo), Mathf.Cos(angulo));

                //La salaCandidata se genera entre el Radio y dos veces el radio en la direccion aleatoria.
                Vector2 salaCandidata = sala + direcion * Random.Range(RadioSalas, RadioSalas * 2);

                //Si la sala no entra en el radio de ninguna otra sala se a�ade a la lista de salas
                if (ValidarSala(salaCandidata, matriz, salas)){
                    salas.Add(salaCandidata);
                    salasActivas.Add(salaCandidata);
                    salaGenerada = true;

                    //Metemos en la matriz el indice de la sala que acabamos de generar
                    //Debug.Log(" La X de la sala en la matriz es: "+(int)(salaCandidata.x / side));
                    //Debug.Log(" La Y de la sala en la matriz es: " + (int)(salaCandidata.y / side));
                    //Debug.Log("-------------------------------------");
                    matriz[(int)Mathf.Clamp((salaCandidata.x / side),0,matriz.GetLength(0)-1), (int)Mathf.Clamp((salaCandidata.y / side),0,matriz.GetLength(1)-1)] = salas.Count;
                    //matriz[(int)(salaCandidata.x / side), (int)(salaCandidata.y)] = salas.Count;
                    break;
                }

            }

            //En caso de que no se haya generado una nueva sala al agotar los intentos, quitamos la sala de las salas activas
            if (!salaGenerada)
            {
                salasActivas.RemoveAt(Index);
                //Debug.Log("Numero de salas activas restantes: " + salasActivas.Count);
            }


            if (salasActivas.Count > 100)
            {
                break;
            }

            
        }
            ColocarSalas(false, 0, salas);
    }

    //Esta funcion genera la distribucion de los otros niveles y recibe como parametro de salas activas las escaleras
    IEnumerator SubPoisson(List<Vector2> escaleras, int altura)
    {
        int[,] subMatriz = new int[(int)(rangoMatrizX / side), (int)(rangoMatrizZ / side)];
        List<Vector2> subSalas = new List<Vector2>();
        List<Vector2> salasActivas = new List<Vector2>();

        //Rellenamos las salas del nivel con las escaleras y las metemos en la subMatriz
        foreach(Vector2 sala in escaleras)
        {
            salasActivas.Add(sala);
            subSalas.Add(sala);
            subMatriz[(int)Mathf.Clamp((sala.x / side), 0, subMatriz.GetLength(0) - 1), (int)Mathf.Clamp((sala.y / side), 0, subMatriz.GetLength(1) - 1)] = subSalas.Count;
        }
        //Debug.Log(subSalas.Count); CORRECTO
        //Debug.Log(salasActivas.Count); CORRECTO

        //Mientras queden salas activas para generar mas salas
        while (salasActivas.Count > 0)
        {
            //Elegimos una sala de las salas activas para mirar a su alrededor
            int Index = Random.RandomRange(0, salasActivas.Count);
            //Debug.Log("Index: " + Index);
            Vector2 sala = (salasActivas[Index]);

            bool salaGenerada = false;
            //Intentamos generar salas alrededor de la sala generada durante un maximo numero de intentos NumIntentosGenerar
            for (int i = 0; i < NumIntentosGenerar; i++)
            {
                yield return new WaitForSeconds(0.005f);
                //Angulo aleatorio para colocar la sala (Las salas se generan en un circulo alrededor de la sala activa)
                float angulo = Random.value * Mathf.PI * 2;
                Vector2 direcion = new Vector2(Mathf.Sin(angulo), Mathf.Cos(angulo));

                //La salaCandidata se genera entre el Radio y dos veces el radio en la direccion aleatoria.
                Vector2 salaCandidata = sala + direcion * Random.Range(RadioSalas, RadioSalas * 2);

                //Si la sala no entra en el radio de ninguna otra sala se a�ade a la lista de salas
                if (ValidarSala(salaCandidata, subMatriz, subSalas))
                {
                    subSalas.Add(salaCandidata);
                    salasActivas.Add(salaCandidata);
                    salaGenerada = true;

                    //Metemos en la matriz el indice de la sala que acabamos de generar
                    //Debug.Log(" La X de la sala en la matriz es: "+(int)(salaCandidata.x / side));
                    //Debug.Log(" La Y de la sala en la matriz es: " + (int)(salaCandidata.y / side));
                    //Debug.Log("-------------------------------------");
                    subMatriz[(int)Mathf.Clamp((salaCandidata.x / side), 0, subMatriz.GetLength(0) - 1), (int)Mathf.Clamp((salaCandidata.y / side), 0, subMatriz.GetLength(1) - 1)] = subSalas.Count;
                    //matriz[(int)(salaCandidata.x / side), (int)(salaCandidata.y)] = salas.Count;
                    //int Indexsala = Random.Range(0, tiposDeSalas.Length - 1);
                    //GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                    //salaAGenerar.transform.position = new Vector3(salaCandidata.x, altura, salaCandidata.y);
                    break;
                }

            }

            //En caso de que no se haya generado una nueva sala al agotar los intentos, quitamos la sala de las salas activas
            if (!salaGenerada)
            {
                salasActivas.RemoveAt(Index);
                //Debug.Log("Numero de salas activas restantes: " + salasActivas.Count);
            }


            if (salasActivas.Count > 100)
            {
                break;
            }


        }
        ColocarSalas(true, altura, subSalas);
    }

    public void ColocarSalas(bool nivel, int altura, List<Vector2> salas)
    {
        int escaleras = 0;
        bool StopEscaleras = false;
        bool par = true;
        int Indexsala = 0;
      


        //nivel comprueba si es el piso principal o los otros
        if (!nivel)
        {
            int esc = 0;
            foreach (Vector2 sala in salas.GetRange(1, salas.Count - 1))
            {
                if (!StopEscaleras)//Ya hay el maximo de escaleras
                {
                    //Aseguramos una escalera para arriba y una para abajo
                    if(esc < 2)
                    {
                        Indexsala = tiposDeSalas.Length-1;
                    }
                    else//Tiramos de probabilidades
                    {
                        Indexsala = MarcRandom(0, salas.Count);                      
                    }
                   
                }
                else
                {
                    Indexsala = MarcRandom(1, salas.Count);
                    //Debug.Log("YA NO MAS ESCALERAS");
                }

                //El ultimo espacio de tipos de sala corresponde a las salas de escaleras
                //En caso de escaleras tenemos un contador de cuantas han aparecido para que no haya demasiadas
                //La primera escalera siempre ira hacia abajo, y la segunda hacia arriba, y asi...
                //Las escaleras se suman a listas distintas para generar mas tarde las salas inferiores y superiores
                if (Indexsala == tiposDeSalas.Length - 1)
                {
                    escaleras++;
                    
                    if (par)
                    {
                        numEscalerasArriba++;
                        par = false;
                        GameObject salaAGenerarMapa = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerarMapa2 = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x)+4000, 1, Mathf.Floor(sala.y));
                        salaAGenerarMapa2.transform.position = new Vector3(Mathf.Floor(sala.x) + 3000, 1, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(Mathf.Floor(sala.x), AlturaEscalerasArriba, Mathf.Floor(sala.y));
                        salasGeneradas.Add(salaAGenerar);
                        superior.Add(new Vector2(salaAGenerar.transform.position.x, salaAGenerar.transform.position.z));
                        salasGeneradasSuperior.Add(salaAGenerar);
                    }
                    else
                    {
                        numEscalerasAbajo++;
                        par = true;
                        GameObject salaAGenerarMapa = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerarMapa2 = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 2000, 1, Mathf.Floor(sala.y));
                        salaAGenerarMapa2.transform.position = new Vector3(Mathf.Floor(sala.x) + 3000, 1, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(Mathf.Floor(sala.x), AlturaEscalerasAbajo, Mathf.Floor(sala.y));
                        salasGeneradas.Add(salaAGenerar);
                        inferior.Add(new Vector2(salaAGenerar.transform.position.x, salaAGenerar.transform.position.z));
                        salasGeneradasInferior.Add(salaAGenerar);
                    }
                    //Debug.Log("ESCALERA");               
                    if (escaleras > maxEscalerasPiso - 1)
                        StopEscaleras = true;
                }
                else
                {
                    GameObject salaAGenerarMapa = Instantiate(PiezasMapa[Indexsala]);
                    GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                    salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 3000, 1, Mathf.Floor(sala.y));
                    salaAGenerar.transform.position = new Vector3(Mathf.Floor(sala.x), 0, Mathf.Floor(sala.y));
                    salasGeneradas.Add(salaAGenerar);
                }

                
            }
            //Una vez terminado de poner las salas hay que generar el poisson de los pisos superiores e inferiores

            //Debug.Log("Hay " + inferior.Count + " escaleras hacia abajo.");
            //Debug.Log("Hay " + superior.Count + " escaleras hacia arriba.");
            StartCoroutine(SubPoisson(superior, AlturaPisoArriba));
            StartCoroutine(SubPoisson(inferior, AlturaPisoAbajo));
            GenerarPasillos(salasGeneradas, altura);
        }
        else//PISOS ARRIBA Y ABAJO
        {
            //Debug.Log("Soy el piso: " + altura + " tengo: "+ salas.Count + " salas");
            if(altura == AlturaPisoArriba)
            {
                int sustituir = 0;
                if (pisoBoss == 1)
                {
                    sustituir = Random.RandomRange(numEscalerasArriba, salas.Count);
                }               
                foreach (Vector2 sala in salas.GetRange(numEscalerasArriba, salas.Count - numEscalerasArriba))//Colocar salas arriba
                {
                    if (sustituir != 0 && salas[sustituir] == sala)//SI esta es la sala a sustituir
                    {
                        GameObject salaAGenerarMapa = Instantiate(piezaPuzle);
                        GameObject salaAGenerar = Instantiate(Puzle.tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 4000, 0, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(sala.x, altura, sala.y);
                        salasGeneradasSuperior.Add(salaAGenerar);
                    }
                    else
                    {
                        Indexsala = MarcRandom(1, salas.Count);
                        GameObject salaAGenerarMapa = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 4000, 0, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(sala.x, altura, sala.y);
                        salasGeneradasSuperior.Add(salaAGenerar);
                    }
                    //Debug.Log("A");
                    
                }
            }
            else
            {
                int sustituir = 0;
                if (pisoBoss == 0)
                {
                    sustituir = Random.RandomRange(numEscalerasAbajo, salas.Count);
                }
                foreach (Vector2 sala in salas.GetRange(numEscalerasAbajo, salas.Count - numEscalerasAbajo))//Colocar salas abajo
                {
                    //Debug.Log("A");
                    if (sustituir != 0 && salas[sustituir] == sala)//SI esta es la sala a sustituir
                    {
                        GameObject salaAGenerarMapa = Instantiate(piezaPuzle);
                        GameObject salaAGenerar = Instantiate(Puzle.tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 2000, 0, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(sala.x, altura, sala.y);
                        salasGeneradasInferior.Add(salaAGenerar);
                    }
                    else
                    {
                        Indexsala = MarcRandom(1, salas.Count);
                        GameObject salaAGenerarMapa = Instantiate(PiezasMapa[Indexsala]);
                        GameObject salaAGenerar = Instantiate(tiposDeSalas[Indexsala].tipoSala);
                        salaAGenerarMapa.transform.position = new Vector3(Mathf.Floor(sala.x) + 2000, 0, Mathf.Floor(sala.y));
                        salaAGenerar.transform.position = new Vector3(sala.x, altura, sala.y);
                        salasGeneradasInferior.Add(salaAGenerar);
                    }
                   
                }
            }
            

            if (altura > 0)
            {
                //Debug.Log(salasGeneradasSuperior.Count);
                GenerarPasillos(salasGeneradasSuperior, altura);
            }
            else
            {
                //Debug.Log(salasGeneradasInferior.Count);
                GenerarPasillos(salasGeneradasInferior, altura);
            }
            
        }

    }
    public int MarcRandom( int escaleras, int numsalas)
    {
        int marcNosPideExtras = Random.RandomRange(0, 100);

        if(escaleras == 0) //con escaleras
        {
            
            if (marcNosPideExtras - salaElite_p <= 0)//sale sala de elite
            {
                return 1;            
            }
            else
            {
                marcNosPideExtras -= salaElite_p;                
                if (marcNosPideExtras - salaNormal_p <= 0)//sale sala normal
                {
                    return 0;
                }
                else
                {
                    marcNosPideExtras -= salaNormal_p;
                    if (marcNosPideExtras - salaRectangular_p <= 0)//sale rectangular
                    {
                        return 2;
                    }
                    else//sale rectangular
                    {
                        return 3;
                    }
                }
            }

        }
        else // sin escaleras
        {
            int probabilidadsobra = salaEscaleras_p / 3;//Dividimos la probabilidad de las escaleras entre el numero de otras salas posibles
            if (marcNosPideExtras - (salaElite_p + probabilidadsobra) <= 0)//sale sala de elite
            {
                return 1;
            }
            else
            {
                marcNosPideExtras -= (salaElite_p + probabilidadsobra);
                if (marcNosPideExtras - (salaNormal_p + probabilidadsobra) <= 0)//sale sala normal
                {
                    return 0;
                }
                else//sale rectangular
                {
                    return 2;
                }
            }
        }

        return 0;
    }


    //Funcion encargada de comprovar la lista de salas y mirar su una sala candidata cabe o no
    //public bool ValidarSala(Vector2 salaEvaluar)
    public bool ValidarSala(Vector2 salaEvaluar, int[,] matrizPiso, List<Vector2> salasPiso)
    {
        //print(salaEvaluar.x + " " + rangoMatrizX + " " + salaEvaluar.y + " " + rangoMatrizZ);
        //Comprobamos que la sala no se sale del maximo o minimo del area
        if (salaEvaluar.x+RadioSalas < rangoMatrizX && salaEvaluar.x-RadioSalas >= 0 && salaEvaluar.y + RadioSalas < rangoMatrizZ && salaEvaluar.y - RadioSalas >= 0)
        {
            //Comprovamos que no molesta al resto de salas mirando en que celda caeria la sala en la matriz y miramos 5 casillas alrededor de esta
                int cellx = (int)(salaEvaluar.x / side);
                int celly = (int)(salaEvaluar.y / side);
                //Esta variable elige entre el valor mas grande entre 0 y la celda -2 para no buscar fuera de la matriz (Las siguientes siguen el mismo principio)
                int searchStartX = Mathf.Max(0, cellx - 2);
                int searchEndX = Mathf.Min(matriz.GetLength(0) - 1, cellx + 2); //GetLenght(0) Coge la Lenght en x de la matriz
                int searchStartY = Mathf.Max(0, celly - 2);
                int searchEndY = Mathf.Min(matriz.GetLength(1) - 1, celly + 2); //GetLenght(0) Coge la Lenght en y de la matriz

                //Debug.Log("StartX: " + searchStartX + ", EndX: " + searchEndX + ", StartY: " + searchStartY + ", EndY: " + searchEndY);    

                for(int x = searchStartX; x < searchEndX; x++)
                {
                    for (int y = searchStartY; y < searchEndY; y++)
                    {
                    
                        int salaPrevia = matrizPiso[x, y] - 1; // (salaPrevia sirve como el indice de la sala que se encuentre en la casilla porque las salas se guardan en la matriz por su indice)
                        //Debug.Log("Index: " + index);
                        //si no es -1 quiere decir que hay una sala en esa casilla y hay que comprobar la distancia
                        if (salaPrevia != -1)
                        {
                            float distancia = (salaEvaluar - salasPiso[salaPrevia]).sqrMagnitude; // Usamos el sqrMagnitude porque es mas barato computacionalmente
                            //Debug.Log("Distancia entre las salas: " +  Mathf.Sqrt(distancia));
                            if(distancia < RadioSalas*RadioSalas)
                            {
                                //Debug.Log("Pa que respetes");
                                return false;
                            }
                        }
                    }
                }

                //Debug.Log("TRUE");
                return true;
        }
        //Debug.Log("FALSE");
        return false;
    }

    public void GenerarPasillos(List<GameObject> salas, int altura)
    {
        this.points = new HashSet<Point>();

        //Creamos un punto de cada sala
        foreach(GameObject sala in salas)
        {
            this.points.Add(new Point(Mathf.RoundToInt(sala.transform.position.x), Mathf.RoundToInt(sala.transform.position.z), sala.GetComponent<SalaSc>().tipo));
        }
        
        //LLamamos a la funcion de triangular los puntos
        var triangles = BowyerWatson.Triangulate(points);

        var graph = new HashSet<Edge>();
        foreach (var triangle in triangles)
            graph.UnionWith(triangle.edges);

        //Creamos el Arbol minimo que une las salas (Nuestro camino principal)
        var tree = Kruskal.MinimumSpanningTree(graph);
        CrearUniones(tree, altura, graph);
        
    }

    public void CrearUniones(List<Edge> tree, int altura, HashSet<Edge> graph)
    {
        Dictionary<Vector3, List<Vector3>> diccionario = new Dictionary<Vector3, List<Vector3>>();
        Dictionary<Vector3, List<Vector3>> diccionarioMapa = new Dictionary<Vector3, List<Vector3>>();
        //1 crear el diccionario (El diccionario contiene cada puerta y cuantos puntos le conectan)
        foreach (var edge in tree)
        {
           
            List<Vector3> puntos = edge.calcularPuntosBordesSalas();
            List<Vector3> puntosMapa = edge.calcularPuntosBordesSalasMapa();

            Vector3 a = puntos[0];
            Vector3 b = puntos[1];
            Vector3 aM = puntosMapa[0];
            Vector3 bM = puntosMapa[1];

            Debug.DrawLine(new Vector3(puntos[0].x, altura, puntos[0].z), new Vector3(puntos[1].x, altura, puntos[1].z), Color.green, 10f);

            //Si ya existe esta "puerta", le a�adimos la relacion. Si no existe Creamos la "puerta" y le a�adimos la relacion
            if (diccionario.ContainsKey(a))
            {
                diccionario[a].Add(b);
                diccionarioMapa[aM].Add(bM);
            }
            else
            {
                List<Vector3> listanueva = new List<Vector3>();
                listanueva.Add(b);
                diccionario.Add(a, listanueva);

                List<Vector3> listanuevaM = new List<Vector3>();
                listanuevaM.Add(bM);
                diccionarioMapa.Add(aM, listanuevaM);
            }

            if (diccionario.ContainsKey(b))
            {
                diccionario[b].Add(a);
                diccionarioMapa[bM].Add(aM);
            }
            else
            {
                List<Vector3> listanueva = new List<Vector3>();
                listanueva.Add(a);
                diccionario.Add(b, listanueva);

                List<Vector3> listanuevaM = new List<Vector3>();
                listanuevaM.Add(aM);
                diccionarioMapa.Add(bM, listanuevaM);
            }

        }
        //2 a�adir algunos caminos extras
        foreach (var edge in graph)
        {

            float random = Random.Range(0, 100);
            if (random < 10)
            {
                List<Vector3> puntos = edge.calcularPuntosBordesSalas();
                List<Vector3> puntosMapa = edge.calcularPuntosBordesSalasMapa();

                Vector3 a = puntos[0];
                Vector3 b = puntos[1];
                Vector3 aM = puntosMapa[0];
                Vector3 bM = puntosMapa[1];



                if (diccionario.ContainsKey(a))
                {
                    if (!diccionario[a].Contains(b))
                    { 

                        Debug.DrawLine(new Vector3(puntos[0].x, altura, puntos[0].z), new Vector3(puntos[1].x, altura, puntos[1].z), Color.red + Color.yellow, 10f);
                        diccionario[a].Add(b);
                        diccionarioMapa[aM].Add(bM);
                    }
                }
                else
                {
                    Debug.DrawLine(new Vector3(puntos[0].x, altura, puntos[0].z), new Vector3(puntos[1].x, altura, puntos[1].z), Color.red + Color.yellow, 10f);
                    List<Vector3> listanueva = new List<Vector3>();
                    listanueva.Add(b);
                    diccionario.Add(a, listanueva);

                    List<Vector3> listanuevaM = new List<Vector3>();
                    listanuevaM.Add(bM);
                    diccionarioMapa.Add(aM, listanuevaM);
                }

                if (diccionario.ContainsKey(b))
                {
                    if (!diccionario[b].Contains(a))
                    {
                        diccionario[b].Add(a);
                        diccionarioMapa[bM].Add(aM);
                    }
                }
                else
                {
                    List<Vector3> listanueva = new List<Vector3>();
                    listanueva.Add(a);
                    diccionario.Add(b, listanueva);

                    List<Vector3> listanuevaM = new List<Vector3>();
                    listanuevaM.Add(aM);
                    diccionarioMapa.Add(bM, listanuevaM);
                }
            }

        }

       //POR CADA ENTRADA INSTANCIO UNA SALA DE TPS, LUEGO RECORRO LOS EDGES PARA VER CUALES TENGO QUE UNIR
        foreach (var entry in diccionario)
        {

            StartCoroutine(ComePuertas(entry.Key, altura));

            if (entry.Value.Count == 1)
            {
                GameObject tp1 = Instantiate(TPS[0]);
                tp1.transform.position = new Vector3(entry.Key.x, altura, entry.Key.z);
                tp1.transform.GetChild(0).GetComponent<TP>().destino = new Vector3(entry.Value[0].x,altura +2, entry.Value[0].z);
            }else if (entry.Value.Count == 2)
            {
                GameObject tp2 = Instantiate(TPS[1]);
                tp2.transform.position = new Vector3(entry.Key.x, altura, entry.Key.z);
                tp2.transform.GetChild(0).GetComponent<TP>().destino = new Vector3(entry.Value[0].x, altura + 2, entry.Value[0].z);
                tp2.transform.GetChild(1).GetComponent<TP>().destino = new Vector3(entry.Value[1].x, altura + 2, entry.Value[1].z);
            }
            else if (entry.Value.Count == 3)
            {
                GameObject tp3 = Instantiate(TPS[2]);
                tp3.transform.position = new Vector3(entry.Key.x, altura, entry.Key.z);
                tp3.transform.GetChild(0).GetComponent<TP>().destino = new Vector3(entry.Value[0].x, altura + 2, entry.Value[0].z);
                tp3.transform.GetChild(1).GetComponent<TP>().destino = new Vector3(entry.Value[1].x, altura + 2, entry.Value[1].z);
                tp3.transform.GetChild(2).GetComponent<TP>().destino = new Vector3(entry.Value[2].x, altura + 2, entry.Value[2].z);
            }
            else if (entry.Value.Count == 4)
            {
                GameObject tp4 = Instantiate(TPS[3]);
                tp4.transform.position = new Vector3(entry.Key.x, altura, entry.Key.z);
                tp4.transform.GetChild(0).GetComponent<TP>().destino = new Vector3(entry.Value[0].x, altura + 2, entry.Value[0].z);
                tp4.transform.GetChild(1).GetComponent<TP>().destino = new Vector3(entry.Value[1].x, altura + 2, entry.Value[1].z);
                tp4.transform.GetChild(2).GetComponent<TP>().destino = new Vector3(entry.Value[2].x, altura + 2, entry.Value[2].z);
                tp4.transform.GetChild(3).GetComponent<TP>().destino = new Vector3(entry.Value[3].x, altura + 2, entry.Value[3].z);
            }
        }

        foreach (var entry in diccionarioMapa)
        {
            if (entry.Value.Count == 1)
            {
                pasillosMapa(entry.Key, entry.Value[0], altura);

            }
            else if (entry.Value.Count == 2)
            {
                pasillosMapa(entry.Key, entry.Value[0], altura);
                pasillosMapa(entry.Key, entry.Value[1], altura);
            }
            else if (entry.Value.Count == 3)
            {
                pasillosMapa(entry.Key, entry.Value[0], altura);
                pasillosMapa(entry.Key, entry.Value[1], altura);
                pasillosMapa(entry.Key, entry.Value[2], altura);
            }
            else if (entry.Value.Count == 4)
            {
                pasillosMapa(entry.Key, entry.Value[0], altura);
                pasillosMapa(entry.Key, entry.Value[1], altura);
                pasillosMapa(entry.Key, entry.Value[2], altura);
                pasillosMapa(entry.Key, entry.Value[3], altura);

            }
        }


    }

    public void pasillosMapa(Vector3 a, Vector3 b, int altura)
    {
        GameObject pasilloMapa = Instantiate(pasillo);
        //Tama�o de los "pasillos"
        pasilloMapa.transform.localScale = new Vector3(this.transform.localScale.x + 5, 0.5f, (Vector3.Distance(new Vector3(a.x, altura, a.z), new Vector3(b.x, altura, b.z))/2)+10);
        if (altura == 0)
        {
            pasilloMapa.transform.position = new Vector3(a.x + 3000, 0, a.z);
            pasilloMapa.transform.LookAt(new Vector3(b.x + 3000, 0, b.z));
            pasilloMapa.transform.position += pasilloMapa.transform.forward * (Vector3.Distance(new Vector3(a.x, altura, a.z), new Vector3(b.x, altura, b.z)) / 5);
        }
        else if (altura < 0)
        {
            pasilloMapa.transform.position = new Vector3(a.x + 2000, 0, a.z);
            pasilloMapa.transform.LookAt(new Vector3(b.x + 2000, 0, b.z));
            pasilloMapa.transform.position += pasilloMapa.transform.forward * (Vector3.Distance(new Vector3(a.x, altura, a.z), new Vector3(b.x, altura, b.z)) / 5);
        }
        else
        {
            pasilloMapa.transform.position = new Vector3(a.x + 4000, 0, a.z);
            pasilloMapa.transform.LookAt(new Vector3(b.x + 4000, 0, b.z));
            pasilloMapa.transform.position += pasilloMapa.transform.forward * (Vector3.Distance(new Vector3(a.x, altura, a.z), new Vector3(b.x, altura, b.z)) / 5);
        }
    }

   IEnumerator ComePuertas(Vector3 centro, int altura)
    {
        GameObject ECP1 = Instantiate(ElComePuertas);
        ECP1.GetComponent<ElComePuertas>().Devorar(new Vector3(centro.x, altura, centro.z));
        yield return new WaitForSeconds(0.3f);
    }


}
