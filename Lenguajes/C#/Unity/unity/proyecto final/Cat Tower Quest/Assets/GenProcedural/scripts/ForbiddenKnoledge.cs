using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForbiddenKnoledge : MonoBehaviour
{

    public GameObject RocketL;
    private bool ya;

    private void Start()
    {
        ya = false;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.GetComponent<MovimientoJugador>().state == MovimientoJugador.States.Dance && !ya)
        {
            RocketL.SetActive(true);
            ya = true;
        }
    }
}
