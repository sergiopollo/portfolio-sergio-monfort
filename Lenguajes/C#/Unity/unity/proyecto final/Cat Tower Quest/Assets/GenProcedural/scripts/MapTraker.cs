using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTraker : MonoBehaviour
{
    public Transform jugador;
    public GameObject fichaJugador;

    private GameObject ficha;
    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player").transform;
        ficha = Instantiate(fichaJugador);
        ficha.transform.position = new Vector3(jugador.position.x+3000, 5, jugador.position.z);
        ficha.transform.eulerAngles = (new Vector3(0,0,0));
        StartCoroutine("Traking");
    }

    IEnumerator Traking()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            ficha.transform.eulerAngles = new Vector3(ficha.transform.eulerAngles.x, jugador.eulerAngles.y, ficha.transform.eulerAngles.z);
            if(jugador.position.y > 15)
            {
                ficha.transform.position = new Vector3(jugador.position.x + 4000, 5, jugador.position.z);
            }
            else if(jugador.position.y < -5)
            {
                ficha.transform.position = new Vector3(jugador.position.x + 2000, 5, jugador.position.z);
            }
            else
            {
                ficha.transform.position = new Vector3(jugador.position.x + 3000, 5, jugador.position.z);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
