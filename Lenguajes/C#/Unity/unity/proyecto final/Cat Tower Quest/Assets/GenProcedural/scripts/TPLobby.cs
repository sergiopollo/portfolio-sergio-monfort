using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TPLobby : MonoBehaviour{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Debug.Log("TE TEPEO AL BOSS");
            other.GetComponent<MovimientoJugador>().getHealed(9999);
            SceneManager.LoadScene("Lobby");
        }
    }
}
