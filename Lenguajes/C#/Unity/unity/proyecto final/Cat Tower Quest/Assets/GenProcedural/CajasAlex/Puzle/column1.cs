using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class column1 : MonoBehaviour
{

    // Start is called before the first frame update
    public List<GameObject> fuegos;
    public int contadorC1;

    void Start()
    {
        contadorC1 = GetComponentInParent<ColumnasGeneral>().contador;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeFires();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player" && GetComponentInParent<ColumnasGeneral>().contador == 0)
        {
            GetComponentInParent<ColumnasGeneral>().contador++;
            print("Contador"+GetComponentInParent<ColumnasGeneral>().contador++);
            this.GetComponent<AudioSource>().Play();

        }
        else if(collision.gameObject.tag == "Player" && GetComponentInParent<ColumnasGeneral>().contador != 0)
        {
            print("Contador" + GetComponentInParent<ColumnasGeneral>().contador++);
            GetComponentInParent<ColumnasGeneral>().contador = 0;
            this.GetComponent<AudioSource>().Play();

        }
    }


    public void ChangeFires()
    {
        if (GetComponentInParent<ColumnasGeneral>().contador == 2)
        {

            fuegos[0].SetActive(false);
            fuegos[1].SetActive(true);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(false);
            fuegos[4].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 4)
        {

            fuegos[0].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[2].SetActive(true);
            fuegos[3].SetActive(false);
            fuegos[4].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 6)
        {
            fuegos[0].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(true);
            fuegos[4].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 8)
        {

            fuegos[3].SetActive(false);
            fuegos[4].SetActive(true);
            fuegos[2].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[2].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 10)
        {

            fuegos[4].SetActive(false);
            fuegos[5].SetActive(true);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[0].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 12)
        {

            fuegos[5].SetActive(false);
            fuegos[6].SetActive(true);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(false);
            fuegos[4].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[0].SetActive(false);
            fuegos[7].SetActive(false);
        }
        else if (GetComponentInParent<ColumnasGeneral>().contador == 14)
        {

            fuegos[6].SetActive(false);
            fuegos[7].SetActive(true);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(false);
            fuegos[4].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[1].SetActive(false);
            fuegos[0].SetActive(false);
        }
        else
        {
            fuegos[0].SetActive(true);
            fuegos[1].SetActive(false);
            fuegos[2].SetActive(false);
            fuegos[3].SetActive(false);
            fuegos[4].SetActive(false);
            fuegos[5].SetActive(false);
            fuegos[6].SetActive(false);
            fuegos[7].SetActive(false);
        }
    }
}

