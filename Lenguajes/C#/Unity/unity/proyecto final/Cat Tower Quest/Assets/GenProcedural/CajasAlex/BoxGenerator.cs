using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGenerator : MonoBehaviour
{
   

    public GameObject Box;
    //tama�o de la matriz donde curramos, pongo siempre -1 para que no se metan en la pared
    public float xSize;
    public float ZSize;
    // siendo sinceros niidea de que es XD estan ambos a 1 y funciona no toquen :)
    public float stepMin = 1.0f;
    public float stepMax = 1.0f;

    // cuanto mas bajo mas juntas salen, es decir 0 = todo lleno de cajas 1 = ninguna caja
    [Range(0, 1)]
    public float threshold = 0.5f;

    // cuanto mas bajo mas juntas salen las cajas una al lado del otro por ejemplo si lo ponemos bajo hariamos "islas" de cajas
    public float noiseStrength = 10.0f;

   
    void Awake()
    {
        StartCoroutine(GenerateBox());
    }
        
    IEnumerator GenerateBox()
    {
        for (float x = 0; x < xSize; x += Random.Range(stepMin, stepMax))
        {
            for (float z = 0; z < ZSize; z += Random.Range(stepMin, stepMax))
            {
                float pcheck = Mathf.PerlinNoise(x / xSize * noiseStrength, z / ZSize * noiseStrength);
                if (pcheck >= threshold)
                {
                    GameObject box =
                    GameObject.Instantiate(Box,
                        new Vector3(x + (transform.position.x - xSize / 1.8f),transform.position.y+0.78f, z + (transform.position.z - ZSize / 1.8f)),Quaternion.identity);
                    box.transform.SetParent(this.transform);
                   // box.hideFlags = HideFlags.HideInHierarchy;
                }
                yield return null;
            }
        }
    }

    }

