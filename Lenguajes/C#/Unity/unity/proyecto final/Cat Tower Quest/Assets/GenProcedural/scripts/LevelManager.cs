using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LevelManager : MonoBehaviour
{
    //new input system
    InputPlayer inputs;

    public List<GameObject> enemigos;
    public int numEnemigos;
    public List<GameObject> enemigosElite;
    public int numEnemigosElite;
    public GameObject canvas;

    private GameObject jugador;
    private GameObject camaraJugador;
    private GameObject p0;
    private GameObject p1;
    private GameObject p2;
    private bool mapa;

    private void Awake()
    {
        inputs = new InputPlayer();
        inputs.Enable();
        inputs.player.map.performed += mapDown;

    }

    private void Start()
    {
        jugador = GameObject.Find("Player");
        camaraJugador = jugador.transform.GetChild(3).gameObject;
        p0 = GameObject.Find("MapaP0");
        p0.SetActive(false);
        p1 = GameObject.Find("MapaP1");
        p1.SetActive(false);
        p2 = GameObject.Find("MapaP2");
        p2.SetActive(false);
        //jugador.transform.position = jugador.transform.up*1000;
        StartCoroutine("Load");
        mapa = false;
    }

    IEnumerator Load()
    {
        jugador.GetComponent<MovimientoJugador>().Pause = true;
        canvas.GetComponent<canvasPlayer>().LoadingScreen();
        yield return new WaitForSeconds(8f);//Camviar por un listener y que avise el generador
        canvas.GetComponent<canvasPlayer>().LoadingScreen();
        jugador.GetComponent<MovimientoJugador>().Pause = false;
    }

    private void Update()
    {
        
    }


    public void mapDown(InputAction.CallbackContext context)
    {
        
        if (mapa)
        {
            mapa = false;
            camaraJugador.SetActive(true);
            p0.SetActive(false);
            p1.SetActive(false);
            p2.SetActive(false);
        }
        else
        {
            mapa = true;

            if (jugador.transform.position.y > 10)
            {
                p2.SetActive(true);
                camaraJugador.SetActive(false);
            }
            else if (jugador.transform.position.y < -5)
            {
                p0.SetActive(true);
                camaraJugador.SetActive(false);
            }
            else
            {
                p1.SetActive(true);
                camaraJugador.SetActive(false);
            }

        }
        
    }

    /*public GameObject Spawn(Vector3 coordenadas)
     {

         for(int i = 0; i < numEnemigos; i++)
         {
             GameObject enemigo = Instantiate(enemigos[Random.Range(0, enemigos.Length)]);//CAMBIAR CUANDO HAYA MAS ENEMIGOSs
             int desviacionx = Random.Range(-10, 10);
             int desviacionz = Random.Range(-10, 10);
             enemigo.transform.position = new Vector3(coordenadas.x + desviacionx, coordenadas.y + 1, coordenadas.z + desviacionz);
         }


     }*/

    private void OnDestroy()
    {
        inputs.player.map.performed -= mapDown;
    }
}
