using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SalaSc : MonoBehaviour
{
    public Sala tipo;
    //public GameObject tipoSala;
    public List<GameObject> Vecinos;
    public GameObject[] puertas;
    public int piso;
    public float radio;
    public float altura;
    public float lado;
    //public bool tengoPasillo = false;

    // Start is called before the first frame update
    void Start()
    {
        //this.tipoSala = tipo.tipoSala;
        //this.Vecinos = tipo.Vecinos;
        //this.puertas = tipo.puertas;
        this.piso = tipo.piso;
        this.radio = tipo.radio;
        this.altura = tipo.altura;
        this.lado = tipo.lado;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
