using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Data", menuName = "ScriptableObjects/sala", order = 1)]
public class Sala : ScriptableObject
{

    public GameObject tipoSala;
    public GameObject[] Vecinos;
    public float[] puertas;
    public int piso;
    public float radio;
    public float lado;
    public float altura;

}
