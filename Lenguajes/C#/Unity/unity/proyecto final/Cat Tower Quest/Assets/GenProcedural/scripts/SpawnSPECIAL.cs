using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSPECIAL : MonoBehaviour
{
    public GameObject mapcover;
    private GameObject mc;
    // Start is called before the first frame update
    void Start()
    {
        mc = Instantiate(mapcover);
        if (this.transform.position.y > 10)
        {
            mc.transform.position = new Vector3(this.transform.position.x + 4000, 2, this.transform.position.z);
        }
        else if (this.transform.position.y < -10)
        {
            mc.transform.position = new Vector3(this.transform.position.x + 2000, 2, this.transform.position.z);
        }
        else
        {
            mc.transform.position = new Vector3(this.transform.position.x + 3000, 2, this.transform.position.z);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && mc != null)
        {
            Destroy(mc.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
