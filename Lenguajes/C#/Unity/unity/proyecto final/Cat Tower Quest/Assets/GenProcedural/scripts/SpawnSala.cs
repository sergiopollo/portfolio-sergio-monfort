using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSala : MonoBehaviour
{

    //Variables publicas
    public GameObject spear;
    public GameObject mapcover;
    public Transform jugador;
    //Variables privadas
    private GameObject levelmanager;
    private bool ini, vivos;
    private int numEnemigos;
    private int enemigosVivos;
    private List<GameObject> enemigos;
    private GameObject mc;

    //Spears
    private GameObject s11, s12, s13, s21, s22, s23, s31, s32, s33, s41, s42, s43;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player").transform;
        ini = false;
        vivos = true;
        enemigosVivos = 0;
        levelmanager = GameObject.Find("LevelManager");
        enemigos = levelmanager.GetComponent<LevelManager>().enemigos;
        numEnemigos = levelmanager.GetComponent<LevelManager>().numEnemigos;
        mc = Instantiate(mapcover);
        if (this.transform.position.y > 10)
        {
            mc.transform.position = new Vector3(this.transform.position.x+4000, 2, this.transform.position.z);
        }
        else if (this.transform.position.y < -10)
        {
            mc.transform.position = new Vector3(this.transform.position.x + 2000, 2, this.transform.position.z);
        }
        else
        {
            mc.transform.position = new Vector3(this.transform.position.x + 3000, 2, this.transform.position.z);
        }        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!ini)
        {
            if (collision.gameObject.tag == "Player")
            {
                StartCoroutine("Combate");
                Destroy(mc.gameObject);
                ini = true;
            }
        }

    }

    public void EnemigoMuerto()
    {
        if (ini)
        {
            //Debug.Log("QUEDA UNO MENOS!!");
            enemigosVivos--;
        }
        
    }

    IEnumerator Combate()
    {
        yield return new WaitForSeconds(0.25f);
        BloquearPuertas();
        jugador.GetChild(5).gameObject.GetComponent<TowerMusic>().StartFightMusic();
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < numEnemigos; i++)
        {
            GameObject enemigo = Instantiate(enemigos[Random.Range(0, enemigos.Count)]);//CAMBIAR CUANDO HAYA MAS ENEMIGOSs
            int desviacionx = Random.Range(-10, 10);
            int desviacionz = Random.Range(-10, 10);
            enemigo.transform.position = new Vector3(this.transform.position.x + desviacionx, this.transform.position.y, this.transform.position.z + desviacionz);
            enemigosVivos++;
            //Debug.Log("Num enemigos: " + enemigosVivos);
        }

        if (Vector3.Distance(jugador.position, this.transform.position) > (this.transform.localScale.z / 2))
        {
            int x = Random.Range(0, 2);
            if (x == 0)
            {
                x = -2;
            }
            else
            {
                x = 2;
            }
            int z = Random.Range(0, 2);
            if (z == 0)
            {
                z = -2;
            }
            else
            {
                z = 2;
            }
            jugador.transform.position = new Vector3(this.transform.position.x + x, this.transform.position.y + 1, this.transform.position.z + z);
        }

        while (vivos)
        {
            yield return new WaitForSeconds(1f);
            //Debug.Log(enemigosVivos);
            if (enemigosVivos <= 0)
            {
                vivos = false;
            }

        }

        StartCoroutine("Bajar");
         
    }

    public void BloquearPuertas()
    {
        //1
        s11 = Instantiate(spear);
        s11.transform.position = new Vector3(this.transform.position.x + (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z);
        s12 = Instantiate(spear);
        s12.transform.position = new Vector3(this.transform.position.x + (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z + 0.5f);
        s13 = Instantiate(spear);
        s13.transform.position = new Vector3(this.transform.position.x + (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z - 0.5f);

        //2
        s21 = Instantiate(spear);
        s21.transform.position = new Vector3(this.transform.position.x  - (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z);
        s22 = Instantiate(spear);
        s22.transform.position = new Vector3(this.transform.position.x  - (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z + 0.5f);
        s23 = Instantiate(spear);
        s23.transform.position = new Vector3(this.transform.position.x  - (this.transform.localScale.x) / 2, this.transform.position.y - 4, this.transform.position.z - 0.5f);

        //3
        s31 = Instantiate(spear);
        s31.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 4, this.transform.position.z  + (this.transform.localScale.z) / 2);
        s32 = Instantiate(spear);
        s32.transform.position = new Vector3(this.transform.position.x + 0.5f, this.transform.position.y - 4, this.transform.position.z  + (this.transform.localScale.z) / 2);
        s33 = Instantiate(spear);
        s33.transform.position = new Vector3(this.transform.position.x - 0.5f, this.transform.position.y - 4, this.transform.position.z  + (this.transform.localScale.z) / 2);

        //4
        s41 = Instantiate(spear);
        s41.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 4, this.transform.position.z  - (this.transform.localScale.z) / 2);
        s42 = Instantiate(spear);
        s42.transform.position = new Vector3(this.transform.position.x + 0.5f, this.transform.position.y - 4, this.transform.position.z  - (this.transform.localScale.z) / 2);
        s43 = Instantiate(spear);
        s43.transform.position = new Vector3(this.transform.position.x - 0.5f, this.transform.position.y - 4, this.transform.position.z  - (this.transform.localScale.z) / 2);

        StartCoroutine("Levantar");
    }

    IEnumerator Levantar()
    {

        s11.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s12.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s13.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s21.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s22.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s23.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s31.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s32.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s33.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s41.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s42.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s43.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;


        s11.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s12.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s13.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;

        s21.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s22.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s23.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;

        s31.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s32.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s33.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;

        s41.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s42.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        s43.GetComponent<Rigidbody>().velocity = s11.transform.up * 20;
        yield return new WaitForSeconds(0.3f);

        s11.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s12.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s13.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        s21.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s22.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s23.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        s31.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s32.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s33.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        s41.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s42.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        s43.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

    }

    IEnumerator Bajar()
    {
        jugador.GetChild(5).gameObject.GetComponent<TowerMusic>().StartTowerMusic();
        s11.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s12.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s13.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s21.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s22.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s23.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s31.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s32.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s33.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s41.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s42.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        s43.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        s11.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s12.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s13.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;

        s21.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s22.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s23.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;

        s31.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s32.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s33.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;

        s41.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s42.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        s43.GetComponent<Rigidbody>().velocity = s11.transform.up * -20;
        yield return new WaitForSeconds(0.3f);

        Destroy(s11);
        Destroy(s12);
        Destroy(s13);

        Destroy(s21);
        Destroy(s22);
        Destroy(s23);

        Destroy(s31);
        Destroy(s32);
        Destroy(s33);

        Destroy(s41);
        Destroy(s42);
        Destroy(s43);

        Destroy(this);//Ya no necesitamos el codigo
    }
}
