using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class esferaMagica : MonoBehaviour
{
    public Material materialInicial;
    public GameObject candelabroAzul;
    public GameObject candelabroRojo;
    public GameObject candelabroRosa;
    public GameObject candelabroNaranja;
    public GameObject candelabroVerde;
    public GameObject candelabroBlanco;
    public GameObject candelabroNegro;
    public GameObject candelabroAmarillo;
    public string lastMaterial;

    public bool conseguido;
    private int color1, color2, color3;
    private string colorS1, colorS2, colorS3;
    private int correctas;

    // Start is called before the first frame update
    void Start()
    {
        correctas = 0;
        conseguido = false;
        materialInicial = this.GetComponent<MeshRenderer>().material;
        seleccionarColores();
    }

    public void seleccionarColores()
    {
        color1 = Random.Range(0, 8);
        do
        {
            color2 = Random.Range(0, 8);
        } while (color2 == color1);

        do
        {
            color3 = Random.Range(0, 8);
        } while (color3 == color1 || color3 == color2);

        colorS1 = pasarAString(color1);
        colorS2 = pasarAString(color2);
        colorS3 = pasarAString(color3);
        ActivarCandelabro(colorS1);
    }

    public string pasarAString(int num)
    {
        string aux = "";
        switch (num)
        {
            case 0:
                aux = "Rosa";
                break;
            case 1:
                aux = "Azul";
                break;
            case 2:
                aux = "Rojo";
                break;
            case 3:
                aux = "Negro";
                break;
            case 4:
                aux = "Blanco";
                break;
            case 5:
                aux = "Amarillo";
                break;
            case 6:
                aux = "Verde";
                break;
            case 7:
                aux = "Naranja";
                break;
        }

        return aux;
    }

    public void DesactivarCandelabros()
    {
        candelabroRosa.SetActive(false);
        candelabroRojo.SetActive(false);
        candelabroAmarillo.SetActive(false);
        candelabroAzul.SetActive(false);
        candelabroBlanco.SetActive(false);
        candelabroNaranja.SetActive(false);
        candelabroNegro.SetActive(false);
        candelabroVerde.SetActive(false);
    }
    public void ActivarCandelabro(string color)
    {
        DesactivarCandelabros();

        switch (color)
        {
            case "Rosa":
                candelabroRosa.SetActive(true);
                break;
            case "Azul":
                candelabroAzul.SetActive(true);
                break;
            case "Rojo":
                candelabroRojo.SetActive(true);
                break;
            case "Negro":
                candelabroNegro.SetActive(true);
                break;
            case "Blanco":
                candelabroBlanco.SetActive(true);
                break;
            case "Amarillo":
                candelabroAmarillo.SetActive(true);
                break;
            case "Verde":
                candelabroVerde.SetActive(true);
                break;
            case "Naranja":
                candelabroNaranja.SetActive(true);
                break;
        }
    }

    public void Reset()
    {
        ActivarCandelabro(colorS1);
        correctas = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {       
        if(collision.gameObject.tag == "ChangeColor" && lastMaterial != collision.gameObject.GetComponent<MeshRenderer>().material.name)
        {
            //Debug.Log("Mio: " + this.GetComponent<MeshRenderer>().material.name + " Otro: " + collision.gameObject.GetComponent<MeshRenderer>().material.name + " Last: " + lastMaterial);
            this.GetComponent<MeshRenderer>().material = collision.gameObject.GetComponent<MeshRenderer>().material;
            lastMaterial = collision.gameObject.GetComponent<MeshRenderer>().material.name;

            if (collision.gameObject.name == colorS1 && correctas == 0)//primero
            {
                ActivarCandelabro(colorS2);
                correctas = 1;
            }
            else if (collision.gameObject.name == colorS2 && correctas == 1)//segundo
            {
                ActivarCandelabro(colorS3);
                correctas = 2;
            }
            else if(collision.gameObject.name == colorS3 && correctas == 2)//fin
            {
                conseguido = true;
                Destroy(this.gameObject);
            }
            else
            {
                Reset();
            }

        }      
    }
}

  