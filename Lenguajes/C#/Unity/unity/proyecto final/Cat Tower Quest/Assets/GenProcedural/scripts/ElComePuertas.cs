using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElComePuertas : MonoBehaviour
{
    private bool Puerta;
    private bool rotado;
    private Vector3 origen;

    private void Start()
    {
        Puerta = false;
        rotado = false;
    }

    public void Devorar(Vector3 centro)
    {
        StartCoroutine(devorar(centro));
    }

    IEnumerator devorar(Vector3 centro)
    {
        this.transform.position = new Vector3(centro.x+4.8f, centro.y, centro.z);
        yield return new WaitForSeconds(0.1f);
        if (!Puerta)
        {
            this.transform.position = new Vector3(centro.x-4.8f, centro.y, centro.z);
            yield return new WaitForSeconds(0.1f);
        }
        if (!Puerta)
        {
            this.transform.position = new Vector3(centro.x, centro.y, centro.z+4.8f);
            yield return new WaitForSeconds(0.1f);
        }
        if (!Puerta)
        {
            this.transform.position = new Vector3(centro.x, centro.y, centro.z-4.8f);
            yield return new WaitForSeconds(0.1f);
        }
        Destroy(this.gameObject);
    }

    // Start is called before the first frame update
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.tag);
        if(collision.gameObject.tag == "Puerta")
        {
            //Debug.Log("PUERTA!!!");
            Puerta = true;
            Destroy(collision.gameObject);
        }      
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log(other.gameObject);
        if (Puerta && !rotado)
        {
            //Debug.Log("ROTAR!!!");
            rotado = true;
            other.transform.LookAt(new Vector3(this.transform.position.x, other.transform.position.y, this.transform.position.z));
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if(Puerta && rotado)
        {
            //Debug.Log("Destruir?");
            //Debug.Log(collision.gameObject);
            if (collision.gameObject.tag == "DestroyableWall")
            {
               // Debug.Log("DESTRUIR!!!");
                Destroy(collision.gameObject);
            }
        }
    }
    
}
