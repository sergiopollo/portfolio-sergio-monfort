using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzleManager : MonoBehaviour
{
    public GameObject colores;
    public esferaMagica esfera;
    public GameObject columna1;
    public GameObject columna2;
    public GameObject columna3;
    public GameObject columna4;
    public GameObject columna5;
    public GameObject columna6;
    public GameObject columna7;
    public GameObject columna8;

    public GameObject portal;
    public ColumnasGeneral columnasG;
    public GameObject jugador;
    bool done;
    bool Edone;
    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player");
        done = false;
        Edone = false;
        this.GetComponent<AudioSource>().Stop() ;
        esfera.gameObject.SetActive(true);
        colores.SetActive(true);
        DesactivarColumnas();       
        StartCoroutine("Check");
    }

    public void DesactivarColumnas()
    {
        columna1.SetActive(false);
        columna2.SetActive(false);
        columna3.SetActive(false);
        columna4.SetActive(false);
        columna5.SetActive(false);
        columna6.SetActive(false);
        columna7.SetActive(false);
        columna8.SetActive(false);
    }
    IEnumerator ActivarColumnas()
    {        
        Activar();
        columna1.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna2.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna3.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna4.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna5.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna6.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna7.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        columna8.GetComponent<AudioSource>().Play();
        Columnas();
        yield return new WaitForSeconds(0.4f);
        Columnas();
        yield return new WaitForSeconds(0.4f);
        Columnas();
        yield return new WaitForSeconds(0.4f);
    }

    public void Activar()
    {
        columna1.SetActive(true);
        columna2.SetActive(true);
        columna3.SetActive(true);
        columna4.SetActive(true);
        columna5.SetActive(true);
        columna6.SetActive(true);
        columna7.SetActive(true);
        columna8.SetActive(true);
    }
    public void Columnas()
    {
        columna1.transform.position = new Vector3(columna1.transform.position.x, columna1.transform.position.y + 0.1f, columna1.transform.position.z);
        columna2.transform.position = new Vector3(columna2.transform.position.x, columna2.transform.position.y + 0.1f, columna2.transform.position.z);
        columna3.transform.position = new Vector3(columna3.transform.position.x, columna3.transform.position.y + 0.1f, columna3.transform.position.z);
        columna4.transform.position = new Vector3(columna4.transform.position.x, columna4.transform.position.y + 0.1f, columna4.transform.position.z);
        columna5.transform.position = new Vector3(columna5.transform.position.x, columna5.transform.position.y + 0.1f, columna5.transform.position.z);
        columna6.transform.position = new Vector3(columna6.transform.position.x, columna6.transform.position.y + 0.1f, columna6.transform.position.z);
        columna7.transform.position = new Vector3(columna7.transform.position.x, columna7.transform.position.y + 0.1f, columna7.transform.position.z);
        columna8.transform.position = new Vector3(columna8.transform.position.x, columna8.transform.position.y + 0.1f, columna8.transform.position.z);
    }
    IEnumerator Check()
    {
        while (!done)
        {
            yield return new WaitForSeconds(0.2f);

            if (esfera.conseguido && !Edone)
            {
                Edone = true;
                colores.SetActive(false);
                StartCoroutine("ActivarColumnas");               
            }
            
            if (columnasG.contador > 14)
            {
                yield return new WaitForSeconds(0.5f);
                done = true;
                DesactivarColumnas();
                portal.SetActive(true);
            }
            if (Input.GetKey(KeyCode.B))
            {
                esfera.conseguido = true;
                Destroy(esfera.gameObject);
            }
            if (Input.GetKey(KeyCode.C))
            {
                DesactivarColumnas();
                portal.SetActive(true);
            }
            if (Input.GetKey(KeyCode.L))
            {
                jugador.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 2, this.transform.position.z);
            }
        }
    }
   
    // Update is called once per frame
    void Update()
    {
        
        
    }
}
