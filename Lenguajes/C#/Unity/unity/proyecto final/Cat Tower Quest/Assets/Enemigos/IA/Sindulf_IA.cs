using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sindulf_IA : IABASE
{
    public Transform jugador;
    public GameObject warning;
    public GameObject warningMeteo;
    public GameObject meteo;
    public GameObject Tank;
    public GameObject Exploder;
    public GameObject portal;
    public habilidadSO[] loot;
    public int numSummons;
    public GameEvent actGUI;

    private bool free;
    private GameObject w;
    private GameObject m;
    private bool inv;
    private bool THEGREATRK;
    public List<AudioClip> audios;
    private GameObject party;
    private List<GameObject> misHijos;
    // Start is called before the first frame update
    void Start()
    {
        THEGREATRK = true;
        free = true;
        party = GameObject.Find("Party");
        misHijos = new List<GameObject>();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        base.init();
        this.estado = EnemyState.DECIDE;
        this.GetComponent<NavMeshAgent>().enabled = true;
        inv = false;
        StartCoroutine("MaquinaEstados");
    }

    /*
     COMPORTAMIENTO DEL BOSS :D

        Sindulfo comprobara cada rato la distancia de el al jugador.
        Si el jugador esta muy lejos sindulf intentara sumonear subditos (exploders y tanks), o hara un ataque de area devastador (70/30)
        Si el jugador esta a medio rango sindulf sumoneara enemigos igualmente o intentara moverse (60/40)
        Si el jugador esta cerca sindulf atacara con una cornada
         */


    IEnumerator MaquinaEstados()
    {
        this.GetComponent<Animator>().Play("Cast Spell");
        yield return new WaitForSeconds(1f);
        this.GetComponent<Animator>().Play("Idle");
        yield return new WaitForSeconds(1f);

        while (this.alive)
        {
            //Debug.Log("Estado: " + estado);
            yield return new WaitForSeconds(1f);
            if (free)
            {
                this.transform.LookAt(new Vector3(jugador.transform.position.x, this.transform.position.y, jugador.transform.position.z));

                switch (estado)
                {
                    case EnemyState.DECIDE:

                        if (Vector3.Distance(this.transform.position, jugador.position) > 40)//LEJOS
                        {
                            int decision = Random.Range(0, 10);
                            if (decision < 3)//BigBoom
                            {
                                estado = EnemyState.ATTACK;
                            }
                            else//sumon
                            {
                                estado = EnemyState.SUMMON;
                            }
                            //Debug.Log("LEJOS");
                        }
                        else if (Vector3.Distance(this.transform.position, jugador.position) > 20)//MEDIO
                        {
                            int decision = Random.Range(0, 10);
                            if (decision < 4)//Move
                            {
                                estado = EnemyState.POSITION;
                            }
                            else//sumon
                            {
                                estado = EnemyState.SUMMON;
                            }
                            //Debug.Log("MEDIO");
                        }
                        else//CERCA
                        {
                                estado = EnemyState.ATTACK;
                            //Debug.Log("CERCA");
                        }
                        break;
                    case EnemyState.POSITION: // Alejarse de el jugador
                        this.GetComponent<Animator>().Play("Walk Forward In Place");
                        Vector3 distancia = -jugador.position + this.transform.position;
                        Vector3 newPosition = jugador.position + new Vector3(distancia.x, distancia.y, distancia.z) * 6;
                        this.GetComponent<NavMeshAgent>().SetDestination(newPosition);
                        yield return new WaitForSeconds(3f);
                        this.GetComponent<NavMeshAgent>().Stop();
                        //this.transform.LookAt(new Vector3(jugador.transform.position.x, this.transform.position.y, this.transform.position.z));
                        estado = EnemyState.DECIDE;
                        this.GetComponent<Animator>().Play("Idle");
                        break;
                    case EnemyState.SUMMON:
                        StartCoroutine("Summon");
                        break;
                    case EnemyState.ATTACK:
                        StartCoroutine("Attack");
                        break;
                }
            }
        }

    }

    IEnumerator Summon()
    {
        free = false;
        int sumon;
        GameObject enemigo;
        this.GetComponent<Animator>().Play("Cast Spell");
        this.GetComponent<AudioSource>().PlayOneShot(audios[2]);
        for (int i = 0; i < numSummons; i++)
        {
            
            sumon = Random.RandomRange(0, 4);// 1/4
            if (sumon == 0)//tank
            {
                 enemigo = Instantiate(Tank);
            }
            else//exploder
            {
                 enemigo = Instantiate(Exploder);
            }
            enemigo.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+1, this.transform.position.z)  + this.transform.forward * 15 + this.transform.right * Random.Range(-5, 5);
            misHijos.Add(enemigo);
            yield return new WaitForSeconds(1f);           
        }
        this.GetComponent<Animator>().Play("Idle");
        estado = EnemyState.DECIDE;
        yield return new WaitForSeconds(2f);
        free = true;
    }

    IEnumerator Attack()
    {
        free = false;
        if (Vector3.Distance(this.transform.position, jugador.position) > 20)//lejos
        {
            this.GetComponent<AudioSource>().PlayOneShot(audios[1]);
            this.GetComponent<Animator>().Play("Smash Attack");
            w = Instantiate(warningMeteo);
            w.transform.position = jugador.position;
            w.transform.localScale = new Vector3(w.transform.localScale.x + 5f, w.transform.localScale.y, w.transform.localScale.z + 5f);
            yield return new WaitForSeconds(1.5f);            
            m = Instantiate(meteo);
            m.GetComponent<Meteo>().dmg = this.damage;
            m.GetComponent<Meteo>().force = (int)this.force;
            m.transform.position = w.transform.position + w.transform.up * 10;
            Destroy(w.gameObject);
            this.GetComponent<AudioSource>().Stop();
            this.GetComponent<Animator>().Play("Idle");
            
        }
        else//cerca
        {
            this.GetComponent<AudioSource>().PlayOneShot(audios[0]);
            this.GetComponent<Animator>().Play("Stab Attack");
            this.GetComponent<NavMeshAgent>().Stop();
            this.GetComponent<NavMeshAgent>().enabled = false;
            this.transform.LookAt(new Vector3(jugador.position.x, this.transform.position.y, jugador.transform.position.z));
            w = Instantiate(warning);
            w.transform.position = this.transform.position + this.transform.forward * 10;
            w.transform.localScale = new Vector3(w.transform.localScale.x + 5f, w.transform.localScale.y, w.transform.localScale.z + 30f);
            w.transform.LookAt(this.transform);
            w.transform.parent = this.transform;
            yield return new WaitForSeconds(1.5f);
            Destroy(w);
            estado = EnemyState.CONFIRMEDATTACK;
            if (alive)
            {
                this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                this.gameObject.GetComponent<Rigidbody>().velocity = this.transform.forward * 250;                           
                yield return new WaitForSeconds(0.25f);
                this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                this.GetComponent<NavMeshAgent>().enabled = true;
            }
            if (alive)
            this.GetComponent<Animator>().Play("Idle");
        }


        estado = EnemyState.DECIDE;
        free = true;
        yield return new WaitForSeconds(1f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (estado == EnemyState.CONFIRMEDATTACK && collision.gameObject.tag == "Player")
        {
            Debug.Log("YEEEEEEEEEEEEEEEE");
            collision.gameObject.GetComponent<MovimientoJugador>().getHurt(this.damage, this.force);
        }
        else if (estado == EnemyState.CONFIRMEDATTACK && collision.gameObject.tag == "Wall")
        {
            Destroy(collision.gameObject);
        }
    }
    public override void GetHurt(int dmg, float force)
    {
        if (!inv)
        {
            if(dmg > 2)
            {
                THEGREATRK = false;
            }

            if (dmg == 0)
            {
                dmg = 1;
            }
            this.vida -= dmg;
            base.ThrowNum(dmg);
            actGUI.Raise();
            //Debug.Log("Me han golpeado: " + vida);
            if (this.vida <= 0 && alive)
            {
                this.alive = false;
                StopCoroutine("MaquinaEstados");
                this.GetComponent<NavMeshAgent>().enabled = false;
                StartCoroutine("Morirme");
            }
            else if (alive)
            {
                if (force > forceResist)
                {
                    StartCoroutine(Empuje(force));
                }
                this.GetComponent<Animator>().Play("Take Damage");
                StartCoroutine("CantTouchThis");
            }
        }
    }
    IEnumerator CantTouchThis()
    {
        inv = true;
        yield return new WaitForSeconds(0.25f);
        inv = false;
    }
    //POR si hacemos una bomba o algo con un monton de empuje
    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation |RigidbodyConstraints.FreezePositionY;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
    IEnumerator Morirme()
    {
        foreach(GameObject hijo in misHijos)
        {
            if (hijo != null && hijo.active)
            {
                hijo.GetComponent<IABASE>().GetHurt(999, 0);
            }
        }

        if (THEGREATRK)
        {
            party.GetComponent<TheGreatParty>().enchegar();
        }
        else
        {
            party.GetComponent<bossMusic>().win();
        }
        this.GetComponent<CapsuleCollider>().isTrigger = true;
        this.gameObject.tag = "Untagged";
        this.GetComponent<Animator>().Play("Die");
        //Debug.Log("Litch DIES");
        morir.Raise();              
        if (!jugador.gameObject.GetComponent<MovimientoJugador>().canDash)
        {
            GameObject reward = armasPool.SharedInstance.GetPooledObject();
            reward.GetComponent<Objeto>().cambiarHabilidad(loot[0]);
            reward.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z);
            reward.SetActive(true);
            reward.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(2, 4) + this.transform.forward * Random.Range(-4, 4) + this.transform.right * Random.Range(-4, 4), ForceMode.Impulse);
        }
        else if (!jugador.gameObject.GetComponent<MovimientoJugador>().canDoubleJump)
        {
            GameObject reward = armasPool.SharedInstance.GetPooledObject();
            reward.GetComponent<Objeto>().cambiarHabilidad(loot[1]);
            reward.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z);
            reward.SetActive(true);
            reward.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(2, 4) + this.transform.forward * Random.Range(-4, 4) + this.transform.right * Random.Range(-4, 4), ForceMode.Impulse);
        }
        else if (!jugador.gameObject.GetComponent<MovimientoJugador>().canWallClimb)
        {
            GameObject reward = armasPool.SharedInstance.GetPooledObject();
            reward.GetComponent<Objeto>().cambiarHabilidad(loot[2]);
            reward.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z);
            reward.SetActive(true);
            reward.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(2, 4) + this.transform.forward * Random.Range(-4, 4) + this.transform.right * Random.Range(-4, 4), ForceMode.Impulse);
        }
        else if (!jugador.gameObject.GetComponent<MovimientoJugador>().canHookIr)
        {
            GameObject reward = armasPool.SharedInstance.GetPooledObject();
            reward.GetComponent<Objeto>().cambiarHabilidad(loot[3]);
            reward.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z);
            reward.SetActive(true);
            reward.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(2, 4) + this.transform.forward * Random.Range(-4, 4) + this.transform.right * Random.Range(-4, 4), ForceMode.Impulse);
        }
        else
        {
            this.coins = this.coins * 2;            
        }
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(5f);
        GameObject portalBack = Instantiate(portal);
        portalBack.transform.position = this.transform.position;
        Destroy(this.gameObject);
    }
}
