using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Exploder_IA : IABASE
{
    private Transform jugador;
    public GameObject warning;
    private GameObject w;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        base.init();
        this.GetComponent<NavMeshAgent>().enabled = true;
        this.transform.GetChild(0).GetComponent<Hitbox>().dmg = this.damage;
        this.transform.GetChild(0).GetComponent<Hitbox>().force = this.force;
        StartCoroutine("Inmolate");
    }

   IEnumerator Inmolate()
    {
        while (Vector3.Distance(this.transform.position, jugador.position) > 3)
        {
            this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
            this.GetComponent<Animator>().Play("Walk Forward In Place");
            yield return new WaitForSeconds(0.5f);
        }

        this.GetComponent<NavMeshAgent>().Stop();
        this.GetComponent<NavMeshAgent>().enabled = false;
        w = Instantiate(warning);
        w.transform.position = this.transform.position;
        w.transform.localScale = new Vector3(w.transform.localScale.x + 3.7f, w.transform.localScale.y, w.transform.localScale.z + 3.7f);
        this.GetComponent<Animator>().Play("Smash Attack");
        yield return new WaitForSeconds(1f);//ESPERA
        this.transform.GetChild(0).gameObject.SetActive(true);       
        this.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.2f);//ESPERA
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.GetHurt(100, 0);
        this.GetComponent<AudioSource>().Play();
    }
    
    public override void GetHurt(int dmg, float force)
    {

        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.alive = false;
            StopAllCoroutines();
            this.GetComponent<NavMeshAgent>().enabled = false;
            if (w != null)
            {
                Destroy(w);
            }
            StartCoroutine("Morirme");
        }
        else if (alive)
        {
            if (force > forceResist)
            {
                StartCoroutine(Empuje(force));
            }
            this.GetComponent<Animator>().Play("Take Damage");
        }

    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    IEnumerator Morirme()
    {
        this.GetComponent<CapsuleCollider>().enabled = false;
        this.GetComponent<Animator>().Play("Die");
        this.gameObject.tag = "Untagged";
        // Debug.Log("Escarabajo DIES");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }
}
