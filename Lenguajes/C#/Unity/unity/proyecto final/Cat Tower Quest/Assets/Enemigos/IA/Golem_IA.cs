using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Golem_IA : IABASE
{

    //Variables de entrada
    public Transform jugador;
    public GameObject roca;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        base.init();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        this.estado = EnemyState.FOLLOW;
        this.GetComponent<NavMeshAgent>().enabled = true;
        StartCoroutine("MaquinaEstados");
    }

    IEnumerator MaquinaEstados()
    {
        //Debug.Log("Enciendo");
        while (this.alive)
        {
            yield return new WaitForSeconds(0.5f);

            switch (estado)
            {
                case EnemyState.FOLLOW:

                    RaycastHit hit;
                    
                    if (Physics.Raycast(this.transform.position, jugador.position- this.transform.position, out hit, 20))
                    {
                        Debug.DrawLine(this.transform.position, hit.point, Color.cyan, 20f);
                        //VEO AL JUGADOR
                        if (hit.transform.gameObject.tag == "Player" && hit.distance > 7 && hit.distance < 13)
                        {                          
                            estado = EnemyState.ATTACK;
                        }//NO VEO AL JUGADOR O ESTOY LEJOS/CERCA
                        else if(hit.transform.gameObject.tag == "Player")
                        {
                            if (Vector3.Distance(this.gameObject.transform.position, jugador.position) > 10)
                            {
                                this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
                                this.GetComponent<Animator>().Play("Walk");
                            }
                            else
                            {
                                Vector3 distancia = -jugador.position + this.transform.position;
                                Vector3 newPosition = jugador.position + distancia * 2;
                                this.GetComponent<NavMeshAgent>().SetDestination(newPosition);
                                this.GetComponent<Animator>().Play("Walk");
                            }
                        }
                        else
                        {
                            this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
                            this.GetComponent<Animator>().Play("Walk");
                        }
                    }
                    else
                    {                       
                            this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
                            this.GetComponent<Animator>().Play("Walk");                        
                    }
                    break;
                case EnemyState.ATTACK:
                    this.GetComponent<AudioSource>().Play();
                    this.GetComponent<Animator>().Play("Attack01");
                    this.GetComponent<NavMeshAgent>().Stop();
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    //APUNTAR
                    yield return new WaitForSeconds(0.25f);
                    this.transform.LookAt(jugador);
                    yield return new WaitForSeconds(0.25f);
                    this.transform.LookAt(jugador);
                    yield return new WaitForSeconds(0.25f);
                    this.transform.LookAt(jugador);
                    yield return new WaitForSeconds(0.25f);
                    //DISPARAR
                    this.transform.LookAt(jugador);
                    GameObject r1 = Instantiate(roca);
                    r1.transform.position = new Vector3(this.transform.position.x+Random.Range(-0.5f,0.5f), this.transform.position.y + 1 + Random.Range(-0.5f, 0.5f), this.transform.position.z + Random.Range(-0.5f, 0.5f));
                    r1.GetComponent<Proyectil>().dmg = damage;
                    r1.GetComponent<Proyectil>().force = force;
                    r1.transform.name = "Iker";
                    r1.GetComponent<Rigidbody>().velocity = this.transform.forward * (15 + Random.Range(-2, 2));
                    GameObject r2 = Instantiate(roca);
                    r2.transform.position = new Vector3(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y + 1 + Random.Range(-0.5f, 0.5f), this.transform.position.z + Random.Range(-0.5f, 0.5f));
                    r2.GetComponent<Proyectil>().dmg = damage;
                    r2.GetComponent<Proyectil>().force = force;
                    r2.GetComponent<Rigidbody>().velocity = this.transform.forward * (15 + Random.Range(-2, 2));
                    r2.transform.name = "Sergio";
                    GameObject r3 = Instantiate(roca);
                    r3.transform.position = new Vector3(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y + 1 + Random.Range(-0.5f, 0.5f), this.transform.position.z + Random.Range(-0.5f, 0.5f));
                    r3.GetComponent<Proyectil>().dmg = damage;
                    r3.GetComponent<Proyectil>().force = force;
                    r3.GetComponent<Rigidbody>().velocity = this.transform.forward * (15 + Random.Range(-2, 2));
                    r3.transform.name = "Alex";
                    GameObject r4 = Instantiate(roca);
                    r4.transform.position = new Vector3(this.transform.position.x + Random.Range(-0.5f, 0.5f), this.transform.position.y + 1 + Random.Range(-0.5f, 0.5f), this.transform.position.z + Random.Range(-0.5f, 0.5f));
                    r4.GetComponent<Proyectil>().dmg = damage;
                    r4.GetComponent<Proyectil>().force = force;
                    r4.GetComponent<Rigidbody>().velocity = this.transform.forward * (15 + Random.Range(-2, 2));
                    r4.transform.name = "Kuan";

                    this.GetComponent<NavMeshAgent>().enabled = true;
                    this.GetComponent<Animator>().Play("Victory");
                    yield return new WaitForSeconds(0.8f);
                    this.estado = EnemyState.FOLLOW;
                    break;
            }
        }

    }

    public override void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.alive = false;
            StopAllCoroutines();
            this.GetComponent<NavMeshAgent>().enabled = false;
            StartCoroutine("Morirme");
        }
        else if(alive)
        {
            if (force > forceResist)
            {
                StartCoroutine(Empuje(force));
            }
            this.GetComponent<Animator>().Play("GetHit");
        }

    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    IEnumerator Morirme()
    {
        this.GetComponent<BoxCollider>().enabled = false;
        this.gameObject.tag = "Untagged";
        this.GetComponent<Animator>().Play("Die");
        //Debug.Log("Golem DIES");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }
}
