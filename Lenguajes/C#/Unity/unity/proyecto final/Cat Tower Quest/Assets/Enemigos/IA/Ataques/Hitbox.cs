using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    public int dmg;
    public float force;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<MovimientoJugador>().getHurt(dmg, force);
        }
        else if (other.tag == "Destruible")
        {
            Destroy(other.gameObject);
        }
    }
}
