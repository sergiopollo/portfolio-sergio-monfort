using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IABASE : MonoBehaviour
{
    //Variables de entrada
    public EnemigoBase stats;
    public GameEvent morir;

    //Variables privadas
    public int vida;
    protected float force;
    protected float forceResist;
    protected int damage;
    protected int coins;
    protected EnemyState estado;
    protected bool alive;
    protected IEnumerator Coins;
    protected IEnumerator Nums;

    public void Start()
    {
        this.force = stats.force;
        this.forceResist = stats.forceResist;
        this.vida = stats.vida;
        this.damage = stats.damage;
        this.coins = stats.coins;
        this.alive = true;
        this.Coins = Throw();
    }

    public void init()
    {
        this.alive = true;
        this.vida = stats.vida;
        this.coins = stats.coins;
        this.force = stats.force;
        //Debug.Log("Mi vida PADRE: " + vida + " STATS: " + stats.vida);
        this.damage = stats.damage;
        this.forceResist = stats.forceResist;
        this.Coins = Throw();
    }

    public virtual void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        ThrowNum(dmg);
    }

    IEnumerator Throw()
    {
        //Debug.Log("Coins: " + coins);
        float speed = 0.1f;
        float force = 2;
        if (coins > 150)
        {
            speed = 0.005f;
            force = 5;
            Debug.Log("SPEEEEEEEEED");
        }
        for (int i = 0; i < coins; i++)
        {
            yield return new WaitForSeconds(speed);
            GameObject coin = coinPool.SharedInstance.GetPooledObject();
            coin.transform.position = new Vector3(this.transform.position.x + Random.Range(-1.5f, 1.5f), this.transform.position.y + 1, this.transform.position.z + Random.Range(-1.5f, 1.5f));
            coin.SetActive(true);
            coin.GetComponent<Rigidbody>().AddForce(coin.transform.forward * Random.Range(-6.5f, -8) + coin.transform.up * Random.Range(-force, force) + coin.transform.right * Random.Range(-2, 2), ForceMode.Impulse);
        }
    }

    public void ThrowNum(int num)
    {
        GameObject numero = numPool.SharedInstance.GetPooledObject();
        numero.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.transform.localScale.y + 1, this.transform.position.z);
        if (num >= 10)
        {
            numero.GetComponent<TMPro.TextMeshPro>().text = "-" + num + "!";
            numero.GetComponent<TMPro.TextMeshPro>().color = Color.red;
        }else if(num <= 5)
        {
            numero.GetComponent<TMPro.TextMeshPro>().text = "-" + num;
            numero.GetComponent<TMPro.TextMeshPro>().color = Color.white;
        }
        else
        {
            numero.GetComponent<TMPro.TextMeshPro>().text = "-" + num;
            numero.GetComponent<TMPro.TextMeshPro>().color = Color.yellow;
        }       
        numero.SetActive(true);
    }

}
