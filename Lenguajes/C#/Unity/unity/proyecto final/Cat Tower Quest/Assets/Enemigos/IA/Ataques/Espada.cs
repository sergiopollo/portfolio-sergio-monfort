using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espada : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {      
        StartCoroutine("Ataque");
    }

    IEnumerator Ataque()
    {
        this.GetComponent<Rigidbody>().velocity = this.transform.up * -20;
        yield return new WaitForSeconds(Random.Range(0.08f, 0.15f));
        this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(0.5f);
        this.GetComponent<Rigidbody>().velocity = this.transform.up * 5;
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
