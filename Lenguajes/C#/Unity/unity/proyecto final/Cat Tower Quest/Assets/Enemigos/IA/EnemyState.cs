using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState
{
    IDLE,
    FOLLOW,
    ATTACK,
    CONFIRMEDATTACK,
    RUN,
    POSITION,
    SUMMON,
    EXPLODE,
    DECIDE,
}
