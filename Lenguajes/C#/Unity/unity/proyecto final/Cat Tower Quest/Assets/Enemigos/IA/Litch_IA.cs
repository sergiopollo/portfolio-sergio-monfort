using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Litch_IA : IABASE
{
   //Variables de entrada
    public Transform jugador;
    public GameObject warning;
    public GameObject espada;

    //Variables privadas
    private GameObject w1;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        base.init();
        this.estado = EnemyState.RUN;
        this.GetComponent<NavMeshAgent>().enabled = true;
        StartCoroutine("MaquinaEstados");
    }

    IEnumerator MaquinaEstados()
    {
        while (this.alive)
        {
            yield return new WaitForSeconds(0.5f);          
            switch (estado)
            {
                case EnemyState.RUN:
                   
                    //HUIR
                    if (Vector3.Distance(this.gameObject.transform.position, jugador.position) < 15)
                    {
                        this.GetComponent<Animator>().Play("run");
                        for (int i = 0; i < 2; i++)
                        {
                            Vector3 distancia = -jugador.position + this.transform.position;
                            Vector3 newPosition = jugador.position + distancia * 2;
                            this.GetComponent<NavMeshAgent>().SetDestination(newPosition);                       
                            yield return new WaitForSeconds(1f);
                        }
                    }
                    else
                    {
                        this.GetComponent<Animator>().Play("victory");
                        yield return new WaitForSeconds(1f);
                    }              

                    estado = EnemyState.ATTACK;

                    break;
                case EnemyState.ATTACK:
                    this.GetComponent<Animator>().Play("attack02");
                    this.GetComponent<NavMeshAgent>().Stop();
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    this.transform.LookAt(new Vector3(jugador.position.x, this.transform.position.y, jugador.transform.position.z));
                    //INSTANCIA EL WARNING
                    w1 = Instantiate(warning);                 
                    for(int i  = 0; i < 10; i++)
                    {                     
                        if(i < 5)
                        {
                            w1.transform.position = new Vector3(jugador.position.x, this.transform.position.y, jugador.position.z);
                        }
                        w1.transform.localScale = new Vector3(w1.transform.localScale.x + 0.25f, w1.transform.localScale.y, w1.transform.localScale.z + 0.25f);
                       
                        if(i == 9)
                        {
                            StartCoroutine("playMusic");
                        }
                        yield return new WaitForSeconds(0.25f);
                    }

                    
                   

                    //INSTANCIA EL ATAQUE
                    GameObject e1 = Instantiate(espada);
                    e1.transform.position = new Vector3(w1.transform.position.x, w1.transform.position.y-5, w1.transform.position.z);
                    e1.GetComponent<Hitbox>().dmg = this.damage;
                    e1.GetComponent<Hitbox>().force = this.force;
                    GameObject e2 = Instantiate(espada);
                    e2.transform.position = new Vector3(w1.transform.position.x+ 1f, w1.transform.position.y - 5, w1.transform.position.z);
                    e2.transform.eulerAngles = new Vector3(e2.transform.eulerAngles.x + Random.Range(-15, 15), e2.transform.eulerAngles.y + Random.Range(-50, 50), e2.transform.eulerAngles.z + Random.Range(-15, 15));
                    e2.GetComponent<Hitbox>().dmg = this.damage;
                    e2.GetComponent<Hitbox>().force = this.force;
                    GameObject e3 = Instantiate(espada);
                    e3.transform.position = new Vector3(w1.transform.position.x-1f, w1.transform.position.y - 5, w1.transform.position.z);
                    e3.transform.eulerAngles = new Vector3(e3.transform.eulerAngles.x + Random.Range(-15, 15), e3.transform.eulerAngles.y + Random.Range(-50, 50), e3.transform.eulerAngles.z + Random.Range(-15, 15));
                    e3.GetComponent<Hitbox>().dmg = this.damage;
                    e3.GetComponent<Hitbox>().force = this.force;
                    GameObject e4 = Instantiate(espada);
                    e4.transform.position = new Vector3(w1.transform.position.x, w1.transform.position.y - 5, w1.transform.position.z +1f);
                    e4.transform.eulerAngles = new Vector3(e4.transform.eulerAngles.x + Random.Range(-15, 15), e4.transform.eulerAngles.y + Random.Range(-50, 50), e4.transform.eulerAngles.z + Random.Range(-15, 15));
                    e4.GetComponent<Hitbox>().dmg = this.damage;
                    e4.GetComponent<Hitbox>().force = this.force;
                    GameObject e5 = Instantiate(espada);
                    e5.transform.position = new Vector3(w1.transform.position.x, w1.transform.position.y - 5, w1.transform.position.z-1f);
                    e5.transform.eulerAngles = new Vector3(e5.transform.eulerAngles.x + Random.Range(-15, 15), e5.transform.eulerAngles.y + Random.Range(-50, 50), e5.transform.eulerAngles.z + Random.Range(-15, 15));
                    e5.GetComponent<Hitbox>().dmg = this.damage;
                    e5.GetComponent<Hitbox>().force = this.force;
                    Destroy(w1.gameObject);
                    



                    this.GetComponent<NavMeshAgent>().enabled = true;
                    this.GetComponent<Animator>().Play("Idle");
                    this.estado = EnemyState.RUN;
                    break;
            }
        }

    }
    IEnumerator playMusic()
    {
        this.GetComponent<AudioSource>().Play();
       
        yield return new WaitForSeconds(0.5f);
       
    }

    public override void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.alive = false;
            StopAllCoroutines();
            this.GetComponent<NavMeshAgent>().enabled = false;
            StartCoroutine("Morirme");
        }
        else if(alive)
        {
            if (force > forceResist)
            {
                StartCoroutine(Empuje(force));
            }
            this.GetComponent<Animator>().Play("GetHit");
        }

    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
    IEnumerator Morirme()
    {
        this.GetComponent<CapsuleCollider>().enabled = false;
        this.gameObject.tag = "Untagged";
        if (w1 != null)
        {
            Destroy(w1);
        }
        this.GetComponent<Animator>().Play("die");
        //Debug.Log("Litch DIES");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }

}
