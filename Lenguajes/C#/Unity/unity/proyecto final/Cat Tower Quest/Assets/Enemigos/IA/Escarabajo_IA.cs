using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;


public class Escarabajo_IA : IABASE
{

    //Variables de entrada
    public Transform jugador;
    public GameObject warning;
    public GameObject num;

    //Variables privadas
    private GameObject w1;

    // Ajustamos las estadisticas del enemigo
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        base.init();
        //this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
        //Debug.Log("Mi vida: " + vida + " STATS: " + stats.vida);
        this.estado = EnemyState.FOLLOW;
        this.GetComponent<NavMeshAgent>().enabled = true;     
        StartCoroutine("MaquinaEstados");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Comportamiento del enemigo
    IEnumerator MaquinaEstados()
    {
      
        while (this.alive)
        {
            yield return new WaitForSeconds(0.5f);

            switch (estado)
            {
                case EnemyState.FOLLOW:
                    if (Vector3.Distance(this.gameObject.transform.position, jugador.position) < 10)
                    {
                        estado = EnemyState.ATTACK;
                    }
                    else
                    {
                        //Debug.Log("HUMMMMM");
                        this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
                        this.GetComponent<Animator>().Play("Walk Forward In Place");
                    }
                    
                    break;
                case EnemyState.ATTACK:
                    //Debug.Log("Ataco");
                    
                    
                    this.GetComponent<Animator>().Play("Stab Attack");
                    this.GetComponent<NavMeshAgent>().Stop();
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    this.transform.LookAt(new Vector3(jugador.position.x, this.transform.position.y, jugador.transform.position.z));
                    w1 = Instantiate(warning);
                    w1.transform.position = this.transform.position + this.transform.forward *4;
                    w1.transform.LookAt(this.transform);
                    w1.transform.parent = this.transform;
                    yield return new WaitForSeconds(1.5f);
                    Destroy(w1);
                    this.GetComponent<AudioSource>().Play();
                    estado = EnemyState.CONFIRMEDATTACK;
                    //Debug.Log("BUM!!!");
                   
                    this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                    this.gameObject.GetComponent<Rigidbody>().velocity = this.transform.forward*50;
                    yield return new WaitForSeconds(0.2f);
                    this.gameObject.GetComponent<Rigidbody>().velocity = this.transform.forward * 0;
                    this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                    this.GetComponent<NavMeshAgent>().enabled = true;
                    this.GetComponent<Animator>().Play("Idle");
                    this.estado = EnemyState.FOLLOW;
                    break;
            }
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(estado == EnemyState.CONFIRMEDATTACK && collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<MovimientoJugador>().getHurt(this.damage, this.force);
        }
        else if (collision.gameObject.tag == "Destruible")
        {
            Destroy(collision.gameObject);
        }
    }

    public override void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }

        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.alive = false;
            StopAllCoroutines();
            this.GetComponent<NavMeshAgent>().enabled = false;
            if(w1 != null)
            {
                Destroy(w1);
            }
            StartCoroutine("Morirme");
        }
        else if(alive)
        {
            if (force > forceResist)
            {
                StartCoroutine(Empuje(force));
            }
            this.GetComponent<Animator>().Play("Take Damage");
        }
        
    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    IEnumerator Morirme()
    {
        this.GetComponent<NavMeshAgent>().enabled = true;
        this.GetComponent<CapsuleCollider>().enabled = false;
        this.gameObject.tag = "Untagged";
        // Debug.Log("Escarabajo DIES");
        this.GetComponent<Animator>().Play("Die");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }
}
