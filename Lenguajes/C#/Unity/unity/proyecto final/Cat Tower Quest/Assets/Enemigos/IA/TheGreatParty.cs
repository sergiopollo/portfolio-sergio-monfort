using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TheGreatParty : MonoBehaviour
{

    public GameObject[] Rokets;
    public GameObject RoketL;
    public GameEvent TheEnd;
    public GameObject boss;
    float colorHue = 0f;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            RoketL.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            boss.GetComponent<Sindulf_IA>().vida = 1;
        }
    }
    public void enchegar()
    {
        TheEnd.Raise();
        this.transform.GetChild(6).GetComponent<Luces>().lucesStart();
        this.transform.GetChild(0).gameObject.SetActive(true);
        this.transform.GetChild(1).gameObject.SetActive(true);
        this.transform.GetChild(2).gameObject.SetActive(true);
        this.transform.GetChild(3).gameObject.SetActive(true);
        this.transform.GetChild(4).gameObject.SetActive(true);
        this.transform.GetChild(5).gameObject.SetActive(true);
        StartCoroutine("Epilepsia");
        StartCoroutine("rotar");
        StartCoroutine("Pums");
        this.gameObject.GetComponent<bossMusic>().startParty();
    }
     
    IEnumerator Epilepsia()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            colorHue += 0.1f;
            colorHue %= 1f;
            this.transform.GetChild(0).GetComponent<TextMeshPro>().color = Color.HSVToRGB(colorHue, 1, 1f);
        }
    }

    IEnumerator rotar()
    {

        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.05f);
            this.transform.GetChild(0).Rotate(0, 0, 2);
        }

        while (true)
        {
            for (int i = 0; i < 20; i++)
            {
                yield return new WaitForSeconds(0.05f);
                this.transform.GetChild(0).Rotate(0, 0, -2);
            }
            for (int i = 0; i < 20; i++)
            {
                yield return new WaitForSeconds(0.05f);
                this.transform.GetChild(0).Rotate(0, 0, 2);
            }
        }
    }

    IEnumerator Pums()
    {
        while(true){

            yield return new WaitForSeconds(0.25f);
            GameObject fw = Instantiate(Rokets[Random.Range(0, 5)]);
            fw.transform.position = new Vector3(this.transform.position.x + Random.Range(-100, 100), -130 + (Random.Range(10,30)), this.transform.position.z + Random.Range(-100, 100));
        }
        
    }

}
