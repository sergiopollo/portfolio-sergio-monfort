using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Soldado_IA : IABASE
{

    //Variables de entrada
    public Transform jugador;

    //Variable privadas
    private bool stunnable;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        base.init();
        this.transform.GetChild(2).GetComponent<Hitbox>().dmg = this.damage;
        this.transform.GetChild(2).GetComponent<Hitbox>().force = this.force;
        this.transform.GetChild(2).gameObject.SetActive(false);
        stunnable = true;
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;       
        this.estado = EnemyState.FOLLOW;
        this.GetComponent<NavMeshAgent>().enabled = true;
        StartCoroutine("MaquinaEstados");
    }

    IEnumerator MaquinaEstados()
    {
        //Debug.Log("Enciendo");
        while (this.alive)
        {
            yield return new WaitForSeconds(0.15f);

            switch (estado)
            {
                case EnemyState.FOLLOW:
                    if (Vector3.Distance(this.gameObject.transform.position, jugador.position) < 4)
                    {
                        estado = EnemyState.ATTACK;
                    }
                    else
                    {
                        this.GetComponent<NavMeshAgent>().SetDestination(jugador.position);
                        this.GetComponent<Animator>().Play("Run");
                    }

                    break;
                case EnemyState.ATTACK:
                    this.GetComponent<AudioSource>().Play();
                    this.GetComponent<Animator>().Play("Attack01");
                    this.GetComponent<NavMeshAgent>().Stop();
                    this.GetComponent<NavMeshAgent>().enabled = false;                   
                    this.transform.LookAt(new Vector3(jugador.position.x, this.transform.position.y, jugador.transform.position.z));
                    yield return new WaitForSeconds(0.5f);
                    this.transform.GetChild(2).gameObject.SetActive(true);
                    yield return new WaitForSeconds(0.3f);
                    this.transform.GetChild(2).gameObject.SetActive(false);                   
                    yield return new WaitForSeconds(0.64f);
                    this.GetComponent<NavMeshAgent>().enabled = true;
                    this.GetComponent<Animator>().Play("Idle");
                    this.estado = EnemyState.FOLLOW;
                    break;
            }
        }

    }

    public override void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.transform.GetChild(2).gameObject.SetActive(false);
            this.alive = false;
            StopAllCoroutines();
            this.GetComponent<NavMeshAgent>().enabled = false;
            StartCoroutine("Morirme");
        }
        else if(alive)
        {
            if (stunnable)
            {
                StartCoroutine("Stuned");
            }
            else
            {
                if (force > forceResist)
                {
                    StartCoroutine(Empuje(force));
                }
                this.GetComponent<Animator>().Play("GetHit");
            }
            
        }

    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    public IEnumerator Stuned()
    {
        //Debug.Log("EEEEEEEEEEEEEEEEEEEEEEE");
        stunnable = false;
        if (this.GetComponent<NavMeshAgent>().enabled == true)
        {
            this.GetComponent<NavMeshAgent>().enabled = false;
        }
        StopCoroutine("MaquinaEstados");
        this.GetComponent<Animator>().Play("GetHit");
        yield return new WaitForSeconds(1.5f);
        if (this.GetComponent<NavMeshAgent>().enabled == false)
        {
            this.GetComponent<NavMeshAgent>().enabled = true;
        }
        estado = EnemyState.FOLLOW;
        StartCoroutine("MaquinaEstados");
        yield return new WaitForSeconds(5f);
        stunnable = true;

    }

    IEnumerator Morirme()
    {
        //Debug.Log("Caballero DIES");
        this.GetComponent<CapsuleCollider>().enabled = false;
        this.gameObject.tag = "Untagged";
        this.GetComponent<Animator>().Play("Death");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }

}
