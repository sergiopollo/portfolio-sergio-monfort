using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Enemigo", order = 1)]
public class EnemigoBase : ScriptableObject
{
    //Parametros que comparten todos los enemigos
    public int vida;
    public int damage;
    public int coins;
    public float forceResist;
    public float force;

}
