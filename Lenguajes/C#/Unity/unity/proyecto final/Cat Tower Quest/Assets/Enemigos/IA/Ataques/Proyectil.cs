using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour
{

    public int dmg;
    public float force;

    private void Start()
    {
        StartCoroutine("morir");
    }

    IEnumerator morir()
    {
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<MovimientoJugador>().getHurt(dmg, force);            
        }
        if(other.gameObject.tag != "Enemigo")
        {
            Destroy(this.gameObject);
        }
        else if (other.tag == "Destruible")
        {
            Destroy(other.gameObject);
        }
    }
}
