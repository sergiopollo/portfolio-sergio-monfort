using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class numero : MonoBehaviour
{

    GameObject jugador;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player");
        //this.transform.eulerAngles = new Vector3(this.transform.eulerAngles.x + Random.Range(-15, 15), this.transform.eulerAngles.y, this.transform.eulerAngles.z);
        this.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(5,7), ForceMode.Impulse);
        StartCoroutine("Vida");
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(jugador.transform.GetChild(3));
    }

    IEnumerator Vida()
    {
        yield return new WaitForSeconds(2.5f);
        this.gameObject.SetActive(false);
    }
}
