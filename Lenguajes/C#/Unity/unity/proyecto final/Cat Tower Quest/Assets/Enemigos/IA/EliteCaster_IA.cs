using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EliteCaster_IA : IABASE
{
    //Variables de entrada
    public Transform jugador;
    public GameObject warning;
    public GameObject espada;
    public Arma[] loot; 

    //Variables privadas
    private GameObject w1;

    private Vector3 ata;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        this.GetComponent<NavMeshAgent>().enabled = false;
        jugador = GameObject.Find("Player").transform;
        base.init();
        this.estado = EnemyState.RUN;
        this.GetComponent<NavMeshAgent>().enabled = true;
        StartCoroutine("MaquinaEstados");
    }

    IEnumerator MaquinaEstados()
    {
        while (this.alive)
        {
            yield return new WaitForSeconds(0.5f);
            switch (estado)
            {
                case EnemyState.RUN:

                    //HUIR
                    if (Vector3.Distance(this.gameObject.transform.position, jugador.position) < 10)
                    {
                        this.GetComponent<Animator>().Play("run");
                        for (int i = 0; i < 2; i++)
                        {
                            Vector3 distancia = -jugador.position + this.transform.position;
                            Vector3 newPosition = jugador.position + distancia * 2;
                            this.GetComponent<NavMeshAgent>().SetDestination(newPosition);
                            yield return new WaitForSeconds(1f);
                        }
                    }
                    else
                    {
                        this.GetComponent<Animator>().Play("victory");
                        yield return new WaitForSeconds(1f);
                    }

                    estado = EnemyState.ATTACK;

                    break;
                case EnemyState.ATTACK:
                    this.GetComponent<Animator>().Play("attack01");
                    this.GetComponent<NavMeshAgent>().Stop();
                    this.GetComponent<NavMeshAgent>().enabled = false;
                    this.transform.LookAt(new Vector3(jugador.position.x, this.transform.position.y, jugador.transform.position.z));
                    //INSTANCIA LOS WARNING                  
                    StartCoroutine(Warning(w1));
                    yield return new WaitForSeconds(0.7f);
                    StartCoroutine(Warning(w1));
                    yield return new WaitForSeconds(0.7f);
                    StartCoroutine(Warning(w1));
                    yield return new WaitForSeconds(0.7f);
                    StartCoroutine(Warning(w1));
                    yield return new WaitForSeconds(0.7f);
                    StartCoroutine(Warning(w1));                   
                    this.GetComponent<NavMeshAgent>().enabled = true;
                    this.GetComponent<Animator>().Play("Idle");
                    
                    yield return new WaitForSeconds(4f);
                    this.estado = EnemyState.RUN;
                    break;
            }
        }

    }

    public void Atacar()
    {
        this.GetComponent<AudioSource>().PlayOneShot(this.GetComponent<AudioSource>().clip);
        this.transform.LookAt(new Vector3(jugador.transform.position.x, this.transform.position.y, jugador.transform.position.z));
        GameObject e1 = Instantiate(espada);
        e1.transform.position = new Vector3(ata.x, ata.y - 5, ata.z);
        e1.transform.localScale = e1.transform.localScale * 1.25f;
        e1.GetComponent<Hitbox>().dmg = this.damage;
        e1.GetComponent<Hitbox>().force = this.force;
        GameObject e2 = Instantiate(espada);
        e2.transform.position = new Vector3(ata.x + 1f, ata.y - 5,ata.z);
        e2.transform.eulerAngles = new Vector3(e2.transform.eulerAngles.x + Random.Range(-10, 10), e2.transform.eulerAngles.y + Random.Range(-50, 50), e2.transform.eulerAngles.z + Random.Range(-10, 10));
        e2.transform.localScale = e1.transform.localScale * 1.25f;
        e2.GetComponent<Hitbox>().dmg = this.damage;
        e2.GetComponent<Hitbox>().force = this.force;
        GameObject e3 = Instantiate(espada);
        e3.transform.position = new Vector3(ata.x - 1f, ata.y - 5, ata.z);
        e3.transform.eulerAngles = new Vector3(e3.transform.eulerAngles.x + Random.Range(-10, 10), e3.transform.eulerAngles.y + Random.Range(-50, 50), e3.transform.eulerAngles.z + Random.Range(-10, 10));
        e3.transform.localScale = e1.transform.localScale * 1.25f;
        e3.GetComponent<Hitbox>().dmg = this.damage;
        e3.GetComponent<Hitbox>().force = this.force;
        GameObject e4 = Instantiate(espada);
        e4.transform.position = new Vector3(ata.x, ata.y - 5,ata.z + 1f);
        e4.transform.eulerAngles = new Vector3(e4.transform.eulerAngles.x + Random.Range(-10, 10), e4.transform.eulerAngles.y + Random.Range(-50, 50), e4.transform.eulerAngles.z + Random.Range(-10, 10));
        e4.transform.localScale = e1.transform.localScale * 1.25f;
        e4.GetComponent<Hitbox>().dmg = this.damage;
        e4.GetComponent<Hitbox>().force = this.force;
        GameObject e5 = Instantiate(espada);
        e5.transform.position = new Vector3(ata.x, ata.y - 5,ata.z - 1f);
        e5.transform.eulerAngles = new Vector3(e5.transform.eulerAngles.x + Random.Range(-10, 10), e5.transform.eulerAngles.y + Random.Range(-50, 50), e5.transform.eulerAngles.z + Random.Range(-10, 10));
        e5.transform.localScale = e1.transform.localScale * 1.25f;
        e5.GetComponent<Hitbox>().dmg = this.damage;
        e5.GetComponent<Hitbox>().force = this.force;
        GameObject e6 = Instantiate(espada);
        e6.transform.position = new Vector3(ata.x+1f, ata.y - 5, ata.z - 1f);
        e6.transform.eulerAngles = new Vector3(e6.transform.eulerAngles.x + Random.Range(-5, 5), e6.transform.eulerAngles.y + Random.Range(-50, 50), e6.transform.eulerAngles.z + Random.Range(-5, 5));
        e6.transform.localScale = e1.transform.localScale * 1.25f;
        e6.GetComponent<Hitbox>().dmg = this.damage;
        e6.GetComponent<Hitbox>().force = this.force;
        GameObject e7 = Instantiate(espada);
        e7.transform.position = new Vector3(ata.x-1f, ata.y - 5, ata.z + 1f);
        e7.transform.eulerAngles = new Vector3(e7.transform.eulerAngles.x + Random.Range(-5, 5), e7.transform.eulerAngles.y + Random.Range(-50, 50), e7.transform.eulerAngles.z + Random.Range(-5, 5));
        e7.transform.localScale = e1.transform.localScale * 1.25f;
        e7.GetComponent<Hitbox>().dmg = this.damage;
        e7.GetComponent<Hitbox>().force = this.force;       
    }
    IEnumerator Warning(GameObject w)
    {
        w = Instantiate(warning);

        for (int i = 0; i < 15; i++)
        {
            if (alive)
            {
                if (i < 12)
                {
                    w.transform.position = new Vector3(jugador.position.x, this.transform.position.y, jugador.position.z);
                    w.transform.localScale = new Vector3(w.transform.localScale.x + 0.3f, w.transform.localScale.y, w.transform.localScale.z + 0.3f);

                }
                yield return new WaitForSeconds(0.25f);
            }
           
        }
        if (alive)
        {
            this.GetComponent<Animator>().Play("attack01");
            ata = w.transform.position;
            Atacar();
        }       
        Destroy(w.gameObject);
    }

    public override void GetHurt(int dmg, float force)
    {
        if (dmg == 0)
        {
            dmg = 1;
        }
        this.vida -= dmg;
        base.ThrowNum(dmg);
        //Debug.Log("Me han golpeado: " + vida);
        if (this.vida <= 0 && alive)
        {
            this.alive = false;
            StopCoroutine("MaquinaEstados");
            this.GetComponent<NavMeshAgent>().enabled = false;
            StartCoroutine("Morirme");
        }
        else if (alive)
        {
            if (force > forceResist)
            {
                StartCoroutine(Empuje(force));
            }
            this.GetComponent<Animator>().Play("GetHit");
        }

    }

    IEnumerator Empuje(float force)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        this.GetComponent<Rigidbody>().AddForce(jugador.transform.forward * force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
    IEnumerator Morirme()
    {
        this.gameObject.tag = "Untagged";
        this.GetComponent<CapsuleCollider>().enabled = false;
        this.GetComponent<Animator>().Play("die");
        //Debug.Log("Litch DIES");
        morir.Raise();
        StartCoroutine(base.Coins);
        yield return new WaitForSeconds(2.5f);
        this.transform.GetChild(1).gameObject.SetActive(false);
        GameObject reward = armasPool.SharedInstance.GetPooledObject();
        int random2 = Random.Range(0, 100);
        if (random2 < 5)
        {
            reward.GetComponent<Objeto>().cambiarArma(loot[2]);
        }
        else
        {
            random2 = Random.Range(0, loot.Length);
            reward.GetComponent<Objeto>().cambiarArma(loot[random2]);
        }        
        reward.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 5, this.transform.position.z);
        reward.SetActive(true);
        reward.GetComponent<Rigidbody>().AddForce(this.transform.up * Random.Range(2,4) + this.transform.forward * Random.Range(-4, 4) + this.transform.right * Random.Range(-4, 4), ForceMode.Impulse);       
        yield return new WaitForSeconds(2.5f);
        Destroy(this.gameObject);
    }

}
