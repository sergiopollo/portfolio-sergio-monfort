using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteo : MonoBehaviour
{
    public int dmg;
    public int force;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<MovimientoJugador>().getHurt(dmg*2, 0);            
        }
        if (other.tag == "Enemigo")
        {
            other.gameObject.GetComponent<IABASE>().GetHurt(dmg * 2, 0);
        }
        if (other.tag == "Suelo")
        {
            StartCoroutine("Destruir");
        }
    }

    IEnumerator Destruir()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
