using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] float hookForce = 25f;

    Grapple grapple;
    Rigidbody rigid;
    LineRenderer lineRenderer;
    private GameObject camera;
    public Rigidbody rigidHit;



    public void Initialize(Grapple grapple, Transform shootTransform)
    {
        
        
        transform.forward = shootTransform.forward;
        this.grapple = grapple;
        rigid = GetComponent<Rigidbody>();
        lineRenderer = GetComponent<LineRenderer>();
        rigid.AddForce(transform.forward * hookForce, ForceMode.Impulse);
        //  print(rigid.AddForce(transform.forward * hookForce, ForceMode.Impulse));
        camera = GameObject.Find("VisionCamara");
    }

    // Update is called once per frame
    void Update()
    {




        Vector3[] positions = new Vector3[]
            {

                transform.position,
                grapple.transform.position
            };

        lineRenderer.SetPositions(positions);
    }

    private void OnCollisionEnter(Collision other)
    {
        rigid.velocity = Vector3.zero;
        if (rigidHit == null)
        {
            //Destroy(this.gameObject);
            if (other.gameObject.tag == "Enemigo" || other.gameObject.tag == "Destruible")
            {

                rigidHit = other.gameObject.GetComponent<Rigidbody>();

                rigid.useGravity = false;
                rigid.isKinematic = true;

                grapple.StartPull();
            }
            rigid.useGravity = false;
            rigid.isKinematic = true;
            grapple.StartPull();
        }
    }
        
}
