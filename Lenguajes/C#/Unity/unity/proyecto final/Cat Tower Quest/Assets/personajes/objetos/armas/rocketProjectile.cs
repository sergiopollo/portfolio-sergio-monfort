using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rocketProjectile : MonoBehaviour
{
    public GameObject explosion;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (other.GetComponent<Rigidbody>().velocity.y <= 0)
            {
                other.GetComponent<Rigidbody>().velocity = new Vector3(other.GetComponent<Rigidbody>().velocity.x, 0, other.GetComponent<Rigidbody>().velocity.z);
            }
            other.GetComponent<Rigidbody>().AddForce(new Vector3(0, 8, 0), ForceMode.Impulse);
            this.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        GameObject newExplosion = Instantiate(explosion);
        newExplosion.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - 1, this.transform.position.z);
    }

}
