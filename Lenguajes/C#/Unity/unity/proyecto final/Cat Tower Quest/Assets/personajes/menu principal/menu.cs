using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menu : MonoBehaviour
{
    //new input system
    InputPlayer inputs;

    public GameObject menuBotones;
    public GameObject selector;
    public int index;
    public MovimientoJugador player;
    public tienda tienda;

    private void Awake()
    {
        inputs = new InputPlayer();
        inputs.Enable();
        ponerInputs();

    }

    // Start is called before the first frame update
    void Start()
    {

    }

    private void ponerInputs()
    {
        inputs.player.menuMoveLeft.started += LeftDown;
        inputs.player.menuMoveRight.started += RightDown;
        inputs.player.seleccionarMenu.performed += SelectDown;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            seleccionar();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            if (index == menuBotones.transform.GetChild(1).transform.childCount - 1)
            {
                index = 0;
            }
            else
            {
                index++;
            }
            moverSelector();
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            if (index == 0)
            {
                index = menuBotones.transform.GetChild(1).transform.childCount - 1;
            }
            else
            {
                index--;
            }
            moverSelector();
        }
        */
    }

    //new input system
    private void SelectDown(InputAction.CallbackContext context)
    {
        
        seleccionar();
        
    }

    private void LeftDown(InputAction.CallbackContext context)
    {
        if (index == 0)
        {
            index = menuBotones.transform.GetChild(1).transform.childCount - 1;
        }
        else
        {
            index--;
        }
        moverSelector();
        
    }

    private void RightDown(InputAction.CallbackContext context)
    {
        if (index == menuBotones.transform.GetChild(1).transform.childCount - 1)
        {
            index = 0;
        }
        else
        {
            index++;
        }
        moverSelector();
    }

    public void moverSelector()
    {
        selector.transform.position = menuBotones.transform.GetChild(1).transform.GetChild(index).transform.position;
    }

    public void seleccionar()
    {
        menuBotones.transform.GetChild(1).transform.GetChild(index).GetComponent<Button>().onClick.Invoke();
    }


    public void newGame()
    {
        tienda.resetTienda();
        player.resetInventory();
        SceneManager.LoadScene("Lobby");
    }

    public void LoadGame()
    {
        bool puedoCargar = this.GetComponent<SaveBueno>().cargarPartida();
        if (puedoCargar)
        {
            SceneManager.LoadScene("Lobby");
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
        inputs.player.menuMoveLeft.started -= LeftDown;
        inputs.player.menuMoveRight.started -= RightDown;
        inputs.player.seleccionarMenu.performed -= SelectDown;
    }

}
