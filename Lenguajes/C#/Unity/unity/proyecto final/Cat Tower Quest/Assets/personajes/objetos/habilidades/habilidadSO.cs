using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/habilidad", order = 1)]
public class habilidadSO : ScriptableObject
{
    public habilidadesTienda habilidad;
    public GameObject modelo;
    public Color colorLuz;
}