using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objeto : MonoBehaviour
{
    public bool isArma;
    public AudioClip clip;
    public habilidadSO habilidadSO;
    public Arma arma;

    // Start is called before the first frame update
    void Start()
    {
        inicializar();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 100 * Time.deltaTime, 0);
    }

    //al instanciar una habilidad llamar a inicializar y pasarle que habilidad es
    public void inicializar()
    {
        if (isArma)
        {
            this.transform.GetChild(2).transform.gameObject.SetActive(false);
            GameObject modelo = Instantiate(arma.modelo);
            modelo.transform.position = this.transform.position;
            modelo.transform.parent = gameObject.transform;
            this.transform.GetChild(1).GetComponent<Light>().color = arma.colorLuz;
        }
        else
        {
            this.transform.GetChild(2).transform.gameObject.SetActive(false);
            GameObject modelo = Instantiate(habilidadSO.modelo);
            modelo.transform.position = this.transform.position;
            modelo.transform.parent = gameObject.transform;
            this.transform.GetChild(1).GetComponent<Light>().color = habilidadSO.colorLuz;
        }
        
    }

    public void cambiarHabilidad(habilidadSO miHabilidad)
    {
        habilidadSO = miHabilidad;
        inicializar();
    }

    public void cambiarArma(Arma miArma)
    {
        arma = miArma;
        isArma = true;
        inicializar();
    }

    /*
    private void OnEnable()
    {

    }
    */

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(clip, other.GetComponent<MovimientoJugador>().vCam.transform.position, 1.0F);
            if (isArma)
            {
                other.GetComponent<MovimientoJugador>().getArma(arma);
            }
            else
            {
                other.GetComponent<MovimientoJugador>().getAbility(habilidadSO.habilidad);
            }
            this.gameObject.SetActive(false);
        }

    }
}
