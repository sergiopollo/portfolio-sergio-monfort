using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/arma", order = 1)]
[System.Serializable]

public class Arma : ScriptableObject
{
    public GameObject modelo;
    public Sprite imagen;
    public int ammo;
    public int maxAmmo;
    public Color colorLuz;

    //proyectiles
    public int numProjectiles;
    public GameObject projectile;
    public float projectileSpeed;
    public Vector3 projectileForce;

    public void PickUp()
    {
        this.ammo = this.maxAmmo;
    }

}
