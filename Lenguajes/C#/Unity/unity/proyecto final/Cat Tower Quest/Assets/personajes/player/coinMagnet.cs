using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinMagnet : MonoBehaviour
{
    public PlayerInventory player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (player.canCoinMagnet)
        {
            if (other.tag == "Coin")
            {
                other.GetComponentInParent<Rigidbody>().AddForce((this.transform.position - other.transform.position)*10);
            }
        }
    }
}
