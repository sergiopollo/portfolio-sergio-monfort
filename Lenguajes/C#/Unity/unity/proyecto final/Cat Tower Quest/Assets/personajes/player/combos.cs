using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class combos : MonoBehaviour
{

    //new input system
    InputPlayer inputs;

    private int numCombo;
    private int comboAereo;
    private bool tooSoon;
    public Animator animator;
    public GameObject hitbox1;
    public GameObject hitbox2;
    public GameObject hitbox3;
    public GameObject hitboxAereo1;
    public GameObject hitboxAereo2;
    public GameObject hitboxFinisher;
    public GameObject explosion;
    public int super;
    public int maxSuper;
    public GameObject AuraSuper;
    private GameObject camara;

    Vector3 FocusedPosition;
    // Start is called before the first frame update

    private void Awake()
    {
        inputs = new InputPlayer();
        inputs.Enable();
        ponerInputs();
    }

    public void ponerInputs()
    {
        inputs.player.attack.performed += MouseButton0Down;
    }

    void Start()
    {
        // state = States.hit1Tierra;
        numCombo = 0;
        comboAereo = 0;
        tooSoon = false;
        camara = GameObject.Find("Camera");
        hitbox1.GetComponent<hitboxAtaque>().dmgMod = 1;
        hitbox2.GetComponent<hitboxAtaque>().dmgMod = 1.25f;
        hitbox3.GetComponent<hitboxAtaque>().dmgMod = 2;

        hitboxAereo1.GetComponent<hitboxAtaque>().dmgMod = 1.25f;
        hitboxAereo2.GetComponent<hitboxAtaque>().dmgMod = 2;
        hitboxFinisher.GetComponent<hitboxAtaque>().dmgMod = 3;
        AuraSuper.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void MouseButton0Down(InputAction.CallbackContext context)
    {
        if (this.GetComponent<MovimientoJugador>().isGrounded && super >= maxSuper && this.GetComponent<MovimientoJugador>().enemigoFijado != null)
        {
            StartCoroutine("SuperAtaque");
        }
    }

    public void giveSuper(int addSuper)
    {
        this.super += addSuper;
        if (super >= maxSuper)
        {
            AuraSuper.SetActive(true);
        }
    }

    //esta funcion la llama player al hacer click
    public void atacar()
    {
        if (this.gameObject.GetComponent<MovimientoJugador>().isGrounded)
        {
            StartCoroutine("ComboTerrestre");
        }
        else
        {
            StartCoroutine("ComboAereo");
        }
    }

  
    IEnumerator SuperAtaque()
    {
        if (this.GetComponent<MovimientoJugador>().currentArma == null)
        {
            this.GetComponentInParent<AudioSource>().PlayOneShot(this.GetComponentInParent<MovimientoJugador>().audios[4]);
            FocusedPosition = this.GetComponent<MovimientoJugador>().enemigoFijado.transform.position;
            hitboxFinisher.SetActive(true);
            this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.YEET);
            this.GetComponent<Rigidbody>().AddForce(new Vector3(0, 500, 0));
            yield return new WaitForSeconds(1f);
            this.GetComponent<Rigidbody>().AddForce((FocusedPosition -this.transform.position) *(200), ForceMode.VelocityChange);
            yield return new WaitForSeconds(0.2f);
            hitboxFinisher.SetActive(false);
            explosion.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.Caminar);
            explosion.SetActive(false);
            super = 0;
            AuraSuper.SetActive(false);
            this.GetComponent<MovimientoJugador>().actualizarUI.Raise();
        }
        // rigid.AddForce((hook.transform.position - transform.position).normalized * (pullSpeed), ForceMode.VelocityChange);
    }
    IEnumerator ComboTerrestre()
    {
        //print(tooSoon);
        if (!tooSoon)
        {
            StopCoroutine("Reset");
            tooSoon = true;
            switch (numCombo)
            {
                case 0:
                    numCombo++;
                    //   m_animator.Play("hit1Tierra", -1, 0f);
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    animator.Play("Attack01");
                    yield return new WaitForSeconds(0.25f);
                    hitbox1.SetActive(true);
                    yield return new WaitForSeconds(0.25f);
                    hitbox1.SetActive(false);                   
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    break;
                case 1:
                    numCombo++;
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    hitbox2.SetActive(true);
                    animator.Play("ShieldAttack");
                    yield return new WaitForSeconds(0.5f);
                    hitbox2.SetActive(false);
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    break;

                case 2:
                    numCombo = 0;
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    animator.Play("Jump Attack");
                    yield return new WaitForSeconds(0.25f);
                    hitbox3.SetActive(true);
                    yield return new WaitForSeconds(0.25f);
                    hitbox3.SetActive(false);
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    break;
            }
            yield return new WaitForSeconds(0.1f); // TIEMPO ASTA PODER ASESTAR EL SIGUIENTE GOLPE
            tooSoon = false;

            StartCoroutine("Reset");
        }
        /*else
        {
            Debug.Log("TO SOON!!!");
        }*/       
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(3f);
        numCombo = 0;
    }

    IEnumerator ComboAereo()
    {
        //print(tooSoon);
        if (!tooSoon)
        {
            StopCoroutine("ResetAereo");
            tooSoon = true;
            yield return new WaitForSeconds(0.2f);
            switch (comboAereo)
            {
                case 0:
                    //   m_animator.Play("hit1Tierra", -1, 0f);
                    comboAereo++;
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    hitbox1.SetActive(true);
                    animator.Play("Attack02");
                    yield return new WaitForSeconds(0.5f);
                    this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.Caminar);
                    hitbox1.SetActive(false);
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    break;
                case 1:
                    comboAereo++;
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    hitboxAereo1.SetActive(true);
                    this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.YEET);
                    this.GetComponent<Rigidbody>().AddForce((this.transform.forward) * 10, ForceMode.Impulse);
                    animator.Play("Swimming");
                    yield return new WaitForSeconds(0.5f);
                    this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.Caminar);
                    hitboxAereo1.SetActive(false);                    
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    break;

                case 2:
                    comboAereo = 0;
                    this.GetComponent<MovimientoJugador>().isAttacking = true;
                    if (!this.GetComponent<MovimientoJugador>().isGrounded)
                    {
                        animator.Play("FrontFlip2");
                        hitboxAereo2.SetActive(true);
                        this.GetComponent<Rigidbody>().AddForce(new Vector3(0, -3000, 0));
                        yield return new WaitForSeconds(1f);
                        hitboxAereo2.SetActive(false);
                    }
                    this.GetComponent<MovimientoJugador>().isAttacking = false;
                    this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.Land);
                    //Debug.Log("YA NO ATACO");
                    break;
            }
            yield return new WaitForSeconds(1f);
        }
        yield return new WaitForSeconds(0.1f); // TIEMPO ASTA PODER ASESTAR EL SIGUIENTE GOLPE
        tooSoon = false;

        StartCoroutine("ResetAereo");
    }

    IEnumerator ResetAereo()
    {
        yield return new WaitForSeconds(3f);
        comboAereo = 0;
    }
    private void OnDestroy()
    {
        inputs.player.attack.performed -= MouseButton0Down;
    }
}
