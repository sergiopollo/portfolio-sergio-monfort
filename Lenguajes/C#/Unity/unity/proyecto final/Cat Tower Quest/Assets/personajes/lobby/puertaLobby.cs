using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puertaLobby : MonoBehaviour
{
    public GameObject puertaD;
    public GameObject puertaI;

    public float rotacionInicialD;

    private IEnumerator corrutinaRotarActual;

    // Start is called before the first frame update
    void Start()
    {
        rotacionInicialD = puertaD.transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (corrutinaRotarActual != null)
            {
                StopCoroutine(corrutinaRotarActual);
            }
            corrutinaRotarActual = rotar(true);

            StartCoroutine(corrutinaRotarActual);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (corrutinaRotarActual != null)
            {
                StopCoroutine(corrutinaRotarActual);
            }
            corrutinaRotarActual = rotar(false);

            StartCoroutine(corrutinaRotarActual);
        }
    }

    IEnumerator rotar(bool abrir)
    {
        if (abrir)
        {
            for (float i = puertaD.transform.eulerAngles.y; i >= -90; i--)
            {

                puertaD.transform.eulerAngles = new Vector3(puertaD.transform.eulerAngles.x, i, puertaD.transform.eulerAngles.z);
                puertaI.transform.eulerAngles = new Vector3(puertaD.transform.eulerAngles.x, -i, puertaD.transform.eulerAngles.z);
                yield return new WaitForSeconds(0.05f);

                if (puertaD.transform.eulerAngles.y == 270)
                {
                    i = -90;
                }
            }
        }
        else
        {
            for (float i = puertaD.transform.eulerAngles.y; i <= 360; i++)
            {
                puertaD.transform.eulerAngles = new Vector3(puertaD.transform.eulerAngles.x, i, puertaD.transform.eulerAngles.z);
                puertaI.transform.eulerAngles = new Vector3(puertaD.transform.eulerAngles.x, -i, puertaD.transform.eulerAngles.z);
                yield return new WaitForSeconds(0.05f);
            }
            //puertaD.transform.eulerAngles = new Vector3();
        }
    }

}
