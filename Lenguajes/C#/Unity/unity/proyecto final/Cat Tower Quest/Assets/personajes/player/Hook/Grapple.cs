using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grapple : MonoBehaviour
{
    [SerializeField] float pullSpeed = 0.5f;
    [SerializeField] float stopDistance = 4f;
    [SerializeField] GameObject hookPrefab;
    [SerializeField] Transform shootTransform;
    [SerializeField] Transform pj;
    [SerializeField] Material materialHookQ;

    bool voy;
    bool ven;
    Hook hook;
    bool pulling;
    Rigidbody rigid;
    Vector3 distance;
    bool acelerar;

    // Start is called before the first frame update
    void Start()
    {
        acelerar = false;
        voy = false;
        ven = false;
        rigid = GetComponent<Rigidbody>();
        pulling = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DestroyHookCheck()
    {
        if (hook != null)
        {
            DestroyHook();
        }
    }

    public void HookIr()
    {
        if (hook == null)
        {
            ven = false;
            voy = true;
            StopAllCoroutines();
            pulling = false;
            hook = Instantiate(hookPrefab, pj.transform.position, Quaternion.identity).GetComponent<Hook>();
            hook.Initialize(this, shootTransform);
            StartCoroutine(DestroyHookAfterLifetime());
        }
        if (!pulling || hook == null) return;

        if (Vector3.Distance(transform.position, hook.transform.position) <= stopDistance)
        {
            DestroyHook();
        }
        else if(voy)
        {
            
            this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.YEET);
            rigid.AddForce((hook.transform.position - transform.position).normalized * (pullSpeed), ForceMode.VelocityChange);
        }
      
    }

    public void HookAtraer()
    {
        if (hook == null)
        {
            voy = false;
            ven = true;
            StopAllCoroutines();
            pulling = false;
            hook = Instantiate(hookPrefab, pj.transform.position, Quaternion.identity).GetComponent<Hook>();
            hook.Initialize(this, shootTransform);
            hook.GetComponent<LineRenderer>().material = materialHookQ;
            StartCoroutine(DestroyHookAfterLifetime());



        }

        if (!pulling || hook == null) return;

        if (Vector3.Distance(transform.position, hook.transform.position) <= stopDistance)
        {
            DestroyHook();
        }
        else if(ven)
        {
           
            hook.rigidHit.AddForce((transform.position - hook.rigidHit.gameObject.transform.position).normalized * (pullSpeed ), ForceMode.VelocityChange);
            hook.rigidHit.constraints = RigidbodyConstraints.FreezeRotation;
            if (Vector3.Distance(hook.rigidHit.transform.position, this.transform.position) <= 5)
            {
                hook.rigidHit.constraints = RigidbodyConstraints.FreezeAll;
                Destroy(hook.gameObject);
                Debug.Log(hook.rigidHit.transform.position.x - this.transform.position.x);
            }
            //rigid.AddForce((hook.transform.position - transform.position).normalized * (pullSpeed), ForceMode.VelocityChange);
        }
    }
    public void StartPull()
    {
        pulling = true;
    }

    private void DestroyHook()
    {
        
        if (hook == null) return;
        this.GetComponent<MovimientoJugador>().CambiarEstado(MovimientoJugador.States.Caminar);
        pulling = false;
        Destroy(hook.gameObject);
        hook = null;
    }

    private IEnumerator DestroyHookAfterLifetime()
    {
        yield return new WaitForSeconds(8f);

        DestroyHook();
    }
}
