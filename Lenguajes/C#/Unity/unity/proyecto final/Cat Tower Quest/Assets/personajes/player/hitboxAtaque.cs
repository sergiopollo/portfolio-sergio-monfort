using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitboxAtaque : MonoBehaviour
{
    public MovimientoJugador player;
    public AudioClip clip;
    public bool isProjectile;
    public float dmgMod;
    // Start is called before the first frame update
    void Start()
    {
        if (isProjectile)
        {
            StartCoroutine(disappear(5f));
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //if (player.isAttacking && other.tag == "Enemigo")
        if (other.tag == "Enemigo")
        {
                if (isProjectile)
                {
                    other.gameObject.GetComponent<IABASE>().GetHurt((int)(player.damage * dmgMod), 13);
                    StartCoroutine(disappear(0.1f));
                }
                else
                {
                    other.gameObject.GetComponent<IABASE>().GetHurt((int)(player.damage * dmgMod), 13);
                }

                player.gameObject.GetComponent<combos>().giveSuper(1);
                this.player.actualizarUI.Raise();
                AudioSource.PlayClipAtPoint(clip, other.transform.position);            
        }else if(other.tag == "Destruible")
        {
            other.gameObject.GetComponent<boxDestruible>().hp--;
        }
        
    }

    IEnumerator disappear(float time)
    {
        yield return new WaitForSeconds(time);
        this.gameObject.SetActive(false);
    }
}
