using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class tienda : MonoBehaviour
{
    //new input system
    InputPlayer inputs;

    public GameObject tiendaCanvas;
    public MovimientoJugador player;
    public GameObject canvasPlayer;
    public bool isTouchingPlayer;

    public bool isTiendaOpen;
    public bool hasSelectedAbility;

    public PlayerInventory playerInventory;
    public GameObject moneyCanvas;

    public GameEvent actualizarUI;

    public GameObject descripcion;

    public int index;
    public GameObject selector;

    //lista seleccion de mejoras de habilidad
    public List<GameObject> seleccionHabilidad;
    public List<mejora> habilidades;
    public habilidadesTienda habilidadSeleccionada;


    //listas para las mejoras de cada habilidad
    public List<GameObject> seleccionMejora;
    public List<mejora> mejoras;

    //haria una lista de listas, pero no puedo tocarla des del editor para ponerle los valores, asi que he tenido que hacer una lista para las mejoras de cada habilidad (que asco da resetearlas) 
    public List<mejora> mejorasMaxHp;

    public List<mejora> mejorasDamage;

    public List<mejora> mejorasSpeed;

    public List<mejora> mejorasDash;

    public List<mejora> mejorasDoubleJump;

    public List<mejora> mejorasWallClimb;

    public List<mejora> mejorasHook;

    public List<mejora> mejorasMoney;

    public List<AudioClip> audios;
    private void Awake()
    {
        inputs = new InputPlayer();
        inputs.Enable();
        ponerInputs();

    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<MovimientoJugador>();
        playerInventory = player.playerInventory;
        canvasPlayer = GameObject.Find("Canvas");
        //tiendaCanvas = GameObject.Find("tiendaCanvas");
        selector = tiendaCanvas.transform.GetChild(0).gameObject;
        moneyCanvas = tiendaCanvas.transform.GetChild(1).gameObject;
        descripcion = tiendaCanvas.transform.GetChild(3).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        //Controles();
        //Debug.Log(isTouchingPlayer);
    }

    private void ponerInputs()
    {
        inputs.player.menuCancel.performed += EscDown;
        inputs.player.abrirYCerrarTienda.performed += TDown;
        inputs.player.menuMoveLeft.started += LeftDown;
        inputs.player.menuMoveRight.started += RightDown;
        inputs.player.seleccionarMenu.performed += SelectDown;
    }

   

    public void InicializarTienda()
    {
        this.GetComponent<AudioSource>().volume = 1f;
        this.GetComponent<AudioSource>().pitch = 1f;
        this.GetComponent<AudioSource>().PlayOneShot(audios[0]);
        isTiendaOpen = true;
        canvasPlayer.SetActive(false);
        player.Pause = true;

        tiendaCanvas.SetActive(true);

        hasSelectedAbility = false;

        //pongo las habilidades en el menu de seleccion
        for (int i = 0; i < seleccionHabilidad.Count; i++)
        {
            seleccionHabilidad[i].SetActive(true);

            if (seleccionHabilidad[i].name != "Exit")
            {
                //pongo su imagen
                seleccionHabilidad[i].transform.GetChild(0).GetComponent<Image>().sprite = habilidades[i].image;
                //quito precio
                seleccionHabilidad[i].transform.GetChild(1).transform.gameObject.SetActive(false);
                //lo bloqueo o desbloqueo segun si el jugador tiene la habilidad o no
                habilidades[i].estado = mejora.state.comprable;
                switch (habilidades[i].habilidad)
                {
                    case habilidadesTienda.DoubleJump:
                        if (!playerInventory.canDoubleJump)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.Dash:
                        if (!playerInventory.canDash)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.WallClimb:
                        if (!playerInventory.canWallClimb)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.Hook:
                        if (!playerInventory.canHookIr)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                }
            }
        }


        index = 0;
        actualizarTienda();
        

        moverSelector();
        OnMoneyChanged();

    }

    public void cerrarTienda()
    {
        isTiendaOpen = false;
        hasSelectedAbility = false;
        tiendaCanvas.SetActive(false);
        player.Pause = false;
        canvasPlayer.SetActive(true);
    }

    //new input system
    private void EscDown(InputAction.CallbackContext context)
    {
        if (isTiendaOpen)
        {
            cerrarTienda();
        }
    }

    private void TDown(InputAction.CallbackContext context)
    {
        if (isTiendaOpen)
        {
            cerrarTienda();
        }
        else if (isTouchingPlayer && !isTiendaOpen && !player.Pause)
        {
            InicializarTienda();
        }
    }

    private void SelectDown(InputAction.CallbackContext context)
    {
        if (isTiendaOpen)
        {
            seleccionar();
        }
    }

    private void LeftDown(InputAction.CallbackContext context)
    {
        if (isTiendaOpen)
        {
            //moverse
            if (hasSelectedAbility)
            {
                
                    if (index == 0)
                    {
                        index = seleccionMejora.Count - 1;
                    }
                    else
                    {
                        index--;
                    }
                    moverSelector();
                
            }
            else
            {
                
                
                    if (index == 0)
                    {
                        index = seleccionHabilidad.Count - 1;
                    }
                    else
                    {
                        index--;
                    }
                    moverSelector();
                
            }
        }
    }

    private void RightDown(InputAction.CallbackContext context)
    {
        
        if (isTiendaOpen)
        {
            //moverse
            if (hasSelectedAbility)
            {
                
                    if (index == seleccionMejora.Count - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    moverSelector();
                
            }
            else
            {
                
                    if (index == seleccionHabilidad.Count - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    moverSelector();
                
            }

        }
    }

    public void inicializarSeccionMejoras()
    {
        hasSelectedAbility = true;
        index = 0;

        //desactiva los gameobjects de selector de mejoras (por si sobran) (lo hace en la lista de seleccionHabilidad porque esa nunca cambia y tiene todos los gameobjects)
        for (int i = 0; i< seleccionHabilidad.Count; i++)
        {
            seleccionHabilidad[i].SetActive(false);
        }

        //pone en la lista de mejoras, las mejoras de la habilidad en cuestion
        seleccionMejora.Clear();

        switch (habilidadSeleccionada)
        {
            case habilidadesTienda.MaxHp:
                mejoras = mejorasMaxHp;
                break;
            case habilidadesTienda.Damage:
                mejoras = mejorasDamage;
                break;
            case habilidadesTienda.Speed:
                mejoras = mejorasSpeed;
                break;
            case habilidadesTienda.DoubleJump:
                mejoras = mejorasDoubleJump;
                break;
            case habilidadesTienda.Dash:
                mejoras = mejorasDash;
                break;
            case habilidadesTienda.WallClimb:
                mejoras = mejorasWallClimb;
                break;
            case habilidadesTienda.Hook:
                mejoras = mejorasHook;
                break;
            case habilidadesTienda.Money:
                mejoras = mejorasMoney;
                break;
        }

        //le pongo los gameobjects selectores necesarios
        for (int i = 0; i < mejoras.Count; i++)
        {
            seleccionMejora.Add(seleccionHabilidad[i]);
        }
        //pongo el boton de exit
        seleccionMejora.Add(seleccionHabilidad[habilidades.Count]);
    
        //reactiva los gameobjects necesarios
        for (int i = 0; i < seleccionMejora.Count; i++)
        {
            seleccionMejora[i].SetActive(true);
        }

        //pongo los valores de las mejoras en la UI
        for (int i = 0; i < mejoras.Count; i++)
        {
            //pongo su imagen
            seleccionMejora[i].transform.GetChild(0).GetComponent<Image>().sprite = mejoras[i].image;
            //pongo su precio
            seleccionMejora[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = mejoras[i].precio + "$";
        }

        actualizarTienda();
        moverSelector();

    }
    /*
    public void Controles()
    {
        if (isTouchingPlayer)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                if (isTiendaOpen)
                {
                    cerrarTienda();
                }
                else if (!isTiendaOpen && !player.Pause)
                {
                    InicializarTienda();
                }
            }
        }


        if (isTiendaOpen)
        {
            //moverse
            if (hasSelectedAbility)
            {
                if (Input.GetKeyDown(KeyCode.D))
                {
                    if (index == seleccionMejora.Count - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    moverSelector();
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    if (index == 0)
                    {
                        index = seleccionMejora.Count - 1;
                    }
                    else
                    {
                        index--;
                    }
                    moverSelector();
                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.D))
                {
                    if (index == seleccionHabilidad.Count - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    moverSelector();
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    if (index == 0)
                    {
                        index = seleccionHabilidad.Count - 1;
                    }
                    else
                    {
                        index--;
                    }
                    moverSelector();
                }
            }
            

            //seleccionar
            if (Input.GetKeyDown(KeyCode.E))
            {
                seleccionar();
            }

        }

        
    }
    */
    public void seleccionar()
    {
        if (hasSelectedAbility)
        {
            //boton de salir
            if (seleccionMejora[index].name == "Exit")
            {
                //vuelve a la seleccion de habilidad
                index = 0;
                hasSelectedAbility = false;
                InicializarTienda();
            }
            else
            {
                switch (mejoras[index].estado)
                {
                    case mejora.state.bloqueado:
                        this.GetComponent<AudioSource>().Stop();
                        this.GetComponent<AudioSource>().volume = 0.7f;
                        this.GetComponent<AudioSource>().pitch = 1f;
                        this.GetComponent<AudioSource>().PlayOneShot(audios[3]);
                        Debug.Log("mejora bloqueada");
                        descripcion.GetComponent<TextMeshProUGUI>().text = "mejora bloqueada";
                        break;
                    case mejora.state.comprable:
                        if (playerInventory.money >= mejoras[index].precio)
                        {
                            this.GetComponent<AudioSource>().Stop();
                            this.GetComponent<AudioSource>().volume = 0.7f;
                            this.GetComponent<AudioSource>().pitch = 1f;
                            this.GetComponent<AudioSource>().PlayOneShot(audios[1]);
                           
                            comprar();
                        }
                        else
                        {
                            Debug.Log("no tienes dinero suficiente");
                            this.GetComponent<AudioSource>().Stop();
                            this.GetComponent<AudioSource>().volume = 0.7f;
                            this.GetComponent<AudioSource>().pitch = 1f;
                            this.GetComponent<AudioSource>().PlayOneShot(audios[2]);
                            descripcion.GetComponent<TextMeshProUGUI>().text = "no tienes dinero suficiente";
                        }
                        break;
                    case mejora.state.comprado:
                        Debug.Log("ya esta comprado");
                        this.GetComponent<AudioSource>().Stop();
                        this.GetComponent<AudioSource>().volume = 0.7f;
                        this.GetComponent<AudioSource>().pitch = 0.7f;
                        this.GetComponent<AudioSource>().PlayOneShot(audios[3]);
                        descripcion.GetComponent<TextMeshProUGUI>().text = "ya esta comprado";
                        break;
                }
            }

        }
        else
        {
            //boton de salir
            if (seleccionHabilidad[index].name == "Exit")
            {
                //sale de la tienda
                cerrarTienda();
            }
            else
            {
                switch (habilidades[index].estado)
                {
                    case mejora.state.bloqueado:
                        Debug.Log("no tienes esta habilidad");
                        descripcion.GetComponent<TextMeshProUGUI>().text = "no tienes esta habilidad";
                        break;
                    //las habilidades que tienes desbloqueadas estan siempre como "comprables", es para no tener que hacer mejora otra vez pero cambiado un poco
                    case mejora.state.comprable:
                        Debug.Log("entrando en las mejoras de la habilidad");
                        habilidadSeleccionada = habilidades[index].habilidad;
                        inicializarSeccionMejoras();
                        break;
                }
            }
        }

    }

    public void moverSelector()
    {
        if (hasSelectedAbility)
        {
            if (seleccionMejora[index].name == "Exit")
            {
                selector.transform.position = seleccionMejora[index].transform.position;
                descripcion.GetComponent<TextMeshProUGUI>().text = "volver a la seleccion de habilidades";
            }
            else
            {
                selector.transform.position = seleccionMejora[index].transform.GetChild(0).transform.position;
                //descripcion
                descripcion.GetComponent<TextMeshProUGUI>().text = mejoras[index].descripcion;
            }
        }
        else
        {
            if (seleccionHabilidad[index].name == "Exit")
            {
                selector.transform.position = seleccionHabilidad[index].transform.position;
                descripcion.GetComponent<TextMeshProUGUI>().text = "salir de la tienda";
            }
            else
            {
                selector.transform.position = seleccionHabilidad[index].transform.GetChild(0).transform.position;
                //descripcion
                descripcion.GetComponent<TextMeshProUGUI>().text = habilidades[index].descripcion;
            }
        }
        
    }

    public void OnMoneyChanged()
    {
        this.moneyCanvas.GetComponent<TextMeshProUGUI>().text = playerInventory.money+"$";
    }

    public void actualizarTienda()
    {
        if (hasSelectedAbility)
        {
            //actualizacion interna
            for (int i = 0; i < mejoras.Count; i++)
            {
                if (i > 0 && mejoras[i - 1].estado == mejora.state.comprado && mejoras[i].estado == mejora.state.bloqueado)
                {
                    mejoras[i].estado = mejora.state.comprable;
                }
            }

            //actualizacion de UI
            for (int i = 0; i < mejoras.Count; i++)
            {
                switch (mejoras[i].estado)
                {
                    case mejora.state.bloqueado:
                        //cambio el color de la imagen
                        seleccionMejora[i].transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 1);
                        //activo el precio
                        seleccionMejora[i].transform.GetChild(1).transform.gameObject.SetActive(true);
                        break;
                    case mejora.state.comprable:
                        //cambio el color de la imagen
                        seleccionMejora[i].transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        //activo el precio
                        seleccionMejora[i].transform.GetChild(1).transform.gameObject.SetActive(true);
                        break;
                    case mejora.state.comprado:
                        //cambio el color de la imagen
                        seleccionMejora[i].transform.GetChild(0).GetComponent<Image>().color = new Color(1, 0.92f, 0.016f, 1);
                        //desactivo el precio(ya que esta comprado)
                        seleccionMejora[i].transform.GetChild(1).transform.gameObject.SetActive(false);
                        break;
                }
            }
            //descripcion
            descripcion.GetComponent<TextMeshProUGUI>().text = mejoras[index].descripcion;
        }
        else
        {
            //actualizacion interna
            for (int i = 0; i < habilidades.Count; i++)
            {
                habilidades[i].estado = mejora.state.comprable;
                switch (habilidades[i].habilidad)
                {
                    case habilidadesTienda.DoubleJump:
                        if (!playerInventory.canDoubleJump)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.Dash:
                        if (!playerInventory.canDash)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.WallClimb:
                        if (!playerInventory.canWallClimb)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                    case habilidadesTienda.Hook:
                        if (!playerInventory.canHookIr)
                        {
                            habilidades[i].estado = mejora.state.bloqueado;
                        }
                        break;
                }
            }

            //actualizacion de UI
            for (int i = 0; i < habilidades.Count; i++)
            {
                switch (habilidades[i].estado)
                {
                    case mejora.state.bloqueado:
                        //cambio el color de la imagen
                        seleccionHabilidad[i].transform.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 1);
                        break;
                    case mejora.state.comprable:
                        //cambio el color de la imagen
                        seleccionHabilidad[i].transform.GetChild(0).GetComponent<Image>().color = new Color(1, 1, 1, 1);
                        break;
                }
            }
            //descripcion
            descripcion.GetComponent<TextMeshProUGUI>().text = habilidades[index].descripcion;
        }

        canvasPlayer.SetActive(true);
        actualizarUI.Raise();
        canvasPlayer.SetActive(false);
    }

    public void comprar()
    {
        playerInventory.money -= mejoras[index].precio;

        switch (mejoras[index].habilidad)
        {
            case habilidadesTienda.MaxHp:
                playerInventory.hpMax += (int)mejoras[index].upgrade;
                player.actualizarInventory();
                player.getHealed(9999);
                //player.getHealed(9999);
                break;
            case habilidadesTienda.Damage:
                playerInventory.damage += mejoras[index].upgrade;
                break;
            case habilidadesTienda.Speed:
                playerInventory.velocidad += mejoras[index].upgrade;
                break;
            case habilidadesTienda.jumpForce:
                playerInventory.jumpForce += mejoras[index].upgrade;
                break;
            case habilidadesTienda.numDoubleJumps:
                playerInventory.numDoubleJumps += (int)mejoras[index].upgrade;
                break;
            case habilidadesTienda.numDashes:
                playerInventory.numDashes += (int)mejoras[index].upgrade;
                break;
            case habilidadesTienda.dashForce:
                playerInventory.dashForce += mejoras[index].upgrade;
                break;
            case habilidadesTienda.wallClimbSpeed:
                playerInventory.wallClimbSpeed += mejoras[index].upgrade;
                break;
            case habilidadesTienda.canHookAtraer:
                playerInventory.canHookAtraer = true;
                break;
            case habilidadesTienda.canCoinMagnet:
                playerInventory.canCoinMagnet = true;
                break;
            case habilidadesTienda.canDamageDash:
                playerInventory.canDamageDash = true;
                break;
        }
        player.inicializarInventory();
        mejoras[index].estado = mejora.state.comprado;
        actualizarTienda();
    }

    public void resetTienda()
    {
        //resetea todas las mejoras (la primera de la lista en comprable y el resto en bloqueada)
        //actualizacion interna

        /*
        for (int i = 0; i < mejoras.Count; i++)
        {
            if (i == 0)
            {
                mejoras[i].estado = mejora.state.comprable;
            }
            else
            {
                mejoras[i].estado = mejora.state.bloqueado;
            }
            //activo el precio
            seleccionMejora[i].transform.GetChild(1).transform.gameObject.SetActive(true);
        }
        */

        for (int i = 0; i < mejorasMaxHp.Count; i++)
        {
            if (i == 0)
            {
                mejorasMaxHp[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasMaxHp[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasDamage.Count; i++)
        {
            if (i == 0)
            {
                mejorasDamage[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasDamage[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasSpeed.Count; i++)
        {
            if (i == 0)
            {
                mejorasSpeed[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasSpeed[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasDash.Count; i++)
        {
            if (i == 0)
            {
                mejorasDash[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasDash[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasDoubleJump.Count; i++)
        {
            if (i == 0)
            {
                mejorasDoubleJump[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasDoubleJump[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasWallClimb.Count; i++)
        {
            if (i == 0)
            {
                mejorasWallClimb[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasWallClimb[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasHook.Count; i++)
        {
            if (i == 0)
            {
                mejorasHook[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasHook[i].estado = mejora.state.bloqueado;
            }
        }

        for (int i = 0; i < mejorasMoney.Count; i++)
        {
            if (i == 0)
            {
                mejorasMoney[i].estado = mejora.state.comprable;
            }
            else
            {
                mejorasMoney[i].estado = mejora.state.bloqueado;
            }
        }

        //InicializarTienda();


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            this.isTouchingPlayer = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            this.isTouchingPlayer = false;
        }
    }

    private void OnDestroy()
    {
        inputs.player.menuCancel.performed -= EscDown;
        inputs.player.abrirYCerrarTienda.performed -= TDown;
        inputs.player.menuMoveLeft.started -= LeftDown;
        inputs.player.menuMoveRight.started -= RightDown;
        inputs.player.seleccionarMenu.performed -= SelectDown;
    }

}
