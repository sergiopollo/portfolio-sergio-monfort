using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{

    public float velocidad;
    private float velocidad_real;

    public PhysicMaterial mifriccion;
    public enum States
    {
        Caminar, Correr, Saltar, WallRun, Dash
    }
    private States state;
    public Camera mainCamera;

    //public GameEventState cambioEstado;
    public float jumpForce;
    public float dashForce;
    public float friccion;

    //pruebas luces
    private bool zonaCreepy = false;
    public Vector3 posicionInicial;
    public int segundosDeOscuridad=5;
    private Coroutine creepyCoroutine = null;
    public GameEvent PasandoZonaCreepy;

    // Start is called before the first frame update
    void Start()
    {
        //cambioEstado.Raise(state);
        state = States.Caminar;
        velocidad_real = velocidad;
        this.posicionInicial = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (state != States.WallRun && state != States.Dash)
        {
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.forward.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.forward.z * velocidad_real);
            }
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.forward.x * velocidad_real * -1, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.forward.z * velocidad_real * -1);
            }

            if (Input.GetKey("a"))
            {
                //this.transform.Rotate(new Vector3(0, -1, 0) * 2);
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.right.x * velocidad_real * -1, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.right.z * velocidad_real * -1);
            }
            else if (Input.GetKey("d"))
            {
                //this.transform.Rotate(new Vector3(0, 1, 0) * 2);
                this.GetComponent<Rigidbody>().velocity = new Vector3(mainCamera.transform.right.x * velocidad_real, this.GetComponent<Rigidbody>().velocity.y, mainCamera.transform.right.z * velocidad_real);

            }


            if (Input.GetKeyDown("c"))
            {
                StartCoroutine("Dash");
            }

            if (Input.GetKeyDown(KeyCode.LeftShift) && state != States.Saltar)
            {
                velocidad_real = velocidad * 2;
                Cambiarestado(States.Correr);
            }
            if (Input.GetKeyUp(KeyCode.LeftShift) && state == States.Correr)
            {
                velocidad_real = velocidad;
                Cambiarestado(States.Caminar);
            }


        }

        if (Input.GetKeyDown("space") && state != States.Dash && state != States.Saltar && state != States.WallRun)
        {
            Cambiarestado(States.Saltar);
            this.GetComponent<Rigidbody>().AddForce(this.transform.up * jumpForce, ForceMode.Impulse);
        }


    }

    public void muerte()
    {
        this.transform.position = this.posicionInicial;
    }

    IEnumerator Dash()
    {
        mifriccion.dynamicFriction = 0;
        mifriccion.bounciness = 1;
        Cambiarestado(States.Dash);
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * dashForce, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        mifriccion.dynamicFriction = 5f;
        //this.GetComponent<Rigidbody>().velocity = new Vector3(transform.forward.x *5, this.GetComponent<Rigidbody>().velocity.y, transform.forward.z *5f);
        yield return new WaitForSeconds(0.5f);
        Cambiarestado(States.Caminar);
        mifriccion.dynamicFriction = friccion;
        mifriccion.bounciness = 0;
    }

    IEnumerator WallRun(Vector3 normal)
    {
        Cambiarestado(States.WallRun);
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;

        //Debug.Log(Vector3.Cross(this.transform.forward, normal).y);
        //Debug.Log(Vector3.Angle(this.transform.forward, normal));
        if (Vector3.Cross(this.transform.forward, normal).y > 0)
        {
            //print("CW");
            float iker = Vector3.Angle(this.transform.forward, normal);
            this.transform.Rotate(new Vector3(0, iker - 90, 0));
            //this.transform.GetChild(0).Rot.Rotate(new Vector3(0, -(iker - 90), 0));
            //this.transform.GetChild(0).Rotate(new Vector3(0, -(iker - 90), 0));
        }
        else
        {
            //print("CC");
            float iker = Vector3.Angle(this.transform.forward, normal);

            this.transform.Rotate(new Vector3(0, -iker + 90, 0));
            //this.transform.GetChild(0).Rotate(new Vector3(0, -(-iker + 90), 0));
        }

        // this.transform.Rotate(new Vector3());
        this.GetComponent<Rigidbody>().velocity = this.transform.forward * velocidad_real;

        //this.transform.Rotate(new Vector3(0, Vector3.Angle(this.gameObject.GetComponent<Rigidbody>().velocity, normal) + 90, 0));
        StartCoroutine(EndWallRun(normal));
        yield return new WaitForSeconds(2f);
        Cambiarestado(States.Caminar);
        this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        StopCoroutine("EndWallRun");

    }

    IEnumerator EndWallRun(Vector3 normalWall)
    {
        //Debug.Log("WOOOOOOOOOOOOOOO");
        bool end = false;
        while (!end)
        {
            yield return new WaitForSeconds(0.3f);
            if (Input.GetKey("space"))
            {
                //Debug.Log("WEEEEEEEEEEE");
                end = true;
                StopCoroutine("WallRun");
                Cambiarestado(States.Saltar);
                this.gameObject.GetComponent<Rigidbody>().useGravity = true;
                this.GetComponent<Rigidbody>().AddForce(this.transform.up * 10, ForceMode.Impulse);
                this.GetComponent<Rigidbody>().AddForce(normalWall * 10, ForceMode.Impulse);
            }
        }


    }

    public void Cambiarestado(States s)
    {
        state = s;
        //cambioEstado.Raise(state);
    }


    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Wall" && state == States.Saltar)
        {
            if (Input.GetKey((KeyCode.LeftShift)))
            {
                StartCoroutine(WallRun(collision.contacts[0].normal));
                //Debug.DrawRay(this.transform.position, collision.contacts[0].normal*10,Color.green,10f);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            mifriccion.dynamicFriction = 0;
            mifriccion.staticFriction = 0;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            Cambiarestado(States.Caminar);
            mifriccion.dynamicFriction = friccion;
            mifriccion.staticFriction = friccion;
        }


        if (collision.gameObject.tag == "Lava")
        {
            //Destroy(this.gameObject);
            muerte();
            print("has muerto");
        }

    }

    public void activarZonaCreepy()
    {
        if (!this.zonaCreepy)
        {
            creepyCoroutine = StartCoroutine(segundosDeVidaEnOscuridad());
            this.zonaCreepy = true;
        }
        else
        {
            StopCoroutine(creepyCoroutine);
            this.zonaCreepy = false;
        }
    }

    IEnumerator segundosDeVidaEnOscuridad()
    {
        for (int i= 0; i< segundosDeOscuridad; i++) {
            yield return new WaitForSeconds(1f);
            Debug.Log("te quedan "+(segundosDeOscuridad-i)+" segundos");
        }
        activarZonaCreepy();
        muerte();
    }

    private void OnTriggerEnter(Collider other)
    {
        print("Iker" + other.transform.name + "Iker");
        if (other.gameObject.name == "ConoLuz")
        {
            Debug.Log("i step into the light!!!!");
            StopCoroutine(creepyCoroutine);
        }

        if (other.gameObject.name == "zonaCreepy")
        {
            activarZonaCreepy();
            this.PasandoZonaCreepy.Raise();
        }
    }

    private void OnTriggerExit(Collider other)
    {
       
        if (other.gameObject.name == "ConoLuz")
        {
            Debug.Log("the light is gone.");
            creepyCoroutine = StartCoroutine(segundosDeVidaEnOscuridad());
        }
    }

}
