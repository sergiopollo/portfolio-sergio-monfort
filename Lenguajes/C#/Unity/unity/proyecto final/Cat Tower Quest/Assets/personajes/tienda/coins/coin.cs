using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin : MonoBehaviour
{
    public AudioClip clip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 100 * Time.deltaTime); //rotates 50 degrees per second around z axis
    }

    private void OnEnable()
    {
        StartCoroutine("BYE");
    }

    IEnumerator BYE()
    {
        yield return new WaitForSeconds(30f);
        this.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(clip, other.GetComponent<MovimientoJugador>().vCam.transform.position, 1.0F);
            //sumo 1 de dinero
            other.GetComponent<MovimientoJugador>().getMoney(1);
            this.gameObject.SetActive(false);
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("Obstacle") || other.gameObject.layer == LayerMask.NameToLayer("CameraIgnoreObstacle"))
        {
            AudioSource.PlayClipAtPoint(clip, this.transform.position, 0.2F);
        }
    }


}
