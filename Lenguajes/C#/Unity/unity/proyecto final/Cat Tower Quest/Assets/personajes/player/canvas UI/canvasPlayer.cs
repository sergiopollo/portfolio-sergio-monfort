using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class canvasPlayer : MonoBehaviour
{
    //tienda para que no pueda mover el canvas con la tienda abierta
    public tienda tienda;

    //new input system
    InputPlayer inputs;

    //menu pausa
    private bool pausa;
    public GameObject menuPausa;
    public GameObject selectorPausa;
    public int index;

    //menu muerte
    private bool playerDead;
    public GameObject menuMuerte;
    public GameObject selectorMuerte;

    //loading screen
    private bool loading;
    public GameObject loadingScreen;

    //player
    public MovimientoJugador player;
    public PlayerInventory playerInventory;

    //elementos UI
    public Slider barraVida;
    public TextMeshProUGUI moneyCanvas;
    public Slider barraUlti;
    public Image weapon;
    public Sprite imagenEspada;

    //barra boss
    public bool isBossScene;
    public Slider barraVidaBoss;
    public Sindulf_IA boss;

    private void Awake()
    {
        Debug.Log("printear");
        inputs = new InputPlayer();
        inputs.Enable();
        ponerInputs();

        player = GameObject.Find("Player").GetComponent<MovimientoJugador>();
        playerInventory = player.playerInventory;
        if (SceneManager.GetActiveScene().name == "Lobby")
        {
            tienda = GameObject.Find("tienda").GetComponent<tienda>();
        }
        Debug.Log(SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name == "Boss")
        {
            Debug.Log("entro");
            isBossScene = true;
            barraVidaBoss.gameObject.SetActive(true);
            boss = GameObject.Find("Sindulf-0 The Father of the Swarm").GetComponent<Sindulf_IA>();
            StartCoroutine(vidaBoss());
        }
        actualizarUI();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (!loading)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (pausa)
                {
                    closePauseMenu();
                }
                else
                {
                    index = 0;
                    moverSelector();
                    pausa = true;
                    menuPausa.SetActive(true);
                    player.Pause = true;
                }
            }

            if (pausa)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    seleccionar();
                }

                if (Input.GetKeyDown(KeyCode.D))
                {
                    if (index == menuPausa.transform.GetChild(1).transform.childCount - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    moverSelector();
                }
                else if (Input.GetKeyDown(KeyCode.A))
                {
                    if (index == 0)
                    {
                        index = menuPausa.transform.GetChild(1).transform.childCount - 1;
                    }
                    else
                    {
                        index--;
                    }
                    moverSelector();
                }
            }
        }*/
        
    }

    IEnumerator vidaBoss()
    {
        yield return new WaitForSeconds(0.1f);
        barraVidaBoss.maxValue = boss.vida;
        barraVidaBoss.value = boss.vida;
    }
    private void ponerInputs()
    {
        inputs.player.menuCancel.performed += cancelDown;
        inputs.player.openPauseMenu.performed += PauseDown;
        inputs.player.menuMoveLeft.started += LeftDown;
        inputs.player.menuMoveRight.started += RightDown;
        inputs.player.seleccionarMenu.performed += SelectDown;
    }


    //new input system
    private void cancelDown(InputAction.CallbackContext context)
    {
        if (tienda)
        {
            if (!tienda.isTiendaOpen && !loading)
            {
                if (pausa)
                {
                    closePauseMenu();
                }
            }
        }
        else
        {
            if (!loading)
            {
                if (pausa)
                {
                    closePauseMenu();
                }
            }
        }
        
    }

    private void PauseDown(InputAction.CallbackContext context)
    {       
        if (tienda)
        {
            if (!tienda.isTiendaOpen && !loading)
            {
                if (pausa)
                {
                    closePauseMenu();
                }
                else if (!playerDead)
                {
                    index = 0;
                    moverSelector();
                    pausa = true;
                    menuPausa.SetActive(true);
                    player.Pause = true;
                    Time.timeScale = 0;
                }
            }
        }
        else
        {
            if (!loading)
            {
                if (pausa)
                {
                    closePauseMenu();
                }
                else if (!playerDead)
                {
                    index = 0;
                    moverSelector();
                    pausa = true;
                    menuPausa.SetActive(true);
                    player.Pause = true;
                    Time.timeScale = 0;
                }
            }
        }

    }

    private void SelectDown(InputAction.CallbackContext context)
    {
        if ((!loading && pausa) || playerDead)
        {
            seleccionar();
        }
    }

    private void LeftDown(InputAction.CallbackContext context)
    {
        if (!loading && pausa)
        {
            if (index == 0)
            {
                index = menuPausa.transform.GetChild(1).transform.childCount - 1;
            }
            else
            {
                index--;
            }
            moverSelector();
        }
        else if (playerDead)
        {
            if (index == 0)
            {
                index = menuMuerte.transform.GetChild(1).transform.childCount - 1;
            }
            else
            {
                index--;
            }
            moverSelector();
        }
    }

    private void RightDown(InputAction.CallbackContext context)
    {
        if (!loading && pausa)
        {

            if (index == menuPausa.transform.GetChild(1).transform.childCount - 1)
            {
                index = 0;
            }
            else
            {
                index++;
            }
            moverSelector();
        }
        else if (playerDead)
        {
            if (index == menuMuerte.transform.GetChild(1).transform.childCount - 1)
            {
                index = 0;
            }
            else
            {
                index++;
            }
            moverSelector();
        }
    }

    //esto salta cuando el player muere
    public void onPlayerDeath()
    {
        //le quito las armas al jugador
        playerInventory.misArmas.Clear();
        closePauseMenu();
        playerDead = true;
        player.Pause = true;
        index = 0;
        menuMuerte.SetActive(true);
    }

    public void closePauseMenu()
    {
        Time.timeScale = 1;
        pausa = false;
        menuPausa.SetActive(false);
        player.Pause = false;
    }

    public void moverSelector()
    {
        if (pausa)
        {
            selectorPausa.transform.position = menuPausa.transform.GetChild(1).transform.GetChild(index).transform.position;
        }
        else if(playerDead)
        {
            selectorMuerte.transform.position = menuMuerte.transform.GetChild(1).transform.GetChild(index).transform.position;
        }
    }

    public void seleccionar()
    {
        if (pausa)
        {
            menuPausa.transform.GetChild(1).transform.GetChild(index).GetComponent<Button>().onClick.Invoke();
        }
        else if (playerDead)
        {
            player.getHealed(9999);
            menuMuerte.transform.GetChild(1).transform.GetChild(index).GetComponent<Button>().onClick.Invoke();
        }
    }

    public void LoadingScreen()
    {
        if (loading)
        {
            loading = false;
            loadingScreen.SetActive(false);
        }
        else
        {
            loading = true;
            loadingScreen.SetActive(true);
        }
    }

    public void actualizarUI()
    {
        //barra de vida
        //slider
        barraVida.maxValue = player.hpMax;
        barraVida.value = player.hp;
        //valor numerico
        barraVida.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text= player.hp +"/"+ player.hpMax;

        //dinero
        this.moneyCanvas.text = playerInventory.money + "$";

        //barra ulti
        barraUlti.maxValue = player.GetComponent<combos>().maxSuper;
        barraUlti.value = player.GetComponent<combos>().super;
        if (player.GetComponent<combos>().super == player.GetComponent<combos>().maxSuper)
        {
            barraUlti.transform.GetChild(0).transform.gameObject.SetActive(true);
        }
        else
        {
            barraUlti.transform.GetChild(0).transform.gameObject.SetActive(false);
        }

        //imagen arma
        if (player.currentArma==null)
        {
            weapon.sprite = imagenEspada;
            //municion arma
            weapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "";
        }
        else
        {
            weapon.sprite = player.currentArma.imagen;
            //municion arma
            weapon.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.currentArma.ammo + "/" + player.currentArma.maxAmmo;
        }

        //barra vida boss
        if (isBossScene)
        {
            barraVidaBoss.value = boss.vida;
        }
        
    }

    public void Exit()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }

    public void SaveGame()
    {
        Time.timeScale = 1;
        this.GetComponent<SaveBueno>().guardarPartida();
        StartCoroutine(cargaFake());
    }

    IEnumerator cargaFake()
    {
        LoadingScreen();
        yield return new WaitForSeconds(1f);
        LoadingScreen();
        Time.timeScale = 0;
    }

    public void goToLobby()
    {
        SceneManager.LoadScene("Lobby");
    }

    private void OnDestroy()
    {
        inputs.player.menuCancel.performed -= cancelDown;
        inputs.player.openPauseMenu.performed -= PauseDown;
        inputs.player.menuMoveLeft.started -= LeftDown;
        inputs.player.menuMoveRight.started -= RightDown;
        inputs.player.seleccionarMenu.performed -= SelectDown;
    }

}
