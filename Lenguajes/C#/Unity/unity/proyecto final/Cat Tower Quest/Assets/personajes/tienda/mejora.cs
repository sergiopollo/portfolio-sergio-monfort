using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/mejora", order = 1)]
[System.Serializable]
public class mejora : ScriptableObject
{
    public habilidadesTienda habilidad;
    public Sprite image;
    public int precio;
    public enum state
    {
        bloqueado, comprable, comprado
    }
    public state estado;
    public float upgrade;
    public string descripcion;
}
