using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitboxVision : MonoBehaviour
{
    public MovimientoJugador player;
    public GameEvent enemyFound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //cosas vision
        if (other.gameObject.tag=="Enemigo")
        {
            player.enemigoFijado = other.gameObject;
            enemyFound.Raise();
            this.gameObject.SetActive(false);
        }
    }
}
