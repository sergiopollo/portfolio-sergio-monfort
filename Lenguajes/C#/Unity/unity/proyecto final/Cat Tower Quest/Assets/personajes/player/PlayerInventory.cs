using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PlayerInventory", order = 1)]
public class PlayerInventory : ScriptableObject
{
    public int money;

    public int hp;
    public int hpMax;
    public float damage;
    public float velocidad;
    public float jumpForce;

    public bool canDoubleJump;
    public int numDoubleJumps;

    public bool canDash;
    public int numDashes;
    public float dashForce;
    public bool canDamageDash;

    public bool canWallClimb;
    public float wallClimbSpeed;

    public bool canHookIr;
    public bool canHookAtraer;

    public bool canCoinMagnet;

    public List<Arma> misArmas;
}