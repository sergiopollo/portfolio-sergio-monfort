using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCoin : MonoBehaviour
{
    public AudioClip clip;
    private void OnTriggerEnter(Collider other)
    {
        AudioSource.PlayClipAtPoint(clip, other.GetComponent<MovimientoJugador>().vCam.transform.position, 1.0F);
        Destroy(this.gameObject);
    }
}
