// GENERATED AUTOMATICALLY FROM 'Assets/personajes/player/input system/InputPlayer.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputPlayer : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputPlayer()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputPlayer"",
    ""maps"": [
        {
            ""name"": ""player"",
            ""id"": ""10dbbe61-e207-44c4-918f-ad698d8ab618"",
            ""actions"": [
                {
                    ""name"": ""saltoYWallclimb"",
                    ""type"": ""Button"",
                    ""id"": ""ec3b18d5-e81b-42ef-bb57-21668f6e1b68"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""movimiento"",
                    ""type"": ""Value"",
                    ""id"": ""0ac7f2fb-6738-4911-b545-362034f7a9dd"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""attack"",
                    ""type"": ""Button"",
                    ""id"": ""f818b2b8-adbb-4a16-9ec1-df01304cb817"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""lookAround"",
                    ""type"": ""Value"",
                    ""id"": ""a47efb4d-2276-4b5d-95f6-efa1544cc833"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""fijarCamara"",
                    ""type"": ""Button"",
                    ""id"": ""c9598547-caed-4fb5-8709-1d6057525a27"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""dash"",
                    ""type"": ""Button"",
                    ""id"": ""bbe08085-5893-4e51-8340-a004e41438db"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""hookIr"",
                    ""type"": ""Button"",
                    ""id"": ""020af25d-a616-42d5-b9e0-3ca57c7be718"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""hookAtraer"",
                    ""type"": ""Button"",
                    ""id"": ""12fb47f0-2980-47d2-871a-16e4d6d9f72d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""changeWeapon"",
                    ""type"": ""Button"",
                    ""id"": ""7ccb817e-0ce1-4d6c-8098-51e05ecf9147"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dance"",
                    ""type"": ""Button"",
                    ""id"": ""2103700e-6c7f-4098-b812-4c226aac7109"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""seleccionarMenu"",
                    ""type"": ""Button"",
                    ""id"": ""b15ca081-adec-4e6b-abfa-d3ddfcbe5eda"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""abrirYCerrarTienda"",
                    ""type"": ""Button"",
                    ""id"": ""52e69e95-3bf2-4c09-95c8-7fcaff9bc5f9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""menuCancel"",
                    ""type"": ""Button"",
                    ""id"": ""78c0e6ab-44cf-4f0f-aec8-cb1298e54b85"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""menuMoveLeft"",
                    ""type"": ""Button"",
                    ""id"": ""d04368ea-8f83-412c-94ef-515cf7a43523"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""menuMoveRight"",
                    ""type"": ""Button"",
                    ""id"": ""0aa1659c-64ba-4c89-b17e-a492c35e3ac9"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""openPauseMenu"",
                    ""type"": ""Button"",
                    ""id"": ""5cdec56f-27ba-4f96-9cbc-3adfdcafa994"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""map"",
                    ""type"": ""Button"",
                    ""id"": ""8a66750b-3092-476d-8df2-7e634924d08e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""85f5547c-e1be-4963-83fe-068b111d4e23"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""saltoYWallclimb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b22734d7-f9e5-4632-855d-3179918caa7e"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""saltoYWallclimb"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""da58aeba-80b8-4ebc-8330-8e796ec69b08"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""movimiento"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""96ef7c54-d085-4033-9a49-395ccab3eb7f"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""e85b25fd-db6b-4819-866b-bb26022d0e49"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""ec280e06-8079-4eaf-a476-9025da5c6714"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""3b955ea7-25d0-41bf-8ef4-c65a6722ff52"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""ac6d1267-70ca-43f8-900e-10af7f27cc17"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""72bf89f2-4027-40f4-93da-a9ccf638d70f"",
                    ""path"": ""<Gamepad>/dpad"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""movimiento"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1d6bb4af-bea8-4cd7-b5b0-933898af58a1"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1ade5fbb-8954-4a9c-b751-3f1d534c039a"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""attack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a96d93f2-777e-4442-807a-45d41a7b1a41"",
                    ""path"": ""<Mouse>/delta"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""lookAround"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5c0df564-c16e-46ff-b767-c4cd2695295b"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""lookAround"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7b5002cd-0a97-4870-8507-57877eef09ab"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""fijarCamara"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8a11d16f-24a3-456d-9fec-73fd590f95b4"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""fijarCamara"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ee53f3fd-2c5e-4641-96b4-2e2710b58ffc"",
                    ""path"": ""<Keyboard>/leftShift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e8887ac7-81f8-41de-b9c4-793ff2971c88"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cdbccd5e-9266-455e-8f80-a1bbc47087c8"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""hookIr"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2ee96a6e-4473-46c7-8c87-e8c63ffbdae3"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""hookIr"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1fb67a21-5a09-4738-8b40-76ec80aabd7c"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""hookAtraer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""73941f29-e742-4112-9607-c6531f971975"",
                    ""path"": ""<Gamepad>/leftTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""hookAtraer"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4863bc8e-2fb2-4a57-98d9-ec30f3a65e21"",
                    ""path"": ""<Keyboard>/r"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""changeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d008d67c-71dd-4711-8b1f-93154df900d1"",
                    ""path"": ""<Gamepad>/buttonNorth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""changeWeapon"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""30c4854a-eb9d-4624-9b52-1f7524b19b15"",
                    ""path"": ""<Keyboard>/v"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""Dance"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""490763da-d04f-44b4-8a67-1f2a7a24513e"",
                    ""path"": ""<Gamepad>/rightStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Dance"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8665324b-dd51-4be7-b90f-d383a44bbf47"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""seleccionarMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47209d0e-e915-4edf-a099-07dd1cdb9cb3"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""seleccionarMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""fe627137-da28-4a48-9950-ce869997e009"",
                    ""path"": ""<Keyboard>/t"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""abrirYCerrarTienda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""86be9f3c-d4c4-4afb-a4fe-0b30ae38bca0"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""abrirYCerrarTienda"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d61c97bd-9b04-441e-a0e8-20ded55f5b95"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""menuCancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9b2e0dc6-d9ae-47f8-a213-b0d341724f84"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuCancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2b898765-bec9-4498-a882-f432dcb1b6e6"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""63b79fe6-ba70-4a92-bbad-36ba7ccfc827"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b197138a-103f-4474-a325-27d0c72b31ed"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""62b3d91e-e3c4-448c-a86c-4537be1e7d44"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""d28735d9-13b3-4da1-b310-ad5cf587b120"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7bc8844b-03f6-4b93-8a92-32a459f38c09"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""837a3b0d-707c-42ff-8d04-588e5aed32e4"",
                    ""path"": ""<Gamepad>/rightStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""efb4e0ec-fdaa-49eb-ac00-46771c8ce1d2"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""menuMoveLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""27804e7a-ecfc-404f-be1b-b80a5e04593d"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9b178776-866e-456b-9114-124f22144cb9"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ae951cd1-3b64-4239-ac58-b961ce17ad1e"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""92207ef2-8f26-46bb-ae8f-d9259551b63d"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""56ad57ba-02a8-46d5-9b74-d2740a7cddc3"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""867ffca3-b0aa-4038-bfdd-6b5560729155"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""3b50529f-8c8a-4e6f-9177-32711bb46827"",
                    ""path"": ""<Gamepad>/rightStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e18e0401-2e44-45a8-a22c-5ce93c3fadea"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""menuMoveRight"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9774d2e1-e795-43a9-8a08-17e95d3ea849"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""openPauseMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""aa06cde6-40f0-42ca-af4e-5292f31c0a9c"",
                    ""path"": ""<Gamepad>/start"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""openPauseMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""675a0c61-988b-4bd3-81c3-8558c4a1ae95"",
                    ""path"": ""<Gamepad>/select"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""openPauseMenu"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4728ab3f-9eac-4018-8aa5-c0e3deb27446"",
                    ""path"": ""<Keyboard>/m"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""KeyBoardAndMouse"",
                    ""action"": ""map"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c2aa9057-f8dd-4c8c-bc83-467cff7ba316"",
                    ""path"": ""<Gamepad>/leftStickPress"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""map"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""KeyBoardAndMouse"",
            ""bindingGroup"": ""KeyBoardAndMouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // player
        m_player = asset.FindActionMap("player", throwIfNotFound: true);
        m_player_saltoYWallclimb = m_player.FindAction("saltoYWallclimb", throwIfNotFound: true);
        m_player_movimiento = m_player.FindAction("movimiento", throwIfNotFound: true);
        m_player_attack = m_player.FindAction("attack", throwIfNotFound: true);
        m_player_lookAround = m_player.FindAction("lookAround", throwIfNotFound: true);
        m_player_fijarCamara = m_player.FindAction("fijarCamara", throwIfNotFound: true);
        m_player_dash = m_player.FindAction("dash", throwIfNotFound: true);
        m_player_hookIr = m_player.FindAction("hookIr", throwIfNotFound: true);
        m_player_hookAtraer = m_player.FindAction("hookAtraer", throwIfNotFound: true);
        m_player_changeWeapon = m_player.FindAction("changeWeapon", throwIfNotFound: true);
        m_player_Dance = m_player.FindAction("Dance", throwIfNotFound: true);
        m_player_seleccionarMenu = m_player.FindAction("seleccionarMenu", throwIfNotFound: true);
        m_player_abrirYCerrarTienda = m_player.FindAction("abrirYCerrarTienda", throwIfNotFound: true);
        m_player_menuCancel = m_player.FindAction("menuCancel", throwIfNotFound: true);
        m_player_menuMoveLeft = m_player.FindAction("menuMoveLeft", throwIfNotFound: true);
        m_player_menuMoveRight = m_player.FindAction("menuMoveRight", throwIfNotFound: true);
        m_player_openPauseMenu = m_player.FindAction("openPauseMenu", throwIfNotFound: true);
        m_player_map = m_player.FindAction("map", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // player
    private readonly InputActionMap m_player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_player_saltoYWallclimb;
    private readonly InputAction m_player_movimiento;
    private readonly InputAction m_player_attack;
    private readonly InputAction m_player_lookAround;
    private readonly InputAction m_player_fijarCamara;
    private readonly InputAction m_player_dash;
    private readonly InputAction m_player_hookIr;
    private readonly InputAction m_player_hookAtraer;
    private readonly InputAction m_player_changeWeapon;
    private readonly InputAction m_player_Dance;
    private readonly InputAction m_player_seleccionarMenu;
    private readonly InputAction m_player_abrirYCerrarTienda;
    private readonly InputAction m_player_menuCancel;
    private readonly InputAction m_player_menuMoveLeft;
    private readonly InputAction m_player_menuMoveRight;
    private readonly InputAction m_player_openPauseMenu;
    private readonly InputAction m_player_map;
    public struct PlayerActions
    {
        private @InputPlayer m_Wrapper;
        public PlayerActions(@InputPlayer wrapper) { m_Wrapper = wrapper; }
        public InputAction @saltoYWallclimb => m_Wrapper.m_player_saltoYWallclimb;
        public InputAction @movimiento => m_Wrapper.m_player_movimiento;
        public InputAction @attack => m_Wrapper.m_player_attack;
        public InputAction @lookAround => m_Wrapper.m_player_lookAround;
        public InputAction @fijarCamara => m_Wrapper.m_player_fijarCamara;
        public InputAction @dash => m_Wrapper.m_player_dash;
        public InputAction @hookIr => m_Wrapper.m_player_hookIr;
        public InputAction @hookAtraer => m_Wrapper.m_player_hookAtraer;
        public InputAction @changeWeapon => m_Wrapper.m_player_changeWeapon;
        public InputAction @Dance => m_Wrapper.m_player_Dance;
        public InputAction @seleccionarMenu => m_Wrapper.m_player_seleccionarMenu;
        public InputAction @abrirYCerrarTienda => m_Wrapper.m_player_abrirYCerrarTienda;
        public InputAction @menuCancel => m_Wrapper.m_player_menuCancel;
        public InputAction @menuMoveLeft => m_Wrapper.m_player_menuMoveLeft;
        public InputAction @menuMoveRight => m_Wrapper.m_player_menuMoveRight;
        public InputAction @openPauseMenu => m_Wrapper.m_player_openPauseMenu;
        public InputAction @map => m_Wrapper.m_player_map;
        public InputActionMap Get() { return m_Wrapper.m_player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @saltoYWallclimb.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSaltoYWallclimb;
                @saltoYWallclimb.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSaltoYWallclimb;
                @saltoYWallclimb.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSaltoYWallclimb;
                @movimiento.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovimiento;
                @movimiento.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovimiento;
                @movimiento.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovimiento;
                @attack.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @attack.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @attack.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAttack;
                @lookAround.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookAround;
                @lookAround.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookAround;
                @lookAround.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLookAround;
                @fijarCamara.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFijarCamara;
                @fijarCamara.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFijarCamara;
                @fijarCamara.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnFijarCamara;
                @dash.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @dash.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @dash.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDash;
                @hookIr.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookIr;
                @hookIr.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookIr;
                @hookIr.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookIr;
                @hookAtraer.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookAtraer;
                @hookAtraer.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookAtraer;
                @hookAtraer.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnHookAtraer;
                @changeWeapon.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @changeWeapon.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @changeWeapon.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnChangeWeapon;
                @Dance.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDance;
                @Dance.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDance;
                @Dance.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnDance;
                @seleccionarMenu.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSeleccionarMenu;
                @seleccionarMenu.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSeleccionarMenu;
                @seleccionarMenu.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSeleccionarMenu;
                @abrirYCerrarTienda.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAbrirYCerrarTienda;
                @abrirYCerrarTienda.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAbrirYCerrarTienda;
                @abrirYCerrarTienda.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnAbrirYCerrarTienda;
                @menuCancel.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @menuCancel.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @menuCancel.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @menuMoveLeft.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveLeft;
                @menuMoveLeft.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveLeft;
                @menuMoveLeft.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveLeft;
                @menuMoveRight.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveRight;
                @menuMoveRight.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveRight;
                @menuMoveRight.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMoveRight;
                @openPauseMenu.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @openPauseMenu.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @openPauseMenu.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnOpenPauseMenu;
                @map.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMap;
                @map.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMap;
                @map.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMap;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @saltoYWallclimb.started += instance.OnSaltoYWallclimb;
                @saltoYWallclimb.performed += instance.OnSaltoYWallclimb;
                @saltoYWallclimb.canceled += instance.OnSaltoYWallclimb;
                @movimiento.started += instance.OnMovimiento;
                @movimiento.performed += instance.OnMovimiento;
                @movimiento.canceled += instance.OnMovimiento;
                @attack.started += instance.OnAttack;
                @attack.performed += instance.OnAttack;
                @attack.canceled += instance.OnAttack;
                @lookAround.started += instance.OnLookAround;
                @lookAround.performed += instance.OnLookAround;
                @lookAround.canceled += instance.OnLookAround;
                @fijarCamara.started += instance.OnFijarCamara;
                @fijarCamara.performed += instance.OnFijarCamara;
                @fijarCamara.canceled += instance.OnFijarCamara;
                @dash.started += instance.OnDash;
                @dash.performed += instance.OnDash;
                @dash.canceled += instance.OnDash;
                @hookIr.started += instance.OnHookIr;
                @hookIr.performed += instance.OnHookIr;
                @hookIr.canceled += instance.OnHookIr;
                @hookAtraer.started += instance.OnHookAtraer;
                @hookAtraer.performed += instance.OnHookAtraer;
                @hookAtraer.canceled += instance.OnHookAtraer;
                @changeWeapon.started += instance.OnChangeWeapon;
                @changeWeapon.performed += instance.OnChangeWeapon;
                @changeWeapon.canceled += instance.OnChangeWeapon;
                @Dance.started += instance.OnDance;
                @Dance.performed += instance.OnDance;
                @Dance.canceled += instance.OnDance;
                @seleccionarMenu.started += instance.OnSeleccionarMenu;
                @seleccionarMenu.performed += instance.OnSeleccionarMenu;
                @seleccionarMenu.canceled += instance.OnSeleccionarMenu;
                @abrirYCerrarTienda.started += instance.OnAbrirYCerrarTienda;
                @abrirYCerrarTienda.performed += instance.OnAbrirYCerrarTienda;
                @abrirYCerrarTienda.canceled += instance.OnAbrirYCerrarTienda;
                @menuCancel.started += instance.OnMenuCancel;
                @menuCancel.performed += instance.OnMenuCancel;
                @menuCancel.canceled += instance.OnMenuCancel;
                @menuMoveLeft.started += instance.OnMenuMoveLeft;
                @menuMoveLeft.performed += instance.OnMenuMoveLeft;
                @menuMoveLeft.canceled += instance.OnMenuMoveLeft;
                @menuMoveRight.started += instance.OnMenuMoveRight;
                @menuMoveRight.performed += instance.OnMenuMoveRight;
                @menuMoveRight.canceled += instance.OnMenuMoveRight;
                @openPauseMenu.started += instance.OnOpenPauseMenu;
                @openPauseMenu.performed += instance.OnOpenPauseMenu;
                @openPauseMenu.canceled += instance.OnOpenPauseMenu;
                @map.started += instance.OnMap;
                @map.performed += instance.OnMap;
                @map.canceled += instance.OnMap;
            }
        }
    }
    public PlayerActions @player => new PlayerActions(this);
    private int m_KeyBoardAndMouseSchemeIndex = -1;
    public InputControlScheme KeyBoardAndMouseScheme
    {
        get
        {
            if (m_KeyBoardAndMouseSchemeIndex == -1) m_KeyBoardAndMouseSchemeIndex = asset.FindControlSchemeIndex("KeyBoardAndMouse");
            return asset.controlSchemes[m_KeyBoardAndMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnSaltoYWallclimb(InputAction.CallbackContext context);
        void OnMovimiento(InputAction.CallbackContext context);
        void OnAttack(InputAction.CallbackContext context);
        void OnLookAround(InputAction.CallbackContext context);
        void OnFijarCamara(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
        void OnHookIr(InputAction.CallbackContext context);
        void OnHookAtraer(InputAction.CallbackContext context);
        void OnChangeWeapon(InputAction.CallbackContext context);
        void OnDance(InputAction.CallbackContext context);
        void OnSeleccionarMenu(InputAction.CallbackContext context);
        void OnAbrirYCerrarTienda(InputAction.CallbackContext context);
        void OnMenuCancel(InputAction.CallbackContext context);
        void OnMenuMoveLeft(InputAction.CallbackContext context);
        void OnMenuMoveRight(InputAction.CallbackContext context);
        void OnOpenPauseMenu(InputAction.CallbackContext context);
        void OnMap(InputAction.CallbackContext context);
    }
}
