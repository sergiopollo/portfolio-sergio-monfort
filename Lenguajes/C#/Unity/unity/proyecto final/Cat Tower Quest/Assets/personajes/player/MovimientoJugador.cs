using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
public class MovimientoJugador : MonoBehaviour
{
    //new input system
    InputPlayer inputs;
    private bool IsCurrentDeviceMouse;

    [Header("Camara")]
    //cosas rrobadas para la rotacion de la camara
    private float _targetRotation = 0.0f;
    private float _rotationVelocity;
    [Range(0.0f, 0.3f)]
    private float RotationSmoothTime = 0.12f;
    private const float _threshold = 0.01f;
    [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
    public GameObject CinemachineCameraTarget;

    [Tooltip("How far in degrees can you move the camera up")]
    public float TopClamp = 70.0f;

    [Tooltip("How far in degrees can you move the camera down")]
    public float BottomClamp = -30.0f;

    //[Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
    private float CameraAngleOverride = 0.0f;

    // cinemachine
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;

    //minimo y maximo de zoom de la camara
    public float minZoom;
    public float maxZoom;

    //virtual camera
    public CinemachineVirtualCamera vCam;

    //layer para el raycast
    private LayerMask ObstacleLayerMask;
    private LayerMask CameraIgnoreObstacleLayerMask;

    //collider de vision para el fijado de camara
    public BoxCollider visionCollider;
    private bool isFijandoCamara;
    public GameObject enemigoFijado;
    public Transform puntoMedioEnemigoYPlayer;
    public GameObject flechaFijacion;
    private GameObject flechaCreada;

    public Camera mainCamera;

    //evento UI
    public GameEvent actualizarUI;
    //evento muerte
    public GameEvent muertePlayer;

    [Header("Pause")]
    public bool Pause;

    [Header("Stats")]
    public PlayerInventory playerInventory;
    public PlayerInventory statsBase;
    public int hp;
    public int hpMax;
    public float damage;
    public float velocidad;
    private float velocidad_real;

    private Rigidbody rigidBody;

    //animator
    public Animator animator;

    //materiales
    public Material materialCuerpo;
    public Material materialCabeza;
    public Material materialDamageDash;
    public Material materialDamageDashCabeza;

    //partes del personaje
    public GameObject cabezaGato;
    public GameObject cuerpo;

    [Header("ListaMusica")]
    public List<AudioClip> audios;
    // 0- fortnite 1- jump 2- dash 3-meow 4-super 5-land

    public enum States
    {
        Idle, Caminar, Saltar, Dash, WallClimb, Fall, Land, Dance, YEET, death
    }
    public States state;
    public bool isGrounded = false;

    public bool isAttacking;
    public SphereCollider attackHitbox;

    //armas
    public GameObject espada;
    public GameObject manoArma;
    public List<GameObject> listaModelosArmaParaLaMano;
    public int indexArmas;
    public Arma currentArma;
    public List<Arma> armas;
    private bool isShooting;

    [Header("Habilidades")]
    public float jumpForce;

    [Header("Doble Salto")]
    public bool canDoubleJump;
    public int numDoubleJumps = 1;
    private int numDoubleJumpsReal;

    [Header("Dash")]
    public bool canDash;
    public int numDashes=1;
    private int numDashesReal;
    public float dashForce;
    public bool canDamageDash;

    [Header("WallClimb")]
    public bool canWallClimb;
    public float wallClimbSpeed=5;
    private bool isTouchingWall;

    [Header("Hook")]
    public bool canHookIr;
    public bool canHookAtraer;
    private Grapple grappleScript;

    [Header("Coin Magnet")]
    public bool canCoinMagnet;

    //hay que tener por parametro la corrutina actual para stopCoroutine, si no no hace nada
    private IEnumerator corrutinaSaltoActual;
    private IEnumerator corrutinaDashActual;
    private IEnumerator corrutinaLandActual;
    private IEnumerator corrutinaDanceActual;
    private IEnumerator corrutinaYEETActual;

    //iman de monedas
    public SphereCollider magnetHitbox;

    private void Awake()
    {
        inputs = new InputPlayer();
        inputs.Enable();
        IsCurrentDeviceMouse = true;
        ponerInputs();
        startPlayer();
    }

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().Stop();
        inicializarInventory();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Pause)
        {
            Controls();
            GroundedAndWallCheck();
            calculaPuntoMedioEnemigoYPlayer();
        }
        //Debug.Log("state: " + state+" isGrounded: "+isGrounded + " isAttacking: " + isAttacking + " isTouchingWall "+isTouchingWall);
        //Debug.Log("velocity: "+rigidBody.velocity);
    }

    private void LateUpdate()
    {
        if (!Pause)
        {
            //rotacion de la camara con el mouse
            CameraRotation();
        }
    }

    private void ponerInputs()
    {
        inputs.player.attack.performed += MouseButton0Down;
        inputs.player.fijarCamara.performed += MouseButton1Down;
        inputs.player.dash.performed += LeftShiftDown;
        inputs.player.saltoYWallclimb.canceled += SpaceBarUp;
        inputs.player.saltoYWallclimb.performed += SpaceBarDown;
        inputs.player.hookIr.canceled += EUp;
        inputs.player.hookAtraer.canceled += QUp;
        inputs.player.changeWeapon.performed += RDown;
        inputs.player.Dance.performed += VDown;
    }


    private void startPlayer()
    {
        velocidad_real = velocidad;
        resetDoubleJumps();
        resetDashes();
        grappleScript = this.GetComponent<Grapple>();
        rigidBody = this.GetComponent<Rigidbody>();
        _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;
        //cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        CambiarEstado(States.Idle);
        //quiero que el raycast actue sobre Obstacle y CameraIgnoreObstacle
        ObstacleLayerMask = LayerMask.GetMask("Obstacle");
        CameraIgnoreObstacleLayerMask = LayerMask.GetMask("CameraIgnoreObstacle");
        ponerMaterialesDefault();
        indexArmas = -1;
    }

    public void inicializarInventory()
    {
        this.hpMax = playerInventory.hpMax;
        this.hp = playerInventory.hp;
        this.damage = playerInventory.damage;
        this.velocidad = playerInventory.velocidad;
        this.jumpForce = playerInventory.jumpForce;
        this.canDoubleJump = playerInventory.canDoubleJump;
        this.numDoubleJumps = playerInventory.numDoubleJumps;
        this.canDash = playerInventory.canDash;
        this.numDashes = playerInventory.numDashes;
        this.dashForce = playerInventory.dashForce;
        this.canDamageDash = playerInventory.canDamageDash;
        this.canWallClimb = playerInventory.canWallClimb;
        this.wallClimbSpeed = playerInventory.wallClimbSpeed;
        this.canHookIr = playerInventory.canHookIr;
        this.canHookAtraer = playerInventory.canHookAtraer;
        this.canCoinMagnet = playerInventory.canCoinMagnet;
        this.armas = playerInventory.misArmas;

        actualizarUI.Raise();
    }

    public void actualizarInventory()
    {
        this.hpMax = playerInventory.hpMax;
        this.hp = playerInventory.hp;
        this.damage = playerInventory.damage;
        this.velocidad = playerInventory.velocidad;
        this.jumpForce = playerInventory.jumpForce;
        this.canDoubleJump = playerInventory.canDoubleJump;
        this.numDoubleJumps = playerInventory.numDoubleJumps;
        this.canDash = playerInventory.canDash;
        this.numDashes = playerInventory.numDashes;
        this.dashForce = playerInventory.dashForce;
        this.canDamageDash = playerInventory.canDamageDash;
        this.canWallClimb = playerInventory.canWallClimb;
        this.wallClimbSpeed = playerInventory.wallClimbSpeed;
        this.canHookIr = playerInventory.canHookIr;
        this.canHookAtraer = playerInventory.canHookAtraer;
        this.canCoinMagnet = playerInventory.canCoinMagnet;

        actualizarUI.Raise();
    }

    public void actualizarGuardadoInventory()
    {
        playerInventory.hp = this.hp;
        playerInventory.misArmas = this.armas;

        actualizarUI.Raise();
    }

    private void GroundedAndWallCheck()
    {

        //GROUNDED CHECK layer 1
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -(this.transform.up), out hit, 0.5f, ObstacleLayerMask))
        {
            if (!isGrounded)
            {
                isGrounded = true;
                CambiarEstado(States.Land);
                resetDoubleJumps();
                resetDashes();
            }

        }
        //GROUNDED CHECK layer 2
        else if (Physics.Raycast(transform.position, -(this.transform.up), out hit, 0.5f, CameraIgnoreObstacleLayerMask))
        {
            Debug.DrawLine(transform.position, -(this.transform.up) - hit.point, Color.red);
            if (!isGrounded)
            {
                isGrounded = true;
                CambiarEstado(States.Land);
                resetDoubleJumps();
                resetDashes();
            }

        }
        else
        {
            isGrounded = false;
        }

        //WALLCHECK layer 1
        RaycastHit hit2;
        if (Physics.Raycast(transform.position, transform.forward, out hit2, 0.6f, ObstacleLayerMask))
        {
            isTouchingWall = true;
            Debug.DrawLine(hit.point, hit.point - (hit.normal * 5), Color.red);
        }
        //WALLCHECK layer 2
        else if (Physics.Raycast(transform.position, transform.forward, out hit2, 0.6f, CameraIgnoreObstacleLayerMask))
        {
            isTouchingWall = true;
            Debug.DrawLine(hit.point, hit.point - (hit.normal * 5), Color.red);
        }
        else
        {
            isTouchingWall = false;
        }
    }
    
    //te puedes mover mientras no estes dasheando. Saltar cancela el dash actual
    private void Controls()
    {
        //movimiento WASD
        if (state != States.Dash && state != States.WallClimb)
        {
            move();
        }

        
        //wallclimb modo terrorista porque el new input system no tiene un equivalente a getKey
        if (inputs.player.saltoYWallclimb.ReadValue<float>() == 1)
        {
            if (canWallClimb && isTouchingWall)
            {
                //raycast para que el personaje mire a la pared si esta mas o menos mirando a ella, si esta de espaldas no puede hacer el wallclimb
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, 2, ObstacleLayerMask))
                {
                    wallClimb(hit);
                }
                //comprobacion en la otra layer
                else if (Physics.Raycast(transform.position, transform.forward, out hit, 2, CameraIgnoreObstacleLayerMask))
                {
                    wallClimb(hit);
                }
            }
        }

        //stop wallclimb 2 (por si el jugador no suelta el espacio y no se llama a la funcion del evento getkeyup)
        if ((!isTouchingWall && state == States.WallClimb))
        {
            CambiarEstado(States.Caminar);
        }

        //hooks modo terrorista tambien
        //hook ir
        if (inputs.player.hookIr.ReadValue<float>() == 1)
        {
            if (!Pause)
            {
                if (canHookIr)
                {
                    grappleScript.HookIr();
                }
            }
        }

        //hook atraer
        if (inputs.player.hookAtraer.ReadValue<float>() == 1)
        {
            if (!Pause)
            {
                if (canHookAtraer)
                {
                    grappleScript.HookAtraer();
                }
            }
        }

        //INPUTS VIEJOS
        /*
        //fijacion de camara
        if (Input.GetMouseButtonDown(1))
        {
            fijarCamara();
        }


        //COMBATE
        //con el click un ataque con combos
        if (Input.GetMouseButtonDown(0) && !isAttacking && state != States.WallClimb)
        {
            StartCoroutine(attack());
        }


        //HABILIDADES
        //Dash
        if (Input.GetKeyDown(KeyCode.LeftShift) && state != States.Dash && (canDash && (isGrounded || numDashesReal > 0)) && state != States.WallClimb)
        {
            if (corrutinaSaltoActual != null)
            {
                StopCoroutine(corrutinaSaltoActual);
            }
            corrutinaDashActual = Dash();
            StartCoroutine(corrutinaDashActual);
        }

        //Salto y "doble salto" en el aire
        if (Input.GetKeyDown(KeyCode.Space) && state != States.Saltar && (isGrounded || (canDoubleJump && numDoubleJumpsReal > 0)) && state != States.WallClimb)
        {
            if (corrutinaDashActual != null)
            {
                StopCoroutine(corrutinaDashActual);
                if (canDamageDash)
                {
                    isAttacking = false;
                    this.attackHitbox.gameObject.SetActive(false);
                    this.gameObject.layer = LayerMask.NameToLayer("Player");
                    ponerMaterialesDefault();
                }
            }
            corrutinaSaltoActual = Salto();
            StartCoroutine(corrutinaSaltoActual);
        }

        //wallclimb
        if (Input.GetKey(KeyCode.Space) && canWallClimb && isTouchingWall)
        {
            //raycast para que el personaje mire a la pared si esta mas o menos mirando a ella, si esta de espaldas no puede hacer el wallclimb
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 2, ObstacleLayerMask))
            {
                wallClimb(hit);
            }
            //comprobacion en la otra layer
            else if (Physics.Raycast(transform.position, transform.forward, out hit, 2, CameraIgnoreObstacleLayerMask))
            {
                wallClimb(hit);
            }
        }

        //hook ir
        if (Input.GetKeyDown(KeyCode.E) && canHookIr)
        {
            grappleScript.HookIr();
        }
        //hook atraer
        if (Input.GetKeyDown(KeyCode.Q) && canHookAtraer)
        {
            grappleScript.HookAtraer();
        }

        //stop wallclimb
        if ((Input.GetKeyUp(KeyCode.Space) && state == States.WallClimb) || (!isTouchingWall && state == States.WallClimb))
        {
            //Debug.Log("paro wallclimb");
            CambiarEstado(States.Caminar);
        }

        //cambiar de arma
        if (Input.GetKeyDown(KeyCode.R) && !isShooting)
        {
            switchArma();
        }

        //easter egg
        if (Input.GetKeyDown(KeyCode.V) && isGrounded && rigidBody.velocity== Vector3.zero)
        {
            CambiarEstado(States.Dance);
        }
        */
    }

    //funciones new input system

    //click izquierdo atacar
    private void MouseButton0Down(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (!isAttacking && state != States.WallClimb)
            {
                StartCoroutine(attack());
            }
        }
    }

    //click derecho fijar camara
    private void MouseButton1Down(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            fijarCamara();
        }
    }

    //left shift dash
    private void LeftShiftDown(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (state != States.Dash && (canDash && (isGrounded || numDashesReal > 0)) && state != States.WallClimb)
            {
                if (corrutinaSaltoActual != null)
                {
                    StopCoroutine(corrutinaSaltoActual);
                }
                corrutinaDashActual = Dash();
                StartCoroutine(corrutinaDashActual);
            }
        }
    }

    //espacio pulsado salto
    private void SpaceBarDown(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (state != States.Saltar && (isGrounded || (canDoubleJump && numDoubleJumpsReal > 0)) && state != States.WallClimb)
            {
                if (corrutinaDashActual != null)
                {
                    StopCoroutine(corrutinaDashActual);
                    if (canDamageDash)
                    {
                        isAttacking = false;
                        this.attackHitbox.gameObject.SetActive(false);
                        this.gameObject.layer = LayerMask.NameToLayer("Player");
                        ponerMaterialesDefault();
                    }
                }
                corrutinaSaltoActual = Salto();
                StartCoroutine(corrutinaSaltoActual);
            }
        }
    }


    //espacio soltado stop wallclimb
    private void SpaceBarUp(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (state == States.WallClimb || (!isTouchingWall && state == States.WallClimb))
            {
                //Debug.Log("paro wallclimb");
                CambiarEstado(States.Caminar);
            }
        }
    }


    //E hook ir soltar
    private void EUp(InputAction.CallbackContext context)
    {
        grappleScript.DestroyHookCheck();
    }

    //Q hook atraer soltar
    private void QUp(InputAction.CallbackContext context)
    {
        grappleScript.DestroyHookCheck();
    }

    //R cambiar de arma
    private void RDown(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (!isShooting)
            {
                switchArma();
            }
        }
    }

    //V bailar
    private void VDown(InputAction.CallbackContext context)
    {
        if (!Pause)
        {
            if (isGrounded && rigidBody.velocity == Vector3.zero)
            {
                CambiarEstado(States.Dance);
            }
        }
    }

    private void fijarCamara()
    {
        //1: activa el collider de vision y mira si hay algun gameobject con el tag enemigo dentro
        //2: si hay alguno (DE MOMENTO PILLA AL PRIMERO Y PUNTO) calcula cual es el mas cercano y lo fija (lo guarda por parametro)
        //si no hay ninguno se activara en cuanto encuentre alguno

        if (isFijandoCamara)
        {
            isFijandoCamara = false;
            visionCollider.gameObject.SetActive(false);
            enemigoFijado = null;
            if (flechaCreada != null)
            {
                Destroy(flechaCreada);
            }

        }
        else
        {
            isFijandoCamara = true;
            visionCollider.gameObject.SetActive(true);
            //espera a que hitboxVision le de un enemigo fijado y entonces hitboxVision se desactivara
        }
    }

    //funcion en respuesta al gameEvent onEnemyDeath, que comprueba si el enemigo fijado es el que ha muerto para desfijarlo
    public void checkEnemigoFijado()
    {
        if (enemigoFijado != null)
        {
            if (enemigoFijado.GetComponent<IABASE>().vida <= 0)
            {
                if (isFijandoCamara)
                {
                    fijarCamara();
                }
            }
        }
    }

    private void calculaPuntoMedioEnemigoYPlayer()
    {
        if (enemigoFijado != null)
        {
            puntoMedioEnemigoYPlayer.position = new Vector3((this.transform.position.x + enemigoFijado.transform.position.x) / 2, (this.transform.position.y + enemigoFijado.transform.position.y) / 2, (this.transform.position.z + enemigoFijado.transform.position.z) / 2);

            puntoMedioEnemigoYPlayer.LookAt(enemigoFijado.transform);

            _cinemachineTargetYaw = puntoMedioEnemigoYPlayer.eulerAngles.y;

            if (puntoMedioEnemigoYPlayer.eulerAngles.x < TopClamp && puntoMedioEnemigoYPlayer.eulerAngles.x > BottomClamp)
            {
                _cinemachineTargetPitch = puntoMedioEnemigoYPlayer.eulerAngles.x;
            }

            //flecha
            if (flechaCreada == null)
            {
                flechaCreada = Instantiate(flechaFijacion); 
            }
            flechaCreada.transform.position = new Vector3(enemigoFijado.transform.position.x, enemigoFijado.transform.position.y + 3, enemigoFijado.transform.position.z);
            flechaCreada.transform.LookAt(enemigoFijado.transform.position, vCam.transform.position);
        }
    }

    private void CameraRotation()
    {
        Vector2 mouse = inputs.player.lookAround.ReadValue<Vector2>();

        //comprobacion de si se esta usando mando o raton para rotar
        if (inputs.player.lookAround.activeControl != null)
        {
            if (inputs.player.lookAround.activeControl.displayName == "Delta")
            {
                IsCurrentDeviceMouse = true;
            }
            else
            {
                IsCurrentDeviceMouse = false;
            }
        }
        
        //ROTACION DE LA CAMARA
        // if there is an input and camera position is not fixed (lo de fixed lo he quitado, era un booleano publico para no poder rotar la camara)
        //if (new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")).sqrMagnitude >= _threshold && !isFijandoCamara)
        //if (new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")).sqrMagnitude >= _threshold && enemigoFijado == null)
        if (mouse.sqrMagnitude >= _threshold && enemigoFijado == null)
        {
            //Don't multiply mouse input by Time.deltaTime;
            //float deltaTimeMultiplier = 1f;//IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
            float deltaTimeMultiplier = IsCurrentDeviceMouse ? 0.1f : 150 * Time.deltaTime;
            _cinemachineTargetYaw += mouse.x * deltaTimeMultiplier;
            _cinemachineTargetPitch += mouse.y * -1 * deltaTimeMultiplier;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        if (enemigoFijado == null)
        {
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);
        }
        //esto rota camaraVision, haciendo que la camara mire siempre al forward rotado de camaraVision (o el target que se ponga).
        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
        _cinemachineTargetYaw, 0.0f);
        
        //ZOOM CON LA RUEDA DEL RATON
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (vCam.m_Lens.FieldOfView < maxZoom)
            {
                vCam.m_Lens.FieldOfView += 5;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (vCam.m_Lens.FieldOfView > minZoom)
            {
                vCam.m_Lens.FieldOfView -= 5;
            }
        }
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        //funcion rara para que la camara gire bien (la verdad no se muy bien que hace)
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

    IEnumerator attack() {
        if (currentArma == null)
        {
            this.GetComponent<combos>().atacar();
            yield return new WaitForSeconds(0.5f);
        }
        else
        {
            shootArma();
        }
    }

    private void move()
    {
        //pone la velocidad segun si esta corriendo o quieto
        //axis raw para que pase de 1 a 0 o 0 a -1 directamente, axis sin mas para que haya un incremento segun el tiempo pulsado (recomendado para mando, creo).
        //if (Input.GetAxis("Vertical") == 0 && Input.GetAxis("Horizontal") == 0)

        //if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == 0)
        Vector2 movimiento = inputs.player.movimiento.ReadValue<Vector2>();
        if (movimiento.x == 0 && movimiento.y == 0)
        {
            if (state != States.YEET)
            {
                velocidad_real = 0.0f;
            }   
            
            if ((state == States.Caminar || state == States.Idle))
            {
                if (isGrounded)
                {
                    if (state == States.Dance)
                    {
                        //no hagas idle
                    }
                    else
                    {
                        CambiarEstado(States.Idle);
                    }
                }
                else
                {
                    CambiarEstado(States.Fall);
                }

            }
        }
        else
        {
            velocidad_real = velocidad;
            if (state == States.Caminar || state == States.Idle || state == States.Dance)
            {
                if (isGrounded)
                {
                    CambiarEstado(States.Caminar);
                }
                else
                {
                    CambiarEstado(States.Fall);
                }
            }
        }

        //ESTO ROTA AL PERSONAJE SEGUN DONDE MIRE LA CAMARA Y SEGUN LOS INPUTS PULSADOS
        // normalise input direction
        Vector3 inputDirection = new Vector3(movimiento.x, 0.0f, movimiento.y).normalized;
        //Vector3 inputDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")).normalized;
        // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is a move input rotate player when the player is moving
        //if (new Vector2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) != Vector2.zero)
        if (movimiento != Vector2.zero)
        {
            _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                              mainCamera.transform.eulerAngles.y;
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                RotationSmoothTime);

            // rotate to face input direction relative to camera position
            transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
        }

        //ESTO MUEVE AL PERSONAJE
        if (state != States.YEET)
        {
            rigidBody.velocity = new Vector3(this.transform.forward.x*velocidad_real, rigidBody.velocity.y, this.transform.forward.z * velocidad_real);
        }
       
    }

    public void CambiarEstado(States s)
    {
        if (s == States.death)
        {
            state = s;
            if (corrutinaYEETActual != null)
            {
                StopCoroutine(corrutinaYEETActual);
            }
            StopAllCoroutines();
            animator.Play("Die");
        }
        else
        {
            //si esta haciendo la corrutina de land pero empieza un estado nuevo que no sea caminar o idle la para
            if (state == States.Land && corrutinaLandActual != null && s != States.Land && s != States.Caminar && s != States.Idle)
            {
                StopCoroutine(corrutinaLandActual);
            }

            //si esta en dance lo para
            if (state == States.Dance)
            {
                StopCoroutine(corrutinaDanceActual);
            }

            if (s != States.Idle && s != States.YEET && corrutinaYEETActual != null)
            {
                StopCoroutine(corrutinaYEETActual);
            }
            state = s;




            if (!isAttacking)
            {
                switch (s)
                {
                    case States.Idle:
                        this.GetComponent<AudioSource>().Stop();
                        animator.Play("Idle");
                        break;
                    case States.Caminar:
                        animator.Play("Caminar");
                        break;
                    case States.Saltar:
                        animator.Play("Saltar");
                        break;
                    case States.Dash:
                        animator.Play("Dash 2");
                        break;
                    case States.WallClimb:
                        animator.Play("WallClimb");
                        break;
                    case States.Fall:
                        animator.Play("Fall");
                        break;
                    case States.Land:
                        corrutinaLandActual = Land();
                        StartCoroutine(corrutinaLandActual);
                        break;
                    case States.Dance:
                        corrutinaDanceActual = Dance();
                        StartCoroutine(corrutinaDanceActual);
                        break;
                    case States.YEET:
                        animator.Play("Fall");
                        break;
                }
            }
            else
            {
                switch (s)
                {
                    case States.Dash:
                        animator.Play("Dash 2");
                        break;
                    case States.WallClimb:
                        animator.Play("WallClimb");
                        break;

                }
            }
        }
    }

    public void getHealed(int healing)
    {
        if (hp + healing > hpMax)
        {
            hp = hpMax;
        }
        else
        {
            hp += healing;
        }
        actualizarGuardadoInventory();
        actualizarUI.Raise();
    }

    public void getHurt(int damage, float fuerza)
    {
        if (state != States.YEET)
        {
            if (hp - damage <= 0)
            {
                hp = 0;
                muerte();
            }
            else
            {
                hp -= damage;
            }
            actualizarGuardadoInventory();
            actualizarUI.Raise();

            if (state != States.death)
            {
                corrutinaYEETActual = yeet(fuerza);
                StartCoroutine(corrutinaYEETActual);
            }
        }
        

    }
    IEnumerator yeet(float force)
    {
        CambiarEstado(States.YEET);
        rigidBody.velocity = Vector3.zero;
        rigidBody.AddForce(this.transform.up * force + this.transform.forward * -force/2, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        CambiarEstado(States.Caminar);
    }

    public void muerte()
    {
        this.gameObject.layer = LayerMask.NameToLayer("IgnoreEnemies");
        muertePlayer.Raise();
        CambiarEstado(States.death);
    }

    IEnumerator Salto()
    {
        //dependiendo de si es el salto des del suelo o un doble salto hara animacion diferente (de momento no xd)
        CambiarEstado(States.Saltar);
        this.GetComponent<AudioSource>().PlayOneShot(audios[1]);
        if (!isGrounded)
        {
            numDoubleJumpsReal--;
        }
        rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0, rigidBody.velocity.z);
        rigidBody.AddForce(this.transform.up * jumpForce, ForceMode.Impulse);
        yield return new WaitForSeconds(0.3f);
        CambiarEstado(States.Caminar);
    }

    IEnumerator Dash()
    {
        
        this.GetComponent<AudioSource>().PlayOneShot(audios[2]);
        CambiarEstado(States.Dash);
        if (!isGrounded)
        {
            numDashesReal--;
        }
        if (canDamageDash)
        {
            isAttacking = true;
            this.gameObject.layer = LayerMask.NameToLayer("IgnoreEnemies");
            this.attackHitbox.gameObject.SetActive(true);
            ponerMaterial();
        }
        rigidBody.velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * dashForce, ForceMode.Impulse);
        yield return new WaitForSeconds(0.5f);
        if (canDamageDash)
        {
            isAttacking = false;
            this.attackHitbox.gameObject.SetActive(false);
            this.gameObject.layer = LayerMask.NameToLayer("Player");
            ponerMaterialesDefault();
        }
        CambiarEstado(States.Caminar);
    }

    public void ponerMaterialesDefault()
    {
        //cabeza
        int numOfChildren = cabezaGato.transform.childCount;
        for (int i = 0; i < numOfChildren; i++)
        {
            GameObject child = cabezaGato.transform.GetChild(i).gameObject;
            child.GetComponent<MeshRenderer>().material = materialCabeza;
        }
        //cuerpo
        cuerpo.GetComponent<SkinnedMeshRenderer>().material = materialCuerpo;
    }

    public void ponerMaterial()
    {
        //cabeza
        int numOfChildren = cabezaGato.transform.childCount;
        for (int i = 0; i < numOfChildren; i++)
        {
            GameObject child = cabezaGato.transform.GetChild(i).gameObject;
            child.GetComponent<MeshRenderer>().material = materialDamageDashCabeza;
        }
        //cuerpo
        cuerpo.GetComponent<SkinnedMeshRenderer>().material = materialDamageDash;
    }

    private void wallClimb(RaycastHit hit)
    {
        Debug.DrawLine(hit.point, hit.point - (hit.normal * 5), Color.red);
        this.transform.forward = -(hit.normal);

        //que pare el salto
        if (corrutinaSaltoActual != null)
        {
            StopCoroutine(corrutinaSaltoActual);
        }
        CambiarEstado(States.WallClimb);

        rigidBody.velocity = new Vector3(0, wallClimbSpeed, 0);

    }

    //llamar a estas al tocar el suelo y si hacemos algo raro en combate que resetee al golpear en la cabeza a un enemigo o algo asi idk
    private void resetDoubleJumps()
    {
        numDoubleJumpsReal = numDoubleJumps;
    }
    private void resetDashes()
    {
        numDashesReal = numDashes;
    }

    IEnumerator Dance()
    {
        this.GetComponent<AudioSource>().Play(0);
        animator.Play("DanceMamadisimo");
        yield return new WaitForSeconds(6.7f);
        CambiarEstado(States.Idle);
    }

    IEnumerator Land()
    {
        this.GetComponent<AudioSource>().PlayOneShot(audios[5]);
        animator.Play("Land");
        yield return new WaitForSeconds(0.3f);
        CambiarEstado(States.Caminar);
    }

    public void resetInventory()
    {
        playerInventory.hpMax = statsBase.hpMax;
        playerInventory.hp = statsBase.hp;
        playerInventory.damage = statsBase.damage;
        playerInventory.velocidad = statsBase.velocidad;
        playerInventory.jumpForce = statsBase.jumpForce;
        playerInventory.canDoubleJump = statsBase.canDoubleJump;
        playerInventory.numDoubleJumps = statsBase.numDoubleJumps;
        playerInventory.canDash = statsBase.canDash;
        playerInventory.numDashes = statsBase.numDashes;
        playerInventory.dashForce = statsBase.dashForce;
        playerInventory.canDamageDash = statsBase.canDamageDash;
        playerInventory.canWallClimb = statsBase.canWallClimb;
        playerInventory.wallClimbSpeed = statsBase.wallClimbSpeed;
        playerInventory.canHookIr = statsBase.canHookIr;
        playerInventory.canHookAtraer = statsBase.canHookAtraer;
        playerInventory.canCoinMagnet = statsBase.canCoinMagnet;
        playerInventory.money = 0;
        playerInventory.misArmas = statsBase.misArmas;
        inicializarInventory();
    }

    public void getMoney(int money)
    {
        this.playerInventory.money += money;
        actualizarUI.Raise();
    }

    public void getAbility(habilidadesTienda habilidad)
    {
        switch (habilidad)
        {
            case habilidadesTienda.Dash:
                playerInventory.canDash = true;
                break;
            case habilidadesTienda.DoubleJump:
                playerInventory.canDoubleJump = true;
                break;
            case habilidadesTienda.WallClimb:
                playerInventory.canWallClimb = true;
                break;
            case habilidadesTienda.Hook:
                playerInventory.canHookIr = true;
                break;
            case habilidadesTienda.Healing:
                getHealed(20);
                break;
        }
        actualizarInventory();
    }

    public void getArma(Arma arma)
    {
        //recarga su municion
        arma.PickUp();
        //a�ade el arma al inventario si no esta, si esta le recarga la ammo y ya
        if (!armas.Contains(arma))
        {
            armas.Add(arma);
        }
        actualizarGuardadoInventory();
    }

    public void switchArma()
    {
        if (armas.Count > 0)
        {
            if (indexArmas == armas.Count-1)
            {
                indexArmas = -1;
            }
            else
            {
                indexArmas++;
            }

            if (indexArmas==-1)
            {
                currentArma = null;
                espada.SetActive(true);
                for (int i = 0; i < listaModelosArmaParaLaMano.Count; i++)
                {
                    listaModelosArmaParaLaMano[i].SetActive(false);
                }
            }
            else
            {
                currentArma = armas[indexArmas];
                espada.SetActive(false);
                //PRUEBAS, esto va a dar asquete
                int cosaAActivar = 0;

                for (int i =0; i<listaModelosArmaParaLaMano.Count; i++)
                {
                    if (currentArma.modelo.name == listaModelosArmaParaLaMano[i].name)
                    {
                        cosaAActivar = i;
                    }
                    listaModelosArmaParaLaMano[i].SetActive(false);
                }

                listaModelosArmaParaLaMano[cosaAActivar].SetActive(true);
            }

        }
        else
        {
            indexArmas = -1;
            currentArma = null;
            espada.SetActive(true);
            for (int i = 0; i < listaModelosArmaParaLaMano.Count; i++)
            {
                listaModelosArmaParaLaMano[i].SetActive(false);
            }
        }
        actualizarUI.Raise();
    }

    public void shootArma()
    {
        isShooting = true;
        for (int i = 0; i< currentArma.numProjectiles; i++)
        {
            GameObject newProjectile = Instantiate(currentArma.projectile);
            newProjectile.transform.position = this.transform.position;
            newProjectile.GetComponent<hitboxAtaque>().player = this;
            if (enemigoFijado != null)
            {
                newProjectile.GetComponent<Rigidbody>().AddForce((enemigoFijado.transform.position-this.transform.position) * 3, ForceMode.Impulse);
            }
            else
            {
                newProjectile.GetComponent<Rigidbody>().AddForce(this.transform.forward * currentArma.projectileSpeed, ForceMode.Impulse);
            }
        }
        currentArma.ammo--;
        if (currentArma.ammo <= 0)
        {
            armas.Remove(currentArma);
            indexArmas = -1;
            switchArma();
            actualizarGuardadoInventory();
        }
        actualizarUI.Raise();
        isShooting = false;
    }



    //el trigger lo hace el script del hijo (el collider de ataque concretamente)

    private void OnDestroy()
    {
        inputs.player.attack.performed -= MouseButton0Down;
        inputs.player.fijarCamara.performed -= MouseButton1Down;
        inputs.player.dash.performed -= LeftShiftDown;
        inputs.player.saltoYWallclimb.canceled -= SpaceBarUp;
        inputs.player.saltoYWallclimb.performed -= SpaceBarDown;
        inputs.player.hookIr.canceled -= EUp;
        inputs.player.hookAtraer.canceled -= QUp;
        inputs.player.changeWeapon.performed -= RDown;
        inputs.player.Dance.performed -= VDown;
    }

}
