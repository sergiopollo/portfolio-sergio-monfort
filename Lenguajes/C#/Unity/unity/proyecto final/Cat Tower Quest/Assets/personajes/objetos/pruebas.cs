using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pruebas : MonoBehaviour
{
    //cosas habilidades
    public GameObject objeto;

    public habilidadSO[] habilidadesQuePondre;

    public Arma[] armas;

    //cosas tienda y player
    public tienda tienda;
    public MovimientoJugador player;
    public GameEvent actualizarUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            int random = Random.Range(0,10);
            int random2 = Random.Range(0, habilidadesQuePondre.Length);

            GameObject newHabilidad = Instantiate(objeto);
            newHabilidad.transform.position = new Vector3(random, 10, random);
            newHabilidad.GetComponent<Objeto>().cambiarHabilidad(habilidadesQuePondre[random2]);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            int random = Random.Range(0, 10);
            int random2 = Random.Range(0, armas.Length);

            GameObject newArma = Instantiate(objeto);
            newArma.transform.position = new Vector3(random, 10, random);
            newArma.GetComponent<Objeto>().cambiarArma(armas[random2]);
        }

        //cheats
        if (Input.GetKeyDown(KeyCode.O))
        {
            player.playerInventory.money += 9999;
            tienda.OnMoneyChanged();
            actualizarUI.Raise();
        }

        //reset tienda y player
        if (Input.GetKeyDown(KeyCode.Y))
        {
            tienda.resetTienda();
            player.resetInventory();
        }
    }
}
