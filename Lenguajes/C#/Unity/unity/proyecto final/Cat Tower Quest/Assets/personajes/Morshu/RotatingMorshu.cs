using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingMorshu : MonoBehaviour
{

    int rotationDirection;
    float colorHue = 0f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Epilepsia");
        rotationDirection = Random.Range(0,2);
        if (rotationDirection == 0)
            rotationDirection = -1;
        StartCoroutine("Rotar");
    }

    IEnumerator Epilepsia()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);           
            this.transform.GetChild(3).GetComponent<Light>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f); ;
        }
    }

    IEnumerator Rotar()
    {
        while (true)
        {
            this.transform.Rotate(0,rotationDirection*Random.Range(2, 6),0);
            yield return new WaitForSeconds(0.05f);
        }
    }

}
