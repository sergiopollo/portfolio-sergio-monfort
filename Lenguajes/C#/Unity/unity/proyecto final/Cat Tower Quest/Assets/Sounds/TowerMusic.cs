using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerMusic : MonoBehaviour
{
    public AudioSource mySource;
    public AudioClip introT;
    public AudioClip loopT;
    public AudioClip introF;
    public AudioClip loopF;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Tower");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartTowerMusic()
    {
        StartCoroutine("Tower");
    }
    public void StartFightMusic()
    {
        StartCoroutine("Fight");
    }
    IEnumerator Fight()
    {
        for(int i = 0; i < 8; i++)
        {
            mySource.volume = mySource.volume - 0.03f;
            yield return new WaitForSeconds(0.2f);
        }        
        StopCoroutine("Tower");
        mySource.loop = false;
        mySource.clip = introF;
        mySource.Play();
        for (int i = 0; i < 8; i++)
        {
            mySource.volume = mySource.volume + 0.03f;
            yield return new WaitForSeconds(0.2f);
        }
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            if (!mySource.isPlaying)
            {
                mySource.clip = loopF;
                mySource.loop = true;
                mySource.Play();
            }
        }
        
    }

    IEnumerator Tower()
    {
        for (int i = 0; i < 8; i++)
        {
            mySource.volume = mySource.volume - 0.03f;
            yield return new WaitForSeconds(0.2f);
        }
        StopCoroutine("Fight");
        mySource.loop = false;
        mySource.clip = introT;
        mySource.Play();
        for (int i = 0; i < 8; i++)
        {
            mySource.volume = mySource.volume + 0.03f;
            yield return new WaitForSeconds(0.2f);
        }
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            if (!mySource.isPlaying)
            {
                mySource.clip = loopT;
                mySource.loop = true;
                mySource.Play();
            }
        }
    }
}
