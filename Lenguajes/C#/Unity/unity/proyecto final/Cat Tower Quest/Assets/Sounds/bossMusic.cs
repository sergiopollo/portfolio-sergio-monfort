using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossMusic : MonoBehaviour
{
    public AudioSource mySource;
    public AudioClip intro;
    public AudioClip loop;
    public AudioClip party;
    public AudioClip normalWin;


    // Start is called before the first frame update
    void Start()
    {
        mySource.clip = intro;
        mySource.Play();
    }

    public void startParty()
    {
        mySource.clip = party;
        mySource.loop = true;
        mySource.Play();
    }

    public void win()
    {
        mySource.clip = normalWin;
        mySource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!mySource.isPlaying)
        {
            mySource.clip = loop;
            mySource.loop = true;
            mySource.Play();
        }
    }
}
