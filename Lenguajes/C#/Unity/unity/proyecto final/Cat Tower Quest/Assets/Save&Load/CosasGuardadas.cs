using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PartidaGuardada", order = 1)]
public class CosasGuardadas : ScriptableObject
{
    public List<mejora> mejoras;
    public PlayerInventory playerInventory;
   // public int dinero = 0;
  
}
