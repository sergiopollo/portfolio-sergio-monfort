using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using OdinSerializer;



public class SaveBueno : MonoBehaviour
{
    public List<mejora> mejoras;
    public tienda tienda;
    public CosasGuardadas partidaGuardada;
    [OdinSerialize]
    public CosasGuardadas partidaNueva;
    public PlayerInventory playerInventorySaved;
    int numMejoras;
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.Z))
        {
            guardarPartida();
        }else if (Input.GetKeyDown(KeyCode.X))
        {
            cargarPartida();
        }*/
    }

    public void guardarPartida()
    {
        listas();
        partidaGuardada.playerInventory = playerInventorySaved;
        byte[] serializedData = SerializationUtility.SerializeValue<CosasGuardadas>(partidaGuardada, DataFormat.JSON);
        File.WriteAllBytes("Save.json", serializedData);

        Debug.Log("partida guardada con exito");
}

public bool cargarPartida()
{
        if (File.Exists("Save.json"))
        {
            listas();
            byte[] postFile = File.ReadAllBytes("Save.json");   
            partidaGuardada = SerializationUtility.DeserializeValue<CosasGuardadas>(postFile, DataFormat.JSON);
            print(mejoras.Count);
            for (int i = 0; i <= mejoras.Count - 1; i++)
            {
               mejoras[i].estado = partidaGuardada.mejoras[i].estado;
            }     
            this.playerInventorySaved.money = partidaGuardada.playerInventory.money;
            this.playerInventorySaved.hpMax = partidaGuardada.playerInventory.hpMax;
            this.playerInventorySaved.hp = partidaGuardada.playerInventory.hpMax;
            this.playerInventorySaved.jumpForce = partidaGuardada.playerInventory.jumpForce;
            this.playerInventorySaved.numDashes = partidaGuardada.playerInventory.numDashes;
            this.playerInventorySaved.numDoubleJumps = partidaGuardada.playerInventory.numDoubleJumps;
            this.playerInventorySaved.velocidad = partidaGuardada.playerInventory.velocidad;
            this.playerInventorySaved.wallClimbSpeed = partidaGuardada.playerInventory.wallClimbSpeed;
            this.playerInventorySaved.dashForce = partidaGuardada.playerInventory.dashForce;
            this.playerInventorySaved.damage = partidaGuardada.playerInventory.damage;
            this.playerInventorySaved.canWallClimb = partidaGuardada.playerInventory.canWallClimb;
            this.playerInventorySaved.canHookIr = partidaGuardada.playerInventory.canHookIr;
            this.playerInventorySaved.canHookAtraer = partidaGuardada.playerInventory.canHookAtraer;
            this.playerInventorySaved.canDoubleJump = partidaGuardada.playerInventory.canDoubleJump;
            this.playerInventorySaved.canDash = partidaGuardada.playerInventory.canDash;
            this.playerInventorySaved.canDamageDash = partidaGuardada.playerInventory.canDamageDash;
            this.playerInventorySaved.canCoinMagnet = partidaGuardada.playerInventory.canCoinMagnet;
            this.playerInventorySaved.misArmas = partidaGuardada.playerInventory.misArmas;

            return true;
        }
        else
        {
            return false;
        }

     
}

    public void listas()
    {
        partidaGuardada.mejoras.Clear();
        mejoras.Clear();
        for (int i = 0; i <= tienda.mejorasDamage.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasDamage[i]);
            mejoras.Add(tienda.mejorasDamage[i]);
        }
        for (int i = 0; i <= tienda.mejorasDash.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasDash[i]);
            mejoras.Add(tienda.mejorasDash[i]);
        }
        for (int i = 0; i <= tienda.mejorasDoubleJump.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasDoubleJump[i]);
            mejoras.Add(tienda.mejorasDoubleJump[i]);
        }
        for (int i = 0; i <= tienda.mejorasHook.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasHook[i]);
            mejoras.Add(tienda.mejorasHook[i]);
        }
        for (int i = 0; i <= tienda.mejorasMaxHp.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasMaxHp[i]);
            mejoras.Add(tienda.mejorasMaxHp[i]);
        }
        for (int i = 0; i <= tienda.mejorasMoney.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasMoney[i]);
            mejoras.Add(tienda.mejorasMoney[i]);
        }
        for (int i = 0; i <= tienda.mejorasSpeed.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasSpeed[i]);
            mejoras.Add(tienda.mejorasSpeed[i]);
        }
        for (int i = 0; i <= tienda.mejorasWallClimb.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.mejorasWallClimb[i]);
            mejoras.Add(tienda.mejorasWallClimb[i]);
        }
        for (int i = 0; i <= tienda.habilidades.Count - 1; i++)
        {
            partidaGuardada.mejoras.Add(tienda.habilidades[i]);
            mejoras.Add(tienda.habilidades[i]);
        }

    }

}
