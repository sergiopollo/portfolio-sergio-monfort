using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheEndOfAll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void theEnd()
    {
        StartCoroutine("TheEndOfAllColors");
    }

    IEnumerator TheEndOfAllColors()
    {
        this.GetComponent<Light>().enabled = true;
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1, 3));

            Color c = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

            this.GetComponent<MeshRenderer>().material.color = c;
            this.GetComponent<Light>().color = c;
        }

    }


}
