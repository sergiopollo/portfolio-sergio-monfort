using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Luces : MonoBehaviour
{

    public GameObject[] l;

    public void lucesStart()
    {
        StartCoroutine("Colorear");
    }

    IEnumerator Colorear()
    {

        l[Random.Range(0, l.Length)].SetActive(true);
        l[Random.Range(0, l.Length)].SetActive(true);
        l[Random.Range(0, l.Length)].SetActive(true);
        l[Random.Range(0, l.Length)].SetActive(true);

        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            l[Random.Range(0, l.Length)].SetActive(false);
            l[Random.Range(0, l.Length)].SetActive(true);
            l[Random.Range(0, l.Length)].GetComponent<Light>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        }
    }

}
