using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SueloEnd : MonoBehaviour
{
    public Material mat;
    public void End()
    {
        this.GetComponent<MeshRenderer>().material = mat;
        this.GetComponent<MeshRenderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f, 0.7f, 0.7f);
        StartCoroutine("color");
    }

    IEnumerator color()
    {
        while(true){
            yield return new WaitForSeconds(3f);
            this.GetComponent<MeshRenderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f, 0.7f, 0.7f);
        }
    }
}
