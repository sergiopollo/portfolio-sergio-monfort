﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class MonopolyContext : DbContext
    {
        //nombro la base de datos
        public MonopolyContext() : base("monopoly")
        {

        }

        //esto son como los DAOs (POCOS -> plain old c# object) 
        public DbSet<Jugadores> Jugadores { get; set; }

        public DbSet<Propiedades> Propiedades { get; set; }

        //esto crea la base de datos
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists); //update, pero crea la DB!
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ProvaContext>());  //aquest seria un metode update pero si canvies el model (els POCOS) reinicia la DB
            Database.SetInitializer(new DropCreateDatabaseAlways<MonopolyContext>());//equivaldria a un create
            base.OnModelCreating(modelBuilder);
        }
    }
}
