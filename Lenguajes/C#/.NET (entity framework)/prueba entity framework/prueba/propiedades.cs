﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class Propiedades
    {
        public int PropiedadesID { get; set; }
        public string nombre { get; set; }
        public int precio { get; set; }
        public int numCasas { get; set; }
        public int precioCasa { get; set; }
        public int posicion { get; set; }
        public bool hipotecado { get; set; }
        public int precioHipoteca { get; set; }
        public int alquiler0 { get; set; }
        public int alquiler1 { get; set; }
        public int alquiler2 { get; set; }
        public int alquiler3 { get; set; }
        public int alquiler4 { get; set; }
        public int alquiler5 { get; set; }

        public Jugadores Jugadores { get; set; }
        public Colores Colores { get; set; }

    }
}
