﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class Colores
    {

        public int ColoresID { get; set; }
        public String nombre { get; set; }

        public ICollection<Propiedades> Propiedadess { get; set; }
        public Jugadores Jugadores { get; set; }

        public Colores()
        {
            Propiedadess = new HashSet<Propiedades>();
        }
    }
}
