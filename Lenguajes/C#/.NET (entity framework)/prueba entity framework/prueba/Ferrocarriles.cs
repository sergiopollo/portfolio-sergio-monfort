﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class Ferrocarriles : Propiedades
    {

        public ICollection<Ferrocarriles> misVecinos { get; set; }
        public ICollection<Ferrocarriles> vecinoDe { get; set; }

        public Ferrocarriles()
        {
            misVecinos = new HashSet<Ferrocarriles>();
            vecinoDe = new HashSet<Ferrocarriles>();
        }


    }
}
