﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class Jugadores
    {
        public int JugadoresID { get; set; }
        public string nombre { get; set; }
        public bool activo { get; set; }
        public int turno { get; set; }
        public int dinero { get; set; }
        public int numPropiedades { get; set; }
        public int numVictorias { get; set; }
        public int posicionTablero { get; set; }

        public ICollection<Propiedades> Propiedadess { get; set; }
        public ICollection<Colores> Colores { get; set; }

        public Jugadores()
        {
            Propiedadess = new HashSet<Propiedades>();
        }

    }
}
