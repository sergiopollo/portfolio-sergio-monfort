﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prueba
{
    class Program
    {
        static MonopolyContext ctx = new MonopolyContext();

        static void Main(string[] args)
        {

            using (ctx)
            {


                Jugadores j1 = new Jugadores() { nombre = "J1", activo = true, dinero = 1500, posicionTablero = 0, numPropiedades = 0, turno = 0 };

                Jugadores j2 = new Jugadores() { nombre = "J2", activo = true, dinero = 1500, posicionTablero = 0, numPropiedades = 0, turno = 1 };

                Colores marron = new Colores() { nombre = "marron" };

               Propiedades p1 = new Propiedades() { nombre = "calle_ASIX", Colores = marron, posicion = 1, precio = 40, hipotecado = false, numCasas = 0, precioCasa = 55, precioHipoteca = 30, alquiler0 = 5, alquiler1 =  12, alquiler2 = 35, alquiler3 = 100, alquiler4 = 170, alquiler5 = 270};
               Propiedades p2 = new Propiedades() { nombre = "calle_SMX", Colores = marron, posicion = 2, precio = 30, hipotecado = false, numCasas = 0, precioCasa = 50, precioHipoteca = 20, alquiler0 = 2, alquiler1 = 10, alquiler2 = 30, alquiler3 = 90, alquiler4 = 160, alquiler5 = 250};
               Propiedades p3 = new Propiedades() { nombre = "calle_Damvi", Colores = marron, posicion = 3, precio = 50, hipotecado = false, numCasas = 0, precioCasa = 75, precioHipoteca = 35, alquiler0 = 10, alquiler1 = 27, alquiler2 = 45, alquiler3 = 110, alquiler4 = 195, alquiler5 = 290 };

                Ferrocarriles f1 = new Ferrocarriles() { nombre = "Poble Nou", posicion = 4, alquiler0 = 200, alquiler1 = 350};
                Ferrocarriles f2 = new Ferrocarriles() { nombre = "Poble Sec", posicion = 5, alquiler0 = 200, alquiler1 = 350};

                f1.vecinoDe.Add(f2);
                f1.misVecinos.Add(f2);

                f2.vecinoDe.Add(f1);
                f2.misVecinos.Add(f1);

                j1.Propiedadess.Add(p1);


                ctx.Jugadores.Add(j1);
                ctx.Jugadores.Add(j2);
                ctx.Propiedades.Add(p1);
                ctx.Propiedades.Add(p2);
                ctx.Propiedades.Add(p3);
                ctx.Propiedades.Add(f1);
                ctx.Propiedades.Add(f2);

                ctx.SaveChanges();

                Console.WriteLine("no ha petado! :D");

                //pruebas funciones
                modificarDiners(j1, 1000);

                Comprar(j1, p1);

                pagarLloguer(j2, p1);

                edificar(j1, p1);
                edificar(j1, p2);

                hipotecar(j1, p1);
                Console.ReadLine();

                deshipotecar(j1, p1);

                Comprar(j1, p2);

                Console.WriteLine("el propietario de p1 tiene todas las de mi color? " + ComprovarColor(p1));

                Comprar(j1, p3);

                Console.WriteLine("el propietario de p1 tiene todas las de mi color? "+ComprovarColor(p1));

                Console.WriteLine("propietario de p1: "+ComprovarPropietari(p1));

                Console.WriteLine(ferrocarrilsVeins(f1).ToList());

                Console.WriteLine(isFerrocarrilVei(f1, f2));

                ctx.SaveChanges();
                //para que no se quite de la consola hasta que pulsemos una tecla
                Console.ReadLine();
              
                Console.ReadLine();

                /*
                //contar
                Console.WriteLine(ctx.Professor.Count());


                //select
                Professor test1 = ctx.Professor
                                    .Where(p => p.cognom == "Albareda")
                                    .FirstOrDefault();
                Console.WriteLine(test1.cognom);

                //find per clau primeria
                Professor test2 = ctx.Professor.Find(2);
                Console.WriteLine(test2.cognom);

                Console.ReadLine();


                //update
                test1.nom = "GregMarc";
                //no cal "guardar l'update", EF ho assumeix auto
                ctx.SaveChanges();



                //all
                bool flag = ctx.Professor.All(p => p.nom.StartsWith("G"));
                Console.WriteLine(flag);

                //delete
                ctx.Professor.Remove(test2);
                ctx.SaveChanges();
                */

            }
        }

        //funciones

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        //sergio: 
        //iker 
        //pau b/Comprar e/Edificar

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /*
    public boolean Comprar(Jugadores j, Propiedades p)
    {

        PropiedadesDao PD = new PropiedadesDao();
        ColoresDao CD = new ColoresDao();

        if (p.getPropietario() == null && j.getDinero() > p.getPrecio())
        {

            //esto no es la verdad absoluta, solo una base que si funciona hay que acortar y tal
            if (p instanceof Ferrocarriles) { // o que su color sea null porque como no tiene pues eso
                                              // lo de añadirselo al jugador y tal
                j.addPropiedad(p);
                this.modificarDiners(j, -p.getPrecio());
                j.setNumPropiedades(j.getNumPropiedades() + 1);
                p.setPropietario(j);

                // miramos cuantos ferrocarriles tiene el jugador despues de comprarlo
                int contadorFerrocarrilesJugador = 0;
                for (Propiedades propiedad : j.getPropiedades())
                {
                    if (propiedad instanceof Ferrocarriles) {
                    contadorFerrocarrilesJugador++;
                }
            }
            //segun los ferrocarriles que maneje se le añadiran en la columna de numCasas(para no hacer otra)
            //tambien los guardo porque no voy a ahcer otro bucle y tal
            for (Propiedades propiedad : j.getPropiedades())
            {
                if (propiedad instanceof Ferrocarriles) {
                propiedad.setNumCasas(contadorFerrocarrilesJugador);
                PD.saveOrUpdate(propiedad);
            }
        }

    
        else {
            j.addPropiedad(p);
            this.modificarDiners(j, -p.getPrecio());
            j.setNumPropiedades(j.getNumPropiedades() + 1);
            p.setPropietario(j);

            if(CD.ComprovarColor(p)){
                p.getColor().setPropietarioColor(j);
                CD.saveOrUpdate(p.getColor());
            }
        PD.saveOrUpdate(p); 
        }


        this.saveOrUpdate(j);
        return true;

    }
    return false;


}
*/

        public static bool Comprar(Jugadores j, Propiedades p)
        {

            if (p.Jugadores == null && j.dinero > p.precio)
            {
                int contadorFerrocarrilesJugador = 0;
                if (p.GetType() == typeof(Ferrocarriles))
                {
                    j.Propiedadess.Add(p);
                    modificarDiners(j, -p.precio);
                    j.numPropiedades++;
                    p.Jugadores = j;

                   

                    foreach (Propiedades propiedad in j.Propiedadess)
                    {
                        if (propiedad.GetType() == typeof(Ferrocarriles))
                        {
                            contadorFerrocarrilesJugador++;
                        }
                    }
                    foreach (Propiedades propiedad in j.Propiedadess)
                    {
                        if (propiedad.GetType() == typeof(Ferrocarriles))
                        {
                            propiedad.numCasas = contadorFerrocarrilesJugador;
                        }
                    }
                }
 
                else
                {
                    j.Propiedadess.Add(p);
                    modificarDiners(j, -p.precio);
                    j.numPropiedades++;
                    p.Jugadores = j;
                    if (ComprovarColor(p))
                    {
                        Colores colores = p.Colores;
                        //Console.WriteLine("He llegado aqui");
                        colores.Jugadores = j;
                    }
                }
                //guardarlo
                ctx.SaveChanges();
                return true;
            }

            return false;
        }

        public static bool pagarLloguer(Jugadores j, Propiedades p)
            {

                //si no tiene propietario devuelve false, aunque si no tiene propietario no deberia llamarse a esta funcion
                int casas = p.numCasas;
                int dineroAPagar = 0;

                switch (casas)
                {
                    case 0:
                    dineroAPagar = p.alquiler0;
                        break;
                    case 1:
                        dineroAPagar = p.alquiler1;
                        break;
                    case 2:
                        dineroAPagar = p.alquiler2;
                        break;
                    case 3:
                        dineroAPagar = p.alquiler3;
                        break;
                    case 4:
                        dineroAPagar = p.alquiler4;
                        break;
                    case 5:
                        dineroAPagar = p.alquiler5;
                        break;

                }

                Jugadores propietario = p.Jugadores;
                if (propietario == null)
                {
                    return false;
                }
                else
                {
                    // si el jugador no tiene dinero suficiente para pagar el alquiler se queda con
                    // deuda
                    modificarDiners(j, dineroAPagar * -1);
                    modificarDiners(propietario, dineroAPagar);
                    //no hacen falta saveorupdate porque modificardiners ya hace el saveorupdate
                    //this.saveOrUpdate(propietario);
                    //this.saveOrUpdate(j);
                    return true;
                }

            }
       
        
        public static int modificarDiners(Jugadores j, int dinero)
        {

            j.dinero +=dinero;
            ctx.SaveChanges();
            return j.dinero;
        }

        

        public static Jugadores Reassignar()
        {

            List<Jugadores> jugadores = ctx.Jugadores.ToList();

            for (int i = jugadores.Count - 1; i >= 0; i--)
            {
                if (jugadores[i].dinero <= 0)
                {
                    Console.WriteLine("jugador " + jugadores[i].nombre + " eliminado");
                    jugadores.Remove(jugadores[i]);
                    ctx.Jugadores.Remove(jugadores[i]);


                   ctx.SaveChanges();

                }
            }
            //System.out.println("//////////////////////");
            //System.out.println(jugadores);

            if (jugadores.Count == 1)
            {
                Console.WriteLine("Fin de partida, gana: " + jugadores[0].nombre);
                return jugadores[0];
            }


            return null;
        }
        
            public static int edificar(Jugadores j, Propiedades p)
            {
                
                if (p.Jugadores == j && p.precioCasa < j.dinero)
                {
                    if (ComprovarColor(p))
                    {
                        p.numCasas++;
                        modificarDiners(j, p.precioCasa * -1);
                        ctx.SaveChanges();
                    }
                }

                // lo return siempre 
                return p.numCasas;

            }

        
        

        
            public static bool hipotecar(Jugadores j, Propiedades p)
            {

                if (p.Jugadores == j)
                {
                    if (p.hipotecado)
                    {
                        return false;
                    }
                    else
                    {
                        p.hipotecado = true;
                        int dineroAPagar = p.precioHipoteca;
                        //no hace falta saveorupdate de jugador porque modificarDiners ya lo hace
                        modificarDiners(j, dineroAPagar);
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
        


            public static bool deshipotecar(Jugadores j, Propiedades p)
            {
                if (j.dinero < p.precioHipoteca || p.Jugadores != j || !p.hipotecado)
                {
                    return false;
                }
                else
                {
                    p.hipotecado = false;
                    modificarDiners(j, p.precioHipoteca * -1);
                return true;
                } 
            }
        



            public static bool ComprovarColor(Propiedades p)
            {
                    Colores c = p.Colores;
            if (c == null)
            {
                return false;
            }
            Jugadores j = p.Jugadores;
            bool flag = true;
            foreach (Propiedades prop in c.Propiedadess)
            {
                if (prop.Jugadores == null || prop.Jugadores != j)
                {
                    flag = false;
                }
            }
            return flag;
            }   

	        
            
        
            public static Jugadores ComprovarPropietari(Propiedades p)
            {
                Jugadores j = p.Jugadores;
                return j;
            }
        
        
       
            public static ICollection<Ferrocarriles> ferrocarrilsVeins(Ferrocarriles f)
            {

                return f.misVecinos;
            }

        
            public static bool isFerrocarrilVei(Ferrocarriles f1, Ferrocarriles f2)
            {
                if (f1.misVecinos.Contains(f2))
                {
                    return true;
                }
                return false;
            }
        








    }
}
