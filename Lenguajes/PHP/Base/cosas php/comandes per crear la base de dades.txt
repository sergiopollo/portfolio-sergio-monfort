
create database PHP_A3;

use PHP_A3;

create table usuaris (id int NOT NULL AUTO_INCREMENT, name varchar(10), password varchar(10), isAdmin BOOLEAN, color varchar(20), puntuacion int, timer boolean, colorVida varchar(20), tamanyo int, posicion int, mirilla varchar(50), PRIMARY KEY (id));


INSERT INTO usuaris (name, password, isAdmin, color, puntuacion, timer, colorVida, tamanyo, posicion, mirilla) values("user1","super3",true, "royalblue", 100, true, "red", 200, 1, "images/crosshair.png");
INSERT INTO usuaris (name, password, isAdmin, color, puntuacion, timer, colorVida, tamanyo, posicion, mirilla) values("user2","super3",false, "green", 0, true, "red", 200, 1, "images/crosshair.png");
INSERT INTO usuaris (name, password, isAdmin, color, puntuacion, timer, colorVida, tamanyo, posicion, mirilla) values("user3","super3",false, "red", 50, true, "red", 200, 1, "images/crosshair.png");