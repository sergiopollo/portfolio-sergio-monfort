window.onload = function() {
    cargarBaseDatos();
};

var posicionObjetivo;
var colorBarraVida;
var tamanyoVariable;
var crosshairVariable;
var timerStyle;

function cargarBaseDatos(){


    $.ajax({
        method:"GET",
        url:"partidaLoad.php",
        dataType:"json",
        success:function(data){
            console.log(data);

            var json = data["0"]

            posicionObjetivo = json["posicion"];
            colorBarraVida = json["colorVida"];
            tamanyoVariable= json["tamanyo"];
            crosshairVariable= json["mirilla"];
            console.log("asdasd"+json["timer"]);

            if(json["timer"] == 1){
                timerStyle=true;
            }else{
                timerStyle=false;
            }

            var mirilla = document.getElementById("mirilla");

            mirilla.src = String(json["mirilla"]);


            var timer = document.getElementById("timer");
            if(timerStyle==true){
                timer.style.visibility = "visible";
            }else{
                timer.style.visibility = "hidden";
            }

            var barraVida = document.getElementById("barraVida");
            barraVida.style.backgroundColor = colorBarraVida;

            var gun = document.getElementById("gun")

                gun.style.height=tamanyoVariable+"px";
                gun.style.width=tamanyoVariable+"px";
                if(tamanyoVariable == 100){
                    gun.style.marginTop="100px";
                }

            var objetivoAnterior = document.getElementById("objetivo");

            if(objetivoAnterior != null){
                objetivoAnterior.remove();
            }

            var capsa = document.createElement("div");

            capsa.innerText="Current Objective: Survive";
            capsa.setAttribute("id", "objetivo");

            if(posicionObjetivo == 1){
                document.getElementsByClassName("2")[0].appendChild(capsa);
            }else if(posicionObjetivo == 2){
                document.getElementsByClassName("4")[0].appendChild(capsa);
            }else{
                document.getElementsByClassName("6")[0].appendChild(capsa);
            }

        },
        error: function (data){
            alert("bb"+data);
            console.log(data);
        }
    })
}

function saveDatabase(){


    $.ajax({
        method:"POST",
        url:"partidaSave.php",
        dataType:"json",
        data: {
            "posicion": posicionObjetivo,
            "color": colorBarraVida,
            "tamanyo":tamanyoVariable,
            "crosshair":crosshairVariable,
            "timerStyle":timerStyle
        },
        success:function(data){
            console.log(data);
        },
        error: function (data){
            alert("bb"+data);
            console.log(data);
        }
    })

    console.log(timerStyle);

}

function reestablecer(){

    posicionObjetivo = 1;
    colorBarraVida = "red";
    tamanyoVariable = 200;
    crosshairVariable = "images/crosshair.png";
    timerStyle = true;


    var mirilla = document.getElementById("mirilla");

    mirilla.src = String(crosshairVariable);


    var timer = document.getElementById("timer");
    if(timerStyle==true){
        timer.style.visibility = "visible";
    }else{
        timer.style.visibility = "hidden";
    }

    var barraVida = document.getElementById("barraVida");
    barraVida.style.backgroundColor = colorBarraVida;

    var gun = document.getElementById("gun")

    gun.style.height=tamanyoVariable+"px";
    gun.style.width=tamanyoVariable+"px";
    if(tamanyoVariable == 100){
        gun.style.marginTop="100px";
    }

    var objetivoAnterior = document.getElementById("objetivo");

    if(objetivoAnterior != null){
        objetivoAnterior.remove();
    }

    var capsa = document.createElement("div");

    capsa.innerText="Current Objective: Survive";
    capsa.setAttribute("id", "objetivo");

    if(posicionObjetivo == 1){
        document.getElementsByClassName("2")[0].appendChild(capsa);
    }else if(posicionObjetivo == 2){
        document.getElementsByClassName("4")[0].appendChild(capsa);
    }else{
        document.getElementsByClassName("6")[0].appendChild(capsa);
    }



}

function editar(){

    var hiddeables = document.getElementsByClassName("hidden");
    var body = document.body;
    //var body =

    for (var i = 0; i < hiddeables.length; i++) {
        if(hiddeables[i].style.visibility == "hidden"){
            body.style.backgroundImage="url('images/partida2.jpg')";
            hiddeables[i].style.visibility = "visible";
        }else{
            body.style.backgroundImage="url('images/partida.jpg')";
            hiddeables[i].style.visibility = "hidden";
        }

    }

}


function crosshair(tipo){

    var mirilla = document.getElementById("mirilla");

    //print(tipo);
    console.log(tipo);

    if(tipo == "verde"){
        mirilla.src = "images/crosshair.png";
        crosshairVariable = "images/crosshair.png";
    }else if(tipo == "rojo"){
        mirilla.src = "images/crosshair2.png";
        crosshairVariable = "images/crosshair2.png";
    }


}

function hide(){

    var timer = document.getElementById("timer");
    var checkbox = document.getElementById("checkbox");

    if(checkbox.checked == true){
        timer.style.visibility = "hidden";
        timerStyle = false;
    }else{
        timer.style.visibility = "visible";
        timerStyle = true;
    }

    console.log(timerStyle);

}

function barraVida(){

    var color = document.getElementById("html5colorpicker")
    var barraVida = document.getElementById("barraVida");
    barraVida.style.backgroundColor = color.value;
    colorBarraVida = color.value;
}


function tamanyo(){

    var gun = document.getElementById("gun")

    if(gun.style.height=="100px"){
        gun.style.height="200px";
        gun.style.width="200px";
        gun.style.marginTop="0px";
        tamanyoVariable = 200;
    }else{
        gun.style.height="100px";
        gun.style.width="100px";
        gun.style.marginTop="100px";
        tamanyoVariable = 100;
    }


}

function move(){

    var objetivoAnterior = document.getElementById("objetivo");

    if(objetivoAnterior != null){
        objetivoAnterior.remove();
    }

    var capsa = document.createElement("div");

    capsa.innerText="Current Objective: Survive";
    capsa.setAttribute("id", "objetivo");

    if(posicionObjetivo == 3){
        document.getElementsByClassName("2")[0].appendChild(capsa);
        posicionObjetivo=1;
    }else if(posicionObjetivo == 1){
        document.getElementsByClassName("4")[0].appendChild(capsa);
        posicionObjetivo=2;
    }else{
        document.getElementsByClassName("6")[0].appendChild(capsa);
        posicionObjetivo=3;
    }

}
