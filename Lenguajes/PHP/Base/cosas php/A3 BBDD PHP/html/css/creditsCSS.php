<?php

session_start();

$usuari=$_SESSION["name"];
$color = $_SESSION["color"];

header("Content-type: text/css");


?>


 body{
     background-color: <?=$color?>;
  background-repeat: no-repeat;
  background-size: cover;*/
  margin: 0;
  font-family: Staatliches, cursive;
    /*overflow: hidden;*/
}

.grid-container {
  display: grid;
  grid-template-columns: 40% 60%;
  grid-template-rows: 20% 80%;
  /*height: 100vh;*/
}
/*
#star1{
    position: relative;
     top: 10px;
    right: 140px;
}

#star2{
    position: relative;
     top: 10px;
    right: -595px;
} 
*/
.star{
    animation-name: brilloStar;
    animation-duration: 2s;
    animation-iteration-count: infinite;
    width: 10%;
    height: auto;
}
/*animacio que gira i canvia la opacitat de les estrelles*/
@keyframes brilloStar {
    0% {transform:rotate(0deg); opacity: 1;}
    25% {transform:rotate(90deg); opacity: 0.5;}
    50% {transform:rotate(180deg); opacity: 0.1;}
    75% {transform:rotate(90deg); opacity: 0.5;}
    100% {transform:rotate(0deg); opacity: 1;}
}

/* #titulo{
    font-size: 40px;
    color:green;
} */

#gun{
    position: fixed;
    z-index: 2;
    top: 40%;
    right: 20%;
    width: 50%;
    height: auto;
    transform:rotate(50deg);
    animation-name: girarGun;
    animation-duration: 4s;
    animation-iteration-count: infinite;
}
/*animacio que gira la ametralladora*/
@keyframes girarGun {
    0% {transform:rotate(50deg);}
    25% {transform:rotate(95deg);}
    50% {transform:rotate(50deg);}
    75% {transform:rotate(5deg);}
    100% {transform:rotate(50deg);}
}

#chapas{
    position: fixed;
    z-index: -1;
    width: 30%;
    height: auto;
    top: 10%
    /*bottom: 1000px;
    right: 20%;*/
}
#suro{
    position: fixed;
    z-index: -2;
    width: 90%;
    height: 90%;
    top: 10%;
    left: 5%;
    /*bottom: 1000px;
    right: 20%;*/
}

.flex-container{
  display:flex;
  
}

#titulo{
   font-size: 50px;
    margin-left: 30%; 
}

.texto{
    font-size: 35px;
    margin-right: 15%;
}


/*el grid esta dividit en header (titol) esquerra (botons) i dreta (imatges)*/
.header{ 
  grid-column-start: 1;
 grid-column-end: 3;
 grid-row-start: 1;
 grid-row-end: 1;
 padding-left: 20%;
}

.izquierda{
   grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
  padding-top: 10%;
  padding-left: 20%;
  padding-right: 30px;
  
}

.derecha{
   grid-column-start: 2;
  grid-column-end: 2;
  grid-row-start: 2;
  grid-row-end: 2;
    overflow-y: scroll;
  
}


input[type="button"]{
  background-color: orange;
  color: black;
  border-radius: 20px;
  border: 4px solid black;
  font-weight: bold;
    font-family: Staatliches, cursive;
  width: 60%;
  margin: 30px;
  /*block-size: 100px; */
  font-size: 50px;
    margin-top: 100%;
}
/*hover que canvia el color dels botons quan passes el ratoli per sobre*/
input[type="button"]:hover{
  background-color: #ff726f;
  }


@media screen and (max-width: 600px){
    .grid-container {
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 20% 80%;
  /*height: 100vh;*/
}
    .header{ 
  grid-column-start: 1;
 grid-column-end: 1;
 grid-row-start: 1;
 grid-row-end: 1;
 padding-left: 0%;
 /* align-items: stretch; */

}
    .izquierda{
        grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
        padding-top: 0%;
  padding-left: 20%;
    }
    
    .derecha{
   grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
  
}
    
#titulo{
   font-size: 50px;
    margin-left: 10%; 
     margin-right: 10%; 
}

input[type=button]{
    bottom:80%;
position: fixed;

}


.texto{
    font-size: 35px;
    margin-right: 10%;
    margin-left: 10%;
}
    
    #chapas{
    position: fixed;
    z-index: -1;
    width: 70%;
    height: auto;
    top: 10%
    /*bottom: 1000px;
    right: 20%;*/
}




}


