
<?php

session_start();

$usuari=$_SESSION["name"];
$color = $_SESSION["color"];
$isAdmin = $_SESSION["isAdmin"];

header("Content-type: text/css");

$state="hidden";
if($isAdmin=="1"){
$state="visible";
}



?>





body{
    background-color: <?=$color?>;
  /*background-image: url("images/taller.jpg");  
  background-repeat: no-repeat;
  background-size: cover;*/
  margin: 0;
  
  font-family: Staatliches, cursive;
}






.grid-container {
  display: grid;
  grid-template-columns: 40% 60%;
  grid-template-rows: 20% 80%;
  /*height: 100vh;*/
}

#admins{

    visibility: <?=$state?>;

}

.star{
    animation-name: brilloStar;
    animation-duration: 2s;
    animation-iteration-count: infinite;
    width: 10%;
    height: auto;
}
/*animacio que gira i canvia la opacitat de les estrelles*/
@keyframes brilloStar {
    0% {transform:rotate(0deg); opacity: 1;}
    25% {transform:rotate(90deg); opacity: 0.5;}
    50% {transform:rotate(180deg); opacity: 0.1;}
    75% {transform:rotate(90deg); opacity: 0.5;}
    100% {transform:rotate(0deg); opacity: 1;}
}
 #titulo{
    font-size: 90px;
} 

#gun{
    position: fixed;
    z-index: 2;
    top: 40%;
    right: 20%;
    width: 50%;
    height: auto;
    transform:rotate(50deg);
    animation-name: girarGun;
    animation-duration: 4s;
    animation-iteration-count: infinite;
}
/*animacio que gira la ametralladora*/
@keyframes girarGun {
    0% {transform:rotate(50deg);}
    25% {transform:rotate(95deg);}
    50% {transform:rotate(50deg);}
    75% {transform:rotate(5deg);}
    100% {transform:rotate(50deg);}
}

#morshu{
    position: fixed;
    z-index: 1;
    width: 70%;
    height: auto;
    top: 20%;
    right: 10%;
    /*bottom: 1000px;
    right: 20%;*/
}

#taller{
    position: relative;
    z-index: -3;
    width: 100%;
    height: auto;
    top: 0px;
    right: 200px;
}

.flex-container{
  display:flex;
  
}


/*el grid esta dividit en header (titol) esquerra (botons) i dreta (imatges)*/
.header{ 
  grid-column-start: 1;
 grid-column-end: 3;
 grid-row-start: 1;
 grid-row-end: 1;
 padding-left: 20%;
background-color: orange;
}

.izquierda{
   grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
  padding-top: 0%;
  padding-left: 20%;
  padding-right: 30px;
  
}

.derecha{
   grid-column-start: 2;
  grid-column-end: 2;
  grid-row-start: 2;
  grid-row-end: 2;
  
}


input[type="button"]{
  background-color: orange;
  color: black;
  border-radius: 20px;
  border: 4px solid black;
  font-weight: bold;
    font-family: Staatliches, cursive;
  width: 60%;
  margin: 10px;
  /*block-size: 100px; */
  font-size: 50px;
}
/*hover que canvia el color dels botons quan passes el ratoli per sobre*/
input[type="button"]:hover{
  background-color: #ff726f;
  }


@media screen and (max-width: 600px){
    .grid-container {
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 20% 80%;
  /*height: 100vh;*/
}
    .header{ 
  grid-column-start: 1;
 grid-column-end: 1;
 grid-row-start: 1;
 grid-row-end: 1;
 padding-left: 0%;
 /* align-items: stretch; */

}
    .izquierda{
        grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
        padding-top: 0%;
  padding-left: 20%;
        bottom: 100px;
    z-index: 2;
    }
    
    .derecha{
   grid-column-start: 1;
  grid-column-end: 1;
  grid-row-start: 2;
  grid-row-end: 2;
  
}
    
    #gun{
    position: relative;
    z-index: -3;
    top: 275px;
    right: -50px;
    width: 70%;
    height: auto;
        opacity: 40%;
    transform:rotate(50deg);
    animation-name: girarGun;
    animation-duration: 4s;
    animation-iteration-count: infinite;
}
#morshu{
    position: relative;
    z-index: -4;
    width: 100%;
    height: auto;
    opacity: 40%;
    top: 0px;
    right: 0px;
}
    
    #taller{
    position: relative;
    z-index: -5;
    width: 100%;
    height: auto;
    top: -250px;
    right: 0px;
}
    
     #titulo{
    font-size: 50px;
} 
    
}


