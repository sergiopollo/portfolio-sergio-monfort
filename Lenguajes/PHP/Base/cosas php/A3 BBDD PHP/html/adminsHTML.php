<?php

session_start();

$servername = "localhost";
$username="root";
$password="super3";
$BBDD="PHP_A3";

try{

    $conn = new PDO("mysql:host=$servername;dbname=$BBDD", $username, $password);


    //primero compruebo si el ususario esta ya
    $query = $conn->prepare("SELECT * FROM usuaris;");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    //print_r(json_encode($result));

}catch(PDOException $e) {
    print_r(json_encode("connection failed: ".$e->getMessage()));
}

?>


<!DOCTYPE html>
<html>
<head>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Staatliches&display=swap" rel="stylesheet">
    <meta charset="UTF-8">
    <title>superShooterTheGame</title>
    <link rel="stylesheet" href="css/adminsCSS.php">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body class=grid-container>

<div class="header">
    <div class="flex-container">

    </div>


</div>


<div class="izquierda">
    <div class="imagenes">
        <img id="chapas" src="images/admin.png" alt="Italian admin">
        <img id="suro" src="images/suro.jpg" alt="Italian suro">
    </div>
    <div>
        <input class=button type="button" value= "Back"  onclick="window.location.href='menuHTML.php';">
    </div>
</div>


<div class="derecha">
    <div><h1 id="titulo">Registros</h1></div>
    <body bgcolor="#EEFDEF">



    <?php

    echo "<table id='tabla' border='1'>

    <tr>
    
    <th>Id</th>
    
    <th>name</th>
    
    <th>isAdmin</th>
    
    <th>color</th>
    
    <th>puntacion</th>
    
    </tr>";

        foreach ($result as $row){
            echo "<tr>";

            echo "<td>" . $row['id'] . "</td>";

            echo "<td>" . $row['name'] . "</td>";

            echo "<td>" . $row['isAdmin'] . "</td>";

            echo "<td>" . $row['color'] . "</td>";

            echo "<td><form action='cambioPuntuacion.php' method='post'>
                    <input type='hidden' name='row' value='".$row['id']."'>
                    <input type='number' placeholder=".$row['puntuacion']." name='puntuacion'>
                    <button type='submit'>cambiar puntuacion</button>
                    </form></td>";

            echo "</tr>";
        }


        echo "</table>";



        ?>

</div>

</body>
</html>