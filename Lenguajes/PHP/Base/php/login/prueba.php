<?php
session_start();
$_SESSION["idpartida"] = 1;
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

//agafem els camps que hem enviat per AJAX.
$nom = $_GET["nom"];
$credencial = $_GET["password"];


try {
    //Connexio a la BD.
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    //QUERY. Les variables son alló que du : davant. En aquest cas :nom i :password
    $query = $conn->prepare("SELECT * FROM users WHERE nom=:nomUsuari and password=:contrassenya");
    //tants cops com variables hi hagi
    $query->bindParam("nomUsuari", $nom, PDO::PARAM_STR);
    $query->bindParam("contrassenya", $credencial, PDO::PARAM_STR);
    //executeu la consulta SQL. Retorna un booleà.
    $query->execute();
    //fetch torna la primera fila que compleix la query
    $result = $query->fetch(PDO::FETCH_ASSOC);
    //si ha conseguit trobar una fila amb la query (nota, si en tornes més d'una també diria 1 perque fetch només retorna el primer)
    if($query->rowCount() == 1) {
        $_SESSION["nom"] = $nom;
        echo(json_encode(true));
    } else{
        echo(json_encode(false));
    }

} catch(PDOException $e) {
    echo(json_encode("Connection failed: " . $e->getMessage()));
}
?>

















