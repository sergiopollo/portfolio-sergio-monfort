$(function(){
    $(":input#boto").on("click",function(){

        var username = $(":input#name").val();
        var pw = $(":input#password").val()
        var partida = $(":input#partida").val()

        $.ajax({

            method:"GET",
            //json.php s'ha de trobar en un servidor web. Sinó no funciona. Es pensa que és un fitxer amb format JSON la ruta.
            url:"login.php",
            //tot i que es un GET jo puc enviar data
            data:{
                "nom":username,
                "password":pw,
                "partida":partida
            },
            dataType:'json',
            success:function(data) {
                console.log(data);

                //cambiar esto!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                if(data==true){
                    $("#resposta2" ).text("Credencial Correcte") ;
                } else{
                    $("#resposta2" ).text("Credencial Incorrecte") ;
                }

            },

            error: function(jqXHR, texStatus, error) {
                alert("Error:" +jqXHR.responseText+" " + texStatus + " " + error);
            }
        });
    });


    $(":input#boto2").on("click",function(){

        var username = $(":input#name").val();
        var pw = $(":input#password").val()
        $.ajax({

            method:"POST",
            //json.php s'ha de trobar en un servidor web. Sinó no funciona. Es pensa que és un fitxer amb format JSON la ruta.
            url:"register.php",
            data:{
                "nom":username,
                "password":pw
            },
            dataType:'json',
            success:function(data) {
                console.log(data);
                if(data==true){
                    $("#resposta2" ).text("Registre Correcte") ;
                } else{
                    $("#resposta2" ).text("No s'ha pogut fer el registre") ;
                    $(":input#name").val("") ;
                    $(":input#surname").val("") ;
                    $(":input#password").val("") ;
                }

            },

            error: function(jqXHR, texStatus, error) {
                console.log("Error:" +jqXHR.responseText+" "+ texStatus + " " + error);
            }
        });
    });


    $(":input#boto3").on("click",function(){

        var nchars = $(":input#nchars").val();
        $.ajax({

            method:"GET",
            //json.php s'ha de trobar en un servidor web. Sinó no funciona. Es pensa que és un fitxer amb format JSON la ruta.
            url:"getByNChars.php",
            data:{
                "nchars":nchars,
            },
            dataType:'json',
            success:function(data) {
                console.log(data);
                //si hi ha qualsevol cosa que no sigui un false assumeix que es true
                if(data.length>0){
                    var s = "";
                    for (var i=0;i<data.length;i++){
                        s+=data[i].nom+" "+data[i].password+"<br>";
                    }
                    $("#resposta2" ).html("Ha trobat"+data.length+" dades:  "+s) ;
                } else{
                    $("#resposta2" ).text("No ha trobat dades") ;
                }

            },

            error: function(jqXHR, texStatus, error) {
                console.log("Error:" +jqXHR.responseText+" "+ texStatus + " " + error);
            }
        });
    });
});