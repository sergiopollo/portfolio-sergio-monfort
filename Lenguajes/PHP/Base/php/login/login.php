<?php
session_start();
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

//agafem els camps que hem enviat per AJAX.
//$nom = $_GET["nom"];
//$credencial = $_GET["password"];
//$partida = $_GET["partida"];
$nom = "Iker";
$credencial="gafas";
$partida = "partida1";

try {
    //Connexio a la BD.
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    //QUERY. Les variables son alló que du : davant. En aquest cas :nom i :password
    $query = $conn->prepare("SELECT * FROM users WHERE nom=:nomUsuari and password=:contrassenya");
    //tants cops com variables hi hagi
    $query->bindParam("nomUsuari", $nom, PDO::PARAM_STR);
    $query->bindParam("contrassenya", $credencial, PDO::PARAM_STR);
    //executeu la consulta SQL. Retorna un booleà.
    $query->execute();
    //fetch torna la primera fila que compleix la query
    $result = $query->fetch(PDO::FETCH_ASSOC);
    //guardo la id del jugador per mes endavant
    $idJugador = $result["id"];
    $_SESSION["idJugador"] = $idJugador;
    //si ha conseguit trobar una fila amb la query (nota, si en tornes més d'una també diria 1 perque fetch només retorna el primer)
    if($query->rowCount() >= 1) {
        $_SESSION["nom"] = $nom;
        //echo(json_encode(true));
        //ha fet login, ara li hem d'assignar una partida, comprove si la partida existeix
        //QUERY. Les variables son alló que du : davant.
        $query = $conn->prepare("SELECT * FROM partida WHERE nomPartida=:partida");
        //tants cops com variables hi hagi
        $query->bindParam("partida", $partida, PDO::PARAM_INT);
        //executeu la consulta SQL. Retorna un booleà.
        $query->execute();
        //fetch torna la primera fila que compleix la query
        $result = $query->fetch(PDO::FETCH_ASSOC);
        //si ha trobat la partida
        if($query->rowCount() >= 1) {
            //echo("partida encontrada");
            $idPartida = $result["id"];
            $_SESSION["idpartida"] = 1;
            $numJugs = $result["numjugs"];
            if($numJugs == 4){
                echo(json_encode(false));
            }else{ //si es menys de 4
                //idJugador ja esta agafada a dalt
                //augmento numJugs
                $numJugs = $numJugs+1;
                $_SESSION["torn"] = $numJugs;
                $query = $conn->prepare("UPDATE partida SET numjugs=:numJugs WHERE id=:idpartida");
                $query->bindParam("idpartida",$idPartida, PDO::PARAM_INT);
                $query->bindParam("numJugs",$numJugs, PDO::PARAM_INT);
                //executeu la consulta SQL. Retorna un booleà.
                $query->execute();
                if($numJugs == 4){
                    $query = $conn->prepare("UPDATE partida SET torn=:1 WHERE id=:idpartida");
                    $query->bindParam("idpartida",$idPartida, PDO::PARAM_INT);
                    //executeu la consulta SQL. Retorna un booleà.
                    $query->execute();
                }
                //insert a user_partida per posar torns
                $query = $conn->prepare("INSERT into user_partida (user_id, partida_id, ma_jugador, torn_jugador) VALUES (:idJugador, :idpartida, :ma, :torn)");
                $query->bindParam("idJugador",$idJugador, PDO::PARAM_INT);
                $query->bindParam("idpartida",$idPartida, PDO::PARAM_INT);
                $ma = "[]";
                $query->bindParam("ma",$ma, PDO::PARAM_STR);
                $query->bindParam("torn",$numJugs, PDO::PARAM_INT);
                //executeu la consulta SQL. Retorna un booleà.
                $query->execute();

                //robar 7 veces (sin avanzar turno)
                for ($i = 0; $i == 7; $i++) {

                    $idPartida=$_SESSION["idpartida"];
                    $torn = $_SESSION["torn"];

                    try{
                        //PASO1: CONNEXION. SIEMPRE IGUAL
                        $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
                        //PASO2: QUERY EN SQL
                        $query = $conn->prepare("SELECT * FROM partida WHERE id=:idpartida");
                        //PONEMOS LOS PARAMETROS EN LAS VARIABLES QUE HEMOS PUESTO EN LA CONSULTA
                        $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
                        //CORREMOS LA CONSULTA
                        $result = $query->execute();
                        //OBTENEMOS EL PRIMER RESULTADO DEL SELECT (SI LOS QUISIERAMOS TODOS HARIAMOS FETCHALL)
                        $result = $query->fetch(PDO::FETCH_ASSOC);
                        if($query->rowCount()==1){
                            $baraja = $result["pilaRobar"];
                            $turnoPartida = $result["torn"];
                            //print_r($turnoPartida);
                            $barajaPHP = json_decode($baraja);
                            $cartaQueHemAgafat = array_splice($barajaPHP,0,1);
                            $query = $conn->prepare("SELECT * FROM user_partida WHERE partida_id=:idpartida and torn_jugador=:turno");
                            $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
                            $query->bindParam("turno",$torn,PDO::PARAM_INT);

                            $result = $query->execute();
                            $result = $query->fetch(PDO::FETCH_ASSOC);
                            if($query->rowCount()==1){
                                //print_r($result);

                                $majugador = $result["ma_jugador"];
                                //print_r($majugador);
                                $majugadorPHP = json_decode($majugador);
                                array_push($majugadorPHP,$cartaQueHemAgafat );

                                $majugador = json_encode($majugadorPHP);
                                $baraja = json_encode($barajaPHP);

                                //echo $majugador;
                                $query = $conn->prepare("UPDATE user_partida SET ma_jugador=:majugador WHERE partida_id=:idpartida and torn_jugador=:turno");
                                $query->bindParam("majugador",$majugador,PDO::PARAM_STR);
                                $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
                                $query->bindParam("turno",$torn,PDO::PARAM_INT);

                                $result = $query->execute();
                                //ver error
                                //print_r($conn->errorInfo());
                                //print_r($query->errorInfo());
                                //print_r($query->errorCode());
                                //echo($result."<br>");

                                $query = $conn->prepare("UPDATE partida SET pilaRobar=:baraja, torn=:turno WHERE id=:idpartida");
                                $query->bindParam("baraja",$baraja,PDO::PARAM_STR);
                                $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
                                $query->bindParam("turno",$turnoPartida,PDO::PARAM_INT);

                                $result = $query->execute();
                                //echo($result."<br>");

                                echo json_encode($cartaQueHemAgafat);




                            }else{
                                echo "fiera";

                            }


                        }
                    }catch(PDOException $e) {
                        print_r(json_encode("Connection failed: " . $e->getMessage()));
                    }

                }

            }

        }else{
            //echo("partida no encontrada");
            $baralla='[
                {
                    "num":1,
                    "color":"red",
                    "img":"row-1-col-1.png"
                },
                {
                    "num":2,
                    "color":"red",
                    "img":"row-1-col-2.png"
                },
                {
                    "num":3,
                    "color":"red",
                    "img":"row-1-col-3.png"
                },
                {
                    "num":4,
                    "color":"red",
                    "img":"row-1-col-4.png"
                },
                {
                    "num":5,
                    "color":"red",
                    "img":"row-1-col-5.png"
                },
                {
                    "num":6,
                    "color":"red",
                    "img":"row-1-col-6.png"
                },
                {
                    "num":7,
                    "color":"red",
                    "img":"row-1-col-7.png"
                },
                {
                    "num":8,
                    "color":"red",
                    "img":"row-1-col-8.png"
                },
                {
                    "num":9,
                    "color":"red",
                    "img":"row-1-col-9.png"
                },
                {
                    "num":0,
                    "color":"red",
                    "img":"row-1-col-10.png"
                },
                {
                    "num":1,
                    "color":"yellow",
                    "img":"row-2-col-1.png"
                },
                {
                    "num":2,
                    "color":"yellow",
                    "img":"row-2-col-2.png"
                },
                {
                    "num":3,
                    "color":"yellow",
                    "img":"row-2-col-3.png"
                },
                {
                    "num":4,
                    "color":"yellow",
                    "img":"row-2-col-4.png"
                },
                {
                    "num":5,
                    "color":"yellow",
                    "img":"row-2-col-5.png"
                },
                {
                    "num":6,
                    "color":"yellow",
                    "img":"row-2-col-6.png"
                },
                {
                    "num":7,
                    "color":"yellow",
                    "img":"row-2-col-7.png"
                },
                {
                    "num":8,
                    "color":"yellow",
                    "img":"row-2-col-8.png"
                },
                {
                    "num":9,
                    "color":"yellow",
                    "img":"row-2-col-9.png"
                },
                {
                    "num":0,
                    "color":"yellow",
                    "img":"row-2-col-10.png"
                },
                {
                    "num":1,
                    "color":"green",
                    "img":"row-3-col-1.png"
                },
                {
                    "num":2,
                    "color":"green",
                    "img":"row-3-col-2.png"
                },
                {
                    "num":3,
                    "color":"green",
                    "img":"row-3-col-3.png"
                },
                {
                    "num":4,
                    "color":"green",
                    "img":"row-3-col-4.png"
                },
                {
                    "num":5,
                    "color":"green",
                    "img":"row-3-col-5.png"
                },
                {
                    "num":6,
                    "color":"green",
                    "img":"row-3-col-6.png"
                },
                {
                    "num":7,
                    "color":"green",
                    "img":"row-3-col-7.png"
                },
                {
                    "num":8,
                    "color":"green",
                    "img":"row-3-col-8.png"
                },
                {
                    "num":9,
                    "color":"green",
                    "img":"row-3-col-9.png"
                },
                {
                    "num":0,
                    "color":"green",
                    "img":"row-3-col-10.png"
                },
                {
                    "num":1,
                    "color":"blue",
                    "img":"row-4-col-1.png"
                },
                {
                    "num":2,
                    "color":"blue",
                    "img":"row-4-col-2.png"
                },
                {
                    "num":3,
                    "color":"blue",
                    "img":"row-4-col-3.png"
                },
                {
                    "num":4,
                    "color":"blue",
                    "img":"row-4-col-4.png"
                },
                {
                    "num":5,
                    "color":"blue",
                    "img":"row-4-col-5.png"
                },
                {
                    "num":6,
                    "color":"blue",
                    "img":"row-4-col-6.png"
                },
                {
                    "num":7,
                    "color":"blue",
                    "img":"row-4-col-7.png"
                },
                {
                    "num":8,
                    "color":"blue",
                    "img":"row-4-col-8.png"
                },
                {
                    "num":9,
                    "color":"blue",
                    "img":"row-4-col-9.png"
                },
                {
                    "num":0,
                    "color":"blue",
                    "img":"row-4-col-10.png"
                },
                {
                    "num":1,
                    "color":"red",
                    "img":"row-1-col-1.png"
                },
                {
                    "num":2,
                    "color":"red",
                    "img":"row-1-col-2.png"
                },
                {
                    "num":3,
                    "color":"red",
                    "img":"row-1-col-3.png"
                },
                {
                    "num":4,
                    "color":"red",
                    "img":"row-1-col-4.png"
                },
                {
                    "num":5,
                    "color":"red",
                    "img":"row-1-col-5.png"
                },
                {
                    "num":6,
                    "color":"red",
                    "img":"row-1-col-6.png"
                },
                {
                    "num":7,
                    "color":"red",
                    "img":"row-1-col-7.png"
                },
                {
                    "num":8,
                    "color":"red",
                    "img":"row-1-col-8.png"
                },
                {
                    "num":9,
                    "color":"red",
                    "img":"row-1-col-9.png"
                },
                {
                    "num":0,
                    "color":"red",
                    "img":"row-1-col-10.png"
                },
                {
                    "num":1,
                    "color":"yellow",
                    "img":"row-2-col-1.png"
                },
                {
                    "num":2,
                    "color":"yellow",
                    "img":"row-2-col-2.png"
                },
                {
                    "num":3,
                    "color":"yellow",
                    "img":"row-2-col-3.png"
                },
                {
                    "num":4,
                    "color":"yellow",
                    "img":"row-2-col-4.png"
                },
                {
                    "num":5,
                    "color":"yellow",
                    "img":"row-2-col-5.png"
                },
                {
                    "num":6,
                    "color":"yellow",
                    "img":"row-2-col-6.png"
                },
                {
                    "num":7,
                    "color":"yellow",
                    "img":"row-2-col-7.png"
                },
                {
                    "num":8,
                    "color":"yellow",
                    "img":"row-2-col-8.png"
                },
                {
                    "num":9,
                    "color":"yellow",
                    "img":"row-2-col-9.png"
                },
                {
                    "num":0,
                    "color":"yellow",
                    "img":"row-2-col-10.png"
                },
                {
                    "num":1,
                    "color":"green",
                    "img":"row-3-col-1.png"
                },
                {
                    "num":2,
                    "color":"green",
                    "img":"row-3-col-2.png"
                },
                {
                    "num":3,
                    "color":"green",
                    "img":"row-3-col-3.png"
                },
                {
                    "num":4,
                    "color":"green",
                    "img":"row-3-col-4.png"
                },
                {
                    "num":5,
                    "color":"green",
                    "img":"row-3-col-5.png"
                },
                {
                    "num":6,
                    "color":"green",
                    "img":"row-3-col-6.png"
                },
                {
                    "num":7,
                    "color":"green",
                    "img":"row-3-col-7.png"
                },
                {
                    "num":8,
                    "color":"green",
                    "img":"row-3-col-8.png"
                },
                {
                    "num":9,
                    "color":"green",
                    "img":"row-3-col-9.png"
                },
                {
                    "num":0,
                    "color":"green",
                    "img":"row-3-col-10.png"
                },
                {
                    "num":1,
                    "color":"blue",
                    "img":"row-4-col-1.png"
                },
                {
                    "num":2,
                    "color":"blue",
                    "img":"row-4-col-2.png"
                },
                {
                    "num":3,
                    "color":"blue",
                    "img":"row-4-col-3.png"
                },
                {
                    "num":4,
                    "color":"blue",
                    "img":"row-4-col-4.png"
                },
                {
                    "num":5,
                    "color":"blue",
                    "img":"row-4-col-5.png"
                },
                {
                    "num":6,
                    "color":"blue",
                    "img":"row-4-col-6.png"
                },
                {
                    "num":7,
                    "color":"blue",
                    "img":"row-4-col-7.png"
                },
                {
                    "num":8,
                    "color":"blue",
                    "img":"row-4-col-8.png"
                },
                {
                    "num":9,
                    "color":"blue",
                    "img":"row-4-col-9.png"
                },
                {
                    "num":0,
                    "color":"blue",
                    "img":"row-4-col-10.png"
                }
            
            ]';


            $arrayBaralla = (json_decode($baralla));

            shuffle($arrayBaralla);
            $primeraCarta = $arrayBaralla[0];
            array_splice($arrayBaralla,0,1);
            $nomPartida = "partida1";
            $torn = 0;

            $jsonPrimeraCarta = json_encode($primeraCarta);
            $jsonBaralla = json_encode($arrayBaralla);

            try {
                //Connexio a la BD.
                $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
                //QUERY. Les variables son alló que du : davant. En aquest cas :nom i :password
                $query = $conn->prepare("INSERT INTO partida (nomPartida,cartaTaula,pilaRobar,torn,numjugs) values(:np,:ct,:pr,:torn,1)");
                //tants cops com variables hi hagi
                $query->bindParam("np", $nomPartida, PDO::PARAM_STR);
                $query->bindParam("ct",$jsonPrimeraCarta , PDO::PARAM_STR);
                $query->bindParam("pr", $jsonBaralla, PDO::PARAM_STR);
                $query->bindParam("torn", $torn, PDO::PARAM_INT);
                //executeu la consulta SQL. Retorna un booleà.
                $resultat = $query->execute();

                if($resultat) {
                    echo(json_encode(true));
                } else{
                    echo(json_encode(false));
                }


            } catch(Exception $e) {
                echo(json_encode("Connection failed: " . $e->getMessage()));
            }

        }

    } else{
        echo(json_encode(false));
    }

} catch(PDOException $e) {
    echo(json_encode("Connection failed: " . $e->getMessage()));
}
?>
