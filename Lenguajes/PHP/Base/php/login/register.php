<?php
$servername = "localhost";
$username = "admin";
$password = "super3";

$nom = $_POST["nom"];
$credencial = $_POST["password"];

try {
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    $query = $conn->prepare("INSERT INTO users (nom,password) values(:nomUsuari,:contrassenya)");
    $query->bindParam("nomUsuari", $nom, PDO::PARAM_STR);
    $query->bindParam("contrassenya", $credencial, PDO::PARAM_STR);
    $resultat = $query->execute();

    if($resultat) {
        echo(json_encode(true));
    } else{
        echo(json_encode(false));
    }

} catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>