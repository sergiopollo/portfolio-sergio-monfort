<?php
//las 3 variables de conexion a BD
$servername = "localhost";
$username = "admin";
$password = "super3";


//PASOS DE check
//1.select de la mano de los 4 jugadores y count
//2.select de cartaTaula de partida
//3.recuperaer torn del session
//4. array associatiu de esto y devolverlo en json


//$idPartida=$_SESSION["idpartida"];
//$torn = $_SESSION["torn"];
$idPartida=1;
$torn = 1;


try{
    //PASO1: CONNEXION. SIEMPRE IGUAL
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    //PASO2: QUERY EN SQL
    $query = $conn->prepare("SELECT * FROM partida WHERE id=:idpartida");
    //PONEMOS LOS PARAMETROS EN LAS VARIABLES QUE HEMOS PUESTO EN LA CONSULTA
    $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
    //CORREMOS LA CONSULTA
    $result = $query->execute();
    //OBTENEMOS EL PRIMER RESULTADO DEL SELECT (SI LOS QUISIERAMOS TODOS HARIAMOS FETCHALL)
    $result = $query->fetch(PDO::FETCH_ASSOC);
    if($query->rowCount()==1){
        $cartaTaula = $result["cartaTaula"];
        $turnoPartida = $result["torn"];

        for ($i = 1; $i <= 4; $i++) {
            $query = $conn->prepare("SELECT * FROM user_partida WHERE partida_id=:idpartida and torn_jugador=:turno");
            $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
            $query->bindParam("turno",$i,PDO::PARAM_INT);

            $result = $query->execute();
            $result = $query->fetch(PDO::FETCH_ASSOC);
            if($query->rowCount()==1) {
                $manoJug = $result["ma_jugador"];
                $manoJugPHP = json_decode($manoJug);

                $numCartas = count($manoJugPHP);

                $llista["jugador" . $i] = $numCartas;
            }
        }

        $llista["cartaTaula"] = $cartaTaula;
        $llista["torn"] = $torn;

        echo(json_encode($llista));

    }
}catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
