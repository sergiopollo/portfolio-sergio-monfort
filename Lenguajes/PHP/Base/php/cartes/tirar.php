<?php
//las 3 variables de conexion a BD
$servername = "localhost";
$username = "admin";
$password = "super3";


//PASOS DE TIRAR
//1. RECUPERAR LA MANO DEL JUGADOR EN USER_PARTIDA
//2. SACARLE LA CARTA IGUAL QUE LA CARTA RECIBIDA I UPDATEAR LA MANO DEL JUGADOR
//3. EN LA TABLA PARTIDA EN EL CAMPO CARTATAULA PONER LA CARTA (UPDATE)


//del AJAX cogemos la carta pulsada
$cartaSeleccionada = (int)$_GET["carta"];
//$cartaSeleccionada = '[{"num":1,"color":"yellow","img":"row-2-col-1.png"}]';
//del session turno y partida id
$idPartida=$_SESSION["idpartida"];
$torn = $_SESSION["torn"];
//$idPartida=1;
//$torn = 1;

try{
    //PASO1: CONNEXION. SIEMPRE IGUAL
    $conn = new PDO("mysql:host=$servername;dbname=m4uno", $username, $password);
    //PASO2: QUERY EN SQL
    $query = $conn->prepare("SELECT * FROM user_partida WHERE partida_id=:idpartida and torn_jugador=:torn");
    //PONEMOS LOS PARAMETROS EN LAS VARIABLES QUE HEMOS PUESTO EN LA CONSULTA
    $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
    $query->bindParam("torn",$torn,PDO::PARAM_INT);
    //CORREMOS LA CONSULTA
    $result = $query->execute();
    //OBTENEMOS EL PRIMER RESULTADO DEL SELECT (SI LOS QUISIERAMOS TODOS HARIAMOS FETCHALL)
    $result = $query->fetch(PDO::FETCH_ASSOC);
    if($query->rowCount()==1){
        $manoJugador = $result["ma_jugador"];
        //print_r($turnoPartida);
        $manoJugadorPHP = json_decode($manoJugador);
        $cartaSeleccionadaPHP = json_decode($cartaSeleccionada);

        for ($i = 0; $i < count($manoJugadorPHP); $i++) {
            if($manoJugadorPHP[$i] == $cartaSeleccionadaPHP){
                array_splice($manoJugadorPHP, $i, 1);
            }

        }

        $manoJugador = json_encode($manoJugadorPHP);

        //echo $majugador;
        $query = $conn->prepare("UPDATE user_partida SET ma_jugador=:majugador WHERE partida_id=:idpartida and torn_jugador=:turno");
        $query->bindParam("majugador",$manoJugador,PDO::PARAM_STR);
        $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
        $query->bindParam("turno",$torn,PDO::PARAM_INT);

        $result = $query->execute();
        //ver error
        //print_r($conn->errorInfo());
        //print_r($query->errorInfo());
        //print_r($query->errorCode());
        //echo($result."<br>");

        $turnoPartida = $torn;
        $turnoPartida++;
        if($turnoPartida > 4){
            $turnoPartida = 1;
        }
        $_SESSION["torn"] = $turnoPartida;

        $query = $conn->prepare("UPDATE partida SET cartaTaula=:carta, torn=:turno WHERE id=:idpartida");
        $query->bindParam("carta",$cartaSeleccionada,PDO::PARAM_STR);
        $query->bindParam("idpartida",$idPartida,PDO::PARAM_INT);
        $query->bindParam("turno",$turnoPartida,PDO::PARAM_INT);

        $result = $query->execute();

        echo(json_encode(true));


    }
}catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}