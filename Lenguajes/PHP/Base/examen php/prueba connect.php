<?php
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

//agafem els camps que hem enviat per AJAX.
//$nom = $_GET["nom"];
//$credencial = $_GET["password"];
//$partida = $_GET["partida"];

$nom = "Bulbasaur";
//Connexio a la BD.
$conn = new PDO("mysql:host=$servername;dbname=pokemon", $username, $password);
$query = $conn->prepare("SELECT * FROM pokemon WHERE name=:nom");
//tants cops com variables hi hagi
$query->bindParam("nom", $nom, PDO::PARAM_STR);

//executeu la consulta SQL. Retorna un booleà.

$query->execute();
//fetch torna la primera fila que compleix la query
$result = $query->fetch(PDO::FETCH_ASSOC);

print_r($result);