$(function(){
    $(":input#boto").on("click",function() {


        $.ajax({
            //GET O POST
            method: "GET",
            //URL A PHP
            url: "https://catfact.ninja/facts?limit=100",
            //EN TOTS ELS EXERCICIS EL DATATYPE ES JSON. NO US L'OBLIDEU O NO PODREU LLEGIR RES
            dataType: "json",

            //FUNCIO SUCCESS
            success: function (data) {
                //EL PRIMER QUE HAURIEU DE FER ES DEBUGAR DATA PER A SABER QUE HEU REBUT
                console.log(data);
                var random = Math.floor(Math.random() * 100);

                var fact = data[random]["fact"];

                $("#resposta").text(fact);


            },
            //FEU SERVIR AQUESTA FUNCIO D'ERROR PERQUE ES LA QUE US DONARA MES INFORMACIO
            error: function (jqXHR, texStatus, error) {
                console.log("Error:" + jqXHR.responseText + " " + texStatus + " " + error);
            }

        });

    });
});