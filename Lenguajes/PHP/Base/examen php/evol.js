$(function(){
    $(":input#boto").on("click",function() {

        var pokemon = $(":input#pokemon").val();

        $.ajax({
            //GET O POST
            method: "GET",
            //URL A PHP
            url: "evol.php",
            //EN TOTS ELS EXERCICIS EL DATATYPE ES JSON. NO US L'OBLIDEU O NO PODREU LLEGIR RES
            data:{
                "pokemon":pokemon
            },
            dataType: "json",

            //FUNCIO SUCCESS
            success: function (data) {
                //EL PRIMER QUE HAURIEU DE FER ES DEBUGAR DATA PER A SABER QUE HEU REBUT
                console.log(data);



            },
            //FEU SERVIR AQUESTA FUNCIO D'ERROR PERQUE ES LA QUE US DONARA MES INFORMACIO
            error: function (jqXHR, texStatus, error) {
                console.log("Error:" + jqXHR.responseText + " " + texStatus + " " + error);
            }

        });

    });
});