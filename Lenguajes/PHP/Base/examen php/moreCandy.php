<?php
session_start();
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

try {
    //Connexio a la BD.
    $conn = new PDO("mysql:host=$servername;dbname=pokemon", $username, $password);

    for ($i = 1; $i <= 7; $i++) {
        $query = $conn->prepare("SELECT * FROM pokemon WHERE idPokemon=:id");
        //tants cops com variables hi hagi
        $query->bindParam("id",$i,PDO::PARAM_INT);
        //executeu la consulta SQL. Retorna un booleà.
        $query->execute();
        //fetch torna la primera fila que compleix la query
        $result = $query->fetch(PDO::FETCH_ASSOC);
        //si ha conseguit trobar una fila amb la query (nota, si en tornes més d'una també diria 1 perque fetch només retorna el primer)
        if($query->rowCount() >= 1) {

            if(is_null($result["candy_count"])){
                //no hagas nada
            }else{

                $candy = $result["candy_count"];
                $candy++;

                $query = $conn->prepare("UPDATE pokemon SET candy_count=:candy WHERE idPokemon=:id");
                $query->bindParam("id", $i, PDO::PARAM_INT);
                $query->bindParam("candy", $candy, PDO::PARAM_INT);
                $result = $query->execute();
            }

        }else{
            echo(json_encode("error"));
        }
    }

    //aqui podria posar un echo i blabla pero no tinc temps :D


} catch(PDOException $e) {
    echo(json_encode("Connection failed: " . $e->getMessage()));
}




