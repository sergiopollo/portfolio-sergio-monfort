<?php
session_start();
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

$pokemon = $_GET["pokemon"];

//$pokemon = "Bulbasaur";

try {
    //Connexio a la BD.
    $conn = new PDO("mysql:host=$servername;dbname=pokemon", $username, $password);
    //QUERY. Les variables son alló que du : davant. En aquest cas :nom i :password
    $query = $conn->prepare("SELECT * FROM pokemon WHERE name=:pokemon");
    //tants cops com variables hi hagi
    $query->bindParam("pokemon", $pokemon, PDO::PARAM_STR);
    //executeu la consulta SQL. Retorna un booleà.
    $query->execute();
    //fetch torna la primera fila que compleix la query
    $result = $query->fetch(PDO::FETCH_ASSOC);
    //si ha conseguit trobar una fila amb la query (nota, si en tornes més d'una també diria 1 perque fetch només retorna el primer)
    if($query->rowCount() >= 1) {

        $evolucions = $result["evolution"];

        if(is_null($evolucions)){
            echo(json_encode("el pokemon no evolucionara"));
        }else {

            $evolucionsPHP = json_decode($evolucions, true);

            $nom = $evolucionsPHP[0]["name"];

            echo(json_encode($nom));

        }
    }else{
        echo(json_encode("el pokemon no existeix"));
    }

} catch(PDOException $e) {
    echo(json_encode("Connection failed: " . $e->getMessage()));
}

