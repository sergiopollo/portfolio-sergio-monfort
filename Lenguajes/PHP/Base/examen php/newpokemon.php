<?php
session_start();
//servername -> a on esta el servidor a on esta la bd. Generalment serà localhost
$servername = "localhost";
//usuari de la BD
$username = "admin";
//pw del usuari de la BD
$password = "super3";

$name = $_GET["name"];
$candy = $_GET["candy"];
$evolution = $_GET["evolution"];

//$name = "asd";
//$candy = 123;
//$evolution = '[{"num":"002","name":"Ivysaur"},{"num":"003","name":"Venusaur"}]';

try {
    //Connexio a la BD.
    $conn = new PDO("mysql:host=$servername;dbname=pokemon", $username, $password);
    //QUERY. Les variables son alló que du : davant. En aquest cas :nom i :password
    $query = $conn->prepare("INSERT INTO pokemon (name,evolution,candy_count) values(:nom,:evolution,:candy)");
    $query->bindParam("nom", $name, PDO::PARAM_STR);
    $query->bindParam("evolution", $evolution, PDO::PARAM_STR);
    $query->bindParam("candy", $candy, PDO::PARAM_INT);
    $resultat = $query->execute();

    if($resultat) {
        echo(json_encode(true));
    } else{
        echo(json_encode(false));
    }

} catch(PDOException $e) {
    echo(json_encode("Connection failed: " . $e->getMessage()));
}


