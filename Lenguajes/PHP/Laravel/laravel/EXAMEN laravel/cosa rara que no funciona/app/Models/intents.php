<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class intents extends Model
{

    protected $fillable = [
        'nota',
    ];

    public function user(){
        return $this -> belongsTo(User::class);
    }

    public function questionari(){
        return $this -> belongsTo(questionari::class);
    }

    public function respostes(){
        return $this->hasMany(respostes::class);
    }
}
