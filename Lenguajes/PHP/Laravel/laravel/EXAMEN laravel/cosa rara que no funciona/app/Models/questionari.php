<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class questionari extends Model
{



    // esto es necesario??
    protected $fillable = [
        'nom',
    ];

    public function intents(){
        return $this->hasMany(intents::class);
    }

    public function preguntes(){
        return $this->hasMany(preguntes::class);
    }
}
