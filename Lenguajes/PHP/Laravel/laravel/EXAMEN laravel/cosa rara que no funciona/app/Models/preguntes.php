<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class preguntes extends Model
{


    protected $fillable = [
        'enunciat',
        'resposta',
    ];


    function questionari(){
        return $this -> belongsTo(questionari::class);
    }

    public function respostes(){
        return $this->hasMany(respostes::class);
    }

}
