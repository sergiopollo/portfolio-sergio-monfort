<?php


namespace App\Http\Controllers;


use App\Http\Middleware\Authenticate;
use App\Models\intents;
use App\Models\preguntes;
use App\Models\questionari;
use App\Models\respostes;
use Auth;
use http\Env\Request;

class RespostaController
{
    public function addResposta(preguntes $pregunta, intents $intent){
        return view('crearRespuesta', compact('pregunta', 'intent'));
    }


    public static function create(preguntes $pregunta, intents $intent, String $request){

        if(Auth::check()){

            $resposta = new respostes();
            $resposta->resposta = $request;
            $resposta->pregunta_id = $pregunta->id;
            $resposta->intent_id=$intent->id;


            $resposta->save();

            //return redirect('/home');
        }
        return redirect('/home');
    }
}
