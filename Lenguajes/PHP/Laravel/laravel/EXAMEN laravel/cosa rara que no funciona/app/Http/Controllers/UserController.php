<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use http\Env\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    public function create(Request $newUser)
    {
        $user = new user();
        $user->name=$newUser->name;
        $user->email=$newUser->email;
        $user->password=$newUser->password;
        if($newUser->kiiboy != null){
            $user->kiiboy= true;
        }
        else{
            $user->kiiboy= false;
        }

    }

}
