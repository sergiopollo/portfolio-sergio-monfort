<?php


namespace App\Http\Controllers;


use App\Models\preguntes;
use App\Models\questionari;
use Illuminate\Http\Request;
use Auth;
use DB;

class QuestionariController extends Controller
{

    public function addQuestionari(){
        return view('crearCuestionario');
    }


    public function create(Request  $request){


        if(Auth::check() && Auth::user()->isKiiboy){

            $questionari = new questionari();
            $questionari->nom = $request->name;

            $questionari->save();

            //return redirect('/home');
        }
        return redirect('/home');
    }

    public function GetAllQuestionaris(){
        //$All_Questionarios = questionari::all();
        $All_Questionarios = DB::table('questionaris')->get();
        return view('selectCuestionario', compact('All_Questionarios'));
    }

    public function GetAllQuestionarisAddPregunta(){
        //$All_Questionarios = questionari::all();
        $All_Questionarios = DB::table('questionaris')->get();
        return view('SelectCPreguntas', compact('All_Questionarios'));
    }


}
