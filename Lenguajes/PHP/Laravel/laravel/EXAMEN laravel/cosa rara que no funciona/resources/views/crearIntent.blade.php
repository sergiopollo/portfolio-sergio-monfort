@extends('layouts.app', ['activePage' => 'register', 'title' => 'Light Bootstrap Dashboard Laravel by Creative Tim & UPDIVISION'])

@section('content')
<div class="container">
    <h2>Add New Intent</h2>

    <form method="POST" action="/intent/crear/{{$questionari->id}}">

        <div class="form-group">

            <table>
                <tbody>
                @foreach($questionari->preguntes as $pregunta)
                    <tr>
                        <td>
                            <div>
                                {{$pregunta->enunciat}}
                                <br>
                                <label for="fname">resposta:</label><br>
                                <input type="text" id="resposta" name={{$pregunta->id}}>
                                <br>
                            </div>
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>



            <button type="submit" class="btn btn-primary">Add Task</button>
        </div>
        {{ csrf_field() }}
    </form>


</div>
@endsection
