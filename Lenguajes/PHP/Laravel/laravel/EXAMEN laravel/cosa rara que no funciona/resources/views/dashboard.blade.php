@extends('layouts.app', ['activePage' => 'dashboard', 'title' => 'Light Bootstrap Dashboard Laravel by Creative Tim & UPDIVISION', 'navName' => 'Dashboard', 'activeButton' => 'laravel'])

@section('content')
    <a href="questionari/crear">
        <button>crear cuestionario</button>
    </a>

    <a href="questionari/addPreguntas">
        <button>crear preguntas</button>
    </a>

    <a href="/questionari/select">
        <button>hacer intento</button>
    </a>

    <a href="/intent/ranking">
        <button>Ver Ranking</button>
    </a>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

            demo.showNotification();

        });
    </script>
@endpush
