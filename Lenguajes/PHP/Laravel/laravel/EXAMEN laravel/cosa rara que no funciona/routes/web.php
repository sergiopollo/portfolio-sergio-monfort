<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');


//El get es para que te envie a la view correcta, en el controler se hacen
//El post es para hacer el cambio en la bbdd
Route::get('/questionari/crear', 'App\Http\Controllers\QuestionariController@addQuestionari');
Route::post('/questionari/crear', 'App\Http\Controllers\QuestionariController@create');

Route::get('/resposta/crear', 'App\Http\Controllers\RespostaController@addResposta');
Route::post('/resposta/crear', 'App\Http\Controllers\RespostaController@create');

Route::get('/intent/crear/{questionari}', 'App\Http\Controllers\IntentController@addIntent');
Route::post('/intent/crear/{questionari}', 'App\Http\Controllers\IntentController@create');

Route::get('/pregunta/crear/{questionari}', 'App\Http\Controllers\PreguntaController@addPregunta');
Route::post('/pregunta/crear/{questionari}', 'App\Http\Controllers\PreguntaController@create');

Route::get('/questionari/select', 'App\Http\Controllers\QuestionariController@GetAllQuestionaris');
Route::get('/questionari/addPreguntas', 'App\Http\Controllers\QuestionariController@GetAllQuestionarisAddPregunta');
Route::get('/intent/ranking', 'App\Http\Controllers\IntentController@GetAllIntents');



//MAGIA
Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});



