@extends('layouts.app', ['activePage' => 'register', 'title' => 'Light Bootstrap Dashboard Laravel by Creative Tim & UPDIVISION'])

@section('content')
    <div class="container">
        <h2>selecciona un cuestionario</h2>



        <table border='1'>
            <tr>
                <th>id_questionario</th>
                <th>id_usuario</th>
                <th>Nota</th>
            </tr>

            @foreach($All_Intents as $m_intents)
                <tr>
                    <td>{{$m_intents->questionari_id}}</td>
                    <td>{{$m_intents->usuari_id}}</td>
                    <td>{{$m_intents->nota}}</td>

                </tr>
            @endforeach
            {{ csrf_field() }}
        </table>


    </div>
@endsection
