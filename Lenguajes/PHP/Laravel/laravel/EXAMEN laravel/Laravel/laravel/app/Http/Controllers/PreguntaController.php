<?php


namespace App\Http\Controllers;


use App\Models\preguntes;
use App\Models\questionari;
use Illuminate\Http\Request;
use Auth;

class PreguntaController extends Controller
{
    //questionari $questionari
    public function addPregunta(questionari $questionari){
        return view('crearPregunta', compact('questionari'));
    }


    public function create(questionari $questionari, Request  $request){

        if(Auth::check() && Auth::user()->isKiiboy) {


            $pregunta = new preguntes();

            $pregunta->enunciat = $request->name;
            $pregunta->resposta = $request->resposta;
            $pregunta->questionari_id = $questionari->id;

            $pregunta->save();

        }
        return redirect('/home');

    }
}
