<?php


namespace App\Http\Controllers;


use App\Http\Middleware\Authenticate;
use App\Models\intents;
use App\Models\questionari;
use Auth;
use Illuminate\Http\Request;
use DB;

class IntentController
{
    public function addIntent(questionari $questionari){
        return view('crearIntent', compact('questionari'));
    }


    public function create(questionari $questionari, Request  $request){

        if(Auth::check()){

            $intent = new intents();
            $intent->nota = 0;
            $intent->questionari_id = $questionari->id;
            $intent->usuari_id = Auth::id();
/*
            if(Auth::user()->isKiiboy){
                error_log("estoy improvisando");
                $intent->is_intento_modelo = true;
            }else{
                error_log("estoy improvisando2");
                $intent->is_intento_modelo = false;
            }
*/
            $intent->save();



            error_log("sergio");
            $numPreguntas = 0;

            foreach($questionari->preguntes as $pregunta){
                error_log($pregunta->id);
                $pid = $pregunta->id;
                error_log($request->$pid);
                RespostaController::create($pregunta, $intent, $request->$pid);
                $numPreguntas++;
            }

            $nota=0;

            foreach($questionari->preguntes as $pregunta) {

                $pid = $pregunta->id;

                $miRespuesta = strtolower($request->$pid);
                $respuestaCorrecta = strtolower($pregunta->resposta);

                if ($miRespuesta == $respuestaCorrecta) {
                    $nota++;
                }

            }

            $notaFinal = ($nota*10)/$numPreguntas;

            $intent->nota = $notaFinal;
            $intent->save();
            //return redirect('/home');
        }
        return redirect('/home');
    }


    public function GetAllIntents(){
        //$All_Questionarios = questionari::all();
        $All_Intents = DB::table('intents')->orderBy('nota', 'desc')->get();
        return view('Ranking', compact('All_Intents'));
    }



}
