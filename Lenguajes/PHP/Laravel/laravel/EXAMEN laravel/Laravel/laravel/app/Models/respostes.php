<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class respostes extends Model
{

    protected $fillable = [
        'resposta',
    ];

    public function intents(){
        return $this -> belongsTo(intents::class);
    }

    public function preguntes(){
        return $this -> belongsTo(preguntes::class);
    }

}
