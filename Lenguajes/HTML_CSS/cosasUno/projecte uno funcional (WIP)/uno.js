
const barallaconst = [
    {
        "num":1,
        "color":"red",
        "img":"row-1-col-1.png"
    },
    {
        "num":2,
        "color":"red",
        "img":"row-1-col-2.png"
    },
    {
        "num":3,
        "color":"red",
        "img":"row-1-col-3.png"
    },
    {
        "num":4,
        "color":"red",
        "img":"row-1-col-4.png"
    },
    {
        "num":5,
        "color":"red",
        "img":"row-1-col-5.png"
    },
    {
        "num":6,
        "color":"red",
        "img":"row-1-col-6.png"
    },
    {
        "num":7,
        "color":"red",
        "img":"row-1-col-7.png"
    },
    {
        "num":8,
        "color":"red",
        "img":"row-1-col-8.png"
    },
    {
        "num":9,
        "color":"red",
        "img":"row-1-col-9.png"
    },
    {
        "num":0,
        "color":"red",
        "img":"row-1-col-10.png"
    },
    {
        "num":1,
        "color":"yellow",
        "img":"row-2-col-1.png"
    },
    {
        "num":2,
        "color":"yellow",
        "img":"row-2-col-2.png"
    },
    {
        "num":3,
        "color":"yellow",
        "img":"row-2-col-3.png"
    },
    {
        "num":4,
        "color":"yellow",
        "img":"row-2-col-4.png"
    },
    {
        "num":5,
        "color":"yellow",
        "img":"row-2-col-5.png"
    },
    {
        "num":6,
        "color":"yellow",
        "img":"row-2-col-6.png"
    },
    {
        "num":7,
        "color":"yellow",
        "img":"row-2-col-7.png"
    },
    {
        "num":8,
        "color":"yellow",
        "img":"row-2-col-8.png"
    },
    {
        "num":9,
        "color":"yellow",
        "img":"row-2-col-9.png"
    },
    {
        "num":0,
        "color":"yellow",
        "img":"row-2-col-10.png"
    },
    {
        "num":1,
        "color":"green",
        "img":"row-3-col-1.png"
    },
    {
        "num":2,
        "color":"green",
        "img":"row-3-col-2.png"
    },
    {
        "num":3,
        "color":"green",
        "img":"row-3-col-3.png"
    },
    {
        "num":4,
        "color":"green",
        "img":"row-3-col-4.png"
    },
    {
        "num":5,
        "color":"green",
        "img":"row-3-col-5.png"
    },
    {
        "num":6,
        "color":"green",
        "img":"row-3-col-6.png"
    },
    {
        "num":7,
        "color":"green",
        "img":"row-3-col-7.png"
    },
    {
        "num":8,
        "color":"green",
        "img":"row-3-col-8.png"
    },
    {
        "num":9,
        "color":"green",
        "img":"row-3-col-9.png"
    },
    {
        "num":0,
        "color":"green",
        "img":"row-3-col-10.png"
    },
    {
        "num":1,
        "color":"blue",
        "img":"row-4-col-1.png"
    },
    {
        "num":2,
        "color":"blue",
        "img":"row-4-col-2.png"
    },
    {
        "num":3,
        "color":"blue",
        "img":"row-4-col-3.png"
    },
    {
        "num":4,
        "color":"blue",
        "img":"row-4-col-4.png"
    },
    {
        "num":5,
        "color":"blue",
        "img":"row-4-col-5.png"
    },
    {
        "num":6,
        "color":"blue",
        "img":"row-4-col-6.png"
    },
    {
        "num":7,
        "color":"blue",
        "img":"row-4-col-7.png"
    },
    {
        "num":8,
        "color":"blue",
        "img":"row-4-col-8.png"
    },
    {
        "num":9,
        "color":"blue",
        "img":"row-4-col-9.png"
    },
    {
        "num":0,
        "color":"blue",
        "img":"row-4-col-10.png"
    },
    {
        "num":1,
        "color":"red",
        "img":"row-1-col-1.png"
    },
    {
        "num":2,
        "color":"red",
        "img":"row-1-col-2.png"
    },
    {
        "num":3,
        "color":"red",
        "img":"row-1-col-3.png"
    },
    {
        "num":4,
        "color":"red",
        "img":"row-1-col-4.png"
    },
    {
        "num":5,
        "color":"red",
        "img":"row-1-col-5.png"
    },
    {
        "num":6,
        "color":"red",
        "img":"row-1-col-6.png"
    },
    {
        "num":7,
        "color":"red",
        "img":"row-1-col-7.png"
    },
    {
        "num":8,
        "color":"red",
        "img":"row-1-col-8.png"
    },
    {
        "num":9,
        "color":"red",
        "img":"row-1-col-9.png"
    },
    {
        "num":0,
        "color":"red",
        "img":"row-1-col-10.png"
    },
    {
        "num":1,
        "color":"yellow",
        "img":"row-2-col-1.png"
    },
    {
        "num":2,
        "color":"yellow",
        "img":"row-2-col-2.png"
    },
    {
        "num":3,
        "color":"yellow",
        "img":"row-2-col-3.png"
    },
    {
        "num":4,
        "color":"yellow",
        "img":"row-2-col-4.png"
    },
    {
        "num":5,
        "color":"yellow",
        "img":"row-2-col-5.png"
    },
    {
        "num":6,
        "color":"yellow",
        "img":"row-2-col-6.png"
    },
    {
        "num":7,
        "color":"yellow",
        "img":"row-2-col-7.png"
    },
    {
        "num":8,
        "color":"yellow",
        "img":"row-2-col-8.png"
    },
    {
        "num":9,
        "color":"yellow",
        "img":"row-2-col-9.png"
    },
    {
        "num":0,
        "color":"yellow",
        "img":"row-2-col-10.png"
    },
    {
        "num":1,
        "color":"green",
        "img":"row-3-col-1.png"
    },
    {
        "num":2,
        "color":"green",
        "img":"row-3-col-2.png"
    },
    {
        "num":3,
        "color":"green",
        "img":"row-3-col-3.png"
    },
    {
        "num":4,
        "color":"green",
        "img":"row-3-col-4.png"
    },
    {
        "num":5,
        "color":"green",
        "img":"row-3-col-5.png"
    },
    {
        "num":6,
        "color":"green",
        "img":"row-3-col-6.png"
    },
    {
        "num":7,
        "color":"green",
        "img":"row-3-col-7.png"
    },
    {
        "num":8,
        "color":"green",
        "img":"row-3-col-8.png"
    },
    {
        "num":9,
        "color":"green",
        "img":"row-3-col-9.png"
    },
    {
        "num":0,
        "color":"green",
        "img":"row-3-col-10.png"
    },
    {
        "num":1,
        "color":"blue",
        "img":"row-4-col-1.png"
    },
    {
        "num":2,
        "color":"blue",
        "img":"row-4-col-2.png"
    },
    {
        "num":3,
        "color":"blue",
        "img":"row-4-col-3.png"
    },
    {
        "num":4,
        "color":"blue",
        "img":"row-4-col-4.png"
    },
    {
        "num":5,
        "color":"blue",
        "img":"row-4-col-5.png"
    },
    {
        "num":6,
        "color":"blue",
        "img":"row-4-col-6.png"
    },
    {
        "num":7,
        "color":"blue",
        "img":"row-4-col-7.png"
    },
    {
        "num":8,
        "color":"blue",
        "img":"row-4-col-8.png"
    },
    {
        "num":9,
        "color":"blue",
        "img":"row-4-col-9.png"
    },
    {
        "num":0,
        "color":"blue",
        "img":"row-4-col-10.png"
    },

]

baralla = barallaconst;

//mostrar les cartes que te cada jugador

function muestraCartas(){

$("#mostrarCartasJ2").html("cartas: "+jugador2.length);
$("#mostrarCartasJ3").html("cartas: "+jugador3.length);
$("#mostrarCartasJ4").html("cartas: "+jugador4.length);

}


//1

function compruebaCarta(cartaT, cartaM){

    var CT = cartaT.color;
	var NT = cartaT.num;
	var CM = cartaM.color;
	var NM = cartaM.num;
	
  var tirar = false;
  
	if(CT == CM || NT == NM || CM == "negre"){
  	
    tirar = true;
    
  }
return tirar;
}

//2

//esta funcion ya no sirve
/*
function añadecarta(){

 var random = Math.floor(Math.random() * baralla.length); 

 var capsa = document.createElement("img");

capsa.setAttribute("src","UNO-resources/"+baralla[random].img);

 document.getElementById("flex-container1").appendChild(capsa);


}

*/

//3

function barallar(){

    for (i = baralla.length -1; i > 0; i--) {
      j = Math.floor(Math.random() * i)
      k = baralla[i]
      baralla[i] = baralla[j]
      baralla[j] = k
    }


}


//arrays dels 4 jugadors


    jugador1 = [],
    jugador2 = [],
    jugador3 = [],
    jugador4 = []

//array de la carta sobre la taula (on nomes pot haver-hi una carta)

    cartaMesa = []


function robar(jugador, contenedor){

    console.log(baralla.length);
    var cartatop = baralla[0];

    jugador.push(cartatop);

    baralla.shift();
    
    var capsa = document.createElement("img");

    capsa.setAttribute("src","UNO-resources/"+cartatop.img);

     document.getElementById(contenedor).appendChild(capsa);

}


function botonrobar(num){
    var contenedor1 = "j1";
    var contenedor2 = "j2";
    var contenedor3 = "j3";
    var contenedor4 = "j4";

if(num == 1){
    robar(jugador1, contenedor1)
}else if(num == 2){
    robar(jugador2, contenedor2)
}else if(num == 3){
    robar(jugador3, contenedor3)
}if(num == 4){
    robar(jugador4, contenedor4)
}

}


function iniciPartida(){
    //reiniciar baralla
    //esto no funciona, no reinicia la baraja pero el marc dice que tampoco hace falta asi que asi se queda
    baralla = barallaconst

    //primer buidar els arrays i els flexbox si ja tenen cartes
    
    document.getElementById("j1").innerHTML = '';
    document.getElementById("j2").innerHTML = '';
    document.getElementById("j3").innerHTML = '';
    document.getElementById("j4").innerHTML = '';
    
    jugador1.splice(0, jugador1.length);
    jugador2.splice(0, jugador2.length);
    jugador3.splice(0, jugador3.length);
    jugador4.splice(0, jugador4.length);
    
    //barallar les cartes

    barallar();

    //repartirles a cada jugador si estan online (fare que comencin sempre online de moment per poder fer proves)

    $(".NO").removeClass("NO").addClass("SI");

    $(".SI").html("Online: SI");


    for(i=0; i<6; i++){

        var contenedor1 = "j1";

        robar(jugador1, contenedor1);

    }

    if($("#onlineJ2").hasClass("SI")){

        for(i=0; i<6; i++){

            var contenedor2 = "j2";

            robar(jugador2, contenedor2);

        }

    }

    
    if($("#onlineJ3").hasClass("SI")){

        for(i=0; i<6; i++){

            var contenedor3 = "j3";
 
            robar(jugador3, contenedor3);

        }

    }
    
    if($("#onlineJ4").hasClass("SI")){

        for(i=0; i<6; i++){

            var contenedor4 = "j4";
 
            robar(jugador4, contenedor4);

        }

    }

    console.log(jugador1);

    console.log(jugador2);

    console.log(jugador3);

    console.log(jugador4);

    //mostrar quantes cartes te cada jugador

    muestraCartas();

    //posar una carta random a la taula

    if(cartaMesa.length >= 1){

        cartaMesa.shift();
    }

    console.log(baralla.length);
    var cartatop = baralla[0];

    cartaMesa.push(cartatop);

    console.log(cartaMesa);

    baralla.shift();
    
    $("#cartaTirada").empty();

    var capsa = document.createElement("img");

    capsa.setAttribute("src","UNO-resources/"+cartatop.img);
    
    capsa.setAttribute("id","#cartaMesa");

    capsa.setAttribute("height","100");

    capsa.setAttribute("width","70")

    $("#cartaTirada").append(capsa);
    

}

//no sirve
/*
function imprimebaralla(){

    for(i = 0; i < baralla.length; i++){
        var capsa = document.createElement("img");

        capsa.setAttribute("src","UNO-resources/"+baralla[i].img);

        document.getElementById("Barallaflex").appendChild(capsa);
    }

}
*/

//4

window.onload=function(){
//al pulsar la carta del flex la envia a la mesa
let flex = document.getElementById("j1");

flex.onclick  = function(event){
//selecciono la imagen de la carta pulsada del flex y su correspondiente objeto del array de jugador
  target = event.target
  var cartaSeleccionada = target;
    console.log("index de cartaseleeciconada "+$(cartaSeleccionada).index());
  var posicionObjeto = Number($(cartaSeleccionada).index());

    console.log(posicionObjeto);

  var objeto = jugador1[posicionObjeto];  


  //si se cumplen los requisitos, la carta se tira a la mesa

    var sePuedeTirar = compruebaCarta(objeto, cartaMesa[0]);

    console.log(sePuedeTirar);

    if(sePuedeTirar){

        //borra del array de la carta de la mesa la que haya dentro y pone el nuevo objeto
        cartaMesa.shift();
        
        cartaMesa.push(objeto);

        //ahora lo mismo en el flex de la mesa

        $("#cartaTirada").empty();

        var capsa = document.createElement("img");

        capsa.setAttribute("src", cartaSeleccionada.getAttribute("src"));
        
        capsa.setAttribute("id","#cartaMesa");

        capsa.setAttribute("height","100");

        capsa.setAttribute("width","70")

        $("#cartaTirada").append(capsa);

        //ahora borra la imagen del flex de la mano del jugador y el objeto del array

        cartaSeleccionada.remove();

        jugador1.splice(posicionObjeto, 1);

        /*
        console.log(cartaMesa);
        console.log(posicionObjeto);
        console.log(objeto);
        */
        console.log(jugador1);
        
    }else{
        console.log("tienes que robar una carta o tirar otra, esa no messirve");
    }

}

//al pulsar la carta del mazo robas una carta
let mazo = document.getElementById("mazo");

mazo.onclick = function (event){

    target = event.target
    
    botonrobar(1);

    console.log(jugador1);
    console.log(cartaMesa);

}

//final de la partida

//no funciona, s'ha de fer amb un custom event, no se que es exactament ho fare un altre dia amb ajuda :3

function fiPartida(){

    //quan un jugador es quedi sense cartes s'acaba la partida i surt una alerta

    var guanyador = 0;
    var nom = "a";

    if(jugador1.length != 0 || jugador1.length != 0 || jugador1.length != 0 || jugador1.length != 0){

        if(jugador1.length == 0){
            guanyador = 1;
            nom = $("#nomJ1")
        }else if(jugador2.length == 0){
            guanyador = 2;
            nom = $("#nomJ2")
        }else if(jugador3.length == 0){
            guanyador = 3;
            nom = $("#nomJ3")
        }else if(jugador4.length == 0){
            guanyador = 4;
            nom = $("#nomJ4")
        }

    }

    if(guanyador != 0){

        alert("Ha guanyat el jugador "+guanyador+": "+nom+"!");

    }

}

}





























