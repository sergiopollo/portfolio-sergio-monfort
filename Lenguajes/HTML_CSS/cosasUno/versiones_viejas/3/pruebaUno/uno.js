
const barallaconst = [
    {
        "num":1,
        "color":"red",
        "img":"row-1-col-1.png"
    },
    {
        "num":2,
        "color":"red",
        "img":"row-1-col-2.png"
    },
    {
        "num":3,
        "color":"red",
        "img":"row-1-col-3.png"
    },
    {
        "num":4,
        "color":"red",
        "img":"row-1-col-4.png"
    },
    {
        "num":5,
        "color":"red",
        "img":"row-1-col-5.png"
    },
    {
        "num":6,
        "color":"red",
        "img":"row-1-col-6.png"
    },
    {
        "num":7,
        "color":"red",
        "img":"row-1-col-7.png"
    },
    {
        "num":8,
        "color":"red",
        "img":"row-1-col-8.png"
    },
    {
        "num":9,
        "color":"red",
        "img":"row-1-col-9.png"
    },
    {
        "num":0,
        "color":"red",
        "img":"row-1-col-10.png"
    },
    {
        "num":1,
        "color":"yellow",
        "img":"row-2-col-1.png"
    },
    {
        "num":2,
        "color":"yellow",
        "img":"row-2-col-2.png"
    },
    {
        "num":3,
        "color":"yellow",
        "img":"row-2-col-3.png"
    },
    {
        "num":4,
        "color":"yellow",
        "img":"row-2-col-4.png"
    },
    {
        "num":5,
        "color":"yellow",
        "img":"row-2-col-5.png"
    },
    {
        "num":6,
        "color":"yellow",
        "img":"row-2-col-6.png"
    },
    {
        "num":7,
        "color":"yellow",
        "img":"row-2-col-7.png"
    },
    {
        "num":8,
        "color":"yellow",
        "img":"row-2-col-8.png"
    },
    {
        "num":9,
        "color":"yellow",
        "img":"row-2-col-9.png"
    },
    {
        "num":0,
        "color":"yellow",
        "img":"row-2-col-10.png"
    },
    {
        "num":1,
        "color":"green",
        "img":"row-3-col-1.png"
    },
    {
        "num":2,
        "color":"green",
        "img":"row-3-col-2.png"
    },
    {
        "num":3,
        "color":"green",
        "img":"row-3-col-3.png"
    },
    {
        "num":4,
        "color":"green",
        "img":"row-3-col-4.png"
    },
    {
        "num":5,
        "color":"green",
        "img":"row-3-col-5.png"
    },
    {
        "num":6,
        "color":"green",
        "img":"row-3-col-6.png"
    },
    {
        "num":7,
        "color":"green",
        "img":"row-3-col-7.png"
    },
    {
        "num":8,
        "color":"green",
        "img":"row-3-col-8.png"
    },
    {
        "num":9,
        "color":"green",
        "img":"row-3-col-9.png"
    },
    {
        "num":0,
        "color":"green",
        "img":"row-3-col-10.png"
    },
    {
        "num":1,
        "color":"blue",
        "img":"row-4-col-1.png"
    },
    {
        "num":2,
        "color":"blue",
        "img":"row-4-col-2.png"
    },
    {
        "num":3,
        "color":"blue",
        "img":"row-4-col-3.png"
    },
    {
        "num":4,
        "color":"blue",
        "img":"row-4-col-4.png"
    },
    {
        "num":5,
        "color":"blue",
        "img":"row-4-col-5.png"
    },
    {
        "num":6,
        "color":"blue",
        "img":"row-4-col-6.png"
    },
    {
        "num":7,
        "color":"blue",
        "img":"row-4-col-7.png"
    },
    {
        "num":8,
        "color":"blue",
        "img":"row-4-col-8.png"
    },
    {
        "num":9,
        "color":"blue",
        "img":"row-4-col-9.png"
    },
    {
        "num":0,
        "color":"blue",
        "img":"row-4-col-10.png"
    },
    {
        "num":1,
        "color":"red",
        "img":"row-1-col-1.png"
    },
    {
        "num":2,
        "color":"red",
        "img":"row-1-col-2.png"
    },
    {
        "num":3,
        "color":"red",
        "img":"row-1-col-3.png"
    },
    {
        "num":4,
        "color":"red",
        "img":"row-1-col-4.png"
    },
    {
        "num":5,
        "color":"red",
        "img":"row-1-col-5.png"
    },
    {
        "num":6,
        "color":"red",
        "img":"row-1-col-6.png"
    },
    {
        "num":7,
        "color":"red",
        "img":"row-1-col-7.png"
    },
    {
        "num":8,
        "color":"red",
        "img":"row-1-col-8.png"
    },
    {
        "num":9,
        "color":"red",
        "img":"row-1-col-9.png"
    },
    {
        "num":0,
        "color":"red",
        "img":"row-1-col-10.png"
    },
    {
        "num":1,
        "color":"yellow",
        "img":"row-2-col-1.png"
    },
    {
        "num":2,
        "color":"yellow",
        "img":"row-2-col-2.png"
    },
    {
        "num":3,
        "color":"yellow",
        "img":"row-2-col-3.png"
    },
    {
        "num":4,
        "color":"yellow",
        "img":"row-2-col-4.png"
    },
    {
        "num":5,
        "color":"yellow",
        "img":"row-2-col-5.png"
    },
    {
        "num":6,
        "color":"yellow",
        "img":"row-2-col-6.png"
    },
    {
        "num":7,
        "color":"yellow",
        "img":"row-2-col-7.png"
    },
    {
        "num":8,
        "color":"yellow",
        "img":"row-2-col-8.png"
    },
    {
        "num":9,
        "color":"yellow",
        "img":"row-2-col-9.png"
    },
    {
        "num":0,
        "color":"yellow",
        "img":"row-2-col-10.png"
    },
    {
        "num":1,
        "color":"green",
        "img":"row-3-col-1.png"
    },
    {
        "num":2,
        "color":"green",
        "img":"row-3-col-2.png"
    },
    {
        "num":3,
        "color":"green",
        "img":"row-3-col-3.png"
    },
    {
        "num":4,
        "color":"green",
        "img":"row-3-col-4.png"
    },
    {
        "num":5,
        "color":"green",
        "img":"row-3-col-5.png"
    },
    {
        "num":6,
        "color":"green",
        "img":"row-3-col-6.png"
    },
    {
        "num":7,
        "color":"green",
        "img":"row-3-col-7.png"
    },
    {
        "num":8,
        "color":"green",
        "img":"row-3-col-8.png"
    },
    {
        "num":9,
        "color":"green",
        "img":"row-3-col-9.png"
    },
    {
        "num":0,
        "color":"green",
        "img":"row-3-col-10.png"
    },
    {
        "num":1,
        "color":"blue",
        "img":"row-4-col-1.png"
    },
    {
        "num":2,
        "color":"blue",
        "img":"row-4-col-2.png"
    },
    {
        "num":3,
        "color":"blue",
        "img":"row-4-col-3.png"
    },
    {
        "num":4,
        "color":"blue",
        "img":"row-4-col-4.png"
    },
    {
        "num":5,
        "color":"blue",
        "img":"row-4-col-5.png"
    },
    {
        "num":6,
        "color":"blue",
        "img":"row-4-col-6.png"
    },
    {
        "num":7,
        "color":"blue",
        "img":"row-4-col-7.png"
    },
    {
        "num":8,
        "color":"blue",
        "img":"row-4-col-8.png"
    },
    {
        "num":9,
        "color":"blue",
        "img":"row-4-col-9.png"
    },
    {
        "num":0,
        "color":"blue",
        "img":"row-4-col-10.png"
    },

]

baralla = barallaconst;

//1
function compruebaCarta(){
	var kk = cosa();
  console.log(kk);
}

function cosa(){

	var CT = document.getElementById("CT").value;
	var NT = document.getElementById("NT").value;
	var CM = document.getElementById("CM").value;
	var NM = document.getElementById("NM").value;
	
  var tirar = false;
  
	if(CT == CM || NT == NM || CM == "negre"){
  	
    tirar = true;
    
  }
return tirar;
}

//2

function añadecarta(){

 var random = Math.floor(Math.random() * baralla.length); 

 var capsa = document.createElement("img");

capsa.setAttribute("src","UNO-resources/"+baralla[random].img);

 document.getElementById("flex-container1").appendChild(capsa);


}


//3

function barallar(){

    for (i = baralla.length -1; i > 0; i--) {
      j = Math.floor(Math.random() * i)
      k = baralla[i]
      baralla[i] = baralla[j]
      baralla[j] = k
    }


}


//arrays dels 4 jugadors


    jugador1 = [],
    jugador2 = [],
    jugador3 = [],
    jugador4 = []



function robar(jugador, contenedor){
    console.log(baralla.length);
    var cartatop = baralla[0];

    jugador.push(cartatop);

    baralla.shift();
    

    var capsa = document.createElement("img");

    capsa.setAttribute("src","UNO-resources/"+cartatop.img);
    
     document.getElementById(contenedor).appendChild(capsa);

    

}


function botonrobar(num){
    var contenedor1 = "flex-containerj1";
    var contenedor2 = "flex-containerj2";
    var contenedor3 = "flex-containerj3";
    var contenedor4 = "flex-containerj4";
if(num == 1){
    robar(jugador1, contenedor1)
}


}


function iniciPartida(){
    //reiniciar baralla
    //esto no funciona, no reinicia la baraja pero el marc dice que tampoco hace falta asi que asi se queda
    baralla = barallaconst

    //primer buidar els arrays si ja tenen cartes
    
    document.getElementById("flex-containerj1").innerHTML = '';
    document.getElementById("flex-containerj2").innerHTML = '';
    document.getElementById("flex-containerj3").innerHTML = '';
    document.getElementById("flex-containerj4").innerHTML = '';

    //barallar les cartes

    barallar();

    //repartirles a cada jugador
    for(i=0; i<6; i++){

        var contenedor1 = "flex-containerj1";
        var contenedor2 = "flex-containerj2";
        var contenedor3 = "flex-containerj3";
        var contenedor4 = "flex-containerj4";

        robar(jugador1,contenedor1);
        robar(jugador2, contenedor2);
        robar(jugador3, contenedor3);
        robar(jugador4, contenedor4);
    }

    console.log(jugador1);

    console.log(jugador2);

    console.log(jugador3);

    console.log(jugador4);



}


function imprimebaralla(){

    for(i = 0; i < baralla.length; i++){
        var capsa = document.createElement("img");

        capsa.setAttribute("src","UNO-resources/"+baralla[i].img);

        document.getElementById("flex-containerB").appendChild(capsa);
    }

}


//4































