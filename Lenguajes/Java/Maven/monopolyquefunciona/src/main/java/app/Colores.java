package app;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "colores")
public class Colores {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_color")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
		@ManyToOne
		@JoinColumn(name="id_jugador", nullable=true)
		@JsonBackReference
		private Jugadores propietarioColor;

		public Jugadores getPropietarioColor() {
			return propietarioColor;
		}

		public void setPropietarioColor(Jugadores propietarioColor) {
			this.propietarioColor = propietarioColor;
		}
		
		//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
		@OneToMany(mappedBy= "color")
		private Set<Propiedades> propiedad;
		
		public Set<Propiedades> getPropiedad() {
			return propiedad;
		}

		public void setPropiedad(Set<Propiedades> propiedad) {
			this.propiedad = propiedad;
		}

	public Colores() {
		super();

		this.propiedad=new HashSet<Propiedades>();
	}

	public Colores(String nombre) {
		super();
		this.nombre = nombre;

		this.propiedad=new HashSet<Propiedades>();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void addPropiedad(Propiedades p) {
		this.propiedad.add(p);
	}
	
	
	
	
	
}
