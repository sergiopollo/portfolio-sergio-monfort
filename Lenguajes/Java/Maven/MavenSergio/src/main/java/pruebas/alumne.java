package pruebas;
//etiquetas

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//raiz, se pone sobre la clase
@XmlRootElement
public class alumne implements Comparable<alumne>{
	
	private String nom;
	private String cognoms;
	private String DNI;
	private String adreca;
	private ArrayList<Integer> telefons;
	private String mail;
	
	//coinstructores
	public alumne() {
		super();
	}

	public alumne(String nom, String cognoms, String dni, String adreca, ArrayList<Integer> telefons, String mail) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.DNI = dni;
		this.adreca = adreca;
		this.telefons = telefons;
		this.mail = mail;
	}
	//getters y setters
    @XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
    @XmlElement
	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
    @XmlElement
	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}
    @XmlElement
	public String getAdreca() {
		return adreca;
	}
    
	public void setAdreca(String adreca) {
		this.adreca = adreca;
	}
	//wrapper: contiene elementos xml
    @XmlElementWrapper(name="telefons")
    @XmlElement(name="telefon")
	public ArrayList<Integer> getTelefons() {
		return telefons;
	}

	public void setTelefons(ArrayList<Integer> telefons) {
		this.telefons = telefons;
	}
    @XmlElement
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "alumne [nom=" + nom + ", cognoms=" + cognoms + ", DNI=" + DNI + ", adreca=" + adreca + ", telefons="
				+ telefons + ", mail=" + mail + "]";
	}

	@Override
	public int compareTo(alumne o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
	
	
}
