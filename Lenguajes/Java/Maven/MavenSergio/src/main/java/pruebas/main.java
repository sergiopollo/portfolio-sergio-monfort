package pruebas;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class main {

	public static void main(String[] args) throws JAXBException{
		// TODO Auto-generated method stub
		
		//XMl
		String nombreFicheroXML = "alumnes.xml";
		/*
		Institut ins = llegirXML(nombreFicheroXML);
		System.out.println(ins);
		
		ArrayList<Integer> listaTelefons = new ArrayList<Integer>();
		listaTelefons.add(666666666);
		alumne alum = new alumne("sergio", "monfort", "23984792384B", "carrer dels carrers carrer num carrer", listaTelefons, "asdasdas@gmail.com");
		
		afegirAlumneXML(nombreFicheroXML, alum);
		
		System.out.println(llegirXML(nombreFicheroXML));
		*/
		//JSON
		String nombreFicheroJSON = "alumnes.JSON";
		
		JSONArray listaFichero = llegirJSON(nombreFicheroJSON);
		System.out.println(listaFichero);
		
		//afegirAlumneJSON(nombreFicheroJSON, "polloloco100", true, 9999);
		
		System.out.println(llegirJSON(nombreFicheroJSON));
		
	}
	
	public static Institut llegirXML(String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(Institut.class);
		//reader (unmarshaller)
		Unmarshaller um = context.createUnmarshaller();
		Institut institut = (Institut) um.unmarshal(f);
		
		return institut;
	}
	
	public static void escriureXML(Institut institut, String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(Institut.class);
		//writer (marshaller)
		Marshaller m = context.createMarshaller();
		m.marshal(institut, f);
		
	}
	
	public static void afegirAlumneXML(String nombreFichero, alumne alumne) throws JAXBException {
		
		Institut ins = llegirXML(nombreFichero);
		
		ArrayList<alumne> alumnes = ins.getAlumnes();
		
		alumnes.add(alumne);
		
		ins.setAlumnes(alumnes);
		
		escriureXML(ins, nombreFichero);
		System.out.println("alumne afegit yay");
	}
	
	public static JSONArray llegirJSON(String nomFitxer) {

		//llegir JSON
		JSONParser parser = new JSONParser();
		
		try {
			
			JSONArray lista = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
			return lista;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void escriureJSON(JSONArray lista, String nombreFichero) {

		try (FileWriter file = new FileWriter(nombreFichero)) {

			file.write(lista.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void afegirAlumneJSON(String nomFitxer, String nomAlumne, boolean tonto, int numCagadas) {
		
		 JSONArray lista = llegirJSON(nomFitxer);
		 
		 JSONObject alumne = new JSONObject();
		 
		 alumne.put("nom", nomAlumne);
		 alumne.put("tonto", tonto);
		 alumne.put("numCagadas", numCagadas);
		 alumne.put("nums", new ArrayList<Double>());
		 
		 lista.add(alumne);
		 
		 escriureJSON(lista, nomFitxer);
		
	}

}
