package proyectoFicheros;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas

//raiz, se pone sobre la clase
@XmlRootElement
public class Joc {
	
	//las etiquetas se ponen en los getters
	
	private ArrayList<jugador> jugadors;

	//constructores
	public Joc(ArrayList<jugador> jugadors) {
		super();
		this.jugadors = jugadors;
	}
	
	public Joc() {
		super();
	}
	
	//getters y setters
	//wrapper: contiene elementos xml
	@XmlElementWrapper(name="jugadors")
	@XmlElement(name="jugador")
	public ArrayList<jugador> getJugadors() {
		return jugadors;
	}

	public void setJugadors(ArrayList<jugador> jugadors) {
		this.jugadors = jugadors;
	}

	//toString
	@Override
	public String toString() {
		return "Joc [jugadors=" + jugadors + "]";
	}

}
