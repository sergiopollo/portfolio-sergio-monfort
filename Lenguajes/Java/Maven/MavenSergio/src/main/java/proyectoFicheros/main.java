package proyectoFicheros;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;



public class main {

	public static void main(String[] args) throws JAXBException {
		
		
		//XMl
		String nombreFicheroXML = "joc.xml";
		/*
		Joc joc = llegirXML(nombreFichero);
		
		try {
			alCarrer(nombreFichero, "carmen");
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	
		try {
			AfegeixPuntuacio(nombreFichero, "carmen", 99999);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		mostrarClassificacio(nombreFichero);
		*/
		
		/*
		String nomJug = "Dani Fernandez";
		//String nomJug = "Pakito";
		
		try {
			canviarInicials(nombreFichero, nomJug, "SAD");
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//prueba funcion afegeixjugador
		ArrayList<Integer> listaPunts = new ArrayList<Integer>();
		listaPunts.add(234);
		listaPunts.add(234672347);
		
		configuracio conf = new configuracio(false,"sad","ad");
		
		jugador j1 = new jugador("carmen", "DUR", 23423, listaPunts, conf);
		
		afegeixJugador(nombreFichero, j1);
		*/
		
		
		//JSON
		String nombreFicheroJSON = "enemics.JSON";
		//llegirJSON(nombreFichero2);
		//LlistarEnemics(nombreFichero2);
		//afegirEnemic(nombreFichero2, "Koopa", false, 6345);
		//AfegirKill(nombreFichero2, "Goomba", 0.5);
		//System.out.println(calcularPuntuacio(nombreFichero2));
		//AfegirPuntuacioEnBaseAPartida(nombreFicheroXML, "carmen", nombreFicheroJSON);
		//fgdg
		
	}
	
	public static Joc llegirXML(String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(Joc.class);
		//reader (unmarshaller)
		Unmarshaller um = context.createUnmarshaller();
		Joc joc = (Joc) um.unmarshal(f);
		
		return joc;
	}
	
	public static void escriureXML(Joc joc, String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(Joc.class);
		//writer (marshaller)
		Marshaller m = context.createMarshaller();
		m.marshal(joc, f);
		
	}
	
	public static jugador getJugador(Joc joc, String nomJugador) {
		
		ArrayList<jugador> listaJug = joc.getJugadors();
		
		for (int i = 0; i < listaJug.size(); i++) {
			
			String nomJug = listaJug.get(i).getNom();
			
			if(nomJug.equals(nomJugador)) {
				return listaJug.get(i);
			}
			
		}
		
		return null;
	}
	
	public static void canviarInicials(String nomFitxer, String nomJugador, String novesInicials) throws JAXBException, MyException {
		
		Joc joc = llegirXML(nomFitxer);
		
		ArrayList<jugador> listaJug = joc.getJugadors();
		
		jugador jug = getJugador(joc, nomJugador);
		
		int index = listaJug.indexOf(jug);
		
		if(jug != null) {
			
			jug.setInicials(novesInicials);
		
			listaJug.set(index, jug);
			
			joc.setJugadors(listaJug);
			
			escriureXML(joc, nomFitxer);
			
		}else {
			throw new MyException();
		}
		
	}
	
	public static void toggleDaltonic(String nomFitxer, String nomJugador) throws JAXBException, MyException {

        Joc joc = llegirXML(nomFitxer);

        ArrayList<jugador> listaJug = joc.getJugadors();

        jugador jug = getJugador(joc, nomJugador);



        if (jug != null) {

            int index = listaJug.indexOf(jug);
            System.out.println("Cambiando el modo daltonico");
            configuracio conf = jug.getConfiguracio();
            conf.setMode_daltonic(!conf.isMode_daltonic());

            jug.setConfiguracio(conf);

            listaJug.set(index, jug);

            joc.setJugadors(listaJug);

            escriureXML(joc, nomFitxer);

        } else {
            throw new MyException();
        }

    }
	
	public static void afegeixJugador(String nomFitxer, jugador jug) throws JAXBException {
		
		Joc joc = llegirXML(nomFitxer);
		
		ArrayList<jugador> listaJug = joc.getJugadors();
		
		//le pongo pos y punts a 0
		jug.setPosicio(0);
		ArrayList<Integer> listaPunts = new ArrayList<Integer>();
		listaPunts.add(0);
		jug.setPuntuacions(listaPunts);
		
		listaJug.add(jug);
		joc.setJugadors(listaJug);
		
		escriureXML(joc, nomFitxer);
		System.out.println("jugador añadido!!!!");

	}
	
	public static void mostrarClassificacio(String nomFitxer) throws JAXBException {

        Joc joc = llegirXML(nomFitxer);

        ArrayList<jugador> listaJug = joc.getJugadors();

        Collections.sort(listaJug);

        jugador jug;

        for (int i = 0; i < listaJug.size(); i++) {

            jug = listaJug.get(i);

            System.out.println("[" + jug.getPosicio() + "] Nom: " + jug.getNom() + ", Inicials: " + jug.getInicials()
                    + ",  Puntuacions: " + jug.getPuntuacions());

        }
    }

	public static Joc actualizarClassificacio(Joc joc) {

        ArrayList<jugador> listaJug = joc.getJugadors();

        //ArrayList<jugador> novalistaJug = listaJug;

        for (int i = 0; i < listaJug.size(); i++) {

            listaJug.get(i).setPosicio(0);

        }

        Collections.sort(listaJug);
        
        for (int i = 0; i < listaJug.size(); i++) {

            listaJug.get(i).setPosicio(i+1);

        }
        
        joc.setJugadors(listaJug);
        System.out.println(joc);
        return joc;

    }

	public static void alCarrer(String nomFitxer, String nomJugador) throws MyException, JAXBException {
	
		Joc joc = llegirXML(nomFitxer);
		
		ArrayList<jugador> listaJug = joc.getJugadors();
		
		jugador jug = getJugador(joc, nomJugador);
		
		int indexJug = listaJug.indexOf(jug);
		
		if(jug != null) {
		
			listaJug.remove(indexJug);
			
			joc.setJugadors(listaJug);
			
			joc = actualizarClassificacio(joc);
			
			escriureXML(joc, nomFitxer);
			
		}else {
			throw new MyException();
		}
		
		
	}
	
	public static void AfegeixPuntuacio(String nomFitxer, String nomJugador, int punt) throws MyException, JAXBException{

        Joc joc = llegirXML(nomFitxer);

        ArrayList<jugador> listaJug = joc.getJugadors();

        jugador jug = getJugador(joc, nomJugador);

        int index = listaJug.indexOf(jug);

        if (jug != null) {

            ArrayList<Integer> aux = jug.getPuntuacions();
            aux.add(punt);
            jug.setPuntuacions(aux);
            listaJug.set(index, jug);
            joc.setJugadors(listaJug);

            joc = actualizarClassificacio(joc);

            escriureXML(joc, nomFitxer);

        } else {
            throw new MyException();
        }
    }
	
	public static JSONArray llegirJSON(String nomFitxer) {

		//llegir JSON
		JSONParser parser = new JSONParser();
		
		try {
			
			JSONArray lista = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
			return lista;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void escriureJSON(JSONArray lista, String nombreFichero) {

		try (FileWriter file = new FileWriter(nombreFichero)) {

			file.write(lista.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void LlistarEnemics(String nomFitxer) {
		//tiene que sacar por pantalla solo el nombre, puntos base y num de muertes
		
		JSONArray lista = llegirJSON(nomFitxer);
		
		for (Object enemigoObject : lista) {
			JSONObject enemigo = (JSONObject) enemigoObject;
			//accedo a sus componentes con enemigo.get("nombreDelTag"); como en un diccionario
			String nom = (String) enemigo.get("nom");
			//ME OBLIGA A PONERLOS COMO LONG, SI LOS PONGO COMO INT DA ERROR
			Long puntsBase = (Long) enemigo.get("puntsBase");
			Long numMorts = (Long) enemigo.get("numMorts");
			
			System.out.println(nom+" "+puntsBase+" "+numMorts);
			
		}
		
	}
	
	public static void afegirEnemic(String nomFitxer, String nomEnemic, boolean elite, int puntsBase) {
		
		 JSONArray lista = llegirJSON(nomFitxer);
		 
		 JSONObject enemigo = new JSONObject();
		 
		 enemigo.put("nom", nomEnemic);
		 enemigo.put("elite", elite);
		 enemigo.put("puntsBase", puntsBase);
		 enemigo.put("numMorts", 0);
		 enemigo.put("bonus", new ArrayList<Double>());
		 
		 lista.add(enemigo);
		 
		 escriureJSON(lista, nomFitxer);
		
	}
	
	public static void AfegirKill(String nomFitxer, String nomEnemic, double bonus) {

        JSONArray lista = llegirJSON(nomFitxer);

           for (Object enemigoObject : lista) {

               JSONObject enemigo = (JSONObject) enemigoObject;

               if(((String)enemigo.get("nom")).equals(nomEnemic)) {
                   Long numMorts = (Long) enemigo.get("numMorts");
                   numMorts++;
                   enemigo.put("numMorts", numMorts);
                   JSONArray auxlist = (JSONArray) enemigo.get("bonus");
                   //System.out.println(auxlist);
                   auxlist.add(bonus);
                   enemigo.put("bonus", auxlist);
                   lista.set(lista.indexOf(enemigoObject), enemigo);
               }
           }    escriureJSON(lista, nomFitxer);

   }
	
	public static int calcularPuntuacio(String nomFitxer){
		
		double puntuacionTotal = 0;
		
		JSONArray lista = llegirJSON(nomFitxer);
		
		for (Object enemigoObject : lista) {
			JSONObject enemigo = (JSONObject) enemigoObject;
			JSONArray listaBonus = (JSONArray) enemigo.get("bonus");
			double puntsBase =  (long) enemigo.get("puntsBase")*1.0;
			boolean elite = (boolean) enemigo.get("elite");
			double puntuacionEnemigo = 0;
			
			for (Object bonusObject : listaBonus) {
				
				if(bonusObject instanceof Long) {
					
					double bonus = (long) bonusObject*1.0;
					
					bonus=bonus*puntsBase;
				
					puntuacionEnemigo=puntuacionEnemigo+bonus;
					
				}else {
					
					double bonus = (double) bonusObject;
					
					bonus=bonus*puntsBase;
				
					puntuacionEnemigo=puntuacionEnemigo+bonus;
				}
				
			}
			
			if(elite) {
				puntuacionEnemigo=puntuacionEnemigo*2;
			}
			
			puntuacionTotal=puntuacionTotal+puntuacionEnemigo;
			
		}
		
		int puntuacionTotalInt = (int) puntuacionTotal;
		
		return puntuacionTotalInt;
	}
	
	public static void AfegirPuntuacioEnBaseAPartida(String nomFitxerXML, String nomJugador, String nomFitxerJSON) {
		
		int puntuacioJugador = calcularPuntuacio(nomFitxerJSON);
		try {
			AfegeixPuntuacio(nomFitxerXML, nomJugador, puntuacioJugador);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
}
