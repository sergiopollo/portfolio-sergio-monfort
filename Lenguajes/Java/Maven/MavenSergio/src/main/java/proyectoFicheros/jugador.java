package proyectoFicheros;

import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class jugador implements Comparable<jugador> {
    
    private String nom;
    private String inicials;
    private int posicio;
    private ArrayList<Integer> puntuacions;
    private configuracio configuracio;
    
    
    //constructores
    public jugador() {
        super();
    }

    public jugador(String nom, String inicials, int posicio, ArrayList<Integer> puntuacions, configuracio configuracio) {
        super();
        this.nom = nom;
        this.inicials = inicials;
        this.posicio = posicio;
        this.puntuacions = puntuacions;
        this.configuracio = configuracio;
    }

    //getters y setters
    @XmlElement
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    @XmlElement
    public String getInicials() {
        return inicials;
    }

    public void setInicials(String inicials) {
        this.inicials = inicials;
    }
    @XmlElement
    public int getPosicio() {
        return posicio;
    }

    public void setPosicio(int posicio) {
        this.posicio = posicio;
    }
    //wrapper: contiene elementos xml
    @XmlElementWrapper(name="puntuacions")
    @XmlElement(name="puntuacio")
    public ArrayList<Integer> getPuntuacions() {
        return puntuacions;
    }

    public void setPuntuacions(ArrayList<Integer> puntuacions) {
        this.puntuacions = puntuacions;
    }

    @XmlElement
    public configuracio getConfiguracio() {
        return configuracio;
    }

    public void setConfiguracio(configuracio configuracio) {
        this.configuracio = configuracio;
    }

    //toString
    @Override
    public String toString() {
        return "jugador [nom=" + nom + ", inicials=" + inicials + ", posicio=" + posicio + ", puntuacions="
                + puntuacions + ", configuracio=" + configuracio + "]";
    }

    @Override
    public int compareTo(jugador o) {
        
        int posicio = Integer.compare(this.getPosicio(), o.getPosicio());
        
        if(posicio == 0) {
            
            Collections.sort(this.getPuntuacions());
            int p1 = this.getPuntuacions().get(this.puntuacions.size()-1);
            Collections.sort(o.getPuntuacions());
            int p2 = o.getPuntuacions().get(o.puntuacions.size()-1);
            int max = Integer.compare(p1,p2);
            return max*-1;
        }
        return posicio;
    }
    
}