package examenFicheros;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas

//raiz, se pone sobre la clase
@XmlRootElement
public class classe {
	
	private String nom;
	private ArrayList<alumne> alumnes;
	
	public classe() {
		super();
	}
	
	public classe(String nom, ArrayList<alumne> alumnes) {
		super();
		this.nom = nom;
		this.alumnes = alumnes;
	}
	@XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	//wrapper: contiene elementos xml
	@XmlElementWrapper(name="alumnes")
	@XmlElement(name="alumne")
	public ArrayList<alumne> getAlumnes() {
		return alumnes;
	}

	public void setAlumnes(ArrayList<alumne> alumnes) {
		this.alumnes = alumnes;
	}

	@Override
	public String toString() {
		return "classe [nom=" + nom + ", alumnes=" + alumnes + "]";
	}

}
