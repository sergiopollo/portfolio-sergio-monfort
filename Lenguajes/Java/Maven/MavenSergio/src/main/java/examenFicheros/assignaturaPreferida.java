package examenFicheros;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas

//raiz, se pone sobre la clase
@XmlRootElement
public class assignaturaPreferida {
	
	private String codi;
	private String nom;
	private int horesSetmana;
	
	public assignaturaPreferida() {
		super();
	}
	
	public assignaturaPreferida(String codi, String nom, int horesSetmana) {
		super();
		this.codi=codi;
		this.nom = nom;
		this.horesSetmana = horesSetmana;
	}
	
	@XmlAttribute(name = "codi")
	public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}
	@XmlElement(name="nom")
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlElement(name="horesSetmana")
	public int getHoresSetmana() {
		return horesSetmana;
	}
	public void setHoresSetmana(int horesSetmana) {
		this.horesSetmana = horesSetmana;
	}

	@Override
	public String toString() {
		return "assignaturaPreferida [codi=" + codi + ", nom=" + nom + ", horesSetmana=" + horesSetmana + "]";
	}
	
	
	
}
