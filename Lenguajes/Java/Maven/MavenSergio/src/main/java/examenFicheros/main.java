package examenFicheros;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class main {

	public static void main(String[] args) throws JAXBException {
		
		//XMl
		String nombreFicheroXML = "classe.xml";
		String nombreFicheroXML2 = "alumne.xml";
		
		classe classe = llegirXML(nombreFicheroXML);
		
		//System.out.println(classe);
		
		//crearCopiaXML(nombreFicheroXML);
		
		//afegirVotXML(nombreFicheroXML, "Iker", true);
		//System.out.println(llegirXML(nombreFicheroXML));
		
		//JSON
		String nombreFicheroJSON = "classe.JSON";
		String nombreFicheroJSON2 = "alumne.JSON";
		
		//crearCopiaJSON(nombreFicheroJSON);
		
		//afegirVotJSON(nombreFicheroJSON, "Iker", true);
		
		//System.out.println(llegirJSON(nombreFicheroJSON));

		//JSON2XML(nombreFicheroJSON2);
		
		//XML2JSON(nombreFicheroXML2);
		
	}
	
	public static classe llegirXML(String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(classe.class);
		//reader (unmarshaller)
		Unmarshaller um = context.createUnmarshaller();
		classe classe = (classe) um.unmarshal(f);
		
		return classe;
	}
	
	public static void escriureXML(classe classe, String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(classe.class);
		//writer (marshaller)
		Marshaller m = context.createMarshaller();
		m.marshal(classe, f);
		
	}
	
	public static alumne llegirXMLAlumne(String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(alumne.class);
		//reader (unmarshaller)
		Unmarshaller um = context.createUnmarshaller();
		alumne alumne = (alumne) um.unmarshal(f);
		
		return alumne;
	}
	
	public static void escriureXMLAlumne(alumne alumne, String nombreFichero) throws JAXBException {
		
		File f = new File(nombreFichero);
		JAXBContext context = JAXBContext.newInstance(alumne.class);
		//writer (marshaller)
		Marshaller m = context.createMarshaller();
		m.marshal(alumne, f);
		
	}
	
	public static void crearCopiaXML(String nombreFichero) throws JAXBException {
		
		classe classe = llegirXML(nombreFichero);
		
		ArrayList<alumne> alumnes = classe.getAlumnes();
		
		for (alumne alumne : alumnes) {
			System.out.println(alumne.getNom());
		}
		
		escriureXML(classe, "classe_copia.xml");
		
	}
	
	public static void afegirVotXML(String nombreFichero, String nomAlumne, boolean vot) throws JAXBException {
		
		classe classe = llegirXML(nombreFichero);
		
		ArrayList<alumne> alumnes = classe.getAlumnes();
		
		for (alumne alumne : alumnes) {
			
			if(alumne.getNom().equals(nomAlumne)) {
				ArrayList<Boolean> vots = alumne.getVots();
				vots.add(vot);
				alumne.setVots(vots);
			}
			
		}
		
		classe.setAlumnes(alumnes);
		
		escriureXML(classe, nombreFichero);
		
	}
	
	public static JSONObject llegirJSON(String nomFitxer) {

		//llegir JSON
		JSONParser parser = new JSONParser();
		
		try {
			
			JSONObject lista = (JSONObject) parser.parse(new FileReader(nomFitxer));
		
			return lista;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void escriureJSON(JSONObject lista, String nombreFichero) {

		try (FileWriter file = new FileWriter(nombreFichero)) {

			file.write(lista.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void crearCopiaJSON(String nombreFichero) {
		
		JSONObject classe = llegirJSON(nombreFichero);
		
		JSONArray listaAlumnes = (JSONArray) classe.get("alumnes");
		
		for (Object alumneObject : listaAlumnes) {
			JSONObject alumne = (JSONObject) alumneObject;
			
			String nom = (String) alumne.get("nom");
		
			System.out.println(nom);
		}
		
		escriureJSON(classe, "classe_copia.JSON");
		
	}
	
	public static void afegirVotJSON(String nombreFichero, String nomAlumne, boolean vot) {
		
		JSONObject classe = llegirJSON(nombreFichero);
		
		JSONArray listaAlumnes = (JSONArray) classe.get("alumnes");
		
		for (Object alumneObject : listaAlumnes) {
			JSONObject alumne = (JSONObject) alumneObject;
			
			String nom = (String) alumne.get("nom");
			
			if(nom.equals(nomAlumne)) {
				
				ArrayList<Boolean> vots = (ArrayList<Boolean>) alumne.get("vots");
				
				vots.add(vot);
				
				alumne.put("vots", vots);

			}
			
		}
		
		escriureJSON(classe, nombreFichero);
		
	}
	
	public static void JSON2XML(String nombreFicheroJSON) throws JAXBException {
		
		JSONObject alumne = llegirJSON(nombreFicheroJSON);
		
		String nom = (String) alumne.get("nom");
		Long edat = (Long) alumne.get("edat");
		int edatInt = edat.intValue();
		double notaMitja = (double) alumne.get("notaMitja");
		JSONObject assignaturaPreferidaJSON = (JSONObject) alumne.get("assignaturaPreferida");
		
		String codi = (String) assignaturaPreferidaJSON.get("codi");
		String nomAssignaturaPreferida = (String) assignaturaPreferidaJSON.get("nom");
		Long horesSetmana = (Long) assignaturaPreferidaJSON.get("horesSetmana");
		int horesSetmanaInt = horesSetmana.intValue();
		
		assignaturaPreferida assignaturaPreferida = new assignaturaPreferida(codi, nomAssignaturaPreferida, horesSetmanaInt);
		
		ArrayList<Boolean> vots = (ArrayList<Boolean>) alumne.get("vots");
		
		
		alumne alumneXML = new alumne(nom, edatInt, notaMitja, assignaturaPreferida, vots);
		
		escriureXMLAlumne(alumneXML, "alumne_traspassat.xml");
		
	}
	
	public static void XML2JSON(String nombreFicheroXml) throws JAXBException{
		
		alumne alumneXML = llegirXMLAlumne(nombreFicheroXml);
		
		String nom = alumneXML.getNom();
		int edat = alumneXML.getEdat();
		double notaMitja = alumneXML.getNotaMitja();
		assignaturaPreferida assignaturaPreferida = alumneXML.getAssignaturaPreferida();
		ArrayList<Boolean> vots = alumneXML.getVots();
		
		JSONObject alumneJSON = new JSONObject();
		
		alumneJSON.put("nom", nom);
		alumneJSON.put("edat", edat);
		alumneJSON.put("notaMitja", notaMitja);
		
		JSONObject assignaturaPreferidaJSON = new JSONObject();
		
			String codi = assignaturaPreferida.getCodi();
			String nomAssignaturaPreferida = assignaturaPreferida.getNom();
			int horesSetmana = assignaturaPreferida.getHoresSetmana();
			
			assignaturaPreferidaJSON.put("codi", codi);
			assignaturaPreferidaJSON.put("nom", nomAssignaturaPreferida);
			assignaturaPreferidaJSON.put("horesSetmana", horesSetmana);
	
		alumneJSON.put("assignaturaPreferida", assignaturaPreferidaJSON);
		alumneJSON.put("vots", vots);
		
		escriureJSON(alumneJSON, "alumne_traspassat.JSON");
		
	}
	
	public static void eliminaVotFalse(String nombreFichero) {
		
		JSONObject classe = llegirJSON(nombreFichero);
		/*
		for (iterable_type iterable_element : iterable) {
			
			
			
		}
		*/
	}
	
}
