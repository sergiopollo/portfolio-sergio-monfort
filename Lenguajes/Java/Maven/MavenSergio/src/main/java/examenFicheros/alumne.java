package examenFicheros;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas

//raiz, se pone sobre la clase
@XmlRootElement
public class alumne {
	
	private String nom;
	private int edat;
	private double notaMitja;
	private assignaturaPreferida assignaturaPreferida;
	private ArrayList<Boolean> vots;
	
	public alumne() {
		super();
	}
	
	public alumne(String nom, int edat, double notaMitja, examenFicheros.assignaturaPreferida assignaturaPreferida,
			ArrayList<Boolean> vots) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.notaMitja = notaMitja;
		this.assignaturaPreferida = assignaturaPreferida;
		this.vots = vots;
	}
	
	@XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	@XmlElement
	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}
	@XmlElement
	public double getNotaMitja() {
		return notaMitja;
	}

	public void setNotaMitja(double notaMitja) {
		this.notaMitja = notaMitja;
	}
	@XmlElement
	public assignaturaPreferida getAssignaturaPreferida() {
		return assignaturaPreferida;
	}

	public void setAssignaturaPreferida(assignaturaPreferida assignaturaPreferida) {
		this.assignaturaPreferida = assignaturaPreferida;
	}
	//wrapper: contiene elementos xml
	@XmlElementWrapper(name="vots")
	@XmlElement(name="vot")
	public ArrayList<Boolean> getVots() {
		return vots;
	}

	public void setVots(ArrayList<Boolean> vots) {
		this.vots = vots;
	}

	@Override
	public String toString() {
		return "alumne [nom=" + nom + ", edat=" + edat + ", notaMitja=" + notaMitja + ", assignaturaPreferida="
				+ assignaturaPreferida + ", vots=" + vots + "]";
	}

}
