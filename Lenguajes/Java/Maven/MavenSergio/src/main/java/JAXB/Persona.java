package JAXB;


import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//etiquetas

//raiz, se pone sobre la clase
@XmlRootElement
public class Persona {
	
	//las etiquetas se ponen en los getters
	
	private String nom;

	private int edat;

	private String sexe;

	private ArrayList<Dret> drets;
	
	//constructores
	public Persona(String nom, int edat, String sexe, ArrayList<Dret> drets) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.sexe = sexe;
		this.drets = drets;
	}
	public Persona() {
		super();
	}
	
	//getters y setters
	@XmlElement
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	//ejemplo: por si los nombres son diferentes se le puede poner el tag name con el nombre del campo en el fichero xml
	@XmlElement(name="edat")
	public int getEdat() {
		return edat;
	}
	public void setEdat(int edat) {
		this.edat = edat;
	}
	@XmlElement
	public String getSexe() {
		return sexe;
	}
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	//wrapper: contiene elementos xml
	@XmlElementWrapper(name="drets")
	@XmlElement(name="dret")
	public ArrayList<Dret> getDrets() {
		return drets;
	}
	public void setDrets(ArrayList<Dret> drets) {
		this.drets = drets;
	}
	
	//toString
	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", edat=" + edat + ", sexe=" + sexe + ", drets=" + drets + "]";
	}
	
}

