package JAXB;
import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class XML2Clase {
	
	public static void main(String[] args) throws JAXBException {
		
		File f = new File("dani.xml");
		JAXBContext context = JAXBContext.newInstance(Persona.class);
		//reader (unmarshaller)
		Unmarshaller um = context.createUnmarshaller();
		Persona dani = (Persona) um.unmarshal(f);
		
		System.out.println(dani);
		
		//cambiar y meter cosas en "persona"
		dani.setEdat(999);
		Dret d = new Dret("Dret a autistejar", 2021);
		dani.getDrets().add(d);
		
		System.out.println(dani);
		
		//luego hay que escribirlo en el archivo con el writer (marshaller)
		Marshaller m = context.createMarshaller();
		m.marshal(dani, f);
		
	}
	
}
