package JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dret {
	
	private String nom;
	private int concedit;
	
	
	public Dret() {
		super();
	}
	public Dret(String nom, int concedit) {
		super();
		this.nom = nom;
		this.concedit = concedit;
	}

	@XmlElement
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement
	public int getConcedit() {
		return concedit;
	}
	public void setConcedit(int concedit) {
		this.concedit = concedit;
	}
	@Override
	public String toString() {
		return "Dret [nom=" + nom + ", concedit=" + concedit + "]";
	}



}
