package examen;

public class AchievementPick extends Achievement {
	
	private int onPick;
	private int onMaxPick;
	private Object objecte;
	

	public AchievementPick(String nom,int onMaxPick, Object objecte) {
		super(nom);
		this.onMaxPick=onMaxPick;
		this.onPick=0;
		this.objecte=objecte;
		EventManager.getInstance().subscribe("OnPick", this);
	}

	public int getOnMaxcatch() {
		return onMaxPick;
	}

	public void setOnMaxPick(int onMaxPick) {
		this.onMaxPick = onMaxPick;
	}

	public int getOnPick() {
		return onPick;
	}
	
	public Object getObjecte() {
		return objecte;
	}

	public void setObjecte(Object objecte) {
		this.objecte = objecte;
	}

	@Override
	public void process() {
		
		onPick++;
		if(onPick==onMaxPick) {
			giveRewards();
		}
	}

	@Override
	public void notifyObserver(String event, Object objecte) {
		//no hace falta que lo compruebe porque el eventManager solo avisa a este si es el evento que quiero
		//if(event.equals("OnPick") && objecte.equals(this.objecte)) {
		if(objecte.equals(this.objecte)) {
			process();
		}
		
	}

	@Override
	public String toString() {
		return "AchievementPick ["+ super.toString() + ", objecte"+this.objecte+"]";
	}

	
	
	
}
