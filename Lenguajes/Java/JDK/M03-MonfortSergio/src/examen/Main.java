package examen;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
		EventManager eventManager = EventManager.getInstance();
	
		AchievementManager achievementManager = new AchievementManager();
		
		AchievementCatch aCatch1 = new AchievementCatch("Your Journey Begins", 1);
			RewardDrop rwd1 = new RewardDrop("Pok�ball");
			aCatch1.addReward(rwd1);
		AchievementCatch aCatch2 = new AchievementCatch("Beginner Trainer", 10);
			RewardDrop rwd2 = new RewardDrop("Masterball");
			RewardGrantXP rwgxp1 = new RewardGrantXP(1000);
			aCatch2.addReward(rwd2);
			aCatch2.addReward(rwgxp1);
		
		AchievementPick aPick1 = new AchievementPick("My first Ball", 1, "Pok�ball");
			RewardGrantXP rwgxp2 = new RewardGrantXP(100);
			aPick1.addReward(rwgxp2);
		AchievementPick aPick2 = new AchievementPick("Gotta catch 'em all", 1, "Masterball");
			RewardGrantXP rwgxp3 = new RewardGrantXP(1000);
			aPick2.addReward(rwgxp3);
			
		achievementManager.addAchievement(aCatch1);
		achievementManager.addAchievement(aCatch2);
		achievementManager.addAchievement(aPick1);
		achievementManager.addAchievement(aPick2);
		
		
		
		for(int i=0; i<10; i++) {
			
			eventManager.notifyObservers("OnCatch", null);
			System.out.println(achievementManager);
		}
		
		
	}

}
