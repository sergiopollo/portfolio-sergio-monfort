package examen;

import java.util.ArrayList;

public abstract class Achievement implements Observer, Comparable<Achievement>{
	
	private String nom;
	private boolean completado;
	private ArrayList<Reward> rewards = new ArrayList<Reward>();
	
	public Achievement(String nom) {
		super();
		this.nom=nom;
		this.completado=false;
	}
	
	public String getNom() {
		return nom;
	}

	public boolean isCompletado() {
		return completado;
	}
	
	public void addReward(Reward el) {
		
		if(!rewards.contains(el)) {
			rewards.add(el);
		}	
		
	}
	
	public void removeReward(Reward el) {
		
		if(rewards.contains(el)) {
			rewards.remove(el);
		}	
		
	}
	
	
	public abstract void process();
	
	@Override
	public abstract void notifyObserver(String event, Object objecte);
	
	protected void giveRewards() {
		
		this.completado=true;
		
		for (Reward reward : this.rewards) {
			if(reward instanceof Reward) {
				reward.execute();
			}
		}
		
	}
	

	@Override
	public int compareTo(Achievement a) {
		
		/*
		devuelve 0 si son iguales
		devuelve 1 si el mio es mayor
		devuelve -1 si el mio es menor
		*/
		
		if(this.completado && a.isCompletado()) {
			
			int nomMesGran = this.nom.compareTo(a.nom);
			
			if(nomMesGran==0) {
				
				if(this.rewards.size() == a.rewards.size()) {
					return 0;
				}else if(this.rewards.size() > a.rewards.size()) {
					return 1;
				}else {
					//el es mas grande
					return -1;
				}
				
			}else {
				return nomMesGran;
			}
		}else if(this.completado && a.isCompletado()==false) {
			return 1;
		}else {
			//el es mas grande
			return -1;
		}
	}

	@Override
	public String toString() {
		return "Achievement [nom=" + nom + ", completado=" + completado +"]";
	}
	
	
	
	
	
}
