package examen;

public class AchievementCatch extends Achievement {
	
	private int onCatch;
	private int onMaxcatch;
	
	
	public AchievementCatch(String nom, int OnMaxCatch) {
		super(nom);
		this.onMaxcatch=OnMaxCatch;
		this.onCatch=0;
		EventManager.getInstance().subscribe("OnCatch", this);
	}

	public int getOnMaxcatch() {
		return onMaxcatch;
	}

	public void setOnMaxcatch(int onMaxcatch) {
		this.onMaxcatch = onMaxcatch;
	}

	public int getOncatch() {
		return onCatch;
	}


	@Override
	public void process() {
		
		onCatch++;
		if(onCatch==onMaxcatch) {
			giveRewards();
		}
	}

	@Override
	public void notifyObserver(String event, Object objecte) {
		//no hace falta que lo compruebe porque el eventManager solo avisa a este si es el evento que quiero
		//if(event.equals("OnCatch")) {
			process();
		//}
		
	}

	@Override
	public String toString() {
		return "AchievementCatch ["+ super.toString() + "onCatch=" + onCatch + ", onMaxcatch=" + onMaxcatch +"]";
	}
	
	

}
