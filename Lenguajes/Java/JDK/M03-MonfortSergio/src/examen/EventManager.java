package examen;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
	
	private static EventManager m_EventManager = null;
	//diccionario
	private HashMap<String, ArrayList<Observer>> eventObservers = new HashMap<String, ArrayList<Observer>>();
	
	//constructor
	private EventManager() {
		super();
	}
	
	public static EventManager getInstance() {
		
		if(m_EventManager == null) {
			m_EventManager= new EventManager();
		}
		return m_EventManager;
		
	}
	
	//getter
	public HashMap<String, ArrayList<Observer>> getObservers() {
		return eventObservers;
	}
	
	//subscriure's (es posa a la llista)
	public void subscribe(String evento, Observer ob) {
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers == null) {
			
			observers = new ArrayList<Observer>();
			eventObservers.put(evento, observers);
		}
		
		if(!observers.contains(ob)) {
			observers.add(ob);
		}	
	
	}
	
	//unsubscribe (borrar de la llista)
	public void unsubscribe(String evento, Observer ob) {
		
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers != null) {
			
			if(observers.contains(ob)) {
				observers.remove(ob);
			}
		}		
	}

	//notifyobservers (avisa a tots els de la llista)
	public void notifyObservers(String evento, Object objecte) {
		
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers != null) {
			
			for (int i = observers.size()-1; i >= 0; i--) {
				
				observers.get(i).notifyObserver(evento, objecte);
			}
			
		}
	}
	
	
}
