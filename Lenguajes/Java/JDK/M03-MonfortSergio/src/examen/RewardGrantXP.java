package examen;

public class RewardGrantXP extends Reward {
	
	private int XP;
	
	public RewardGrantXP(int XP) {
		super();
		this.XP = XP;
	}
	
	
	public int getXP() {
		return XP;
	}

	
	@Override
	public void execute() {
		
		System.out.println("Gained "+this.XP+" experience points");
		
	}


	@Override
	public String toString() {
		return "RewardGrantXP [XP=" + XP + "]";
	}

	
	
}
