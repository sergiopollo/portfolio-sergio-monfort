package examen;

import java.util.ArrayList;
import java.util.Iterator;

public class AchievementManager {
	
	private ArrayList<Achievement> achievements;
	
	public AchievementManager() {
		super();
		this.achievements= new ArrayList<Achievement>();
	}
	
	//subscriure's (es posa a la llista)
	public void addAchievement(Achievement ob) {
		
		if(!achievements.contains(ob)) {
			achievements.add(ob);
		}
	}
	
	//unsubscribe (borrar de la llista)
	public void removeAchievement(Achievement ob) {
		
		if(achievements.contains(ob)) {
			achievements.remove(ob);
		}
	}

	@Override
	public String toString() {
		return "AchievementManager [achievements=" + achievements + "]";
	}
	
}
