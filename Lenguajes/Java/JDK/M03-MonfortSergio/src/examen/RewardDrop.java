package examen;

public class RewardDrop extends Reward {
	
	private String objecte;
	
	public RewardDrop(String objecte) {
		super();
		this.objecte = objecte;
	}
	
	@Override
	public void execute() {
		
		System.out.println("You recieved:"+this.objecte);
		EventManager.getInstance().notifyObservers("OnPick", this.objecte);
		
	}

	@Override
	public String toString() {
		return "RewardDrop [objecte=" + objecte + "]";
	}
	
	

}
