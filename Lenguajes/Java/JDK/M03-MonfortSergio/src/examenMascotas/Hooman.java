package examenMascotas;

import java.util.ArrayList;

public class Hooman {
	
	private String name;
	private ArrayList<Action> actions = new ArrayList<Action>();

	public Hooman(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public void addAction(Action ac) {
		actions.add(ac);
	}
	
	public void dailyRoutine() {
		
		for (Action action : actions) {
			action.act();
		}
		
	}
	
	@Override
	public String toString() {
		return name+" (Hooman)" ;
	}
	
}
