package examenMascotas;

public abstract class Action {
	
	private Hooman miHumano;
	
	public Action(Hooman miHumano) {
		super();
		this.miHumano = miHumano;
	}

	
	public Hooman getMiHumano() {
		return miHumano;
	}

	public abstract void act();
	
}
