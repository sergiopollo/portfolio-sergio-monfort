package examenMascotas;

public interface Observer {

	public void notifyObserver(String event, Object sender);
	
}
