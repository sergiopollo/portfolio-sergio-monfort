package examenMascotas;

import java.util.ArrayList;

public class Pet {
	
	private String name;
	private ArrayList<Behaviour> behaviours = new ArrayList<Behaviour>();

	public Pet(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addBehaviour(Behaviour be) {
		behaviours.add(be);
	}
	
	@Override
	public String toString() {
		return name+" (Pet)";
	}

}
