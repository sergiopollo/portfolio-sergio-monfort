package examenMascotas;

public class main {

	public static void main(String[] args) {
		
		
		Pet kotaro = new Pet("Kotaro");
			Playful kotaroP = new Playful(kotaro);
			kotaro.addBehaviour(kotaroP);
				kotaroP.addEvent("OnCall");
			Sleepy kotaroS = new Sleepy(kotaro);
			kotaro.addBehaviour(kotaroS);
				kotaroS.addEvent("OnSleep");
			Eats kotaroE = new Eats(kotaro);
			kotaro.addBehaviour(kotaroE);
				kotaroE.addEvent("OnFood");
			
		Pet kami = new Pet("Kami");
			Playful kamiP = new Playful(kami);
			kami.addBehaviour(kamiP);
				kamiP.addEvent("OnCall");
				kamiP.addEvent("OnFood");
				kamiP.addEvent("OnPlay");
			Eats kamiE = new Eats(kami);
			kami.addBehaviour(kamiE);
				kamiE.addEvent("OnFood");
			
		Pet turti = new Pet("Turti");
			Eats turtiE = new Eats(turti);
			turti.addBehaviour(turtiE);
				turtiE.addEvent("OnFood");	
				turtiE.addEvent("OnEat");	
			
		Hooman chechu = new Hooman("Chechu");
			Call call =  new Call(chechu);
			Cook cook = new Cook(chechu);
			Sleep sleep = new Sleep(chechu);
			chechu.addAction(call);
			chechu.addAction(cook);
			chechu.addAction(sleep);
			
		Hooman froggo = new Hooman("Froggo");
			Call call2 =  new Call(froggo);
			Sleep sleep2 = new Sleep(froggo);	
			froggo.addAction(call2);
			froggo.addAction(sleep2);
			
			chechu.dailyRoutine();
			System.out.println("");
			froggo.dailyRoutine();
	}

}
