package examenMascotas;

import java.util.ArrayList;

public abstract class Behaviour implements Observer{
	
	private Pet miPet;
	private ArrayList<String> events = new ArrayList<String>();
	
	public Behaviour(Pet miPet) {
		super();
		this.miPet = miPet;
	}

	public Pet getMiPet() {
		return miPet;
	}

	public void addEvent(String event) {
		events.add(event);
		EventManager.getInstance().subscribe(event, this);
	}

	public abstract void behave(Object sender);

	@Override
	public void notifyObserver(String event, Object sender) {
		
		if(sender != miPet) {
			
			behave(sender);
			
		}
		
	}
	
}
