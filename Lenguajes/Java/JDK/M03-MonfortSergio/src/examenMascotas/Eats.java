package examenMascotas;

public class Eats extends Behaviour {

	public Eats(Pet miPet) {
		super(miPet);
	}
	
	@Override
	public void behave(Object sender) {
		
		System.out.println(this.getMiPet()+" eats "+sender+"'s food");
		EventManager.getInstance().notifyObservers("OnEat", this.getMiPet());
	}

}
