package examenMascotas;

public class Sleepy extends Behaviour {

	public Sleepy(Pet miPet) {
		super(miPet);
	}
	
	@Override
	public void behave(Object sender) {
		
		System.out.println(this.getMiPet()+" goes to sleep");
		EventManager.getInstance().notifyObservers("OnSleep", this.getMiPet());
	}

}
