package examenMascotas;

public class Call extends Action {

	public Call(Hooman miHumano) {
		super(miHumano);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act() {
		System.out.println(this.getMiHumano()+" calls out for pets");
		EventManager.getInstance().notifyObservers("OnCall", getMiHumano());
		
	}

}
