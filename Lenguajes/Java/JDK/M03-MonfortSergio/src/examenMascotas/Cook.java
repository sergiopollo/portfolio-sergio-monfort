package examenMascotas;

public class Cook extends Action {

	public Cook(Hooman miHumano) {
		super(miHumano);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act() {
		System.out.println(this.getMiHumano()+" starts cooking");
		EventManager.getInstance().notifyObservers("OnFood", getMiHumano());
		
	}

}
