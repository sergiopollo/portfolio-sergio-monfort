package examenMascotas;

public class Sleep extends Action {

	public Sleep(Hooman miHumano) {
		super(miHumano);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void act() {
		System.out.println(this.getMiHumano()+" goes to sleep");
		EventManager.getInstance().notifyObservers("OnSleep", getMiHumano());
		
	}

}
