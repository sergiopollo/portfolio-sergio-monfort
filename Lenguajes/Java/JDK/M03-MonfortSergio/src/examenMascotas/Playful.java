package examenMascotas;

public class Playful extends Behaviour {

	public Playful(Pet miPet) {
		super(miPet);
	}
	
	@Override
	public void behave(Object sender) {
		
		System.out.println(this.getMiPet()+" plays with "+sender);
		EventManager.getInstance().notifyObservers("OnPlay", this.getMiPet());
	}

}
