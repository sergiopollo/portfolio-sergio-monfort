package Timer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;


public class mainExecutor {

	public static void main(String[] args)
	{
		ExecutorService executor = Executors.newCachedThreadPool();
		System.out.println("Inicio el main.");
		for(int i=0;i<10;i++) {
			executor.execute(new ThreadTimer());
		}
		
		System.out.println("Aturem ms execucions.");
		//shutdown hace que no puedas iniciar mas threads y espera a que los activos acaben
		executor.shutdown(); //aturem ms execucions
		
		
		//espero 5 segons
		for(int i=0;i<5;i++) {
			System.out.println("soc el main i espero "+i+" segons");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			System.out.println("Aturem les execucions.");
			//shutdownnow para todos los threads
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Acabo el main.");
	}

	
}
