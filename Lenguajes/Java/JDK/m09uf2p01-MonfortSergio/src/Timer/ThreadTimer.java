package Timer;

import java.util.Iterator;
import java.util.Random;

public class ThreadTimer implements Runnable{

	private int numSegons;
	private int i;
	
	public ThreadTimer() {
		super();
		Random r = new Random();
		this.numSegons = r.nextInt(10)+1;
	}



	@Override
	public void run() {
		
		try {
			System.out.println("Sc el thread " + Thread.currentThread().getId());
			
			for( i=numSegons; i> 0; i--) {
				System.out.println("Sc el thread " + Thread.currentThread().getId()+" i em queden "+i+" segons");
				//un segundo
				Thread.sleep(1000);
			}
			System.out.println("Sc el thread "+Thread.currentThread().getId()+" i m'acabo.");

		} catch (InterruptedException e) {
			System.out.println("Sc el thread " + Thread.currentThread().getId() + " i m'han aturat amb "+ i +" segons restants");
		}
	}

}
