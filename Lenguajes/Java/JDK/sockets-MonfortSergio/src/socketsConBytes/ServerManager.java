package socketsConBytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONException;


public class ServerManager implements Runnable{
	
	
	private static HashSet<String> nicknames = new HashSet<String>();
	private static ArrayList<Pregunta> preguntes = new ArrayList<Pregunta>();
	
	private boolean partidaEnCurso=false;
	
	private int numJugadoresEnPartida;
	private Pregunta preguntaActual;
	private Resultados resultadosActuales;
	
	//semaforos
	public Object lobby = new Object();
	public Object connectar = new Object();
	public Object recibirPregunta = new Object();
	public Object recibirRespuesta = new Object();
	public Object recibirResultados = new Object();
	
	@Override
	public void run() {
		
    	inicializarPreguntas();
    	try {
			System.out.println("iniciando un lobby");
			this.numJugadoresEnPartida=0;
			while(true) {
				System.out.println("numJugs: "+this.numJugadoresEnPartida);
				//avisa a los que esten esperando que hay un lobby disponible
				synchronized(lobby) {
					lobby.notifyAll();
				}
				//espera un rato a que se unan los jugadores que esperaban
				synchronized(connectar) {
					connectar.wait();
				}
				//hace una cuenta atras
				Thread.sleep(5000);
				
				//inicia la partida
				partidaEnCurso=true;
				System.out.println("iniciando partida");
				try {
					
					partida();
					
				}catch (partidaCanceladaException a) {
					System.out.println("algun jugador se ha desconectado");
					a.printStackTrace();
					if(numJugadoresEnPartida == 0) {
						System.out.println("pues cierro la partida (no funciona)");
					}
				}
				
				partidaEnCurso=false;
				System.out.println("partida acabada");
			}
    	} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void partida() throws partidaCanceladaException, InterruptedException{
		
		//envia pregunta
		Random r = new Random();
		int preguntaRandom = r.nextInt(preguntes.size());
		this.preguntaActual = preguntes.get(preguntaRandom);
		
		synchronized(recibirPregunta) {
			recibirPregunta.notifyAll();
		}
		
		
		//espera a que todos respondan
		resultadosActuales = new Resultados();
		
		synchronized (recibirRespuesta) {
			for(int i=0; i<numJugadoresEnPartida; i++) {
				recibirRespuesta.wait(5000);
			}
		}
		
		//envia los resultados
		synchronized(recibirResultados){
			recibirResultados.notifyAll();
		}
		
	}
	
	public synchronized boolean responderPregunta(String nick, int respuesta) {
		
		int respuestaCorrecta = preguntaActual.getPosicionRespostaCorrecta();
		
		if(respuesta == respuestaCorrecta) {
			resultadosActuales.addResultado(nick, 1);
			synchronized (recibirRespuesta) {
				recibirRespuesta.notify();
			}
			
			return true;
		}else {
			resultadosActuales.addResultado(nick, 0);
			synchronized (recibirRespuesta) {
				recibirRespuesta.notify();
			}
			return false;
		}

	}
	
	
	public Pregunta getPregunta() {
		
		return preguntaActual;
		
	}
	
	public Resultados getResultados() {
		
		return resultadosActuales;
		
	}
	
	public synchronized boolean puedoUnirmeAlLobby() {
		if(partidaEnCurso) {
			return false;
		}else {
			
			this.numJugadoresEnPartida++;
			
			synchronized(connectar){
				connectar.notify();
			}
			return true;
		}
	}
	
	
	public synchronized boolean revisarNickname(String nick) {
		
		if(nicknames.contains(nick)) {
			//si el nick ya esta en uso aviso al usuario y espero otra respuesta
			return false;
			
		}else {
			//anyado el nick
			nicknames.add(nick);
			return true;
		}
		
	}
	
	public synchronized void desconectarse(String nick, boolean isDentroDePartida) {
		System.out.println("jugadores en partida before DC: "+numJugadoresEnPartida);
		if(nicknames.contains(nick)) {
			nicknames.remove(nick);
		}
		if(isDentroDePartida) {
			acabaPartida();
		}
		System.out.println("jugadores en partida: "+numJugadoresEnPartida);
	}
	
	public synchronized void acabaPartida() {
		this.numJugadoresEnPartida--;
	}
	
	public Pregunta fetchPregunta() {
		Random r = new Random();
		int random = r.nextInt(preguntes.size());
		
		return preguntes.get(random);
	}


	private void inicializarPreguntas() {
		
		Pregunta p1 = new Pregunta();
    	p1.addEnunciado("es iker mongolo?");
    	p1.addResposta("1: si", true);
    	p1.addResposta("2: por supuesto", false);
    	p1.addResposta("3: claramente", false);
    	p1.addResposta("4: absolutamente", false);
		
    	preguntes.add(p1);
    	
    	Pregunta p2 = new Pregunta();
    	p2.addEnunciado("estan los aldeanos rotisimos?");
    	p2.addResposta("1: si", true);
    	p2.addResposta("2: no", false);
    	p2.addResposta("3: la eslavitud aldeanistica mola", false);
		
    	preguntes.add(p2);
		
    	Pregunta p3 = new Pregunta();
    	p3.addEnunciado("cual es la peor asignatura de damvi?");
    	p3.addResposta("1: interficies", false);
    	p3.addResposta("2: acces a dades", false);
    	p3.addResposta("3: videojocs", false);
    	p3.addResposta("4: android", true);
		
    	preguntes.add(p3);
		
	}
}
