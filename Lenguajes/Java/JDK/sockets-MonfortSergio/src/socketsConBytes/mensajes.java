package socketsConBytes;

public class mensajes {
	
	public final static byte ACK = 0b00000001;
	public final static byte BENVINGUT = 0b00000010;
	public final static byte NICK_EN_US = 0b00000011;
	public final static byte ESTAS_DINS = 0b00000100;
	public final static byte EN_CURS = 0b00000101;
	public final static byte CORRECTE = 0b00000110;
	public final static byte INCORRECTE = 0b00000111;
	public final static byte WANNA_CONTINUE = 0b00001000;
	public final static byte YES_CONTINUE = 0b00001001;
	public final static byte NO_CONTINUE = 0b00001010;
	
	
	public static String getMessageFromByte(byte mensaje) {
		
		switch(mensaje) {
		case mensajes.ACK:
			return "ACK";
		case mensajes.BENVINGUT:
			return "BENVINGUT";
		case mensajes.NICK_EN_US:
			return "el nick esta en uso";
		case mensajes.ESTAS_DINS:
			return "ESTAS DINS";
		case mensajes.EN_CURS:
			return "PARTIDA EN CURS";
		case mensajes.CORRECTE:
			return "CORRECTE";
		case mensajes.INCORRECTE:
			return "INCORRECTE";
		case mensajes.WANNA_CONTINUE:
			return "QUIERES VOLVER A JUGAR?";
		case mensajes.YES_CONTINUE:
			return "YES CONTINUE";
		case mensajes.NO_CONTINUE:
			return "NO CONTINUE";
		default:
			return "error";
		}
			
	}
	
	
}
