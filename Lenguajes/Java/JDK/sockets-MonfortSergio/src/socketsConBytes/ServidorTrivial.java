package socketsConBytes;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServidorTrivial {

	
	//String hostName = "10.1.82.10";
    private static int portNumber = 60009;
	
	public static void main(String[] args)
    {
        /*
        if (args.length != 1)
        {
            System.err.println("Expected PORT as parameter");
            System.exit(1);
        }
        */
    	
		
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
        ) {
        	ExecutorService executors = Executors.newCachedThreadPool();
        	
        	//lanzo el manager
        	ServerManager manager = new ServerManager();
        	
        	executors.execute(manager);
        	
        	//voy esperando recibir clientes nuevos
        	while(true)
        	{
        		System.out.println("Esperant que es connecti un client");
        		Socket clientSocket = serverSocket.accept();
        		executors.execute(new ClientHandler(clientSocket, manager));
        	}
        } catch (IOException e)
        {
            System.out.println("Exception caught when trying to listen to port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
	
	
	
}
