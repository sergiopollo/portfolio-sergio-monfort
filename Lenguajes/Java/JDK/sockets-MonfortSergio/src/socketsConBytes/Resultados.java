package socketsConBytes;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Resultados {

    private ArrayList<String> resultados;

    public Resultados() {
    	resultados = new ArrayList<String>();
    }

    public Resultados(String json) {

    	resultados = new ArrayList<String>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray res = (JSONArray) obj.get("resultats");
            for (int i = 0; i < res.length(); i++) {
            	
            	JSONObject temp = res.getJSONObject(i);
            	
            	resultados.add(temp.getString("nick")+"_"+temp.getInt("puntuacio"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void mostrar() {
        System.out.println("resultados: ");
        for (String string : resultados) {
        	
        	String[] results = string.split("_");
        	
            System.out.println("jugador: "+results[0]+" puntuacion: "+results[1]);
            
            
        }
    }

    public void addResultado(String nick, int puntuacio) {
    	resultados.add(nick+"_"+puntuacio);
    }
    
    public String json() {

        String json = "{\"resultats\":[";

        for (int i = 0; i < resultados.size()-1; i++) {
        	
        	String[] results = resultados.get(i).split("_");
        	
            json = json+"{\"nick\": \""+results[0]+"\", \"puntuacio\": "+results[1]+"}, ";
        }
        
        String[] results = resultados.get(resultados.size()-1).split("_");
        
        json = json+"{\"nick\": \""+results[0]+"\", \"puntuacio\": "+results[1]+"}]}";
            return json;

    }
    
}
