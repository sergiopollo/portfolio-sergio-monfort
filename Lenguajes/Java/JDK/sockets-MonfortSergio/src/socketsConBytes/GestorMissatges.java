package socketsConBytes;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GestorMissatges implements AutoCloseable{

	private Socket mySocket;
	//private PrintWriter out;
	//private BufferedReader in;
	private DataOutputStream out;
	private DataInputStream in;
 	private BufferedReader stdIn;
 	private PrintWriter stdOut;

	
	public GestorMissatges(Socket socket) {
		
		this.mySocket = socket;
        
		try {
			/*
			this.out = new PrintWriter(socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			*/
			this.in = new DataInputStream(mySocket.getInputStream());
			this.out=new DataOutputStream(mySocket.getOutputStream());
			this.stdIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.stdOut =  new PrintWriter(socket.getOutputStream(), true);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	/*
	public Socket getMySocket() {
		return mySocket;
	}

	public PrintWriter getOut() {
		return out;
	}

	public BufferedReader getIn() {
		return in;
	}
	 */

	//on enviem una string.
	public void send(String message) {
		
		System.out.println("sending: "+message);
		
		stdOut.println(message);
	}
	
	//on enviem una string i rebem una resposta que retornem.
	public String sendAndReceive(String message) throws IOException {
		
		send(message);
		
		return receiveString();
	}
	
	//on enviem una string i esperem rebre response com a resposta.
	public void sendAndReceive(String message, String response) throws IOException, ProtocoloFallidoException {
		
		send(message);
		
		receiveString(response);
		
	}
	
	//on rebem un missatge que retornem.
	public String receiveString() throws IOException {
		
		String line = stdIn.readLine();
		
		System.out.println("received: "+line);
		return line;
	}
	
	//on esperem rebre un missatge concret.
	public void receiveString(String receive) throws IOException, ProtocoloFallidoException {
		
		String cosaQueHeRecibido= receiveString();
		
		if(!cosaQueHeRecibido.equals(receive)) {
			throw new ProtocoloFallidoException("esperaba recibir "+receive+" String recibido: "+cosaQueHeRecibido);
		}
	}
	
	//on esperem rebre un missatge concret i responem amb response.
	public void receiveAndSend(String message, String response) throws IOException, ProtocoloFallidoException {
		
		receiveString(message);
		send(response);
		
	}
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//BYTES
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	//on enviem una string.
		public void send(byte message) throws IOException {
			
			System.out.println("sending: "+mensajes.getMessageFromByte(message));
			
			out.writeByte(message);
		}
		
		//on enviem una string i rebem una resposta que retornem.
		public byte sendAndReceive(byte message) throws IOException {
			
			send(message);
			
			return receiveByte();
		}
		
		//on enviem una string i esperem rebre response com a resposta.
		public void sendAndReceive(byte message, byte response) throws IOException, ProtocoloFallidoException {
			
			send(message);
			
			receiveByte(response);
			
		}
		
		//on rebem un missatge que retornem.
		public byte receiveByte() throws IOException {
			
			byte line = in.readByte();
			
			System.out.println("received: "+mensajes.getMessageFromByte(line));
			
			return line;
		}
		
		//on esperem rebre un missatge concret.
		public void receiveByte(byte receive) throws IOException, ProtocoloFallidoException {
			
			byte cosaQueHeRecibido= receiveByte();
			
			if(cosaQueHeRecibido != receive) {
				throw new ProtocoloFallidoException("esperaba recibir "+receive+"->"+mensajes.getMessageFromByte(receive)+" byte recibido: "+cosaQueHeRecibido+"->"+mensajes.getMessageFromByte(cosaQueHeRecibido));
			}
		}
		
		//on esperem rebre un missatge concret i responem amb response.
		public void receiveAndSend(byte message, byte response) throws IOException, ProtocoloFallidoException {
			
			receiveByte(message);
			send(response);
			
		}
	
	
		public void send(int message) throws IOException {
			
			System.out.println("sending: "+message);
			out.writeInt(message);
		}
		
		public int receiveInt() throws IOException {
			
			int line = in.readInt();
			System.out.println("received: "+line);
			return line;
		}
		
		public void receiveInt(int receive) throws IOException, ProtocoloFallidoException {
			
			int cosaQueHeRecibido= receiveInt();
			
			if(cosaQueHeRecibido != receive) {
				throw new ProtocoloFallidoException("esperaba recibir "+receive+" int recibido: "+cosaQueHeRecibido);
			}
		}
	
	
	
	//tanca els buffers d�entrada i sortida.
	public void close() throws IOException {
		this.in.close();
		this.out.close();
	}
	
}
