package socketsConBytes;

import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.*;


public class ClientHandler implements Runnable
{
	private Socket socket;
	private PrintWriter out;                   
	private BufferedReader in;
	private GestorMissatges gm;
	private ServerManager manager;
	private String nickDeMiJugador;
	private boolean isDentroDePartida;
	
	/*controlar: nick no escogido, nick escogido, 
	servidor esperando: ha avisado ya,  aun no ha avisado
	*/
	
    
    public ClientHandler(Socket socket, ServerManager manager) throws IOException
    {
    	this.socket = socket;
    	out = new PrintWriter(socket.getOutputStream(), true);                   
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    	gm = new GestorMissatges(socket);
    	this.manager = manager;
    	this.isDentroDePartida=false;
    }
    
	public void run()
	{
		try 
		{
			System.out.println(Thread.currentThread().getName() + ": Connection established with "+ socket.getInetAddress() +" at port " + socket.getPort()
			+ " and local port " + socket.getLocalPort());

			
			gm.sendAndReceive(mensajes.BENVINGUT, mensajes.ACK);
        	
			//revision de nickname
			
			boolean nickCorrecto = false;
    		
    		while(!nickCorrecto) {
    			//espero recibir el nick del jugador
        		String nick = gm.receiveString();
        		this.nickDeMiJugador=nick;
        		
        		//reviso si el nick esta ya en uso
        		boolean nickCorrecto2;
				
        		nickCorrecto2 = manager.revisarNickname(nick);
				
        		
        		if(!nickCorrecto2) {
        			gm.send(mensajes.NICK_EN_US);
        		}else {
        			gm.send(mensajes.ACK);
        			nickCorrecto=true;
        		}
        		
        
    		}
			
			
			boolean jugadorSigue =true;
			
        	while(jugadorSigue) {
        		this.isDentroDePartida=false;
        		
        		boolean jugadorSeUneAlLobby = false;
        		
        		while(!jugadorSeUneAlLobby) {
        			//el jugador pregunta si se puede unir a un lobby y notifica que se ha conectado
            		if(manager.puedoUnirmeAlLobby()) {
            			jugadorSeUneAlLobby=true;
            			this.isDentroDePartida=true;
            			gm.send(mensajes.ESTAS_DINS);
            			gm.receiveByte(mensajes.ACK);
            			
            		}else {
            			//el jugador espera a que pueda jugar una partida
                		synchronized(manager.lobby) {
                			manager.lobby.wait();
                			gm.send(mensajes.EN_CURS);
                			gm.receiveByte(mensajes.ACK);
                		}
            		}
        		}
        		
        		
        		//PARTIDA
        		
        		//esperan a recibir una pregunta
        		synchronized (manager.recibirPregunta) {
					manager.recibirPregunta.wait();
				}
        		
        		//cuando la reciben la envian al usuario
        		Pregunta preg = manager.getPregunta();
        		gm.send(preg.json());
        		gm.receiveByte(mensajes.ACK);
        		
        		
        		//espera recibir la respuesta
        		int respuestaUsuario = gm.receiveInt();
        		gm.send(mensajes.ACK);
        		
        		//avisa al manager que ha respondido el usuario y le pasa la respuesta al manager
        		
        		boolean esCorrecto = false;
        		
        		//espera recibir los resultados
        		//ha petat per aqu� abans d'avisar
        		synchronized (manager.recibirResultados) {
        			synchronized (manager.recibirRespuesta) {
            			esCorrecto = manager.responderPregunta(this.nickDeMiJugador, respuestaUsuario);
    				}
        			//ha petat aqu� despr�s d'avisar
        			//aqui el servidor ja ha estat notificat per tu, si ets l'�ltim...
					manager.recibirResultados.wait();
				}
        		
        		
        		Resultados res = manager.getResultados();
        		
        		//le envia al usuario si la respuesta es correcta o incorrecta
        		if(esCorrecto) {
        			gm.send(mensajes.CORRECTE);
        		}else {
        			gm.send(mensajes.INCORRECTE);
        		}
        		gm.receiveByte(mensajes.ACK);
        		
        		//envia los resultados
        		gm.send(res.json());
        		gm.receiveByte(mensajes.ACK);
        		
        		
        		//!!!!!!!!!!!!!!!!!!
        		/*controlar: nick no escogido, nick escogido, 
        		servidor esperando: ha avisado ya,  aun no ha avisado
        		*/
        		
        		//pregunto al usuario si quiere seguir
        		gm.send(mensajes.WANNA_CONTINUE);
        		gm.receiveByte(mensajes.ACK);
        		
        		Byte yesOrNo = gm.receiveByte();
        		gm.send(mensajes.ACK);
        		
        		if(yesOrNo == mensajes.NO_CONTINUE) {
        			jugadorSigue=false;
        			//se para el bucle y se cierra la conexion
        			
        			//al desconectarse tiene que borrar su nick y restar 1 en numJugadores
        			//lo hace en el finally
        		}else if(yesOrNo == mensajes.YES_CONTINUE) {
        			manager.acabaPartida();
        		}
        		
        	}
        	
			
		} catch (IOException e) {
            System.out.println("Exception caught when trying to listen to port "
                + socket.getLocalPort() + " or listening for a connection");
            System.out.println(e.getMessage());
            
            
        } catch (ProtocoloFallidoException e) {
			// TODO Auto-generated catch block
        	System.out.println("fallo del protocolo");
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	try
        	{
        		manager.desconectarse(nickDeMiJugador, isDentroDePartida);
				out.close();
				in.close();
				socket.close();
        	} catch (IOException e)
	        {
        		System.out.println(e.getMessage());
	        }
        }
	}
	
}

