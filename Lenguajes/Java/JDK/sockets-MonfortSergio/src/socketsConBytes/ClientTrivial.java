package socketsConBytes;


import java.io.*;
import java.net.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class ClientTrivial {

	
	public static void main(String[] args) {
		
		
		String hostName = "10.1.82.64";
		//String hostName = "10.1.82.10";
        int portNumber = 60009;

        try(
        		
        	Socket echoSocket = new Socket(hostName, portNumber);
        	GestorMissatges gm = new GestorMissatges(echoSocket);
        		
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ){
            String userInput;
            
            
            //primero conectarse
            
            gm.receiveAndSend(mensajes.BENVINGUT, mensajes.ACK);
            
            boolean nickEnUso = true;
            
            while(nickEnUso) {
            	
            	System.out.println("introduce tu nickname");
                
                //escribes el nick
                userInput = stdIn.readLine();
                //envias el nick
                gm.send(userInput);
                
                byte respuestaNick = gm.receiveByte();
                
                if(respuestaNick == mensajes.ACK) {
                	nickEnUso = false;
                }else if(respuestaNick == mensajes.NICK_EN_US) {
                	System.out.println("ese nickname ya esta en uso");
                }else {
                	System.out.println("ERROR");
                	throw new ProtocoloFallidoException();
                }
            	
            }
            
            //el server me dice si hay partida en curso
            boolean partidaEnCurso = true;
            
            while(partidaEnCurso){
            	byte respuestaPartidaDisponible = gm.receiveByte();
                gm.send(mensajes.ACK);
                
                if(respuestaPartidaDisponible == mensajes.EN_CURS) {
                	System.out.println("la partida esta en curso, esperate pls");
                }else if(respuestaPartidaDisponible == mensajes.ESTAS_DINS) {
                	System.out.println("entrando en la partida");
                	
                	//entras en la partida
                    
                        //recibo pregunta
                        String pregunta = gm.receiveString();
                        
        				JSONObject JSONobj = new JSONObject(pregunta);

        				
        				String enunciado = (String) JSONobj.get("pregunta");
        				JSONArray respuestasJSON = (JSONArray) JSONobj.get("respostes");
        				
        				
        				System.out.println("PREGUNTA: "+enunciado+"\n");
        				
        				
        				System.out.println("SELECCIONA UNA RESPUESTA (1/2/3/4 segun las respuestas que haya)");
        				
        				for(int i =0; i<respuestasJSON.length();i++) {

        					String respuesta = (String) respuestasJSON.get(i);

        					int num = i+1;
        					
        					System.out.println("\n"+num+": "+respuesta);
        				}
        				
                        //System.out.println("pregunta rebuda: "+pregunta);
                        
                        gm.send(mensajes.ACK);
                        
                        //seleccionas la respuesta
                        userInput = stdIn.readLine();
                        
                        //casteo a int
                        int InputInt = 1;
                        try {
                        	InputInt = Integer.parseInt(userInput);
                        }catch(NumberFormatException e) {
                        	System.out.println("tienes que escribir un int, mongolo");
                        	InputInt = 1;
                        }
                        //envio la respuesta y espero un ACK
                        gm.send(InputInt); 
                        gm.receiveByte(mensajes.ACK);
                        
                        //recibo si la respuesta es correcta o no
                        byte resultado = gm.receiveByte();
                        
                        if(resultado == mensajes.CORRECTE) {
                        	System.out.println("muy bien! respuesta correcta! toma una galletita");
                        	gm.send(mensajes.ACK);
                        }else if(resultado==mensajes.INCORRECTE) {
                        	System.out.println("manco, respuesta incorrecta");
                        	gm.send(mensajes.ACK);
                        }else {
                        	System.out.println("._.");
                        	gm.send(mensajes.ACK);
                        	throw new ProtocoloFallidoException();
                        }
                        
                        //se reciben los resultados
                        String JSONResultados = gm.receiveString();
                        //System.out.println(JSONResultados);
                        gm.send(mensajes.ACK);
                        
                        //System.out.println(JSONResultados);
                        Resultados r = new Resultados(JSONResultados);
                        r.mostrar();
                        
                        //el server pregunta al jugador si quieres seguir
                        gm.receiveByte(mensajes.WANNA_CONTINUE);
                        gm.send(mensajes.ACK);
                        
                        //el jugador decide si quiere continuar o no
                        System.out.println("quieres seguir jugando? (Y/N)");
                        userInput = stdIn.readLine();
                        
                        if(userInput.equals("Y") || userInput.equals("y")) {
                        	gm.send(mensajes.YES_CONTINUE);
                        }else {
                        	gm.send(mensajes.NO_CONTINUE);

                            //si dice que no ps se acaba
                        	System.out.println("cerrando juego");
                        	partidaEnCurso=false;
                        }
                        gm.receiveByte(mensajes.ACK);
                     
                }else {
                	System.out.println("ERROR");
                	throw new ProtocoloFallidoException();
                }
                
              
                
                
            }
            
            
            
        }catch (UnknownHostException e){
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        }catch (IOException e){
        	System.out.println("you disconnected");
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }catch (ProtocoloFallidoException e){
        	
            System.err.println("no has seguido el protocolo, pedazo tonto: "+e.getMessage());
            System.exit(1);
        }catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
             
		
		
		

	}

}
