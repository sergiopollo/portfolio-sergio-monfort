package socketsConBytes;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Pregunta {

    private String pregunta;
    private ArrayList<String> respostes;
    private String respostaCorrecta;

    public Pregunta() {
        respostes = new ArrayList<String>();
    }

    public Pregunta(String json) {

        respostes = new ArrayList<String>();
        try {
            JSONObject obj = new JSONObject(json);
            pregunta = obj.getString("pregunta");
            JSONArray res = (JSONArray) obj.get("respostes");
            for (int i = 0; i < res.length(); i++) {
                respostes.add(res.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void mostrar() {
        System.out.println("Enunciado: " + pregunta);
        System.out.println("Respuestas: ");
        for (String string : respostes) {
            System.out.println(string);
        }
    }

    public void addEnunciado(String enunciado) {
        pregunta = enunciado;
    }

    public void addResposta(String resposta, boolean isCorrecta) {
        respostes.add(resposta);
        if(isCorrecta ==true && this.respostaCorrecta == null) {
        	this.respostaCorrecta = resposta;
        }
    }
    
    public String getRespostaCorrecta() {
    	return this.respostaCorrecta;
    }
    
    public int getPosicionRespostaCorrecta() {
    	
    	return respostes.indexOf(respostaCorrecta)+1;
    	
    }

    public String json() {

        String json = "{\"pregunta\": \""+pregunta+"\",\"respostes\": [";

        for (int i = 0; i < respostes.size()-1; i++) {
            json = json+"\""+respostes.get(i)+"\", ";
        }
        json = json+"\""+respostes.get(respostes.size()-1)+"\"]}";
            return json;

    }
}
