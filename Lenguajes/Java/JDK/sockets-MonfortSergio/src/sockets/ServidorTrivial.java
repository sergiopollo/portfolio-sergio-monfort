package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONException;

public class ServidorTrivial {
	
	private static ArrayList<Pregunta> preguntes = new ArrayList<Pregunta>();
	private static boolean jugadorSigue = true;
	//String hostName = "10.1.82.10";
    private static int portNumber = 60009;

    Socket a;
	
   
	public static void main(String[] args) {
			
			inicializarPreguntas();
		
	        try
	        (
	            ServerSocket serverSocket = new ServerSocket(portNumber);
	            Socket clientSocket = serverSocket.accept();
	        	GestorMissatges gm = new GestorMissatges(clientSocket);
	        ) {
	        	System.out.println("Connection established at port " + clientSocket.getPort()
	        						+ " and local port " + clientSocket.getLocalPort());
	            
	        	
	        	gm.sendAndReceive("BENVINGUT", mensajes.ACK);
	        	
	        	while(jugadorSigue) {
	        		
		        	gm.sendAndReceive("PREPARAT", mensajes.ACK);
		        	
		        	//String json = "{'pregunta': 'es iker tonto?', 'respostes':['1: si', '2: por supuesto']}";
		        	Pregunta p1 = fetchPregunta();
		        	
		        	gm.sendAndReceive(p1.json(), mensajes.ACK);
		        	
		        	String respuesta = gm.receive();
		        	
		        	gm.send(mensajes.ACK);
		        	
		        	int respuestaInt = Integer.parseInt(respuesta);
		        	
		        	
		        	if(respuestaInt == p1.getPosicionRespostaCorrecta()) {
		        		
		        		gm.sendAndReceive(mensajes.CORRECTE, mensajes.ACK);
		        		
		        	}else {
		        		
		        		gm.sendAndReceive(mensajes.INCORRECTE, mensajes.ACK);
		        		
		        	}
		        	
		        	gm.send(mensajes.WANNA_CONTINUE);
		        	
		        	String yesOrNo = gm.receive();
		        	
		        	if(yesOrNo.equals(mensajes.YES_CONTINUE)) {
		        		//pos sigue, todo correcto
		        	}else {
		        		clientSocket.close();
		        		jugadorSigue = false;
		        	}
	        	}
	        	
	        	
	        } catch (IOException e){
	            System.out.println("Exception caught when trying to listen to port "
	                + portNumber + " or listening for a connection");
	            System.out.println(e.getMessage());
	        }catch (ProtocoloFallidoException e){
	        	
	            System.err.println("no has seguido el protocolo, pedazo tonto: "+e.getMessage());
	            //System.exit(1);
	        }
		
		
		
	}
	
	
	private static Pregunta fetchPregunta() {
		Random r = new Random();
		int random = r.nextInt(preguntes.size());
		
		return preguntes.get(random);
	}


	private static void inicializarPreguntas() {
		
		Pregunta p1 = new Pregunta();
    	p1.addEnunciado("es iker mongolo?");
    	p1.addResposta("1: si", true);
    	p1.addResposta("2: por supuesto", false);
    	p1.addResposta("3: claramente", false);
    	p1.addResposta("4: absolutamente", false);
		
    	preguntes.add(p1);
    	
    	Pregunta p2 = new Pregunta();
    	p2.addEnunciado("estan los aldeanos rotisimos?");
    	p2.addResposta("1: si", true);
    	p2.addResposta("2: no", false);
    	p2.addResposta("3: la eslavitud aldeanistica mola", false);
		
    	preguntes.add(p2);
		
    	Pregunta p3 = new Pregunta();
    	p3.addEnunciado("cual es la peor asignatura de damvi?");
    	p3.addResposta("1: interficies", false);
    	p3.addResposta("2: acces a dades", false);
    	p3.addResposta("3: videojocs", false);
    	p3.addResposta("4: android", true);
		
    	preguntes.add(p3);
		
	}
}
