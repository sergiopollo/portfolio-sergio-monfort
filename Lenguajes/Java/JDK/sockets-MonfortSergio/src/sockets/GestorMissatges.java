package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GestorMissatges implements AutoCloseable{

	private Socket mySocket;
	private PrintWriter out;
	private BufferedReader in;
	
	public GestorMissatges(Socket socket) {
		
		this.mySocket = socket;
        
		try {
			
			this.out = new PrintWriter(socket.getOutputStream(), true);
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	public Socket getMySocket() {
		return mySocket;
	}

	public PrintWriter getOut() {
		return out;
	}

	public BufferedReader getIn() {
		return in;
	}


	//on enviem una string.
	public void send(String message) {
		
		//System.out.println("sending: "+message);
		
		out.println(message);
	}
	
	//on enviem una string i rebem una resposta que retornem.
	public String sendAndReceive(String message) throws IOException {
		
		send(message);
		
		return receive();
	}
	
	//on enviem una string i esperem rebre response com a resposta.
	public void sendAndReceive(String message, String response) throws IOException, ProtocoloFallidoException {
		
		send(message);
		
		receive(response);
		
	}
	
	//on rebem un missatge que retornem.
	public String receive() throws IOException {
		
		String line = in.readLine();
		
		//System.out.println("received: "+line);
		return line;
	}
	
	//on esperem rebre un missatge concret.
	public void receive(String receive) throws IOException, ProtocoloFallidoException {
		
		String cosaQueHeRecibido= receive();
		
		if(!cosaQueHeRecibido.equals(receive)) {
			throw new ProtocoloFallidoException("esperaba recibir "+receive+" String recibido: "+cosaQueHeRecibido);
		}
	}
	
	//on esperem rebre un missatge concret i responem amb response.
	public void receiveAndSend(String message, String response) throws IOException, ProtocoloFallidoException {
		
		receive(message);
		send(response);
		
	}
	/*
	//obre buffers d�entrada i sortida pel socket donat. //recomano que aix� sigui el constructor.
	public void open(Socket socket) {

		GestorMissatges gm = new GestorMissatges(socket);

	}
	*/
	//tanca els buffers d�entrada i sortida.
	public void close() throws IOException {
		this.in.close();
		this.out.close();
	}
	
}
