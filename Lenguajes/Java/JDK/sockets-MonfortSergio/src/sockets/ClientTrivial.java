package sockets;


import java.io.*;
import java.net.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ClientTrivial {

	
	public static void main(String[] args) {
		
		
		String hostName = "10.1.82.64";
        int portNumber = 60009;
        boolean jugando = true;

        try(
        		
        	Socket echoSocket = new Socket(hostName, portNumber);
        	GestorMissatges gm = new GestorMissatges(echoSocket);
        		
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ){
            String userInput;
            
            
            //primero conectarse
            //luego blablabla...
            
            gm.receiveAndSend("BENVINGUT", mensajes.ACK);
            
            //mientras no decidas hacer dc te va mandando preguntas
            while(jugando) {
            	gm.receiveAndSend("PREPARAT", mensajes.ACK);
                
                String pregunta = gm.receive();
                
                
				JSONObject JSONobj = new JSONObject(pregunta);

				
				String enunciado = (String) JSONobj.get("pregunta");
				JSONArray respuestasJSON = (JSONArray) JSONobj.get("respostes");
				
				
				System.out.println("PREGUNTA: "+enunciado+"\n");
				
				
				System.out.println("SELECCIONA UNA RESPUESTA (1/2/3/4 segun las respuestas que haya)");
				
				for(int i =0; i<respuestasJSON.length();i++) {

					String respuesta = (String) respuestasJSON.get(i);

					int num = i+1;
					
					System.out.println("\n"+num+": "+respuesta);
				}
				
				
				
                //System.out.println("pregunta rebuda: "+pregunta);
                
                gm.send(mensajes.ACK);
                
                //seleccionas la respuesta
                userInput = stdIn.readLine();
                
                //envio la respuesta y espero un ACK
                gm.sendAndReceive(userInput, mensajes.ACK);
                
                //recibo si la respuesta es correcta o no
                String resultado = gm.receive();
                
                if(resultado.equals(mensajes.CORRECTE)) {
                	System.out.println("muy bien! respuesta correcta! toma una galletita");
                	gm.send(mensajes.ACK);
                }else if(resultado.equals(mensajes.INCORRECTE)) {
                	System.out.println("manco, respuesta incorrecta");
                	gm.send(mensajes.ACK);
                }else {
                	System.out.println("._.");
                	gm.send("error chungo");
                }
                
                //el server pregunta al jugador si quieres seguir
                gm.receive(mensajes.WANNA_CONTINUE);
                
                //el jugador decide si quiere continuar o no
                System.out.println("quieres seguir jugando? (Y/N)");
                userInput = stdIn.readLine();
                
                if(userInput.equals("Y") || userInput.equals("y")) {
                	gm.send(mensajes.YES_CONTINUE);
                }else {
                	gm.send(mensajes.NO_CONTINUE);

                    //si dice que no ps se acaba
                	System.out.println("cerrando juego");
                	jugando=false;
                }
                
            }
            
        }catch (UnknownHostException e){
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        }catch (IOException e){
        	
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        }catch (ProtocoloFallidoException e){
        	
            System.err.println("no has seguido el protocolo, pedazo tonto: "+e.getMessage());
            System.exit(1);
        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

		
		
		

	}

}
