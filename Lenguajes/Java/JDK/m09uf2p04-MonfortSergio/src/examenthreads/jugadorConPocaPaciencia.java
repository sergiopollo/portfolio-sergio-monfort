package examenthreads;

import java.util.Random;
import java.util.concurrent.Callable;


public class jugadorConPocaPaciencia extends jugador implements Callable<Boolean>{
	
	private int intentsRegne=3;
	private int intentsServidor=7;
	
	
	public jugadorConPocaPaciencia(servidor servidorAlQueQuieroIr, regne regne) {
		super(servidorAlQueQuieroIr, regne);
		
	}

	@Override
	public Boolean call(){
		
		try {
			while(!heJugado) {
				//intento entrar
				if(regne.EntrarEnReino(this)) {
					//he entrado
					while(!heJugado) {
						//intento entrar a mi servidor
						
						if(ServidorAlQueQuieroIr.EntrarEnServidor(this)) {
							
							System.out.println(this+" Per fi jugant a "+this.ServidorAlQueQuieroIr);
							//he entrado y juego

							//pongo esto aqui para el ejercicio del mantenimiento
							this.heJugado=true;
							
							Random r = new Random();
							int random = r.nextInt(1000)+10;
							
							Thread.sleep(random);
							
							System.out.println(this+" ja prou per avui, deixant plaza lliure a "+this.ServidorAlQueQuieroIr);
							
							//salgo del servidor
							ServidorAlQueQuieroIr.saleJugador();
							
							//salgo del reino
							regne.saleJugador();
							
							System.out.println(this+" ha acabado");
						}else {
							
							this.intentsServidor--;
							
							if(this.intentsServidor == 0) {
								System.out.println(this+" me he cansado de esperar para entrar a "+this.ServidorAlQueQuieroIr+", me piro");
								//salgo del reino
								regne.saleJugador();
							}else {

								//si no consigue entrar al server se espera	
								synchronized(ServidorAlQueQuieroIr.semaforo) {
									System.out.println("jugador "+this+" un altre cop cua a "+this.ServidorAlQueQuieroIr+"?!");
									ServidorAlQueQuieroIr.semaforo.wait();
								}
							}
							
						}
					}
				}else {
					
					this.intentsRegne--;
					
					if(this.intentsRegne == 0) {
						
						System.out.println(this+" me he cansado de esperar para entrar al fokin juego, chau");
						//acabo
						
					}else {

						//si no consigue entrar espera y luego lo vuelve a intentar
						synchronized(regne.semaforo) {
							System.out.println("jugador "+this+" no puc ni entrar al joc!");
							regne.semaforo.wait();
						}
					}
					
				}
			}
		} catch (InterruptedException e) {
			//aqui deberia entrar solo en el ejercicio del mantenimiento del servidor
			//deberia llegar aqui solo si el jugador estaba conectado al servidor
			System.out.println("me han echado del juego");
			
			//salgo del servidor
			ServidorAlQueQuieroIr.saleJugador();
			
			//salgo del reino
			regne.saleJugador();
			
		}
		return null;
		
	}

	
	
	
	
}
