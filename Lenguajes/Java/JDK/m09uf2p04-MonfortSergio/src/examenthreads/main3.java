package examenthreads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


public class main3 {

	public static void main(String[] args) {
		
		
		regneConMantenimiento regne = new regneConMantenimiento();
		
		servidor zinn = new servidor("Zinnervale", regne, 20);
		servidor kad = new servidor("Kadan", regne, 20);
		servidor nin = new servidor("Nineveh", regne, 10);
		
		ArrayList<jugador> jugadors = new ArrayList<jugador>();
		
		for (int i = 0; i < 20; i++) {
			
			jugadorConPocaPaciencia c1 = new jugadorConPocaPaciencia(zinn, regne);
			jugadors.add(c1);
			
		}
		
		for (int i = 0; i < 20; i++) {
			
			jugadorConPocaPaciencia c1 = new jugadorConPocaPaciencia(kad, regne);
			jugadors.add(c1);
			
		}
		
		for (int i = 0; i < 10; i++) {
			
			jugadorConPocaPaciencia c1 = new jugadorConPocaPaciencia(nin, regne);
			jugadors.add(c1);
			
		}
		
		Collections.shuffle(jugadors);
		
		ExecutorService executor = Executors.newCachedThreadPool();

		Future<Boolean> regneFuturo = executor.submit(regne);
		
		ArrayList<Future<Boolean>> jugadorsFuturo = new ArrayList<Future<Boolean>>();
		
		for (int i = 0; i < jugadors.size(); i++) {
			jugadorsFuturo.add(executor.submit(jugadors.get(i)));
		}
		
		executor.shutdown();
		
		try {
			//cuando acabe el temporizador inicia el mantenimiento y hace el shutdownNow
			regneFuturo.get();
		
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			
			
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		try {
			
			for (int i = 0; i < jugadorsFuturo.size(); i++) {	
				
				jugadorsFuturo.get(i).get();
			
			}
			
		} catch (InterruptedException | ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		
		
		
		
		
		
		
		
		

	}

}
