package examenthreads;

import java.util.Random;
import java.util.concurrent.Callable;

public class jugador implements Callable<Boolean>{
	
	protected String name;
	private static int numeroName;
	protected regne regne;
	protected servidor ServidorAlQueQuieroIr;
	protected boolean heJugado=false;
	
	
	public jugador(servidor servidorAlQueQuieroIr, regne regne) {
		super();
		
		this.numeroName++;
		this.name = "jugador"+numeroName;
		ServidorAlQueQuieroIr = servidorAlQueQuieroIr;
		this.regne=regne;
	}
	
	
	
	@Override
	public Boolean call(){
		
		try {
			while(!heJugado) {
				//intento entrar
				if(regne.EntrarEnReino(this)) {
					//he entrado
					while(!heJugado) {
						//intento entrar a mi servidor
						
						if(ServidorAlQueQuieroIr.EntrarEnServidor(this)) {
							
							System.out.println(this+" Per fi jugant a "+this.ServidorAlQueQuieroIr);
							//he entrado y juego
							Random r = new Random();
							int random = r.nextInt(90)+10;
							
							Thread.sleep(random);
							
							System.out.println(this+" ja prou per avui, deixant plaza lliure a "+this.ServidorAlQueQuieroIr);
							
							//salgo del servidor
							ServidorAlQueQuieroIr.saleJugador();
							
							//salgo del reino
							regne.saleJugador();
							
							this.heJugado=true;
							System.out.println(this+" ha acabado");
						}else {
							//si no consigue entrar al server se espera	
							synchronized(ServidorAlQueQuieroIr.semaforo) {
								System.out.println("jugador "+this+" un altre cop cua a "+this.ServidorAlQueQuieroIr+"?!");
								ServidorAlQueQuieroIr.semaforo.wait();
							}
						}
					}
				}else {
					//si no consigue entrar espera y luego lo vuelve a intentar
					synchronized(regne.semaforo) {
						System.out.println("jugador "+this+" no puc ni entrar al joc!");
						regne.semaforo.wait();
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("me han interrumpido el call");
		}
		return null;
		
	}


	@Override
	public String toString() {
		return  name ;
	}
	
	
	
	
}
