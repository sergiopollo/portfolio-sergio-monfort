package examenthreads;


public class servidor {
	
	private String name;
	private regne regne;
	private int capacitat;
	private int capacitatActual;
	public Object semaforo = new Object();
	
	public servidor(String name, regne regne, int capacitat) {
		super();
		this.name = name;
		this.capacitat = capacitat;
		this.capacitatActual = capacitat;
	}

	public boolean EntrarEnServidor(jugador jug) {
		
		synchronized(semaforo) {
			System.out.println("capacitatServer"+this.name+": "+capacitatActual);
			if(capacitatActual>0 && capacitatActual<=capacitat){
				this.capacitatActual--;
				
				return true;
			}else {
				
				return false;
			}
		}
	}
	
	public void saleJugador() {
		synchronized(semaforo) {
			this.capacitatActual++;
			System.out.println("capacitatServer"+this.name+": "+capacitatActual);
			this.semaforo.notifyAll();
		}
	}

	@Override
	public String toString() {
		return "servidor "+name;
	}
	
	
	
}
