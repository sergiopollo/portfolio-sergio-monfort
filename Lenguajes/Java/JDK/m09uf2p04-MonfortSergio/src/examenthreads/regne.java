package examenthreads;

public class regne {
	
	private int capacitat;
	private int capacitatActual;
	public Object semaforo = new Object();
	
	public regne() {
		super();
		this.capacitat = 40;
		this.capacitatActual = 40;
	}
	
	public boolean EntrarEnReino(jugador jug) {
		
		synchronized(semaforo) {
			System.out.println("capacidadRegne: "+capacitatActual);
			if(capacitatActual>0 && capacitatActual<=capacitat){
				this.capacitatActual--;
				//System.out.println("customer "+c+" ha entrado");
				return true;
			}else {
				//System.out.println("customer "+c+" ha intentado entrar y no ha podido");
				return false;
			}
		}
	}
	
	public void saleJugador() {
		synchronized(semaforo) {
			this.capacitatActual++;
			System.out.println("capacidadRegne: "+capacitatActual);
			this.semaforo.notifyAll();
		}
	}
	
}
