package examenthreads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class main {

	public static void main(String[] args) {
		
		
		regne regne = new regne();
		
		servidor zinn = new servidor("Zinnervale", regne, 20);
		servidor kad = new servidor("Kadan", regne, 20);
		servidor nin = new servidor("Nineveh", regne, 10);
		
		ArrayList<jugador> jugadors = new ArrayList<jugador>();
		
		for (int i = 0; i < 20; i++) {
			
			jugador c1 = new jugador(zinn, regne);
			jugadors.add(c1);
			
		}
		
		for (int i = 0; i < 20; i++) {
			
			jugador c1 = new jugador(kad, regne);
			jugadors.add(c1);
			
		}
		
		for (int i = 0; i < 10; i++) {
			
			jugador c1 = new jugador(nin, regne);
			jugadors.add(c1);
			
		}
		
		Collections.shuffle(jugadors);
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		ArrayList<Future<Boolean>> jugadorsFuturo = new ArrayList<Future<Boolean>>();
		
		for (int i = 0; i < jugadors.size(); i++) {
			jugadorsFuturo.add(executor.submit(jugadors.get(i)));
		}
		
		executor.shutdown();
		
		for (int i = 0; i < jugadorsFuturo.size(); i++) {	
			try {
				jugadorsFuturo.get(i).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		

	}

}
