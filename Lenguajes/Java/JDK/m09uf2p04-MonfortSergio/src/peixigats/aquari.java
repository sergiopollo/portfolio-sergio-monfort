package peixigats;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class aquari implements Callable<Boolean>{
	
	private ArrayList<peix> peixos;
	private boolean heSacadoPez =false;

	public aquari(int numPeixos) {
		super();
		this.peixos = new ArrayList<peix>();
		for(int i=0; i<numPeixos; i++) {
			this.peixos.add(new peix());
		}
	}

	@Override
	public Boolean call() {
		
		try {
			
			while(peixos.size() > 0) {
				
				
				Thread.sleep(75);
				this.heSacadoPez=false;
				System.out.println("sale un pez del acuario");	
				synchronized(this) {
					this.notifyAll();
				}
			
			}
			System.out.println("ya no quedan peces");
				
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}

	public synchronized peix sacoPez(mixo gatete) {
		
		if(!heSacadoPez) {
			this.heSacadoPez=true;
			peix pez = peixos.get(0);
			peixos.remove(pez);
			System.out.println(gatete+" ha cogido al pez");
			return pez; 
		}
		
		return null;
	}
	
	@Override
	public String toString() {
		return "aquari [peixos=" + peixos + "]";
	}
	
	
}
