package peixigats;

import java.util.Random;

public class peix {
	
	private int pes;

	public peix() {
		super();
		Random r = new Random();
		int random = r.nextInt(200)+50;
		this.pes=random;
	}

	@Override
	public String toString() {
		return "peix [pes=" + pes + "]";
	}

	public int getPes() {
		return pes;
	}
	
}
