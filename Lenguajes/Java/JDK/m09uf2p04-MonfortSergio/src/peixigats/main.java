package peixigats;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import act04.Desirable;
import act04.Eager;

public class main {

	public static void main(String[] args) {
		
		aquari aquari = new aquari(20);
		
		ArrayList<mixo> mixos = new ArrayList<mixo>();
		
		for (int i = 0; i < 5; i++) {
			
			mixo gatete = new mixo(aquari);
			mixos.add(gatete);
			
		}
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		for (int i = 0; i < mixos.size(); i++) {
			executor.submit(mixos.get(i));
		}
		
		Future<Boolean> aquariFuturo = executor.submit(aquari);
		
		
		executor.shutdown();
		
		
		try {
			//se espera a que el acuario se quede sin peces
			aquariFuturo.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		try {
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			
			//me espero a que todos digan su mensaje de interrupcion
			Thread.sleep(100);
			
			System.out.println("RESULTADOS: ");
			for (int i = 0; i < mixos.size(); i++) {
				System.out.println(mixos.get(i));
			}
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
	}

}
