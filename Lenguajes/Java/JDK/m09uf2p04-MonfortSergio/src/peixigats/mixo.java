package peixigats;

import java.util.concurrent.Callable;


public class mixo implements Callable<Boolean>{

	private String nom;
	private static int contador;
	private int numPeixosMenjats;
	private int numGramsTotals;
	private aquari aquari;
	
	public mixo(aquari aquari) {
		super();
		contador++;
		this.nom="mixo"+contador;
		this.aquari=aquari;
	}

	@Override
	public Boolean call() {
		
		try {
			
			while(true) {

				//me espero a que el acuario saque un pez
				synchronized(aquari) {
					aquari.wait();
				}
				
				//miro a ver si puedo pillar al pez
				System.out.println(this+" intenta coger un pez");
				peix pez = aquari.sacoPez(this);
				
				if(pez == null) {
					System.out.println(this+" no ha cogido nada...");
				}else {
					System.out.println(this+" se queda comiendo el pez");
					this.numGramsTotals+=pez.getPes();
					this.numPeixosMenjats++;
					Thread.sleep(500);
					
				}
				System.out.println(this+" espera a que salga otro pez");
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("el acuario se ha apagao, sad :C");
		}
		
		return null;
	}

	@Override
	public String toString() {
		return "mixo [nom=" + nom + ", numPeixosMenjats=" + numPeixosMenjats + ", numGramsTotals=" + numGramsTotals
				+ "]";
	}

}
