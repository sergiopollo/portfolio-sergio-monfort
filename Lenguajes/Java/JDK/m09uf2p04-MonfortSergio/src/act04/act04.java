package act04;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class act04 {
	
	public static void main(String[] args) {
		
		
		Desirable kane = new Desirable("kane");
		
		ArrayList<Eager> simps = new ArrayList<Eager>();
		
		for (int i = 0; i < 20; i++) {
			
			Eager pesao = new Eager("simp"+i,kane);
			simps.add(pesao);
			
		}
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		ArrayList<Future<Boolean>> futureSimps = new ArrayList<Future<Boolean>>();
		
		for (int i = 0; i < simps.size(); i++) {
			Future<Boolean> eagerFuturo = executor.submit(simps.get(i));
			futureSimps.add(eagerFuturo);
		}
		
		Future<Boolean> desirableFuturo = executor.submit(kane);
		
		
		executor.shutdown();
		
		for (int i = 0; i < futureSimps.size(); i++) {	
			try {
				futureSimps.get(i).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		try {
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
