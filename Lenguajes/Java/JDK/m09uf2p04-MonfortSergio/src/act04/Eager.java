package act04;

import java.util.concurrent.Callable;

public class Eager implements Callable<Boolean> {

	private String nombre;
	private Desirable desirable;
	private int numSaludos;

	public Eager(String nombre, Desirable desirable) {
		super();
		this.nombre = nombre;
		this.desirable = desirable;
	}

	@Override
	public Boolean call() {

		try {
			
			while (numSaludos < 3) {
				
				synchronized (desirable.disponible) {
					System.out.println("soy " + this + " y estoy esperando a " + desirable);

					desirable.disponible.wait();

				}
				
				boolean puedeSaludar = desirable.saludo(this);

				if (puedeSaludar) {
					try {

						this.numSaludos++;
						System.out.println(this + " saluda a " + desirable+"(he saludado "+numSaludos+" veces)");
						Thread.sleep(100);
						synchronized (desirable.saludado) {
							desirable.saludado.notify();
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("me ha dejado en visto ("+this+") (he saludado "+numSaludos+" veces)");
				}
				
				//System.out.println("soy " + this + ", heSaludado:"+heSaludado);
				
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	
	
	public int getNumSaludos() {
		return numSaludos;
	}

	@Override
	public String toString() {
		return "Eager " + nombre;
	}

}
