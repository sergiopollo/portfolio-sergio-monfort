package bakery;

public class bakery {
	
	public Object fuera= new Object();
	public Object dentro = new Object();
	private int numCapacidadDeGente=3;
	private int numStaffQuePuedeAtender=2;
	
	public bakery() {
		super();
	}
	
	public boolean entrarDentro(customer c) {
		
		synchronized(fuera) {
			System.out.println("capacidad: "+numCapacidadDeGente);
			if(numCapacidadDeGente>0 && numCapacidadDeGente<=3){
				this.numCapacidadDeGente--;
				System.out.println("customer "+c+" ha entrado");
				return true;
			}else {
				System.out.println("customer "+c+" ha intentado entrar y no ha podido");
				return false;
			}
		}
	}
	
	public boolean comprar(customer c) {
		
		synchronized(dentro) {
			System.out.println("staff: "+numStaffQuePuedeAtender);
			if(numStaffQuePuedeAtender>0 && numStaffQuePuedeAtender<=3){
				this.numStaffQuePuedeAtender--;
				System.out.println("customer "+c+" esta comprando");
				return true;
			}else {
				System.out.println("customer "+c+" ha intentado comprar pero no ha podido");
				return false;
			}
		}
	}
	
	public synchronized void salgoDeLaTienda() {
		synchronized(this.fuera) {
			this.numStaffQuePuedeAtender++;
			System.out.println("staff: "+this.numStaffQuePuedeAtender);
			this.fuera.notifyAll();
		}
	}
	
	public void dejoDeSerAtendido() {
		synchronized(this.dentro) {
			this.numCapacidadDeGente++;
			System.out.println("capacidadGente: "+this.numCapacidadDeGente);
			this.dentro.notifyAll();
		}
	}
	
	
	
	
}
