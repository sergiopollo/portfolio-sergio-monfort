package bakery;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


public class main {

	public static void main(String[] args) {
		
		
		bakery bakery = new bakery();
		
		ArrayList<customer> customers = new ArrayList<customer>();
		
		for (int i = 0; i < 5; i++) {
			
			customer c1 = new customer(bakery);
			customers.add(c1);
			
		}
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		ArrayList<Future<Boolean>> customersFuturo = new ArrayList<Future<Boolean>>();
		
		for (int i = 0; i < customers.size(); i++) {
			customersFuturo.add(executor.submit(customers.get(i)));
		}
		
		//Future<Boolean> aquariFuturo = executor.submit(aquari);
		
		
		executor.shutdown();
		
		for (int i = 0; i < customersFuturo.size(); i++) {	
			try {
				customersFuturo.get(i).get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/*
		try {
			//se espera a que el acuario se quede sin peces
			//aquariFuturo.get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	/*
		try {
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			
			//me espero a que todos digan su mensaje de interrupcion
			Thread.sleep(100);
			
			System.out.println("RESULTADOS: ");
			
			for (int i = 0; i < mixos.size(); i++) {
				System.out.println(mixos.get(i));
			}
			
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		*/
		
		
	}

}
