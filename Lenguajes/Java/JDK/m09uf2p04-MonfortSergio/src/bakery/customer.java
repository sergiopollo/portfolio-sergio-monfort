package bakery;

import java.util.concurrent.Callable;

public class customer implements Callable<Boolean>{
	
	private String name;
	private static int numCustomers;
	private bakery bakery;
	private boolean heComprado=false;

	public customer(bakery bakery) {
		super();
		this.numCustomers++;
		this.name="customer"+this.numCustomers;
		this.bakery = bakery;
	}
	
	
	
	
	@Override
	public Boolean call() {
		try {
			while(!heComprado) {
				//intento entrar
				if(bakery.entrarDentro(this)) {
					//he entrado
					while(!heComprado) {
						//intento comprar
						if(bakery.comprar(this)) {
							//he comprado
							Thread.sleep(50);
							System.out.println(this+" he acabado de comprar, me piro");
							//dejo de ser atendido
							//synchronized(bakery.dentro) {
							bakery.dejoDeSerAtendido();
							
							//synchronized(bakery.fuera) {
							//salgo de la bakery
							bakery.salgoDeLaTienda();
							
							this.heComprado=true;
							System.out.println(this+" ha acabado");
						}else {
							//si no consigue comprar se espera	
							synchronized(bakery.dentro) {
								System.out.println("customer "+this+" se espera dentro");
								bakery.dentro.wait();
							}
						}
					}
				}else {
					//si no consigue entrar espera y luego lo vuelve a intentar
					synchronized(bakery.fuera) {
						System.out.println("customer "+this+" se espera fuera");
						bakery.fuera.wait();
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("me han interrumpido el call");
		}
		return null;
	}

	@Override
	public String toString() {
		return  name ;
	}



	
	
	
	
	
	
}
