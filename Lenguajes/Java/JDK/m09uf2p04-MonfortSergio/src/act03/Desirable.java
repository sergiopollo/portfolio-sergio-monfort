package act03;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Desirable implements Callable<Boolean>{

	private String nombre;
	public Object disponible = new Object();
	public Object saludado = new Object();
	//puede saludar a 5
	private int personasSaludandomeALaVez = 0;
	private int limiteDePersonasSaludandomeALaVez = 5;
	
	public Desirable(String nombre) {
		super();
		this.nombre = nombre;
	}


	public synchronized boolean saludo(Eager pesao) {
		
		if(personasSaludandomeALaVez < limiteDePersonasSaludandomeALaVez) {
			personasSaludandomeALaVez++;
			System.out.println("personas saludandome: "+personasSaludandomeALaVez);
			return true;
		}else {
			return false;
		}
		
	}
	

	@Override
	public Boolean call() {
		
		try {
			
			Thread.sleep(1000);
			System.out.println("OJO QUE HE LLEGADO!! (soy: "+this+")");
			
			synchronized(disponible){
				disponible.notifyAll();
			}
			
			while(true) {
				
				synchronized(saludado){
					saludado.wait();
					
					//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
					personasSaludandomeALaVez--;
					System.out.println("personas saludandome: "+personasSaludandomeALaVez);
					//AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
					synchronized(disponible){
						disponible.notifyAll();
					}
				}
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("me han interrumpido o he acabado de saludar a todos");
			//e.printStackTrace();
		}
		
		return null;
	}


	
	
	public int getPersonasSaludandomeALaVez() {
		return personasSaludandomeALaVez;
	}


	public void setPersonasSaludandomeALaVez(int personasSaludandomeALaVez) {
		this.personasSaludandomeALaVez = personasSaludandomeALaVez;
	}


	@Override
	public String toString() {
		return "Desirable "+ nombre;
	}

}
