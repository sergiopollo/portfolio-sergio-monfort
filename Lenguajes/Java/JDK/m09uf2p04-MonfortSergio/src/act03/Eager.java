package act03;

import java.util.concurrent.Callable;

public class Eager implements Callable<Boolean> {

	private String nombre;
	private Desirable desirable;
	private boolean heSaludado = false;

	public Eager(String nombre, Desirable desirable) {
		super();
		this.nombre = nombre;
		this.desirable = desirable;
	}

	@Override
	public Boolean call() {

		try {
			
			while (!heSaludado) {
				
				synchronized (desirable.disponible) {
					System.out.println("soy " + this + " y estoy esperando a " + desirable);

					desirable.disponible.wait();

				}
				
				boolean puedeSaludar = desirable.saludo(this);

				if (puedeSaludar) {
					try {
						System.out.println(this + " saluda a " + desirable);
						Thread.sleep(100);
						this.heSaludado = true;
						synchronized (desirable.saludado) {
							desirable.saludado.notify();
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("me ha dejado en visto ("+this+")");
				}
				
				//System.out.println("soy " + this + ", heSaludado:"+heSaludado);
				
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String toString() {
		return "Eager " + nombre;
	}

}
