package act01;

import java.util.concurrent.Callable;

public class Eager implements Callable<Boolean> {

	private String nombre;
	private Desirable desirable;

	public Eager(String nombre, Desirable desirable) {
		super();
		this.nombre = nombre;
		this.desirable = desirable;
	}

	@Override
	public Boolean call() {

		try {
			
			
				
				synchronized (desirable.disponible) {
					System.out.println("soy " + this + " y estoy esperando a " + desirable);

					desirable.disponible.wait();

				}
				
				boolean puedeSaludar = desirable.saludo(this);

				if (puedeSaludar) {
					try {
						System.out.println(this + " saluda a " + desirable);
						Thread.sleep(50);
						synchronized (desirable.saludado) {
							desirable.saludado.notify();
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("me ha dejado en visto");
				}
				
				
				
			

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String toString() {
		return "Eager " + nombre;
	}

}
