package act01;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class act01 {
	
	public static void main(String[] args) {
		
		
		Desirable kane = new Desirable("kane");
		
		ArrayList<Eager> simps = new ArrayList<Eager>();
		
		for (int i = 0; i < 5; i++) {
			
			Eager pesao = new Eager("simp"+i,kane);
			simps.add(pesao);
			
		}
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		for (int i = 0; i < simps.size(); i++) {
			Future<Boolean> eagerFuturo = executor.submit(simps.get(i));
		}
		
		Future<Boolean> desirableFuturo = executor.submit(kane);
		
		
		executor.shutdown();
		
		
		
		
	}
	
}
