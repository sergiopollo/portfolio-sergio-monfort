package act02;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Desirable implements Callable<Boolean>{

	private String nombre;
	private boolean puedoSaludar;
	public Object disponible = new Object();
	public Object saludado = new Object();
	
	public Desirable(String nombre) {
		super();
		this.nombre = nombre;
		this.puedoSaludar=true;
	}


	public synchronized boolean saludo(Eager pesao) {
		
		if(puedoSaludar) {
			this.puedoSaludar=false;
			return true;
		}else {
			return false;
		}
		
	}
	

	@Override
	public Boolean call() {
		
		try {
			
			Thread.sleep(1000);
			System.out.println("OJO QUE HE LLEGADO!! (soy: "+this+")");
			
			synchronized(disponible){
				disponible.notifyAll();
			}
			
			while(true) {
				
				synchronized(saludado){
					saludado.wait();
					this.puedoSaludar=true;
					
					synchronized(disponible){
						disponible.notifyAll();
					}
				}
			}
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("me han interrumpido o he acabado de saludar a todos");
			//e.printStackTrace();
		}
		
		return null;
	}


	@Override
	public String toString() {
		return "Desirable "+ nombre;
	}

}
