package GameLoop;

import java.util.Random;

public class Timeout extends Component {
	
	private double timeToLive;
	private Timer timer;
	private boolean vivo=true;
	
	public Timeout() {
		super();
		Random r = new Random();
		double random = r.nextInt(6);
		this.timeToLive=random+1;
		timer = new Timer(false);
		
		//se que es terrorismo, es para pruebas
		System.out.println("timetolive: "+timeToLive);
	}
	
	
	
	public boolean isVivo() {
		return vivo;
	}
	
	public void resetTimeout() {
		this.timer.resetTimer();
		this.vivo=true;
	}


	@Override
	public void update(float deltaTime) {
		//System.out.println("soy "+this.getClass()+" y me updateo cada "+deltaTime+" segundos");
		timer.update(deltaTime);
		if(this.timeToLive == timer.getTimePassed() && vivo) {
			//System.out.println("me desactivo");
			this.vivo=false;
			GameLoop.getInstance().getEventManager().notifyObservers("DEAD", this.getGameObject());
			//this.getGameObject().removeComponent(this);
		}
	}

	@Override
	public void render() {
		//System.out.println("soy "+this.getClass()+" y me renderizo");

	}

}
