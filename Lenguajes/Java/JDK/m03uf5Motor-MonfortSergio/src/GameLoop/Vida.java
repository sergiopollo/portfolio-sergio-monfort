package GameLoop;

public class Vida extends State {

	private Timer timer;
	private double timeToLive;
	private Component miComponent;
	
	public Vida(String name, Component miComponent, double timeToLive) {
		super(name);

		timer = new Timer(false);
		this.timeToLive=timeToLive;
		this.miComponent=miComponent;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

		System.out.println("estoy vivo");
		this.timer.resetTimer();
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		//System.out.println("aaa");
		//System.out.println(timer.getTimePassed());
		timer.update(deltaTime);
		if(this.timeToLive == timer.getTimePassed()) {
			//System.out.println("me desactivo");
			GameLoop.getInstance().getEventManager().notifyObservers("DEAD", this.miComponent.getGameObject());
			if(this.miComponent instanceof RETimeout) {
				RETimeout rt = (RETimeout) this.miComponent;
				this.timer.resetTimer();
				rt.getMaquinaEstados().popState();
			}
		}
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub
		
		System.out.println("mr stark i dont feel so good");
		this.timer.resetTimer();
		/*
		if(this.miComponent instanceof RETimeout) {
			RETimeout rt = (RETimeout) this.miComponent;
			this.timer.resetTimer();
			rt.getMaquinaEstados().setState(Mort.class);
		}
		*/
	}

}
