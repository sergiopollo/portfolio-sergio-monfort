package GameLoop;

import java.util.ArrayList;

public class GameObject {

	
	private ArrayList<Component> components = new ArrayList<Component>();;
	private ArrayList<Component> listaComponentsToAdd = new ArrayList<Component>();
	private ArrayList<Component> listaComponentsToRemove = new ArrayList<Component>();
	
	private boolean updating;
	
	public GameObject() {
		super();
		
		//GameLoop.getInstance().addGameObject(this);
	}



	public void addComponent(Component c) {
		
		if(this.updating) {
			this.listaComponentsToAdd.add(c);
			c.setGameObject(this);
		}else {
			this.components.add(c);
			c.setGameObject(this);
		}
	}
	
	public void removeComponent(Component c) {
		
		if(updating) {
			if(this.components.contains(c)) {
				c.setGameObject(null);
				this.listaComponentsToRemove.add(c);
				System.out.println("he borrado "+c);
			}
		}else {
			if(this.components.contains(c)) {
				c.setGameObject(null);
				this.components.remove(c);
				System.out.println("he borrado "+c);
			}
		}
	}
	
	public <T extends Component> boolean hasComponent(Class<T> clase) {
		for (Component component : components) {
			if(clase.isInstance(component)) {
				return true;
			}
		}
		return false;
	}
	
	public <T extends Component> T getComponent(Class<T> clase) {
		for (Component component : components) {
			if(clase.isInstance(component)) {
				return (T) component;
			}
		}
		return null;
	}
	
	public void update(float deltaTime) {
		updating=true;
		
		for (Component component : components) {
			component.update(deltaTime);
		}
		//updateo de componentsComponents
		components.removeAll(listaComponentsToRemove);
		components.addAll(listaComponentsToAdd);
		listaComponentsToRemove.clear();
		listaComponentsToAdd.clear();
		
		updating=false;
	}
	
	public void render() {
		for (Component component : components) {
			component.render();
		}
	}

	@Override
	public String toString() {
		return "GameObject [components=" + components + "]";
	}
}
