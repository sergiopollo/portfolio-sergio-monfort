package GameLoop;

public abstract class State {
	
	private String name;

	public State(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	
	public abstract void init();
	
	public abstract void update(float deltaTime);
	
	public abstract void exit();

	@Override
	public String toString() {
		return "State [type="+this.getClass()+", name=" + name + "]";
	}
}
