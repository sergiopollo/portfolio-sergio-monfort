package GameLoop;

import java.util.Random;

public class RETimeout extends Component {
	
	private double timeToLive;
	//private Timer timer;
	private StackStateMachine maquinaEstados;
	private Vida v;
	private Mort m;
	private Ressurreccio res;
	
	public RETimeout() {
		super();
		Random r = new Random();
		double random = r.nextInt(6);
		this.timeToLive=random+1;
		//timer = new Timer(false);
		
		//se que es terrorismo, es para pruebas
		System.out.println("timetolive: "+timeToLive);
		this.maquinaEstados = new StackStateMachine();
		this.v = new Vida("vida",this, timeToLive);
		this.m = new Mort("mort",this);
		this.res = new Ressurreccio("ressurreccio",this);
		maquinaEstados.addState(v);
		maquinaEstados.addState(m);
		maquinaEstados.addState(res);
		this.maquinaEstados.pushState(res.getClass(), false);
		this.maquinaEstados.pushState(m.getClass(), false);
		this.maquinaEstados.pushState(v.getClass(), true);
		//this.maquinaEstados.setState(Vida.class);
	}
	
	
	public StackStateMachine getMaquinaEstados() {
		return maquinaEstados;
	}


	public boolean isVivo() {
		if(this.maquinaEstados.getStackStates().stackTop() == this.v) {
			return true;
		}
		return false;
	}
	

	@Override
	public void update(float deltaTime) {
		/*
		//System.out.println("soy "+this.getClass()+" y me updateo cada "+deltaTime+" segundos");
		timer.update(deltaTime);
		if(this.timeToLive == timer.getTimePassed() && this.maquinaEstados.getStackStates().stackTop() == this.m && !this.eventoEnviado) {
			//System.out.println("me desactivo");
			this.eventoEnviado=true;
			GameLoop.getInstance().getEventManager().notifyObservers("DEAD", this.getGameObject());
			this.maquinaEstados.pushState(m.getClass());
			//this.getGameObject().removeComponent(this);
		}
		*/
		
		this.maquinaEstados.update(deltaTime);
		//System.out.println(maquinaEstados.getStackStates());
	}

	@Override
	public void render() {
		//System.out.println("soy "+this.getClass()+" y me renderizo");

	}

}

