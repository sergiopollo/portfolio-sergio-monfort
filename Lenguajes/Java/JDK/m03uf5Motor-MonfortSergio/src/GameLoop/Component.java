package GameLoop;

public abstract class Component {
	
	private GameObject gameObjectAlCualPertenezco;
	
	public GameObject getGameObject() {
		return this.gameObjectAlCualPertenezco;
	}
	
	public void setGameObject(GameObject gameObject) {
		this.gameObjectAlCualPertenezco = gameObject;
	}
	
	public abstract void update(float deltaTime);
	
	public abstract void render();

	@Override
	public String toString() {
		return "Component [Clase="+this.getClass()+" gameObjectAlCualPertenezco=" + "]";
	}
}
