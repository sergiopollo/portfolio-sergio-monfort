package GameLoop;

public class Timer extends Component {

	private boolean showTimePassed;
	private int nextSecond=1;
	private double timePassed = 0;
	
	public Timer() {
		super();
		this.showTimePassed=true;
	}
	
	public Timer(boolean showTimePassed) {
		super();
		this.showTimePassed=showTimePassed;
	}
	
	public int getTimePassed() {
		return (int)timePassed;
	}
	
	public void resetTimer() {
		this.timePassed = 0;
	}
	
	@Override
	public void update(float deltaTime) {
		//System.out.println("soy "+this.getClass()+" y me updateo cada "+deltaTime+" segundos");
		timePassed += deltaTime;
		if((int)timePassed == nextSecond) {
			if(showTimePassed) {
				System.out.println("han pasado "+(int)timePassed+" segundos");
			}
			nextSecond++;
		}
	}

	@Override
	public void render() {
		//System.out.println("soy "+this.getClass()+" y me renderizo");

	}

}
