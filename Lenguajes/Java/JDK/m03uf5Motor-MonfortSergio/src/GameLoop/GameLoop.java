package GameLoop;

import java.util.ArrayList;
import java.util.Iterator;

public class GameLoop {
	
	//singleton
	private static GameLoop m_gameloop = null; 
	private ArrayList<GameObject> listaGameObjects = new ArrayList<GameObject>();
	private ArrayList<GameObject> listaGameObjectsToAdd = new ArrayList<GameObject>();
	private ArrayList<GameObject> listaGameObjectsToRemove = new ArrayList<GameObject>();
	private EventManager eventManager = new EventManager();

	private GameLoop(){
		super();
	}
	
	public static GameLoop getInstance(){
		if(m_gameloop == null)
			m_gameloop = new GameLoop();
		
		return m_gameloop;
	}
	
	public EventManager getEventManager() {
		return eventManager;
	}

	public void addGameObject(GameObject gameObject) {
		listaGameObjectsToAdd.add(gameObject);
	}
	
	public void removeGameObject(GameObject gameObject) {
		if(listaGameObjects.contains(gameObject)) {
			listaGameObjectsToRemove.add(gameObject);
		}
	}
	
	public ArrayList<GameObject> getGameObjects() {
		return this.listaGameObjects;
	}
	
	public void init() {
		
		/*
		//pruebas pool y spawner
		GameObject cosa = new GameObject();
		cosa.addComponent(new Spawner(3));
		
		GameLoop.getInstance().addGameObject(cosa);
		*/
		
		/*
		//pruebas events
		GameObject cosa = new GameObject();
		cosa.addComponent(new Spawner(1));
		
		GameLoop.getInstance().addGameObject(cosa);
		
		for(int i = 0; i< 10; i++) {
			
			GameObject go = new GameObject();
			go.addComponent(new CRespects());
			
			GameLoop.getInstance().addGameObject(go);
			
		}*/
		
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//toca hacer todo lo de maquines d'estats (stackstatemachine)
		//pruebas maquinas estados revividoras
		GameObject cosa = new GameObject();
		cosa.addComponent(new RETimeout());
		
		GameLoop.getInstance().addGameObject(cosa);
		
	}
	
	public void input() {
		
	}
	
	public void update(float deltaTime) {
		//System.out.println(listaGameObjects);
		for (GameObject gameObject : listaGameObjects) {
			gameObject.update(deltaTime);
		}
		//updateo de gameobjects
		listaGameObjects.removeAll(listaGameObjectsToRemove);
		listaGameObjects.addAll(listaGameObjectsToAdd);
		listaGameObjectsToRemove.clear();
		listaGameObjectsToAdd.clear();
	}
	
	public void render() {
		for (GameObject gameObject : listaGameObjects) {
			gameObject.render();
		}
	}
	
	
	
}
