package GameLoop;

import java.util.ArrayList;
import java.util.Iterator;


public class MyPool2<T> {
	
	//clase interna privada de objetos con un booleano para saber si estan en uso o no
	private class ObjectePool {
		
		public boolean EnUso;
		public T objectePool;

		public ObjectePool(T objectePool) {
			super();
			this.objectePool = objectePool;
		}

		@Override
		public String toString() {
			return "ObjectePool [objectePool=" + objectePool + "EnUso="+EnUso+"]";
		}
		
	}

	private ArrayList<ObjectePool> listaElementos = new ArrayList<ObjectePool>();
	private int tamanyo;
	private  IPoolableFactory<T> factory;
	
	//constructores
	public MyPool2(IPoolableFactory<T> factory) {
		initPool(factory, 10);
	}
	
	public MyPool2(IPoolableFactory<T> factory, int size) {
		initPool(factory, size);
	}
	
	private void initPool(IPoolableFactory<T> factory, int size)
	{
		this.factory=factory;
		this.tamanyo = size;
		listaElementos = new ArrayList<ObjectePool>();
		for (int i = 0; i < this.tamanyo; i++) {
			T element = factory.create();
			ObjectePool obP = new ObjectePool(element);
			listaElementos.add(obP);
		}
	}
	
	public T getElement() throws PoolVaciaException{
		
		for (int i = 0; i < listaElementos.size(); i++) {
			
			ObjectePool obP = listaElementos.get(i);
			
			if(obP.EnUso == false) {
				
				obP.EnUso = true;
				factory.activate(obP.objectePool);
				return obP.objectePool;
			}
			
		}
		//si no encuentra ninguno devuelve excepcion
		throw new PoolVaciaException();
	 
	}
	
	public boolean tryReturnElement(T element) throws PoolElementoIncorrectoException{
		
		for (int i = 0; i < listaElementos.size(); i++) {
			
			ObjectePool obP = listaElementos.get(i);
			
			if(element.equals(obP.objectePool)) {
				
				obP.EnUso = false;
				factory.deactivate(obP.objectePool);
				return true;

			}
		}
		throw new PoolElementoIncorrectoException();
		
	}
	
	@Override
	public String toString() {
		return ""+listaElementos;
	}
}

