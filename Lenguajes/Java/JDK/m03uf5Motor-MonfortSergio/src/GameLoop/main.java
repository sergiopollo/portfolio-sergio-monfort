package GameLoop;

public class main {

	public static void main(String[] args) {
		
		GameLoop gameloop = GameLoop.getInstance();
		
		//frames por segundo que quiere el usuario
		float FPS = 60;
		//milliseconds que queremos que duerma el sistema 
		Long sleepMillis = (long) (1000/FPS);
		//nanoseconds perdidos haciendo input update y render
		Long LostSleepNano;
		//milliseconds que le pasamos al thread.sleep teniendo en cuenta los nanoseconds perdidos
		Long finalSleepMillis;
		
		gameloop.init();
		
		Long tiempoFrameAnterior = System.nanoTime();
		float deltaTime;
		
		while(true) {
			//segundos transcurridos en este frame (pasados de nanoseconds a seconds)
			deltaTime=(System.nanoTime()-tiempoFrameAnterior)/1000000000f;
			//nanoseconds de este frame
			tiempoFrameAnterior = System.nanoTime();
			
			gameloop.input();
			gameloop.update(deltaTime);
			gameloop.render();
			
			//tiempo perdido en hacer el input update y render
			LostSleepNano=tiempoFrameAnterior-System.nanoTime();
			
			//tiempo que le tengo que pasar a thread.sleep para que lo haga realmente en el tiempo deseado restandole el tiempo perdido
			finalSleepMillis = (long) (sleepMillis-LostSleepNano/1000000f);
			try {
				Thread.sleep(finalSleepMillis);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

}
