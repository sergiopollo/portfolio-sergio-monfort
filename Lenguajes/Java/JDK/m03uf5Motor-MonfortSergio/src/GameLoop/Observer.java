package GameLoop;

public interface Observer {

	public void notifyObserver(String event, GameObject sender);
	
}
