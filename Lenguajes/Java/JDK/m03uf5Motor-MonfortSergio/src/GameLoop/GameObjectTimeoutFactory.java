package GameLoop;


public class GameObjectTimeoutFactory implements IPoolableFactory<GameObject>{
	
	private static GameObjectTimeoutFactory instance = null; 
	
	private GameObjectTimeoutFactory()
	{
		super();
	}
	
	public static GameObjectTimeoutFactory getInstance()
	{
		if(instance == null)
			instance = new GameObjectTimeoutFactory();
		
		return instance;
	}
	
	@Override
	public GameObject create() {
		GameObject go = new GameObject();
		//creo gameobjects que tienen timeouts
		go.addComponent(new Timeout());
		//les pongo Cname
		go.addComponent(new CName());
		System.out.println(go);
		return go;
	}

	@Override
	public void activate(GameObject element) {
		element.getComponent(Timeout.class).resetTimeout();
		GameLoop.getInstance().addGameObject(element);
	}

	@Override
	public void deactivate(GameObject element) {
		GameLoop.getInstance().removeGameObject(element);
	}

	@Override
	public void delete(GameObject element) {
		
	}
}
