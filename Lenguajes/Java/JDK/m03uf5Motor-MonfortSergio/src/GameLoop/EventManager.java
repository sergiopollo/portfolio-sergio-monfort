package GameLoop;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
	
	//diccionario
	private HashMap<String, ArrayList<Observer>> eventObservers = new HashMap<String, ArrayList<Observer>>();
	
	//constructor
	public EventManager() {
		super();
	}
	
	//getter
	public HashMap<String, ArrayList<Observer>> getObservers() {
		return eventObservers;
	}
	
	//subscriure's (es posa a la llista)
	public void subscribe(String evento, Observer ob) {
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers == null) {
			
			observers = new ArrayList<Observer>();
			eventObservers.put(evento, observers);
		}
		
		if(!observers.contains(ob)) {
			observers.add(ob);
		}	
	
	}
	
	//unsubscribe (borrar de la llista)
	public void unsubscribe(String evento, Observer ob) {
		
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers != null) {
			
			if(observers.contains(ob)) {
				observers.remove(ob);
			}
		}		
	}

	//notifyobservers (avisa a tots els de la llista)
	public void notifyObservers(String evento, GameObject GO) {
		
		ArrayList<Observer> observers = eventObservers.get(evento);
		if(observers != null) {
			
			for (int i = observers.size()-1; i >= 0; i--) {
				
				observers.get(i).notifyObserver(evento, GO);
			}
			
		}
	}
	
	
}
