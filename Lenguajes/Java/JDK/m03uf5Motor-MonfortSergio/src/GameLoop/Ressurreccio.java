package GameLoop;

public class Ressurreccio extends State {

	private Timer timer;
	private double timeToLive = 1;
	private Component miComponent;
	
	public Ressurreccio(String name, Component miComponent) {
		super(name);
		// TODO Auto-generated constructor stub
		this.miComponent = miComponent;
		timer = new Timer(false);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		System.out.println("were you killed? sadly yes...");
		this.timer.resetTimer();
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		timer.update(deltaTime);
		if(this.timeToLive == timer.getTimePassed()) {
			System.out.println("REVIVO");
			if(this.miComponent instanceof RETimeout) {
				RETimeout rt = (RETimeout) this.miComponent;
				this.timer.resetTimer();
				rt.getMaquinaEstados().popState();
			}
		}
	}

	@Override
	public void exit() {
		System.out.println("but i lived!!");
		this.timer.resetTimer();
		
		if(this.miComponent instanceof RETimeout) {
			RETimeout rt = (RETimeout) this.miComponent;
			//rt.getMaquinaEstados().pushState(this.getClass(), false);
			rt.getMaquinaEstados().pushState(Mort.class, false);
			rt.getMaquinaEstados().pushState(Vida.class, false);
			rt.getMaquinaEstados().pushState(Vida.class, true);
		}
		
		/*
		if(this.miComponent instanceof RETimeout) {
			RETimeout rt = (RETimeout) this.miComponent;
			this.timer.resetTimer();
			rt.getMaquinaEstados().setState(Vida.class);
		}
		*/
	}

}
