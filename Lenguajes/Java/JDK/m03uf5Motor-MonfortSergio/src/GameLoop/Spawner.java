package GameLoop;

import java.util.ArrayList;
import java.util.Random;

public class Spawner extends Component{
	
	private ArrayList<GameObject> activos = new ArrayList<GameObject>();
	private MyPool2<GameObject> pool;
	private int segonsAEsperar;
	private Timer timer;
	
	public Spawner(int size) {
		super();
		this.pool = new MyPool2<GameObject>(GameObjectTimeoutFactory.getInstance(), size);
		this.segonsAEsperar=3;
		timer = new Timer();
		
		//se que es terrorismo, es para pruebas
		System.out.println("segonsAEsperar: "+segonsAEsperar);
	}
	
	@Override
	public void update(float deltaTime) {
		timer.update(deltaTime);
		
		if(this.segonsAEsperar == timer.getTimePassed()) {
			timer.resetTimer();
			
			//version con pool
			System.out.println("activo un nuevo gameobject con timeout");
			
			try {
				//esto lo activa y lo a�ade al gameloop (viene con un timeout ya de por si)
				GameObject go = pool.getElement();
				
				activos.add(go);
				
				
			} catch (PoolVaciaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		for (int i = activos.size()-1; i >= 0; i--) {
			
			
			if(activos.get(i).getComponent(Timeout.class).isVivo() == false) {
				
				System.out.println("devuelvo el GO a la pool");
				
				try {
					pool.tryReturnElement(activos.get(i));
					activos.remove(i);
				} catch (PoolElementoIncorrectoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
	}

	@Override
	public void render() {
		
		
	}

}
