package GameLoop;

public class Mort extends State {

	private Timer timer;
	private double timeToLive = 3;
	private Component miComponent;
	
	public Mort(String name, Component miComponent) {
		super(name);

		timer = new Timer(false);
		this.miComponent=miComponent;
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		System.out.println("me mueroooo *dies*");
		this.timer.resetTimer();
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		timer.update(deltaTime);
		if(this.timeToLive == timer.getTimePassed()) {
			//System.out.println("me desactivo");
			if(this.miComponent instanceof RETimeout) {
				RETimeout rt = (RETimeout) this.miComponent;
				this.timer.resetTimer();
				rt.getMaquinaEstados().popState();
			}
		}
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub
		
		System.out.println("heroes never die!");
		this.timer.resetTimer();
		/*
		if(this.miComponent instanceof RETimeout) {
			RETimeout rt = (RETimeout) this.miComponent;
			this.timer.resetTimer();
			rt.getMaquinaEstados().setState(Ressurreccio.class);
		}*/
	}

}
