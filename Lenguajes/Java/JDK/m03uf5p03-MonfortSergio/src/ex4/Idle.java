package ex4;

public class Idle extends State {

	public Idle(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {
		System.out.println("empiezo y configuro idle");

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		System.out.println("me quedo quieto");
	}

	@Override
	public void exit() {
		
		System.out.println("acabo y desconfiguro idle");

	}

}
