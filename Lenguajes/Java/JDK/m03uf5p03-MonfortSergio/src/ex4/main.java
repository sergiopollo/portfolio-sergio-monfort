package ex4;

public class main {

	public static void main(String[] args) {
		
		StateMachine sm = new StateMachine();
		
		Pegar p = new Pegar("punyetazo");
		
		Idle i = new Idle("idle");
		
		sm.addState(p);

		sm.addState(p);

		sm.addState(i);
		
		System.out.println(sm.getStates());
		
		sm.setState(Pegar.class);

		sm.update();
		
		sm.setState(Idle.class);

		sm.update();
		
		System.out.println("////////////////stack state machine////////////////");
		
		StackStateMachine ssm = new StackStateMachine();
		
		ssm.addState(p);
		ssm.addState(i);
		
		ssm.pushState(Pegar.class);
		ssm.update();
		ssm.popState();
		
		ssm.pushState(Idle.class);
		ssm.pushState(Pegar.class);
		
		ssm.update();
		
		ssm.popState();
		
		ssm.update();
		
		ssm.popState();
	}

}
