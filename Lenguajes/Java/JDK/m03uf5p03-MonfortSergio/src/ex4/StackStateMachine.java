package ex4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class StackStateMachine {
	
	private HashSet<State> states = new HashSet<State>();
	private MyStack<State> stackStates = new MyStack<State>();
	
	public StackStateMachine() {
		super();
	}

	public MyStack<State> getStackStates() {
		return stackStates;
	}
	
	public HashSet<State> getStates() {
		return states;
	}
	
	public void addState(State state) {
		if(!states.contains(state)) {
			states.add(state);
		}
	}
	
	public void removeState(State state) {
		if(states.contains(state)) {
			states.add(state);
		}
	}
	
	
	public void pushState(Class<? extends State> state) {
		
		State estadoDeMiLista = searchState(state);
		
		if(estadoDeMiLista != null) {
			
			if(!stackStates.isEmpty()) {
				stackStates.stackTop().exit();
			}
			
			stackStates.Push(estadoDeMiLista);
			stackStates.stackTop().init();
		}
		
	}
	
	public void popState() {
		try {
			stackStates.stackTop().exit();
			stackStates.Pop();
			if(!stackStates.isEmpty()) {
				stackStates.stackTop().init();
			}
		} catch (StackVacioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void setState(Class<? extends State> state) {
		
		popState();
		
		State estadoDeMiLista = searchState(state);
		
		if(estadoDeMiLista != null) {
			stackStates.Push(estadoDeMiLista);
		}
	}
	
	public void update() {
		stackStates.stackTop().update();
	}
	
	private <T extends State> T searchState(Class<T> state) {
		for (State stateLista : states) {
			
			if(state.isInstance(stateLista)) {
				return state.cast(stateLista);
			}
		}
		return null;
	}
	
}
