package ex4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class StateMachine {
	
	private HashSet<State> states = new HashSet<State>();
	private State estadoActual;
	
	public StateMachine() {
		super();
	}

	public HashSet<State> getStates() {
		return states;
	}
	
	public void addState(State state) {
		if(!states.contains(state)) {
			states.add(state);
		}
	}
	
	public void removeState(State state) {
		if(states.contains(state)) {
			states.add(state);
		}
	}
	
	//public void setState(State state) {
	public void setState(Class<? extends State> state) {
		for(State estado : states) {
			if(state.isInstance(estado)) {
				
				if(estadoActual != null) {
					estadoActual.exit();
				}
				estadoActual = estado;
				estadoActual.init();
				System.out.println("state successfully changed to: "+estado);
				return;
			}
		}
	}
	
	public void update() {
		estadoActual.update();
	}
	
}
