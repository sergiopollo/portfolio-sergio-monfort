package ex4;

import java.util.ArrayList;
import java.util.Iterator;

public class MyStack<T> implements Iterable<T> {

	
	private ArrayList<T> lista = new ArrayList<T>();
	
	//constructor
	public MyStack() {
		
		lista = new ArrayList<T>();
		
	}

	public T Pop() throws StackVacioException{
		
		//si el stack esta vacio que lanze una excepcion
		if(lista.isEmpty()) {
			throw new StackVacioException();
		}else {
			
			T cosaABorrar = lista.get(lista.size()-1);
			
			lista.remove(lista.get(lista.size()-1));
			
			return cosaABorrar;
		}
	
	}
	
	public void Push(T element) {
	
		lista.add(element);

	}
	/*
	public ArrayList<T> getStack() {
		
		return lista;

	}
	*/
	
	public boolean isEmpty() {
		
		return lista.isEmpty();

	}
	
	
	@Override
	public String toString() {
		return ""+lista;
	}

	public T stackTop() {
		if(lista.isEmpty()) {
			return null;
		}else {
			return lista.get(lista.size()-1);
		}
	}
	
	private class MyStackIterator<T> implements Iterator<T>{

		int posicion = lista.size();
		
		@Override
		public boolean hasNext() {
			//compruebo si la lista del stack tiene un elemento anterior
			if(posicion > 0) {
				return true;
			}
			return false;
		}

		@Override
		public T next() {
			//cojo el anterior elemento de la lista del stack
			this.posicion--;
			
			T siguiente = (T) lista.get(posicion);
			
			return siguiente;
		}
		
	}
	
	@Override
	public Iterator<T> iterator() {
		
		MyStackIterator<T> iterador = new MyStackIterator<>();
		return iterador;
	}
	
}
