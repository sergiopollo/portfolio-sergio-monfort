package ex2;

public abstract class Component {
	
	public String name;

	public Component(String name) {
		super();
		this.name = name;
	}

	public Component() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public abstract void update();
	
}
