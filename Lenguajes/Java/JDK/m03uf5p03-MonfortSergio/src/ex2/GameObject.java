package ex2;

import java.util.ArrayList;

public class GameObject {
	
	private ArrayList<Component> components = new ArrayList<Component>();

	public GameObject() {
		super();
	}

	public ArrayList<Component> getComponents() {
		return components;
	}
	
	
	public void addComponent(Component component) {
		
		if(!components.contains(component)) {
			components.add(component);
		}else {
			System.out.println("ese componente exacto ya esta en la lista, a�ade otro del mismo tipo pero no el mismo");
		}
		
	}
	
	public void removeComponent(Component component) {
		
		if(components.contains(component)) {
			components.remove(component);
		}	
		
	}
	
	public <T extends Component> boolean hasComponent(Class<T> classe){
		
		for (Component component : components) {
			
			if(classe.isInstance(component)) {
				return true;
			}
			
		}
		return false;
		
	}
	
	//aqui se accede a la variable classe como si fuese un tipo concreto
	public <T extends Component> T getComponent(Class<T> classe){
		
		for (Component component : components) {
			
			//como instanceof pero al reves porque si no no deja
			if(classe.isInstance(component)) {
				//como un casteo pero al reves que si no no deja
				return classe.cast(component);
			}
		}
		return null;
		
	}
	
	public void update() {
		
		for (Component component : components) {
			
			component.update();
			
		}
		
	}
	
}
