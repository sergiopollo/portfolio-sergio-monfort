package ex1;

public class main {

	public static void main(String[] args) {
		
		Spell hechizoAgua = new Spell("salpicadura");
		
		hechizoAgua.addElement(new Water());
		hechizoAgua.addElement(new Viento());
		
		hechizoAgua.cast();

	}

}
