package ex1;

import java.util.ArrayList;

public class Spell {
	
	private String nombre;
	private ArrayList<Element> listaElementos = new ArrayList<Element>();
	
	public Spell(String nombre) {
		this.nombre = nombre;
	}

	public void addElement(Element el) {
	
		if(!listaElementos.contains(el)) {
			listaElementos.add(el);
		}	
		
	}
	
	public void removeElement(Element el) {
		
		if(listaElementos.contains(el)) {
			listaElementos.remove(el);
		}	
		
	}
	
	public void cast() {
		
		System.out.println("Casteando el hechizo " + nombre);
		
		for (Element element : listaElementos) {
			element.cast();
		}
		
	}
	
	
}
