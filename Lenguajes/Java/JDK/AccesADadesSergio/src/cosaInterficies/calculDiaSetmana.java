package cosaInterficies;

public class calculDiaSetmana {
	
	// La congru�ncia de Zeller �s un algoritme per calcular el dia de la 
	// setmana de qualsevol data del calendari.
	
	public static void main(String[] args) {
		
		int dia = buscar_dia(3,2,0);
		   
		System.out.println(dia);
		
	    String diaTexto = "";
	    
	    switch (dia) {
		case 2:
			diaTexto="Monday";
			break;
		case 3:
			diaTexto="Tuesday";
			break;
		case 4:
			diaTexto="Wednesday";
			break;
		case 5:
			diaTexto="Thursday";
			break;
		case 6:
			diaTexto="Friday";
			break;
		case 0:
			diaTexto="Saturday";
			break;
		case 1:
			diaTexto="Sunday";
			break;
		default:
			diaTexto="error: fecha no v�lida";
			break;
		}
	    
	    System.out.println(diaTexto);
		
	}
	
	public static int buscar_dia(int dia, int mes, int any) {
		
		boolean error = false;
		
		if(dia < 1 || dia > 31) {
			error = true;
		}
		if(mes < 1 || mes > 12) {
			error = true;
		}
		if(any < 0) {
			error = true;
		}
		
		if(!error) {
			if (mes == 1) {
				 mes = 13;
				 	any--;
			    }
			    if (mes == 2) {
			    	mes = 14;
			        any--;
			    }
			    int q = dia;
			    int m = mes;
			    int k = any % 100;
			    int j = any / 100;
			    int h
			        = q + 13 * (m + 1) / 5 + k + k / 4 +
			                              j / 4 + 5 * j;
			    h = h % 7;
			   
			    return h;
		}else {
			return -1;
		}
	}

}
