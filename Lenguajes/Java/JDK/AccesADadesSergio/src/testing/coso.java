package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import cosaInterficies.calculDiaSetmana;

class coso {

	@Test
	
	void buscar_dia() {
		assertEquals(4, cosaInterficies.calculDiaSetmana.buscar_dia(18, 8, 1999));
		assertEquals(4, cosaInterficies.calculDiaSetmana.buscar_dia(31, 2, 2021));
		assertEquals(-1, cosaInterficies.calculDiaSetmana.buscar_dia(-669, -56, 420));
		//assertEquals("error", cosaInterficies.calculDiaSetmana.buscar_dia("hola buenas", 8, 1999));
		assertEquals(-1, cosaInterficies.calculDiaSetmana.buscar_dia(99999999, 9999999, 99999999));
		//assertEquals("error", cosaInterficies.calculDiaSetmana.buscar_dia(18.546, 8, 1999));
	}

}
