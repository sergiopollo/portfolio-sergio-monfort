package GDiccionaris;

import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Resolucio de l'exercici Desierto del Sinai
 * 17/12/20
 * @author IES SABADELL
 *
 */
public class DesiertoSinai {
	
	/**
	 * En Battlefield, els usuaris poden seleccionar el mapa que volen despr�s de cada partida. El mapa que sol guanyar es Desierto del Sina�, el m�s divertit i popular dels mapes. El preferit de tothom.
Fes un sistema de votacions per als mapes.
L�usuari introduira noms de mapes fins a posar �xxx� com a nom de mapa
Crea un Map amb tota aquesta informaci� (1p)
Per cada cop que surti un mapa, suma-li 1 als vots que t�. Si es el primer cop afegeix-lo al Map (1p)
Torna el nom del mapa m�s votat (1p)

Consell: Aquest exercici es pot fer de moltes formes diferents. Si la teva forma passa per rec�rrer el set de claus, recorda que un set nom�s es pot rec�rrer amb un enhanced for. Recorda que les claus es poden treure com a collection, i per tant se�ls hi pot aplicar els m�todes de collection

	 */
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		HashMap<String,Integer> vots = new HashMap<String,Integer>();
		while(true) {
			
			String mapa = sc.nextLine();
			if(mapa.equals("xxx")) {
				break;
			}
			
			/* forma llarga
			//si ja tenim el mapa a la llista de vots
			if(vots.containsKey(mapa)){
				//agafem quans vots te
				int valor = vots.get(mapa);
				//sumem un
				valor++;
				//guardem el nou valor de vots
				vots.put(mapa, valor);
			}else {
				//si no esta el mapa a la llista de vots
				//posem el mapa amb un vot
				vots.put(mapa,1);
			}
			
			*/
			//forma curta
			
			//el valor es el que ja hi havia. Si el mapa no esta a la llista es que te 0 vots
			int valor = vots.getOrDefault(mapa, 0);
			//sumes un vot
			valor++;
			//poses el mapa. recordeu que put actualitza si ja existeix i afegeix si no existeix, i ja ho fa automaticament
			vots.put(mapa, valor);
		}
		System.out.println(vots);
		
		int valorMax = 0;
		String clauMax = "";
		//un diccionari es recorre aix�
		for (String string : vots.keySet()) {
			if(vots.get(string)>valorMax) {
				valorMax = vots.get(string);
				clauMax = string;
			}
		}
	
		System.out.println(clauMax);
	}

}
