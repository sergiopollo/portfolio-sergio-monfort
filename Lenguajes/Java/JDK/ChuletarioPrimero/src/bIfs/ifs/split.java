package bIfs.ifs;

import java.util.Scanner;


/**
 * Exemple de com fer servir el split: 5/10/20
 * @author IES SABADELL
 *
 */
public class split {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String a = sc.nextLine();
		
		String[] split = a.split(":");
		
		int n1 = Integer.parseInt(split[0]);
		int n2 = Integer.parseInt(split[1]);
		
		System.out.println(split[0]+" "+split[1]);
		
	}

}
