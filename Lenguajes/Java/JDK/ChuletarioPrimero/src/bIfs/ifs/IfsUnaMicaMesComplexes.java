package bIfs.ifs;

/**
 * ifs amb ands i ors  25/9/20
 * @author IES SABADELL
 *
 */
public class IfsUnaMicaMesComplexes {
	
	public static void main(String[] args) {
		
		
		int a = 3;
		int b = 2;
		int c = 5;
		//dos iguals. Hem de mirar 3 coses
		//si a es igual a b
		//si b es igual a c
		//si a es igual a c
		if(a == b || b == c || a == c) {
			System.out.println("SI");
		}else {
			System.out.println("NO");
		}
		
		//si els tres son iguals: And
		if( a == b && b == c) {
			System.out.println("SI");
		}else {
			System.out.println("NO");
		}
		
		
	}

}
