package bIfs.ifs;
import java.util.Scanner;


/**
 * Exemples d'exercicis d'ifs: Mitges
 * 2/10/20
 * @author IES SABADELL
 *
 */
public class exemplesIF {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		
		
		
		//mitja de 3: (a+b+c)/3
		
		int resultat = (a+b+c)/3;
		
		if(resultat >= 5) {
			System.out.println("Mitja Simple: Aprovat");
		}else {
			System.out.println("Mitja Simple: Susp�s");
		}
		
		
		//si en qualsevol examen treus menys d'un 4, et queda suspesa la UF.
		
		/*
		if(a<4 || b<4 || c<4 || resultat < 5) {
			System.out.println("SUSPES");
		}else {
			System.out.println("APROVAT");
		}
		*/
		
		/*
		if(a>=4 && b>= 4 && c >=4 && resultat>=5) {
			System.out.println("APROVAT");
		}else {
			System.out.println("SUSPES");
		}
		*/
		
		if(a<4 || b<4 || c<4) {
			System.out.println("SUSPES");
		}else {
			if(resultat<5) {
				System.out.println("SUSPES");
			}else {
				System.out.println("APROVAT");
			}
		}
		
		
		
		
		
		
		
		
	}

}
