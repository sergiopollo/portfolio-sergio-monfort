package bIfs.ifs;

/**
 * diferencies entre ifs i if/elseif 25/9/20
 * @author IES SABADELL
 *
 */
public class ifsvselseif {
	
	public static void main(String[] args) {
		
		
		int a = 5;
		int b = 9;
		
		//si es compleix l'if mai anira a l'else if encara que tambe es compleixi
		if(a<10) {
			System.out.println("A");
		}else if (b < 10){
			System.out.println("B");
		}
		
		//dos ifs independents, si les dues condicions es compleixen s'executen els dos
		if(a<10) {
			System.out.println("C");
		}if (b < 10){
			System.out.println("D");
		}
		
		
		
		//exemple super mario
		
		//WASD
		String tecla = "a";
		
		if(tecla.equals("a")) {
			System.out.println("ESQUERRA");
		}else if(tecla.equals("d")) {
			System.out.println("DRETA");
		}
		if(tecla.equals("w")) {
			System.out.println("SALTA");
		}
		//decisio de consens
		if(tecla.equals("d")) {
			System.out.println("AJUP");
		}
		
		
		
		
		
	}

}
