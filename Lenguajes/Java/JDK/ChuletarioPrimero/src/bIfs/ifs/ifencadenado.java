package bIfs.ifs;
import java.util.Scanner;

/**
 * Exercici ifs encadenats 
 * 2/10/20
 * @author IES SABADELL
 *
 */
public class ifencadenado {
	
	public static void main(String[] args) {
		
		/*
		 * RBG
		 * 
		 * Red  Blue  Green
		 * true false false = Red
		 * true true  false = Magenta
		 * true false true  = Yellow
		 * 
		 *  
		 * RB
		 *
		 */
		
		Scanner sc = new Scanner(System.in);
		
		boolean red = sc.nextBoolean();
		boolean blue = sc.nextBoolean();
		/* forma tradicional - con ands
		if(red == true && blue == true) {
			System.out.println("Magenta");
		}
		else if(red == false && blue == true) {
			System.out.println("Blue");
		}else if(red == true && blue == false) {
			System.out.println("Red");
		}else if(red == false && blue == false) {
			System.out.println("Black");
		}
		*/
		
		if(red == true) {
			//estem DINTRE DE L'IF
			//TOT EL QUE PASSA AQUI PASSA AMB RED SENT TRUE
			if(blue == true) {
				//aqui red es true i blue es true
				System.out.println("Magenta");
			}else {
				//red es true i blue es false
				System.out.println("Red");
			}
		}else {
			//tot el que passa aqui passa amb red sent fals
			if(blue == true) {
				//aqui red es false i blue es true
				System.out.println("Blue");
			}else {
				//aqui red es false i blue es false
				System.out.println("Black");
			}
			
		}
		
		
		
		
	}

}
