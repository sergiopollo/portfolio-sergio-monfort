package bIfs.ifs;

import java.util.Scanner;

/**
 * Exercici resolt: Poker de Daus a Temeria 5/10/20
 * @author IES SABADELL
 *
 */
public class pokerDaus {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		int d = sc.nextInt();
		
		if(a==b&&b==c&&c==d) {
			System.out.println("POKER");
		}else if(a==b&&a==c || a==b&&a==d || a==c&&a==d || b==c&&b==d) {
			System.out.println("TRIO");
		}else if(a==b||a==c||a==d||b==c||b==d||c==d) {
			System.out.println("PARELLA");
		}else {
			System.out.println("RES");
		}
		
		
		
	}

}
