package bIfs.ifs;
import java.util.Scanner;

/**
 * Introducci� a switch case 25/9/20
 * @author IES SABADELL
 *
 */
public class switchcase {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String cosaImportant = sc.nextLine();
		
		//switch per una variable
		switch(cosaImportant) {
		//cada cas es si la variable es aixo
		case "Programaci�":
			System.out.println("Marc, la programaci� NO es tan important.");
			//com que no hi ha break seguira cap abaix fins que hi hagi un break. Escriur� per tant, Gryffindor
		case "Coratge":
			//pots posar tants casos com vulguis que dugin al mateix lloc
		case "Valentia":
		case "Bravura o algo as� no se":
			System.out.println("Gryffindor");
			//quan acabem un case s'ha de posar break
			break;
		case "Coneixement":
			System.out.println("Ravenclaw");
			break;
		case "Ambici�":
			System.out.println("Slytherin");
			break;
		//default: Qualsevol altre cas
		default:
			System.out.println("Hufflepuff");
			//com que es l'ultim, no cal posarli un break
		
		
		}
		
		
	}

}
