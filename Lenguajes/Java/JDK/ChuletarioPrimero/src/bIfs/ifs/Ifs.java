package bIfs.ifs;

public class Ifs {
	/**
	 * intro ifs 25/9/20
	 * @param args
	 */
	public static void main(String[] args) {
		
		//el if es una estructura molt simple que consisteix en
		/*
		 * if(condicio HA DE SER O ACABAR SENT UN BOOLEAN){  <--- no us deixeu la clau
		 * 	   codi que s'executa si la condici� �s true.
		 * 
		 * 
		 * }else{  //opcional!
		 *     codi que s'executa si la condici� es false
		 * 
		 * 
		 * }
		 * 
		 * 
		 * 
		 */
		
		if(true) {
			System.out.println("Aixo s'imprimir� si true es true.");
		}
		
		
		/*
		 *Comparadors
		 * == comparar si son iguals
		 * > major que
		 * >= major o igual que
		 * < menor que
		 * <= menor o igual que
		 * != diferent
		 * 
		 */
		
		
		int a = -4;
		int b = 5;
		//si la condicio es compleix
		if(a>b) {
			//imprimeix si
			System.out.println("SI");
		//si no es compleix
		}else {
			//imprimeix no
			System.out.println("NO");
		}
		//aixo esta fora de l'if, s'imprimeix sempre
		System.out.println("Aixo va despres de l'if");
		
		
		
		
		
		
	}

}
