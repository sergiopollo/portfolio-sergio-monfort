package bIfs.repasifs;

import java.util.Scanner;

public class ifelse {
	
	public static void main(String[] args) {
		
		
		Scanner gatito = new Scanner(System.in);
		
		
		
		//metodes importants del scanner
		
		//nextline llegeix una linea de text (es a dir, fins que apretes enter) i te la torna com una string.
		//String linea = sc.nextLine();
		
		//next  llegeix una paraula (es a dir, fins que troba un espai o un enter, o un delimitador) i te la torna com una string
		//String paraula = sc.next();
		
		//nextInt llegeix un Enter (es a dir un numero), fins que troba un espai o un enter, o un delimitador) i te la torna com un int
		//tamnbe hi ha nextDouble, nextBoolean, 
		
		int a = gatito.nextInt();
		int b = gatito.nextInt();
		
		//else. El else s'executa SI I NOMES SI no es compleix la condicio de l'if abans
		//per tant significa que l'else depen de l'if. Pots posar un if sense else, pro mai un else sense if.
		
		//aixo esta malament perque si poses 5 i 5, et torna B es mes gran
		/*
		if(a>b) {
			System.out.println("A ES MES GRAN");
		}else {
			System.out.println("B ES MES GRAN");
		}
		*/
		
		//else if. else if es quan vols posar varies condicions. Aleshores significa que de totes nomes s'executara una.
		if(a>b) {
			System.out.println("A ES MES GRAN");
		}else if(b>a){
			System.out.println("B ES MES GRAN");
		//aquest else nomes s'activa si no es compleix ni l'if ni l'else if.
		}else {
			System.out.println("SON IGUALS");
		}
		
		
		
		
		
		
		
		
		
		
	}

}
