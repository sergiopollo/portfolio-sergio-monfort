package bIfs.repasifs;

public class IfMovimentVideojoc {
	
	
	public static void main(String[] args) {
		
		
		//ANEM A FER UN PERSONATGE DE PLATAFORMES QUE ES MOU
		
		//saltar
		//correr cap a l'esquerra
		//correr cap a la dreta
		
		String tecla = "w";
		
		//WASD
		
		//per comparar strings s'ha de comprar amb equals. No pots comparar strings amb == en Java. en C# si. Perque C# ES MOLT MILLOR QUE JAVA.
		//aqui vols fer if/else if perque no vols anar a l'esquerra i a ladreta al mateix temps
		if(tecla.equals("a")) {
			System.out.println("ANAR ESQUERRA");
		}else if(tecla.equals("d")) {
			System.out.println("ANAR A LA DRETA");
		}
		
		//aqui vols fer if perque si que es possible que vulguis anar a l'esquerra i saltar al mateix temps.
		if(tecla.equals("w")) {
			System.out.println("SALTA");
		}
		
		
		
		
		
		
	}

}
