package bIfs.repasifs;

import java.util.Scanner;

public class Diferenciaentreifielseif {
	
	public static void main(String[] args) {
		
		
		Scanner gatito = new Scanner(System.in);
		
		int edat = gatito.nextInt();
		
		
		
		//si tu tens un bloc de if/else if/else if/else if  NOMES UN ES COMPLIRA. I A LA QUE ES COMPLEIX UN, ES DESACTIVEN TOTS ELS DEMES
		
		//HOLA HE FET UN CANVI
		
		
		if(edat < 12) {
			System.out.println("VAS A PRIMARIA");
		}else if(edat < 17) {
			System.out.println("VAS A LA ESO");
		}else if(edat < 19) {
			System.out.println("VAS A BATXI / GM");
		}else {
			System.out.println("VAS A GS/UNI");
		}
		
		
		//si tu tens ifs es poden complir varios al mateix temps
		
		if(edat < 12) {
			System.out.println("VAS A PRIMARIA");
		}
		
		
		if(edat < 17) {
			System.out.println("VAS A LA ESO");
		}
		
		
		if(edat < 19) {
			System.out.println("VAS A BATXI / GM");
		}else {
			System.out.println("VAS A GS/UNI");
		}
		
		
		
		
	}

}
