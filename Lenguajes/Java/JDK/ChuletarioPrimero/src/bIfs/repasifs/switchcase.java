package bIfs.repasifs;

import java.util.Scanner;

public class switchcase {

	public static void main(String[] args) {

		/*
		 * 1 - OR 2 - PLATA 3 - BRONZE 4-8 - DIPLOMA OLIMPIC resta - RES.
		 * 
		 */

		Scanner sc = new Scanner(System.in);

		int posicio = sc.nextInt();

		switch (posicio) {

		//case amb el cas de si la variable es aquest valor
		case 1:
			//codi que vols fer
			System.out.println("OR");
			//acabes sempre amb un break;
			break;
		case 2:
			System.out.println("PLATA");
			break;
		case 3:
			System.out.println("BRONZE");
			break;
		//si poses varios case junts, es fa el mateix codi si es compleix quaslevol d'ells
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
			System.out.println("DIPLOMA OLIMPIC");
			break;
		//default es qualsevol altre valor que no formi part de cap case
		default:
			System.out.println("RES");

		}

	}

}
