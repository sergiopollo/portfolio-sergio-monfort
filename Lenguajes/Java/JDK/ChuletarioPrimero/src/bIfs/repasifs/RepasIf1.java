package bIfs.repasifs;

public class RepasIf1 {

	public static void main(String[] args) {
		
		int a = 4;
		int b = 5;
		
		//if( condicio ) {
		//  codi que s'executa si es compleix la condicio
		//  mes codi.
		// mes codi. Tantes linies com vulguis
		// }
		
		// el codi que posis mes enlla de la clau no té a veure amb l'if i s'executara sempre
		
		//les igualtats comparatives son amb == .
		
		if(a==b) {
			System.out.println("A ES IGUAL A B");
		}
		
		if(a<b) {
			System.out.println("A ES MES PETIT QUE B");
		}
		
		if(a>b) {
			System.out.println("A ES MES GRAN QUE B");
		}
		
		//aquest hola cau fora dels ifs i per tant s'imprimira sempre
		System.out.println("HOLA");
		
		
	}

}
