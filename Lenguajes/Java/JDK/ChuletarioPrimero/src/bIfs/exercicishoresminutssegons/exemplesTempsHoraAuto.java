package bIfs.exercicishoresminutssegons;
import java.util.Date;
import java.util.Scanner;

/**
 * Exemple de passar segons a hores, fent servir una classe Date per agafar l'hora actual
 * 2/10/2020
 * @author marc
 *
 */
public class exemplesTempsHoraAuto {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		//la classe date et permet representar dades
		//un new date et dona el temps actual
		Date d = new Date();
		//90061
		//el mes et torna un numero del 0 al 11, amb 0 sent gener.
		//com que nosaltres fem servir els mesos amb gener sent 1, sumem 1
		int mes = d.getMonth()+1;
		//ull, getDay et torna el dia de la setmana
		int dia = d.getDate();
		int h = d.getHours();
		int m = d.getMinutes();
		int s = d.getSeconds();
		
		System.out.println("Es el "+dia+" del mes "+mes+" a les "+h+":"+m+":"+s);

		if (mes < 10) {
			System.out.println("No ha sortit");
		} else if (mes > 10) {
			System.out.println("Si ha sortit");
		} else if (mes == 10) {
			// el mes es octubre, s'ha de consultar el dia
			if (dia < 27) {
				System.out.println("No ha sortit");
			} else if (dia > 27) {
				System.out.println("Si ha sortit");
			} else if (dia == 27) {
				// el dia es 27, s'ha de consultar les hores
				if (h < 12) {
					System.out.println("No ha sortit");
				} else if (h > 12) {
					System.out.println("Si ha sortit");
				} else if (h == 12) {
					if (m < 30) {
						System.out.println("No ha sortit");
					} else {
						// no importa si es 30 o mes gran que 30 perque surt just a i 30.
						System.out.println("Si ha sortit");
					}
				}
			}
		}

	}
}
