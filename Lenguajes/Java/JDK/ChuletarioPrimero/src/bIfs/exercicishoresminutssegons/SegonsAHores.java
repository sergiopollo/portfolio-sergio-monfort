package bIfs.exercicishoresminutssegons;
import java.util.Scanner;

/**
 * Exemple de passar segons a hores
 * 2/10/2020
 * @author marc
 *
 */
public class SegonsAHores {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int segons = sc.nextInt();
		int minuts = segons/60;
		segons = segons%60;
		
		System.out.println(minuts+" minuts i "+segons+" segons");
		
		

	}

}
