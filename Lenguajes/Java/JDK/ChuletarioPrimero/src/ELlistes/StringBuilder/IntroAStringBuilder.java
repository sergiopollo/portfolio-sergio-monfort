package ELlistes.StringBuilder;

public class IntroAStringBuilder {
	
	public static void main(String[] args) {
		
		String s = "sadsad";
		System.out.println(s);
		
		//declarar un StringBuilder
		StringBuilder sb = new StringBuilder("Hola que tal, ");
		//imprimir un StringBuilder
		System.out.println(sb.toString());
		
		//metodes especials del StringBuilder
		//append: Afegeix per el final. Funciona exactament igual que String + algo
		sb.append("supremacia racial");
		System.out.println("despres del apend queda "+sb);
		//insert afegeix epr la posici� indicada.
		sb.insert(2, "albert");
		System.out.println("despres del insert queda "+sb);
		//setCharAt: Modifica el caracter a la posici� indicada
		sb.setCharAt(5, '*');
		System.out.println("despres del setcharat queda "+sb);
		//delete. Esborra de la posici� inicial a la final. Inclou la inicial, no inclou la final
		sb.delete(2, 8);
		System.out.println("despres del delete queda "+sb);
		//deletecharAt. Esborra el car�cter a la posici�.
		sb.deleteCharAt(0);
		System.out.println("despres del deletecharat queda "+sb);
		//reverse: Posa la String al reves.
		sb.reverse();
		System.out.println("despres de posarla del reves queda: "+sb);
	}

}
