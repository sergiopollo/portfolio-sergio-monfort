package ELlistes.Desde0;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MasCosasConListas {
	
	public static void main(String[] args) {
		
		//declarar llista amb valors inicialitzats.
		ArrayList<String> llista = new ArrayList<String>(Arrays.asList("farola", "digues?", "formatge", "patata", "peluca", "que pasa", "ehmmmm"));
		
		
		System.out.println(llista);
		
		Collections.sort(llista);
		
		System.out.println(llista);
		
		Collections.reverse(llista);
		
		System.out.println(llista);
		
		Collections.shuffle(llista);
		System.out.println(llista);
		
		
		llista.remove("peluca");
		System.out.println(llista);
		
		//borrar la lista
		llista.clear();
		
		
		
		
		
	}

}
