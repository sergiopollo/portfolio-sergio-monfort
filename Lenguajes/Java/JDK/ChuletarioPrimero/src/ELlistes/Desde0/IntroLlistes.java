package ELlistes.Desde0;

import java.util.ArrayList;

public class IntroLlistes {
	
	public static void main(String[] args) {
		
		//les llistes sempre es declaren igual
		ArrayList<Integer> llistaInt = new ArrayList<>();
		ArrayList<String> llistaString = new ArrayList<>();
		
		//tu no declares una llista amb tamany fixe. la declares buida
		//el tamany d'una llista es variable
		
		System.out.println("el tamany de la llista es "+llistaInt.size());
		
		
		llistaInt.add(5);
		llistaInt.add(98);
		llistaInt.add(7);
		llistaInt.add(24);
		llistaInt.add(8);
		llistaInt.add(21);
		
		System.out.println(llistaInt);
		
		//obtenir el que hi ha en una posicio
		int loquehayenlaposicion2 = llistaInt.get(2);
		
		//canviar el valor d'una posicio
		//set(posicionQueQuieresCambiar, NuevoValor)
		llistaInt.set(2, 3);
		
		System.out.println(llistaInt);
		
		int buscar = 24;
		//contains. Te dice si encuentra un valor en la posicion
		boolean encontrado = llistaInt.contains(24);
		
		if(encontrado==true) {
			System.out.println("SI");
		}else {
			System.out.println("FALSE");
		}
		
		//indexOf. Te dice la posicion en la que esta un valor.
		int posicion = llistaInt.indexOf(24);
		System.out.println("El 24 se encuentra en la posicion "+posicion);
		
		//quiero borrar lo que hay en la posicion 3.
		llistaInt.remove(3);
		System.out.println(llistaInt);
		
		
		llistaInt.add(3, 69);
		
		System.out.println(llistaInt);
		
		
		
		
	}

}
