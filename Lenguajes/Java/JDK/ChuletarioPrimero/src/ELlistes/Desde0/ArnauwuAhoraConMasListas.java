package ELlistes.Desde0;

import java.util.ArrayList;
import java.util.Scanner;

public class ArnauwuAhoraConMasListas {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		int ncasos = sc.nextInt();
		for (int cas = 0; cas < ncasos; cas++) {
			//mirem quants animals hi ha
			int animals = sc.nextInt();
			///famos bug
			sc.nextLine();
			ArrayList<String> llista = new ArrayList<>();
			//per tots els animals que hi ha
			//es animals-1 perque ho diu l'enunciat
			for (int i = 0; i < animals-1; i++) {
				//llegim l'animal
				String animal = sc.nextLine();
				//el posem a la llista
				llista.add(animal);
			}
			//llegim l'ultim animal, el que busquem
			String animalBuscat = sc.nextLine();
			//si la lllista cont� l'animal...
			if(llista.contains(animalBuscat)) {
				System.out.println("SI");
			}else {
				System.out.println("NO");
			}
			
			
		}
		
		
	}

}
