package ELlistes.Sets;

import java.util.Iterator;
import java.util.TreeSet;

public class RecorrerSets {
	
	public static void main(String[] args) {
		
		TreeSet<String> treeSetString = new TreeSet<String>();
		
		treeSetString.add("Berenjena");
		treeSetString.add("Que pasa");
		treeSetString.add("Merrorista");
		treeSetString.add("Markus");
		treeSetString.add("uwu");
		treeSetString.add("patata");
		treeSetString.add("uwu");
		
		System.out.println(treeSetString);
		
		//foreach:
		//per totes els strings a treeSetString
		for (String string : treeSetString) {
			System.out.println(string);
		}
		
		//un set nomes es pot recorrer amb un foreach
		
		
		
	}

}
