package ELlistes.Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

/**
 * Introduccio als Sets
 * 20/11/20
 * @author IES SABADELL
 *
 */
public class IntroASets {
	
	public static void main(String[] args) {
		
		
		//COLLECTIONS
		
		//Llistes <List> son un grup molt gran
		//ArrayList. List. LinkedList, Deque etc.
		
		//Conjunts <Set> son un grup
		//HashSet i TreeSet
		
		
		//Difer�ncies entre Llistes i Sets
		//Diferencia 1: Un Set NO POT TENIR ELEMENTS REPETITS.
		//Diferencia 2: Un Set SEMPRE AGAFA UN ORDRE ESPEC�FIC.
		
		
		ArrayList<Integer> llistaInts = new ArrayList<Integer>();
		HashSet<Integer> setInts = new HashSet<Integer>();
		boolean afegit;
		
		llistaInts.add(4);
		llistaInts.add(5);
		llistaInts.add(6);
		
		setInts.add(4);
		setInts.add(5);
		afegit = setInts.add(6);
		
		System.out.println("La llista diu "+llistaInts);
		System.out.println("El set diu "+afegit+" "+setInts);
		
		//provem d'afegir un element repetit
		llistaInts.add(4);
		afegit = setInts.add(4);
		
		System.out.println("La llista diu "+llistaInts);
		System.out.println("El set diu "+afegit+" "+setInts);
		
		llistaInts.add(-91);
		afegit = setInts.add(-91);
		afegit = setInts.add(7);
		afegit = setInts.add(-7);
		
		System.out.println("La llista diu "+llistaInts);
		System.out.println("El set diu "+afegit+" "+setInts);
		
		//TREESET. ORDENA SEMPRE PER EL SEU ORDRE NATURAL.
		TreeSet<Integer> treeSetInts = new TreeSet<Integer>();
		
		treeSetInts.add(4);
		treeSetInts.add(5);
		treeSetInts.add(6);
		treeSetInts.add(4);
		afegit = treeSetInts.add(-91);
		afegit = treeSetInts.add(7);
		afegit = treeSetInts.add(-7);
		
		System.out.println("TreeSet diu "+treeSetInts);
		
		
		//LINKEDHASHSET. ORDENA SEMPRE PER ORDRE D'INTRODUCCIO
		LinkedHashSet<Integer> lhSetints = new LinkedHashSet<Integer>();
		
		lhSetints.add(4);
		lhSetints.add(5);
		lhSetints.add(6);
		lhSetints.add(4);
		afegit = lhSetints.add(-91);
		afegit = lhSetints.add(7);
		afegit = lhSetints.add(-7);
		
		System.out.println("Linked hash set diu "+lhSetints);
		
		
		
		
		
		
		
	}

}
