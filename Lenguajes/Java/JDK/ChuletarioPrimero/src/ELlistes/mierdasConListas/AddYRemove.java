package ELlistes.mierdasConListas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddYRemove {
	
	public static void main(String[] args) {
		
		
		int[] array = {1,2,3,4,5,6,7};
		
		//aqui esta la forma d'inicialitzar una llista en una sola linea
		ArrayList<Integer> llistaInt = new ArrayList<Integer>(Arrays.asList(324,52,7,-84,69,420,666,-34,42,1));
		ArrayList<String> llistaStr = new ArrayList<String>(Arrays.asList("azucar","darksouls","aaaasupremaciaracial","dormir","avena","soyunfurrouwu","esternocleidomastoideo"));
		System.out.println(llistaInt);
		System.out.println(llistaStr);
		
		//add, afegeix una cosa al final d'una llista
		//la comanda add canvia el tamany de la llista i per tant el size
		llistaInt.add(99);
		llistaStr.add("supercalifragilisticoespialidoso");
		System.out.println(llistaInt);
		System.out.println(llistaStr);
		
		//afegeix algo al principi (posici� 0)
		llistaStr.add(0, "hola");
		System.out.println(llistaStr);
		
		//elimina la posici� 0. Retorna el que has esborrat
		String borrat = llistaStr.remove(0);
		System.out.println("He esborrat "+borrat);
		System.out.println("Ara la llista es "+llistaStr);
		//elimina l'eleent anomenat aix� si existeix. Si l'esborra torna true
		llistaStr.remove("aaaasupremaciaracial");
		
		//si la llista es d'enters, posar el numero esborra la posici�
		llistaInt.remove(1);
		System.out.println(llistaInt);
		//si vull esborrar el numero 1 i no la posicio 1
		//creo un objecte de tipus Integer
		Integer numero1 = 1;
		llistaInt.remove(numero1);
		
		
		
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		llistaStr.add("uwu");
		
		System.out.println(llistaStr);
		
		//esborrar tots els elements de la llista
		boolean flag = true;
		//mentre segueixin havent elements
		while(flag==true) {
			//el remove torna true si ha esborrat algo, false si no.
			flag = llistaStr.remove("uwu");
		}
		
		
		
		
		
	}

}
