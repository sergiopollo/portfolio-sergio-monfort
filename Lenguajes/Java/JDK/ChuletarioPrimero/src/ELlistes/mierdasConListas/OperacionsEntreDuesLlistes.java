package ELlistes.mierdasConListas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Operacions entre 2 llistes
 * 24/11/20
 * @author marc
 *
 */
public class OperacionsEntreDuesLlistes {
	
	public static void main(String[] args) {
		
		//iniciar llista desde la declaraci�
		ArrayList<Integer> llistaInt = new ArrayList<Integer>(Arrays.asList(3,6,7,-8,6,4,6,-3,0,1));
		
		ArrayList<Integer> llistaInt2 = new ArrayList<Integer>();
		Random r = new Random();
		int n = 10;
		for (int i = 0; i < n; i++) {
			llistaInt2.add(r.nextInt(20)-10);
		}
		
		System.out.println(llistaInt);
		System.out.println(llistaInt2);
		
		
		
		//operacions en 2 llistes que interactuen entre elles
		
		//comprovar si dues llistes son iguals
		//equals. comprova si dues llistes son iguals. Dues llistes son iguals si tots els seus elements son iguals un a un (primer amb primer, seogn amb segon, etc)
		llistaInt.equals(llistaInt2);
		
		//copiar una llista. Creeu una nova i als parentesis del new li especifiques la llista que vols copiar
		ArrayList<Integer> llistaIntCopy = new ArrayList<Integer>(llistaInt);
		ArrayList<Integer> llistaIntCopy2 = new ArrayList<Integer>(llistaInt);

		
		//sumar dues llistes
		//addAll. Suma dues llistes. Appenda la llista del final a la primera llista
		llistaInt.addAll(llistaInt2);
		//compte, modifica la primera llista.
		System.out.println("llista 1 + llista 2 = "+llistaInt);
		
		
		
		//addAll tambien funciona de otra forma
		llistaInt.addAll(2, llistaInt2);
		//aqui poses un index. La llista es posa a partir de l'index.
		System.out.println(llistaInt);
		
		//removeAll. Treu tots els elements que estan a la llista que li passes
		//es podria dir que es llistaInt-llistaInt2;
		//borra tamb� tots els repetits encara que a llista1 surti el mateix numero varies vegades i a llista2 nom�s una
		llistaIntCopy.removeAll(llistaInt2);
		System.out.println("llista1 - llista2 "+llistaIntCopy);
		
		
		//retainAll. Fa una uni� de la llista
		//llista1 && llista2
		llistaIntCopy2.retainAll(llistaInt2);
		System.out.println("llista1 && llista2 "+llistaIntCopy2);


		
		
		
		
	}

}
