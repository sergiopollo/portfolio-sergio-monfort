package ELlistes.mierdasConListas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class CanviarLaLlistaSencera {
	
	public static void main(String[] args) {
		
		
		ArrayList<Integer> llistaInt = new ArrayList<Integer>(Arrays.asList(324,69,7,-84,69,420,666,-34,42,1));
		ArrayList<String> llistaStr = new ArrayList<String>(Arrays.asList("azucar","darksouls","aaaasupremaciaracial","dormir","avena","soyunfurrouwu","esternocleidomastoideo","oppais","onlyfans","Si"));
		System.out.println(llistaInt);
		System.out.println(llistaStr);
		Scanner sc = new Scanner(System.in);
		
		//replaceAll, reemplaša tot
		Collections.replaceAll(llistaInt, 69, 90);
		System.out.println(llistaInt);
		
		//reverse, posa la llista del reves
		Collections.reverse(llistaStr);
		System.out.println(llistaStr);
		
		//Shuffle torna un ordre aleatori
		Collections.shuffle(llistaStr);
		System.out.println(llistaStr);
		
		//rotate corre tots una posicio a la dreta (negatiu a l'esquerra)
		Collections.rotate(llistaStr, 1);
		System.out.println(llistaStr);
		
		//clear: esborra la llista
		llistaInt.clear();
		System.out.println(llistaInt);
		
		
	}

}
