package ELlistes.Repas;

import java.util.Set;

public class StringBuilders {
	
	public static void main(String[] args) {
		
		//declarar
		StringBuilder sb = new StringBuilder("hola que tal supremacia racial");
		
		//setCharat es com el charAt, pero amb set en comptes de get
		sb.setCharAt(5, 'k');
		System.out.println(sb);
		
		
		//reverse
		sb.reverse();
		System.out.println(sb);
		
		//torno a posar del dret
		sb.reverse();
		
		
		
		//delete: eliminarte un cacho de string. el primer l'inclou i el segon no
		sb.delete(6, 13);
		System.out.println(sb);
		
		//insert: afeig un cacho de String a la posicio indicada
		sb.insert(4, "Albert ");
		System.out.println(sb);
		
		
		
	}

}
