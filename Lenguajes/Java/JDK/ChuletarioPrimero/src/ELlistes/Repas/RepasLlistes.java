package ELlistes.Repas;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class RepasLlistes {
	
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		

		Random r = new Random();
		//crear una llista buida
		ArrayList<String> llista = new ArrayList<String>();
		
		for (int i = 0; i < 10; i++) {
			String s = "Kernel "+i;
			llista.add(s);
		}
		
		System.out.println(llista);
		
		//get retorna el valor si li dones la posicio
		System.out.println(llista.get(2));
		
		//set
		llista.set(2, "Albert");
		
		System.out.println(llista.get(2));
		
		//contains. retorna true si esta dintre
		boolean conte = llista.contains("Kernel 4");
		System.out.println(conte);
		
		//indexof retorna la posicio de la llista si li dones el valor
		//torna -1 si no esta dintre.
		int posicio = llista.indexOf("Albert");
		System.out.println(posicio);
		
		//remove per borrar. funciona per index o per objecte
		llista.remove(1);
		llista.remove("Kernel 7");
		System.out.println(llista);
		
		//add es pot insertar al mig per posicio
		llista.add(4,"Luis");
		System.out.println(llista);
		
		//torna el tamany. Important a l'hora de fer un bucle!
		llista.size();
		
		
		//afegir "A" al final de la paraula si comença per "K"
		for (int i = 0; i < llista.size(); i++) {
			//llista.get(i) torna una String. Per tant després pots posar un altre punt i fer una funcio de Strings
			if(llista.get(i).charAt(0)=='K') {
				//posar a la posicio i el que ja teniem + A
				llista.set(i, llista.get(i)+"A");
			}
		}
		
		
		
		
	}

}
