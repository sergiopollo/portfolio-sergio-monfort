package ELlistes.Repas;

import java.util.LinkedHashSet;
import java.util.TreeSet;

public class RepasSets {
	
	public static void main(String[] args) {
		
		
		
		//sets son llistes amb ordre determinat que no es repeteixin
		
		//treeset sempre ordena per ordre estandard (numeric o alfabetic)
		TreeSet<Integer> treeset = new TreeSet<Integer>();
		
		treeset.add(3);
		treeset.add(4);
		treeset.add(3);
		treeset.add(5);
		treeset.add(5);
		treeset.add(2);
		
		System.out.println(treeset);
		
		
		//linkedhashset sempre ordena per ordre d'introduccio
		LinkedHashSet<Integer> lhset = new LinkedHashSet<Integer>();
		
		lhset.add(3);
		lhset.add(4);
		lhset.add(3);
		lhset.add(5);
		lhset.add(5);
		lhset.add(2);
		
		System.out.println(lhset);
		
	}

}
