package ELlistes.Repas;

import java.util.Random;

public class RepasArrays {

	public static void main(String[] args) {

		Random r = new Random();

		// declarar array: tipus nom[] = new tipus[tamany]
		int array[] = new int[10];

		// for per omplir array
		for (int i = 0; i < array.length; i++) {
			array[i] = r.nextInt(10);
		}

		// imprimir array
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		// comparar amb un array
		int comptde5 = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == 5) {
				comptde5++;
			}
		}
		System.out.println("hi han " + comptde5 + " cincs");

		// imprimir array del reves (recorrer un array del reves)
		// array.length-1 - ultim element

		for (int i = array.length - 1; i >= 0; i--) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		int[] arraygirat = new int[10];

		// crear un array girat.
		for (int i = 0; i < arraygirat.length; i++) {
			arraygirat[i] = array[array.length - 1 - i];
		}

		// comparar 2 arrays
		boolean iguals = true;

		for (int i = 0; i < arraygirat.length; i++) {
			if (array[i] != arraygirat[i]) {
				iguals = false;
			}
		}
		System.out.println(iguals);

		// modificar un array
		// si el numero es senar sumarem 1
		for (int i = 0; i < array.length; i++) {
			// modul de 2 serveix per saber si es parell o senar
			if (array[i] % 2 == 1) {
				array[i]++;
			}
		}
		// imprimir array
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

	}

}
