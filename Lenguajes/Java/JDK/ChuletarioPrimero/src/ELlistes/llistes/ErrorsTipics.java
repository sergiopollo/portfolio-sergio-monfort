package ELlistes.llistes;

import java.util.ArrayList;
import java.util.Scanner;

public class ErrorsTipics {

	public static void main(String[] args) {
		

/*
		// Error tipic 1: Variables dintre o fora del bucle
		{

			// 1. Comptadors Acumuladors i Flags sempre es declaren FORA del bucle a on es
			// fan servir

			for (int i = 0; i < args.length; i++) {
				int comptador = 0;
				for (int j = 0; j < args.length; j++) {
					comptador++;
				}
			}
			
			//colocar numeros en un array;

			Scanner sc = new Scanner(System.in);
			int n = sc.nextInt();
			
			//jo no vull que a cada iteracio del bucle l'array es reinicii
			int[] array = new int[n];
			for (int i = 0; i < n; i++) {
				//jo vull que a cada iteracio del bucle l'array es reinicii?
				//int[] array = new int[n];
				int numero = sc.nextInt();
				numero++;
				array[i] = numero;
				
			}
			
			
		}
		
		
		//Error tipic 2: Bucle de casos
		
		//bucle de casos 1: El primer nombre indica quants caoss hi ha
		{
			Scanner sc = new Scanner(System.in);
			int ncasos = sc.nextInt();
			sc.nextLine(); //por si acaso luego teneis uqe leer strings
			for (int cas = 0; cas < ncasos; cas++) {
				//AQUI VA EL CASO INDIVIDUAL QUE HABREIS HECHO PREVIAMENTE Y YA HABEIS COMPROBADO QUE OS FUNCIONA 
				int arraySize = sc.nextInt();
				
				//el deixem dintre del bucle de casos eprque com que cada cas es diferent ens interessa que es reinicii l'array
				int[] array = new int[arraySize];
				//primer bucle para rellenar el array
				for (int i = 0; i < array.length; i++) {
					array[i] = sc.nextInt();
				}
				int numeroAContar = sc.nextInt();
				int contador = 0;
				//segundo bucle para contar el numero en el array que hemos rellenado
				for (int i = 0; i < array.length; i++) {
					if(array[i]==numeroAContar) {
						contador++;
					}
				}
				System.out.println(contador);
				
			}
		}
		
	*/	
		//Error tipic 2.b: Lo mismo pero con listas
		
				//bucle de casos 1: El primer nombre indica quants caoss hi ha
				{
					Scanner sc = new Scanner(System.in);
					int ncasos = sc.nextInt();
					sc.nextLine(); //por si acaso luego teneis uqe leer strings
					
					for (int cas = 0; cas < ncasos; cas++) {
						//AQUI VA EL CASO INDIVIDUAL QUE HABREIS HECHO PREVIAMENTE Y YA HABEIS COMPROBADO QUE OS FUNCIONA 
						int arraySize = sc.nextInt();
						//el deixem dintre del bucle de casos eprque com que cada cas es diferent ens interessa que es reinicii l'array
						ArrayList<Integer> lista = new ArrayList<Integer>();
						//primer bucle para rellenar el array
						for (int i = 0; i < arraySize; i++) {
							lista.add(sc.nextInt());
						}
						System.out.println(lista);
						int numeroAContar = sc.nextInt();
						int contador = 0;
						//segundo bucle para contar el numero en el array que hemos rellenado
						for (int i = 0; i < arraySize; i++) {
							if(lista.get(i)==numeroAContar) {
								contador++;
							}
						}
						System.out.println(contador);
						
					}
				}
				
				
				//bucle de casos 2: Llegeixes fins que llegeixes un -1
				{
					Scanner sc = new Scanner(System.in);
					//comences llegint la primera variable;
					int arraySize = sc.nextInt();

					do {
						//AQUI VA EL CASO INDIVIDUAL QUE HABREIS HECHO PREVIAMENTE Y YA HABEIS COMPROBADO QUE OS FUNCIONA 						//el deixem dintre del bucle de casos eprque com que cada cas es diferent ens interessa que es reinicii l'array
						ArrayList<Integer> lista = new ArrayList<Integer>();
						//primer bucle para rellenar el array
						for (int i = 0; i < arraySize; i++) {
							lista.add(sc.nextInt());
						}
						System.out.println(lista);
						int numeroAContar = sc.nextInt();
						int contador = 0;
						//segundo bucle para contar el numero en el array que hemos rellenado
						for (int i = 0; i < arraySize; i++) {
							if(lista.get(i)==numeroAContar) {
								contador++;
							}
						}
						System.out.println(contador);
						
						
						//tornes a llegir el seg�ent arraySize per mirar si es -1
						arraySize = sc.nextInt();
					//si es -1 surts.
					}while(arraySize!=-1);
				}

	}

}
