package ELlistes.llistes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class llistes {
	
	public static void main(String[] args) {
		
		
		int array[] = new int[5];
		
		//ArrayList<Tipus> nom = new ArrayList<>();
		ArrayList<Integer> llista = new ArrayList<>();
		
		//omplir un array vs omplir una llista
		Random r = new Random();
		
		for (int i = 0; i < array.length; i++) {
			array[i]=r.nextInt(10);
		}
		
		for (int i = 0; i < 5; i++) {
			int numeroRandom = r.nextInt(10);
			//afegeix un element al final
			llista.add(numeroRandom);
		}
		System.out.println("Array");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
		System.out.println("Llista");
		
		//el tamany de la llista es size, no length. No se per que
		for (int i = 0; i < llista.size(); i++) {
			System.out.print(llista.get(i)+" ");
		}
		System.out.println();
		System.out.println(llista);
		
		
		
		//volem saber si l'array cont� un 5;
		System.out.println("Array conte 5?");
		int n = 5;
		boolean flag = false;
		for (int i = 0; i < array.length; i++) {
			if(array[i]==n) {
				flag=true;
			}
		}
		System.out.println(flag);
		
		System.out.println("Llista conte 5?");
		boolean conte = llista.contains(5);
		System.out.println(conte);
		
		
		System.out.println("en quina posici� de l'array est� el 5");
		
		int posicio = -1;
		boolean trobat = false;
		n = 5;
		for (int i = 0; i < array.length; i++) {
			if(array[i]==5 && trobat == false) {
				posicio = i;
				trobat = true;
			}
		}
		System.out.println(posicio);
		System.out.println("en quina posici� de la llista est� el 5");
		int index = llista.indexOf(5);
		System.out.println(index);

		//comptar quants 5
		System.out.println("quants 5 hi ha en l'array");
		int comptador = 0;
		for (int i = 0; i < array.length; i++) {
			if(array[i]==5) {
				comptador++;
			}
		}
		System.out.println(comptador);
		System.out.println("quants 5 hi ha a la llista");
		int comptadorl = Collections.frequency(llista, 5);
		System.out.println(comptadorl);
		
		
		
		
		//sumar 1 a tots els elements
		for (int i = 0; i < array.length; i++) {
			int element = array[i];
			array[i] = element+1;
		}
		//imprimer array
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		
		//sumar 1 a tots els elemnts dela llista
		for (int i = 0; i < llista.size(); i++) {
			int element = llista.get(i);
			//modificar element
			llista.set(i, element+1);
		}
		System.out.println(llista);



		
		
		
		
		
	}

}
