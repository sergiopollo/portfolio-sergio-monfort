package ELlistes.llistes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class passarArrayALlista {
	
	public static void main(String[] args) {
		
		//declarar array d'inici
		//compte, si vols passarho a llista, l'array ha de ser d'integer, no de int. LLista MAI pot ser d'int
		Integer[] arrayInt = {32,543,43,6,8,654,7};
		String[] arrayString = {"asdas","hola","hueahs"};
		
		//declarar llista d'inic
		ArrayList<Integer> llistaIntInici = new ArrayList<>(Arrays.asList(1,24,5,7,87345,34234,7657654));
		ArrayList<String> llistaStringInici = new ArrayList<>(Arrays.asList("jskahds","jhjkh","asdas","oiuo"));
		
		//passar de array a llista
		ArrayList<Integer> llistaInt = new ArrayList<Integer>(Arrays.asList(arrayInt));
		ArrayList<String> llistaString = new ArrayList<String>(Arrays.asList(arrayString));
		
		System.out.println(llistaInt);
		System.out.println(llistaString);
		
		//passar de llista a Array
		//String[] arrayString2 = (String[]) llistaString.toArray();
		//Integer[] arrayInt2 = (Integer[]) llistaInt.toArray();
		
		//
		
		
	}

}
