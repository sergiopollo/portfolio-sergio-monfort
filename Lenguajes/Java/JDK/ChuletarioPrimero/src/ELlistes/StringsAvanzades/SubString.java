package ELlistes.StringsAvanzades;

public class SubString {
	
	public static void main(String[] args) {
		
		
		String s = "Albert Menja Allioli";
		
		//substring et crea strings m�s petites a partir de la String gran
		
		//String s2 = s.substring(posicioInicial,posicioFinal)
		String s2 = s.substring(0,5);
		//inclou el primer pero no l'ultim
		System.out.println("La substring de 0 a 5 es "+s2);
		//si nomes poses un numero es desde aquest numero fins al final
		String s3 = s.substring(10);
		System.out.println("La substring de 10 al final es "+s3);
		//si poses el mateix numero et torna una string buida
		String s4 = s.substring(3,3);
		System.out.println("La substring de 3 a 3 es "+s4);
		
		
		
	}

}
