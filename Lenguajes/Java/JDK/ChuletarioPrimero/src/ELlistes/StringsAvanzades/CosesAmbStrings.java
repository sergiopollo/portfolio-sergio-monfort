package ELlistes.StringsAvanzades;

import java.util.Scanner;

public class CosesAmbStrings {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//declarar String
		String s1 = "Holahola";
		String s2 = sc.nextLine();
		
		//recorrer String
		for (int i = 0; i < s2.length(); i++) {
			//recorre una string caracter a caracter
			//charAt et torna el caracter en la posicio i
			char c = s2.charAt(i);
			System.out.println(c);
		}
		
		//m�todes rapids. Que ja coneixem
		
		//contains. Et diu si la string cont� una substring que li passis
		boolean conte = s2.contains("Albert");
		System.out.println(conte);
		
		//equals. Et torna si les dues strings son iguals, == no funciona
		s2.equals("AlbertMenjaAllioli");
		
		//indexOf. et torna la posici� del primer caracter de la subString
		int index = s2.indexOf("Albert");
		System.out.println(index);
		
		//replace
		String stringAmbCanvis = s2.replace("Allioli", "Furros");
		System.out.println(stringAmbCanvis);
		
		//el replace es pot fer servir per borrar
		String furros = "Albert Furros Furros MenFurrosja Furros AllFurrosioli";
		String furrosAmbCanvis = furros.replace("Furros", "");
		System.out.println(furrosAmbCanvis);
		
		//startsWith i endsWith. Et diu si comen�a o acaba amb la substring
		boolean comenzar = furros.startsWith("Albert");
		
		//ToUpperCase i ToLowerCase  et passa la string a tot MAJUS o tot minus.
		String FURROSMAJ = furros.toUpperCase();
		System.out.println(FURROSMAJ);
		
	
		
		
		
		
		
	}

}
