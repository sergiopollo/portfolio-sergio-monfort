package ELlistes.StringsAvanzades;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class exempleSplit {
	
	//et passen una frase amb moltes paraules i has de dir quina es la m�s llarga i quina �s la m�s curta
	
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		//1 separar la frase en paraules individuals
		
		String frase = sc.nextLine();
		
		//parteixme per espai
		//fes un array de paraules. El separador sera l'espai. Es a dir, cada cop que et trobis un espai, agafes el que hi ha abans i despres i va a posicions diferents de l'array. El separador se'l menja
		String[] split = frase.split(" ");
		
		//converteixo l'array a llista
		//ArrayList<String> llistaSplit = new ArrayList<String>(Arrays.asList(split));
		//System.out.println(llistaSplit);
		
		//2 saber la longitud de cada frase
		for (int i = 0; i < split.length; i++) {
			System.out.println(split[i]+ " te una longiud de "+split[i].length());
		}
		
		//3 calcular el maxim i el minim
		
		//la forma mes senzilla es inicialitzar maxims i minims amb el primer cas
		int longMax = split[0].length();
		int longMin = split[0].length();
		String paraulaAmbLongMax = "";
		String paraulaAmbLongMin = "";
		for (int i = 0; i < split.length; i++) {
			//si la longitud de la paraula que recorrem actalment es mes gran que la longitud maxima
			if(split[i].length()>longMax) {
				//la longitud maxima es la de la nova paraula
				longMax = split[i].length();
				paraulaAmbLongMax = split[i];
			}else if(split[i].length()<longMin) {
				longMin = split[i].length();
				paraulaAmbLongMin = split[i];
			}
		}
		System.out.println(paraulaAmbLongMax+" te La longitud maxima, es "+longMax+". i "+paraulaAmbLongMin+" te la longitud minima es "+longMin);
		
		
		
	}

}
