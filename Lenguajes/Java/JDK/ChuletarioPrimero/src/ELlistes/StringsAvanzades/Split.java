package ELlistes.StringsAvanzades;

import java.util.Arrays;

public class Split {

	public static void main(String[] args) {

		String s = "Albert menja Allioli";

		// split es un metode molt especial de l'String
		// tu li passes un delimitador i et torna un ARRAY de totes les strings tallades
		// per aquest delimitador

		System.out.println("Split per espai en blanc");
		// split per espai en blanc. Separa per paraules
		String[] split = s.split(" ");

		for (int i = 0; i < split.length; i++) {
			System.out.println(split[i]);
		}

		System.out.println("Split per e");
		//fixeuvos com el delimitador se'l menja
		String[] split2 = s.split("e");

		for (int i = 0; i < split2.length; i++) {
			System.out.println(split2[i]);
		}
		
		
		System.out.println("Split per espai o coma");
		//Expressions regulars. Es pot escriure un llibre
		String[] splitRegex = s.split("[ l]+");
		for (int i = 0; i < splitRegex.length; i++) {
			System.out.println(splitRegex[i]);
		}
		
		
		
		System.out.println("Split per cada n posicions fixes");
		String splitperposicio="holaquetalsupremaciaracia";
		//la idea es que poses "(?<=\\G..)" posant tants punts com caracters vols
		String[] splitpos =  splitperposicio.split("(?<=\\G...)");
		//Imprimir un array rapidament en format llista.
		System.out.println(Arrays.toString(splitpos));

	}

}
