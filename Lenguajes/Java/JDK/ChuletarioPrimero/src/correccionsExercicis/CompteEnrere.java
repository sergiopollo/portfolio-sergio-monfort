package correccionsExercicis;

import java.util.Scanner;

public class CompteEnrere {
	
	public static void main(String[] args) {
		
		
		
		//FORMA 1 DE FERHO
		/*
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		while(n>0) {
			System.out.println(n);
			n--; //n = n-1;
		}*/
		//FORMA 2 DE FERHO
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		
		//tu saps quants cops s'executa el bucle -> For
		//si no saps quan s'acaba el bucle -> While
		for (int i = n; i > 0; i--) {
			System.out.println(i);
		}
		
		
		
		
	}

}
