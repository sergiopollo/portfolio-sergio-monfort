package FMatrius.matrius;

import java.util.Random;

public class MatriusFilesiColumnes {

	public static void main(String[] args) {

		// una matriu es un array2d, un array d'arrays
		// acaba organitzant-se per files i columnes
		int[][] array2d = new int[4][3];

		// el nombre de files es la quantitat d'arrays que t� l'array d'arrays, per
		// tant, .length
		int files = array2d.length;
		// el nombre de columnes es la quantitat d'elements que t� cadascun dels arrays
		// interns de l'array d'array, per tant, array[qualsevol].length. Posem 0 perque
		// segur que sempre hi ha un element 0.
		int columnes = array2d[0].length;

		// Omplir un Array
		Random r = new Random();
		// sempre recorrem primer per files i despres per columnes
		for (int f = 0; f < files; f++) {
			// ara per columnes
			for (int c = 0; c < columnes; c++) {
				// per accedir a totes les posicions de l'array es fa
				// array[iteradorDeFiles][iteradorDeColumnes];
				array2d[f][c] = r.nextInt(10);
			}
		}

		// accedeixo al primer element del primer array intern
		// per tant es pot dir que accedeixo a fila 0, columna 0.
		System.out.println(array2d[0][0]);

		for (int f = 0; f < files; f++) {
			// ara per columnes
			for (int c = 0; c < columnes; c++) {
				System.out.print(array2d[f][c] + " ");
			}
			// entre el primer i el segon for posem l'espai en blanc, aixi posara cada fila
			// separadament.
			System.out.println();
		}
		
		
		//vale. Vull mirar si a la fila 2 hi ha un 5.
		
		//quan recorres una fila la recorres per columnes.
		//primer mirar, f2c0, f2c1, f2c2
		boolean flag = false;
		for (int i = 0; i < columnes; i++) {
			int valor = array2d[2][i];
			if(valor==5) {
				flag=true;
			}
		}
		System.out.println(" En fila2 hi ha un 5? "+flag);
		
		
		//OK OK. Ara vull mirar si a la COLUMNA 2 hi ha un 5.
		//primer mirar f0c2, f1c2, f2c2, f3c2
		flag = false;
		for (int i = 0; i < files ; i++) {
			int valor = array2d[i][2];
			if(valor==5) {
				flag=true;
			}
		}
		System.out.println(" En col2 hi ha un 5? "+flag);

	}
}
