package FMatrius.matrius;

import java.util.Scanner;

public class MatriusMourePJ {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int[][] taulell = new int[7][11];
		int files = taulell.length;
		int columnes = taulell[0].length;

		// posicio del pj
		int pjfil = 3;
		int pjcol = 5;

		// inicialitzo la matriu a 0
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				taulell[i][j] = 0;
			}
		}

		// el personatge es representa amb un 1.
		taulell[3][5] = 1;

		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				System.out.print(taulell[i][j]);
			}
			System.out.println();
		}

		while (true) {

			String opcio = sc.nextLine();
			switch (opcio) {
			case "w":
				if (pjfil > 0) {
					// esborrem el pj
					taulell[pjfil][pjcol] = 0;
					// pujem una fila
					pjfil--;
					// escrivim de nou el pj
					taulell[pjfil][pjcol] = 1;
				}
				break;
			case "s":
				if (pjfil < files - 1) {
					// esborrem el pj
					taulell[pjfil][pjcol] = 0;
					// baixar una fila
					pjfil++;
					// escrivim de nou el pj
					taulell[pjfil][pjcol] = 1;
				}
				break;
			case "a":
				if (pjcol > 0) {
					// esborrem el pj
					taulell[pjfil][pjcol] = 0;
					// una columna a l'esquerra
					pjcol--;
					// escrivim de nou el pj
					taulell[pjfil][pjcol] = 1;
				}
				break;
			case "d":
				if (pjcol < columnes - 1) {
					// esborrem el pj
					taulell[pjfil][pjcol] = 0;
					// una columna a la dreta
					pjcol++;
					// escrivim de nou el pj
					taulell[pjfil][pjcol] = 1;
				}
				break;
			}

			// imprimir la matriu
			for (int i = 0; i < files; i++) {
				for (int j = 0; j < columnes; j++) {
					System.out.print(taulell[i][j]);
				}
				System.out.println();
			}

		}

	}

}
