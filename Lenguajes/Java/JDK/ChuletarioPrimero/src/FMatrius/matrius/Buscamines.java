package FMatrius.matrius;

import java.util.Scanner;

public class Buscamines {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int l = sc.nextInt();
		int f = sc.nextInt();
		int c = sc.nextInt();
		
		int[][] mat = new int[f][c];
		
		int files = mat.length;
		int columnes = mat[0].length;
		
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		
		int x = sc.nextInt();
		int y = sc.nextInt();
		
		//si hi ha una mina a la casella clicada
		if(mat[x][y]==1) {
			System.out.println("BOOM");
		}else {
			//com consultar caselles adjacents
			int comptadorminesadjacents=0;
			for (int i = x-1; i <= x+1; i++) {
				for (int j = y-1; j <= y+1; j++) {
					if(i>=0 && i<files && j>=0 && j<columnes) {
						if(mat[i][j]==1) {
							comptadorminesadjacents++;
						}
					}
				}
			}
			System.out.println(comptadorminesadjacents);
			
		}
		
		
		
	}

}
