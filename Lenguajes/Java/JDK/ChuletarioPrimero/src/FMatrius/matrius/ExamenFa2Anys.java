package FMatrius.matrius;

import java.util.Random;
import java.util.Scanner;

/**
 * Examen de 2018 corregit per a preparacio per l'examen
 * 17/12/20
 * @author marc
 *
 */
public class ExamenFa2Anys {
	
	/**
	 * Funcio per imprimir una matriu
	 * @param matriu la matriu que imprimeixes
	 */
	private static void imprimirMatriu(int[][] matriu) {
		for (int i = 0; i < matriu.length; i++) {
			for (int j = 0; j < matriu[0].length; j++) {
				System.out.print(matriu[i][j]+" ");
			}System.out.println();
		}
		
	}
	
	private static void imprimirMatriu(String[][] matriu) {
		for (int i = 0; i < matriu.length; i++) {
			for (int j = 0; j < matriu[0].length; j++) {
				System.out.print(matriu[i][j]+" ");
			}System.out.println();
		}
		
	}
	
	public static void main(String[] args) {
		
		exercici1();
		exercici2();
		exercici3();
		exercici4();
		
		
	}

	private static void exercici4() {
		// TODO Auto-generated method stub
		System.out.println("Exercici4");
		Scanner sc = new Scanner(System.in);
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		int[][] mat = new int[files][columnes];
		Random r = new Random();
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				mat[i][j] = r.nextInt(10);
				
			}
		}
		
		imprimirMatriu(mat);
		
		int f1 = sc.nextInt();
		int c1= sc.nextInt();
		
		int accF=0;
		int accC=0;
		
		for (int i = 0; i < files; i++) {
			accC= accC+mat[i][c1];
		}
		
		for (int i = 0; i < columnes; i++) {
			accF = accF+mat[f1][i];
		}
		
		imprimirMatriu(mat);
		
		int sumaTotal = accF+accC-mat[f1][c1];
		
		System.out.println(sumaTotal);
	
		
		
	}

	private static void exercici3() {
		// TODO Auto-generated method stub
		System.out.println("Exercici3");
		Scanner sc = new Scanner(System.in);
		
		int size = sc.nextInt();
		
		
		String[][] mat = new String[size][size];
		
		
		
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				mat[i][j]=".";
			}
		}
		//n=7
		//primera fila
		//la fila se mantiene a 0, recorremos la columna
		for (int i = 0; i < size; i++) {
			mat[0][i] = "X";
		}
		
		//ultima fila
		//la fila se mantiene a size-1, recorremos la columna
		for (int i = 0; i < size; i++) {
			mat[size-1][i] = "X";
		}
		
		//primera col
		//la col se mantiene a 0, recorremos la fila
		for (int i = 0; i < size; i++) {
			mat[i][0]="X";
		}
		
		//ultima col
		//la col se mantiene a size-1, recorremos la fila
		for (int i = 0; i < size; i++) {
			mat[i][size-1]="X";
		}
		
		
		
		
		imprimirMatriu(mat);

	}

	private static void exercici2() {
		// TODO Auto-generated method stub
		System.out.println("Exercici2");
	}

	private static void exercici1() {
		// TODO Auto-generated method stub
		System.out.println("Exercici1");
		Scanner sc = new Scanner(System.in);
		int[][] matriu = new int[7][7];
		int files = matriu.length;
		int columnes = matriu[0].length;
		Random r = new Random();
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				int numRandom = r.nextInt(5);
				if(numRandom==1) {
					matriu[i][j]=1;
				}else if(numRandom==2) {
					matriu[i][j]=2;
				}else {
					matriu[i][j]=0;
				}
			}
		}
		
		imprimirMatriu(matriu);
		
		int fsalt = sc.nextInt();
		int csalt = sc.nextInt();
		
		int contadorEnemigos = 0;
		if(matriu[fsalt][csalt]==1) {
			//queremos mirar arriba a la izquierda
			//fsalt-1 csalt-1
			//queremos que este dentro de la matriz
			//que significa que este dentor de la matriz?
			//que las filas esten entre 0 y el tama�o de las filas (que esta en la variable files)
			//que las columnas esten entre 0 y el tama�o delas columnas (que esta en la variables columnes)
			if(fsalt-1>=0 && csalt-1>=0 && fsalt-1<files && csalt-1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt-1][csalt-1]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar arriba
			//fsalt-1 csalt (porque es igual)
			if(fsalt-1>=0 && csalt>=0 && fsalt-1<files && csalt-1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt-1][csalt]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar arriba a la derecha
			//fsalt-1 csalt+1
			if(fsalt-1>=0 && csalt+1>=0 && fsalt-1<files && csalt+1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt-1][csalt+1]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar a la izquierda
			if(fsalt>=0 && csalt-1>=0 && fsalt<files && csalt-1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt][csalt-1]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar a la derecha
			if(fsalt>=0 && csalt+1>=0 && fsalt<files && csalt+1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt][csalt+1]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar abajo a la izquierda
			if(fsalt+1>=0 && csalt-1>=0 && fsalt+1<files && csalt-1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt+1][csalt-1]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar abajo
			if(fsalt+1>=0 && csalt>=0 && fsalt+1<files && csalt<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt+1][csalt]==2) {
					contadorEnemigos++;
				}
			}
			//queremos mirar abajo a la derecha
			if(fsalt+1>=0 && csalt+1>=0 && fsalt+1<files && csalt+1<columnes) {
				//si no te pasas
				//mira si hay un enemigo en esa posicion
				if(matriu[fsalt+1][csalt+1]==2) {
					contadorEnemigos++;
				}
			}
			
			System.out.println(contadorEnemigos);
		}else {
			System.out.println("ERROR");
		}
		
	}
	
	
	

	

}
