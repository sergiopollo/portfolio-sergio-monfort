package Cbucles.bucleswhile;

/**
 * Introducci� a bucles While, a on la condici� de sortida canvia i per tant acaba sortint del bucle
 * 13/10/20
 * @author marc
 *
 */
public class WhileCanviant {
	
	public static void main(String[] args) {
		
		int a = 5;
		int b = 9;
		//WHILE ES LA CONDICIO PER ESTAR DINTRE DEL WHILE
		//LA CONDICIO NO ES LA CONDICIO DE SORTIDA
		while(a<=b) {
			System.out.println("1");
			a++; //a = a +1;
		}
		System.out.println("2");
	}

}
