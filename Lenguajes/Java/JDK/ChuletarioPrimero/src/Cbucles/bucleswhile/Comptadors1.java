package Cbucles.bucleswhile;

import java.util.Scanner;

/**
 * Introducció al concepte de comptador i acumulador
 * 13/10/20
 * @author marc
 *
 */
public class Comptadors1 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int i = 0; //els comptadors per conveni es solen dir i
		//tradicionalment amb el comptador nomes faras una cosa, que es i++;
		//VOLEM UN CODI QUE LI DONES UN NUMERO I ET TORNA LA SUMA DE TOTS ELS NUMEROS FINS A AQUELL.
		//PER EXEMPLE, SI LI DONES 8, T'HA DE TORNAR 1+2+3+4+5+6+7+8= 36
		
		int acc = 0; //l'acumulador es com un comptador pero es va sumant
		//la formula de l'acumulador es
		//acc = acc + algunaCosa;
		//acc = acc - algunaCosa;
		//acc = acc * algunaCosa;
		
		while(i<n) {
			i++;
			//l'acumulaor es ell mateix mes i
			//per tant, la primera interacio sera 0+1, la segona 1+2, la tercera 3+3, la quarta 6+4, la tercera 10+5, etc.
			acc = acc + i;
		}
		System.out.println(acc);
		
	}

}
