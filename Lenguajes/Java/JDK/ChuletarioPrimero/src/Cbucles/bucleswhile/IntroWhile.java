package Cbucles.bucleswhile;

/**
 * Introducci a bucles While
 * 13/10/20
 * @author marc
 *
 */
public class IntroWhile {
	
	public static void main(String[] args) {
		
		//while. Funciona exactament igual que un if
		//while ( condicio booleana ) {
		//  codi que executa mentres es compleixi la condicio
		// }
		int a = 40;
		int b = 5;
		while(a<b) {//si la condicio no es compleix d'inici, no s'executa res.
			System.out.println("1");
		}//quan arribes al final de un while, tornes al principi, a comprovar la condicio
		System.out.println("2");
		

		//he fet un canvi absurd.
		//he fet un canvi absurd 2.
		
	}

}
