package Cbucles.bucleswhile;

/**
 * Introducció a bucles While fets servir per a comptar
 * 13/10/20
 * @author marc
 *
 */
public class WhilePerComptar {
	
	public static void main(String[] args) {
		
		
		int a = 0;
		
		//COMPTE A ON FAS SERVIR UNA VARIABLE COMPTADORA O ACUMULADORA, SI DESPRES O ABANS DE SUMAR.
		while(a<10) {
			System.out.println("Aqui imprimeixo abans de sumar "+a);
			a++;
			System.out.println("Aqui imprimeixo despres de sumar "+a);
		}
		
		System.out.println("Ja he acabat");
		
		
		//IMPRIMEIX "A" 8 vegades
		
		int b = 0;
		while(b<8) {
			//b++; //A
			System.out.println("A");
			//b++; //B
		}
		
	}

}
