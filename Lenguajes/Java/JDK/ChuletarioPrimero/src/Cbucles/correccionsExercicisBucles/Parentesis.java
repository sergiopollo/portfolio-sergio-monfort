package Cbucles.correccionsExercicisBucles;

import java.util.Scanner;

public class Parentesis {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ParentesisObertsSenseTancar = 0;

		String frase = sc.nextLine();
		boolean error = false;

		for (int i = 0; i < frase.length(); i++) {
			char c = frase.charAt(i);
			if (c == '(') {
				ParentesisObertsSenseTancar++;
			} else if (c == ')') {
				if (ParentesisObertsSenseTancar > 0) {
					ParentesisObertsSenseTancar--;
				} else {
					error = true;
				}
			}
		}
		if (error == true) {
			System.out.println("NO");
		} else if (ParentesisObertsSenseTancar > 0) {
			System.out.println("NO");
		} else {
			System.out.println("SI");
		}

	}

}
