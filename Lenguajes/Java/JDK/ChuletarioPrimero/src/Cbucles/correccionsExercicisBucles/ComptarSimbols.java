package Cbucles.correccionsExercicisBucles;

public class ComptarSimbols {

	public static void main(String[] args) {

		// contar F.

		String frase = "El Marc no vindra fins d'aqui uns dies perque te l'esquena feta pols i esta tirat al llit.";

		int tamanyFrase = frase.length();

		int f = 0;

		for (int i = 0; i < tamanyFrase; i++) {
			char c = frase.charAt(i);
			if (c == 'f' || c == 'F') { // fixeuvos que en chars si que pots fer comparacio directa i no per equals
				f++;
			}

		}
		System.out.println(f);
	}

}
