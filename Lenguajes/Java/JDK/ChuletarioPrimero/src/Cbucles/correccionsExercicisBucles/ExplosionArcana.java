package Cbucles.correccionsExercicisBucles;

import java.util.Scanner;

public class ExplosionArcana {

	public static void main(String[] args) {

		/*
		 * Un ic�nic atac de mag en el WoW es Explosi�n Arcana. Aquest atac comen�a fent
		 * poc mal, pero cada cop que el fas servir fa un 100% m�s de mal. Per exemple,
		 * si el primer cop fa 100 punts de mal, el seg�ent far� 200, el seg�ent 300, el
		 * seg�ent 400, i aix� fins que passa un temps.
		 */

		Scanner sc = new Scanner(System.in);

		int ini = sc.nextInt();
		int q = sc.nextInt();

		/*
		 * Forma 1 de ferho int total = 0; //volem que vagi de 1 a Q for (int i = 1; i
		 * <= q; i++) { //tu pots dir que la primera explosio es ini*1, la segona ini*2,
		 * la tercera ini*3, i aix� et permet fer servir la i. int explosioActual =
		 * ini*i; //acumulem el dany total a l'acumulador total = total +
		 * explosioActual; } System.out.println(total);
		 */

		/* Forma 2 de fer-ho */

		// acumulador d'explosio
		int explosio = 0;
		// acumulador de dany total
		int total = 0;
		// fem tantes rondes com Q
		for (int i = 0; i < q; i++) {
			// cada explosio es la que ja teniem + 100;
			explosio = explosio + ini;
			// acumulem el dany total
			total = total + explosio;
		}
		// imprimim el total
		System.out.println(total);
	}

}
