package Cbucles.correccionsExercicisBucles;

import java.util.Scanner;

public class CompteVeri {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int ncasos = sc.nextInt();

		for (int cas = 0; cas < ncasos; cas++) {

			// aqui va el codi de 1 cas.
			int hp = sc.nextInt();
			int ram = sc.nextInt();
			int tw = sc.nextInt();
			int matat = 0;

			while (hp > 0) {
				// ataca ra mmus
				hp = hp - ram;
				if (hp <= 0) {
					// l'ha matat rammus
					matat = 1;
				}
				if (matat != 1) {
					// ataca twitch NOMES SI NO L'HA MATAT ABANS RAMMUS
					hp = hp - tw;
					if (hp <= 0) {
						// l'ha matat twitch
						matat = 2;
					}
				}
			}
			if (matat == 1) {
				System.out.println("RAMMUS");
			} else if (matat == 2) {
				System.out.println("TWITCH");
			}

		}

	}

}
