package Cbucles.correccionsExercicisBucles;

import java.util.Scanner;

public class ExplosionArcana2 {

	public static void main(String[] args) {

		/*
		 * Un ic�nic atac de mag en el WoW es Explosi�n Arcana. Aquest atac comen�a fent
		 * poc mal, pero cada cop que el fas servir fa un 100% m�s de mal. Per exemple,
		 * si el primer cop fa 100 punts de mal, el seg�ent far� 200, el seg�ent 300, el
		 * seg�ent 400, i aix� fins que passa un temps.
		 */

		Scanner sc = new Scanner(System.in);

		int ini = sc.nextInt();
		int hp = sc.nextInt();

		// comencem en la primera ronda
		int ronda = 1;

		// la idea es anar restant la vida i sortir del bucle quan l'haguem matat
		while (hp > 0) {
			// l'explosio es el dany inicial multiplicat per la ronda
			int explosio = ini * ronda;
			// acumulador, pero restant en comtpes de sumar. Volem que la vida sigui ella
			// mateixa menys el dany de l'explosio, que li restarem
			hp = hp - explosio;
			// incrementem la ronda
			ronda++;
		}
		System.out.println(ronda);

	}

}
