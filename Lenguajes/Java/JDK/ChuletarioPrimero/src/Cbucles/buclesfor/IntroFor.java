package Cbucles.buclesfor;

/**
 * Introducci� a bucles for
 * 20/10/20
 * @author marc
 *
 */
public class IntroFor {
	
	public static void main(String[] args) {
		
		
		//for es un bucle molt especific que nom�s serveix per una cosa
		//serveix per comptar d'un inici que ja coneixes a un final que ja coneixes
		
		//en el 99.9% el for ser� sobre un enter
		//for(int i = valor_inicial; condicio; step)
		
		//si volem comptar de 1 a 10;
		for(int i = 1; i<=10; i++) {
			System.out.println("comptar del 1 al 10 "+i);
		}
		
		//si volem comptar enrere del 10 al 1;
		for(int i = 10; i>0; i--) {
			System.out.println("comptar enrere del 10 al 1 "+i);
		}
		
		int a = 10;
		int b = 20;
		//vull comptar de "A" a "B". I a m�s vull comptar de dos en dos.
		//i+=2 es exactament el mateix que dir i= i+2;
		for (int i = a; i<= b; i+=2) {
			System.out.println("compto de a a b de 2 en 2 "+i);
		}
		
	}

}
