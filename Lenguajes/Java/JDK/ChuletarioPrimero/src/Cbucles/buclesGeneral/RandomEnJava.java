package Cbucles.buclesGeneral;

import java.util.Random;

/**
 * Com es fa servir la Classe Random
 * 20/10/20
 * @author marc
 *
 */
public class RandomEnJava {
	
	public static void main(String[] args) {
		
		//per generar numeros aleatoris fas servier la classe random
		Random r = new Random();
		
		//numero random. MOLT RANDOM
		int random1 = r.nextInt();
		
		System.out.println(random1);
		
		//numero random entre 0 i 10 (sense comptar el 10)
		int random2 = r.nextInt(10);
		
		System.out.println(random2);
		
		//numero random entre 10 i 20 (comtpant els dos)
		int random3 = r.nextInt(11)+10;
		System.out.println(random3);
		
		//numero random entre -20 i 20 (comptant els dos)
		int random4 = r.nextInt(41)-20;
		System.out.println(random4);
		
		//tirar moneda (true o false)
		boolean moneda = r.nextBoolean();
		System.out.println(moneda);
		
		//numero decimal entre 0 i 1 (infinit!)
		double decimal = r.nextDouble();
		System.out.println(decimal);
	}

}
