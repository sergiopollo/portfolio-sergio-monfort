package Cbucles.buclesGeneral;


/**
 * M�s exemples de localitat (variables locals i el seu ambit)
 * 16/10/20
 * @author marc
 *
 */
public class localitat2 {
	
	public static void main(String[] args) {
		
		
		int a = 5;
		//no puc fer una nova variable a perque ja existeix
		//String a = "Hola"; dona error
		
		int i = 0;
		while(i<10) {
			//no puc fer una nova variable a perque ja existeix (i l'if esta dintre del main)
			//String a = "Hola";  dona error
			int b = 5;
			i++;
			
		}
		//puc fer una nova variable b perque en el moment que surts del bucle int b deixa d'existir i pots tornar a fer servir b.
		String b = "Adeu";
	}

}
