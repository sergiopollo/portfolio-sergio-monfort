package Cbucles.buclesGeneral;


/**
 * Introducci� a localitat (variables locals i el seu ambit)
 * 16/10/20
 * @author marc
 *
 */
public class localitat {
	
	int socUnaVariableGlobal;
	
	public static void main(String[] args) {
		
		
		//hi ha dos tipus de variables. Variables Globals i Variables Locals
		//les variables locals nom�s es poden utilitzar en el teu ambit local
		//les variables locals en eclipse son marrons
		//les variables globals en eclise son blaves
		
		int socUnaVariablelocal;
		
		//la localitat la marquen les claus  { }
		
		//una variable es local al lloc a on ha estat declarada
		//per tant, a on has posat el tipus i l'has fet servir per primera vegada
		//aquesta variable a es local a main.
		int a = 3;
		
		if(4 == 4) {
			//pots fer servir a perque es local a main i l'if esta dintre de main
			a = 5;
			//aquesta variable b es local al if.
			int b = 4;
		}
		
		//no pots fer servir b perque es local a l'if i ara estem fora de l'if.
		//System.out.println(b);
		
		System.out.println(a);
		
		
		
	}

}
