package Cbucles.buclesdowhile;

import java.util.Scanner;

/**
 * Cas a on un do-while es millor que un while
 * 20/10/20
 * @author marc
 *
 */
public class casUtildeDoWhile {
	
	
	//llegeix numeros fins a trobar un 0
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		//amb while has de fer una lectura inicial abans i fa lleig
		int input = sc.nextInt();
		while(input != 0) {
			System.out.println(input);
			input = sc.nextInt();
		}
		
		//amb dowhile es m�s senzill
		do {
			input = sc.nextInt();
			System.out.println(input);
		}while(input!=0);
		
		
	}

}
