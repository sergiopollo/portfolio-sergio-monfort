package Cbucles.buclesdowhile;

/**
 * diferencies entre while i dowhile
 * 20/10/20
 * @author marc
 *
 */
public class diferenciaWhileiDoWhile {
	
public static void main(String[] args) {
		

		
		//un do while es diferencia d'un while en el cas de que la condicio no es compleixi d'inici
		int i = 0;
		do {
			i++;
			System.out.println("He comptat fins a "+i);
		}while(i<0);
		
		//en la majoria de casos fa el mateix que un while
		int j = 0;
		while(j<0) {
			j++;
			System.out.println("He comptat fins a "+j);
		}
	}

}
