package Cbucles.buclesdowhile;


/**
 * Introducci� a bucles do-while
 * 20/10/20
 * @author marc
 *
 */
public class introDoWhile {
	
	public static void main(String[] args) {
		

		
		//un bucle do while es exactament igual que un while.
		//la diferencia es que la primera iteraci� en un do while es fa sempre
		int i = 0;
		do {
			i++;
			System.out.println("He comptat fins a "+i);
		}while(i<10);
		
		//en la majoria de casos fa el mateix que un while
		int j = 0;
		while(j<10) {
			j++;
			System.out.println("He comptat fins a "+j);
		}
	}

}
