package Cbucles.flags;

import java.util.Scanner;

/**
 * 27/10/20
 * Introducció als flags
 * @author IES SABADELL
 *
 */
public class IntroFlags {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		/**
		 * Llegeix numeros i torna el numero +1 fins que llegeixis un numero negatiu.
		 */
		
		//per conveni un flag s'inicia a false i s'activa a true.
		boolean flag = false;
		
		do {
			int num = sc.nextInt();
			
			if(num<0) {
				//activem el flag
				flag = true;
			}else {
				System.out.println(num+1);
			}
		}while(!flag);
		
		
		/**
		 * Llegeix strings i torna la string +"a" fins que llegeixis "FIN".
		 */
		
	
		boolean flagString = false;
		while(!flagString) {
			String s = sc.nextLine();
			if(s.equals("FIN")) {
				flag=true;
			}else {
				System.out.println(s+"a");
			}
		}
		
				
	}

}
