package Cbucles.exercicisBucles;

/**
 * Exercicis de Bucles de pintar quadrats
 * 21/10/20
 * @author marc
 *
 */
public class PintarCuadrat {
	
	public static void main(String[] args) {
		
		int n = 5;
		
		//pintar un quadrat.
		
		//2 bucles
		//bucle? FOR
		//bucle? FOR. Sabem inici = 0  final = n;
		//necessitem un comptador? NO
		//necessitem un acumulador? SI - String
		
		//bucle de fora. Calcula el nombre de linies que necessites
		for (int i = 0; i < n; i++) {
			String acc = "";
			//bucle de dintre. Imprimeix les X necessÓries a cada linea
			for (int j = 0; j < n ; j++) {
				//formula del acumulador es acc = acc + algunacosa
				acc = acc + "X";
			}
			System.out.println(acc);
		}
		
		
		
	}

}
