package AIntro.scanner;
import java.util.Scanner;

public class EntradaPerTeclat {
	public static void main(String[] args) {
		
		//es complicado
		
		//TIPUS NOM = VALOR
		//aquesta frase memoritzeula. 
		Scanner lector = new Scanner(System.in);
		
		
		System.out.println("A qui vols votar com a impostor:");
		//scanner.nextLine(); llegeix una linea de text i la torna com un string
		String nomImpostor = lector.nextLine();
		System.out.println(nomImpostor+" was ejected.");
		
		//si tu saps que t'arribara un numero
		System.out.println("Digues la teva edat");
		//scanner.nextInt(); llegeix un enter
		int edat = lector.nextInt();
		edat = edat + 1;
		System.out.println("D'aqui un any, tindras "+edat+" anys");
		
		
		System.out.println("Digues el teu pes");
		double pes = lector.nextDouble();
		pes = pes * 0.7;
		System.out.println("En Mart, pesaries "+pes+" (mas o menos)");
		
		
		
		
	}
}
