package AIntro.scanner;

public class IntroiPrints {

	// qualsevol funcio que no sigui main no s'executara
	public static void noMain() {
		System.out.println("Hola Damvi E");
	}

	// La funcio main es la funcio que s'executa al principi
	public static void main(String[] args) {
		// AIXO ES UN COMENTARI. ELS COMENTARIS SON IGNORATS I NOMES SERVEIXEN PER A
		// PARLAR AMB ALTRES PROGRAMADORS

		//s'escriu per pantalla amb el System.out.println
		System.out.println("Hola Damvi D");
		
		//dre�era de teclat: syso ctrl-espai
		System.out.println("Hola Tamb� misteriosos habitants del Discord");
		
		//cada linea s'executa sequencialment.
		
		/*
		 * variables
		 */
		
		//tipus primitius, que son tipus de variables molt comuns
		
		//int -> nombre enter
		//per declarar una variable fem servir
		//TIPUS_VARIABLE NOM_VARIABLE = VALOR;
		int numMacarrons = 5;
		
		System.out.println(numMacarrons);
		
		//double -> nombre decimal
		double notaExamenAngel = 10.75;
		double notaRealAngel = notaExamenAngel - 10.74;
		
		System.out.println(notaExamenAngel);
		System.out.println(notaRealAngel);
		
		//podem fer operacions matematiques (mes o menys)
		
		int a = 5;
		int b = 7;
		//pots modificar els valors de les variables, i fins i tot fer auto-refer�ncia
		//si vols modificar una variable ja creada no tornes a posar el tipus.
		a = a + 2;
		//en una divisio entera simplement elimina els decimals

		int c = a/b;
		
		System.out.println(c);
		
		//String -> texto
		String juli = "Juli es el impostor";
		String motivo = "Lo he visto meterse en la alcantarilla";
		
		System.out.println(juli+" "+motivo);
		
		
		System.out.println("La nota de Angel es: "+notaRealAngel);
		
		//boolean -> SI/NO
		boolean siONo = true;
		System.out.println(siONo);
		

	}

}
