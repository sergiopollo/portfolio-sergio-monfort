package AIntro.scanner;
import java.util.Scanner;

public class ScannerVidaYMilagros {
	
	
	public static void main(String[] args) {
		
		//proTIP Scanner: si escrius Scanner i ctrl-espai, l'importa automaticament 
		//Per que escrius aquesta frase?
		Scanner sc = new Scanner(System.in);
		
		//amb l'escanner pots llegir linies senceres i aniran a una String
		String s = sc.nextLine();
		
		System.out.println(s);
		
		//nextInt llegeix un numero. Ignora espais i salts de linia fins que troba el seguent numero
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		int d = sc.nextInt();
		int e = sc.nextInt();
		
		int suma = a+b+c+d+e;
		System.out.println(suma);
		
		
		//nextDouble 
		
		
		
	}

}
