package AIntro.canvistipus;

/**
 * Casteig - Canvis de tipus
 * 23/10/20
 * @author IES SABADELL
 *
 */
public class Casteig {

	public static void main(String[] args) {
		
		int enter= 1234;
		double doble = 1234.5678;
		String str = "hola";
		String enterComAStr = "4321";
		String dobleComAStr = "4321.9876";
		
		//de doble (o float) a enter
		//Casteig: Posar entre parentesis davant el tipus al que vols passar
		int nouEnter = (int) doble;
		//El casteig no es pot fer servir sempre. Nomes en tipus molt similars o en herencia i polimorfia (ja ho veurem)
		
		//De Str a Enter
		//Nomes funciona si la string era num�rica en primer lloc
		int enterStr = Integer.parseInt(enterComAStr);
		enterStr++;
		System.out.println(enterStr);
		
		//De Str a Double
		double dobleStr = Double.parseDouble(dobleComAStr);
		dobleStr += 0.2;
		System.out.println(dobleStr);
		
		
		//De int a Str
		String novaString = enter+"";
		novaString+= "a";
		String tambeServeix = String.valueOf(enter);
		System.out.println(novaString);
		
		
		//De char a int
		
		//forma llarga
		char c = '4';
		int intDeChar = Character.getNumericValue(c);
		//forma curta. El 0 es 48 en el ASCII aixi que es pot restar 48.
		int intDeChar2 = c-48;
		System.out.println(intDeChar + " " + intDeChar2);
		
		
		
		
	}
}
