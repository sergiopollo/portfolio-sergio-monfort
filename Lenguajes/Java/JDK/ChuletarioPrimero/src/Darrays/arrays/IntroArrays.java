package Darrays.arrays;

import java.util.Random;
import java.util.Scanner;

public class IntroArrays {
	
	public static void main(String[] args) {
		
		//arrays es una forma d'emmagatzemar dades
		//1. Ha de ser un array del mateix tipus
		//2. Ha de tenir una mida fixa i que la defineixes al principi.
		
		//declaraci� d'un array
		//tipus[] nomArray = new tipus[tamany];
		int[] arrayInts = new int[5];
		String[] arrayStrings = new String[3];
		Scanner[] arrayScanner = new Scanner[1];
		
		Random r = new Random();
		
		int longitudArray = arrayInts.length; //es diferent a la String.
		
		//escriu en la posici� 0 de l'array un 4
		arrayInts[0] = 4;
		System.out.println(arrayInts[1]);
		
		
		//escriu un numero random a cada posici� de l'array
		for (int i = 0; i < arrayInts.length; i++) {
			int numeroRandom = r.nextInt(100)+1;
			arrayInts[i]=numeroRandom;
		}
		
		//llegeix tots els valors de l'array
		for (int i = 0; i < arrayInts.length; i++) {
			System.out.println(arrayInts[i]);
		}
		
		//suma tots els valors de l'array
		int acc = 0;
		for (int i = 0; i < arrayInts.length; i++) {
			//recordeu que aix� �s igual a acc = acc + arrayInts[i];
			acc+= arrayInts[i];
		}
		
		System.out.println(acc);
		
		
		
		
		
		
	}

}
