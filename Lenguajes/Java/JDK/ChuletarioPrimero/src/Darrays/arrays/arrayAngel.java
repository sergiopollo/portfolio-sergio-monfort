package Darrays.arrays;

import java.util.Iterator;
import java.util.Random;

public class arrayAngel {
	
	public static void main(String[] args) {
		
		
		Angel[] arrayAngels = new Angel[3];
		
		Random r = new Random();
		
		for (int i = 0; i < arrayAngels.length; i++) {
			Angel a = new Angel();
			a.neuronas = r.nextInt();
			a.dineros = r.nextDouble()*100;
			a.pareja = r.nextBoolean();
			arrayAngels[i] = a;
		}
		
		for (Angel angel : arrayAngels) {
			System.out.println("Tenemos un Angel con "+angel.neuronas+" neuronas, "+angel.dineros+" euros, y tiene pareja? "+angel.pareja);
		}
		
		
	}

}
