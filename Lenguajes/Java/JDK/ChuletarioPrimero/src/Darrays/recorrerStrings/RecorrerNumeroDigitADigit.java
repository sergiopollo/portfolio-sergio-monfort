package Darrays.recorrerStrings;

public class RecorrerNumeroDigitADigit {
	
	public static void main(String[] args) {
		
		//SUMA TOTS ELS DIGITS D'UN NUMERO
		
		int a = 123456;
		
		//convertim el nombre a String
		String str = a+"";
		int acc = 0;
		
		//recorrem el nombre com a String
		for (int i = 0; i < str.length(); i++) {
			//agafem la i-ena posicio
			char c = str.charAt(i);
			//convertim a num
			int num = c-48;
			//formula acumulador
			acc+= num; 
			
		}
		System.out.println(acc);
		
		
	}

}
