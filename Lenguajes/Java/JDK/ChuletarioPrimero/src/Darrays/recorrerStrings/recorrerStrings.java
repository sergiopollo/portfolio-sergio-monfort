package Darrays.recorrerStrings;

import java.util.Scanner;

/**
 * Recorrer Strings amb un bucle For
 * 22/10/20
 * @author IES SABADELL
 *
 */
public class recorrerStrings {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		String frase = sc.nextLine();
		
		//string.length -> diu la longitud de l'string
		//string.charAt -> et dona el caracter a la posicio indicada
		
		//longitud de string
		int longitudString = frase.length();
		
		System.out.println(longitudString);
		
		for (int i = 0; i < longitudString; i++) {
			
			//recorre la string caracter a caracter
			char c = frase.charAt(i);
			System.out.println("El caracter en la posicio "+i+" es: "+c);
			
		}
		
		
		
		
		
		
	}

}
