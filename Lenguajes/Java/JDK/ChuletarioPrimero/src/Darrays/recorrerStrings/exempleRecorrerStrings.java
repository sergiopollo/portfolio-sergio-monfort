package Darrays.recorrerStrings;

import java.util.Scanner;

/**
 * Comptar a en String
 * 22/10/20
 * @author IES SABADELL
 *
 */
public class exempleRecorrerStrings {
	
	public static void main(String[] args) {
		
Scanner sc = new Scanner(System.in);
		
		String frase = sc.nextLine();
		
		//string.length -> diu la longitud de l'string
		//string.charAt -> et dona el caracter a la posicio indicada
		
		//longitud de string
		int longitudString = frase.length();
		
		int nombredeAs = 0;
		
		for (int i = 0; i < longitudString; i++) {
			
			//recorre la string caracter a caracter
			char c = frase.charAt(i);
			//chars. els podem comprar amb == i duen cometa simple
			if(c == 'a' || c=='A') {
				nombredeAs++;
			}
		}
		//per imprimir un resultat s'imprimeix fora del for.
		System.out.println(nombredeAs);
		
	}

}
