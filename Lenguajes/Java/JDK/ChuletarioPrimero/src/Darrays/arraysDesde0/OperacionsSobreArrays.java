package Darrays.arraysDesde0;

import java.util.Random;

public class OperacionsSobreArrays {

	public static void main(String[] args) {

		int[] aRandom = new int[10]; // un array de 10 enters

		Random r = new Random();

		// omplim array amb numeros randoms
		for (int i = 0; i < aRandom.length; i++) {
			aRandom[i] = r.nextInt(10) + 1;
		}

		// llegir un array, tots els numeros en una sola linea separats per espai
		for (int i = 0; i < aRandom.length; i++) {
			// print printa pero sense posar salt de linea al final
			System.out.print(aRandom[i] + " "); // syso del 1r pos, despres del 2n,
		}
		// despres del for posem nosaltres a ma el salt de linea
		System.out.println();

		// vull saber si hi ha el numero 5 en aquest array.
		boolean flag = false;
		for (int i = 0; i < aRandom.length; i++) {
			if (aRandom[i] == 5) {
				flag = true;
			}
		}
		if (flag) {
			System.out.println("Hem trobat el numero 5");
		} else {
			System.out.println("No hem trobat el numero 5");
		}
		
		
		//vull comptar quantes vegades apareix el numero 5
		int comptador = 0;
		for (int i = 0; i < aRandom.length; i++) {
			if(aRandom[i]==5) {
				comptador++;
			}
		}
		System.out.println("Hem trobat el num 5: " +comptador+ " vegades");
		
		
		
		//vull saber la posicio a la que surt el primer num 5
		
				int posicio = -1;
				boolean trobat = false;
				for (int i = 0; i < aRandom.length; i++) {
					if(aRandom[i]==5 && !trobat) {
						trobat = true;
						posicio = i;
					}
				}
				if(posicio==-1) {
					System.out.println("No hem trobat el num 5");
				}else {
					System.out.println("Hem trobat el num 5 a la posicio " +posicio);
				}
				
				
				

	}

}
