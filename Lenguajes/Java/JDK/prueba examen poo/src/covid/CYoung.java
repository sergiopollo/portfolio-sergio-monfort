package covid;

import java.util.Random;

public class CYoung extends Citizen {
	
	public CYoung() {
		super();
		Random r = new Random();
		int probabilidad = r.nextInt(21);
		this.age = probabilidad;
	}

	@Override
	public boolean isSick() {
		if(this.isInfected()) {
			return true;
		}else {
			return false;
		}
	}

}
