package covid;

import java.util.Random;

public abstract class Citizen {
	
	private static int numCitizen;
	private String name;
	protected int age;
	private boolean infected;
	
	public Citizen() {
		this.name="Citizen #"+numCitizen;
		numCitizen++;
		this.infected=false;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public boolean isInfected() {
		return infected;
	}

	@Override
	public String toString() {
		return "Citizen [name=" + name + ", age=" + age + "]";
	}
	
	public void infect() {
		this.infected=true;
	}
	
	public void interact(Citizen citizen) {
		
		Random r = new Random();
		
		int probabilidad = r.nextInt(2);
		
		if(probabilidad==1) {
			if(this.infected) {
				citizen.infected=true;
			}else if(citizen.isInfected()) {
				this.infected=true;
			}
		}
	}
	
	public abstract boolean isSick();
	
}
