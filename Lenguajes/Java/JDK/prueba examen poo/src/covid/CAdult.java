package covid;

import java.util.Random;

public class CAdult extends Citizen {
	
	public CAdult() {
		super();
		Random r = new Random();
		int probabilidad = r.nextInt(41)+20;
		this.age = probabilidad;
	}

	@Override
	public boolean isSick() {
		Random r = new Random();
		int probabilidad = r.nextInt(2);
		if(this.isInfected() && probabilidad==1) {
			return true;
		}else {
			return false;
		}
	}

}
