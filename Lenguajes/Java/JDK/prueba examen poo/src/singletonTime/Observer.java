package singletonTime;

public interface Observer {

	public void notifyObserver(int codi);
	
}
