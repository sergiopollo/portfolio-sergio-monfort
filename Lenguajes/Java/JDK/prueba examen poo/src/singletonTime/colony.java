package singletonTime;

import java.util.ArrayList;

public class colony {
	
	private static colony m_colony = null;
	private ArrayList<Room> rooms;
	
	private colony() {
		super();
		rooms = new ArrayList<Room>();
	}
	
	public static colony getInstance() {
		
		if(m_colony == null) {
			m_colony= new colony();
		}
		return m_colony;
		
	}

	public ArrayList<Room> getRooms() {
		return rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}

	@Override
	public String toString() {
		return "colony [rooms=" + rooms + "]";
	}
	
	public void addRoom(Room sala) {
		
		if(!this.rooms.contains(sala)) {
			this.rooms.add(sala);
		}
	}
	
	public void removeRoom(Room sala) {
		
		if(this.rooms.contains(sala)) {
			this.rooms.remove(sala);
		}
	}
}
