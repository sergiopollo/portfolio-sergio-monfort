package singletonTime;

import java.util.ArrayList;

public class Settler implements Observer{
	
	private static int SumaID;
	private String nom;
	private int IDSala;
	
	public Settler() {
		super();
		this.nom = "settler #"+SumaID;
		SumaID++;
		//p� que el default no sea 0
		this.IDSala=-1;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIDSala() {
		return IDSala;
	}

	public void setIDSala(int iDSala) {
		IDSala = iDSala;
	}

	@Override
	public String toString() {
		return "Settler [nom=" + nom + ", IDSala=" + IDSala + "]";
	}

	@Override
	public void notifyObserver(int codi) {
		
		if(this.IDSala == codi) {
			//hacen cosas, son catalanes
			ArrayList<Room> salas = colony.getInstance().getRooms();
			
			for (Room room : salas) {
				if(room.getID() == this.IDSala) {
					System.out.println("soy "+this.nom+" y estoy activando las maquinas de la sala "+room.getNom());
					room.doAccio();
				}
			}
		}else {
			System.out.println("soy "+this.nom+" y estoy del chill");
		}
		
	}

}
