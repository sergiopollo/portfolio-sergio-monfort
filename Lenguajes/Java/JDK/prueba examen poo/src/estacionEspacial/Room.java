package estacionEspacial;

import java.util.ArrayList;

public class Room {
	
	private static int SumaID;
	private int ID;
	private String nom;
	private ArrayList<Machine> maquines = new ArrayList<Machine>();
	
	public Room(String nom) {
		super();
		this.nom=nom;
		ID = SumaID;
		SumaID++;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public ArrayList<Machine> getMaquines() {
		return maquines;
	}
	public void setMaquines(ArrayList<Machine> maquines) {
		this.maquines = maquines;
	}
	@Override
	public String toString() {
		return "Room [ID=" + ID + ", nom="+nom+" maquines=" + maquines + "]";
	}
	
	public void addMachine(Machine maquina) {
		
		//si ja esta que no deixi afegir-la
		boolean encontrado = false;
		for (Machine machine : maquines) {
			if(machine.getNom() == maquina.getNom()) {
				encontrado=true;
				System.out.println("la maquina ja esta a la sala, no es pot tenir una altra igual");
			}
		}
		
		if(!encontrado) {
			maquines.add(maquina);
		}
		
	}
	
	public void deleteMachine(Machine maquina) {
		if(maquines.contains(maquina)) {
			maquines.remove(maquina);
		}
	}
	
	public void doAccio() {
		
		System.out.println("Room #"+ID+" ("+this.nom+") executing tasks");
		if(!maquines.isEmpty()) {
			for (Machine machine : maquines) {
				machine.doAccio();
			}
		}
	}
	
}
