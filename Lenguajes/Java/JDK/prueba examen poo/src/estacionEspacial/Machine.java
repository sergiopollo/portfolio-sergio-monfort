package estacionEspacial;

public abstract class Machine {
	
	private String nom;
	
	public Machine(String nom) {
		this.nom= nom;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void doAccio() {
		//aqui nada, ves a sus hijos
	}

	@Override
	public String toString() {
		return "Machine [nom=" + nom + "]";
	}
}
