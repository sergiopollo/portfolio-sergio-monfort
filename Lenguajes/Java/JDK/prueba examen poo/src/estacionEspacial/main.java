package estacionEspacial;

import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		
		colony colony = new colony();
		
		Room medbay = new Room("Medbay");
		Room engineering = new Room("Engineering gaming");
		Room maintenance = new Room("Maintenance");
		
		colony.addRoom(medbay);
		colony.addRoom(medbay);
		colony.addRoom(engineering);
		colony.addRoom(maintenance);
		
		Dispenser d1 = new Dispenser("d1");
		
		medbay.addMachine(d1);
		medbay.addMachine(d1);
		engineering.addMachine(d1);
		
		
		//System.out.println(colony);
		
		
		//hab1.doAccio();
		
		Scheduler scheduler = new Scheduler();
		
		//ArrayList<Settler> crewmates = new ArrayList<Settler>();
		/*
		for (int i = 0; i < 9; i++) {
			
			Settler s1 = new Settler(colony);
			scheduler.getListaObservers().add(s1);
			//crewmates.add(s1);
		}
		*/
		//System.out.println(crewmates);
		//dia1
		
		//System.out.println(medbay);
		
		/*
		Settler s1 = (Settler) scheduler.getListaObservers().get(0);
		s1.setIDSala(medbay.getID());
		Settler s2 = (Settler) scheduler.getListaObservers().get(1);
		s2.setIDSala(medbay.getID());
		Settler s3 = (Settler)scheduler.getListaObservers().get(2);
		s3.setIDSala(medbay.getID());
		

		Settler s4 = (Settler)scheduler.getListaObservers().get(3);
		s4.setIDSala(engineering.getID());
		Settler s5 = (Settler)scheduler.getListaObservers().get(4);
		s5.setIDSala(engineering.getID());
		Settler s6 = (Settler)scheduler.getListaObservers().get(5);
		s6.setIDSala(engineering.getID());
		*/
		
		Settler s1 = new Settler(colony);
		scheduler.getListaObservers().add(s1);
		Settler s2 = new Settler(colony);
		scheduler.getListaObservers().add(s2);
		Settler s3 = new Settler(colony);
		scheduler.getListaObservers().add(s3);
		Settler s4 = new Settler(colony);
		scheduler.getListaObservers().add(s4);
		Settler s5 = new Settler(colony);
		scheduler.getListaObservers().add(s5);
		Settler s6 = new Settler(colony);
		scheduler.getListaObservers().add(s6);
		Settler s7 = new Settler(colony);
		scheduler.getListaObservers().add(s7);
		Settler s8 = new Settler(colony);
		scheduler.getListaObservers().add(s8);
		Settler s9 = new Settler(colony);
		scheduler.getListaObservers().add(s9);
		
		s1.setIDSala(medbay.getID());
		s2.setIDSala(medbay.getID());
		s3.setIDSala(medbay.getID());
		
		s4.setIDSala(engineering.getID());
		s5.setIDSala(engineering.getID());
		s6.setIDSala(engineering.getID());
		
		//System.out.println(crewmates);
		
		scheduler.notifyObservers(engineering.getID());
		
	}

}
