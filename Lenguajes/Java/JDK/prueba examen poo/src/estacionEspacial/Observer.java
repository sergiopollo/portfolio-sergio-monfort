package estacionEspacial;

public interface Observer {

	public void notifyObserver(int codi);
	
}
