package estacionEspacial;

import java.util.ArrayList;
import java.util.Iterator;

public class Observable {
	
	//lista observers
	private ArrayList<Observer> listaObservers = new ArrayList<Observer>();

	//constructor
	public Observable() {
		super();
	}
	
	//getter
	public ArrayList<Observer> getListaObservers() {
		return listaObservers;
	}
	
	//subscriure's (es posa a la llista)
	public void subscribe(Observer ob) {

		listaObservers.add(ob);
	}
	
	//unsubscribe (borrar de la llista)
	public void unsubscribe(Observer ob) {

		listaObservers.remove(ob);
	
	}

	//notifyobservers (avisa a tots els de la llista)
	public void notifyObservers(int id) {
		
		//recorro la lista al reves para poder desuscribir y borrar de la lista mientras la recorre sin que pete
		for (int i = listaObservers.size()-1; i >= 0; i--) {
			
			Observer observer = listaObservers.get(i);
			observer.notifyObserver(id);
			
		}

	}
	
}
