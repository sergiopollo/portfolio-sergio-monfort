package estacionEspacial;

public class Dispenser extends Machine {
	
	public Dispenser(String nom) {
		super(nom);
	}
	@Override
	public void doAccio() {
		System.out.println(this.getNom()+" Dispenser - dispensing");
	}
	
}
