package estacionEspacial;

import java.util.ArrayList;

public class colony {
	
	public ArrayList<Room> rooms = new ArrayList<Room>();
	
	public colony() {
		super();
	}

	public ArrayList<Room> getRooms() {
		return rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}

	@Override
	public String toString() {
		return "colony [rooms=" + rooms + "]";
	}
	
	public void addRoom(Room sala) {
		
		if(!this.rooms.contains(sala)) {
			this.rooms.add(sala);
		}
	}
	
	public void removeRoom(Room sala) {
		
		if(this.rooms.contains(sala)) {
			this.rooms.remove(sala);
		}
	}
}
