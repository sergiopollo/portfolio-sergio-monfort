package coso;

public class Gato{
	
	private String name;

	public Gato(String name) {
		super();
		this.name = name;
	}

	public Gato() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Gato [name=" + name + "]";
	}
	
}
