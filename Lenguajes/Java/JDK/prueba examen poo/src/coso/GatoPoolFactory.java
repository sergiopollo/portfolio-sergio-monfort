package coso;

public class GatoPoolFactory implements PoolFactory<Gato>{

	@Override
	public Gato create() {
		Gato gato = new Gato();
		return gato;
	}
	
	//esto no sirve? deberia ponerlo en la Mypool2 para que lo utilizase.
	public Gato create(String nombre) {
		Gato gato = new Gato(nombre);
		return gato;
	}

	@Override
	public void destroy(Gato gato) {
		
		//noseque de garbage collector aqui no hay que poner nada
		
	}

}
