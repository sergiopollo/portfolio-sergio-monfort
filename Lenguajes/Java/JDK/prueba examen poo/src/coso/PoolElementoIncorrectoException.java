package coso;

public class PoolElementoIncorrectoException extends Exception {
	
	public PoolElementoIncorrectoException() {
		super("devuelve un elemento que este en uso y que sea del tipo correcto");
	}
	
}
