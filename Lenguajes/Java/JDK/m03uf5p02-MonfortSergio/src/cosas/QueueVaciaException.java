package cosas;

public class QueueVaciaException extends Exception {
	
	public QueueVaciaException() {
		super("la cola esta vacia, no se puede borrar el primer elemento");
	}
	
}
