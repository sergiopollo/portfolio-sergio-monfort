package cosas;

import java.util.ArrayList;
import java.util.Collections;

public class main {

	public static void main(String[] args) {

		MyStack<Pollo> lista = new MyStack<Pollo>();
		try {
			Pollo p1 = new Pollo("pollete1");
			Pollo p2 = new Pollo("polleta2");
			Pollo p3 = new Pollo("polleto3", 2, 35);
			Pollo p4 = new Pollo("polleto4", 3, 32);
			Pollo p5 = new Pollo("polleto5", 3, 699);

			lista.Push(p1);
			lista.Push(p2);
			lista.Push(p3);
			lista.Push(p4);
			lista.Push(p5);

			System.out.println(lista);

			//Collections.sort(lista);

			//System.out.println(lista);

			for (Pollo pollo : lista) {
				System.out.println(pollo);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Consulta consulta = new Consulta();
		
		Pacient p1 = new Pacient("Sergio", consulta);
		Pacient p2 = new Pacient("Iker", consulta);
		Pacient p3 = new Pacient("Hector", consulta);
		
		consulta.subscribe(p1);
		consulta.subscribe(p2);
		consulta.subscribe(p3);
		
		consulta.notifyObservers("Sergio");
		System.out.println(consulta.getListaObservers());
		consulta.notifyObservers("Iker");
		System.out.println(consulta.getListaObservers());
		consulta.notifyObservers("Hector");
		System.out.println(consulta.getListaObservers());
		
		//HACER MAS PRUEBAS CON EL EVENTMANAGER
		
		
		EventManager manager = new EventManager();
		
		PersonaObserver po1 = new PersonaObserver("Sergio"); 
		PersonaObserver po2 = new PersonaObserver("Iker"); 
		PersonaObserver po3 = new PersonaObserver("Carmen"); 
		//PersonaObserver po4 = new PersonaObserver("Valentina"); 
		//PersonaObserver po5 = new PersonaObserver("Hector"); 
		
		manager.subscribe("cagar", po1);
		manager.subscribe("cagar", po2);
		
		manager.subscribe("comer", po1);
		manager.subscribe("comer", po3);
		
		manager.notifyObservers("cagar");
		manager.notifyObservers("comer");
		manager.notifyObservers("gritar");
		
	}

}
