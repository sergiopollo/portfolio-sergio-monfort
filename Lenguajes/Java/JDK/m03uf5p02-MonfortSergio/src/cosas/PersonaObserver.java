package cosas;

public class PersonaObserver implements Observer{
	
	private String nom;
	
	public PersonaObserver(String nom) {
		this.nom=nom;
	}

	@Override
	public void notifyObserver(String nom) {
			
		System.out.println("soy "+this.nom+" y hago el evento "+nom);
		
	}

	@Override
	public String toString() {
		return "PersonaObserver [nom=" + nom + "]";
	}
	
}
