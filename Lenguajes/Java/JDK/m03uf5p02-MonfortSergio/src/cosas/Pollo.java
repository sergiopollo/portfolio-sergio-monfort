package cosas;

import java.util.Random;

public class Pollo implements Comparable<Pollo>{
	
	private String nombre;
	private int edad;
	private int numPlumas;

	//constructores
	public Pollo()
	{
		Random r = new Random();
		int random1 = r.nextInt(2);
		int random2 = r.nextInt(2);
		this.nombre = "pollete";
		this.edad = random1;
		this.numPlumas=random2;
	}
	
	public Pollo(String nombre)
	{
		Random r = new Random();
		int random1 = r.nextInt(2);
		int random2 = r.nextInt(2);
		this.nombre = nombre;
		this.edad = random1;
		this.numPlumas=random2;
	}

	public Pollo(String nombre, int edad, int numPlumas)
	{
		this.nombre = nombre;
		this.edad =edad;
		this.numPlumas=numPlumas;
	}
	
	@Override
	public String toString() {
		return "Pollo [nombre=" + nombre + ", edad=" + edad + ", numPlumas=" + numPlumas + "]";
	}

	@Override
	public int compareTo(Pollo o) {
		
		/*
		devuelve 0 si son iguales
		devuelve 1 si el mio es mayor
		devuelve -1 si el mio es menor
		*/
		
		int nomMesGran = this.nombre.compareTo(o.nombre);
		
		if(nomMesGran == 0) {
			
			if(this.edad == o.edad) {
				if(this.numPlumas == o.numPlumas) {
					return 0;
				}else if(this.numPlumas > o.numPlumas) {
					return 1;
				}else if(this.numPlumas < o.numPlumas) {
					return -1;
				}
			}else if(this.edad > o.edad) {
				return 1;
			}else if(this.edad < o.edad) {
				return -1;
			}
		}
		
		return nomMesGran;
	}
	
}
