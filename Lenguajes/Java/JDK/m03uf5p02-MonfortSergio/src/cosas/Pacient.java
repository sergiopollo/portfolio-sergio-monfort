package cosas;

public class Pacient implements Observer{

	private String nom;
	private Consulta consulta;
	
	public Pacient(String nom, Consulta consulta) {
		this.nom=nom;
		this.consulta=consulta;
	}

	@Override
	public void notifyObserver(String nom) {
			
		if(this.nom.equals(nom)) {
			System.out.println("soy yo! "+this.nom+ ", me desuscribo");

			consulta.unsubscribe(this);
		}
		
	}

	@Override
	public String toString() {
		return "Pacient [nom=" + nom + "]";
	}
	
	

}
