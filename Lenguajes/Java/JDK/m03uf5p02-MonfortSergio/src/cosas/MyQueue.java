package cosas;

import java.util.ArrayList;
import java.util.Iterator;

public class MyQueue<T> implements Iterable<T>{

private ArrayList<T> lista = new ArrayList<T>();
	
	//constructor
	public MyQueue() {
		
		lista = new ArrayList<T>();
		
	}

	public T Pop() throws QueueVaciaException{

		//si la cola esta vacia que lanze una excepcion
		if(lista.isEmpty()) {
			throw new QueueVaciaException();
		}else {
			
			T cosaABorrar = lista.get(0);
			
			lista.remove(lista.get(0));
			
			return cosaABorrar;
		}
		
	}
	
	public void Push(T element) {

		lista.add(element);

	}

	@Override
	public String toString() {
		return ""+lista;
	}

	@Override
	public Iterator<T> iterator() {
		return lista.iterator();
	}
	
}
