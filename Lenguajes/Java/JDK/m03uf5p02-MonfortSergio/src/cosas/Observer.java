package cosas;

public interface Observer {

	public void notifyObserver(String nom);
	
}
