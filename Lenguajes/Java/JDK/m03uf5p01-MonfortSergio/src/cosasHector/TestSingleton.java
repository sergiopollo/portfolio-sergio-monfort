package cosasHector;

import java.util.ArrayList;

public class TestSingleton
{
	public static void main(String[] args)
	{
		ArrayList<Singleton> singletons = new ArrayList<Singleton>();
		
		//Iniciem els 10 singletons amb getInstance()
		for (int i = 0; i < 10; i++)
		{
			Singleton singleton = Singleton.getInstance();
			singletons.add(singleton);
		}
		
		//Quants singletons tindrem?
		for(Singleton singleton : singletons)
		{
			System.out.println(singleton);
		}
		
		//En principi són tots el mateix objecte
	}
}
