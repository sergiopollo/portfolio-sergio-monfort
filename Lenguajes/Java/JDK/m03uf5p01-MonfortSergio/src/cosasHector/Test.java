package cosasHector;

public class Test
{
	public static void main(String[] args)
	{
		/*
		 * A la que anem a fer servir la classe que necessita d'una
		 * factoria per a funcionar, cal que implementem la factoria
		 * en una classe intermitja.
		 * En el nostre cas definirem una factoria per a ClasseExemple.
		 */
		ClasseExemplePoolFactory factoria = new ClasseExemplePoolFactory();
		/*
		 * Ara passem la factoria com a paràmetre per a que la nostra pool
		 * la pugui utilitzar internament i pugui funcionar.
		 */
		Pool<ClasseExemple> pool = new Pool<ClasseExemple>(factoria);
		
		/*
		 * Mirem que la pool s'hagi creat bé.
		 */
		System.out.println(pool);
		
	}
}
