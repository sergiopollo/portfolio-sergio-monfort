package cosasHector;

public class Singleton
{
	private static Singleton m_singleton = null; 
	
	private Singleton()
	{
		//aqui fariem la inicialització normal de la classe
	}
	
	public static Singleton getInstance()
	{
		if(m_singleton == null)
			m_singleton = new Singleton();
		
		return m_singleton;
	}

}
