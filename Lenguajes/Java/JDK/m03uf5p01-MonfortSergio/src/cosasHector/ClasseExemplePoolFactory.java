package cosasHector;

public class ClasseExemplePoolFactory implements PoolFactory<ClasseExemple>
{

	private static int m_counter = 0;
	@Override
	public ClasseExemple create()
	{
		/*
		 * Aquí és on nosaltres, coneixedors de la classe que es farà
		 * servir, farem la classe factoria per a simplificar les coses.
		 * En el nostre cas anirem canviant el nom dels elements amb una
		 * variable de comptador estàtica.
		 */
		ClasseExemple nouElement = new ClasseExemple("classe " + m_counter);
		m_counter++;
		return nouElement;
	}

	@Override
	public void destroy(ClasseExemple element)
	{
		/*
		 * Aquí fariem el que fes falta per a esborrar
		 * un element de la classe.
		 * Potser no cal fer res perquè java i garbagecollector.
		 */		
	}

}
