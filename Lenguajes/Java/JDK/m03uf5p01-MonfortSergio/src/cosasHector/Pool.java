package cosasHector;

import java.util.ArrayList;

public class Pool<T>
{
	private ArrayList<T> m_list;
	private PoolFactory<T> m_factory;
	
	public Pool(PoolFactory<T> factory)
	{
		m_list = new ArrayList<T>();
		m_factory = factory;
		
		for(int i = 0; i < 10; i++)
		{
			/*
			 * En algun punt del meu codi amb genèrics em trobo
			 * amb la necessitat de fer new del paràmetre genèric T.
			 * En aquest cas, no sé com resoldre new T().
			 * No sempre podré fer-ho perquè potser no existeix el
			 * constructor per defecte sense paràmetres.
			 */
			//T element = new T();

			/*
			 * En aquest cas podem utilitzar una classe intermitja
			 * que serà una factoria que s'encarregarà de la creació
			 * d'objectes del tipus T i li deleguem la responsabilitat.
			 */
			T element = m_factory.create();
			m_list.add(element);
		}
	}
	
	public void emptyPool()
	{
		for(T element: m_list)
		{
			/*
			 * El mateix ens pot passar a l'hora d'esborrar
			 * en cas que els elements necessitin d'algun
			 * codi específic per a alliberar memòria,
			 * trencar referències, etc.
			 */
			m_factory.destroy(element);
		}
	}
	
	public String toString()
	{
		return m_list.toString();
	}
	
	/*
	 * HOLA!
	 * A aquesta Pool li falten TOTS els mètodes interns de gestió
	 * sobre els elements de la pool.
	 * Només s'ha utilitzat el seu nom com a exemple d'on ens podem
	 * trobar amb el problema del new T();
	 */
}
