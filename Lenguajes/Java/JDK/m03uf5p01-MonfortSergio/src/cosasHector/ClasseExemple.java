package cosasHector;

public class ClasseExemple
{
	private String m_name;
	
	/*
	 * La nostra classe no té constructor per defecte
	 * per a que quedi més clar que no sempre pots fer
	 * new NomClasse() i ha de funcionar. A vegades no
	 * tenen constructor sense paràmetres.
	 */
	private ClasseExemple()
	{
		
	}
	
	public ClasseExemple(String name)
	{
		m_name = name;
	}
	
	public String toString()
	{
		return "El meu nom és: " + m_name;
	}
}
