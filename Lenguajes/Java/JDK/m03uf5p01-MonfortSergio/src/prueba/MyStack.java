package prueba;

import java.util.ArrayList;

public class MyStack<T> {

	private ArrayList<T> lista = new ArrayList<T>();
	
	//constructor
	public MyStack() {
		
		lista = new ArrayList<T>();
		
	}

	public T Pop() throws StackVacioException{
		
		//si el stack esta vacio que lanze una excepcion
		if(lista.isEmpty()) {
			throw new StackVacioException();
		}else {
			
			T cosaABorrar = lista.get(lista.size()-1);
			
			lista.remove(lista.get(lista.size()-1));
			
			return cosaABorrar;
		}
	
	}
	
	public void Push(T element) {
	
		lista.add(element);

	}

	@Override
	public String toString() {
		return ""+lista;
	}
	
	
	
}
