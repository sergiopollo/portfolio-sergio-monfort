package prueba;

public class polloPoolFactory implements PoolFactory<pollo>{
	
	private static int contador = 0;
	@Override
	public pollo create()
	{
		pollo pollo = new pollo(""+contador);
		contador++;
		return pollo;
	}

	@Override
	public void destroy(pollo element)
	{
		//aqui nada?	
	}
}
