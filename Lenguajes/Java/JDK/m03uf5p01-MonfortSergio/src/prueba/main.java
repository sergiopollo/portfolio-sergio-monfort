package prueba;

import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MyStack<Integer> prueba = new MyStack<>();
		
		try {
			int ultimo = prueba.Pop();
			
		} catch (StackVacioException e1) {
			e1.printStackTrace();
		}
	
			prueba.Push(1);
			prueba.Push(1);
			prueba.Push(1);
			prueba.Push(3);
			prueba.Push(4);
			prueba.Push(5);

		System.out.println(prueba);
		
		try {
			int ultimo = prueba.Pop();
			
		} catch (StackVacioException e1) {
			e1.printStackTrace();
		}
		
		//System.out.println(ultimo);
		
		System.out.println(prueba);
		
		
		//queue
		MyQueue<String> queue = new MyQueue<>();
		
		try {
			queue.Pop();
			
		} catch (QueueVaciaException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
	
			queue.Push("hola");
			queue.Push("adios");
			queue.Push("help");
			queue.Push("me");
			queue.Push("cuesta");
			queue.Push("entender");


		System.out.println(queue);
		
		String primero;
		try {
			primero = queue.Pop();
			System.out.println(primero);
		} catch (QueueVaciaException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		System.out.println(queue);
		
		//pool de ints
		MyPool pool = new MyPool(2);
		
		//test excepciones
		Object obj2434 = new Object(); 
		try {
			pool.Return(obj2434);
		} catch (PoolElementoIncorrectoException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		
		
		
		System.out.println(pool);
		
		Object obj;
		Object obj1;
		Object obj2;
		
		try {
			 obj = pool.get();
		} catch (PoolVaciaException e) {
			System.out.println(e.getMessage());
			 obj = null;
			
		}
		try {
			 obj1 = pool.get();
		} catch (PoolVaciaException e) {
			System.out.println(e.getMessage());
			 obj1 = null;
			
		}
		try {
			 obj2 = pool.get();
		} catch (PoolVaciaException e) {
			System.out.println(e.getMessage());
			 obj2 = null;
			
		}
		
		System.out.println(obj);
		System.out.println(obj1);
		System.out.println(obj2);
		System.out.println(pool);
		
		try {
			pool.Return(obj);
		} catch (PoolElementoIncorrectoException e1) {

			System.out.println(e1.getMessage());
		}
		System.out.println(pool);
		
		//pool generica (pollos en este caso)
		
		polloPoolFactory factory = new polloPoolFactory();
		
		MyPool2<pollo> pool2 = new MyPool2<pollo>(factory, 2);
		
		System.out.println(pool2);
		ArrayList<pollo> poolElements = new ArrayList<pollo>();
		try {
			for (int i = 0; i < 3; i++) {
				poolElements.add(pool2.get());
			}
		} catch (PoolVaciaException e) {
			e.printStackTrace();
		}
		System.out.println("Mira sergio, est� seguint");
		
		System.out.println(pool2);
		
	}

}
