package prueba;

import java.util.ArrayList;
import java.util.Iterator;


public class MyPool {
	
	//clase interna privada de objetos con un booleano para saber si estan en uso o no
	private class ObjectePool {
		
		public boolean EnUso;
		public Object objectePool;
		
		public ObjectePool(Object objectePool) {
			super();
			this.objectePool = objectePool;
		}

		@Override
		public String toString() {
			return "ObjectePool [objectePool=" + objectePool + "EnUso="+EnUso+"]";
		}
		
	}

	private ArrayList<ObjectePool> lista = new ArrayList<ObjectePool>();
	private int tamanyo;
	
	//constructores
	public MyPool() {
		
		System.out.println("pool default de 10");
		
		this.tamanyo = 10;
		lista = new ArrayList<ObjectePool>();
		
		for (int i = 0; i < this.tamanyo; i++) {
			ObjectePool obP = new ObjectePool(i);
			lista.add(obP);
		}
		
	}
	
	public MyPool(int size) {
		
		this.tamanyo = size;
		lista = new ArrayList<ObjectePool>();
		for (int i = 0; i < this.tamanyo; i++) {
			ObjectePool obP = new ObjectePool(i);
			lista.add(obP);
		}
	}
	
	public Object get() throws PoolVaciaException{
		
		for (int i = 0; i < lista.size(); i++) {
			
			ObjectePool obP = lista.get(i);
			
			if(obP.EnUso == false) {
				
				obP.EnUso = true;
				return obP.objectePool;
			}
			
		}
		//si no encuentra ninguno devuelve null
		//return null;
		
		throw new PoolVaciaException();
	 
	}
	
	public void Return(Object element) throws PoolElementoIncorrectoException{
		
		for (int i = 0; i < lista.size(); i++) {
			
			ObjectePool obP = lista.get(i);
			
			
			if(element.equals(obP.objectePool)) {
				
				obP.EnUso = false;

			}else {
				throw new PoolElementoIncorrectoException();
			}
		
		}
		
	}
	
	@Override
	public String toString() {
		return ""+lista;
	}
}
