package prueba;

import java.util.ArrayList;
import java.util.Iterator;



public class MyPool2<T> {
	
	//clase interna privada de objetos con un booleano para saber si estan en uso o no
	private class ObjectePool {
		
		public boolean EnUso;
		public T objectePool;

		public ObjectePool(T objectePool) {
			super();
			this.objectePool = objectePool;
		}

		@Override
		public String toString() {
			return "ObjectePool [objectePool=" + objectePool + "EnUso="+EnUso+"]";
		}
		
	}

	private ArrayList<ObjectePool> lista = new ArrayList<ObjectePool>();
	private int tamanyo;
	public PoolFactory<T> factory;
	
	//constructores
	public MyPool2(PoolFactory<T> factory) {
		
		System.out.println("pool default de 10");
		
		this.tamanyo = 10;
		lista = new ArrayList<ObjectePool>();
		
		for (int i = 0; i < this.tamanyo; i++) {
			T element = factory.create();
			ObjectePool obP = new ObjectePool(element);
			lista.add(obP);
		}
		
	}
	
	public MyPool2(PoolFactory<T> factory, int size) {
		
		this.tamanyo = size;
		lista = new ArrayList<ObjectePool>();
		for (int i = 0; i < this.tamanyo; i++) {
			T element = factory.create();
			ObjectePool obP = new ObjectePool(element);
			lista.add(obP);
		}
	}
	
	public T get() throws PoolVaciaException{
		
		for (int i = 0; i < lista.size(); i++) {
			
			ObjectePool obP = lista.get(i);
			
			if(obP.EnUso == false) {
				
				obP.EnUso = true;
				return obP.objectePool;
			}
			
		}
		//si no encuentra ninguno devuelve excepcion
		throw new PoolVaciaException();
	 
	}
	
	public void Return(T element) throws PoolElementoIncorrectoException{
		
		for (int i = 0; i < lista.size(); i++) {
			
			ObjectePool obP = lista.get(i);
			
			if(element.equals(obP.objectePool)) {
				
				obP.EnUso = false;

			}
		}
		throw new PoolElementoIncorrectoException();
		
	}
	
	@Override
	public String toString() {
		return ""+lista;
	}
}

