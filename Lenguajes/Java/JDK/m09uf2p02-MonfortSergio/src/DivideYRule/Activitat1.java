package DivideYRule;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Activitat1 {
	
	
	//Activitat 1:
		public static void main(String[] args) {
			
			ArrayList<Integer> llista = new ArrayList<Integer>();
			
			//pool de x threads
			ExecutorService executor = Executors.newFixedThreadPool(10);
		
			Random r = new Random();
			
			for(int i = 0; i<99999999; i++) {
				int numero = r.nextInt();
				llista.add(numero);
			}
			
			long tempsActual = System.nanoTime();
			
			System.out.println("llano el thread");
			Future<ArrayList<Integer>> resultats = executor.submit(new ThreadBuscador(llista));
			executor.shutdown();
			
			try {
				System.out.println("Esperant que acabi el thread.");
				resultats.get();
				long nouTemps = System.nanoTime();
				
				long tempsFinal = nouTemps-tempsActual;
				
				System.out.println("temps utilitzat: "+tempsFinal+", numPetit: "+resultats.get().get(0)+", numGran: "+resultats.get().get(1));
				
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
}
