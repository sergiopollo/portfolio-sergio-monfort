package DivideYRule;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Activitat3 {

	
	//Activitat 3:
		public static void main(String[] args) {
			
			//N
			int numsLista = 1000000;
			
			//Y
			int numThreads = (int) Runtime.getRuntime().availableProcessors();
			//int numThreads = 100;
			
			long tempsActual = 0;
			ArrayList<Integer> llistaNums = new ArrayList<Integer>();
			
			//pool de x threads
			ExecutorService executor = Executors.newFixedThreadPool(numThreads);
		
			Random r = new Random();
			
			
			
			for(int i = 0; i<numsLista; i++) {
				int numero = r.nextInt();
				llistaNums.add(numero);
			}
			
			ArrayList<ArrayList<Integer>> listaListas = new ArrayList<ArrayList<Integer>>();
			
			int reparto = llistaNums.size()/numThreads;
			
			for(int i = 0; i<numThreads; i++) {
				
				ArrayList<Integer> llista1 = new ArrayList<Integer>();
				
				for(int j = reparto*i; j<reparto*(i+1); j++) {
					//System.out.println("thread numero: "+i+" j:"+j);
					llista1.add(llistaNums.get(j));
				}
				
				listaListas.add(llista1);
			}
			
			tempsActual = System.nanoTime();
			
			System.out.println("llamo a los threads");
			
			ArrayList<Future<ArrayList<Integer>>> listaThreads = new ArrayList<Future<ArrayList<Integer>>>();
			
			for (int i = 0; i < numThreads; i++) {
				
				Future<ArrayList<Integer>> resultats1 = executor.submit(new ThreadBuscador(listaListas.get(i)));
				listaThreads.add(resultats1);
			}
			executor.shutdown();
			
			try {
				System.out.println("Esperant que acabi el thread.");
				
				for (int i = 0; i < numThreads; i++) {
					
					listaThreads.get(i).get();
					//System.out.println(listaThreads.get(i).get());
				}
				
				long nouTemps = System.nanoTime();

				long tempsFinal = nouTemps-tempsActual;

				llistaNums = new ArrayList<Integer>();
				
				for (int i = 0; i < numThreads; i++) {

					llistaNums.addAll(listaThreads.get(i).get());
				}
				
				
				System.out.println("busco el mas grande y el mas peque�o EN EL MAIN");
				
				ArrayList<Integer> resultats = new ArrayList<Integer>();
				
				int masPequeno = 0;
				int masGrande = 0;
				
				for(int i=0; i<llistaNums.size(); i++) {
					
					if(llistaNums.get(i) < masPequeno) {
						masPequeno=llistaNums.get(i);
					}
					if(llistaNums.get(i) > masGrande) {
						masGrande=llistaNums.get(i);
					}
					
				}
				
				resultats.add(masPequeno);
				resultats.add(masGrande);
				
				
				System.out.println("temps utilitzat: "+tempsFinal+", numPetit: "+resultats.get(0)+", numGran: "+resultats.get(1));
				
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
		
}
