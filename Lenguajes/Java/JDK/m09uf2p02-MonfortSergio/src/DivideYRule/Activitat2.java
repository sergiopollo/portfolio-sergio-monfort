package DivideYRule;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Activitat2 {

	
	//Activitat 2:
		public static void main(String[] args) {
			
			long tempsActual = 0;
			ArrayList<Integer> llista = new ArrayList<Integer>();
			
			//pool de x threads
			ExecutorService executor = Executors.newFixedThreadPool(10);
		
			Random r = new Random();
			
			for(int i = 0; i<99999999; i++) {
				int numero = r.nextInt();
				llista.add(numero);
			}
			
			int mitad = llista.size()/2;
			
			ArrayList<Integer> llista1 = new ArrayList<Integer>();
			ArrayList<Integer> llista2 = new ArrayList<Integer>();
			
			for (int i = 0; i < llista.size(); i++) {
				if(i < mitad) {
					llista1.add(llista.get(i));
				}else {
					llista2.add(llista.get(i));
				}
			}
			
			tempsActual = System.nanoTime();
			
			System.out.println("llamo a los threads");
			
			Future<ArrayList<Integer>> resultats1 = executor.submit(new ThreadBuscador(llista1));
			Future<ArrayList<Integer>> resultats2 = executor.submit(new ThreadBuscador(llista2));
			executor.shutdown();
			
			try {
				System.out.println("Esperant que acabi el thread.");
				resultats1.get();
				resultats2.get();

				System.out.println(resultats1.get());
				System.out.println(resultats2.get());
				
				long nouTemps = System.nanoTime();

				long tempsFinal = nouTemps-tempsActual;

				llista = new ArrayList<Integer>();
				llista.addAll(resultats1.get());
				llista.addAll(resultats2.get());
				
				System.out.println("busco el mas grande y el mas peque�o EN EL MAIN");
				
				ArrayList<Integer> resultats = new ArrayList<Integer>();
				
				int masPequeno = 0;
				int masGrande = 0;
				
				for(int i=0; i<llista.size(); i++) {
					
					if(llista.get(i) < masPequeno) {
						masPequeno=llista.get(i);
					}
					if(llista.get(i) > masGrande) {
						masGrande=llista.get(i);
					}
					
				}
				
				resultats.add(masPequeno);
				resultats.add(masGrande);
				
				
				System.out.println("temps utilitzat: "+tempsFinal+", numPetit: "+resultats.get(0)+", numGran: "+resultats.get(1));
				
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

		}
		
}
