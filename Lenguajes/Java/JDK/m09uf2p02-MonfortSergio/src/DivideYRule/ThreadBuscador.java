package DivideYRule;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class ThreadBuscador implements Callable<ArrayList<Integer>>
{
	
	private ArrayList<Integer> llista;
	
	public ThreadBuscador(ArrayList<Integer> llista){
		this.llista=llista;
	}
	
	public ArrayList<Integer> call() throws Exception
	{
		
		//System.out.println("busco el mas grande y el mas peque�o");
		
		ArrayList<Integer> resultats = new ArrayList<Integer>();
		
		int masPequeno = 0;
		int masGrande = 0;
		
		for(int i=0; i<llista.size(); i++) {
			
			if(llista.get(i) < masPequeno) {
				masPequeno=llista.get(i);
			}
			if(llista.get(i) > masGrande) {
				masGrande=llista.get(i);
			}
			
		}
		
		resultats.add(masPequeno);
		resultats.add(masGrande);
		
		return resultats;
	}

}

