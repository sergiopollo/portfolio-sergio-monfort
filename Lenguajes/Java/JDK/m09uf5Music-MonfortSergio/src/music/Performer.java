package music;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.sound.midi.Instrument;

public class Performer implements Callable<Note> {

	private Note[] partitura;
	private Conductor conductor;
	private int canal;
	//velocity es lo fuerte o flojo que se "pulsa" la tecla musical
	private int velocity;

	public Performer(Note[] notas, Conductor conductor, int canal, int velocity) {

		this.partitura = notas;
		this.conductor = conductor;
		this.canal = canal;
		this.velocity=velocity;
	}
	
	public Performer(Note[] notas, Conductor conductor, int canal) {

		this.partitura = notas;
		this.conductor = conductor;
		this.canal = canal;
		this.velocity=75;
	}

	@Override
	public Note call() {
		try {

			synchronized (conductor) {

				conductor.wait();

			}

			for (int i = 0; i < partitura.length; i++) {

				MidiPlayer.play(canal, velocity ,partitura[i]);

				for (int j = 0; j < partitura[i].getDuration(); j++) {
					synchronized (conductor) {

						conductor.wait();

					}
				}

				MidiPlayer.stop(canal, partitura[i]);

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("kk me interrumpieron we");
			// e.printStackTrace();
		}
		return null;
	}

}
