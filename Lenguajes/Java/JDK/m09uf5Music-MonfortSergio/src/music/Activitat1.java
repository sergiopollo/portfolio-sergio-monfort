package music;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class Activitat1 {
	
	
	public static void main(String[] args) {
		
		
		
		//pool de x threads
		ExecutorService executor = Executors.newFixedThreadPool(10);
	
		/*
		Note[] notesLead = 
			{
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter),
				new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter),
				
				new Note(Note.Frequency.A3, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter),
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.A5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter),
				new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter)
			};
		*/
		
		Note[] notesLead = 
			{
					
					//Eb, Bb, Ab
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.negra),
				new Note(Note.Frequency.F4, Note.Duration.blanca),
				new Note(Note.Frequency.Eb4, Note.Duration.negra),
				new Note(Note.Frequency.F4, Note.Duration.negra),
				new Note(Note.Frequency.Bb5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.G4, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.C5, Note.Duration.blanca),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.G5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb6, Note.Duration.blanca),
				new Note(Note.Frequency.D6, Note.Duration.negra),
				new Note(Note.Frequency.Bb5, Note.Duration.blanca),
				new Note(Note.Frequency.F5, Note.Duration.negra),
				new Note(Note.Frequency.G5, Note.Duration.blanca+Note.Duration.negra),
				
					
				new Note(Note.Frequency.G5, Note.Duration.blanca),
				new Note(Note.Frequency.Ab6, Note.Duration.corchea),
				new Note(Note.Frequency.Bb6, Note.Duration.corchea),
				new Note(Note.Frequency.Ab6, Note.Duration.blanca),
				new Note(Note.Frequency.G5, Note.Duration.negra),
				new Note(Note.Frequency.F5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.D5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.G5, Note.Duration.blanca+Note.Duration.negra)
				
			};
		
		
		ArrayList<Note> llista = new ArrayList<Note>(Arrays.asList(notesLead));
		
		float tempo = 45*3;
		
		
		//Random r = new Random();
		/*
		for(int i = 0; i<99999999; i++) {
			int numero = r.nextInt();
			//llista.add(numero);
		}
		*/
		//long tempsActual = System.nanoTime();
		
		System.out.println("llamo al thread");
		Future<Note> resultats = executor.submit(new PerformerViejo(llista, tempo));
		executor.shutdown();
		
		try {
			System.out.println("Esperant que acabi el thread.");
			resultats.get();
			
			System.out.println(resultats.get());
			
			//long nouTemps = System.nanoTime();
			
			//long tempsFinal = nouTemps-tempsActual;
			
			//System.out.println("temps utilitzat: "+tempsFinal+", numPetit: "+resultats.get().get(0)+", numGran: "+resultats.get().get(1));
			
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
