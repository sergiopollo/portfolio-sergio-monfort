package music;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sound.midi.Instrument;


public class Activitat2 {
	
	
	public static void main(String[] args) {
		
		
		
		//pool de x threads
		ExecutorService executor = Executors.newFixedThreadPool(10);
	
		/*
		Note[] notesLead = 
			{
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter),
				new Note(Note.Frequency.C6, Note.Duration.quarter),
				new Note(Note.Frequency.G5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.D5, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.G4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.D4, Note.Duration.quarter),
				
				new Note(Note.Frequency.A3, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter),
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.A5, Note.Duration.quarter),
				new Note(Note.Frequency.E5, Note.Duration.quarter),
				new Note(Note.Frequency.C5, Note.Duration.quarter),
				new Note(Note.Frequency.B4, Note.Duration.quarter),
				new Note(Note.Frequency.A4, Note.Duration.quarter),
				new Note(Note.Frequency.E4, Note.Duration.quarter),
				new Note(Note.Frequency.C4, Note.Duration.quarter),
				new Note(Note.Frequency.B3, Note.Duration.quarter)
			};
		*/
		
		
		
		
		
		Note[] arpegio = 
			{
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.C7, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A2, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.C7, Note.Duration.semicorchea),
				new Note(Note.Frequency.G6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.D6, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.G5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.D5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.G4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.D4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.G3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.D3, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A2, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				
				
				new Note(Note.Frequency.A6, Note.Duration.semicorchea),
				new Note(Note.Frequency.E6, Note.Duration.semicorchea),
				new Note(Note.Frequency.C6, Note.Duration.semicorchea),
				new Note(Note.Frequency.B5, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A5, Note.Duration.semicorchea),
				new Note(Note.Frequency.E5, Note.Duration.semicorchea),
				new Note(Note.Frequency.C5, Note.Duration.semicorchea),
				new Note(Note.Frequency.B4, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A4, Note.Duration.semicorchea),
				new Note(Note.Frequency.E4, Note.Duration.semicorchea),
				new Note(Note.Frequency.C4, Note.Duration.semicorchea),
				new Note(Note.Frequency.B3, Note.Duration.semicorchea),
				
				new Note(Note.Frequency.A3, Note.Duration.semicorchea),
				new Note(Note.Frequency.E3, Note.Duration.semicorchea),
				new Note(Note.Frequency.C3, Note.Duration.semicorchea),
				new Note(Note.Frequency.B2, Note.Duration.semicorchea),
			};
		
		Note[] piano =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B5, Note.Duration.blanca),
				
				new Note(Note.Frequency.D6, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				/*1end*/
				
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B5, Note.Duration.blanca),
				
				new Note(Note.Frequency.D6, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.D6, Note.Duration.corchea),
				new Note(Note.Frequency.E6, Note.Duration.corchea),
				
				new Note(Note.Frequency.C6, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.E6, Note.Duration.negra),
				
				new Note(Note.Frequency.D6, Note.Duration.negra),
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				/*2end*/
			};
		
		Note[] piano2 =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B4, Note.Duration.blanca),
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.F5, Note.Duration.negra),
				
				new Note(Note.Frequency.E5, Note.Duration.negra),
				
				new Note(Note.Frequency.D5, Note.Duration.negra),
				
				new Note(Note.Frequency.F5, Note.Duration.negra),
				/*1end*/
				
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.B4, Note.Duration.blanca),
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.D5, Note.Duration.corchea),
				new Note(Note.Frequency.E5, Note.Duration.corchea),
				
				new Note(Note.Frequency.C5, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.C6, Note.Duration.negra),
				
				new Note(Note.Frequency.B5, Note.Duration.negra),
				
				new Note(Note.Frequency.A5, Note.Duration.negra),
				
				new Note(Note.Frequency.G5, Note.Duration.negra),
				/*2end*/
			};
		
		Note[] piano3 =
			{
				//new Note(Note.Frequency.SILENCE, Note.Duration.redonda*12),
				
				
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				new Note(Note.Frequency.F5, Note.Duration.blanca),
				
				/*1*/
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.SILENCE, Note.Duration.redonda),
				/*1end*/
				
				new Note(Note.Frequency.E5, Note.Duration.redonda),
				
				
				new Note(Note.Frequency.D5, Note.Duration.blanca),
				
				new Note(Note.Frequency.F5, Note.Duration.blanca),
				
				/*2*/
				new Note(Note.Frequency.B5, Note.Duration.corchea),
				new Note(Note.Frequency.C6, Note.Duration.corchea),
				
				new Note(Note.Frequency.A5, Note.Duration.blanca+Note.Duration.negra),
				
				
				new Note(Note.Frequency.SILENCE, Note.Duration.redonda)
				/*2end*/
			};
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		///////////////////COSAS NO HECTOR////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		Note[] notesLead = {
					
					//Eb, Bb, Ab
				/*
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.D4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.negra),
				new Note(Note.Frequency.F4, Note.Duration.blanca),
				new Note(Note.Frequency.Eb4, Note.Duration.negra),
				new Note(Note.Frequency.F4, Note.Duration.negra),
				new Note(Note.Frequency.Bb5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.G4, Note.Duration.blanca+Note.Duration.negra),
				*/
				
				new Note(Note.Frequency.C5, Note.Duration.blanca),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.G5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb6, Note.Duration.blanca),
				new Note(Note.Frequency.D6, Note.Duration.negra),
				new Note(Note.Frequency.Bb5, Note.Duration.blanca),
				new Note(Note.Frequency.F5, Note.Duration.negra),
				new Note(Note.Frequency.G5, Note.Duration.blanca+Note.Duration.negra),
				
					
				new Note(Note.Frequency.G5, Note.Duration.blanca),
				new Note(Note.Frequency.Ab6, Note.Duration.corchea),
				new Note(Note.Frequency.Bb6, Note.Duration.corchea),
				new Note(Note.Frequency.Ab6, Note.Duration.blanca),
				new Note(Note.Frequency.G5, Note.Duration.negra),
				new Note(Note.Frequency.F5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.D5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.G5, Note.Duration.blanca+Note.Duration.negra)
				
			};
		
		Note[] notes2 = {
			
		};
		
		Note[] notes3 = {
				//Fb (efe-b), Gb, Cb
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.E5, Note.Duration.negra),
				new Note(Note.Frequency.E5, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.A6, Note.Duration.negra),
				new Note(Note.Frequency.A6, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.D5, Note.Duration.negra),
				new Note(Note.Frequency.D5, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.B6, Note.Duration.negra),
				new Note(Note.Frequency.B6, Note.Duration.negra),
				
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.E5, Note.Duration.negra),
				new Note(Note.Frequency.E5, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.A6, Note.Duration.negra),
				new Note(Note.Frequency.A6, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.D5, Note.Duration.negra),
				new Note(Note.Frequency.D5, Note.Duration.negra),
				new Note(Note.Frequency.SILENCE, Note.Duration.negra),
				new Note(Note.Frequency.B6, Note.Duration.negra),
				new Note(Note.Frequency.B6, Note.Duration.negra),
				
				
				
		};
		
		Note[] queen1 = {
				
				//tempo: 100
				//Gb, Ab, Bb, Db, Eb,
				
				//seccion 1
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				//nota sin bemoll
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.redonda),
				
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.redonda),
				

				////////////////
				//seccion 2:
				////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				//con bemoll
				new Note(Note.Frequency.Cb5, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb6, Note.Duration.negra),
				new Note(Note.Frequency.Db6, Note.Duration.negra),
				
				new Note(Note.Frequency.Db6, Note.Duration.blanca),
				new Note(Note.Frequency.C6, Note.Duration.negra),
				new Note(Note.Frequency.Db6, Note.Duration.negra),
				
				new Note(Note.Frequency.C6, Note.Duration.redonda),
				//NOTA RARA
				//new Note(Note.Frequency.F5, Note.Duration.corchea),
				
				/////////////////
				//seccion 3
				/////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				//nota sin bemoll
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F5, Note.Duration.redonda),
				
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab6, Note.Duration.corchea),
				new Note(Note.Frequency.C6, Note.Duration.corchea),
				new Note(Note.Frequency.Db6, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F5, Note.Duration.redonda),
				
				////////////////////
				//seccion 4
				///////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.Gb5, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				//con bemoll
				new Note(Note.Frequency.Cb5, Note.Duration.blanca),
				
				new Note(Note.Frequency.SILENCE, Note.Duration.corchea),
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.Eb5, Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb4, Note.Duration.negra),
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				
				new Note(Note.Frequency.Db5, Note.Duration.blanca),
				new Note(Note.Frequency.C5, Note.Duration.negra),
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.C5, Note.Duration.redonda),
				
				//////////////////////
				//seccion 5
				//////////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.Db5, Note.Duration.redonda),
				
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.C5, Note.Duration.negra),
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.Eb5, Note.Duration.negra),
				
				new Note(Note.Frequency.Db5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.semicorchea),
				new Note(Note.Frequency.Bb5, Note.Duration.negra),
				new Note(Note.Frequency.Bb5, Note.Duration.negra),
				
				new Note(Note.Frequency.SILENCE, Note.Duration.blanca),
				new Note(Note.Frequency.C5, Note.Duration.blanca),
				
				//////////////////////
				//seccion 6
				//////////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.Db5, Note.Duration.redonda),
				
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.Eb5, Note.Duration.negra),
				new Note(Note.Frequency.Gb5, Note.Duration.negra),
				
				new Note(Note.Frequency.F5, Note.Duration.negra+Note.Duration.corchea),
				new Note(Note.Frequency.Db5, Note.Duration.semicorchea),
				new Note(Note.Frequency.Db5, Note.Duration.negra),
				new Note(Note.Frequency.Bb6, Note.Duration.negra),
				
				new Note(Note.Frequency.Bb6, Note.Duration.negra),
				new Note(Note.Frequency.Ab6, Note.Duration.negra),
				new Note(Note.Frequency.F5, Note.Duration.negra),
				new Note(Note.Frequency.Eb5, Note.Duration.negra),
				
				
		};
		
		Note[] queen2 = {
				//tempo: 100
				//Gb, Ab, Bb, Db, Eb,
				
				//seccion 1
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),

				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),

				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				
				
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Db4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Db4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				
				////////////////////////
				//seccion 2
				///////////////////////
				
				//Gb, Ab, Bb, Db, Eb,
				//con bemoll
				new Note(Note.Frequency.Cb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Db4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				//sin bemoll
				new Note(Note.Frequency.E3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.G3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Eb3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				new Note(Note.Frequency.F3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.A4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				//NOTA RARA
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				
				////////////////////
				//seccion 3
				///////////////////
				//Gb, Ab, Bb, Db, Eb,
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),

				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				//NOTA RARA
				//new Note(Note.Frequency.F4, Note.Duration.redonda),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.A5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.C5, Note.Duration.corchea),
				
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				//NOTA RARA
				//new Note(Note.Frequency.Gb4, Note.Duration.blanca),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Bb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				//NOTA RARA
				//new Note(Note.Frequency.F4, Note.Duration.blanca),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Db4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				new Note(Note.Frequency.Db4, Note.Duration.corchea),
				new Note(Note.Frequency.Ab5, Note.Duration.corchea),
				
				//////////////////////
				//seccion 4
				/////////////////////
				//Gb, Ab, Bb, Db, Eb,
				//con bemoll
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb4, Note.Duration.corchea),
				//con bemoll
				new Note(Note.Frequency.Cb5, Note.Duration.corchea),
				
				new Note(Note.Frequency.Ab4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Ab4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				new Note(Note.Frequency.Eb4, Note.Duration.corchea),
				new Note(Note.Frequency.F4, Note.Duration.corchea),
				
				//sin bemoll
				new Note(Note.Frequency.E3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				//sin bemoll
				new Note(Note.Frequency.G3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				new Note(Note.Frequency.Eb3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				new Note(Note.Frequency.Gb3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				new Note(Note.Frequency.F3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				new Note(Note.Frequency.F3, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				new Note(Note.Frequency.Bb4, Note.Duration.corchea),
				new Note(Note.Frequency.C4, Note.Duration.corchea),
				
				
		};
		
		
		//ArrayList<Note> llista = new ArrayList<Note>(Arrays.asList(notesLead));
		//ArrayList<Note> llista2 = new ArrayList<Note>(Arrays.asList(notes2));
		
		Instrument[] instruments = MidiPlayer.getInstruments();
		
		MidiPlayer.setInstrument(instruments[46], 0);
		MidiPlayer.setInstrument(instruments[1], 4);
		MidiPlayer.setInstrument(instruments[6], 2);
		
		
		float tempo=100;
		
		//float tempo = 45*3;
		
		ArrayList<Future<Note>> futuros = new ArrayList<Future<Note>>();
		
		
		
		Conductor conductor = new Conductor(tempo);
		
		System.out.println("llamo al thread");
		//cancion 1: tempo 45*3
		//futuros.add(executor.submit(new Performer(notesLead, conductor,4)));
		//futuros.add(executor.submit(new Performer(notes2, conductor,4)));
		//futuros.add(executor.submit(new Performer(notes3, conductor,4, 50)));
		
		//cancion 2: tempo:100
		futuros.add(executor.submit(new Performer(queen1, conductor,2, 65)));
		futuros.add(executor.submit(new Performer(queen2, conductor,2, 40)));
		
		
		
		try {
			Thread.sleep(50);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		executor.submit(conductor);
		executor.shutdown();
		
		try {
			System.out.println("Esperant que acabi el thread.");
			
			for (Future<Note> future : futuros) {
				
				future.get();
				
			}
			
			executor.shutdownNow();
			executor.awaitTermination(1, TimeUnit.SECONDS);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
