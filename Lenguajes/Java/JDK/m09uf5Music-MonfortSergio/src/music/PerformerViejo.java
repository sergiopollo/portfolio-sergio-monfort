package music;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.sound.midi.Instrument;

public class PerformerViejo implements Callable<Note>{

	private ArrayList<Note> partitura;
	private float tempo;
	private long tempoMilliseconds;
	
	public PerformerViejo(ArrayList<Note> notas, float tempo ) {
		
		this.partitura=notas;
		this.tempo=tempo;
		this.tempoMilliseconds=(long) (60000/tempo/16);
	}
	
	@Override
	public Note call() throws Exception {
		
		try
		{
			/*
			Instrument[] instruments = MidiPlayer.getInstruments();

			MidiPlayer.setInstrument(instruments[103], 1);
			*/
			for(int i = 0; i < partitura.size(); i++)
			{
				//MidiPlayer.setInstrument(instruments[0]);
				MidiPlayer.play(0, partitura.get(i));
				//MidiPlayer.play(1, notesRythm[i]);
				Thread.sleep(tempoMilliseconds*partitura.get(i).getDuration());
				
				//MidiPlayer.stop(0, notesLead[i]);
				MidiPlayer.stop(1, partitura.get(i));
			}
			
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}	
		
		return null;
	}

}
