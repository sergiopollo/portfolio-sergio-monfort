package music;

import java.util.concurrent.Callable;

public class Conductor implements Callable<Note>{

	private float tempo;
	private long tempoMilliseconds;
	
	public Conductor(float tempo){
		this.tempo=tempo;
		this.tempoMilliseconds=(60000/(int)tempo/16);
		System.out.println(tempoMilliseconds);
	}
	
	
	@Override
	public Note call() {
		
		try {
			
			while(true) {
				
				Thread.sleep(tempoMilliseconds);
				synchronized(this){
					this.notifyAll();
				}
				
				
			}
			
			
		}catch(InterruptedException e)
		{
			System.out.println("orquesta finalizada. fino se�ores");
			//e.printStackTrace();
		}
		
		
		return null;
	}


	
	
	
}
