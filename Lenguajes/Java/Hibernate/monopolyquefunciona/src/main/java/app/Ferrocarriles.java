package app;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import com.fasterxml.jackson.annotation.JsonBackReference;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

@Entity
@DiscriminatorValue("ferrocarril")
public class Ferrocarriles extends Propiedades{

	
	public Ferrocarriles(){
		super();
		this.misVecinos = new HashSet<Ferrocarriles>();
		this.vecinoDe = new HashSet<Ferrocarriles>();
	}
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "ferrocarril_vecino", 
        joinColumns = { @JoinColumn(name = "ferrocarril_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "vecino_id") }
    )
	private Set<Ferrocarriles> misVecinos;
	@JsonBackReference
	public Set<Ferrocarriles> getMisVecinos() {
		return misVecinos;
	}



	@ManyToMany(mappedBy = "misVecinos")
	@JsonBackReference
	private Set<Ferrocarriles> vecinoDe;
	
	public Set<Ferrocarriles> getVecinoDe() {
		return vecinoDe;
	}
	
	public void addVecino(Ferrocarriles f) {
		
		vecinoDe.add(f);
		misVecinos.add(f);
		
	}
	
	
	
}
