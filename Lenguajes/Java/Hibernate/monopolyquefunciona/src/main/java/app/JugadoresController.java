package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.aspectj.apache.bcel.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;




@Controller    // This means that this class is a Controller
@RequestMapping(path="/jugadores") // This means URL's start with /demo (after Application path)
public class JugadoresController {
	@Autowired // This means to get the bean called userRepository
	           // Which is auto-generated by Spring, we will use it to handle the data
	private JugadoresRepository repository;
	@Autowired private PropiedadesRepository propiedadesRepository;
	@Autowired private ColoresRepository colorRepository;
	
	
	
	@GetMapping(path="/modificarDiners/{idJug}/{diners}") // Map ONLY GET Requests
	public @ResponseBody int modificarDiners (@PathVariable Integer idJug, @PathVariable Integer diners) {
		Jugadores j =  repository.findById(idJug).get();
		int dineroactual = j.getDinero();
		dineroactual += diners;
		j.setDinero(dineroactual);
		repository.save(j);
		return dineroactual;
	}
	
	
	@GetMapping(path="/Reasignar") // Map ONLY GET Requests
	public @ResponseBody Jugadores Reasignar (){

		ArrayList<Jugadores> jugadores = (ArrayList<Jugadores>) repository.findAll();
		
		for(int i = jugadores.size()-1; i >= 0; i--) {
			if(jugadores.get(i).getDinero() <= 0) {
				repository.delete(jugadores.get(i));
			}
		}
				
		if(jugadores.size() == 1) {
			return jugadores.get(0);
		}
		
		return null;

	}
	
	
	@GetMapping(path="/addVacio") // Map ONLY GET Requests
	public @ResponseBody String addNewJugador () {

		Jugadores m = new Jugadores();
		
		repository.save(m);
		return "<h1>Saved</h1>";
	}
	
	
	@GetMapping(path="/add") // Map ONLY GET Requests
	public @ResponseBody String addNewAlmohadaDeEjemplo (@RequestParam String t
			, @RequestParam String dib) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		
		Jugadores m = new Jugadores();
		
		repository.save(m);
		return "Saved";
	}
	
	
	
	@GetMapping(path="/pagarLloguer/{idJ}/{idP}") // Map ONLY GET Requests
	public @ResponseBody boolean pagarLloguer (@PathVariable Integer idJ,@PathVariable Integer idP) {
		Jugadores j =  repository.findById(idJ).get();
		Propiedades p = propiedadesRepository.findById(idP).get();
		
		//si no tiene propietario devuelve false, aunque si no tiene propietario no deberia llamarse a esta funcion
		int casas = p.getNumCasas();
		int dineroAPagar = 0;

		switch (casas) {
		case 0:
			dineroAPagar = p.getAlquiler0();
			break;
		case 1:
			dineroAPagar = p.getAlquiler1();
			break;
		case 2:
			dineroAPagar = p.getAlquiler2();
			break;
		case 3:
			dineroAPagar = p.getAlquiler3();
			break;
		case 4:
			dineroAPagar = p.getAlquiler4();
			break;
		case 5:
			dineroAPagar = p.getAlquiler5();
			break;

		}

		Jugadores propietario = p.getPropietario();
		if (propietario == null) {
			return false;
		} else {
			// si el jugador no tiene dinero suficiente para pagar el alquiler se queda con
			// deuda
			modificarDiners(j.getId(), dineroAPagar * -1);
			modificarDiners(propietario.getId(), dineroAPagar);
			//no hacen falta saveorupdate porque modificardiners ya hace el saveorupdate
			//this.saveOrUpdate(propietario);
			//this.saveOrUpdate(j);
			return true;
		}

	}
	
	@GetMapping(path="/hipotecar/{idJ}/{idP}") // Map ONLY GET Requests
	public @ResponseBody boolean hipotecar (@PathVariable Integer idJ,@PathVariable Integer idP) {
		
		Jugadores j =  repository.findById(idJ).get();
		Propiedades p = propiedadesRepository.findById(idP).get();
		
		if(p.getPropietario().equals(j)) {
			if(p.isHipotecado()) {
				return false;
			}else {
				p.setHipotecado(true);
				int dineroAPagar = p.getPrecioHipoteca();
				//no hace falta saveorupdate de jugador porque modificarDiners ya lo hace
				modificarDiners(idJ, dineroAPagar);
				return true;
			}
		}else {
			return false;
		}
	}
	
	
	@GetMapping(path="/deshipotecar/{idJ}/{idP}") // Map ONLY GET Requests
	public @ResponseBody String deshipotecar (@PathVariable Integer idJ,@PathVariable Integer idP) {
		
		Jugadores j =  repository.findById(idJ).get();
		Propiedades p = propiedadesRepository.findById(idP).get();
		
		
		if(j.getDinero() < p.getPrecioHipoteca() || p.getPropietario() != j || !p.isHipotecado()) {
			return "<h1>deshipotecar</h1><br><br><p>No se a podido deshipotecar la propiedad " + p.getNombre() + "</p>";
		}
		else{
			p.setHipotecado(false);
			modificarDiners(idJ, p.getPrecioHipoteca()*-1);
			
			repository.save(j);
			propiedadesRepository.save(p);
			return "<h1>deshipotecar</h1><br><br><p>Se a podido deshipotecar la propiedad " + p.getNombre() + "</p>";
		}
	}
	
	
	// esto tiene que estar aqui porque da null si lo llamas desde el PropiedadesController
	@GetMapping(path="/comprovarColor/{id}")
	public @ResponseBody boolean ComporovarColor(@PathVariable Integer id) {	
		
		Propiedades p = propiedadesRepository.findById(id).get();
		
		Colores c = p.getColor();
		if(c == null) {
			return false;
		}
		
		Jugadores j = p.getPropietario();
		boolean flag = true;
		for (Propiedades prop : c.getPropiedad()) {
			if(prop.getPropietario()==null || !prop.getPropietario().equals(j)) {
				flag=false;
			}
		}
		return flag;
	}
	
	
	@GetMapping(path="/Comprar/{idJ}/{idP}") // Map ONLY GET Requests
	public @ResponseBody String Comprar (@PathVariable Integer idJ,@PathVariable Integer idP) {
		
		Jugadores j =  repository.findById(idJ).get();
		Propiedades p = propiedadesRepository.findById(idP).get();
		
		
			if (p.getPropietario() == null && j.getDinero() > p.getPrecio()) {
			
				if(p instanceof Ferrocarriles) { 
					
					j.addPropiedad(p);
					modificarDiners(idJ, -p.getPrecio());
					j.setNumPropiedades(j.getNumPropiedades() + 1);
					p.setPropietario(j);
					
				
					int contadorFerrocarrilesJugador = 0;
					for (Propiedades propiedad : j.getPropiedades()) {
						if(propiedad instanceof Ferrocarriles) {
							contadorFerrocarrilesJugador++;
						}
					}
				
					for (Propiedades propiedad : j.getPropiedades()) {
						if(propiedad instanceof Ferrocarriles) {
							propiedad.setNumCasas(contadorFerrocarrilesJugador);
							propiedadesRepository.save(propiedad);
						}
					}
					
				}
				else {
					j.addPropiedad(p);
					modificarDiners(idJ, -p.getPrecio());
					j.setNumPropiedades(j.getNumPropiedades() + 1);
					p.setPropietario(j);
	
					if(ComporovarColor(p.getId())){
						p.getColor().setPropietarioColor(j);
						colorRepository.save(p.getColor());
					}
					
					
					propiedadesRepository.save(p); 
				}
			
			
			repository.save(j);
			return "<h1>COMPRAR</h1><br><br><p>Se a podido comprar la propiedad " + p.getNombre() + "</p><p>:D</p>";
		}
			return "<h1>COMPRAR</h1><br><br><p>No se a podido comprar la propiedad " + p.getNombre() + "</p><br><p>:(</p>";
	}
	

	
	@GetMapping(path="/edificar/{idJ}/{idP}") // Map ONLY GET Requests
	public @ResponseBody String edificar (@PathVariable Integer idJ,@PathVariable Integer idP) {
		
		Jugadores j =  repository.findById(idJ).get();
		Propiedades p = propiedadesRepository.findById(idP).get();
		

				
		if(p.getPropietario() == j && p.getPrecioCasa() < j.getDinero()) {
			
			
			if(ComporovarColor(p.getId())) {
				p.setNumCasas(p.getNumCasas() + 1);
				modificarDiners(idJ, p.getPrecioCasa()*-1);
				p.getColor().setPropietarioColor(j);
				
				repository.save(j);
				propiedadesRepository.save(p);
				colorRepository.save(p.getColor());
				
			}
			
		}
			
		if(p.getNumCasas() == 0) {
			return "<h1>EDIFICAR</h1><br><br><p>No se a contruido nada</p>";
		}
		return "<h1>EDIFICAR</h1><br><br> <p> actualmente la casilla " + p.getNombre() + " tiene -->" +p.getNumCasas() + "</p>";
		
	}
	

	/*
	@GetMapping(path="/alm/{id}")
	public @ResponseBody Jugadores getUser(@PathVariable int id) {
		Optional<Jugadores> user = repository.findById(id);
		if(user.isPresent()) {
			return user.get();
		}else {
			return null;
		}
		
	}
	*/
	/*
	@GetMapping(path="/tipo/{tipo}")
	public @ResponseBody Iterable<Jugadores> getJugadorByTipo(@PathVariable asd iddasdad) {
		return repository.findBycosaquenose(iasd);

	}
	 */
	

	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Jugadores> getAllUsers() {
		// This returns a JSON or XML with the users
		return repository.findAll();
	}
	
	
	// testeo
	@GetMapping(path="/dust/{idJ}/{diners}")
	public @ResponseBody String dust(@PathVariable Integer idJ,  @PathVariable Integer diners) {
		// This returns a JSON or XML with the users
		return "me voy a hacer dust " + idJ + diners;
	}
}
