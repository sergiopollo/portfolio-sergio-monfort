package app;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "jugadores")
public class Jugadores {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_jugador")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="activo")
	private boolean activo;
	
	@Column(name="turno")
	private int turno;
	
	@Column(name="dinero")
	private int dinero;
	
	@Column(name="num_propiedades")
	private int numPropiedades;
	
	@Column(name="num_victorias")
	private int numVictorias;
	
	
	@Column(name="posicion_tablero")
	private int posicionTablero;

	@OneToMany(mappedBy= "propietario")
	private Set<Propiedades> propiedades;
	
	public Set<Propiedades> getPropiedades() { 
		return propiedades;
	}

	public void setPropiedades(Set<Propiedades> propiedades) {
		this.propiedades = propiedades;
	}
	
	//OneToMany. Aqui diem que volem el set (i per tant la taula), i quina variable ens far referencia, es a dir, quina variable representa la clau foranea.
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
	@OneToMany(mappedBy= "propietarioColor")
	private Set<Colores> colores;
	
	public Set<Colores> getColores() {
		return colores;
	}

	public void setColores(Set<Colores> colores) {
		this.colores = colores;
	}

	public Jugadores() {
		super();
		this.propiedades=new HashSet<Propiedades>();
	}

	public Jugadores(int id, String nombre, boolean activo, int turno, int dinero, int numPropiedades,
			int numVictorias) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activo = activo;
		this.turno = turno;
		this.dinero = dinero;
		this.numPropiedades = numPropiedades;
		this.numVictorias = numVictorias;
		this.propiedades=new HashSet<Propiedades>();
	}

	public int getId() {
		return id;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public int getTurno() {
		return turno;
	}

	public void setTurno(int turno) {
		this.turno = turno;
	}

	public int getDinero() {
		return dinero;
	}

	public void setDinero(int dinero) {
		this.dinero = dinero;
	}

	public int getNumPropiedades() {
		return numPropiedades;
	}

	public void setNumPropiedades(int numPropiedades) {
		this.numPropiedades = numPropiedades;
	}

	public int getNumVictorias() {
		return numVictorias;
	}

	public void setNumVictorias(int numVictorias) {
		this.numVictorias = numVictorias;
	}
	
	public void addPropiedad(Propiedades p) {
		this.propiedades.add(p);
	}
	
	public void addColor(Colores c) {
		this.colores.add(c);
	}
	
	
	public int getPosicionTablero() {
		return posicionTablero;
	}

	public void setPosicionTablero(int posicionTablero) {
		this.posicionTablero = posicionTablero;
	}

	@Override
	public String toString() {
		return "Jugadores [id=" + id + ", nombre=" + nombre + ", activo=" + activo + ", turno=" + turno + ", dinero="
				+ dinero + ", numPropiedades=" + numPropiedades + ", numVictorias=" + numVictorias + "]";
	}
	
	

	
}
