package app;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import com.fasterxml.jackson.annotation.JsonBackReference;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("calle")
@Table(name = "propiedades")
public class Propiedades implements Comparable<Propiedades>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_propiedad")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="precio")
	private int precio;
	
	@Column(name="num_casas")
	private int numCasas;
	
	@Column(name="precio_casa")
	private int precioCasa;
	
	@Column(name="posicion")
	private int posicion;
	
	@Column(name="hipotecado")
	private boolean hipotecado;
	
	@Column(name="precio_hipoteca")
	private int precioHipoteca;
	
	@Column(name="alquiler0")
	private int alquiler0;
	
	@Column(name="alquiler1")
	private int alquiler1;
	
	@Column(name="alquiler2")
	private int alquiler2;
	
	@Column(name="alquiler3")
	private int alquiler3;
	
	@Column(name="alquiler4")
	private int alquiler4;
	
	@Column(name="alquiler5")
	private int alquiler5;
	
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
	@ManyToOne
	@JoinColumn(name="id_jugador", nullable=true)
	@JsonBackReference
	private Jugadores propietario;
	
	public Jugadores getPropietario() {
		return propietario;
	}

	public void setPropietario(Jugadores propietario) {
		this.propietario = propietario;
	}
	
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
		@ManyToOne
		@JoinColumn(name="id_color", nullable=true)
		@JsonBackReference
		private Colores color;
		
		public Colores getColor() {
			return color;
		}

		public void setColor(Colores color) {
			this.color = color;
		}

	public Propiedades() {
		super();
	}

	public Propiedades(int id, String nombre, int precio, int numCasas, int precioCasa, int posicion,
			boolean hipotecado, int precioHipoteca, int alquiler0, int alquiler1, int alquiler2, int alquiler3,
			int alquiler4, int alquiler5) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.numCasas = numCasas;
		this.precioCasa = precioCasa;
		this.posicion = posicion;
		this.hipotecado = hipotecado;
		this.precioHipoteca = precioHipoteca;
		this.alquiler0 = alquiler0;
		this.alquiler1 = alquiler1;
		this.alquiler2 = alquiler2;
		this.alquiler3 = alquiler3;
		this.alquiler4 = alquiler4;
		this.alquiler5 = alquiler5;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getNumCasas() {
		return numCasas;
	}

	public void setNumCasas(int numCasas) {
		this.numCasas = numCasas;
	}

	public int getPrecioCasa() {
		return precioCasa;
	}

	public void setPrecioCasa(int precioCasa) {
		this.precioCasa = precioCasa;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public boolean isHipotecado() {
		return hipotecado;
	}

	public void setHipotecado(boolean hipotecado) {
		this.hipotecado = hipotecado;
	}

	public int getPrecioHipoteca() {
		return precioHipoteca;
	}

	public void setPrecioHipoteca(int precioHipoteca) {
		this.precioHipoteca = precioHipoteca;
	}

	public int getAlquiler0() {
		return alquiler0;
	}

	public void setAlquiler0(int alquiler0) {
		this.alquiler0 = alquiler0;
	}

	public int getAlquiler1() {
		return alquiler1;
	}

	public void setAlquiler1(int alquiler1) {
		this.alquiler1 = alquiler1;
	}

	public int getAlquiler2() {
		return alquiler2;
	}

	public void setAlquiler2(int alquiler2) {
		this.alquiler2 = alquiler2;
	}

	public int getAlquiler3() {
		return alquiler3;
	}

	public void setAlquiler3(int alquiler3) {
		this.alquiler3 = alquiler3;
	}

	public int getAlquiler4() {
		return alquiler4;
	}

	public void setAlquiler4(int alquiler4) {
		this.alquiler4 = alquiler4;
	}

	public int getAlquiler5() {
		return alquiler5;
	}

	public void setAlquiler5(int alquiler5) {
		this.alquiler5 = alquiler5;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Propiedades [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", numCasas=" + numCasas
				+ ", precioCasa=" + precioCasa + ", posicion=" + posicion + ", hipotecado=" + hipotecado
				+ ", precioHipoteca=" + precioHipoteca + ", alquiler0=" + alquiler0 + ", alquiler1=" + alquiler1
				+ ", alquiler2=" + alquiler2 + ", alquiler3=" + alquiler3 + ", alquiler4=" + alquiler4 + ", alquiler5="
				+ alquiler5 + "]";
	}

	@Override
	public int compareTo(Propiedades o) {
		//devuelvo 1 si soy mas grande
		//devuelvo 0 si soy igual
		//devuelvo -1 si soy mas peque�o
		if(o.getPosicion() < this.getPosicion()) {
			return 1;
		}else if(o.getPosicion() > this.getPosicion()) {
			return -1;
		}else {
			return 0;
		}
	}
	
	
	
}
