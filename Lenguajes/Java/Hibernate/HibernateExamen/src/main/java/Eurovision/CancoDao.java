package Eurovision;

public class CancoDao extends GenericDao<Canco, Integer> implements ICancoDao{

	@Override
	public void canviAlbum(int id, String album) {
		
		Canco canco = this.get(id);
		
		canco.setAlbum(album);
		
		this.saveOrUpdate(canco);
		
	}
	
	
	
}
