package Eurovision;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "canco")
public class Canco {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_canco", nullable=false)
	private int id;
	
	@Column(name="nom", nullable=true)
	private String nombre;
	
	@Column(name="any_publicacio", nullable=true)
	private int anyPublicacio;
	
	@Column(name="album", nullable=true, length=20)
	private String album;
	
	@Column(name="estil", nullable=true)
	private Estil estil;
	
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "canco_cantant", 
        joinColumns = { @JoinColumn(name = "id_canco") }, 
        inverseJoinColumns = { @JoinColumn(name = "id_cantant") }
    )
	private Set<Cantant> cantants;
	
	public Set<Cantant> getCantants() {
		return cantants;
	}

	public void setCantants(Set<Cantant> cantants) {
		this.cantants = cantants;
	}
	
	public void addCantant(Cantant cantant) {
		this.cantants.add(cantant);
	}
	
	public Canco() {
		super();
		this.cantants = new HashSet<Cantant>();
	}

	public Canco(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cantants = new HashSet<Cantant>();
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getAnyPublicacio() {
		return anyPublicacio;
	}

	public void setAnyPublicacio(int anyPublicacio) {
		this.anyPublicacio = anyPublicacio;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public Estil getEstil() {
		return estil;
	}

	public void setEstil(Estil estil) {
		this.estil = estil;
	}

	@Override
	public String toString() {
		return "Canco [id=" + id + ", nombre=" + nombre + ", anyPublicacio=" + anyPublicacio + ", album=" + album
				+ ", estil=" + estil + ", cantants=" + cantants + "]";
	}

}
