package Eurovision;


public interface ICancoDao extends IGenericDao<Canco, Integer>{

	public void canviAlbum(int id, String album);
	
}
