package Eurovision;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.build.AllowSysOut;
import org.hibernate.service.ServiceRegistry;

public class main { 
	
	static SessionFactory sessionFactory;
	static Session session;
	
	static CancoDao CCD;
	static CantantDao CTD;
	
	
	
	
	public static void main(String[] args) {
		sessionFactory = Utils.getSessionFactory();
		session = sessionFactory.getCurrentSession();

		// genero los DAO
		// recordar poner en todos los DAO's los save or update
		CCD = new CancoDao();
		CTD = new CantantDao();

		
		
		Cantant ct1 = new Cantant();
		
		ct1.setSobrenom("sergio");
		
		Cantant ct2 = new Cantant();
		
		ct2.setSobrenom("marc");
		
		Canco c1 = new Canco();
		c1.setNombre("cancion 1");
		c1.setEstil(Estil.Hip_Hop);
		
		Canco c2 = new Canco();
		
		c2.setNombre("cancion 2");
		
		Canco c3 = new Canco();
		
		c3.setNombre("cancion 3");
		
		c1.addCantant(ct1);
		ct1.addCanco(c1);
		
		c2.addCantant(ct1);
		ct1.addCanco(c2);
		
		c1.addCantant(ct2);
		ct2.addCanco(c1);
		
		c3.addCantant(ct2);
		ct2.addCanco(c3);

		CTD.saveOrUpdate(ct1);
		CCD.saveOrUpdate(c1);
		CTD.saveOrUpdate(ct2);
		CCD.saveOrUpdate(c2);
		CCD.saveOrUpdate(c3);
		
		//ArrayList<Cantant> cantants = ct1.getCantantsAmbMes1Cancons();
		
		CCD.canviAlbum(1, "superAlbum");
		
		//System.out.println(cantants);
		
	}

	/*
	 * chuleta sql
	 * 
	 * create database sergioexamen; use sergioexamen;
	 * 
	 * 
	 */
}
