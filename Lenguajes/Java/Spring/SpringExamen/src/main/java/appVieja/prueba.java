package appVieja;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "prueba")
public class prueba {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_prueba")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
	@ManyToOne
	@JoinColumn(name="id_persona", nullable=true)
	@JsonBackReference
	private persona propietarioPrueba;

	public persona getPropietarioPrueba() {
		return propietarioPrueba;
	}

	public void setPropietarioPrueba(persona propietarioPrueba) {
		this.propietarioPrueba = propietarioPrueba;
	}

	public prueba() {
		super();
	}

	public prueba(int id, String nombre, persona propietarioPrueba) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.propietarioPrueba = propietarioPrueba;
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	

	@Override
	public String toString() {
		return "prueba [id=" + id + ", nombre=" + nombre + ", propietarioPrueba=" + propietarioPrueba + "]";
	}
	
}
