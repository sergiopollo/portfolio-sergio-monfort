package appVieja;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;



@Entity
@Table(name = "cantant")
public class Cantant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_cantant", nullable=false)
	private int id;
	
	@Column(name="sobrenom", nullable=true)
	private String sobrenom;
	
	@Column(name="edat", nullable=true)
	private int edat;
	
	@Column(name="productor_discografic", nullable=true, length=35)
	private int productorDiscografic;
	
	@ManyToMany(mappedBy = "cantants")
	private Set<Canco> cancons;
	
	public Set<Canco> getCancons() {
		return cancons;
	}

	public void setCancons(Set<Canco> cancons) {
		this.cancons = cancons;
	}
	
	public void addCanco(Canco canco) {
		this.cancons.add(canco);
	}

	public Cantant() {
		super();
		this.cancons=new HashSet<Canco>();
	}

	public Cantant(String sobrenom, int edat, int productorDiscografic) {
		super();
		this.sobrenom = sobrenom;
		this.edat = edat;
		this.productorDiscografic = productorDiscografic;
		this.cancons=new HashSet<Canco>();
	}
	
	

	public String getSobrenom() {
		return sobrenom;
	}

	public void setSobrenom(String sobrenom) {
		this.sobrenom = sobrenom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public int getProductorDiscografic() {
		return productorDiscografic;
	}

	public void setProductorDiscografic(int productorDiscografic) {
		this.productorDiscografic = productorDiscografic;
	}
	
	
	


	@Override
	public String toString() {
		return "Cantant [id=" + id + ", sobrenom=" + sobrenom + ", edat=" + edat + ", productorDiscografic="
				+ productorDiscografic + ", cancons=" + cancons + "]";
	}

}
