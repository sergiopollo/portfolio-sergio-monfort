package appVieja;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "persona")
public class persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_persona")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	//OneToMany. Aqui diem que volem el set (i per tant la taula), i quina variable ens far referencia, es a dir, quina variable representa la clau foranea.
	//la variable s'ha de dir com la variable de JAVA, no com la columna de SQL
	@OneToMany(mappedBy= "propietarioPrueba")
	private Set<prueba> pruebas;
	
	public Set<prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(Set<prueba> pruebas) {
		this.pruebas = pruebas;
	}

	public persona() {
		super();
		this.pruebas = new HashSet<prueba>();
	}

	public persona(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.pruebas = new HashSet<prueba>();
	}

	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	public void addPrueba(prueba prueba) {
		this.pruebas.add(prueba);
	}

	@Override
	public String toString() {
		return "persona [id=" + id + ", nombre=" + nombre + ", pruebas=" + pruebas + "]";
	}
	
	
	
}
