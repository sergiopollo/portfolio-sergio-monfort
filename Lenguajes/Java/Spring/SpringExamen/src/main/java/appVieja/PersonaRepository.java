package appVieja;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<persona, Integer>{
	
	//insert magia negra
	persona findByNombreIgnoreCaseOrderByIdAsc(String n);
	
}
