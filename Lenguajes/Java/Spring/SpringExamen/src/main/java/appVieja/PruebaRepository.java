package appVieja;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface PruebaRepository extends CrudRepository<prueba, Integer>{
	
	//insert magia negra
	prueba findByNombreIgnoreCaseOrderByIdAsc(String n);
	
}
