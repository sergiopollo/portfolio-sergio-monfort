package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

public interface CancoRepository extends CrudRepository<Canco, Integer>{
	
	//insert magia negra
	Canco findByNombreIgnoreCaseOrderByIdAsc(String n);
	
	List<Canco> findByAnyPublicacioOrderByIdAsc(int any);
	
}
