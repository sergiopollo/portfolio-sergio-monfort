package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.annotation.JsonBackReference;

@SpringBootApplication
@ComponentScan({"app"})
@EntityScan("app")
@EnableJpaRepositories("app")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}




//RECORDAR! 
//@JsonBackReference en las relaciones que den bucle infinito, solo en un lado de la relacion
