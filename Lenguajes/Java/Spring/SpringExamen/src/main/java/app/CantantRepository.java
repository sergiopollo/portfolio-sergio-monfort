package app;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

public interface CantantRepository extends CrudRepository<Cantant, Integer>{
	
	//insert magia negra
	List<Cantant> findBySobrenomIgnoreCaseOrderByIdAsc(String n);
	
}
