package com.example.uf1_1_a15_listview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.R.string;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;
import android.telephony.TelephonyManager;

public class EditorSubtitulo extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editor_subtitulo);

        Button btn = (Button) findViewById (R.id.buttonSend);
        final EditText editText = (EditText) findViewById (R.id.editTextSubtitulo);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent inData = new Intent();

                inData.putExtra("RESULTADO", editText.getText().toString());

                setResult(RESULT_OK, inData);
                // Cerramos la Activity
                finish();

            }
        });

    }
}
