package com.example.uf1_1_a12_dues_activities_intents;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class frmMensaje extends Activity implements OnClickListener{

	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        // Mostrar el view frmmensaje creat a layout
	        setContentView(R.layout.frmmensaje);
	        
	        TextView lblSaludo = (TextView) findViewById(R.id.lblMensaje);
	        // 
	        Bundle bundle =this.getIntent().getExtras();
	        lblSaludo.setText("Hola, "+ bundle.getString("NOMBRE")+" - "+ bundle.getString("TEXTE")
	         		+" - "+ bundle.getString("TEXTE2"));	        
	        
	        // Tamb� es pot fer directament:
	        lblSaludo.setText("Hola, "+ this.getIntent().getStringExtra("NOMBRE")+" - "+ this.getIntent().getStringExtra("TEXTE")
	      	        		+" - "+ this.getIntent().getStringExtra("TEXTE2"));	        
	      	        
	        
	        // Si no existeix el nomde la variable al bundle, retorna null
	        
	        
	        // Codi per retornar valor a l'activity "pare"
	        Button cmdOK = (Button) findViewById(R.id.cmdDevolverOK);	        
	        Button cmdKO = (Button) findViewById(R.id.cmdDevolverKO);
	        cmdOK.setOnClickListener(this);
	        cmdKO.setOnClickListener(this);
	        
	    }
	 // Codi per retornar valor a l'activity "pare"
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent inData = new Intent();
		switch (v.getId()) {
			case R.id.cmdDevolverOK:
				
				inData.putExtra("RESULTADO", "Resultado Correcto");
				
				
				setResult(RESULT_OK, inData);
				// Cerramos la Activity
				finish();
				break;
	
			case R.id.cmdDevolverKO:
				
				inData.putExtra("RESULTADO", "Resultado Erroneo");
				setResult(RESULT_OK, inData);
				// Cerramos la Activity
				finish();
				break;
	
	
			default:
				break;
		}
	}

	
}
