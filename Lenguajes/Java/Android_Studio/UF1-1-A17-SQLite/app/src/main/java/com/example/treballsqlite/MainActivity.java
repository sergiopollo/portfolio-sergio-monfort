package com.example.treballsqlite;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

import java.util.ArrayList;

public class MainActivity extends Activity {



	public MiAgendaBBDD miAgenda;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //  Creem l objecte de la clase SQLiteOpenHelper que hem creat:
        // Per la simple ceracio de l'objecte si la BBDD no existeix, es crea, i si la versio que li passem es superior
        // a l'anterior, es fa el metode onUpgrade
        // Si la versio es la mateixa, simplment s'obre.
        
        miAgenda = new MiAgendaBBDD(this, "Agenda", null, 1);
        // Creamos  una instancia de la BBDD obteniendo un objeto, referencia a la bbDd para escribir o
        // para leer
        // getWritableDatabase--> Per obtenir access d'escritutra a la BBDD
        // getReadableDatabase--> Per obtenir access de lectura a la BBDD
        
        // El metodes mes importants de la classe SQLiteOpenHelper:
        // close() --> Tanca conexio a la BBDD
        // getWritableDatabase--> Per obtenir access d'escritutra a la BBDD        
        // getReadableDatabase--> Per obtenir access de lectura a la BBDD
        // onCreate(SQLiteDatabase db) --> Per crear la BBDD
        // onDowngrade(SQLiteDatabase db, int oldversion, int newversion) --> Per degradar la BBDD
        // onOpen(SQLiteDatabase db) --> Per obre conexiop
        // onUpgrate(SQLiteDatabase db, int oldversion, int newversion)) --> Per actulitzar la versio de la BBDD
       
        
        /* 
         PEr accedir a la BBDD es fa en dos fases:
         Perimer, obtenim un objecte del tipo SqLiteDatabase i posteriorment fem servir les funcions per gestionar
         les dades de la BBDD. 
         Suposem que la BBDD esta completament creada, totes les seves taules, relacions, etc...
         En cas de que la sentencia retorni dades, timdrem access a un objecte CURSOR.
         
          */
         
        
 
        SQLiteDatabase db =  miAgenda.getWritableDatabase();

        // Vamos a comprobar que todo ha ido bien y luego a insertar datos
        if (db != null)    
        {
        	///////////////////////////////////////////////////////////////////////////////////
        	//DESCOMENTAR ESTO CUANDO EMPIECE A HACWER COSAS O ENTREGUE
			/////////////////////////////////////////////////////////////////////////////////////

			Cursor c0 = db.rawQuery("SELECT * FROM Agenda", null);


			if (c0.moveToFirst()==false) {
				// Insertamos 5 registros para probar
				for (int i = 1; i <= 5; i++) {
					String nombre = "Contacto " + i;
					String email = "mail" + i + "@gmail.cat";

					db.execSQL("INSERT INTO Agenda (nom, email) VALUES ('" + nombre + "', '" + email + "')");


				}
			}

        	// NOTA: Per a comprovar les dades, SQLite deixa un fitxer a 
        	// la SD:  /data/data/<paquet.java.de.la.aplicacio.>/databases/<nom_BBDD>
        	// a SQLite les dades de cada aplicacio nomes les poden visualitzar la mateixa aplicacio
        	// Per compartir dades hem de fer servir els provider services.
        	// PEr buscar-lo hem d'anar a la perspectiva DDMS de eclipse i a la pestanya "File Explorer"
        	// Evidement si tanquen el simulador s'esborra tot.
        	
        	
        	
        	// con execSQL podemos ejecutar cualquier sentencia SQL pero para los insert, delete y update
        	// existe otra forma:
        	// Con la clase ContentValues, donde ponemos la coleccion de valores por parejas Campo-valor
        	// y con el metodo insert, update o delete.
        	
        	
        	/* Per l'access a la BBDD farem servir dos tipus de metodes
        	 1) fer servir les les funcions RAWQUERY() i EXECSQL(). La primera retorna un cursor
        	  amb les dades (select), i l'EXECSQL es fa servir per sentencies que no retornes resultat
        	  (update, insert, delete) 
        	  
        	 2) Fer servir els metodes INSERT, REPLACE, QUERY i DELETE passant-li els parametres
        	 oportuns.        	          	         	 
        	 
        	 */
        	
        	
        	/*
        	ContentValues nuevoRegistro =  new ContentValues();
        	nuevoRegistro.put("nom", "Niko");
        	nuevoRegistro.put("email", "niko@gmail.com");
        	db.insert("Agenda", null, nuevoRegistro); // El segundo parametro es para cuando se inserta unregistro vacio
        	
        	// Ahora lo modificamos
        	ContentValues nuevosValores = new ContentValues();
        	nuevosValores.put("email", "nicolas@gmail.com");
        	db.update("Agenda", nuevosValores, "nom='Niko'", null);
        	
        	// Otra opcion es usar condiciones del where con parametros.
        	// Estos parametros se indican mediante una tabla donde la posicion determina el orden
        	
        	String[] argumentos = new String[] {"niko","nicolas"};
        	db.update("Agenda", nuevosValores, "nom=? or nom= ?", argumentos);
        	*/
        	// Esta forma de usar parametros nos libera de tener que crear la sentencia SQL "a pelo"
        	// contatenando cadenas y de mas yq ue nos de un error dificil de localizar.
        	
        	
        	
        	// VAMOS A CONSULTAR REGISTROS !!!!!!!!!!!!!!
        	// Como todo en la vida hay mas de una forma de hacer las cosas:
        	// 1. metodo rawQuery() de la clase SQLiteDatabase . Le tenemos que hacer nosotros el SELECT
        	// 2. metodo query() de la clase SQLiteDatabase. tiene argumentos para columnas, where, group by, having y orderby... por separado y el construye el SELECT
        	// Ambos devuelven un objeto de la clase Cursos que luego podemos recorrer.
        	
        	
        	Cursor c1 = db.rawQuery("SELECT id, nom, email FROM Agenda WHERE nom='nicolas'", null);
        	// Tambien podiamos haber usado argumentos:
        	String[] arguswhere = new String[]{"nicolas"};
        	Cursor c2 = db.rawQuery("SELECT id, nom, email FROM Agenda WHERE nom=?", arguswhere);
       
        	// la segunda forma
        	
        	String[] argus2where = new String[]{"nicolas"};
        	String[] columnas = new String[] {"id", "nom", "email"}; // Lista de coampos a recuperar.
       	
        	Cursor c3= db.query("Agenda", columnas, "nom='nicolas'", null, null, null, null); // Sin Parametros
        	Cursor c4= db.query("Agenda", columnas, "nom=?", argus2where, null, null, null); // Con parametros
        	
        	// Ahora solo faltaria recorrer el Cursor
        	// Tenemos los metodos moveToFirst y moveToNext de la clase Cursor que devuelve TRUE si han ido bien
        	// otros moveToiPRevisus, moveToLast, moveToPosition
        	// Si devuelven FALSE es que  no hay registro en esa posicion
        	// Luego, una vez situados, accedemos al campo deseado mediante getXXX(indice). 
        	// donde XXX es el tipo de datos del campo y indice es la posicio de la columna (OJO!! Empieza por 0)
        	
        	/*
        	if (c1!= null && c1.moveToFirst())
        	{
        		do {
        			int miId = c1.getInt(0);
        			String miNombre = c1.getString(1);
        			String miEmail = c1.getString(2);
        		} while (c1.moveToNext());
        		
        	}*/
        	
        	// Con getCount tenemos el numero de registros
        	
        	// Mostrem un misatge per pantalla
        	Toast.makeText(this, "Finalitzat: tot Correcte", Toast.LENGTH_LONG).show();



			Cursor c5 = db.rawQuery("SELECT * FROM Agenda", null);

			ArrayList<Titular> datosE = new ArrayList<Titular>();

			if (c5!= null && c5.moveToFirst())
			{
				do {
					int miId = c5.getInt(0);
					Log.i("Niko", "a"+miId);
					String miNombre = c5.getString(1);
					String miEmail = c5.getString(2);
					datosE.add(new Titular(miId ,miNombre, miEmail));
				} while (c5.moveToNext());

			}


			AdaptadorTitulares adaptador = new AdaptadorTitulares(this, datosE);

			ListView lstOpciones = (ListView) findViewById(R.id.lstOpcions);

			lstOpciones.setAdapter(adaptador);

			db.close(); // Cerramos la BBDD


			//esto para el boton de añadir nuevo contacto
			final EditText nuevoNombre = (EditText) findViewById(R.id.editTextNombre);
			final EditText nuevoCorreo = (EditText) findViewById(R.id.editTextCorreo);
			Button addNewContact = (Button) findViewById(R.id.buttonAdd);

			addNewContact.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View arg0){

					SQLiteDatabase db =  miAgenda.getWritableDatabase();

					String nombre = nuevoNombre.getText().toString();
					String correo = nuevoCorreo.getText().toString();

					if(!nombre.equals("") && !correo.equals("")){
						db.execSQL("INSERT INTO Agenda (nom, email) VALUES ('"+nombre+"','"+correo+"')");

						//actualizo la lista
						actualizaLista();

					}
					db.close();

				}


			});

        	//preferences
			Button editPrefs = (Button) findViewById(R.id.btnPreferences);


			editPrefs.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {

					Intent myIntent = new Intent(view.getContext(), PonerPrefs.class);

					startActivityForResult(myIntent, 0);
				}

			});


			//para recoger las preferencias
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

			//para coger los valores de cada preferencia
			////////////////ABAJO DESCOMENTAR
			/*
			text1.setText(new Boolean(prefs.getBoolean("checkboxPref", false)).toString());
			text2.setText(prefs.getString("editTextPref", "<unset>"));;
			text3.setText(prefs.getString("listPref", "<unset>"));
			text4.setText(prefs.getString("VALOR", "null"));
			*/
        	
        	// PAra comprobar los datos, podemos descargarnos el fichero al PC y comprobarlos con un 
        	// administrador de SQLite (SQLite Administrator) o bien usar la utilidad adb.exe de la 
        	// platform-tools de android (buscarlo en directorio de instalacion). 
        	// adb,exe -s emulador-5554 shell
        	// dentro del shell # ejecutamos sqlite3 /data/data/<paquet.java.de.la.aplicacio.>/databases/<nom_BBDD>
        	// y nos saldra shell sqlite. Aqui podemos hacer sentencias sql.
        	// para salir   .exit  i exit
        	
        	
        } 

        
        
        
    }


    public void actualizaLista(){

		SQLiteDatabase db =  miAgenda.getWritableDatabase();

		Cursor c5 = db.rawQuery("SELECT * FROM Agenda", null);

		ArrayList<Titular> datosE = new ArrayList<Titular>();

		if (c5!= null && c5.moveToFirst())
		{
			do {
				int miId = c5.getInt(0);
				String miNombre = c5.getString(1);
				String miEmail = c5.getString(2);
				datosE.add(new Titular(miId,miNombre, miEmail));
			} while (c5.moveToNext());

		}


		AdaptadorTitulares adaptador = new AdaptadorTitulares(this, datosE);

		ListView lstOpciones = (ListView) findViewById(R.id.lstOpcions);

		lstOpciones.setAdapter(adaptador);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		if (requestCode== 1) {
			if (resultCode==RESULT_OK){
				Toast.makeText(this, data.getStringExtra("RESULTADO").toString(), Toast.LENGTH_SHORT).show();
			} else if (resultCode==RESULT_CANCELED) {
				Toast.makeText(this, data.getStringExtra("RESULTADO").toString(), Toast.LENGTH_SHORT).show();
			} else	{
				Toast.makeText(this, "Resultado incorrecto", Toast.LENGTH_SHORT).show();
			}

			Intent refresh = new Intent(this, MainActivity.class);
			startActivity(refresh);
			this.finish();

		}else{
			Toast.makeText(this, "Activity no controlada", Toast.LENGTH_SHORT).show();
			actualizaLista();
		}

	}




	class AdaptadorTitulares extends ArrayAdapter  {

    	private ArrayList<Titular> datosE;
		//preferencias
		public SharedPreferences prefs;
		Activity context;

		public AdaptadorTitulares(Activity context, ArrayList<Titular> datosE) {
			super(context, R.layout.listitem_titular, datosE);
			this.context = (Activity) context;
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
			this.datosE=datosE;
		}


		// GetView s'executa per cada element de l'array de dades i el que fa
		// es "inflar" el layout del XML que hem creat

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);

			//////////////////////////////////////////////////////////////////////////////////
			//le sumo 1 porque en la BBDD empieza por 1 y no por 0

			//Log.i("Niko", "aaaaa");
			//Log.i("Niko", ""+datosE.get(position).getId());
			final int posicion = position;
			//Log.i("Niko","1");
			// Inflem el Lauoyt
			LayoutInflater inflater = context.getLayoutInflater();
			// Sobre el layout crear (inflat) dupliquem el layour creat amb els objectes, view personals.
			View item = inflater.inflate(R.layout.listitem_titular, null);
			// OJOOOO!!!!! hemos de hacer el findViewById del item que tenemos inflado.
			TextView lblTitulo = (TextView) item.findViewById(R.id.lblNom);
			lblTitulo.setText(datosE.get(position).getTitulo().toString());
			TextView lblSubTitulo = (TextView) item.findViewById(R.id.lblCorreo);
			// Log.i("Niko","3->"+datosE[position].getSubtitulo().toString() );
			lblSubTitulo.setText(datosE.get(position).getSubtitulo().toString());
			// Log.i("Niko","4");
			Button btnEditar = (Button) item.findViewById(R.id.btnEditar);
			Button btnBorrar = (Button) item.findViewById(R.id.btnBorrar);


			if(prefs.getBoolean("checkboxPref", false)==true){

				lblTitulo.setBackgroundColor(Color.BLACK);
				lblTitulo.setTextColor(Color.WHITE);
				lblSubTitulo.setBackgroundColor(Color.BLACK);
				lblSubTitulo.setTextColor(Color.WHITE);
				btnEditar.setBackgroundColor(Color.GRAY);
				btnBorrar.setBackgroundColor(Color.GRAY);

			}else{
				lblTitulo.setBackgroundColor(Color.WHITE);
				lblTitulo.setTextColor(Color.BLACK);
				lblSubTitulo.setBackgroundColor(Color.WHITE);
				lblSubTitulo.setTextColor(Color.BLACK);
				btnEditar.setBackgroundColor(Color.LTGRAY);
				btnBorrar.setBackgroundColor(Color.LTGRAY);
			}


			btnEditar.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View arg0){

					// Generem el Intent per cridar a la segona Activity (layoyt)
					Intent intent = new Intent(getApplicationContext(), editarContacto.class); // (MainActivity.this,frmMensaje.class);
					// Generem un bundle.
					// Un Bundle serveis per contenir tipus primetius i objectes d'altres clases
					// Amb aquesta clase podem passar dades entre diferents activitys.
					Bundle bundle = new Bundle();
					//Afegim una dada al bundle.
					bundle.putInt("ID", datosE.get(posicion).getId());
					// Afegim el bundle a l'intent
					intent.putExtras(bundle);


					// iniciem l'intent per "saltar" a l'altre pantalla
					//startActivity(intent);

					// Codi per cridar una Activity que retorna resultat;:

					startActivityForResult(intent, 1 ); // requestCode Identifica la
					// Actividad que estoy llamando. Sirve para luego recoger el resultado
					// y saber de que actividad son.

				}



			});

			btnBorrar.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View arg0){

					SQLiteDatabase db =  miAgenda.getWritableDatabase();

					Cursor c1 = db.rawQuery("SELECT * FROM Agenda", null);

					String[] argumentos = new String[]{""+datosE.get(posicion).getId()};
					db.delete("Agenda", "id=?", argumentos);

					db.close();

					//actualizo la lista
					actualizaLista();

				}


			});

			return (item);
		}

	}


}
