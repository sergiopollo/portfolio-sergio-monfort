package com.example.treballsqlite;

public class Titular {
	private int id;
	private String titulo;
	private String subtitulo;
	
	public Titular(int id, String tit, String sub) {
		this.id=id;
		titulo=tit;
		subtitulo=sub;
	}

	public int getId(){
		return id;
	}

	public String getTitulo(){
		return titulo;
	}
	
	public String getSubtitulo(){
		return subtitulo;
	}
	
}