package com.example.treballsqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class editarContacto extends Activity {

     String miNombre="";
    String miEmail="";
    int id = -1;

    EditText nombre;
    EditText email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Mostrar el view frmmensaje creat a layout
        setContentView(R.layout.editar_contacto);

         nombre = (EditText) findViewById(R.id.nombre);
         email = (EditText) findViewById(R.id.email);
        Button btnEditar = (Button) findViewById(R.id.buttonEdit);
        Button btnCancelar = (Button) findViewById(R.id.buttonCancel);


        Bundle bundle =this.getIntent().getExtras();

        id = bundle.getInt("ID");

        MiAgendaBBDD miAgenda = new MiAgendaBBDD(this, "Agenda", null, 1);
        final SQLiteDatabase db =  miAgenda.getWritableDatabase();

        Cursor c5 = db.rawQuery("SELECT * FROM Agenda WHERE id="+id, null);

        if (c5!= null && c5.moveToFirst())
        {
            do {
                //int miId = c5.getInt(0);
                miNombre = c5.getString(1);
                miEmail = c5.getString(2);
            } while (c5.moveToNext());

        }

        nombre.setText(miNombre);
        email.setText(miEmail);


        btnCancelar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){

                // TODO Auto-generated method stub
                Intent inData = new Intent();


                inData.putExtra("RESULTADO", "cancelado");


                setResult(RESULT_CANCELED, inData);
                // Cerramos la Activity
                finish();

            }


        });

        btnEditar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0){

                //edita la BBDD
                ContentValues nuevosValores = new ContentValues();
                nuevosValores.put("nom", nombre.getText().toString());
                nuevosValores.put("email", email.getText().toString());
                db.update("Agenda", nuevosValores, "id="+id, null);



                Intent inData = new Intent();


                inData.putExtra("RESULTADO", "contacto editado");


                setResult(RESULT_OK, inData);
                // Cerramos la Activity
                finish();

            }


        });


        // Si no existeix el nomde la variable al bundle, retorna null


        // Codi per retornar valor a l'activity "pare"
        //Button cmdOK = (Button) findViewById(R.id.cmdDevolverOK);
        //Button cmdKO = (Button) findViewById(R.id.cmdDevolverKO);

    }





}
