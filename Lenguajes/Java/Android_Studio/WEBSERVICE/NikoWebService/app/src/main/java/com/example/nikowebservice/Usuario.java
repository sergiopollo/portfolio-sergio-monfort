package com.example.nikowebservice;

public class Usuario {

    private String nombre;
    private String apellido;
    private Double latitud;
    private Double longitud;

    public Usuario(String nombre, String apellido,Double latitud, Double longitud) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public Double getLatitud () { return latitud; }
    public Double getLongitud () { return longitud; }

}
