package com.example.nikowebservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class partida extends Activity implements View.OnClickListener {

    private boolean GameOver = false;
    private int turno = 0;
    private String jugadorTurno = "O";
    private ArrayList<String> tablero;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partida);



        resetTablero();


        Button login = (Button) this.findViewById(R.id.reset);

        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                resetTablero();

            }


        });





    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();


    }

    private void resetTablero(){
        this.GameOver=false;
        this.turno = 0;

        ImageView r1c1 = (ImageView) findViewById(R.id.r1c1);
        r1c1.setOnClickListener(this);
        r1c1.setImageResource(R.drawable.vacio);

        ImageView r1c2 = (ImageView) findViewById(R.id.r1c2);
        r1c2.setOnClickListener(this);
        r1c2.setImageResource(R.drawable.vacio);

        ImageView r1c3 = (ImageView) findViewById(R.id.r1c3);
        r1c3.setOnClickListener(this);
        r1c3.setImageResource(R.drawable.vacio);

        ImageView r2c1 = (ImageView) findViewById(R.id.r2c1);
        r2c1.setOnClickListener(this);
        r2c1.setImageResource(R.drawable.vacio);

        ImageView r2c2 = (ImageView) findViewById(R.id.r2c2);
        r2c2.setOnClickListener(this);
        r2c2.setImageResource(R.drawable.vacio);

        ImageView r2c3 = (ImageView) findViewById(R.id.r2c3);
        r2c3.setOnClickListener(this);
        r2c3.setImageResource(R.drawable.vacio);

        ImageView r3c1 = (ImageView) findViewById(R.id.r3c1);
        r3c1.setOnClickListener(this);
        r3c1.setImageResource(R.drawable.vacio);

        ImageView r3c2 = (ImageView) findViewById(R.id.r3c2);
        r3c2.setOnClickListener(this);
        r3c2.setImageResource(R.drawable.vacio);

        ImageView r3c3 = (ImageView) findViewById(R.id.r3c3);
        r3c3.setOnClickListener(this);
        r3c3.setImageResource(R.drawable.vacio);


        tablero = new ArrayList<String>();

        for(int i = 0; i<9; i++){
            tablero.add("null");
        }
    }

    private void calculoTurno(int id, int posicion){

        ImageView imagen = (ImageView) findViewById(id);

        if(!GameOver){

            if(tablero.get(posicion).equals("null")){
                if(turno % 2 == 0){
                    imagen.setImageResource(R.drawable.o);

                    this.tablero.set(posicion, "o");


                }else{
                    imagen.setImageResource(R.drawable.x);

                    this.tablero.set(posicion, "x");
                }

                String ganador = this.compruebaGanador();

                if(ganador != null){
                    this.GameOver=true;

                    if(ganador == "draw"){
                        System.out.println("empate");
                        TextView comentario = (TextView) this.findViewById(R.id.comentario);
                        comentario.setText("empate");

                    }else{
                        System.out.println("gana "+ganador);
                        TextView comentario = (TextView) this.findViewById(R.id.comentario);
                        comentario.setText("gana "+ganador + "("+jugadorTurno+")");

                    }
                }

                this.turno++;
                if(turno % 2 == 0){
                    this.jugadorTurno = "yo";
                }else{
                    this.jugadorTurno = "el otro";
                }

            }


        }


    }

    private String compruebaGanador(){

            for (int a = 0; a < 8; a++) {
                String line = null;

                switch (a) {
                    case 0:
                        line = tablero.get(0) + tablero.get(1) + tablero.get(2);
                        break;
                    case 1:
                        line = tablero.get(3) + tablero.get(4) + tablero.get(5);
                        break;
                    case 2:
                        line = tablero.get(6) + tablero.get(7) + tablero.get(8);
                        break;
                    case 3:
                        line = tablero.get(0) + tablero.get(3) + tablero.get(6);
                        break;
                    case 4:
                        line = tablero.get(1) + tablero.get(4) + tablero.get(7);
                        break;
                    case 5:
                        line = tablero.get(2) + tablero.get(5) + tablero.get(8);
                        break;
                    case 6:
                        line = tablero.get(0) + tablero.get(4) + tablero.get(8);
                        break;
                    case 7:
                        line = tablero.get(2) + tablero.get(4) + tablero.get(6);
                        break;
                }
                //For X winner
                if (line.equals("xxx")) {
                    return "x";
                }

                // For O winner
                else if (line.equals("ooo")) {
                    return "o";
                }
            }

            boolean empate = true;

            for (int i = 0; i < 9; i++) {
                if(tablero.get(i).equals("null")){
                    empate = false;
                }
            }

            if(empate){
                return "draw";
            }
            TextView comentario = (TextView) this.findViewById(R.id.comentario);
            comentario.setText("turno de "+jugadorTurno);

            return null;

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.r1c1:

                calculoTurno(R.id.r1c1, 0);

                break;
            case R.id.r1c2:

                calculoTurno(R.id.r1c2, 1);

                break;
            case R.id.r1c3:

                calculoTurno(R.id.r1c3, 2);

                break;
            case R.id.r2c1:
                calculoTurno(R.id.r2c1, 3);

                break;
            case R.id.r2c2:
                calculoTurno(R.id.r2c2, 4);

                break;
            case R.id.r2c3:
                calculoTurno(R.id.r2c3,5);

                break;
            case R.id.r3c1:
                calculoTurno(R.id.r3c1,6);

                break;
            case R.id.r3c2:
                calculoTurno(R.id.r3c2,7);


                break;
            case R.id.r3c3:
                calculoTurno(R.id.r3c3,8);

                break;

        }



    }
}
