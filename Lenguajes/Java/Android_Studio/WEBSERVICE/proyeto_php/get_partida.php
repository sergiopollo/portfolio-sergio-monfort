<?php

$servername = "localhost";
$username = "root";
$password = "super3";
$BBDD = "webservice";

$nombreJ1 = $_POST["nombreJ1"];
$nombreJ2 = $_POST["nombreJ2"];

try {

    $conn = new PDO("mysql:host=$servername;dbname=$BBDD", $username, $password);

    //COGEMOS LA ID DEL JUGADOR 1
    $query = $conn->prepare("SELECT id FROM usuario WHERE nombre = '$nombreJ1';");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $idJ1 = $result[0]["id"];


    //COGEMOS LA ID DEL JUGADOR 2
    $query = $conn->prepare("SELECT id FROM usuario WHERE nombre = '$nombreJ2';");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $idJ2 = $result[0]["id"];


    //COGEMOS TODAS LAS PARTIDAS CON ESTOS JUGADORES
    $query = $conn->prepare("SELECT * FROM partida WHERE (idJ1 = '$idJ1' OR idJ2 = '$idJ1') AND ((idJ1 = '$idJ2' OR idJ2 = '$idJ2'));");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);


    //COMPROBAR SI ESTAN TODAS ACABADAS
    $enPartida = false;
    $idPartida = 0;
    $isJ1=false;
    foreach ($result as $partida)
    {
        if($partida["partidaAcabada"] == false){
            $enPartida = true;
            $idPartida = $partida["id"];
            if($partida["idJ1"] == $idJ1){
                $isJ1=true;
            }
            break;
        }
    }


    //DEVOLVEMOS TRUE O FALSE EN FUNCION DE SI ESTAN O NO EN PARTIDA, SI ESTAN EN PARTIDA TAMBIEN DEVOLVEMOS LA ID DE PARTIDA Y LA ID DEL JUGADOR 1
    $response=array();
    if($enPartida == false){
        $response["success"] = false;
    }else{
        $response["success"] = true;
        $response["IdJugador"] = $idJ1;
        $response["IdPartida"] = $idPartida;
        $response["isJ1"] = $isJ1;
    }
    echo json_encode($response);


} catch (PDOException $e) {
    print_r(json_encode("connection failed: " . $e->getMessage()));
}
