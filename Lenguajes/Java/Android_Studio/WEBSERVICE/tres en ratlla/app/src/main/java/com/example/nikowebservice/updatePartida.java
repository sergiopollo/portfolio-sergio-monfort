package com.example.nikowebservice;

/* **************
ESta clase sirve para la inserción de registros en la BBDD remota usando el servicioweb ya creao en el servidor

Tenemos que usar la  libreria Volley -->        implementation 'com.android.volley:volley:1.1.1'  y tenemos que añadir esa line a build.grade
Esto es para poer usar la StringRequest.
Volley es una libreria desarrollada por Google para  optimizar el envio de peticiones Http desde Android a servidores extenos.
https://developer.android.com/training/volley


*************** */

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;



public class updatePartida extends StringRequest {
    // Definimos la ruta del archivo php para escribir en la BBDD

    private static final String ruta = "http://10.0.2.2:63343/proyeto_php/get_tablero_partida.php"; // "http://10.1.131.139/niko/put_usuario.php";
    private Map<String, String> parametros;  // parametros que contentdra los valores del registro

    // Constructor
    public updatePartida (int id, Response.Listener<String> listener){
        // el listener es lo que tiene que pasar cuando le enviemos la informacion al servidor. PAra saber como ha ido.
        super(Request.Method.POST, ruta, listener, null); // LA peticion es post
        // Creamos un HaspMap para psar datos del registro
        parametros = new HashMap<>();
        parametros.put("id",""+id);
    }

    public updatePartida (int id, String tablero, int turno, Response.Listener<String> listener, String ruta){
        // el listener es lo que tiene que pasar cuando le enviemos la informacion al servidor. PAra saber como ha ido.
        super(Request.Method.POST, ruta, listener, null); // LA peticion es post
        // Creamos un HaspMap para psar datos del registro
        parametros = new HashMap<>();
        parametros.put("id",""+id);
        parametros.put("tablero",tablero);
        parametros.put("turno",""+turno);
    }

    public updatePartida (int id, String tablero, boolean partidaAcabada,int turno, Response.Listener<String> listener, String ruta){
        // el listener es lo que tiene que pasar cuando le enviemos la informacion al servidor. PAra saber como ha ido.
        super(Request.Method.POST, ruta, listener, null); // LA peticion es post
        // Creamos un HaspMap para psar datos del registro
        parametros = new HashMap<>();
        parametros.put("id",""+id);
        parametros.put("tablero",tablero);
        parametros.put("partidaAcabada",""+partidaAcabada);
        parametros.put("turno", ""+turno);
    }


    public updatePartida (int idJugador, boolean playerWins, Response.Listener<String> listener, String ruta){
        // el listener es lo que tiene que pasar cuando le enviemos la informacion al servidor. PAra saber como ha ido.
        super(Request.Method.POST, ruta, listener, null); // LA peticion es post
        // Creamos un HaspMap para psar datos del registro
        parametros = new HashMap<>();
        parametros.put("id",""+idJugador);
        parametros.put("playerWins",""+playerWins);
    }

    // Sobreescribimos map  para asegurarnos que nos devuelve la información de uestros parametros
    @Override
    protected Map<String, String> getParams() {
        return parametros;
    }
}