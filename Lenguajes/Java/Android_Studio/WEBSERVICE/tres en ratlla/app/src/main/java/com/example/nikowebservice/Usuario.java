package com.example.nikowebservice;

public class Usuario {

    private String nombre;
    private int wins;
    private int losses;

    public Usuario(String nombre, int wins,int losses) {
        this.nombre = nombre;
        this.wins = wins;
        this.losses = losses;
    }

    public String getNombre() {
        return nombre;
    }
    public int getWins() {
        return wins;
    }
    public int getLosses() {
        return losses;
    }
}
