package com.example.nikowebservice;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundManager {

    private Context pContext;
    private SoundPool sndPool;
    private float rate = 1.0f;
    private float masterVolume = 1.0f;
    private float leftVolume = 1.0f;
    private float rightVolume = 1.0f;
    private float balance = 0.5f;

    // Constructor, setup the audio manager and store the app context
    // constructor que nos permitiráinicializar un objeto de tipo SoundPool 
    // que define el volumen de los sonidos al 100% y con la capacidad de poder controlar el audio de la pista.
    public SoundManager(Context appContext)
    {
        sndPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
        pContext = appContext;
    }

    // recibe el id de la pista para que la elija de los archivosalojadosfísicamente en el proyecto
    // Load up a sound and return the id
    public int load(int sound_id)
    {
        return sndPool.load(pContext, sound_id, 1);
    }

    //es simplemente una función que nos permitareproducir la pista pasando como parámetro el resto de los ajustes como sonido, balance y velocidad.
    public void play(int sound_id)
    {
        sndPool.play(sound_id, leftVolume, rightVolume, 1, 0, rate);
    }

    // Set volume values based on existing balance value
    public void setVolume(float vol)
    {
        masterVolume = vol;

        if(balance < 1.0f)
        {
            leftVolume = masterVolume;
            rightVolume = masterVolume * balance;
        }
        else
        {
            rightVolume = masterVolume;
            leftVolume = masterVolume * ( 2.0f - balance );
        }

    }
    public void setSpeed(float speed)
    {
        rate = speed;

        // Speed of zero is invalid 
        if(rate < 0.01f)
            rate = 0.01f;

        // Speed has a maximum of 2.0
        if(rate > 2.0f)
            rate = 2.0f;
    }

    public void setBalance(float balVal)
    {
        balance = balVal;

        // Recalculate volume levels
        setVolume(masterVolume);
    }


    // nos permitiráliberar la memoria de todos los objetosSoundPool que ya no 
    // son requeridos y evitar así, que la aplicaciónalente el teléfonodonde se 
    // está ejecutando la app

    public void unloadAll()
    {
        sndPool.release();
    }

}