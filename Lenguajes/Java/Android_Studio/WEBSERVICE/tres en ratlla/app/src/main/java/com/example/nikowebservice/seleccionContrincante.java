package com.example.nikowebservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;

public class seleccionContrincante extends Activity {

    /*
      Variables globales
       */
    ListView lista;
    ArrayAdapter adaptador;
    HttpURLConnection con;

    String miNombre;

    // String DireccionServer = "http://10.1.131.139/niko";
    //String DireccionServer = "http://192.168.0.15/niko";
    //String DireccionServer = "http://localhost:63343/proyeto_php";
    String DireccionServer = "http://10.0.2.2:63343/proyeto_php";

    //create table usuario (id int NOT NULL AUTO_INCREMENT, nombre varchar(10), apellido varchar(10), latitud double, longitud double, PRIMARY KEY (id));

    //create table partida (id int NOT NULL AUTO_INCREMENT, idJ1 int, idJ2 int, partidaAcabada boolean, turno int, simboloJ1 varchar(1), simboloJ2 varchar(1), tablero varchar(18), PRIMARY KEY (id), FOREIGN KEY (idJ1) REFERENCES usuario(id), FOREIGN KEY (idJ2) REFERENCES usuario(id));

    boolean success=false;
    int idDeMiJugador=0;
    int idPartida=0;
    boolean isJ1=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seleccion_contrincante);


        Bundle bundle = getIntent().getExtras();

        miNombre = bundle.getString("nombre");



        Button preferencesButton = this.findViewById(R.id.preferences);

        preferencesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent myIntent = new Intent(view.getContext(), PonerPrefs.class);

                startActivityForResult(myIntent, 0);
            }

        });



        // requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);


        // Accedemos a la lista de usuarios del layout
        lista= (ListView) findViewById(R.id.lvUsuarios);

         /*  **********************************
        // Inici   Suponemos que tenemos el JSON  en un fichero.

        String jsonString = "";
        try {
            InputStream stream = getAssets().open("usuarios.json");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            jsonString = new String(buffer);
        } catch (IOException e) {
            Log.i("nico", "Error al leer el archivo!, " + e.getMessage());
        }
        // YA tenemos leido el fichero

        List<Usuario> usuarios = new ArrayList<>();
        try {
            JSONArray users= new JSONArray(jsonString);
            for (int i=0; i<users.length(); i++) {
                JSONObject user=users.getJSONObject(i);
                Usuario newUser = new Usuario(user.getString("nombre").toString(),
                                              user.get("apellido").toString(),
                                               user.getDouble("latitud"),
                                                user.getDouble("longitud") )  ;
                usuarios.add(newUser);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (usuarios!=null) {
            adaptador = new AdaptadorDeUsuarios(getApplicationContext(),usuarios);
            lista.setAdapter(adaptador);
        }else {
            Toast.makeText(getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
        }

        // Final de tener el JSON en un fichero
         *************************  */




        // ************   Inicio de inserción de registros
        // Suponemos que queremos insertar estos datos. Los datos pueden provenir de la funcionalidad del sistema

/*
        String nombreLocal="Eduardo";
        String apellidoLocal="MArtínez";
        Double latitudLocal=5.235;
        Double longitudLocal=-12.253;

        // Creamos el Listener que nos va a disparar cuando enviemos los datos al servidor.
        Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // Aqui ponemos lo que queremos que haga con la respuesta (response) del servidor a nuestra peticion
                // Pensemos que estamos dando de alta un nuevo registro.
                // El php envia un array en formato json, por lo tanto tenemos que tratar la información en json
                try {
                    JSONObject jsonRepuesta = new JSONObject(response);
                    boolean ok =jsonRepuesta.getBoolean("success"); // El php nos responde un array con un valor "success". Aquó lo rescatamos- Es un  boolea
                    if (ok) {
                        Toast.makeText(getApplicationContext(),"Inserción en la BBDD Correcta. Verificalo con el PhpAdmin.", LENGTH_LONG).show();
                    } else {
                        AlertDialog.Builder alerta = new AlertDialog.Builder(getApplicationContext());
                        alerta.setMessage("Fallo en el registro")
                                .setNegativeButton("Reintentar", null)
                                .create()
                                .show();
                        Toast.makeText(getApplicationContext(),"ERROR.Algo ha fallado en la inserción!!!!", LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        };
        // Una vez tenemos el Listener ya podemos llamar a la peticion
        Log.i("nico","1");
        RegistroRequest r= new RegistroRequest(nombreLocal, respuesta );
        // El Volley, es quien hace la comunicación, y permite poenr las consultas en una cola. Tenemos que poner la peticion enla cola para que se ejecute
        Log.i("nico","2");
        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.

        Log.i("nico","3");


*/
        //*************** Fin de inserción de registros




        // **Inicio Leer Datos del servidor *****************


        // Comprobar la disponibilidad de la Red --> Ojo hay que dar permiso en Manifest.XML
        // Usamos un controlador de conectividad
        ConnectivityManager connMgr =  (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infored = connMgr.getActiveNetworkInfo();  // Depreciated a partir version 29

        if (infored !=null && infored.isConnected()){
            // Hay información y esta conectado
            // Ejecutamos la Tarea Asincrona creada abajo
            // Esta tarea no abre canal
            try {

                new seleccionContrincante.JsonTask(this).execute( new URL(DireccionServer+"/get_usuarios.php"));  //http://localhost/niko/get_usuarios.php"));
                // OJO!!!! La dirección ha de ser donde tengamos el servidor web. eneste caso al ser local, es la dirección que en cada momento tenga el PC en la red. (ipConfig / All
                // y la dirección IPv4, en mi cado

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

        } else {
            makeText(this,"Error red", LENGTH_LONG).show();
        }
        //*****************************



    }



    /* ***********************************  */

    // Creamos la tasca asincrona

    public  class JsonTask extends AsyncTask<URL, Void, List<Usuario>> {
        // Mostramos un ProgressDialog para hacer mas amena la esper (Esto es prescindible--> Hay que inicializar en el onPreExecute())
        private ProgressDialog pd;

        /* Constructor */
        public JsonTask (Activity activity) {

            this.pd=new ProgressDialog (activity); // Necesitamos el Activity en el constuctor para que funcione el ProgessDialog

        }


        @Override
        protected void onPreExecute() {
            // Barra de progreso

            this.pd.setMessage("Obteniendo datos....");
            this.pd.setIndeterminate(false);
            this.pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            this.pd.setCancelable(false);
            this.pd.show();
        }


        @Override
        protected List<Usuario> doInBackground(URL... urls) {

            // DEvolveremos una lista de usuarios que llemos del servicio web
            List<Usuario> usuarios= null;

            // Establecemos conexión sobre la variable global con
            // y sobre el primero de los valores de urls
            // Es obligado que este en un try/catch
            try {

                con = (HttpURLConnection) urls[0].openConnection();
                //con.setDoOutput(true);
                //con.setRequestMethod("GET");
                //con.setRequestProperty("Content-Type", "application/json");
                // Parmatreizamos los tiempos de espera y de lectura. En milisegundos
                con.setConnectTimeout(15000);
                con.setReadTimeout(10000);
                Log.i("nico",urls[0].toString());
                // Ejecturamos conexión y evaluamos resultado
                //Log.i("nico", "eee"+con.getResponseCode());
                int codEstadoCon = con.getResponseCode();
                //int codEstadoCon = 200;
                Log.i("nico", "aaaa");
                if (codEstadoCon==200) {

                    // Provocamos un retraso para que se vea efecto del ProgessDialog  ->> mejor no poner Sleeps ni toast  en tareas asincronas,
                    for (int j = 0; j < 9999; j++) {
                        int i = 0;
                        while (i < 9999) {
                            i = i + 1;
                        }

                    }
                    // Tenemos que parsear los datos JSON que nos legan a traves de la conexión
                    InputStream in= new BufferedInputStream(con.getInputStream());
                    // Vamos parseando la entrada usando Gson a usuarios
                    GsonUsuarioParser parser = new GsonUsuarioParser();
                    usuarios = parser.leerFlujoJson(in);

                } else {

                    // Generamos un usuario ficticio para mostrar ERRO
                    usuarios = new ArrayList<>();
                    usuarios.add (new Usuario("ERROR "+codEstadoCon,0, 0));

                }
            } catch (IOException e) {
                e.printStackTrace();

                Log.i("nico", "catch");
            } finally {
                // Desconectamos en cualquera de los casos la conneción
                con.disconnect();
            }
            return usuarios;
        }


        @Override
        protected void onPostExecute(List<Usuario> usuarios) {
            // super.onPostExecute(usuarios);
            // Una vez hemos ejecutado la lectura de los objetos json tenimos que parsearlos al adatador
            if (usuarios!=null) {
                adaptador = new AdaptadorDeUsuarios(getApplicationContext(),usuarios);
                lista.setAdapter(adaptador);

            }else {

                Toast.makeText(getApplicationContext(), "Error parseando datos", Toast.LENGTH_LONG).show();
            }


            // Quitamos progress
            if (pd.isShowing()) {
                pd.dismiss();
            }

        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        // super.onActivityResult(requestCode, resultCode, data);


            //actualizaLista();
            //Intent refresh = new Intent(this, MainActivity.class);

            this.finish();
            startActivity(getIntent());



    }

    public class AdaptadorDeUsuarios extends ArrayAdapter<Usuario> {

        public SharedPreferences prefs;

        public AdaptadorDeUsuarios(Context context, List<Usuario> objects) {
            super(context, 0, objects);
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //Obteniendo una instancia del inflater
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Salvando la referencia del View de la fila
            View v = convertView;
            //Comprobando si el View no existe
            if (null == convertView) {
                //Si no existe, entonces inflarlo
                v = inflater.inflate(R.layout.item_usuario, parent, false);
            }

            RelativeLayout relativeLayout = findViewById(R.id.rlVar1);
            //Obteniendo instancias de los elementos
            TextView especieAnimal = (TextView) v.findViewById(R.id.tvNombre);
            TextView descAnimal = (TextView) v.findViewById(R.id.tvApellido);
            TextView losses = (TextView) v.findViewById(R.id.losses);

            Log.i("iker", "prefs: "+prefs.getBoolean("checkboxPref", false));
            if(prefs.getBoolean("checkboxPref", false)==true){

                relativeLayout.setBackgroundResource(R.color.black);

                especieAnimal.setTextColor(Color.WHITE);
                descAnimal.setTextColor(Color.WHITE);
                losses.setTextColor(Color.WHITE);

            }else{

                relativeLayout.setBackgroundResource(R.color.white);

                especieAnimal.setTextColor(Color.BLACK);
                descAnimal.setTextColor(Color.BLACK);
                losses.setTextColor(Color.BLACK);
            }


            //Obteniendo instancia de la Tarea en la posición actual
            Usuario item = getItem(position);

            final String nombreItem = item.getNombre();

            especieAnimal.setText(item.getNombre());

            String wins = "wins: "+item.getWins();
            descAnimal.setText(wins);

            String Stringlosses = "losses: "+item.getLosses();
            losses.setText(Stringlosses);
            //imagenAnimal.setImageResource(convertirRutaEnId(item.getImagen()));

            Button login = (Button) v.findViewById(R.id.jugar);

            login.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){


                    if(miNombre.equals(nombreItem)){

                        Toast toast = Toast.makeText(getContext(), "no puedes jugar contra ti mismo", LENGTH_LONG);
                        toast.show();

                    }else{

                        //compruebo si la partida esta creada, si esta creada me uno, si no la creo y me uno
                        getPartida(miNombre, nombreItem);

                    }
                }


            });

            //Devolver al ListView la fila creada
            return v;

        }

        /**
         * Este método nos permite obtener el Id de un drawable a través
         * de su nombre
         *
         * @param nombre Nombre del drawable sin la extensión de la imagen
         * @return Retorna un tipo int que representa el Id del recurso
         */
        private int convertirRutaEnId(String nombre) {
            Context context = getContext();
            return context.getResources()
                    .getIdentifier(nombre, "drawable", context.getPackageName());
        }
    }


    public void getPartida(final String nombreJ1, final String nombreJ2){

        final Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonRepuesta = null;
                try {
                    jsonRepuesta = new JSONObject(response);
                    success = jsonRepuesta.getBoolean("success");
                    Log.i("nico", "success: "+success);
                    if(success){
                        idDeMiJugador =jsonRepuesta.getInt("IdJugador");
                        idPartida =jsonRepuesta.getInt("IdPartida");
                        isJ1 = jsonRepuesta.getBoolean("isJ1");
                        Log.i("nico", "isJ1-1: "+isJ1);
                        Bundle bundle = new Bundle();

                        bundle.putInt("idJugador", idDeMiJugador);
                        bundle.putInt("idPartida", idPartida);
                        bundle.putBoolean("isJ1", isJ1);

                        Intent myIntent = new Intent(getApplicationContext(), partida.class);

                        myIntent.putExtras(bundle);

                        startActivityForResult(myIntent, 0);

                    }else{
                        //si succes es false es que no hay partida, creamos la partida
                        crearPartida(nombreJ1, nombreJ2);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final RequestQueue cola = Volley.newRequestQueue(this);

        final String ruta = "http://10.0.2.2:63343/proyeto_php/get_partida.php";

        RegistroRequest r = new RegistroRequest(nombreJ1,nombreJ2, respuesta, ruta);

        cola.add(r);

    }


    public void crearPartida(final String nombreJ1, final String nombreJ2){


        final Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonRepuesta = null;
                try {
                    jsonRepuesta = new JSONObject(response);
                    success =jsonRepuesta.getBoolean("success");

                    if(success){
                        //crea la partida y llama a getPartida de nuevo para que envie al jugador a la activity
                        getPartida(nombreJ1, nombreJ2);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final RequestQueue cola = Volley.newRequestQueue(this);

        final String ruta = "http://10.0.2.2:63343/proyeto_php/crear_partida.php";

        RegistroRequest r = new RegistroRequest(nombreJ1,nombreJ2, respuesta, ruta);

        cola.add(r);




    }


}
