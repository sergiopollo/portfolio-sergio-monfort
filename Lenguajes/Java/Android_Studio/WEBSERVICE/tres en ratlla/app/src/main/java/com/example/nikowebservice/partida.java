package com.example.nikowebservice;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class partida extends Activity implements View.OnClickListener {

    private boolean GameOver = false;
    private int turno = 1;
    private String jugadorTurno = "O";
    private boolean isJ1 = false;
    private ArrayList<String> tablero;
    private int idDeMiJugador;
    private int idPartida;
    private Contador counter;
    private boolean puedesColocar;

    //sonido
    private SoundManager sm;
    private int winSound, loseSound, pickSound;

    //cosas para mirar la bbdd
    int turnoNuevo;
    String tableroNuevo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.partida);

        Bundle bundle = getIntent().getExtras();

        idDeMiJugador = bundle.getInt("idJugador");
        idPartida = bundle.getInt("idPartida");
        isJ1 = bundle.getBoolean("isJ1");

        //sonidos
         sm = new SoundManager(this.getApplicationContext());
         this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
         winSound = sm.load(R.raw.cheer);
        pickSound =  sm.load(R.raw.pickup);


        Log.i("nico", "idJugador"+idDeMiJugador);
        Log.i("nico", "idPartida"+idPartida);
        Log.i("nico", "isJ1-2: "+isJ1);
        Log.i("nico", "puedesColocar: "+puedesColocar);

        resetTablero();

        counter = new Contador(300000,1000);
        Log.i("iker", "AAAAAAAAAAAAAAAAAAAAAAA");
        if(!isJ1) {
            Log.i("iker", "no soy j1 y me espero");
            counter.start();
            puedesColocar=false;
        }else {
            Log.i("iker", "soy j1 y puedo colocar");
            puedesColocar=true;
        }
/*
        Button login = (Button) this.findViewById(R.id.reset);

        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                resetTablero();

            }


        });
*/



    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();


    }

    private void resetTablero(){
        this.GameOver=false;
        this.turno = 1;

        ImageView r1c1 = (ImageView) findViewById(R.id.r1c1);
        r1c1.setOnClickListener(this);
        r1c1.setImageResource(R.drawable.vacio);

        ImageView r1c2 = (ImageView) findViewById(R.id.r1c2);
        r1c2.setOnClickListener(this);
        r1c2.setImageResource(R.drawable.vacio);

        ImageView r1c3 = (ImageView) findViewById(R.id.r1c3);
        r1c3.setOnClickListener(this);
        r1c3.setImageResource(R.drawable.vacio);

        ImageView r2c1 = (ImageView) findViewById(R.id.r2c1);
        r2c1.setOnClickListener(this);
        r2c1.setImageResource(R.drawable.vacio);

        ImageView r2c2 = (ImageView) findViewById(R.id.r2c2);
        r2c2.setOnClickListener(this);
        r2c2.setImageResource(R.drawable.vacio);

        ImageView r2c3 = (ImageView) findViewById(R.id.r2c3);
        r2c3.setOnClickListener(this);
        r2c3.setImageResource(R.drawable.vacio);

        ImageView r3c1 = (ImageView) findViewById(R.id.r3c1);
        r3c1.setOnClickListener(this);
        r3c1.setImageResource(R.drawable.vacio);

        ImageView r3c2 = (ImageView) findViewById(R.id.r3c2);
        r3c2.setOnClickListener(this);
        r3c2.setImageResource(R.drawable.vacio);

        ImageView r3c3 = (ImageView) findViewById(R.id.r3c3);
        r3c3.setOnClickListener(this);
        r3c3.setImageResource(R.drawable.vacio);


        tablero = new ArrayList<String>();

        for(int i = 0; i<9; i++){
            tablero.add("null");
        }
    }

    private void calculoTurno(int id, int posicion){

        ImageView imagen = (ImageView) findViewById(id);

        Log.i("nico", "puedesColocar: "+puedesColocar);

        if(puedesColocar){
            if(!GameOver){

                //que mire en la base de datos si es su turno


                if(tablero.get(posicion).equals("null")){
                    if(turno % 2 == 0){
                        imagen.setImageResource(R.drawable.o);

                        this.tablero.set(posicion, "o");


                    }else{
                        imagen.setImageResource(R.drawable.x);

                        this.tablero.set(posicion, "x");
                    }

                    compruebaGanador();

                    if(!GameOver){
                        this.turno++;
                        putTableroBBDD();

                        if(isJ1) {
                            if(turno % 2 == 0){
                                this.jugadorTurno = "el otro";
                                counter.start();
                                puedesColocar=false;
                            }else{
                                this.jugadorTurno = "yo";

                                puedesColocar=true;
                            }
                        }else {
                            if(turno % 2 == 0){
                                this.jugadorTurno = "yo";

                                puedesColocar=true;

                            }else{
                                this.jugadorTurno = "el otro";
                                counter.start();
                                puedesColocar=false;
                            }
                        }

                    }

                }
            }


        }


    }

    private void compruebaGanador(){

        String ganador = "partida en curso";

            for (int a = 0; a < 8; a++) {
                String line = null;

                switch (a) {
                    case 0:
                        line = tablero.get(0) + tablero.get(1) + tablero.get(2);
                        break;
                    case 1:
                        line = tablero.get(3) + tablero.get(4) + tablero.get(5);
                        break;
                    case 2:
                        line = tablero.get(6) + tablero.get(7) + tablero.get(8);
                        break;
                    case 3:
                        line = tablero.get(0) + tablero.get(3) + tablero.get(6);
                        break;
                    case 4:
                        line = tablero.get(1) + tablero.get(4) + tablero.get(7);
                        break;
                    case 5:
                        line = tablero.get(2) + tablero.get(5) + tablero.get(8);
                        break;
                    case 6:
                        line = tablero.get(0) + tablero.get(4) + tablero.get(8);
                        break;
                    case 7:
                        line = tablero.get(2) + tablero.get(4) + tablero.get(6);
                        break;
                }
                //For X winner
                if (line.equals("xxx")) {
                    ganador = "x";
                }

                // For O winner
                else if (line.equals("ooo")) {
                    ganador = "o";
                }
            }

            boolean empate = true;

            for (int i = 0; i < 9; i++) {
                if(tablero.get(i).equals("null")){
                    empate = false;
                }
            }

            if(empate){
                ganador = "draw";
            }
            TextView comentario = (TextView) this.findViewById(R.id.comentario);
            comentario.setText("turno de "+jugadorTurno);

            //ganador = null;

        if(ganador != "partida en curso"){
            this.GameOver=true;

            if(ganador == "draw"){
                System.out.println("empate");
                 comentario = (TextView) this.findViewById(R.id.comentario);
                comentario.setText("empate");

            }else{
                System.out.println("gana "+ganador);
                 comentario = (TextView) this.findViewById(R.id.comentario);
                comentario.setText("gana "+ganador + "("+jugadorTurno+")");

            }

            putTableroBBDD();

            //pongo partidaAcabada en true
            final Response.Listener<String> respuesta = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    JSONObject jsonRepuesta = null;
                    try {
                        jsonRepuesta = new JSONObject(response);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            };

            final RequestQueue cola = Volley.newRequestQueue(this);

            updatePartida r = new updatePartida(idPartida,turnTableroToString(),true, turno+1, respuesta, "http://10.0.2.2:63343/proyeto_php/acabaPartida.php");

            cola.add(r);


            //actualizo wins o losses del jugador
            Log.i("iker", "soy j1?: "+isJ1+" ha ganado: "+ganador+" "+jugadorTurno);
            if(isJ1){

                if(ganador.equals("o")){
                    updatePlayerWins(false);
                }else if(ganador.equals("x")){
                    sm.play(winSound);
                    updatePlayerWins(true);
                }

            }else{
                if(ganador.equals("o")){
                    sm.play(winSound);
                    updatePlayerWins(true);
                }else if(ganador.equals("x")){
                    updatePlayerWins(false);
                }
            }


        }

    }

    private void updatePlayerWins(boolean playerWins){
        final Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonRepuesta = null;
                try {
                    jsonRepuesta = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final RequestQueue cola = Volley.newRequestQueue(this);

        updatePartida r = new updatePartida(idDeMiJugador,playerWins, respuesta, "http://10.0.2.2:63343/proyeto_php/update_usuario_win.php");

        cola.add(r);
    }

    private void actualizaTablero(String tableroBBDD) {

        String[] tablero2 = tableroBBDD.split("-");

        tablero.clear();

        for(int i =0; i<tablero2.length; i++) {
            tablero.add(tablero2[i]);
        }

        //ahora actualiza de forma visual

        ImageView imagen=(ImageView) findViewById(R.id.r1c1);

        for(int i =0; i<tablero.size(); i++) {

            switch (i) {
                case 0:
                    imagen = (ImageView) findViewById(R.id.r1c1);

                    break;
                case 1:

                    imagen = (ImageView) findViewById(R.id.r1c2);

                    break;
                case 2:

                    imagen = (ImageView) findViewById(R.id.r1c3);

                    break;
                case 3:
                    imagen = (ImageView) findViewById(R.id.r2c1);

                    break;
                case 4:
                    imagen = (ImageView) findViewById(R.id.r2c2);

                    break;
                case 5:
                    imagen = (ImageView) findViewById(R.id.r2c3);

                    break;
                case 6:
                    imagen = (ImageView) findViewById(R.id.r3c1);

                    break;
                case 7:
                    imagen = (ImageView) findViewById(R.id.r3c2);


                    break;
                case 8:
                    imagen = (ImageView) findViewById(R.id.r3c3);

                    break;

            }

            Log.i("nico", "tablero: "+tablero);
            Log.i("nico", "tablero simbolo: "+tablero.get(i));

            if(tablero.get(i).equals("o")) {
                imagen.setImageResource(R.drawable.o);
            }else if(tablero.get(i).equals("x")){
                imagen.setImageResource(R.drawable.x);
            }
        }

        compruebaGanador();

    }

    private String turnTableroToString() {

        String tableroBBDD = "";

        for(int i =0; i<tablero.size()-1; i++) {
            tableroBBDD+=tablero.get(i)+"-";
        }
        tableroBBDD+=tablero.get(tablero.size()-1);
        return tableroBBDD;
    }

    private void putTableroBBDD(){

        final Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonRepuesta = null;
                try {
                    jsonRepuesta = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final RequestQueue cola = Volley.newRequestQueue(this);

        updatePartida r = new updatePartida(idPartida,turnTableroToString(),turno, respuesta, "http://10.0.2.2:63343/proyeto_php/update_tablero_partida.php");

        cola.add(r);

    }

    private void getTablero() {

        final Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                JSONObject jsonRepuesta = null;
                try {
                    jsonRepuesta = new JSONObject(response);
                    turnoNuevo =jsonRepuesta.getInt("turno");
                    tableroNuevo=jsonRepuesta.getString("tablero");
                    Log.i("iker", "turno: "+turno);
                    Log.i("iker", "turnoNuevo: "+turnoNuevo);
                    Log.i("iker", "tableroNuevo: "+tableroNuevo);

                    if(turnoNuevo != turno){
                        counter.cancel();
                        turno = turnoNuevo;
                        actualizaTablero(tableroNuevo);
                        puedesColocar=true;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        final RequestQueue cola = Volley.newRequestQueue(this);

        updatePartida r = new updatePartida(idPartida, respuesta);

        cola.add(r);




    }

    @Override
    public void onClick(View view) {

        Log.i("nico", "isJ1: "+isJ1);
        Log.i("nico", "puedesColocar: "+puedesColocar);

        if(puedesColocar) {

            sm.play(pickSound);

            switch (view.getId()) {
                case R.id.r1c1:
                    Log.i("nico", "nico isJ1: "+isJ1);
                    calculoTurno(R.id.r1c1, 0);

                    break;
                case R.id.r1c2:

                    calculoTurno(R.id.r1c2, 1);

                    break;
                case R.id.r1c3:

                    calculoTurno(R.id.r1c3, 2);

                    break;
                case R.id.r2c1:
                    calculoTurno(R.id.r2c1, 3);

                    break;
                case R.id.r2c2:
                    calculoTurno(R.id.r2c2, 4);

                    break;
                case R.id.r2c3:
                    calculoTurno(R.id.r2c3, 5);

                    break;
                case R.id.r3c1:
                    calculoTurno(R.id.r3c1, 6);

                    break;
                case R.id.r3c2:
                    calculoTurno(R.id.r3c2, 7);


                    break;
                case R.id.r3c3:
                    calculoTurno(R.id.r3c3, 8);

                    break;

            }
        }


    }


        //timer

        public void fin(){
            //SE ACABA EL TIEMPO HACEMOS UN TOAST DE AVISO Y LO SACAMOS A LA PANTALLA ANTERIOR
        }


        public class Contador extends CountDownTimer {

            public Contador(long millisInFuture, long countDownInterval) {
                super(millisInFuture, countDownInterval);
            }

            @Override
            public void onFinish() {
                fin();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                //Log.i("nico", "cogiendo tablero");
                getTablero();
            }

        }
}





