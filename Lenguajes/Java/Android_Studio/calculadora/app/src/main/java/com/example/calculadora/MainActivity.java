package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    public String numsCalcu = "";
    public String operador = "";
    public String memoria = "";
    public double resultat;
    public String memoriaM = "";
    public double resultatM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        TextView resultado = (TextView) findViewById(R.id.resultado);

        Button btnResultado = (Button) findViewById(R.id.btnResultado);
        btnResultado.setOnClickListener(this);
        Button btnSuma = (Button) findViewById(R.id.btnSuma);
        btnSuma.setOnClickListener(this);
        Button btnResta = (Button) findViewById(R.id.btnResta);
        btnResta.setOnClickListener(this);
        Button btnmulti = (Button) findViewById(R.id.btnmulti);
        btnmulti.setOnClickListener(this);
        Button btnDivi = (Button) findViewById(R.id.btnDivi);
        btnDivi.setOnClickListener(this);
        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        Button btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        Button btn4 = (Button) findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        Button btn5 = (Button) findViewById(R.id.btn5);
        btn5.setOnClickListener(this);
        Button btn6 = (Button) findViewById(R.id.btn6);
        btn6.setOnClickListener(this);
        Button btn7 = (Button) findViewById(R.id.btn7);
        btn7.setOnClickListener(this);
        Button btn8 = (Button) findViewById(R.id.btn8);
        btn8.setOnClickListener(this);
        Button btn9 = (Button) findViewById(R.id.btn9);
        btn9.setOnClickListener(this);
        Button btnMmas = (Button) findViewById(R.id.btnMmas);
        btnMmas.setOnClickListener(this);
        Button btnMmenos = (Button) findViewById(R.id.btnMmenos);
        btnMmenos.setOnClickListener(this);
        Button btnMC = (Button) findViewById(R.id.btnMC);
        btnMC.setOnClickListener(this);
        Button btnMR = (Button) findViewById(R.id.btnMR);
        btnMR.setOnClickListener(this);
        Button btnReset = (Button) findViewById(R.id.btnReset);
        btnReset.setOnClickListener(this);





    }

    @Override
    public void onClick(View view) {
        TextView resultado = (TextView) findViewById(R.id.resultado);

        numsCalcu = resultado.getText().toString();



        int id;


        switch (view.getId()) {
            case R.id.btn1:
                id = R.id.btn1;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn2:
                id = R.id.btn2;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn3:
                id = R.id.btn3;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn4:
                id = R.id.btn4;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn5:
                id = R.id.btn5;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn6:
                id = R.id.btn6;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn7:
                id = R.id.btn7;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn8:
                id = R.id.btn8;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btn9:
                id = R.id.btn9;
                botonNumero(id, numsCalcu, resultado);
                break;
            case R.id.btnResultado:

                switch(operador){
                    case "+":
                        resultat = Double.parseDouble(memoria) + Double.parseDouble(numsCalcu);
                        break;
                    case "-":
                        resultat = Double.parseDouble(memoria) - Double.parseDouble(numsCalcu);
                        break;
                    case "*":
                        resultat = Double.parseDouble(memoria) * Double.parseDouble(numsCalcu);
                        break;
                    case "/":
                        resultat = Double.parseDouble(memoria) / Double.parseDouble(numsCalcu);
                        break;
                }

                resultado.setText(""+resultat);

                break;
            case R.id.btnSuma:
                operador = "+";
                memoria = numsCalcu;
                resultado.setText("");
                break;
            case R.id.btnResta:
                operador = "-";
                memoria = numsCalcu;
                resultado.setText("");
                break;
            case R.id.btnmulti:
                operador = "*";
                memoria = numsCalcu;
                resultado.setText("");
                break;
            case R.id.btnDivi:
                operador = "/";
                memoria = numsCalcu;
                resultado.setText("");
                break;
            case R.id.btnMmas:
                if(memoriaM.length() > 0){
                    double numsM = Double.parseDouble(memoriaM) + Double.parseDouble(numsCalcu);
                    memoriaM = String.valueOf(numsM);
                }else{
                    memoriaM = numsCalcu;
                }
                resultado.setText("");
                break;
            case R.id.btnMmenos:
                if(memoriaM.length() > 0){
                    double numsM = Double.parseDouble(memoriaM) - Double.parseDouble(numsCalcu);
                    memoriaM = String.valueOf(numsM);
                }else{
                    memoriaM = "-"+numsCalcu;
                }
                resultado.setText("");
                break;
            case R.id.btnMC:
                memoriaM = "";
                break;
            case R.id.btnMR:
                resultado.setText(memoriaM);
                break;
            case R.id.btnReset:
                 numsCalcu = "";
                 operador = "";
                 memoria = "";
                resultado.setText("0");
                break;
            default:
                resultado.setText("mongolo");
                break;
        }
    }

    public void botonNumero(int id, String textoCalcu, TextView resultado){
        if(textoCalcu.equals("0")){
            Button b = (Button) findViewById(id);
            String textoBtn = b.getText().toString();
            resultado.setText(textoBtn);
        }else{
            Button b = (Button) findViewById(id);
            String textoBtn = b.getText().toString();
            resultado.setText(textoCalcu+textoBtn);
        }
    }



}