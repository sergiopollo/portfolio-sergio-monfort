package com.example.uf1_1_a12_dues_activities_intents;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class frmMensaje extends Activity implements OnClickListener{

	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        // Mostrar el view frmmensaje creat a layout
	        setContentView(R.layout.frmmensaje);
	        final TextView txtID = (TextView) findViewById(R.id.txtID);
		 final EditText EdittxtResultado = (EditText) findViewById(R.id.txtNombre);
		 final Button btnPulsa = (Button) findViewById (R.id.cmdDevolverOK);

		 Bundle bundle =this.getIntent().getExtras();

		 txtID.setText("soy la activity numero " + bundle.getString("ID"));


		 btnPulsa.setOnClickListener(this);



	 }


	@Override
	public void onClick(View view) {
		Intent inData = new Intent();
		EditText EdittxtResultado = (EditText) findViewById(R.id.txtNombre);
		String txtResultado = EdittxtResultado.getText().toString();
		inData.putExtra("RESULTADO", txtResultado);


		setResult(RESULT_OK, inData);
		// Cerramos la Activity
		finish();
	}
}
