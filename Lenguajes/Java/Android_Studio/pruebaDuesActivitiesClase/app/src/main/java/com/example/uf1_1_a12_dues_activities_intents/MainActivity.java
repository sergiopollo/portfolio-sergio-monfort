package com.example.uf1_1_a12_dues_activities_intents;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
//import android.support.v4.app.NavUtils;

public class MainActivity extends Activity implements OnClickListener{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		Button btnPulsa = (Button) findViewById (R.id.btnPulsa);
		btnPulsa.setOnClickListener(this);

		Button btnPulsa2 = (Button) findViewById (R.id.btnPulsa2);
		btnPulsa2.setOnClickListener(this);

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.btnPulsa:

				// Generem el Intent per cridar a la segona Activity (layoyt)
				Intent intent = new Intent(getApplicationContext(), frmMensaje.class); // (MainActivity.this,frmMensaje.class);
				// Generem un bundle.
				// Un Bundle serveis per contenir tipus primetius i objectes d'altres clases
				// Amb aquesta clase podem passar dades entre diferents activitys.
				Bundle bundle = new Bundle();
				//Afegim una dada al bundle.
				bundle.putInt("ID", 1);
				// Afegim el bundle a l'intent
				intent.putExtras(bundle);

				// iniciem l'intent per "saltar" a l'altre pantalla
				//startActivity(intent);

				// Codi per cridar una Activity que retorna resultat;:

				startActivityForResult(intent, 1 ); // requestCode Identifica la
				// Actividad que estoy llamando. Sirve para luego recoger el resultado
				// y saber de que actividad son.

				break;

			case R.id.btnPulsa2:

				// Generem el Intent per cridar a la segona Activity (layoyt)
				Intent intent2 = new Intent(getApplicationContext(), frmMensaje.class); // (MainActivity.this,frmMensaje.class);
				// Generem un bundle.
				// Un Bundle serveis per contenir tipus primetius i objectes d'altres clases
				// Amb aquesta clase podem passar dades entre diferents activitys.
				Bundle bundle2 = new Bundle();
				//Afegim una dada al bundle.
				bundle2.putInt("ID", 2);
				// Afegim el bundle a l'intent
				intent2.putExtras(bundle2);

				// iniciem l'intent per "saltar" a l'altre pantalla
				//startActivity(intent);

				// Codi per cridar una Activity que retorna resultat;:

				startActivityForResult(intent2, 2 ); // requestCode Identifica la
				// Actividad que estoy llamando. Sirve para luego recoger el resultado
				// y saber de que actividad son.

				break;


			default:
				break;
		}
	}

	// public void onCreate(Bundle savedInstanceState)


	// REcogemos lo resultado
	// Tenemos que evaluarl el ResquestCode para saber de que Activity "hija" son
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);

		TextView resultado = (TextView) findViewById(R.id.txtResultat);

		String texto = resultado.getText().toString() + "\n resultat enviat per l'activitat numero "+requestCode+" "+data.getStringExtra("RESULTADO").toString();

		resultado.setText(texto);

	}

    
}
