package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import java.util.ArrayList;

public class MyGdxGame extends ApplicationAdapter {
	//DEFINICIÓN VARIABLES
	int width; // ancho de la pantalla física.
	int height; // // alto de la pantalla física.
	OrthographicCamera cam; //La camera que usaremos.
	SpriteBatch batch; //la clase encargada de dibujar en pantalla.
	jugador j1;
	personaje cruceta;
	ArrayList<obstaculo> listaObstaculos = new ArrayList<obstaculo>();
	ArrayList<personaje> listaTrampas = new ArrayList<personaje>();
	ShapeRenderer renderer = null;
	TiledMap tiledMap;
	TiledMapRenderer tiledMapRenderer;
	BitmapFont font;
	BitmapFont font2;
	int tiempo;
	String textoGameOver;
	boolean GameOver = false;

	//FIN DEFINICIÓN VARIABLES

    /* *************private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture texture;
    private Sprite sprite;
    *************************/

	private Music music;

	@Override
	public void create () {
		//El método create(), es el PRIMER método en ser llamado al inicializarse el juego (despues del constructor)
		width = Gdx.graphics.getWidth(); //Obtenemos el ancho de la pantalla.
		height = Gdx.graphics.getHeight(); //Obtenemos el alto de la pantalla
		cam = new OrthographicCamera(width, height ); //Creamos una camera con el alto y ancho de la pantalla.
		cam.setToOrtho(false, width, height); //Posicionamos la camera a la mitad de la pantalla.
		batch = new SpriteBatch(); //inicializamos nuestro dibujador.
		tiledMap = new TmxMapLoader().load("pruebaTileMapPeque.tmx");
		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);



		obstaculo ob3 = new obstaculo(32, 992, 0, 992, 0,"obstaculoNegro.png", width, height);


		obstaculo ob1 = new obstaculo(576, 32, 0, 416, 0,"obstaculoNegro.png", width, height);
		obstaculo ob2 = new obstaculo(32, 992+32, 0, 416, 0,"obstaculoNegro.png", width, height);
		obstaculo ob5 = new obstaculo(640-64-32, 32, 0, 384+64+32, 992,"obstaculoNegro.png", width, height);
		obstaculo ob6 = new obstaculo(32, 128, 0, 928, 32,"obstaculoNegro.png", width, height);
		obstaculo ob7 = new obstaculo(32, 32, 0, 672, 32,"obstaculoNegro.png", width, height);
		obstaculo ob8 = new obstaculo(32*13, 32, 0, 480, 32*2,"obstaculoNegro.png", width, height);
		obstaculo ob9 = new obstaculo(32*4, 32, 0, 864, 768,"obstaculoNegro.png", width, height);
		obstaculo ob10 = new obstaculo(32, 32*3, 0, 864, 128,"obstaculoNegro.png", width, height);
		obstaculo ob11 = new obstaculo(32, 32*7, 0, 864+32, 224-32,"obstaculoNegro.png", width, height);
		obstaculo ob12 = new obstaculo(32+32, 32, 0, 864-32, 224+32*5,"obstaculoNegro.png", width, height);
		obstaculo ob13 = new obstaculo(32, 32*7, 0, 928, 448,"obstaculoNegro.png", width, height);
		obstaculo ob14 = new obstaculo(32, 32*4, 0, 928-64, 448,"obstaculoNegro.png", width, height);
		obstaculo ob15 = new obstaculo(32, 32*4, 0, 928-64-64-32, 448-64,"obstaculoNegro.png", width, height);
		obstaculo ob16 = new obstaculo(32, 32*4, 0, 928-64-64-32-64-64-64-32, 448-64,"obstaculoNegro.png", width, height);
		obstaculo ob17 = new obstaculo(32, 32, 0, 928-64-64-32-32, 448-64,"obstaculoNegro.png", width, height);
		obstaculo ob18 = new obstaculo(32, 32, 0, 928-64-64-32-32, 448+32,"obstaculoNegro.png", width, height);
		obstaculo ob19 = new obstaculo(32*4, 32, 0, 928-64-64-32-64-64-64, 448-64,"obstaculoNegro.png", width, height);
		obstaculo ob20 = new obstaculo(32*3, 32, 0, 928-64-64-32-64-32, 448-64-32,"obstaculoNegro.png", width, height);
		obstaculo ob21 = new obstaculo(32*6, 32, 0, 928-64-64-32-64-32-64-64-32, 480,"obstaculoNegro.png", width, height);
		obstaculo ob22 = new obstaculo(32, 32, 0, 928-64-64-32-64-32, 480+32,"obstaculoNegro.png", width, height);
		obstaculo ob23 = new obstaculo(32*6, 32, 0, 928-64-64-32-64-32, 480+64,"obstaculoNegro.png", width, height);
		obstaculo ob24 = new obstaculo(32, 32, 0, 928-64-64-32-32, 480+64+32,"obstaculoNegro.png", width, height);
		obstaculo ob25 = new obstaculo(32*10, 32, 0, 928-64-64-32-32-64-64-32, 480+64+64,"obstaculoNegro.png", width, height);
		obstaculo ob26 = new obstaculo(32*2, 32, 0, 928-64, 480+64+64+32,"obstaculoNegro.png", width, height);
		obstaculo ob27 = new obstaculo(32, 32, 0, 960, 512,"obstaculoNegro.png", width, height);
		obstaculo ob28 = new obstaculo(32*5, 32, 0, 448, 224,"obstaculoNegro.png", width, height);
		obstaculo ob29 = new obstaculo(32*3, 32, 0, 512, 160,"obstaculoNegro.png", width, height);
		obstaculo ob30 = new obstaculo(32*9, 32, 0, 512-32, 160-32,"obstaculoNegro.png", width, height);
		obstaculo ob31 = new obstaculo(32*6, 32, 0, 512-32, 160+32*4,"obstaculoNegro.png", width, height);
		obstaculo ob32 = new obstaculo(32*5, 32, 0, 512+32*6, 160+32*4,"obstaculoNegro.png", width, height);
		obstaculo ob33 = new obstaculo(32, 32*4, 0, 512+32*4, 160,"obstaculoNegro.png", width, height);
		obstaculo ob34 = new obstaculo(32, 32*3, 0, 512+32*6, 160+32,"obstaculoNegro.png", width, height);
		obstaculo ob35 = new obstaculo(32*3, 32, 0, 512+32*7, 160+32*2,"obstaculoNegro.png", width, height);
		obstaculo ob36 = new obstaculo(32*2, 32, 0, 480, 320,"obstaculoNegro.png", width, height);
		obstaculo ob37 = new obstaculo(32, 32*3, 0, 480, 320+32,"obstaculoNegro.png", width, height);
		obstaculo ob38 = new obstaculo(32, 32*3, 0, 800, 640,"obstaculoNegro.png", width, height);
		obstaculo ob39 = new obstaculo(32*4, 32, 0, 800+32, 640+32*2,"obstaculoNegro.png", width, height);
		obstaculo ob40 = new obstaculo(32, 32*3, 0, 448, 480,"obstaculoNegro.png", width, height);
		obstaculo ob41 = new obstaculo(32*4, 32, 0, 448+32, 480+32*2,"obstaculoNegro.png", width, height);
		obstaculo ob42 = new obstaculo(32*2, 32, 0, 448, 608,"obstaculoNegro.png", width, height);
		obstaculo ob43 = new obstaculo(32*5, 32, 0, 448, 672,"obstaculoNegro.png", width, height);
		obstaculo ob44 = new obstaculo(32, 32*8, 0, 576, 704,"obstaculoNegro.png", width, height);
		obstaculo ob45 = new obstaculo(32, 32*8, 0, 576+64, 704+32,"obstaculoNegro.png", width, height);
		obstaculo ob46 = new obstaculo(32, 32*2, 0, 448, 704,"obstaculoNegro.png", width, height);
		obstaculo ob47 = new obstaculo(32, 32*5, 0, 448, 704+32*3,"obstaculoNegro.png", width, height);
		obstaculo ob48 = new obstaculo(32*2, 32, 0, 448+32, 704+32*3,"obstaculoNegro.png", width, height);
		obstaculo ob49 = new obstaculo(32*2, 32, 0, 448+32, 704+32,"obstaculoNegro.png", width, height);
		obstaculo ob50 = new obstaculo(32, 32*3, 0, 448+32*2, 704+32*6,"obstaculoNegro.png", width, height);
		obstaculo ob51 = new obstaculo(32*8, 32, 0, 704, 928,"obstaculoNegro.png", width, height);
		obstaculo ob52 = new obstaculo(32, 32*6, 0, 704+32, 928-32*6,"obstaculoNegro.png", width, height);
		obstaculo ob53 = new obstaculo(32*3, 32, 0, 640, 672,"obstaculoNegro.png", width, height);
		obstaculo ob54 = new obstaculo(32, 32*2, 0, 640+32*2, 672+32,"obstaculoNegro.png", width, height);
		obstaculo ob55 = new obstaculo(32, 32, 0, 768, 800,"obstaculoNegro.png", width, height);
		obstaculo ob56 = new obstaculo(32, 32*3, 0, 768+32, 800,"obstaculoNegro.png", width, height);
		obstaculo ob57 = new obstaculo(32, 32*3, 0, 864, 832,"obstaculoNegro.png", width, height);
		obstaculo ob58 = new obstaculo(32, 32*3, 0, 864+32, 832,"obstaculoNegro.png", width, height);


		//añado obstaculos a la lista
		listaObstaculos.add(ob1);
		listaObstaculos.add(ob2);
		listaObstaculos.add(ob3);
		listaObstaculos.add(ob5);
		listaObstaculos.add(ob6);
		listaObstaculos.add(ob7);
		listaObstaculos.add(ob8);
		listaObstaculos.add(ob9);
		listaObstaculos.add(ob10);
		listaObstaculos.add(ob11);
		listaObstaculos.add(ob12);
		listaObstaculos.add(ob13);
		listaObstaculos.add(ob14);
		listaObstaculos.add(ob15);
		listaObstaculos.add(ob16);
		listaObstaculos.add(ob17);
		listaObstaculos.add(ob18);
		listaObstaculos.add(ob19);
		listaObstaculos.add(ob20);
		listaObstaculos.add(ob21);
		listaObstaculos.add(ob22);
		listaObstaculos.add(ob23);
		listaObstaculos.add(ob24);
		listaObstaculos.add(ob25);
		listaObstaculos.add(ob26);
		listaObstaculos.add(ob27);
		listaObstaculos.add(ob28);
		listaObstaculos.add(ob29);
		listaObstaculos.add(ob30);
		listaObstaculos.add(ob31);
		listaObstaculos.add(ob32);
		listaObstaculos.add(ob33);
		listaObstaculos.add(ob34);
		listaObstaculos.add(ob35);
		listaObstaculos.add(ob36);
		listaObstaculos.add(ob37);
		listaObstaculos.add(ob38);
		listaObstaculos.add(ob39);
		listaObstaculos.add(ob40);
		listaObstaculos.add(ob41);
		listaObstaculos.add(ob42);
		listaObstaculos.add(ob43);
		listaObstaculos.add(ob44);
		listaObstaculos.add(ob45);
		listaObstaculos.add(ob46);
		listaObstaculos.add(ob47);
		listaObstaculos.add(ob48);
		listaObstaculos.add(ob49);
		listaObstaculos.add(ob50);
		listaObstaculos.add(ob51);
		listaObstaculos.add(ob52);
		listaObstaculos.add(ob53);
		listaObstaculos.add(ob54);
		listaObstaculos.add(ob55);
		listaObstaculos.add(ob56);
		listaObstaculos.add(ob57);
		listaObstaculos.add(ob58);



		j1 = new jugador("caminando.png", width, height);

		personaje trampa1 = new personaje(16, 16,1,455, 100, "sierraCircular.gif", width, height, true);
		personaje trampa2 = new personaje(16, 16,1,454, 651, "sierraCircular.gif", width, height, false);


		listaTrampas.add(trampa1);
		listaTrampas.add(trampa2);

		//UI
		//cruceta
		cruceta = new personaje(100, 100,0,0, 100, "cruceta.png", width, height);
		cruceta.setRegion(0, 0, 180, 180);  // Definimos que parte de la textura queremos mostrar,

		//texto puntuacion/tiempo
		font = new BitmapFont();
		font2 = new BitmapFont();
		tiempo=0;
		textoGameOver="";

		renderer = new ShapeRenderer();






		/* **************************
		  float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1, h/w);
		batch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("data/libgdx.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);

		sprite = new Sprite(region);
		sprite.setSize(0.9f, 0.9f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);
		********************** */


		/* MUsica
		 * Creamos el objeto Music y le decimos que queremos que cuando acabe vuelva a empezar con el metodo setLooping pasando el valor true */

		music = Gdx.audio.newMusic(Gdx.files.internal("sounds/wiiMusic.mp3"));
		music.setLooping(true);
		music.play();

	}

	@Override
	public void dispose() {

		batch.dispose();


		/*  musica: Para liberar el fichero y no dejarlo abierto cuando cerremos la aplicacion se usa el metodo dispose */
		music.dispose();
	}

	@Override
	public void render () {

		if(!GameOver){

			j1.movimiento();
			cruceta.setCenter(j1.getX()+210, j1.getY()-150);

			for (personaje trampa: listaTrampas) {
				trampa.movimientoSpriteEstatico();
			}


			if(j1.getY() < 1003){
				tiempo++;
			}
		}


		if(j1.getY() > 1003){
			textoGameOver="You Win! \n Tiempo: "+tiempo;
			GameOver=true;
		}

		if(j1.isGameOver()){
			textoGameOver="Game Over \n Tiempo: "+tiempo;
			GameOver=true;
		}

		// Logica juego


		cam.position.set(j1.getX(), j1.getY(), 0);
		cam.update();
		System.out.println("x: "+j1.getX()+" y: "+j1.getY());

		//colisiones

		j1.colision(listaObstaculos, listaTrampas);

		for (personaje trampa: listaTrampas) {
			trampa.colisionLista(listaObstaculos);
		}





		// Fin logica

		Gdx.gl20.glClearColor(1, 1, 1, 1);  // Definimos colo blanco de fondo
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiamos la pantalla

		//tilemap
		tiledMapRenderer.setView(cam);
		tiledMapRenderer.render();

		batch.setProjectionMatrix(cam.combined); //Le asignamos al dibujador la camera que vamos a usar.
		// No hace falta asignarle camara y entonces usara por defecto es escenario.
		batch.begin(); //Antes de dibujar, tenemos que avisar al dibujador.

		for (obstaculo ob: listaObstaculos) {

			ob.draw(batch);

		}
		//p1.draw(batch); //Dibujamos al personaje, pasando por parametro el dibujador.
		//p2.draw(batch);
		j1.draw(batch);

		cruceta.draw(batch);

		for (personaje trampa: listaTrampas) {

			trampa.draw(batch);

		}

		//UI
		font.draw(batch, "tiempo: "+tiempo, j1.getX()-310, j1.getY()+230);
		font2.draw(batch, textoGameOver, j1.getX(), j1.getY());


		batch.end(); //cerramos al dibujador

		//dibujador de rectangulos de hitbox
		/*
		if(renderer != null){
			renderer.begin(ShapeType.Line);
			renderer.setColor(Color.GOLD);
			//renderer.rect(rec1.getX(), rec1.getY(), rec1.getWidth(), rec1.getHeight());

			for (obstaculo ob: listaObstaculos) {

				renderer.rect(ob.rec.getX(), ob.rec.getY(), ob.rec.getWidth(), ob.rec.getHeight());

			}

			for (personaje trampa: listaTrampas) {

				renderer.rect(trampa.rec.getX(), trampa.rec.getY(), trampa.rec.getWidth(), trampa.rec.getHeight());

			}
			//renderer.rect(p1.rec.getX(), p1.rec.getY(), p1.rec.getWidth(), p1.rec.getHeight());
			//renderer.rect(p2.rec.getX(), p2.rec.getY(), p2.rec.getWidth(), p2.rec.getHeight());
			renderer.rect(j1.rec.getX(), j1.rec.getY(), j1.rec.getWidth(), j1.rec.getHeight());
			renderer.end();
		}
		*/


		/* ***********************
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();
		 ***************** */
	}



	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
