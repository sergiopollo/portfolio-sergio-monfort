package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class obstaculo extends personaje {

    //constructores
    public obstaculo(String path, int widthPantalla, int heightPantalla) {
        super( path,  widthPantalla,  heightPantalla);
    }

    public obstaculo(int ancho, int alto,int speed,int posX, int posY, String path, int widthPantalla, int heightPantalla) {
        super( ancho,  alto, 0, posX,  posY,  path,  widthPantalla,  heightPantalla);
    }

    public obstaculo(int posX, int posY, String path, int widthPantalla, int heightPantalla) {
        super( posX,  posY,  path,  widthPantalla,  heightPantalla);
    }

    //funciones
    //no tiene movimiento, aunque su padre si, pero no debe usarlo

    public void colision(personaje p){

        //si choca con un NO jugador, que lo gestione
        //la colision con jugador la gestiona jugador
        if(!(p instanceof jugador)){
            if(this.rec.overlaps(p.rec)){
               //aqui cosas para las trampas que se mueven solas o algo
            }else{
                this.setColor(Color.WHITE);
            }
        }

    }



}
