package com.mygdx.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class personaje extends Sprite {

    public int ancho;
    public int alto;
    public int paso =0;
    public int cambiopaso=0;
    public int sentidoSprite =0;
    public int sentidoX = 1;
    public int sentidoY = 1;
    public int speed;
    public int widthPantalla;
    public int heightPantalla;
    public Rectangle rec;
    public boolean meMuevoVerticalmente;

    //constructores
    public personaje(String path, int widthPantalla, int heightPantalla) {
        super();
        this.setTexture(new Texture(path));
        this.ancho = 50;
        this.alto = 50;
        this.speed = 1;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(10, 50); //posicionamos el personaje en  la pantalla.
        this.rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    public personaje(int ancho, int alto,int speed,int posX, int posY, String path, int widthPantalla, int heightPantalla) {
        super();
        this.setTexture(new Texture(path));
        this.ancho = ancho;
        this.alto = alto;
        this.speed = speed;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(posX, posY); //posicionamos el personaje en  la pantalla.
        this.rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    public personaje(int ancho, int alto,int speed,int posX, int posY, String path, int widthPantalla, int heightPantalla, boolean meMuevoVerticalmente) {
        super();
        this.setTexture(new Texture(path));
        this.ancho = ancho;
        this.alto = alto;
        this.speed = speed;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(posX, posY); //posicionamos el personaje en  la pantalla.
        this.rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        this.meMuevoVerticalmente=meMuevoVerticalmente;
    }

    public personaje(int posX, int posY, String path, int widthPantalla, int heightPantalla) {
        super();
        this.setTexture(new Texture(path));
        this.ancho = 50;
        this.alto = 50;
        this.speed = 1;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(posX, posY); //posicionamos el personaje en  la pantalla.
        this.rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    //getters y setters
    public void setSpeed(int speed){
        this.speed = speed;
    }

    //funciones
    public void movimiento(){

        //movimiento del Sprite
        cambiopaso=(cambiopaso+1)%10;
        if (cambiopaso==0) {
            paso=(paso+1) % 6;
            this.setRegion(paso*43, 64* sentidoSprite, 43, 64);  // Definimos que parte de la textura queremos mostrar,
        }

        //movimiento del personaje en x
        this.setX(this.getX()+speed* sentidoX);
        rec.setX(this.getX()+speed* sentidoX);

        //movimiento del personaje en y
        this.setY(this.getY()+speed* sentidoY);
        rec.setY(this.getY()+speed* sentidoY);

        //al llegar a un borde que gire el sprite y cambie el sentido del movimiento
        if(this.getX() >= widthPantalla-ancho){
            sentidoX = sentidoX *-1;
            sentidoSprite = 1;
        }else if(this.getX() <= 0){
            sentidoX = sentidoX *-1;
            sentidoSprite = 0;
        }

        //al llegar a un borde del eje Y que rebote en sentido contrario
        if(this.getY() >= heightPantalla-alto){
            sentidoY = sentidoY *-1;
        }else if(this.getY() <= 0){
            sentidoY = sentidoY *-1;
        }

    }

    //funciones
    public void movimientoSpriteEstatico(){

        this.setRegion(0, 0, 250, 250);  // Definimos que parte de la textura queremos mostrar,


        if(meMuevoVerticalmente){
            //movimiento del personaje en y
            this.setY(this.getY()+speed* sentidoY);
            rec.setY(this.getY()+speed* sentidoY);
        }else{
            //movimiento del personaje en x
            this.setX(this.getX()+speed* sentidoX);
            rec.setX(this.getX()+speed* sentidoX);
        }



    }

    public void colision(personaje p){

        //cambio de color al chocar los rectangulos
        if(this.rec.overlaps(p.rec)){
            this.setColor(Color.RED);
        }else{
            this.setColor(Color.WHITE);
        }


        //colisiones de X
        if((this.getX() < p.getX()+p.ancho && this.getX() > p.getX() && this.getY() < p.getY()+p.alto && this.getY() > p.getY()) || (this.getX()+this.ancho > p.getX() && this.getX()+this.ancho < p.getX()+p.ancho && this.getY() < p.getY()+p.alto && this.getY() > p.getY())){
            //System.out.println("colisiono"+this.getX());
            if(this.sentidoX > 0 && p.sentidoX > 0){
                if(this.getX() < p.getX()){
                    this.sentidoX = sentidoX *-1;
                    //this.sentidoSprite =1;
                }
            }else if(this.sentidoX < 0 && p.sentidoX < 0){
                if(this.getX() > p.getX()){
                    this.sentidoX = sentidoX *-1;
                    //this.sentidoSprite =0;
                }
            }else{
                this.sentidoX = sentidoX *-1;
                if(this.sentidoSprite ==0){
                    //this.sentidoSprite =1;
                }else{
                    //this.sentidoSprite =0;
                }
            }



        }

        //colisiones de Y
        if((this.getY() < p.getY()+p.alto && this.getY() > p.getY() && this.getX() < p.getX()+p.ancho && this.getX() > p.getX()) || (this.getY()+this.alto > p.getY() && this.getY()+this.alto < p.getY()+p.alto && this.getX() < p.getX()+p.ancho && this.getX() > p.getX())){
            //System.out.println("colisiono"+this.getX());
            if(this.sentidoY > 0 && p.sentidoY > 0){
                if(this.getY() < p.getY()){
                    this.sentidoY = sentidoY *-1;
                }
            }else if(this.sentidoY < 0 && p.sentidoY < 0){
                if(this.getY() > p.getY()){
                    this.sentidoY = sentidoY *-1;
                }
            }else{
                this.sentidoY = sentidoY *-1;
                }
            }


    }

    public void colisionLista(ArrayList<obstaculo> listaObstaculos){

        for (obstaculo p: listaObstaculos) {
            //cambio de color al chocar los rectangulos
            if(this.rec.overlaps(p.rec)){
                this.setColor(Color.RED);
            }else{
                this.setColor(Color.WHITE);
            }

            //colisiones de X
            if((this.getX() < p.getX()+p.ancho && this.getX() > p.getX() && this.getY() < p.getY()+p.alto && this.getY() > p.getY()) || (this.getX()+this.ancho > p.getX() && this.getX()+this.ancho < p.getX()+p.ancho && this.getY() < p.getY()+p.alto && this.getY() > p.getY())){
                //System.out.println("colisiono"+this.getX());
                if(this.sentidoX > 0 && p.sentidoX > 0){
                    if(this.getX() < p.getX()){
                        this.sentidoX = sentidoX *-1;
                        //this.sentidoSprite =1;
                    }
                }else if(this.sentidoX < 0 && p.sentidoX < 0){
                    if(this.getX() > p.getX()){
                        this.sentidoX = sentidoX *-1;
                        //this.sentidoSprite =0;
                    }
                }else{
                    this.sentidoX = sentidoX *-1;
                    if(this.sentidoSprite ==0){
                        //this.sentidoSprite =1;
                    }else{
                        //this.sentidoSprite =0;
                    }
                }


            }

            //colisiones de Y
            if((this.getY() < p.getY()+p.alto && this.getY() > p.getY() && this.getX() < p.getX()+p.ancho && this.getX() > p.getX()) || (this.getY()+this.alto > p.getY() && this.getY()+this.alto < p.getY()+p.alto && this.getX() < p.getX()+p.ancho && this.getX() > p.getX())){
                //System.out.println("colisiono"+this.getX());
                if(this.sentidoY > 0 && p.sentidoY > 0){
                    if(this.getY() < p.getY()){
                        this.sentidoY = sentidoY *-1;
                    }
                }else if(this.sentidoY < 0 && p.sentidoY < 0){
                    if(this.getY() > p.getY()){
                        this.sentidoY = sentidoY *-1;
                    }
                }else{
                    this.sentidoY = sentidoY *-1;
                }
            }
        }

    }


}
