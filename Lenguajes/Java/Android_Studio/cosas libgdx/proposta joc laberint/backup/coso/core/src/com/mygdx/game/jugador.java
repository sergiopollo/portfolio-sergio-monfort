package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;

public class jugador extends personaje{

	private boolean noPuedesAvanzarUP = false;
	private boolean noPuedesAvanzarDOWN = false;
	private boolean noPuedesAvanzarLEFT = false;
	private boolean noPuedesAvanzarRIGHT = false;
	private boolean gameOver = false;
	private Music deathSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/oof.mp3"));
	private boolean muerto=false;

    //constructor
    public jugador(String path, int widthPantalla, int heightPantalla) {
        super(path, widthPantalla, heightPantalla);
        this.setTexture(new Texture(path));
        this.ancho = 10;
        this.alto = 10;
        this.speed = 1;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(965, 50); //posicionamos el personaje en  la pantalla.
		this.rec = new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

	public boolean isGameOver() {
		return gameOver;
	}

	//version con lista
	public void colision(ArrayList<obstaculo> listaObstaculos, ArrayList<personaje> listaTrampas){

		//si choca con un obstaculo, que sea diferente a una trampa
		//hacer que compruebe la lista, y si no esta tocando a ninguno que se pueda mover, si toca alguno que no le deje moverse blablabla
		//obstaculos (paredes)
		boolean choco = false;
		for (obstaculo p : listaObstaculos) {
			//System.out.println(p.getY());
			if(this.rec.overlaps(p.rec)){
				choco = true;
				this.setColor(Color.RED);

				float xFinal = this.getX()+this.ancho;
				float yFinal = this.getY()+this.alto;

				float xFinalObstaculo = p.getX()+p.ancho;
				float yFinalObstaculo = p.getY()+p.alto;



				//colisiones de X
				if(xFinal-1 < p.getX()){
					//System.out.println("ENTRO");
					this.noPuedesAvanzarRIGHT=true;


				}else if(this.getX() > xFinalObstaculo-1){
					this.noPuedesAvanzarLEFT=true;
				}

				//colisiones de Y
				if(yFinal-1 < p.getY()){
					//System.out.println("ENTRO");
					this.noPuedesAvanzarUP=true;


				}else if(this.getY() > yFinalObstaculo-1){
					this.noPuedesAvanzarDOWN=true;
				}

			}
		}

		if(!choco){
			this.setColor(Color.WHITE);
			this.noPuedesAvanzarRIGHT=false;
			this.noPuedesAvanzarLEFT=false;
			this.noPuedesAvanzarUP=false;
			this.noPuedesAvanzarDOWN=false;
		}

		//trampas
		for (personaje trampa : listaTrampas) {

			if(this.rec.overlaps(trampa.rec)){
				System.out.println("ouch");
				morir();
			}

		}
	}

	public void morir(){
    	if(!muerto){
    		this.muerto=true;
			this.gameOver=true;
			//le cambio el sprite
			this.setTexture(new Texture("obstaculoRojo.png"));
			deathSound.play();
		}
	}

    @Override
    public void movimiento(){

		//movimiento del Sprite
		cambiopaso=(cambiopaso+1)%10;
		if (cambiopaso==0) {
			paso=(paso+1) % 6;
			this.setRegion(paso*43, 64* sentidoSprite, 43, 64);  // Definimos que parte de la textura queremos mostrar,
		}

		//que no te deje avanzar si te chocas con un borde de la pantalla

		//control del personaje con teclas
		if(Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)){
			this.sentidoY = 1;
			//si estos booleanos estan activos que no deje avanzar en la direccion concreta
			if(!this.noPuedesAvanzarUP){
				this.setY(this.getY()+speed*sentidoY);
				rec.setY(this.getY()+speed*sentidoY);
			}
		}else if(Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)){
			this.sentidoY = -1;
			//si estos booleanos estan activos que no deje avanzar en la direccion concreta
			if(!this.noPuedesAvanzarDOWN){
				this.setY(this.getY()+speed*sentidoY);
				rec.setY(this.getY()+speed*sentidoY);
			}
		}

		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)){
			this.sentidoX = 1;
			this.sentidoSprite = 0;
			//si estos booleanos estan activos que no deje avanzar en la direccion concreta
			if(!this.noPuedesAvanzarRIGHT){
				this.setX(this.getX()+speed*sentidoX);
				rec.setX(this.getX()+speed*sentidoX);
			}
		}else if(Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)){
			this.sentidoX = -1;
			this.sentidoSprite = 1;
			//si estos booleanos estan activos que no deje avanzar en la direccion concreta
			if(!this.noPuedesAvanzarLEFT){
				this.setX(this.getX()+speed*sentidoX);
				rec.setX(this.getX()+speed*sentidoX);
			}
		}

		//control del personaje con touch
		if(Gdx.input.isTouched()){
			float xmin=480;
			float ymin=340;
			float xmax=580;
			float ymax=440;
			float xcentro=530;
			float ycentro=390;
			float xIzquierda=513;
			float xDerecha=546;
			float yArriba=373;
			float yAbajo=406;

			int pulsaX = Gdx.input.getX();
			int pulsaY = Gdx.input.getY();
			System.out.println("pulsaX: "+pulsaX+" pulsay: "+pulsaY);

			if(pulsaX >= xmin && pulsaX <=xmax && pulsaY < yArriba && pulsaY > ymin){//&& pulsaY < ycentro){
				this.sentidoY = 1;
				//si estos booleanos estan activos que no deje avanzar en la direccion concreta
				if(!this.noPuedesAvanzarUP){
					this.setY(this.getY()+speed*sentidoY);
					rec.setY(this.getY()+speed*sentidoY);
				}
			}else if(pulsaX >= xmin && pulsaX <=xmax && pulsaY > yAbajo && pulsaY < ymax){
				this.sentidoY = -1;
				//si estos booleanos estan activos que no deje avanzar en la direccion concreta
				if(!this.noPuedesAvanzarDOWN){
					this.setY(this.getY()+speed*sentidoY);
					rec.setY(this.getY()+speed*sentidoY);
				}
			}

			if(pulsaY >= ymin && pulsaY <=ymax && pulsaX > xDerecha && pulsaX < xmax){
				this.sentidoX = 1;
				this.sentidoSprite = 0;
				//si estos booleanos estan activos que no deje avanzar en la direccion concreta
				if(!this.noPuedesAvanzarRIGHT){
					this.setX(this.getX()+speed*sentidoX);
					rec.setX(this.getX()+speed*sentidoX);
				}
			}else if(pulsaY >= ymin && pulsaY <=ymax && pulsaX < xIzquierda && pulsaX > xmin){
				this.sentidoX = -1;
				this.sentidoSprite = 1;
				//si estos booleanos estan activos que no deje avanzar en la direccion concreta
				if(!this.noPuedesAvanzarLEFT){
					this.setX(this.getX()+speed*sentidoX);
					rec.setX(this.getX()+speed*sentidoX);
				}
			}
		}

		/*
		//cambio de velocidad (testing)
		if(Gdx.input.isKeyPressed(Input.Keys.E)) {

			if(this.speed == 1){
				this.setSpeed(7);
			}else{
				this.setSpeed(1);
			}
		}
		*/
    }


}
