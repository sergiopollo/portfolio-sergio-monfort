package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class MyGdxGame extends ApplicationAdapter {
	//DEFINICIÓN VARIABLES
	int width; // ancho de la pantalla física.
	int height; // // alto de la pantalla física.
	OrthographicCamera cam; //La camera que usaremos.
	SpriteBatch batch; //la clase encargada de dibujar en pantalla.
	personaje p1;
	personaje p2;
	jugador j1;
	ShapeRenderer renderer = null;

	//FIN DEFINICIÓN VARIABLES

    /* *************private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture texture;
    private Sprite sprite;
    *************************/

	private Music music;

	@Override
	public void create () {
		//El método create(), es el PRIMER método en ser llamado al inicializarse el juego (despues del constructor)
		width = Gdx.graphics.getWidth(); //Obtenemos el ancho de la pantalla.
		height = Gdx.graphics.getHeight(); //Obtenemos el alto de la pantalla
		cam = new OrthographicCamera(width, height ); //Creamos una camera con el alto y ancho de la pantalla.
		cam.setToOrtho(false, width, height); //Posicionamos la camera a la mitad de la pantalla.
		batch = new SpriteBatch(); //inicializamos nuestro dibujador.




		p1 = new personaje(170, 100,"caminando.png", width, height);
		p1.setSpeed(1);
		p2 = new personaje(170,10,"caminando.png", width, height);
		p2.setSpeed(3);
		j1 = new jugador("caminando.png", width, height);

		renderer = new ShapeRenderer();






		/* **************************
		  float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1, h/w);
		batch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("data/libgdx.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);

		sprite = new Sprite(region);
		sprite.setSize(0.9f, 0.9f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);
		********************** */


		/* MUsica
		 * Creamos el objeto Music y le decimos que queremos que cuando acabe vuelva a empezar con el metodo setLooping pasando el valor true */

		music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));
		music.setLooping(true);
		music.play();

	}

	@Override
	public void dispose() {

		batch.dispose();
		//textura.dispose();


		/*  musica: Para liberar el fichero y no dejarlo abierto cuando cerremos la aplicacion se usa el metodo dispose */
		music.dispose();
	}

	@Override
	public void render () {



		// Logica juego

		p1.movimiento();
		p2.movimiento();
		j1.movimiento();

		//prueba colisiones

		p1.colision(p2);
		p1.colision(j1);
		p2.colision(p1);
		p2.colision(j1);
		j1.colision(p1);
		j1.colision(p2);




		// Fin logica

		Gdx.gl20.glClearColor(1, 1, 1, 1);  // Definimos colo blanco de fondo
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiamos la pantalla

		batch.setProjectionMatrix(cam.combined); //Le asignamos al dibujador la camera que vamos a usar.
		// No hace falta asignarle camara y entonces usara por defecto es escenario.
		batch.begin(); //Antes de dibujar, tenemos que avisar al dibujador.
		p1.draw(batch); //Dibujamos al personaje, pasando por parametro el dibujador.
		p2.draw(batch);
		j1.draw(batch);

		batch.end(); //cerramos al dibujador

		if(renderer != null){
			renderer.begin(ShapeRenderer.ShapeType.Line);
			renderer.setColor(Color.GOLD);
			//renderer.rect(rec1.getX(), rec1.getY(), rec1.getWidth(), rec1.getHeight());
			renderer.rect(p1.rec.getX(), p1.rec.getY(), p1.rec.getWidth(), p1.rec.getHeight());
			renderer.rect(p2.rec.getX(), p2.rec.getY(), p2.rec.getWidth(), p2.rec.getHeight());
			renderer.rect(j1.rec.getX(), j1.rec.getY(), j1.rec.getWidth(), j1.rec.getHeight());
			renderer.end();
		}


		/* ***********************
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();
		 ***************** */
	}



	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

}
