package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;

public class jugador extends personaje{

    //constructor
    public jugador(String path, int widthPantalla, int heightPantalla) {
        super(path, widthPantalla, heightPantalla);
        this.setTexture(new Texture(path));
        this.ancho = 50;
        this.alto = 50;
        this.speed = 1;
        this.widthPantalla = widthPantalla;
        this.heightPantalla = heightPantalla;
        this.setSize(ancho, alto);
        this.setPosition(10, 50); //posicionamos el personaje en  la pantalla.
    }

    @Override
    public void movimiento(){

		//movimiento del Sprite
		cambiopaso=(cambiopaso+1)%10;
		if (cambiopaso==0) {
			paso=(paso+1) % 6;
			this.setRegion(paso*43, 64*sentido, 43, 64);  // Definimos que parte de la textura queremos mostrar,
		}

		//movimiento del personaje
		this.setX(this.getX()+speed*girarX);
		rec.setX(this.getX()+speed*girarX);

        //control del personaje con touch

        int xTouch = Gdx.input.getX();
        int yTouch = Gdx.input.getY();

		if(xTouch == widthPantalla/3 && yTouch == heightPantalla/3) {
			speed = 0;
		}else{
			if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
				speed++;
			}else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
				if(speed > 0) {
					speed--;
				}
			}

			if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
				girarX=-1;
				sentido = 1;
			}else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				girarX=1;
				sentido = 0;
			}
		}

		//control del personaje con teclas
		if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
			speed = 0;
		}else{
			if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
				speed++;
			}else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
				if(speed > 0) {
					speed--;
				}
			}

			if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
				girarX=-1;
				sentido = 1;
			}else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				girarX =1;
				sentido = 0;
			}
		}

		//al llegar a un borde que gire el sprite y cambie el sentido del movimiento
		if(this.getX() >= widthPantalla-ancho){
			girarX=girarX*-1;
			sentido = 1;
		}else if(this.getX() <= 0){
			girarX=girarX*-1;
			sentido = 0;
		}

    }


}
