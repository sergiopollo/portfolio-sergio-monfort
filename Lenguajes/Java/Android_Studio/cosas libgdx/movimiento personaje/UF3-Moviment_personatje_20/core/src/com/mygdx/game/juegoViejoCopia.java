
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.ApplicationAdapter;

public class juegoViejoCopia extends ApplicationAdapter {

	//DEFINICIÓN VARIABLES
	int width; // ancho de la pantalla física.
	int height; // // alto de la pantalla física.
	OrthographicCamera cam; //La camera que usaremos.
	Sprite personaje; //sera nuestro personaje, esta clase contiene los métodos y atributos
	//necesarios para que lo podamos mover, rotar y escalar de una manera mas fácil.
	SpriteBatch batch; //la clase encargada de dibujar en pantalla.
	Texture textura; //variable donde almacenaremos nuestra textura.

	int anchopersona =43;
	int altopersona = 64;
	int paso =0;
	int cambiopaso=0;
	int sentido=0;
	int girar = 1;
	int speed = 1;
	int xTouch = 0;
	int yTouch = 0;
	//FIN DEFINICIÓN VARIABLES

    /* *************private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture texture;
    private Sprite sprite;
    *************************/

    private Music music;

    @Override
    public void create () {
        //El método create(), es el PRIMER método en ser llamado al inicializarse el juego (despues del constructor)
        width = Gdx.graphics.getWidth(); //Obtenemos el ancho de la pantalla.
        height = Gdx.graphics.getHeight(); //Obtenemos el alto de la pantalla
        cam = new OrthographicCamera(width, height ); //Creamos una camera con el alto y ancho de la pantalla.
        cam.setToOrtho(false, width, height); //Posicionamos la camera a la mitad de la pantalla.
        batch = new SpriteBatch(); //inicializamos nuestro dibujador.
        textura = new Texture("caminando.png");//cargamos la Textura desde un PNG.

        //  textura = new Texture(Gdx.files.internal("data/libgdx.png"));//cargamos la Textura desde un PNG.
        textura.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear); // Le indicamos como queremos que se
        // comporte a la hora de redimensionar las imagenes: NEAREST (pixelado) BILINEAR (difuninado, mas lento al dibujar)
        // REPEATING (repetido)
        // Primer parametro al minimizar y segundo al maximizar
        textura.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Linear );

        //  TextureRegion region = new TextureRegion(textura, 0, 0, textura.getWidth()/3, textura.getHeight()/3);  //  #1

        personaje = new Sprite(textura); //  #2 (textura); //Creamos un personaje y le asignamos la textura.
        //	personaje.setOrigin(personaje.getWidth()/2, personaje.getHeight()/2);
        //	personaje.setSize(personaje.getWidth()*2, personaje.getHeight()*2); // Aumentamos tamaño de imagen

        personaje.setSize(anchopersona, altopersona);
        personaje.setRegion(paso*anchopersona, altopersona*sentido, (paso+1)*anchopersona, altopersona);  // Definimos que parte de la tetura queremos mostrar,
        // No es necesario si queremos mostrar toda
        personaje.setPosition(10, 50); //posicionamos el personaje en  la pantalla.


		/* **************************
		  float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera(1, h/w);
		batch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("data/libgdx.png"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);

		sprite = new Sprite(region);
		sprite.setSize(0.9f, 0.9f * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);
		********************** */


        /* MUsica
         * Creamos el objeto Music y le decimos que queremos que cuando acabe vuelva a empezar con el metodo setLooping pasando el valor true */

        music = Gdx.audio.newMusic(Gdx.files.internal("sounds/fear.mp3"));
        music.setLooping(true);
        music.play();

    }

    @Override
    public void dispose() {

        batch.dispose();
        textura.dispose();


        /*  musica: Para liberar el fichero y no dejarlo abierto cuando cerremos la aplicacion se usa el metodo dispose */
        music.dispose();
    }

    @Override
    public void render () {

        // Logica juego
        cambiopaso=(cambiopaso+1)%10;
        if (cambiopaso==0) {
            paso=(paso+1) % 6;
            personaje.setRegion(paso*anchopersona, altopersona*sentido, anchopersona, altopersona);  // Definimos que parte de la textura queremos mostrar,
        }

        // Fin logica

        Gdx.gl20.glClearColor(1, 1, 1, 1);  // Definimos colo blanco de fondo
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT); //Limpiamos la pantalla

        batch.setProjectionMatrix(cam.combined); //Le asignamos al dibujador la camera que vamos a usar.
        // No hace falta asignarle camara y entonces usara por defecto es escenario.
        batch.begin(); //Antes de dibujar, tenemos que avisar al dibujador.
        personaje.draw(batch); //Dibujamos al personaje, pasando por parametro el dibujador.
        /*  personaje.setRotation(90);  por si se quiere rotar */

        personaje.setX(personaje.getX()	+speed*girar);

        xTouch = Gdx.input.getX();
        yTouch = Gdx.input.getY();

        //control del personaje con touch
        if (Gdx.input.isTouched ()) {
            //System.out.println("de entrada se produjo en x =" + x + ", y =" + y);
            if (xTouch > width / 3 && xTouch < (width / 3) * 2 && yTouch > (height / 3) && yTouch < (height / 3) * 2) {
                speed = 0;

            } else {
                if (xTouch < width / 3) {
                    girar = -1;
                    sentido = 1;
                }
                if (xTouch > (width / 3) * 2) {
                    girar = 1;
                    sentido = 0;
                }

                if (xTouch > width / 3 && xTouch < (width / 3) * 2 && yTouch < (height / 3)) {
                    speed++;
                }
                if (xTouch > width / 3 && xTouch < (width / 3) * 2 && yTouch > (height / 3) * 2) {
                    if (speed > 0)
                        speed--;
                }

            }
        }


        /*
		if(xTouch == width/3 && yTouch == height/3) {
			speed = 0;
		}else{
			if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
				speed++;
			}else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
				if(speed > 0) {
					speed--;
				}
			}

			if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
				girar=-1;
				sentido = 1;
			}else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				girar =1;
				sentido = 0;
			}
		}
		*/

        //control del personaje con teclas
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            speed = 0;
        }else{
            if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
                speed++;
            }else if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
                if(speed > 0) {
                    speed--;
                }
            }

            if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                girar=-1;
                sentido = 1;
            }else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                girar =1;
                sentido = 0;
            }
        }

        //al llegar a un borde que gire el sprite y cambie el sentido del movimiento
        if(personaje.getX() >= width-anchopersona){
            girar=girar*-1;
            sentido = 1;
        }else if(personaje.getX() <= 0){
            girar=girar*-1;
            sentido = 0;
        }


        // Podriamos haber dibujado directamente la textura:
        // batch.draw(textura,10 , 10);
        batch.end(); //cerramos al dibujador




		/* ***********************
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();
		 ***************** */
    }



    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }


}
