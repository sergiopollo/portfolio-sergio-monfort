package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;

import sun.awt.image.IntegerComponentRaster;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

	Integer x, y, imgHeight, imgWidth;
	boolean reboteX = true;
	boolean reboteY = true;

	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		y=0;
		x=0;
		imgWidth = img.getWidth();
		imgHeight = img.getHeight();
	}

	@Override
	public void render () {
		ScreenUtils.clear(1, 0, 0, 1);
		//para saber la altura: Gdx.graphics.getHeight()
		//para saber la anchura: Gdx.graphics.getWidth()

		if(reboteX){
			x++;
		}else{
			x--;
		}

		if(x== Gdx.graphics.getWidth()-imgWidth || x== 0){
			if(reboteX){
				reboteX=false;
			}else{
				reboteX=true;
			}
		}

		if(reboteY){
			y++;
		}else{
			y--;
		}

		if(y== Gdx.graphics.getHeight()-imgHeight || y==0){
			if(reboteY){
				reboteY=false;
			}else{
				reboteY=true;
			}
		}

		batch.begin();
		batch.draw(img, x, y);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
