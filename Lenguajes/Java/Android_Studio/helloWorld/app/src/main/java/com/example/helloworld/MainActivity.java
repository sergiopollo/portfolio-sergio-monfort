package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        getWindow().setTitle("hola");

        //Button btnCuenta = (Button) findViewById(R.id.btn1);
        /*
        btnCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editTexto = (EditText) findViewById(R.id.edit1);
                int numLetras = editTexto.getText().toString().length();
                TextView resultado = (TextView) findViewById(R.id.texto1);
                resultado.setText("hay "+numLetras+" letras");

            }
        });
        */
        EditText editTexto = (EditText) findViewById(R.id.edit1);


        editTexto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int numLetras = editTexto.getText().toString().length();
                TextView resultado = (TextView) findViewById(R.id.texto1);
                resultado.setText("hay "+numLetras+" letras");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });




    }
}