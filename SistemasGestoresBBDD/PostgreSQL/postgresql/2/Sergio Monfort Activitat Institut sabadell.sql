--FASE 1

--Ex 2

create view cicles_info
as select c.nom , c.descripcio, c.id , m.modul , m.nom as nnom ,m.hores as mhores , u.hores , (m.hores - u.hores) as hores_lliures, COUNT(m.id)
from institut.escola.cicles c 
join institut.escola.moduls m on m.cicle = c.id 
join institut.escola.unitatsformatives u on u.idmodul = m.id 
group by c.nom , c.descripcio, c.id , m.modul , m.nom ,m.hores , u.hores , (m.hores - u.hores)

--Ex 2.2

select concat(a.cognom1 , ' ', a.cognom2, ', ', a.nom ) as nomComplet, m.modul , u.unitat, q.nota1c , concat(p.cognom , ', ', p.nom ) as professor
from institut.escola.alumnes a
left join institut.escola.grupsunitatsformatives g on g.idgrup = a.grup 
left join institut.escola.unitatsformatives u on u.id = g.idunitatformativa 
left join institut.escola.moduls m on u.idmodul = m.id 
left join institut.escola.professor p on g.idprofessor = p.id 
left join institut.escola.qualificacions q on q.idalumne = a.id 



--FASE 2

--a la fase 1 ho he fet tot a institut, ara ho fare a ies21.

--Ex 3:

create user adminies createrole password 'super3';

ALTER DATABASE ies21 OWNER TO adminies;


--Ex 4:

--psql -U adminies -d ies21

create schema curriculum;

create schema cursorganitzacio;

create schema cursalumnat;

--entro desde l'usuari postgres psql -U postgres -d ies21

ALTER TABLE escola.alumnes OWNER TO adminies;
ALTER TABLE escola.aules OWNER TO adminies;
ALTER TABLE escola.cicles OWNER TO adminies;
ALTER TABLE escola.grups OWNER TO adminies;
ALTER TABLE escola.grupsunitatsformatives OWNER TO adminies;
ALTER TABLE escola.moduls OWNER TO adminies;
ALTER TABLE escola.professor OWNER TO adminies;
ALTER TABLE escola.qualificacions OWNER TO adminies;
ALTER TABLE escola.unitatsformatives OWNER TO adminies;

--torno a entrar desde adminies psql -U adminies -d ies21

ALTER TABLE escola.alumnes SET SCHEMA cursalumnat;

ALTER TABLE escola.qualificacions SET SCHEMA cursalumnat;

ALTER TABLE escola.grups SET SCHEMA cursorganitzacio;

ALTER TABLE escola.aules SET SCHEMA cursorganitzacio;

ALTER TABLE escola.professor SET SCHEMA cursorganitzacio;

ALTER TABLE escola.grupsunitatsformatives SET SCHEMA cursorganitzacio;

ALTER TABLE escola.cicles SET SCHEMA curriculum;

ALTER TABLE escola.moduls SET SCHEMA curriculum;

ALTER TABLE escola.unitatsformatives SET SCHEMA curriculum;


--Ex 5:

create role gestor;

create role capEstudis;

create role professor;

create role alumne noinherit;

GRANT all privileges ON ALL TABLES IN SCHEMA curriculum TO gestor;

GRANT select ON ALL TABLES IN SCHEMA curriculum TO capEstudis;

GRANT all privileges ON ALL TABLES IN SCHEMA cursorganitzacio TO capEstudis;

GRANT select ON ALL TABLES IN SCHEMA curriculum TO professor;

GRANT select ON ALL TABLES IN SCHEMA cursorganitzacio TO professor;

GRANT all privileges ON ALL TABLES IN SCHEMA cursalumnat TO professor;

GRANT select ON ALL TABLES IN SCHEMA cursalumnat TO alumne;

--Ex 6:

--els hi poso de password super3 a tots per comoditat de no haver d'escriure una diferent amb cada usuari.

create user super password 'super3';
create user supergestor password 'super3';
create user cap password 'super3';
create user gemma password 'super3';
create user gregorio password 'super3';
create user joan password 'super3';
create user alumne1 password 'super3';
create user alumne2 password 'super3';

grant gestor to super;

grant capestudis to super;

grant professor to super;

grant alumne to super;

grant gestor to supergestor;

grant capestudis to cap;

grant professor to gemma;

grant professor to gregorio;

grant professor to joan;

grant alumne to alumne1;

grant alumne to alumne2;


--Ex 7:






