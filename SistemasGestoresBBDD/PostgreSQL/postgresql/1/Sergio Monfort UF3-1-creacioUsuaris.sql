
--#1

create database hotel;
\i hotel.sql
create database miniwind;
\i miniwind.sql


--#2

create user terrassa;

--No es pot perque l'usuari no t� contrasenya i es necessita una per poder accedir. (The server requested password-based authentication, but no password was provided.)


--#3:

create user sabadell password 'super3';

-- Si m'ha deixat perque l'usuari si que t� contrasenya.


--#4: 

--No he pogut perque l'usuari sabadell no t� permisos d'edici� (SQL Error [42501]: ERROR: permiso denegado al esquema hotel Position: 15).


--#5:

--Tenen els permisos connect, create temp tamble, execute i usage.


--#6: 

\du

--
--                                     Lista de roles
-- Nombre de rol |                         Atributos                          | Miembro de
---------------+------------------------------------------------------------+------------
-- postgres      | Superusuario, Crear rol, Crear BD, Replicaci�n, Ignora RLS | {}
-- sabadell      |                                                            | {}
-- terrassa      |                                                            | {}                                                        


--#7:

--no es pot fer la connexi� amb aquest usuari ja que no t� contrasenya;


--#8: 

psql -h 127.0.0.1 -U postgres -d hotel


--#9: 

GRANT SELECT ON customers TO sabadell;

SELECT * FROM hotel.customers;

--Es poden fer consultes des de la consola, des del dbeaver no es pot.


--#10:

\dp customers;


--#11:

-- Esquema |  Nombre   | Tipo  |        Privilegios        | Privilegios de acceso a columnas | Pol�ticas
-----------+-----------+-------+---------------------------+----------------------------------+-----------
-- hotel   | customers | tabla | postgres=arwdDxt/postgres+|                                  |
--         |           |       | sabadell=r/postgres       |                                  |

--surt que l'usuari sabadell t� permisos de select (r) a la taula.


--#12:

GRANT UPDATE ON bookings TO sabadell;
GRANT INSERT ON bookings TO sabadell;

INSERT INTO roomtypes VALUES ('9','habitacion to flama', '2', '69');

--funciona


--#13:

\dp

--                                                      Privilegios
-- Esquema |          Nombre           |   Tipo    |        Privilegios        | Privilegios de acceso a columnas | Pol�ticas
-----------+---------------------------+-----------+---------------------------+----------------------------------+-----------
-- hotel   | bookingcalendar           | tabla     |                           |                                  |
-- hotel   | bookings                  | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=aw/postgres      |                                  |
-- hotel   | bookings_id_seq           | secuencia |                           |                                  |
-- hotel   | customers                 | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |

--(la resta de taules no tenien cap permis aixi que no les copio)                                 


--#14:

GRANT SELECT ON ALL TABLES IN SCHEMA hotel TO sabadell;

SELECT * FROM hotel.roomtypes;

--                                                        Privilegios
-- Esquema |          Nombre           |   Tipo    |        Privilegios        | Privilegios de acceso a columnas | Pol�ticas
-----------+---------------------------+-----------+---------------------------+----------------------------------+-----------
-- hotel   | bookingcalendar           | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | bookings                  | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=arw/postgres     |                                  |
-- hotel   | bookings_id_seq           | secuencia |                           |                                  |
-- hotel   | customers                 | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | customers_id_seq          | secuencia |                           |                                  |
-- hotel   | facilities                | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | facilities_id_seq         | secuencia |                           |                                  |
-- hotel   | hosts                     | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | hosts_id_seq              | secuencia |                           |                                  |
-- hotel   | priceseasons              | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | priceseasons_id_seq       | secuencia |                           |                                  |
-- hotel   | rooms                     | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | rooms_roomnumber_seq      | secuencia |                           |                                  |
-- hotel   | roomtypefacilities        | tabla     | postgres=arwdDxt/postgres+|                                  |
--         |                           |           | sabadell=r/postgres       |                                  |
-- hotel   | roomtypefacilities_id_seq | secuencia |                           |                                  |
-- hotel   | roomtypes                 | tabla     | postgres=arwdDxt/postgres+|                                  |

--funciona


--#15:

GRANT SELECT ON products TO sabadell;

--                                                      Privilegios
-- Esquema  |        Nombre        |   Tipo    |        Privilegios        | Privilegios de acceso a columnas | Pol�ticas 
------------+----------------------+-----------+---------------------------+----------------------------------+---------
-- miniwind | products             | tabla     | postgres=arwdDxt/postgres+|                                  |
--          |                      |           | sabadell=r/postgres       |                                  |


--funciona


--#16:

DROP USER terrassa;

REASSIGN OWNED BY sabadell TO postgres;

DROP OWNED BY sabadell;

DROP USER sabadell;

